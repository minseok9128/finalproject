package com.kh.sm.client.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.kh.sm.client.model.service.ClientService;
import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.lead.model.vo.Lead;

@Controller
public class ClientController {
	
	@Autowired
	private final ClientService cs;
	
	public ClientController(ClientService cs) {
		this.cs = cs;
	}
	
	@PostMapping("selectClient.cl")
	public void selectClient(int companyId, HttpServletResponse response) {
		List<Client> list = null;
		System.out.println(companyId);
		if(companyId == 0) {
			list = cs.selectClient();
		} else {
			
			list = cs.selectClientToCompany(companyId);
		}
		try {
			System.out.println(list);
		
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@PostMapping("selectClient2.cl")
	public void selectClient(@RequestParam("companyId") int companyId,@RequestParam("memberId") int memberId, HttpServletRequest request, HttpServletResponse response) {
		List<Client> list = null;
		
		System.out.println("회원 번호 : "+memberId);
		
		if(companyId == 0) {
			list = cs.selectClient(memberId);
		} else {
			list = cs.selectClientToCompany(companyId, memberId);
		}
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@GetMapping("goInsert.cl")
	public String goInsertClient(int num, Model model) {
		
		Lead lead = cs.selectOne(num);
		System.out.println("lead : "+ lead);
		model.addAttribute("lead", lead);
		
		return "sales/clientManagement/client/insertClient";
	}
	@GetMapping("goInsert2.cl")
	public String goInsertClient2(Model model) {
		return "sales/clientManagement/client/insertClient";
	}
	//고객 리스트 불러오기
	@GetMapping("clientList.cl")
	public String ClientList( Model model, HttpServletRequest request) {
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		
		
		List<Client> list = cs.clientList(empNo);
		System.out.println("list : "+ list);
		model.addAttribute("list", list);
		
		return "sales/clientManagement/client/clientList2";
	}
	
	@PostMapping("insert.cl")
	public String insertClient(Client client,MultipartHttpServletRequest multipartHttpServletRequest, Model model ) {
		
		
//		int leadId = Integer.parseInt(multipartHttpServletRequest.getParameter("leadId"));
		System.out.println("client : " + client);
		
		//맥일 경우 와 윈도우일 경우 경로 변경 
		String osName = System.getProperty("os.name").toLowerCase();
		System.out.println("osname ; " + osName);
		String slash = "";
		if(osName.equals("mac os x")) {
			slash = "/";
		} else {
			slash = "\\";
		}
		
		MultipartFile multipartFile = null;//파일을 저장함 
		String originalFileName = null;//이름 담기 위해
		//경로
//		String root = request.getSession().getServletContext().getRealPath("resources");
		String root = multipartHttpServletRequest.getSession().getServletContext().getRealPath("resources");
		//업로드 경로
		//파일경로 String filePath = root + slash +"uploadFiles" + slash +  "files";
		//사진경로
		String filePath = root + slash +"uploadFiles" + slash +  "pictures";
		//객체에 담을 리스트
		List<Attachment> fList = null;
		//멀티파트로 바꾸는거 사실 메소드에 써도 되느느듯
//		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		//반복해서 돌리려고
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		//다중 파일을 저장할 List생성
		fList = new ArrayList<Attachment>();
		
		//여기서 업로드도 하고 삭제도 하고 리스트에 담음
		//조건문으로 파일이 있을때랑 없을 때 걸르기
		while(iterator.hasNext()){
			Attachment at = new Attachment();
			multipartFile = multipartHttpServletRequest.getFile(iterator.next());
			originalFileName = multipartFile.getOriginalFilename();
			String ext = originalFileName.substring(originalFileName.lastIndexOf("."));	//확장자 이전부터 잘라서 ext라는 확장자로 저장
			String changeName = CommonUtils.getRandomString();	//32비트의 랜덤한 문자열 생성
			at.setOriginName(originalFileName);
			at.setChangeName(changeName + ext);
			at.setFilePath(filePath);
			try {
				multipartFile.transferTo(new File(filePath + slash + changeName + ext));
				
			} catch (Exception e) {
				System.out.println("여기는 니나노");
				for(Attachment list : fList) {
					new File(filePath + slash + list.getChangeName() + ext).delete();
				}
				model.addAttribute("msg", "회원가입 실패");
				return "commons/errorPage";
				
			}

			fList.add(at);

		}
		client.setAttList((ArrayList<Attachment>) fList);
		System.out.println("flist :" + fList);
		if(client.getKeyMan().equals("on")) {
			client.setKeyMan("Y");
		} else {
			client.setKeyMan("N");
		}
		//db에 없는 고객이면 등록하고 
		System.out.println("고객사 정보 : " + client);
		int result = cs.insertClient(client);
		System.out.println("고객사 정보 : " + client);
		
		return "redirect:/clientList.cl";
	}
}
