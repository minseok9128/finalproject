package com.kh.sm.client.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.lead.model.vo.Lead;

public interface ClientDao {

	List<Client> selectComapny(SqlSessionTemplate sqlSession, int memberId);

	List<Client> selectCompanyToClient(SqlSessionTemplate sqlSession, int companyId , int memberId);

	int insertClient(SqlSessionTemplate sqlSession, Client client);

	Lead selectOne(SqlSessionTemplate sqlSession, int num);

	int insertCompany(SqlSessionTemplate sqlSession, String comName);

	Company checkComName(SqlSessionTemplate sqlSession, String comName);

	int updateLead(SqlSessionTemplate sqlSession, Client client);

	List<Client> selectList(SqlSessionTemplate sqlSession, int empNo);
	
	List<Client> selectComapny(SqlSessionTemplate sqlSession);
	
	List<Client> selectCompanyToClient(SqlSessionTemplate sqlSession, int companyId);

	


}
