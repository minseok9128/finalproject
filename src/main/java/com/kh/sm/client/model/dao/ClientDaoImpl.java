package com.kh.sm.client.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.lead.model.vo.Lead;

@Repository
public class ClientDaoImpl implements ClientDao {
	
	//고객 모달에서 고객 정보 불러오는 메소드
	@Override
	public List<Client> selectComapny(SqlSessionTemplate sqlSession , int memberId) {

		List<Client> list = sqlSession.selectList("Client.selectAll2", memberId);
		
		return list;
	}
	
	@Override
	public List<Client> selectComapny(SqlSessionTemplate sqlSession) {

		List<Client> list = sqlSession.selectList("Client.selectAll");
		
		return list;
	}

	@Override
	public List<Client> selectCompanyToClient(SqlSessionTemplate sqlSession, int companyId, int memberId) {

		HashMap<String, Integer> id = new HashMap<String, Integer>();
		id.put("companyId", companyId);
		id.put("memberId", memberId);
		
		//고객사 아이디를 이용한 고객 조회
		List<Client> list = sqlSession.selectList("Client.selectCompany2", id);
		
		return list;
	}

	@Override
	public int insertClient(SqlSessionTemplate sqlSession, Client client) {
		int result = sqlSession.insert("Client.insert", client);
		
		for(Attachment att : client.getAttList()) {
			sqlSession.insert("Client.fileInsert", att);
		}
		return result;
	}

	@Override
	public Lead selectOne(SqlSessionTemplate sqlSession, int num) {
		
		return sqlSession.selectOne("Client.selectOne", num);
	}

	@Override
	public int insertCompany(SqlSessionTemplate sqlSession, String comName) {
			System.out.println("comName  : " + comName );
			Company company = new Company();
			company.setComName(comName);
			sqlSession.insert("Client.insertCompany",company);
		return company.getComId();
	}

	@Override
	public Company checkComName(SqlSessionTemplate sqlSession, String comName) {
		
		Company company = new Company();
		company.setComName(comName);
				
		List<Company> comList = sqlSession.selectList("Client.checkComName",company);
		try {
			company = (Company) comList.get(0);
		}catch (Exception e) {
			company = new Company();
		}
		return company;
	}

	@Override
	public int updateLead(SqlSessionTemplate sqlSession, Client client) {
		return sqlSession.update("Client.updateLead",client);
	}

	@Override
	public List<Client> selectList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Client.selectList", empNo);
	}
	
	@Override
	public List<Client> selectCompanyToClient(SqlSessionTemplate sqlSession, int companyId) {
		
		//고객사 아이디를 이용한 고객 조회
		List<Client> list = sqlSession.selectList("Client.selectCompany", companyId);
		
		return list;
	}
	

	

}
