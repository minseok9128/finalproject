package com.kh.sm.client.model.service;

import java.util.ArrayList;
import java.util.List;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.lead.model.vo.Lead;

public interface ClientService {

	List<Client> selectClient(int memberId);

	List<Client> selectClientToCompany(int companyId, int memberId);

	int insertClient(Client client);

	Lead selectOne(int num );
	
	List<Client> selectClient();
	
	List<Client> selectClientToCompany(int companyId);

	List<Client> clientList(int empNo);

}
