package com.kh.sm.client.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.client.model.dao.ClientDao;
import com.kh.sm.client.model.vo.Client;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.lead.model.vo.Lead;

@Service
public class ClientServiceImpl implements ClientService {
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private final ClientDao cd;
	
	public ClientServiceImpl(ClientDao cd) {
		this.cd = cd;
	}
	
	@Override
	public List<Client> selectClient(int memberId) {
		
		List<Client> list = cd.selectComapny(sqlSession, memberId);
		
		return list;
	}

	@Override
	public List<Client> selectClientToCompany(int companyId, int memberId) {
		
		List<Client> list = cd.selectCompanyToClient(sqlSession, companyId, memberId);
		
		return list;
	}

	@Override
	public int insertClient(Client client) {
		
		int result = 0;
		
		if(client.getComId() == 0) {
			//고객사가 있는지 찾는 부분 (이름으로 )
			String comName = client.getComName();
			Company com = cd.checkComName(sqlSession,comName);
			System.out.println("getCom : " + com);
			System.out.println("getname : " + com.getComName());
			if(com.getComName() != null) {
				//있으면 찾아서 인서
				client.setComName(com.getComName());
				client.setComId(com.getComId());
			} else {
				//없으면 인서트해서 값 넣기		
				//고객사가 없을 때 인서트하는 부분
				int comid = cd.insertCompany(sqlSession, client.getComName());
				System.out.println("인서ㅡㅌ한  고객사 아이디 : " + comid);
				System.out.println("인서ㅡㅌ한 고 : " + client);
				client.setComId(comid);
			}
			result += cd.insertClient(sqlSession, client);
			return  cd.updateLead(sqlSession, client);
		} else {
			result += cd.updateLead(sqlSession, client);
			return cd.insertClient(sqlSession, client);
		}
		
	}

	@Override
	public Lead selectOne(int num) {
		return cd.selectOne(sqlSession, num);
	}

	@Override
	public List<Client> clientList(int empNo) {
		return cd.selectList(sqlSession, empNo);
	}
	
	@Override
	public List<Client> selectClient() {
		
		List<Client> list = cd.selectComapny(sqlSession);
		
		return list;
	}
	
	@Override
	public List<Client> selectClientToCompany(int companyId) {
		
		List<Client> list = cd.selectCompanyToClient(sqlSession, companyId);
		
		return list;
	}

	

}
