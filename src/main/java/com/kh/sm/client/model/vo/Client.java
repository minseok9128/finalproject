package com.kh.sm.client.model.vo;

import java.sql.Date;
import java.util.ArrayList;

import com.kh.sm.contract.model.vo.Attachment;

public class Client implements java.io.Serializable {
	private int clientId;		//고객ID
	private String clientName;	//고객명
	private String department;	//부서
	private String job;			//직책
	private String phone;		//휴대번
	private String tel;			//유선번호
	private String email;		//메일
	private String rank;		//고객등급
	private String keyMan;		//KeyMan여부
	private Date birthday;		//생년월일
	private String religion;	//종교
	private int category;		//구분
	private Date enrollDate;	//등록일
	private Date modifyDate;	//변경일
	private int empNo;			//담당자
	private int comId;			//고객사
	private String exception;	//애러
	private String comName;     //고객사명
	private String empName;
	private int leadId;			//잠재고객id
	private ArrayList<Attachment> attList;
	
	public Client() {}

	public Client(int clientId, String clientName, String department, String job, String phone, String tel,
			String email, String rank, String keyMan, Date birthday, String religion, int category, Date enrollDate,
			Date modifyDate, int empNo, int comId, String exception, String comName, String empName, int leadId,
			ArrayList<Attachment> attList) {
		super();
		this.clientId = clientId;
		this.clientName = clientName;
		this.department = department;
		this.job = job;
		this.phone = phone;
		this.tel = tel;
		this.email = email;
		this.rank = rank;
		this.keyMan = keyMan;
		this.birthday = birthday;
		this.religion = religion;
		this.category = category;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.empNo = empNo;
		this.comId = comId;
		this.exception = exception;
		this.comName = comName;
		this.empName = empName;
		this.leadId = leadId;
		this.attList = attList;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getKeyMan() {
		return keyMan;
	}

	public void setKeyMan(String keyMan) {
		this.keyMan = keyMan;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public int getComId() {
		return comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getLeadId() {
		return leadId;
	}

	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	public ArrayList<Attachment> getAttList() {
		return attList;
	}

	public void setAttList(ArrayList<Attachment> attList) {
		this.attList = attList;
	}

	@Override
	public String toString() {
		return "Client [clientId=" + clientId + ", clientName=" + clientName + ", department=" + department + ", job="
				+ job + ", phone=" + phone + ", tel=" + tel + ", email=" + email + ", rank=" + rank + ", keyMan="
				+ keyMan + ", birthday=" + birthday + ", religion=" + religion + ", category=" + category
				+ ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate + ", empNo=" + empNo + ", comId=" + comId
				+ ", exception=" + exception + ", comName=" + comName + ", empName=" + empName + ", leadId=" + leadId
				+ ", attList=" + attList + "]";
	}

	
}
