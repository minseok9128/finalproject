package com.kh.sm.common;

import java.sql.Date;

public class Attachment {
	private int fileId;
	private String originName;
	private String changeName;
	private String filePath;
	private Date enrollDate;
	private String delYn;
	private int category;
	private int categoryId;
	
	public Attachment() {}

	public Attachment(int fileId, String originName, String changeName, String filePath, Date enrollDate, String delYn,
			int category, int categoryId) {
		super();
		this.fileId = fileId;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.enrollDate = enrollDate;
		this.delYn = delYn;
		this.category = category;
		this.categoryId = categoryId;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "Attachment [fileId=" + fileId + ", originName=" + originName + ", changeName=" + changeName
				+ ", filePath=" + filePath + ", enrollDate=" + enrollDate + ", delYn=" + delYn + ", category="
				+ category + ", categoryId=" + categoryId + "]";
	}
	
	
	
}
