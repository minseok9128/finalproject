package com.kh.sm.common;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class FileDownload implements ApplicationContextAware {

	private WebApplicationContext context = null;
	
	
	@RequestMapping("download.do")
	public ModelAndView download(@RequestParam("fileName")String fileName,
			@RequestParam("filePath")String filePath,@RequestParam("originName")String originName) {
		
		String path = filePath;
		String fullPath = path + "\\" + fileName;
		
		File file = new File(fullPath);
		
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(fullPath);
		list.add(originName);
		
		return new ModelAndView("download","downloadFile", list);
	}
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		// TODO Auto-generated method stub
		
		this.context = (WebApplicationContext)arg0;
	}

}
