package com.kh.sm.common;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

@Repository
public class SaveFile {
	
	public void saveFile(MultipartFile file, HttpServletRequest request, Attachment ac) {

		UUID uuid = UUID.randomUUID();
		String saveName = uuid +"_"+file.getOriginalFilename();

		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root+"\\uploadFiles\\files";

		String originFileName = file.getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();


		try {
			file.transferTo(new File(filePath+"\\"+changeName + ext));

		} catch (IllegalStateException | IOException e) {
			//실패시 올린 파일 삭제
			new File(filePath + "\\" + changeName + ext).delete();
		}
		
		ac.setOriginName(originFileName);
		ac.setChangeName(changeName);
		ac.setFilePath(filePath);
		
	}
}
