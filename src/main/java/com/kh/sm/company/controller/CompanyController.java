package com.kh.sm.company.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.company.model.exception.SelectException;
import com.kh.sm.company.model.service.CompanyService;
import com.kh.sm.company.model.vo.Company;


@Controller
public class CompanyController {
	
	@Autowired
	private final CompanyService cs;
	
	public CompanyController(CompanyService cs) {
		this.cs = cs;
	}
	
	@PostMapping("selectCompany.co")
	public void selectCompany(HttpServletResponse response) {
		List<Company> result = null;
		try {
			result = cs.selectCompany();
		} catch (SelectException e1) {
			Company co = new Company();
			co.setException(e1.getMessage());
			result.add(co);
		}
		try {
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(result, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@GetMapping("goInsert.co")
	public String goInsert() {
		return "sales/clientManagement/company/insertCompany";
	}
	
//	@PostMapping("insert.co")
//	public String insertCompany(Company company) {
//		System.out.println("com :" + company);
//		
//		int result = cs.insertCompany(company);
//		
//		return "redirect:7.sa";
//	}
	
//	@PostMapping("insertOpportunityEnrollment.so")
//	public String insertOpportunityEnrollment(Company company,
//	Model model, MultipartHttpServletRequest multiRequest, HttpServletRequest request){
//	    String root = request.getSession().getServletContext().getRealPath("resources");
//	    String filePath = root + "//uploadFiles//files";
//	    List<MultipartFile> fList = multiRequest.getFiles("files");
//
//	    ArrayList<String> changeNames = new ArrayList<>();
//
//	    for(int i = 0; i < fList.size(); i++){
//	        System.out.println("fListName : " + fList.get(i).getOriginalFilename());
//	        String originFileName = fList.get(i).getOriginalFilename();
//	        String ext = originFileName.substring(originFileName.lastIndexOf("."));
//	        String changeName = CommonUtils.getRandomString();
//	        System.out.println("changeName : " + changeName);
//	        try{
//	            fList.get(i).transferTo(new File(filePath + "//" + changeName + ext));
//
//	        }catch(IllegalStateException | IOException e){
//	            e.printStackTrace();
//	        }
//	        changeNames.add(changeName);
//	    }
//	    int result = 0;
//
//	    result = cs.insertCompany(company);
//
//	    if(result > 0){
//	        int result2 = cs.insertAttach(fList, changeNames, filePath);
//	    }
//		return filePath;
//
//
//	}
	
	@GetMapping("selList.co")
	public String selListCompany(Model model) {
	System.out.println("hello");
		List<Company> list = cs.selListCompany();
		System.out.println("hello2");
		model.addAttribute("companyList", list);
		System.out.println("list");
		
		return "sales/clientManagement/company/companyList";
	}
	
	@GetMapping("selOne.co")
	public String selOneCompany(Model model, Company company) {
		
		Company companyOne = cs.selOneCompany(company);
		System.out.println("companyOne : " + companyOne);
		model.addAttribute("company", companyOne);
		
		
		return "sales/clientManagement/company/companyDetail";
	}
	
	
}












