package com.kh.sm.company.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.company.model.vo.Company;
import com.kh.sm.contract.model.vo.Attachment;

public interface CompanyDao {

	List<Company> selectCompany(SqlSessionTemplate sqlSession);

	int insertCompany(Company company, SqlSessionTemplate sqlSession);

	List<Company> selListCompany(SqlSessionTemplate sqlSession);

	Company selOneCompany(SqlSessionTemplate sqlSession, Company company);

	int insertAttach(SqlSessionTemplate sqlSession, Attachment attc);
	
	
}
