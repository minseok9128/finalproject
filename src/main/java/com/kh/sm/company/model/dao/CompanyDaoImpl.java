package com.kh.sm.company.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.company.model.vo.Company;
import com.kh.sm.contract.model.vo.Attachment;

@Repository
public class CompanyDaoImpl implements CompanyDao{

	//고객사 모달에서 고객사 정보 불러오는 메소드
	@Override
	public List<Company> selectCompany(SqlSessionTemplate sqlSession) {

		List<Company> result = sqlSession.selectList("Company.selectAll");
		
		return result;
	}

	@Override
	public int insertCompany(Company company, SqlSessionTemplate sqlSession) {

		return sqlSession.insert("Company.insert", company);
	}

	@Override
	public List<Company> selListCompany(SqlSessionTemplate sqlSession) {
		System.out.println("hello5");
		return sqlSession.selectList("Company.selectList");
	}

	@Override
	public Company selOneCompany(SqlSessionTemplate sqlSession, Company company) {

		return sqlSession.selectOne("Company.selectOne", company.getComId());
	}

	@Override
	public int insertAttach(SqlSessionTemplate sqlSession, Attachment attc) {

		int result = 0;
		
		result = sqlSession.insert("Company.insertFile", attc);
		
		return result;
	}
}
