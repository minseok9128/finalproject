package com.kh.sm.company.model.exception;

public class SelectException extends Exception {
	
	public SelectException(String msg) {
		super(msg);
	}
}
