package com.kh.sm.company.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.company.model.exception.SelectException;
import com.kh.sm.company.model.vo.Company;

public interface CompanyService {

	List<Company> selectCompany() throws SelectException;

	int insertCompany(Company company);

	List<Company> selListCompany();

	Company selOneCompany(Company company);

	int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath);



}
