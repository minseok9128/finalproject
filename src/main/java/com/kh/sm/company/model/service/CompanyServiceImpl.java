package com.kh.sm.company.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.company.model.dao.CompanyDao;
import com.kh.sm.company.model.exception.SelectException;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.contract.model.vo.Attachment;

@Service
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private final CompanyDao cd;
	
	public CompanyServiceImpl(CompanyDao cd) {
		this.cd = cd;
	}
	
	@Override
	public List<Company> selectCompany() throws SelectException {
		
		List<Company> result = cd.selectCompany(sqlSession);
		
		if(result == null) {
			throw new SelectException("조회실패");
		}
		
		return result;
	}

	@Override
	public int insertCompany(Company company) {

		return cd.insertCompany(company, sqlSession);
	}

	@Override
	public List<Company> selListCompany() {
		System.out.println("hello4");
		return cd.selListCompany(sqlSession);
	}

	@Override
	public Company selOneCompany(Company company) {

		return cd.selOneCompany(sqlSession, company);
	}

	@Override
	public int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath) {
		int result = 0;
		
		Attachment attc = null;
		for(int i = 0; i < fList.size(); i++) {
			
			attc = new Attachment();
			attc.setCategory(3);
			attc.setChangeName(changeNames.get(i));
			attc.setFilePath(filePath);
			attc.setOriginName(fList.get(i).getOriginalFilename());
			attc.setDelYn("N");
			attc.setFileId(1);
			result += cd.insertAttach(sqlSession, attc);
		}
		
		return result;
	}


	


}
