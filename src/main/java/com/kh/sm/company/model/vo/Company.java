package com.kh.sm.company.model.vo;

import java.sql.Date;

public class Company implements java.io.Serializable {
	private int comId; 			//고객사 ID
	private String comName; 	//고객사명
	private int category; 		//구분
	private String comRating; 	//고객사등급
	private int revenueYear;	//매출(년)
	private int empNum;			//사원수
	private int bizLisence;		//사업자번호
	private String tel;			//유선번호
	private String fax;			//팩스번호
	private String webSite;		//웹사이트
	private String address;		//주소
	private String status;		//삭제여부
	private Date enrollDate;	//등록일
	private Date modifyDate;	//변경일
	private String agentName;	//대표자명
	private String exception;	//애러

	public Company() {}

	public Company(int comId, String comName, int category, String comRating, int revenueYear, int empNum,
			int bizLisence, String tel, String fax, String webSite, String address, String status, Date enrollDate,
			Date modifyDate, String agentName, String exception) {
		super();
		this.comId = comId;
		this.comName = comName;
		this.category = category;
		this.comRating = comRating;
		this.revenueYear = revenueYear;
		this.empNum = empNum;
		this.bizLisence = bizLisence;
		this.tel = tel;
		this.fax = fax;
		this.webSite = webSite;
		this.address = address;
		this.status = status;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.agentName = agentName;
		this.exception = exception;
	}

	public int getComId() {
		return comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getComRating() {
		return comRating;
	}

	public void setComRating(String comRating) {
		this.comRating = comRating;
	}

	public int getRevenueYear() {
		return revenueYear;
	}

	public void setRevenueYear(int revenueYear) {
		this.revenueYear = revenueYear;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	public int getBizLisence() {
		return bizLisence;
	}

	public void setBizLisence(int bizLisence) {
		this.bizLisence = bizLisence;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "Company [comId=" + comId + ", comName=" + comName + ", category=" + category + ", comRating="
				+ comRating + ", revenueYear=" + revenueYear + ", empNum=" + empNum + ", bizLisence=" + bizLisence
				+ ", tel=" + tel + ", fax=" + fax + ", webSite=" + webSite + ", address=" + address + ", status="
				+ status + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate + ", agentName=" + agentName
				+ ", exception=" + exception + "]";
	}

	
	
	
	
}
