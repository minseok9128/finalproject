package com.kh.sm.contract.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.contract.model.service.ContractService;
import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.contract.model.vo.ContProduct;
import com.kh.sm.contract.model.vo.Contract;
import com.kh.sm.employee.model.vo.Employee;

@Controller
public class ContractController {
	
	@Autowired
	private ContractService cs;
	
	@GetMapping("selList.con")
	public String selListContract(Model model, HttpServletRequest request) {
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		
		List<Contract> list = cs.selList(empNo);
		System.out.println("list :" + list);
		model.addAttribute("list" , list);
		
		return "sales/salesManagement/contractMain";
				
	}
	
	@PostMapping("selListModal.con")
	public void searchProduct(Contract con, HttpServletResponse response) {
//		
		List<Contract> list = cs.selListModal(con);
		System.out.println("list :" + list);
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@GetMapping("selOne.con")
	public String selOneContract(Model model, int contId) {
		System.out.println("contId : " + contId);
		
		Contract con = cs.selOne(contId);
		System.out.println("con :" + con);
		
		model.addAttribute("con" , con);
		
		return "sales/salesManagement/contractDetail";
				
	}
	
	
	@PostMapping("insert.con")
	public String insertContract(Contract con, Model model
			,HttpServletRequest request ) {
		//맥일 경우 와 윈도우일 경우 경로 변경 
		String osName = System.getProperty("os.name").toLowerCase();
		System.out.println("osname ; " + osName);
		String slash = "";
		if(osName.equals("mac os x")) {
			slash = "/";
		} else {
			slash = "\\";
		}
		//disabled 시 값 널로 들어오는거 처리
		if(con.getExpectAlarm() == null) {
			con.setExpectAlarm("NONE");
		}
		
		if(con.getUpdateAlarm() == null) {
			con.setUpdateAlarm("NONE");
		}
		MultipartFile multipartFile = null;//파일을 저장함 
		String originalFileName = null;//이름 담기 위해
		//경로
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		System.out.println("con ; " + con);
		//계약제품 넣는 부분 시작
		//계약제품 정보 가져오기
		String[] id = request.getParameterValues("prId");
		String[] amount = request.getParameterValues("amount");
		String[] rP = request.getParameterValues("revenuePeriod");
		String[] rD = request.getParameterValues("revDate");
		String[] rM = request.getParameterValues("remark");
		//계약제품 리스트 생성
		ArrayList<ContProduct> pList = new ArrayList<ContProduct>();
		//담기
		if(id != null) {
			 System.out.println(id.length);
			for(int i = 0; i < id.length; i ++) {
				ContProduct cp = new ContProduct();
				cp.setPdtId(Integer.parseInt(id[i]));
				cp.setRevenuePeriod(rP[i]);
				cp.setRevenueDate(Date.valueOf(rD[i]));
				cp.setContProductRemark(rM[i]);
				cp.setAmount(Integer.parseInt(amount[i]));
				pList.add(cp);
			}
		}
		//계약객체에 담기
		con.setPrList(pList);
		//계약제품 넣는 부분 끝
		//업로드 경로
		String filePath = root + slash +"uploadFiles" + slash +  "files";
		//객체에 담을 리스트
		List<Attachment> fList = null;
		//멀티파트로 바꾸는거 사실 메소드에 써도 되느느듯
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		//반복해서 돌리려고
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		//다중 파일을 저장할 List생성
		fList = new ArrayList<Attachment>();
		
		
		//여기서 업로드도 하고 삭제도 하고 리스트에 담음
		//조건문으로 파일이 있을때랑 없을 때 걸르기
		while(iterator.hasNext()){
			Attachment at = new Attachment();
			multipartFile = multipartHttpServletRequest.getFile(iterator.next());
			originalFileName = multipartFile.getOriginalFilename();
			String ext = originalFileName.substring(originalFileName.lastIndexOf("."));	//확장자 이전부터 잘라서 ext라는 확장자로 저장
			String changeName = CommonUtils.getRandomString();	//32비트의 랜덤한 문자열 생성
			at.setOriginName(originalFileName);
			at.setChangeName(changeName);
			at.setFilePath(filePath);
			try {
				multipartFile.transferTo(new File(filePath + slash + changeName + ext));
				
			} catch (Exception e) {
				System.out.println("여기는 니나노");
				for(Attachment list : fList) {
					new File(filePath + slash + list.getChangeName() + ext).delete();
				}
				model.addAttribute("msg", "회원가입 실패");
				return "commons/errorPage";
				
			}

			fList.add(at);

		}
		//파일 처리하는 객체를 계약에 담음
		con.setAttList((ArrayList<Attachment>) fList);
		for(Attachment list : fList) {
			System.out.println(list);
		}
		System.out.println("con : " + con);
		//로직처리
		int result = cs.insertCont(con);
	
		
		return "redirect:/selList.con";
	}

}
