package com.kh.sm.contract.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.contract.model.vo.ContProduct;
import com.kh.sm.contract.model.vo.Contract;

public interface ContractDao {

	int insertCont(SqlSessionTemplate sqlSession, Contract con);

	List<Contract> selList(SqlSessionTemplate sqlSession, int empNo);

	Contract selOne(SqlSessionTemplate sqlSession, int contId);

	List<Attachment> selattList(SqlSessionTemplate sqlSession, int contId);

	List<ContProduct> selprList(SqlSessionTemplate sqlSession, int contId);

	List<Contract> selListModal(SqlSessionTemplate sqlSession, Contract con);



}
