package com.kh.sm.contract.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.contract.model.vo.Attachment;
import com.kh.sm.contract.model.vo.ContProduct;
import com.kh.sm.contract.model.vo.Contract;
import com.kh.sm.process.model.vo.OppProcess;

@Repository
public class ContractDaoImpl implements ContractDao {

	@Override
	public int insertCont(SqlSessionTemplate sqlSession, Contract con) {
		//계약 
		int result = sqlSession.insert("Contract.insertCon", con);
		//첨부파일 리스
		for(Attachment list : con.getAttList()) {
			sqlSession.insert("Contract.insertFile", list);
		}
		//계약제품
		for(ContProduct list2 : con.getPrList()) {
			sqlSession.insert("Contract.insertProd", list2);
		}
		//프로세스 업데이트
		if(result > 0 ) {
			int oppNo = con.getOppId();
			//progress update 계약 시 종료(성공)으로 바꿈
			sqlSession.update("Contract.updateProgress",oppNo);
			//Process
			OppProcess process = new OppProcess();
			process.setProcessId(oppNo);
			sqlSession.update("Contract.updateOpp", process);
		}
		
		return result;
	}

	@Override
	public List<Contract> selList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Contract.selList", empNo);
	}

	@Override
	public Contract selOne(SqlSessionTemplate sqlSession, int contId) {
		Contract con = new Contract();
		con = sqlSession.selectOne("Contract.selOne",contId);
		return con;
	}

	@Override
	public List<Attachment> selattList(SqlSessionTemplate sqlSession, int contId) {
		System.out.println("durl dao: " + contId);
		int categoryId = contId;
		return sqlSession.selectList("Contract.listAtt", categoryId);
	}

	@Override
	public List<ContProduct> selprList(SqlSessionTemplate sqlSession, int contId) {
		System.out.println("contId ; " + contId);
		System.out.println("dao : " + sqlSession.selectList("Contract.listProd", contId));
		return sqlSession.selectList("Contract.listProd", contId);
	}

	@Override
	public List<Contract> selListModal(SqlSessionTemplate sqlSession, Contract con) {
		return sqlSession.selectList("Contract.listModal", con);
	}

	

}
