package com.kh.sm.contract.model.service;

import java.util.ArrayList;
import java.util.List;

import com.kh.sm.contract.model.vo.Contract;
import com.kh.sm.estimate.model.vo.Estimate;

public interface ContractService {

	int insertCont(Contract con);

	List<Contract> selList(int empNo);

	Contract selOne(int contId);

	List<Contract> selListModal(Contract con);


}
