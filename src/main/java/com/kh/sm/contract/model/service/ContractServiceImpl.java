package com.kh.sm.contract.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.contract.model.dao.ContractDao;
import com.kh.sm.contract.model.vo.ContProduct;
import com.kh.sm.contract.model.vo.Contract;

@Service
public class ContractServiceImpl implements ContractService {
	
	@Autowired
	private ContractDao cd;
	
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public int insertCont(Contract con) {
		return cd.insertCont(sqlSession, con);
	}

	@Override
	public List<Contract> selList(int empNo) {
		return cd.selList(sqlSession, empNo);
	}

	@Override
	public Contract selOne(int contId) {
		  Contract con = new Contract();
	      con = cd.selOne(sqlSession, contId);
//	      System.out.println("attLst : " + (cd.selattList(sqlSession, contId)));
	      List<com.kh.sm.contract.model.vo.Attachment> list1 = cd.selattList(sqlSession, contId);
	      List<ContProduct> list2 = cd.selprList(sqlSession, contId);
	      System.out.println("attList : " + list1);
	      System.out.println("prList : " + list2);
	      con.setPrList((ArrayList<ContProduct>)list2);
	      con.setAttList((ArrayList<com.kh.sm.contract.model.vo.Attachment>)list1);
	      System.out.println("service :" + con );
		
		return con;
	}

	@Override
	public List<Contract> selListModal(Contract con) {
		return cd.selListModal(sqlSession, con);
	}

	
}
