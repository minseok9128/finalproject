package com.kh.sm.contract.model.vo;

import java.sql.Date;

public class ContProduct implements java.io.Serializable {
	
	private int contProductId;
	private String pdtName;
	private int price;
	private String revenuePeriod;
	private Date revenueDate;
	private String contProductRemark;
	private int contId;
	private int amount;
	private int pdtId;
	
	public ContProduct () {}

	public ContProduct(int contProductId, String pdtName, int price, String revenuePeriod, Date revenueDate,
			String contProductRemark, int contId, int amount, int pdtId) {
		super();
		this.contProductId = contProductId;
		this.pdtName = pdtName;
		this.price = price;
		this.revenuePeriod = revenuePeriod;
		this.revenueDate = revenueDate;
		this.contProductRemark = contProductRemark;
		this.contId = contId;
		this.amount = amount;
		this.pdtId = pdtId;
	}

	public int getContProductId() {
		return contProductId;
	}

	public void setContProductId(int contProductId) {
		this.contProductId = contProductId;
	}

	public String getPdtName() {
		return pdtName;
	}

	public void setPdtName(String pdtName) {
		this.pdtName = pdtName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getRevenuePeriod() {
		return revenuePeriod;
	}

	public void setRevenuePeriod(String revenuePeriod) {
		this.revenuePeriod = revenuePeriod;
	}

	public Date getRevenueDate() {
		return revenueDate;
	}

	public void setRevenueDate(Date revenueDate) {
		this.revenueDate = revenueDate;
	}

	public String getContProductRemark() {
		return contProductRemark;
	}

	public void setContProductRemark(String contProductRemark) {
		this.contProductRemark = contProductRemark;
	}

	public int getContId() {
		return contId;
	}

	public void setContId(int contId) {
		this.contId = contId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getPdtId() {
		return pdtId;
	}

	public void setPdtId(int pdtId) {
		this.pdtId = pdtId;
	}

	@Override
	public String toString() {
		return "ContProduct [contProductId=" + contProductId + ", pdtName=" + pdtName + ", price=" + price
				+ ", revenuePeriod=" + revenuePeriod + ", revenueDate=" + revenueDate + ", contProductRemark="
				+ contProductRemark + ", contId=" + contId + ", amount=" + amount + ", pdtId=" + pdtId + "]";
	}

		
}
