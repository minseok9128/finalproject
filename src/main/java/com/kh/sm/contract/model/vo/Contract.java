package com.kh.sm.contract.model.vo;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.sm.product.model.vo.Product;

public class Contract implements java.io.Serializable {
	private int contId;
	private String contName;
	private int comId;
	private String comName;
	private int clientId;
	private String clientName;
	private int oppId;
	private String oppName;
	private int estId;
	private String estName;
	private Date contDate;
	private Date contStart;
	private Date contEnd;
	private int contPrice;
	private int contVat;
	private int warrantyPeriod;
	private Date expectDate;
	private String expectAlarm;
	private Date updateDate;
	private String updateAlarm;
	private int empNo;
	private String empName;
	private String contRemark;
	private String bizRegYn;
	private String conStatus;
	private ArrayList<ContProduct> prList;
	private ArrayList<HashMap<String,Object>> prListH;
	private ArrayList<Attachment> attList;
	
	public Contract() {}

	public Contract(int contId, String contName, int comId, String comName, int clientId, String clientName, int oppId,
			String oppName, int estId, String estName, Date contDate, Date contStart, Date contEnd, int contPrice,
			int contVat, int warrantyPeriod, Date expectDate, String expectAlarm, Date updateDate, String updateAlarm,
			int empNo, String empName, String contRemark, String bizRegYn, String conStatus,
			ArrayList<ContProduct> prList, ArrayList<HashMap<String, Object>> prListH, ArrayList<Attachment> attList) {
		super();
		this.contId = contId;
		this.contName = contName;
		this.comId = comId;
		this.comName = comName;
		this.clientId = clientId;
		this.clientName = clientName;
		this.oppId = oppId;
		this.oppName = oppName;
		this.estId = estId;
		this.estName = estName;
		this.contDate = contDate;
		this.contStart = contStart;
		this.contEnd = contEnd;
		this.contPrice = contPrice;
		this.contVat = contVat;
		this.warrantyPeriod = warrantyPeriod;
		this.expectDate = expectDate;
		this.expectAlarm = expectAlarm;
		this.updateDate = updateDate;
		this.updateAlarm = updateAlarm;
		this.empNo = empNo;
		this.empName = empName;
		this.contRemark = contRemark;
		this.bizRegYn = bizRegYn;
		this.conStatus = conStatus;
		this.prList = prList;
		this.prListH = prListH;
		this.attList = attList;
	}

	public int getContId() {
		return contId;
	}

	public void setContId(int contId) {
		this.contId = contId;
	}

	public String getContName() {
		return contName;
	}

	public void setContName(String contName) {
		this.contName = contName;
	}

	public int getComId() {
		return comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public String getOppName() {
		return oppName;
	}

	public void setOppName(String oppName) {
		this.oppName = oppName;
	}

	public int getEstId() {
		return estId;
	}

	public void setEstId(int estId) {
		this.estId = estId;
	}

	public String getEstName() {
		return estName;
	}

	public void setEstName(String estName) {
		this.estName = estName;
	}

	public Date getContDate() {
		return contDate;
	}

	public void setContDate(Date contDate) {
		this.contDate = contDate;
	}

	public Date getContStart() {
		return contStart;
	}

	public void setContStart(Date contStart) {
		this.contStart = contStart;
	}

	public Date getContEnd() {
		return contEnd;
	}

	public void setContEnd(Date contEnd) {
		this.contEnd = contEnd;
	}

	public int getContPrice() {
		return contPrice;
	}

	public void setContPrice(int contPrice) {
		this.contPrice = contPrice;
	}

	public int getContVat() {
		return contVat;
	}

	public void setContVat(int contVat) {
		this.contVat = contVat;
	}

	public int getWarrantyPeriod() {
		return warrantyPeriod;
	}

	public void setWarrantyPeriod(int warrantyPeriod) {
		this.warrantyPeriod = warrantyPeriod;
	}

	public Date getExpectDate() {
		return expectDate;
	}

	public void setExpectDate(Date expectDate) {
		this.expectDate = expectDate;
	}

	public String getExpectAlarm() {
		return expectAlarm;
	}

	public void setExpectAlarm(String expectAlarm) {
		this.expectAlarm = expectAlarm;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateAlarm() {
		return updateAlarm;
	}

	public void setUpdateAlarm(String updateAlarm) {
		this.updateAlarm = updateAlarm;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getContRemark() {
		return contRemark;
	}

	public void setContRemark(String contRemark) {
		this.contRemark = contRemark;
	}

	public String getBizRegYn() {
		return bizRegYn;
	}

	public void setBizRegYn(String bizRegYn) {
		this.bizRegYn = bizRegYn;
	}

	public String getConStatus() {
		return conStatus;
	}

	public void setConStatus(String conStatus) {
		this.conStatus = conStatus;
	}

	public ArrayList<ContProduct> getPrList() {
		return prList;
	}

	public void setPrList(ArrayList<ContProduct> prList) {
		this.prList = prList;
	}

	public ArrayList<HashMap<String, Object>> getPrListH() {
		return prListH;
	}

	public void setPrListH(ArrayList<HashMap<String, Object>> prListH) {
		this.prListH = prListH;
	}

	public ArrayList<Attachment> getAttList() {
		return attList;
	}

	public void setAttList(ArrayList<Attachment> attList) {
		this.attList = attList;
	}

	@Override
	public String toString() {
		return "Contract [contId=" + contId + ", contName=" + contName + ", comId=" + comId + ", comName=" + comName
				+ ", clientId=" + clientId + ", clientName=" + clientName + ", oppId=" + oppId + ", oppName=" + oppName
				+ ", estId=" + estId + ", estName=" + estName + ", contDate=" + contDate + ", contStart=" + contStart
				+ ", contEnd=" + contEnd + ", contPrice=" + contPrice + ", contVat=" + contVat + ", warrantyPeriod="
				+ warrantyPeriod + ", expectDate=" + expectDate + ", expectAlarm=" + expectAlarm + ", updateDate="
				+ updateDate + ", updateAlarm=" + updateAlarm + ", empNo=" + empNo + ", empName=" + empName
				+ ", contRemark=" + contRemark + ", bizRegYn=" + bizRegYn + ", conStatus=" + conStatus + ", prList="
				+ prList + ", prListH=" + prListH + ", attList=" + attList + "]";
	}
	
	
}
