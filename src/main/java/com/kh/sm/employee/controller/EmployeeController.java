package com.kh.sm.employee.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.exception.LoginException;
import com.kh.sm.employee.model.service.EmployeeService;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeHistory;

@SessionAttributes("loginEmp")
@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService es;

	// 생성자를 통한 주입
	public EmployeeController(EmployeeService es) {
		this.es = es;
	}

	// 로그인 메소드
	@RequestMapping("login.em")
	public String loginCheck(Employee emp, Model model,HttpServletRequest request) {
		Employee loginEmp;
		HttpSession session = request.getSession();
		try {
			loginEmp = es.loginEmployee(emp);
			model.addAttribute("loginEmp", loginEmp);
			//session.setAttribute("loginEmp", loginEmp);
			System.out.println("employee : " + loginEmp);
			
		
			return "redirect:noticeMain.em";
			
			
		} catch (LoginException e) {
			model.addAttribute("msg", "로그인실패!!");

			return "common/errorPage";
		}

	}

	// 로그아웃 메소드
	@RequestMapping("logout.em")
	public String logout(SessionStatus status) {
		status.setComplete();

		return "redirect:index.jsp";
	}

	// 회원 insert
	@PostMapping("insert.em")
	public String insertEmployee(Model model, Employee emp) {

		// 암호화 작업 제외하고 insert 먼저 해보기!

		if (emp.getEmpType() == 2) {
			emp.setEmpType(2);
		} else if (emp.getEmpType() == 3) {
			emp.setEmpType(3);
		}
		
		EmployeeHistory empHis = new EmployeeHistory();
		

		int result = es.insertEmployee(emp);
		int result2 = 0;
		
		
		if (result > 0) {
			
			result2 = es.insertHisEmployee(empHis);
			
			if(result2 > 0) {
				
				model.addAttribute("msg","직원등록이 완료되었습니다.");
				model.addAttribute("aa",1);
				return "admin/employee/empSuccesspage";
			}else {
				model.addAttribute("msg", "회원이력 남기기 실패!");
				return "common/errorPage";
			}
			

		} else {
			model.addAttribute("msg", "회원가입 실패!");
			return "common/errorPage";

		}

	}
	
	

	
	
	//관리자 직원정보 수정
	@PostMapping("update.em")
	public String updateEmployee(Model model, Employee emp) {
		
		if (emp.getEmpType() == 2) {
			emp.setEmpType(2);
		} else if (emp.getEmpType()== 3) {
			emp.setEmpType(3);
		}
		
	
		 
		EmployeeHistory empHis = new EmployeeHistory();
		
		
		int result = es.updateEmployee(emp);
		empHis.setEmpNo(emp.getEmpNo());
		
		int result2 = 0;
		
		if (result > 0) {
			
			result2 = es.updateHisEmployee(empHis);
			
			if(result2>0) {
				
				model.addAttribute("updateMsg","직원수정이 완료되었습니다.");
				model.addAttribute("bb",1);
				return "admin/employee/empSuccesspage";
				
			}else {
				model.addAttribute("msg", "회원정보 수정 이력실패!");
				return "common/errorPage";
				
			}
			

		} else {
			model.addAttribute("msg", "회원정보 수정실패!");
			return "common/errorPage";

		}

	}
	
	@PostMapping("delete.em")
	public String deleteEmployee(Model model , Employee emp ) {
		
		
		
		int result = es.deleteEmployee(emp);
		int result2 = 0;
		
		EmployeeHistory empHis = new EmployeeHistory();
		empHis.setEmpNo(emp.getEmpNo());
		
		  if (result > 0) { 
			  result2 = es.deleteHisEmployee(empHis);
			  
			  if(result2 >0) {
				  
				  model.addAttribute("deleteMsg","직원삭제가 완료되었습니다.");
					model.addAttribute("cc",1);
					return "admin/employee/empSuccesspage";
				  
			  }else {
				  model.addAttribute("msg", "회원삭제 이력 실패!"); 
				  return "common/errorPage";
			  }
			 
		  }else { 
			  model.addAttribute("msg", "회원삭제 실패!"); 
			  return "common/errorPage";
		  }
		
	}
	@ResponseBody
	 @RequestMapping("insertExcel.em")
	public ModelAndView insertExcel(ModelAndView mv,MultipartHttpServletRequest request) {
		 MultipartFile excelFile =request.getFile("excelFile");
		 System.out.println("excelFile" + excelFile);
		
		return mv;
	}




	
	

	// 담당자 조회 모달에서 가져가는 메소드
	@PostMapping("selectEmployeeModal.em")
	public void selectEmployeeModal(HttpServletResponse response) {
		List<Employee> list = null;

		list = es.selectEmpAll();

		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@GetMapping("showAdminEmpExcelEnroll.em")
	public String selectShowAdminEmpExcelEnroll(HttpServletRequest request) {
		
		Attachment att = es.selectAtt();
		
		request.setAttribute("att", att);
		System.out.println("찾아온 att : " + att );
		
		return "admin/employee/empExcelEnroll";
	}
	
	@GetMapping("noticeMain.em")
	public ModelAndView noticeMain(ModelAndView mv, HttpServletRequest request) {
		System.out.println("메인컨트롤러");
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		String code = request.getParameter("code");
		
		List<HashMap<String,Object>> noticeMain = es.selectNoticeList();
		List<HashMap<String,Object>> activityList = es.selectActivityList(empNo);
		List<HashMap<String,Object>> oppList = es.selectOpportunityList(empNo);
		List<HashMap<String,Object>> revRank = es.selectRevRankList();
		List<HashMap<String,Object>> pdtTargetList = es.selectPdtTargetList(empNo);
		HashMap<String,Object> myRank = es.selectMyRank(empNo);

		HashMap<String,Object> adminInfo = es.selectAdminInfo();
		
		HttpSession session = request.getSession();
		
		session.setAttribute("adminInfo", adminInfo);
		session.setAttribute("pdtList", pdtTargetList);
		session.setAttribute("myRank", myRank);
		session.setAttribute("revRank", revRank);
		session.setAttribute("oppList", oppList);
		session.setAttribute("noticeList", noticeMain);
		session.setAttribute("activityList", activityList);
		
		if(code != null && code.equals("1")) {
			mv.setViewName("main/salesMain");
		}else if(code != null && code.equals("2")){
			mv.setViewName("main/adminMain");
		}else {
			mv.setViewName("redirect:index.jsp");						
		}
		
		return mv;
	}

}
