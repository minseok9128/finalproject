package com.kh.sm.employee.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.kh.sm.employee.model.service.EmployeeService;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeAjax;
import com.kh.sm.employee.model.vo.EmployeeHistory;

@Controller
public class EmployeeController2 {

	@Autowired
	private EmployeeService es;

	// 생성자를 통한 주입
	public EmployeeController2(EmployeeService es) {
		this.es = es;
	}

	@PostMapping("searchEmpName.em")
	public ModelAndView searchEmpName(String empName, ModelAndView mv) {
//		Employee emp = new Employee();
//		emp.setEmpName(request.getParameter("empName"));
		
//		System.out.println("emp : " + emp);

		List<Employee> list = es.searchEmpName(empName);

		mv.addObject("list", list);
		
		
		System.out.println("list : "+ list );
		mv.setViewName("jsonView");

		return mv;

	}
	
	// 관리자 직원관리 해당 직원클릭하면 정보 select
	@PostMapping("searchEmp.em")
	public ModelAndView SearchEmp(HttpServletRequest request, ModelAndView mv) {

		EmployeeAjax emp = new EmployeeAjax();
		emp.setEmpNo(Integer.parseInt(request.getParameter("empNo")));

		EmployeeAjax searchEmp = es.searchEmp(emp);

		

		mv.addObject(searchEmp);
		mv.setViewName("jsonView");

	
		return mv;
	}
	

	@PostMapping("selectEmpHis.em")
	public ModelAndView SelectEmpHis(HttpServletRequest request, ModelAndView mv) {
		
		EmployeeHistory empHis = new EmployeeHistory();
		empHis.setEmpNo(Integer.parseInt(request.getParameter("empNo")));
		
		List<EmployeeHistory> SearchEmpHis = es.selectEmpHis(empHis);
		int hisCount = es.selectHisCount(empHis);
		
		mv.addObject(SearchEmpHis);
		mv.addObject(hisCount);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	// 직원 등록시 아이디 중복체크 ajax
	@PostMapping("duplicationCheck.em")
	public void duplicationCheck(@RequestParam String empId, HttpServletResponse response) {

		try {
			Employee emp = new Employee();
			emp.setEmpId(empId);

			int result = es.selectEmpId(emp);

			if (result > 0) {
				response.getWriter().print("success");
			} else {
				response.getWriter().print("fail");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

}
