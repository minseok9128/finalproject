package com.kh.sm.employee.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeAjax;
import com.kh.sm.employee.model.vo.EmployeeHistory;

public interface EmployeeDao {

	String selectEncPassword(SqlSessionTemplate sqlSession, Employee emp);

	Employee selectEmployee(SqlSessionTemplate sqlSession, Employee emp);

	//직원 insert
	int insertEmployee(SqlSessionTemplate sqlSession,Employee emp);

	//직원 등록시 아이디 중복체크
	int insertEmpId(SqlSessionTemplate sqlSession, Employee emp);

	//직원 관리 이름 검색
	List<Employee> searchEmpName(SqlSessionTemplate sqlSession, String empName);
	List<Employee> selectEmployeeAll(SqlSessionTemplate sqlSession);

	//관리자 직원관리 해당 직원클릭하면 정보 select
	EmployeeAjax searchEmp(SqlSessionTemplate sqlSession, EmployeeAjax emp);

	//관리자 직원정보 수정
	int updateEmployee(SqlSessionTemplate sqlSession, Employee emp);

	//관리자 직원 삭제
	int deleteEmployee(SqlSessionTemplate sqlSession, Employee emp);

	//직원 생성시 이력남기기
	int insertHisEmployee(SqlSessionTemplate sqlSession, EmployeeHistory empHis);

	int updateHisEmployee(SqlSessionTemplate sqlSession, EmployeeHistory empHis);

	int deleteHisEmployee(SqlSessionTemplate sqlSession, EmployeeHistory empHis);

	//해당 직원 이력 select
	List<EmployeeHistory> selectEmpHis(SqlSessionTemplate sqlSession, EmployeeHistory empHis);

	int selectHisCount(SqlSessionTemplate sqlSession, EmployeeHistory empHis);

	Attachment selectAtt(SqlSessionTemplate sqlSession);

	List<HashMap<String, Object>> selectNoticeList(SqlSessionTemplate sqlSession);

	List<HashMap<String, Object>> selectActivityList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectOpportunityList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectRevRankList(SqlSessionTemplate sqlSession);

	HashMap<String, Object> selectMyRank(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectPdtTargetList(SqlSessionTemplate sqlSession, int empNo);

	HashMap<String, Object> selectAdminInfo(SqlSessionTemplate sqlSession);
	
	

}
