package com.kh.sm.employee.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeAjax;
import com.kh.sm.employee.model.vo.EmployeeHistory;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public String selectEncPassword(SqlSessionTemplate sqlSession, Employee emp) {
		return sqlSession.selectOne("Employee.selectPwd", emp.getEmpId());
	}

	@Override
	public Employee selectEmployee(SqlSessionTemplate sqlSession, Employee emp) {
		return sqlSession.selectOne("Employee.selectLoginEmp", emp);
	}

	@Override
	public int insertEmployee(SqlSessionTemplate sqlSession, Employee emp) {
		/* System.out.println(emp + " emp"); */
		return sqlSession.insert("Employee.insertEmployee",emp);
	}

	@Override
	public int insertEmpId(SqlSessionTemplate sqlSession, Employee emp) {
		
		return sqlSession.selectOne("Employee.selectEmpId",emp);
	}

	@Override
	public List<Employee> searchEmpName(SqlSessionTemplate sqlSession,String empName) {
	
		
		 return sqlSession.selectList("Employee.searchEmpName",empName); 
	}
	public List<Employee> selectEmployeeAll(SqlSessionTemplate sqlSession) {
		
		List<Employee> list = sqlSession.selectList("Employee.selectAll");
		return list;
	}

	//관리자 직원관리 해당 직원클릭하면 정보 select
	@Override
	public EmployeeAjax searchEmp(SqlSessionTemplate sqlSession, EmployeeAjax emp) {
		
		return sqlSession.selectOne("Employee.searchEmp",emp);
	}
	//관리자 직원정보 수정
	@Override
	public int updateEmployee(SqlSessionTemplate sqlSession, Employee emp) {
	
		return sqlSession.update("Employee.updateEmp",emp);
	}

	//관리자 직원 삭제
	@Override
	public int deleteEmployee(SqlSessionTemplate sqlSession, Employee emp) {
		
		return sqlSession.update("Employee.deleteEmp",emp);
	}

	@Override
	public int insertHisEmployee(SqlSessionTemplate sqlSession, EmployeeHistory empHis) {
		
		return sqlSession.insert("Employee.insertHisEmp",empHis);
	}

	@Override
	public int updateHisEmployee(SqlSessionTemplate sqlSession, EmployeeHistory empHis) {
	
		return sqlSession.insert("Employee.updateHisEmp",empHis);
	}

	@Override
	public int deleteHisEmployee(SqlSessionTemplate sqlSession, EmployeeHistory empHis) {
		
		return sqlSession.insert("Employee.deleteHisEmp",empHis);
	}

	@Override
	public List<EmployeeHistory> selectEmpHis(SqlSessionTemplate sqlSession, EmployeeHistory empHis) {
		
		return sqlSession.selectList("Employee.selectEmpHis",empHis);
	}

	@Override
	public int selectHisCount(SqlSessionTemplate sqlSession, EmployeeHistory empHis) {
		
		return sqlSession.selectOne("Employee.selectHisCount",empHis);
	}

	@Override
	public Attachment selectAtt(SqlSessionTemplate sqlSession) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Employee.selectAtt");
	}

	@Override
	public List<HashMap<String, Object>> selectNoticeList(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Employee.selectNoticeList");
	}

	@Override
	public List<HashMap<String, Object>> selectActivityList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Employee.selectActivityList", empNo);
	}

	@Override
	public List<HashMap<String, Object>> selectOpportunityList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Employee.selectOpportunityList", empNo);
	}

	@Override
	public List<HashMap<String, Object>> selectRevRankList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Employee.selectRevRankList");
	}

	@Override
	public HashMap<String, Object> selectMyRank(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectOne("Employee.selectMyRank", empNo);
	}

	@Override
	public List<HashMap<String, Object>> selectPdtTargetList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Employee.selectPdtList", empNo);
	}

	@Override
	public HashMap<String, Object> selectAdminInfo(SqlSessionTemplate sqlSession) {
		HashMap<String, Object> adminInfo = new HashMap<>();
		int clientCount = sqlSession.selectOne("Employee.selectClientCount");
		int leadCount = sqlSession.selectOne("Employee.selectLeadCount");
		int companyCount = sqlSession.selectOne("Employee.selectCompanyCount");
		List<HashMap<String, Object>> oppCount = sqlSession.selectList("Employee.selectOppCount");
		
		adminInfo.put("oppCount", oppCount);
		adminInfo.put("clientCount", clientCount);
		adminInfo.put("leadCount", leadCount);
		adminInfo.put("companyCount", companyCount);
		
		return adminInfo;
	}
	
	
	

}
