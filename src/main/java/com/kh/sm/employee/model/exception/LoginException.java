package com.kh.sm.employee.model.exception;

public class LoginException extends Exception {
	public LoginException(String msg) {
		super(msg);
	}
}
