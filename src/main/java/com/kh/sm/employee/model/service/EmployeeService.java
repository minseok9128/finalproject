package com.kh.sm.employee.model.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.exception.LoginException;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeAjax;
import com.kh.sm.employee.model.vo.EmployeeHistory;

public interface EmployeeService {

	 Employee loginEmployee(Employee emp) throws LoginException;

	 //직원 insert
	int insertEmployee(Employee emp);

	//직원 등록시 아이디 중복확인
	int selectEmpId(Employee emp);

	//직원 아이디 검색시 select
	List<Employee> searchEmpName(String empName);
	List<Employee> selectEmpAll();

	//관리자 직원관리 해당 직원클릭하면 정보 select
	EmployeeAjax searchEmp(EmployeeAjax emp);

	//관리자 직원정보 수정
	int updateEmployee(Employee emp);

	//관리자 직원 삭제
	int deleteEmployee(Employee emp);

	//직원 insert 이력 남기기
	int insertHisEmployee(EmployeeHistory empHis);

	//직원 update 이력 남기기
	int updateHisEmployee(EmployeeHistory empHis);

	int deleteHisEmployee(EmployeeHistory empHis);

	//해당 직원 이력 select
	List<EmployeeHistory> selectEmpHis(EmployeeHistory empHis);

	int selectHisCount(EmployeeHistory empHis);

	Attachment selectAtt();

	List<HashMap<String, Object>> selectNoticeList();

	List<HashMap<String, Object>> selectActivityList(int empNo);

	List<HashMap<String, Object>> selectOpportunityList(int empNo);

	List<HashMap<String, Object>> selectRevRankList();

	HashMap<String, Object> selectMyRank(int empNo);

	List<HashMap<String, Object>> selectPdtTargetList(int empNo);

	HashMap<String, Object> selectAdminInfo();
	
}
