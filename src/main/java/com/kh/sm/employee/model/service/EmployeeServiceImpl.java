package com.kh.sm.employee.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.dao.EmployeeDao;
import com.kh.sm.employee.model.exception.LoginException;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeAjax;
import com.kh.sm.employee.model.vo.EmployeeHistory;



@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private EmployeeDao ed;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public Employee loginEmployee(Employee emp) throws LoginException {
		System.out.println(sqlSession.hashCode());
		Employee loginEmp = null;
		
		String encPassword = ed.selectEncPassword(sqlSession, emp);
		if(passwordEncoder.matches(emp.getEmpPwd(), encPassword)) {
			loginEmp = ed.selectEmployee(sqlSession, emp);
		}else {
			throw new LoginException("로그인 실패");
		} 
		return loginEmp;
	}

	//직원 insert
	@Override
	public int insertEmployee(Employee emp) {
		
		String encPassword2 = passwordEncoder.encode(emp.getEmpPwd());
		
		emp.setEmpPwd(encPassword2);
		
		
		return ed.insertEmployee(sqlSession, emp);
	}

	@Override
	public int selectEmpId(Employee emp) {
		
		return ed.insertEmpId(sqlSession,emp);
	}

	@Override
	public List<Employee> searchEmpName(String empName) {
		
		
		return ed.searchEmpName(sqlSession,empName);
	}
	public List<Employee> selectEmpAll() {
		List<Employee> list = ed.selectEmployeeAll(sqlSession);
		return list;
	}

	//관리자 직원관리 해당 직원클릭하면 정보 select
	@Override
	public EmployeeAjax searchEmp(EmployeeAjax emp) {
		
		
		
		return ed.searchEmp(sqlSession,emp);
	}

	//회원 정보 수정
	@Override
	public int updateEmployee(Employee emp) {
		
		return ed.updateEmployee(sqlSession,emp);
	}

	//관리자 직원 삭제
	@Override
	public int deleteEmployee(Employee emp) {
		
		return ed.deleteEmployee(sqlSession,emp);
	}

	@Override
	public int insertHisEmployee(EmployeeHistory empHis) {
		
		return ed.insertHisEmployee(sqlSession,empHis);
	}

	@Override
	public int updateHisEmployee(EmployeeHistory empHis) {
		
		return ed.updateHisEmployee(sqlSession,empHis);
	}

	@Override
	public int deleteHisEmployee(EmployeeHistory empHis) {
		
		return ed.deleteHisEmployee(sqlSession,empHis);
	}

	@Override
	public List<EmployeeHistory> selectEmpHis(EmployeeHistory empHis) {
	
		return ed.selectEmpHis(sqlSession,empHis);
	}

	@Override
	public int selectHisCount(EmployeeHistory empHis) {
		
		return ed.selectHisCount(sqlSession,empHis);
	}

	@Override
	public Attachment selectAtt() {
		// TODO Auto-generated method stub
		return ed.selectAtt(sqlSession);
	}

	@Override
	public List<HashMap<String, Object>> selectNoticeList() {
		List<HashMap<String, Object>> noticeList = ed.selectNoticeList(sqlSession);
		return noticeList;
	}

	@Override
	public List<HashMap<String, Object>> selectActivityList(int empNo) {
		List<HashMap<String, Object>> activityList = ed.selectActivityList(sqlSession, empNo);
		return activityList;
	}

	@Override
	public List<HashMap<String, Object>> selectOpportunityList(int empNo) {
		List<HashMap<String, Object>> oppList = ed.selectOpportunityList(sqlSession, empNo);
		return oppList;
	}

	@Override
	public List<HashMap<String, Object>> selectRevRankList() {
		List<HashMap<String, Object>> revRank = ed.selectRevRankList(sqlSession);
		return revRank;
	}

	@Override
	public HashMap<String, Object> selectMyRank(int empNo) {
		HashMap<String, Object> myRank = ed.selectMyRank(sqlSession, empNo);
		return myRank;
	}

	@Override
	public List<HashMap<String, Object>> selectPdtTargetList(int empNo) {
		List<HashMap<String, Object>> pdtList = ed.selectPdtTargetList(sqlSession, empNo);
		return pdtList;
	}

	@Override
	public HashMap<String, Object> selectAdminInfo() {
		HashMap<String, Object> adminInfo = ed.selectAdminInfo(sqlSession);
		return adminInfo;
	}

}
