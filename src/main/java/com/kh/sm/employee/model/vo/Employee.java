package com.kh.sm.employee.model.vo;

import java.sql.Date;

public class Employee {
	private int empNo;
	private String empName;
	private String empId;
	private String empPwd;
	private String deptName;
	private String empPhone;
	private String email;
	private String comPhone;
	private int empType;
	private Date enrollDate;
	private Date modifyDate;
	private String empRemark;
	private String empDelYn;
	
	public Employee() {}

	public Employee(int empNo, String empName, String empId, String empPwd, String deptName, String empPhone,
			String email, String comPhone, int empType, Date enrollDate, Date modifyDate, String empRemark,
			String empDelYn) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.empId = empId;
		this.empPwd = empPwd;
		this.deptName = deptName;
		this.empPhone = empPhone;
		this.email = email;
		this.comPhone = comPhone;
		this.empType = empType;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.empRemark = empRemark;
		this.empDelYn = empDelYn;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpPwd() {
		return empPwd;
	}

	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComPhone() {
		return comPhone;
	}

	public void setComPhone(String comPhone) {
		this.comPhone = comPhone;
	}

	public int getEmpType() {
		return empType;
	}

	public void setEmpType(int empType) {
		this.empType = empType;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getEmpRemark() {
		return empRemark;
	}

	public void setEmpRemark(String empRemark) {
		this.empRemark = empRemark;
	}

	public String getEmpDelYn() {
		return empDelYn;
	}

	public void setEmpDelYn(String empDelYn) {
		this.empDelYn = empDelYn;
	}

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", empName=" + empName + ", empId=" + empId + ", empPwd=" + empPwd
				+ ", deptName=" + deptName + ", empPhone=" + empPhone + ", email=" + email + ", comPhone=" + comPhone
				+ ", empType=" + empType + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate + ", empRemark="
				+ empRemark + ", empDelYn=" + empDelYn + "]";
	}
	
	
}
