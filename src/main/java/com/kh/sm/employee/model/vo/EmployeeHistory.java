package com.kh.sm.employee.model.vo;

import java.util.Date;

public class EmployeeHistory {
	
	private int empHisId;
	private String empHisStatus;
	private Date empHisDate;
	private int empNo;
	
	public EmployeeHistory() {}

	public EmployeeHistory(int empHisId, String empHisStatus, Date empHisDate, int empNo) {
		super();
		this.empHisId = empHisId;
		this.empHisStatus = empHisStatus;
		this.empHisDate = empHisDate;
		this.empNo = empNo;
	}

	public int getEmpHisId() {
		return empHisId;
	}

	public void setEmpHisId(int empHisId) {
		this.empHisId = empHisId;
	}

	public String getEmpHisStatus() {
		return empHisStatus;
	}

	public void setEmpHisStatus(String empHisStatus) {
		this.empHisStatus = empHisStatus;
	}

	public Date getEmpHisDate() {
		return empHisDate;
	}

	public void setEmpHisDate(Date empHisDate) {
		this.empHisDate = empHisDate;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "EmployeeHistory [empHisId=" + empHisId + ", empHisStatus=" + empHisStatus + ", empHisDate=" + empHisDate
				+ ", empNo=" + empNo + "]";
	}
	
	
	

}
