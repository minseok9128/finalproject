package com.kh.sm.employee.model.vo;



public class EmployeeHistoryAjax {	
	
	private int empHisId;
private String empHisStatus;
private String empHisDate;
private int empNo;

public EmployeeHistoryAjax() {}

public EmployeeHistoryAjax(int empHisId, String empHisStatus, String empHisDate, int empNo) {
	super();
	this.empHisId = empHisId;
	this.empHisStatus = empHisStatus;
	this.empHisDate = empHisDate;
	this.empNo = empNo;
}

public int getEmpHisId() {
	return empHisId;
}

public void setEmpHisId(int empHisId) {
	this.empHisId = empHisId;
}

public String getEmpHisStatus() {
	return empHisStatus;
}

public void setEmpHisStatus(String empHisStatus) {
	this.empHisStatus = empHisStatus;
}

public String getEmpHisDate() {
	return empHisDate;
}

public void setEmpHisDate(String empHisDate) {
	this.empHisDate = empHisDate;
}

public int getEmpNo() {
	return empNo;
}

public void setEmpNo(int empNo) {
	this.empNo = empNo;
}

@Override
public String toString() {
	return "EmployeeHistoryAjax [empHisId=" + empHisId + ", empHisStatus=" + empHisStatus + ", empHisDate=" + empHisDate
			+ ", empNo=" + empNo + "]";
}




}
