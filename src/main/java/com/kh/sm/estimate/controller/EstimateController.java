package com.kh.sm.estimate.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Redirect;
import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.kh.sm.common.Attachment;
import com.kh.sm.common.SaveFile;
import com.kh.sm.estimate.model.exception.InsertEstimateEnrollmentException;
import com.kh.sm.estimate.model.exception.SelectEstumateDetailException;
import com.kh.sm.estimate.model.service.EstimateService;
import com.kh.sm.estimate.model.vo.Estimate;
import com.kh.sm.offer.model.service.OfferService;
import com.kh.sm.product.model.service.ProductService;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;

@Controller
public class EstimateController {
	
	@Autowired
	private EstimateService es;
	
	@Autowired
	private ProductService ps;
	
	@Autowired
	private SaveFile sf;
	
	@RequestMapping("estimateMain.est")
	public String estimateMain(HttpServletRequest request, @RequestParam("memberId") int memberId) {
		int aa = 0;
		String msg = "";
		if(request.getParameter("aa") != null) {
			 aa = Integer.parseInt(request.getParameter("aa"));
			msg = request.getParameter("msg");
			request.setAttribute("aa", aa);
			request.setAttribute("msg", msg);
		}
		
		List<Estimate> estList = es.selectEstimateMain(memberId);
		int estCount = es.selectEstCount(memberId);
		request.setAttribute("list", estList);
		request.setAttribute("estCount", estCount);
		
		return "sales/salesManagement/estimateMain";
	}
	
	@PostMapping("selList.est")
	public void searchProduct(Estimate est, HttpServletResponse response) {
		List<Estimate> list = es.selList(est);
		System.out.println("list : " + list);
		for(Estimate list1 : list) {
			System.out.println("ListId" + list1.getEstId());
			for(ProductDetail prl : list1.getPrList()) {
				System.out.println("prl :" + prl);
			}
		}
		
		
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@PostMapping("estimateEnrollment.est")
	public ModelAndView estimateEnrollment(Estimate est, ProductDetail prdDetail,
			Model mo, MultipartHttpServletRequest multiRequest,HttpServletRequest request ) {
		
		if(est.getEstVatYn() == null) {
			est.setEstVatYn("N");
		}
		
		est.setEstQuantity(changeNumber(request.getParameter("estQuantityString")));
		est.setEstTotalValue(changeNumber(request.getParameter("estTotalValueString")));
		est.setEstValue(changeNumber(request.getParameter("estValueString")));
		est.setEstTax(changeNumber(request.getParameter("estTaxString")));
		est.setEstTotal(changeNumber(request.getParameter("estTotalString")));
		int processId = Integer.parseInt(request.getParameter("processId"));
//===================== 견적 정보들 가져오는 것 =========================================
		String[] pdtId = (String[]) request.getParameterValues("pdtId");
		String[] discountRate = (String[]) request.getParameterValues("discountRate");
		String[] detailPrice = (String[]) request.getParameterValues("detailPrice");
		String[] quantity = (String[]) request.getParameterValues("quantity");
		String[] price = (String[]) request.getParameterValues("price");
		ProductDetail prD = null;
	
		List<MultipartFile> attachment = multiRequest.getFiles("attachment");
		ModelAndView mv = new ModelAndView();
		try {
			int result = es.insertEstimateEnrollment(est);
			es.updateProcess(processId, est.getOppId());
			System.out.println(pdtId);
			if(pdtId != null) {
				for(int i=0; i<pdtId.length; i++) {
					prD = new ProductDetail();
					prD.setPdtId(Integer.parseInt(pdtId[i])); 				//제품코드
					prD.setDiscountRate(Integer.parseInt(discountRate[i]));	//할인율
					prD.setDetailPrice(changeNumber(price[i]));			//단가
					prD.setQuantity(changeNumber(quantity[i]));			//수량
					prD.setTotalPrice(changeNumber(detailPrice[i]));	//합계
					prD.setEstId(est.getEstId());
					prD.setCategory("EST");
					prD.setCategoryId(est.getEstId());
					prD.setOppId(Integer.parseInt(request.getParameter("oppId")));
					ps.insertProductDetail(prD);
					System.out.println(prD);
				}
			}
			for(int i=0; i<attachment.size(); i++) {
				Attachment ac = new Attachment();

				ac.setCategoryId(est.getEstId());
				ac.setCategory(9);
				sf.saveFile(attachment.get(i), request, ac);
				int requestFileInsert = es.insertAttachment(ac);
			}
			
			mv.setViewName("redirect:/estimateMain.est");
			mv.addObject("msg", "등록이 완료되었습니다.");
			mv.addObject("aa", 1);
			mv.addObject("memberId", est.getEmpNo());
			return mv;
		} catch (InsertEstimateEnrollmentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mv;
		}
	}
	
	@PostMapping("estMainscroll.est")
	public void estMainscroll(ModelAndView mv, @RequestParam("lastCno") int lastCno, @RequestParam("memberId") int memberId, HttpServletResponse response) {
		
		List<Estimate> list = es.selectEstimateScroll(lastCno, memberId);
		
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			new Gson().toJson(list, response.getWriter());
		} catch (JsonIOException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@GetMapping("EstimateDetail.est")
	public String estimateDetail(HttpServletRequest request, @RequestParam("estId") int estId) {
		
		try {
			Estimate est = es.selectEstimateDetail(estId);
			List<Attachment> at = es.selectAttachmentEst(estId);
			List<Object> proList = es.selectProductList(estId);
			
			request.setAttribute("est", est);
			request.setAttribute("atList", at);
			request.setAttribute("proList", proList);
			return "sales/salesManagement/estimateDetail";
		} catch (SelectEstumateDetailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return "";
		}
		
		
	}
	@GetMapping("modifyEstimate.est")
	public String modifyEstimate(@RequestParam("estId") int estId, HttpServletRequest request) {
		// 견적 상세에서 수정 버튼을 클릭해서 이동하는 곳
		try {
			Estimate est = es.selectEstimateDetail(estId);
			List<Attachment> at = es.selectAttachmentEst(estId);
			List<Object> proList = es.selectProductList(estId);
			
			request.setAttribute("est", est);
			request.setAttribute("estId", est.getEstId());
			request.setAttribute("atList", at);
			request.setAttribute("proList", proList);
			return "sales/salesManagement/estimateEnrollment";
		} catch (SelectEstumateDetailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	
	
	@PostMapping("estimateModify.est")
	public ModelAndView estimateModify(Estimate est, ProductDetail prdDetail,
			Model mo, RedirectAttributes rttr,MultipartHttpServletRequest multiRequest,HttpServletRequest request ) throws InsertEstimateEnrollmentException {
		
		if(est.getEstVatYn() == null) {
			est.setEstVatYn("N");
		}
		
		est.setEstQuantity(changeNumber(request.getParameter("estQuantityString")));
		est.setEstTotalValue(changeNumber(request.getParameter("estTotalValueString")));
		est.setEstValue(changeNumber(request.getParameter("estValueString")));
		est.setEstTax(changeNumber(request.getParameter("estTaxString")));
		est.setEstTotal(changeNumber(request.getParameter("estTotalString")));
//===================== 견적 정보들 가져오는 것 =========================================
		String[] pdtId = (String[]) request.getParameterValues("pdtId");
		String[] discountRate = (String[]) request.getParameterValues("discountRate");
		String[] detailPrice = (String[]) request.getParameterValues("detailPrice");
		String[] quantity = (String[]) request.getParameterValues("quantity");
		String[] price = (String[]) request.getParameterValues("price");
		String[] deleteFile =  (String[])request.getParameterValues("deleteFile");
		String[] deleteProduct = (String[]) request.getParameterValues("deleteProductId");
		List<MultipartFile> attachment = multiRequest.getFiles("attachment");
			
			int result = es.updateEstimateEnrollment(est);
			ProductDetail prD = null;
			
			System.out.println("품목 삭제 : "+deleteProduct);
			if(deleteProduct != null) {
				for(int i=0; i<deleteProduct.length; i++) {
					es.deleteProduct(Integer.parseInt(deleteProduct[i]));
				}
			}
			
			if(deleteFile != null) {
				for(int i=0; i<deleteFile.length; i++) {
					es.deleteFile(Integer.parseInt(deleteFile[i]));
				}
			}
			
			List<ProductDetail> proList = es.selectPrdList(est.getEstId());
			List<ProductDetail> proList2 = new ArrayList<ProductDetail>();
			
			if(pdtId != null) {
				for(int i=0; i<pdtId.length; i++) {
					prD = new ProductDetail();
					prD.setPdtId(changeNumber(pdtId[i])); 
					prD.setDiscountRate(changeNumber(discountRate[i]));	//할인율
					prD.setDetailPrice(changeNumber(price[i]));			//단가
					prD.setQuantity(changeNumber(quantity[i]));			//수량
					prD.setTotalPrice(changeNumber(detailPrice[i]));	//합계
					prD.setEstId(est.getEstId());
					prD.setCategory("EST");
					prD.setCategoryId(est.getEstId());
					prD.setOppId(changeNumber(request.getParameter("oppId")));
					proList2.add(prD);
				}
			}
				for(int i=0; i<attachment.size(); i++) {

					Attachment ac = new Attachment();

					ac.setCategoryId(est.getEstId());
					ac.setCategory(9);
					sf.saveFile(attachment.get(i), request, ac);
					System.out.println("새로 등록 하는 파일 : "+ac.getOriginName());
					es.insertAttachment(ac);
				}
				
			boolean a = true;
			for(int i=0; i<proList2.size(); i++) {
				for(int j=0; j<proList.size(); j++) {
					if(proList2.get(i).getPdtId() == proList.get(j).getPdtId()) {
						a = true;
						break;
					} else {
						a = false;
					}
				}
				if(a == true) {
					System.out.println("기존에 있는 품목 업데이트 : " + proList2.get(i));
					es.updateProductDetail(proList2.get(i));
				} else {
					System.out.println("새로 등록하는 품목 : "+proList2.get(i));
					ps.insertProductDetail(proList2.get(i));
 				}
			}
			
			ModelAndView mv = new ModelAndView();
			
			mv.setViewName("redirect:showSuccessPage.est");
			mv.addObject("msg", "수정이 완료되었습니다.");
			mv.addObject("aa", 3);
			mv.addObject("Id", est.getEstId());
			return mv;
	}
	@PostMapping("selectOppPrd.est")
	public void selectOppPrd(ModelAndView mv, @RequestParam("oppId") int oppId, HttpServletResponse response) {
		System.out.println(oppId);
		
		List<Object> OppProductDetail = es.selectOppPrd(oppId);
		System.out.println(OppProductDetail);
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(OppProductDetail, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@RequestMapping("showSuccessPage.est")
	public String successPage(HttpServletRequest request, RedirectAttributes rttr, @RequestParam("msg")String msg, @RequestParam("aa")int aa, @RequestParam("Id")int Id) {
		
		  request.setAttribute("msg",msg); 
		  request.setAttribute("aa",aa); 
		  request.setAttribute("Id",Id);
		 return "sales/salesManagement/successPage";
	}
	
	public int changeNumber(String number) {
		int changeName = 0;
		if(number.indexOf(',') != -1) {
			String[] StringA = number.split(",");
			String StringB = "";
			for(int i=0; i<StringA.length; i++) {
				StringB += StringA[i];
			}
			changeName = Integer.parseInt(StringB);
		} else {
			changeName = Integer.parseInt(number);
		}
		
		
		return changeName;
	}

	

}
