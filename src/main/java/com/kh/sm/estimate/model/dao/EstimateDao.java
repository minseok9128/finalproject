package com.kh.sm.estimate.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.common.Attachment;
import com.kh.sm.estimate.model.vo.Estimate;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.product.model.vo.ProductHistory;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;

public interface EstimateDao {

	List<Estimate> selList(SqlSessionTemplate sqlSession, Estimate est);

	int insertEstimateEnrollment(Estimate est, SqlSessionTemplate sqlSession);

	int insertAttachment(Attachment ac, SqlSessionTemplate sqlSession);

	List<Estimate> selectEstimateMian(int memberId, SqlSessionTemplate sqlSession);

	List<Estimate> selectEstimateScroll(int lastCno, int memberId, SqlSessionTemplate sqlSession);

	Estimate selectEstimateDetail(int estId, SqlSessionTemplate sqlSession);

	List<Attachment> selectAttachmentEst(int estId, SqlSessionTemplate sqlSession);

	List<Object> selectProductList(int estId, SqlSessionTemplate sqlSession);

	int updateEstimateEnrollment(Estimate est, SqlSessionTemplate sqlSession);

	int updateProductDetail(ProductDetail productDetail, SqlSessionTemplate sqlSession);

	List<ProductDetail> selectPrdList(int estId, SqlSessionTemplate sqlSession);

	int deleteFile(int fileId, SqlSessionTemplate sqlSession);

	int deleteProduct(int prdId, SqlSessionTemplate sqlSession);

	int updateProcess(int processId, SqlSessionTemplate sqlSession, int oppId);

	List<Object> selectOppPrd(int oppId, SqlSessionTemplate sqlSession);

	int selectEstCount(int memberId, SqlSessionTemplate sqlSession);

	Opportunity selectOppDetail(int oppId, SqlSessionTemplate sqlSession);



}
