package com.kh.sm.estimate.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.jca.cci.object.EisOperation;
import org.springframework.stereotype.Repository;

import com.kh.sm.common.Attachment;
import com.kh.sm.estimate.model.vo.Estimate;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;



@Repository
public class EstimateDaoImpl implements EstimateDao{

	@Override
	public List<Estimate> selList(SqlSessionTemplate sqlSession, Estimate est) {
		List<Estimate> list = sqlSession.selectList("Estimate.selectList", est);
		for(Estimate estimate : list) {
			int estId = estimate.getEstId();
			estimate.setPrList(sqlSession.selectList("Estimate.selectPr",estId)); ;
		}
		
		return list;
	}

	@Override
	public int insertEstimateEnrollment(Estimate est, SqlSessionTemplate sqlSession) {
		System.out.println(est);
		return sqlSession.insert("Estimate.insertEst", est);
	}

	@Override
	public int insertAttachment(Attachment ac, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Estimate.insertAttachment", ac);
	}

	@Override
	public List<Estimate> selectEstimateMian(int memberId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Estimate.selectEstMain", memberId);
	}

	@Override
	public List<Estimate> selectEstimateScroll(int lastCno, int memberId, SqlSessionTemplate sqlSession) {
		
		RowBounds row = new RowBounds(lastCno+1, 20);
		return sqlSession.selectList("Estimate.selectScroll", memberId, row);
	}

	@Override
	public Estimate selectEstimateDetail(int estId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Estimate.selectDetail", estId);
	}

	@Override
	public List<Attachment> selectAttachmentEst(int estId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Estimate.selectAttachmentEst", estId);
	}

	@Override
	public List<Object> selectProductList(int estId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Estimate.selectproductEst", estId);
	}

	@Override
	public int updateEstimateEnrollment(Estimate est, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Estimate.updateEstimateEnrollment", est);
	}

	@Override
	public int updateProductDetail(ProductDetail productDetail, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Estimate.updateProduct", productDetail);
	}

	@Override
	public List<ProductDetail> selectPrdList(int estId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Estimate.selectPrdList", estId);
	}

	@Override
	public int deleteFile(int fileId, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Estimate.deleteFile", fileId);
	}

	@Override
	public int deleteProduct(int prdId, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Estimate.deleteProduct", prdId);
	}

	@Override
	public int updateProcess(int processId, SqlSessionTemplate sqlSession, int oppId) {
		
		int prosu = sqlSession.selectOne("Estimate.processSucces", processId);
		
		OppProcess op = new OppProcess();
		op.setProcessId(processId);
		op.setProcessSuccess(prosu);
		
		return sqlSession.update("Estimate.process", op);
	}

	@Override
	public List<Object> selectOppPrd(int oppId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Estimate.selectOppPrd", oppId);
	}

	@Override
	public int selectEstCount(int memberId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Estimate.selectCount", memberId);
	}

	@Override
	public Opportunity selectOppDetail(int oppId, SqlSessionTemplate sqlSession) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Estimate.selectOpp",oppId);
	}




}
