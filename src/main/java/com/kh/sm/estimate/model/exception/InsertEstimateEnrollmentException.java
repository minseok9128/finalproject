package com.kh.sm.estimate.model.exception;

public class InsertEstimateEnrollmentException extends Exception {
	public InsertEstimateEnrollmentException(String msg) {
		super(msg);
	}
}
