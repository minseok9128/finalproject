package com.kh.sm.estimate.model.exception;

public class SelectEstumateDetailException extends Exception {
	public SelectEstumateDetailException(String msg) {
		super(msg);
	}
}
