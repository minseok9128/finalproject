package com.kh.sm.estimate.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.kh.sm.common.Attachment;
import com.kh.sm.estimate.model.exception.InsertEstimateEnrollmentException;
import com.kh.sm.estimate.model.exception.SelectEstumateDetailException;
import com.kh.sm.estimate.model.vo.Estimate;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;

public interface EstimateService {

	List<Estimate> selList(Estimate est);

	int insertEstimateEnrollment(Estimate est) throws InsertEstimateEnrollmentException;

	int insertAttachment(Attachment ac);

	List<Estimate> selectEstimateMain(int memberId);

	List<Estimate> selectEstimateScroll(int lastCno, int memberId);

	Estimate selectEstimateDetail(int estId) throws SelectEstumateDetailException;

	List<Attachment> selectAttachmentEst(int estId);

	List<Object> selectProductList(int estId);

	int updateEstimateEnrollment(Estimate est);

	void updateProductDetail(ProductDetail productDetail);

	List<ProductDetail> selectPrdList(int estId);

	void deleteFile(int parseInt);

	void deleteProduct(int prdId);

	void updateProcess(int processId, int oppId);

	List<Object> selectOppPrd(int oppId);

	int selectEstCount(int memberId);

	Opportunity selectOppDetail(int oppId);



}
