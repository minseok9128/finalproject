package com.kh.sm.estimate.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.common.Attachment;
import com.kh.sm.estimate.model.dao.EstimateDao;
import com.kh.sm.estimate.model.exception.InsertEstimateEnrollmentException;
import com.kh.sm.estimate.model.exception.SelectEstumateDetailException;
import com.kh.sm.estimate.model.vo.Estimate;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;


@Service
public class EstimateServiceImpl implements EstimateService{
	
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private EstimateDao ed;

	@Override
	public List<Estimate> selList(Estimate est) {
		return ed.selList(sqlSession,est);
	}

	@Override
	public int insertEstimateEnrollment(Estimate est) throws InsertEstimateEnrollmentException {
		
		int result = ed.insertEstimateEnrollment(est, sqlSession);
		
		if(result < 0) {
			throw new InsertEstimateEnrollmentException("견적 등록 실패");
		}
		
		return result;
	}

	@Override
	public int insertAttachment(Attachment ac) {
		
		int result = ed.insertAttachment(ac, sqlSession);
		
		return result;
	}

	@Override
	public List<Estimate> selectEstimateMain(int memberId) {
		
		List<Estimate> list = ed.selectEstimateMian(memberId, sqlSession);
		
		return list;
	}

	@Override
	public List<Estimate> selectEstimateScroll(int lastCno, int memberId) {
		
		List<Estimate> list = ed.selectEstimateScroll(lastCno, memberId, sqlSession);
		
		return list;
	}

	@Override
	public Estimate selectEstimateDetail(int estId) throws SelectEstumateDetailException {
		
		Estimate est = ed.selectEstimateDetail(estId, sqlSession);
		
		if(est == null) {
			throw new SelectEstumateDetailException("견적 조회 실패");
		}
		
		return est;
	}

	@Override
	public List<Attachment> selectAttachmentEst(int estId) {
		
		List<Attachment> list = ed.selectAttachmentEst(estId, sqlSession);
		
		if(list == null) {
			
		}
		
		return list;
	}

	@Override
	public List<Object> selectProductList(int estId) {
		
		List<Object> proList = ed.selectProductList(estId, sqlSession);
		
		if(proList == null) {
			
		}
		
		return proList;
	}

	@Override
	public int updateEstimateEnrollment(Estimate est) {
		
		int result = ed.updateEstimateEnrollment(est, sqlSession);
		
		if(result < 0) {
			
		}
		
		return result;
	}

	@Override
	public void updateProductDetail(ProductDetail productDetail) {
		
		int result = ed.updateProductDetail(productDetail, sqlSession);
		
	}

	@Override
	public List<ProductDetail> selectPrdList(int estId) {
		return ed.selectPrdList(estId, sqlSession);
	}

	@Override
	public void deleteFile(int fileId) {
		int result = ed.deleteFile(fileId, sqlSession);
	}

	@Override
	public void deleteProduct(int prdId) {
		int result = ed.deleteProduct(prdId, sqlSession);
		
	}

	@Override
	public void updateProcess(int processId, int oppId) {
		int result = ed.updateProcess(processId, sqlSession, oppId);
		
		if(result < 0) {
			
		}
		
	}

	@Override
	public List<Object> selectOppPrd(int oppId) {
		List<Object> list = ed.selectOppPrd(oppId, sqlSession);
		return list;
	}

	@Override
	public int selectEstCount(int memberId) {
		return ed.selectEstCount(memberId, sqlSession);
	}

	@Override
	public Opportunity selectOppDetail(int oppId) {
		// TODO Auto-generated method stub
		return ed.selectOppDetail(oppId,sqlSession);
	}

}
