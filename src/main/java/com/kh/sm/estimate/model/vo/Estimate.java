package com.kh.sm.estimate.model.vo;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.product.model.vo.ProductDetail;

public class Estimate {
	private int estId;
	private String estName;
	private int companyId;
	private String comName;
	private int clientId;
	private String clientName;
	private int oppId;
	private String oppName;
	private Date estDate;
	private int estQuantity;
	private int estTotalValue;
	private int estValue;
	private int estTax;
	private int estTotal;
	private String estVatYn;
	private int empNo;
	private String empName;
	private String estContent;
	private List<ProductDetail> prList;
	private List<HashMap<String,Object>> prListH;
	private Client client;
	private Company company;
	
	public Estimate() {}

	public Estimate(int estId, String estName, int companyId, String comName, int clientId, String clientName,
			int oppId, String oppName, Date estDate, int estQuantity, int estTotalValue, int estValue, int estTax,
			int estTotal, String estVatYn, int empNo, String empName, String estContent, List<ProductDetail> prList,
			List<HashMap<String, Object>> prListH, Client client, Company company) {
		super();
		this.estId = estId;
		this.estName = estName;
		this.companyId = companyId;
		this.comName = comName;
		this.clientId = clientId;
		this.clientName = clientName;
		this.oppId = oppId;
		this.oppName = oppName;
		this.estDate = estDate;
		this.estQuantity = estQuantity;
		this.estTotalValue = estTotalValue;
		this.estValue = estValue;
		this.estTax = estTax;
		this.estTotal = estTotal;
		this.estVatYn = estVatYn;
		this.empNo = empNo;
		this.empName = empName;
		this.estContent = estContent;
		this.prList = prList;
		this.prListH = prListH;
		this.client = client;
		this.company = company;
	}

	public int getEstId() {
		return estId;
	}

	public void setEstId(int estId) {
		this.estId = estId;
	}

	public String getEstName() {
		return estName;
	}

	public void setEstName(String estName) {
		this.estName = estName;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public String getOppName() {
		return oppName;
	}

	public void setOppName(String oppName) {
		this.oppName = oppName;
	}

	public Date getEstDate() {
		return estDate;
	}

	public void setEstDate(Date estDate) {
		this.estDate = estDate;
	}

	public int getEstQuantity() {
		return estQuantity;
	}

	public void setEstQuantity(int estQuantity) {
		this.estQuantity = estQuantity;
	}

	public int getEstTotalValue() {
		return estTotalValue;
	}

	public void setEstTotalValue(int estTotalValue) {
		this.estTotalValue = estTotalValue;
	}

	public int getEstValue() {
		return estValue;
	}

	public void setEstValue(int estValue) {
		this.estValue = estValue;
	}

	public int getEstTax() {
		return estTax;
	}

	public void setEstTax(int estTax) {
		this.estTax = estTax;
	}

	public int getEstTotal() {
		return estTotal;
	}

	public void setEstTotal(int estTotal) {
		this.estTotal = estTotal;
	}

	public String getEstVatYn() {
		return estVatYn;
	}

	public void setEstVatYn(String estVatYn) {
		this.estVatYn = estVatYn;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEstContent() {
		return estContent;
	}

	public void setEstContent(String estContent) {
		this.estContent = estContent;
	}

	public List<ProductDetail> getPrList() {
		return prList;
	}

	public void setPrList(List<ProductDetail> prList) {
		this.prList = prList;
	}

	public List<HashMap<String, Object>> getPrListH() {
		return prListH;
	}

	public void setPrListH(List<HashMap<String, Object>> prListH) {
		this.prListH = prListH;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Estimate [estId=" + estId + ", estName=" + estName + ", companyId=" + companyId + ", comName=" + comName
				+ ", clientId=" + clientId + ", clientName=" + clientName + ", oppId=" + oppId + ", oppName=" + oppName
				+ ", estDate=" + estDate + ", estQuantity=" + estQuantity + ", estTotalValue=" + estTotalValue
				+ ", estValue=" + estValue + ", estTax=" + estTax + ", estTotal=" + estTotal + ", estVatYn=" + estVatYn
				+ ", empNo=" + empNo + ", empName=" + empName + ", estContent=" + estContent + ", prList=" + prList
				+ ", prListH=" + prListH + ", client=" + client + ", company=" + company + "]";
	}

	
	
}
