package com.kh.sm.goView;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ViewController {
	
	@GetMapping("0.go")
	public String go0() {
		return "common/searchContents";
	}
	@GetMapping("01.go")
	public String go01() {
		return "common/searchContentsA";
	}
	
	
	@GetMapping("1.go")
	public String go1() {
		return "main/salesMain";
	}
	//영업사원 시작
		@GetMapping("2.go")
		public String go2() {
			return "sales/clientManagement/client/clientDetail";
		}
		@GetMapping("3.go")
		public String go3() {
			return "sales/clientManagement/client/clientList";
		}
		@GetMapping("4.go")
		public String go4() {
			return "sales/clientManagement/client/insertClient";
		}
		@GetMapping("5.go")
		public String go5() {
			return "sales/clientManagement/client/updateClient";
		}
		@GetMapping("6.go")
		public String go6() {
			return "sales/clientManagement/company/companyDetail";
		}
		@GetMapping("7.go")
		public String go7() {
			return "sales/clientManagement/company/companyList";
		}
		@GetMapping("8.go")
		public String go8() {
			return "sales/clientManagement/company/insertCompany";
		}
		@GetMapping("9.go")
		public String go9() {
			return "sales/clientManagement/company/updateCompany";
		}
		@GetMapping("10.go")
		public String go10() {
			return "sales/clientManagement/lead/LeadInsert";
		}
		@GetMapping("11.go")
		public String go11() {
			return "sales/clientManagement/Lead/LeadDetail";
		}
		@GetMapping("12.go")
		public String go12() {
			return "sales/clientManagement/Lead/LeadList2";
		}
		@GetMapping("13.go")
		public String go13() {
			return "sales/clientManagement/lead/updateLead";
		}
	//영업활동
	@GetMapping("14.go")
	public String go14() {
		return "sales/salesActivity/salesActivitiesList";
	}
	@GetMapping("15.go")
	public String go15() {
		return "sales/salesActivity/salesActivitiesTable";
	}
	@GetMapping("16.go")
	public String go16() {
		return "sales/salesActivity/salesActivitiesEnroll";
	}
	@GetMapping("17.go")
	public String go17() {
		return "sales/salesActivity/salesActivitiesCal";
	}
	@GetMapping("18.go")
	public String go18() {
		return "sales/salesActivity/salesActivitiesDetail";
	}
	//영업기회
	@GetMapping("19.go")
	public String go19() {
		return "sales/salesOpportunity/salesOpportunity";
	}
	@GetMapping("20.go")
	public String go20() {
		return "sales/salesOpportunity/salesOpportunityDetail";
	}
	@GetMapping("21.go")
	public String go21() {
		return "sales/salesOpportunity/salesOpportunityEnroll";
	}
	//영업공지
	@GetMapping("22.go")
	public String go22() {
		return "sales/salesNotice/insertSalesNotice";
	}
	@GetMapping("23.go")
	public String go23() {
		return "sales/salesNotice/salesNotice";
	}
	@GetMapping("24.go")
	public String go24() {
		return "sales/salesNotice/salesNoticeDetail";
	}
	@GetMapping("25.go")
	public String go25() {
		return "sales/salesNotice/updateSalesNotice";
	}
	//영업현황
	@GetMapping("26.go")
	public String go26() {
		return "sales/salesStatus/customerContactStatus";
	}
	@GetMapping("27.go")
	public String go27() {
		return "sales/salesStatus/opportunityStatus";
	}
	@GetMapping("28.go")
	public String go28() {
		return "sales/salesStatus/salesAchievementStatus";
	}
	@GetMapping("29.go")
	public String go29() {
		return "sales/salesStatus/salesActivitiesByPeriod";
	}
	@GetMapping("30.go")
	public String go30() {
		return "sales/salesStatus/salesActivitiesByProduct";
	}
	@GetMapping("31.go")
	public String go31() {
		return "sales/salesStatus/salesStatus";
	}
	//영업관리
	@GetMapping("32.go")
	public String go32() {
		return "sales/salesManagement/contractDetail";
	}
	@GetMapping("33.go")
	public String go33() {
		return "sales/salesManagement/contractEnrollment";
	}
	@GetMapping("34.go")
	public String go34() {
		return "sales/salesManagement/contractMain";
	}
	@GetMapping("35.go")
	public String go35() {
		return "sales/salesManagement/estimateDetail";
	}
	@GetMapping("36.go")
	public String go36() {
		return "sales/salesManagement/estimateEnrollment";
	}
	@GetMapping("37.go")
	public String go37() {
		return "sales/salesManagement/estimateMain";
	}
	
	@GetMapping("39.go")
	public String go39() {
		return "sales/salesManagement/insertSalesReport";
	}
	@GetMapping("40.go")
	public String go40() {
		return "sales/salesManagement/offerDetail";
	}
	@GetMapping("41.go")
	public String go41() {
		return "sales/salesManagement/offerEnrollment";
	}
	@GetMapping("42.go")
	public String go42() {
		return "sales/salesManagement/offerMain";
	}
	@GetMapping("43.go")
	public String go43() {
		return "sales/salesManagement/productMain";
	}
	@GetMapping("44.go")
	public String go44() {
		return "sales/salesManagement/salesInquiry";
	}
	@GetMapping("45.go")
	public String go45() {
		return "sales/salesManagement/salesReport";
	}
	@GetMapping("46.go")
	public String go46() {
		return "sales/salesManagement/salesReportDetail";
	}
	@GetMapping("47.go")
	public String go47() {
		return "sales/salesManagement/selectSalesReport";
	}
	//영업사원 끝 
	
	//관리자 시작
	
	//관리자 메인
	@GetMapping("500.go")
	public String go500() {
		return "main/adminMain";
	}
	
	//clientManagement(고객관리)
	@GetMapping("48.go")
	public String go48() {
		return "admin/clientManagement/client";
	}
	
	@GetMapping("49.go")
	public String go49() {
		return "admin/clientManagement/company";
	}
	
	@GetMapping("50.go")
	public String go50() {
		return "admin/clientManagement/companyAndClientUpload";
	}
	
	@GetMapping("51.go")
	public String go51() {
		return "admin/clientManagement/ResponsibilityManagement";
	}
	//employee(직원)
	@GetMapping("53.go")
	public String go53() {
		return "admin/employee/empEnroll";
	}
	@GetMapping("54.go")
	public String go54() {
		return "admin/employee/empExcelEnroll";
	}
	@GetMapping("55.go")
	public String go55() {
		return "admin/employee/empManagement";
	}
	//영업활동
	@GetMapping("56.go")
	public String go56() {
		return "admin/salesActivity/salesActivity";
	}
	//영업관리
	@GetMapping("57.go")
	public String go57() {
		return "admin/salesManagement/contract";
	}
	@GetMapping("58.go")
	public String go58() {
		return "admin/salesManagement/estimate";
	}
	@GetMapping("59.go")
	public String go59() {
		return "admin/salesManagement/productManagement";
	}
	//영업기회
	@GetMapping("60.go")
	public String go60() {
		return "admin/salesOpportunity/salesOpportunity";
	}
	//영업현황
	@GetMapping("61.go")
	public String go61() {
		return "admin/salesStatus/monthbusinessStatus";
	}
	@GetMapping("62.go")
	public String go62() {
		return "admin/salesStatus/monthCustomerContactStatus";
	}
	@GetMapping("63.go")
	public String go63() {
		return "admin/salesStatus/monthSealseStatus";
	}
	@GetMapping("64.go")
	public String go64() {
		return "admin/salesStatus/periodActivity";
	}
	@GetMapping("65.go")
	public String go65() {
		return "admin/salesStatus/productSalesStatus";
	}
	@GetMapping("66.go")
	public String go66() {
		return "admin/salesStatus/salesPersonalStatus";
	}
	//관리자 끝
	
	//STATUS 시작
	
	//STATUS 메인(기회현황)
	@GetMapping("700.go")
	public String go700() {
		return "status/OpportunityStatus/opportunityStatus";
	}
	//활동현황
	@GetMapping("67.go")
	public String go67() {
		return "status/ActivityStatus/statusActivityStatus";
	}
	//고객현황
	@GetMapping("68.go")
	public String go68() {
		return "status/clientStatus/customerStatus";
	}
	//계약현황
	@GetMapping("69.go")
	public String go69() {
		return "status/contractStatus/contractStatus";
	}
	//실적현황
	@GetMapping("70.go")
	public String go70() {
		return "status/OpportunityStatus/opportunityStatus";
	}
	
	@GetMapping("73.go")
	public String go73() {
		return "status/ActivityStatus/ActivityStatusData";
	}
	//STATUS 끝
	
	//기타 페이지 이동
	
	@PostMapping("71.go")
	public String go71() {
		return "common/jusoPopup";
	}
	@PostMapping("72.go")
	public String go72() {
		return "common/Sample";
	}
	
}
