package com.kh.sm.lead.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kh.sm.lead.model.service.LeadService;
import com.kh.sm.lead.model.vo.Lead;
import com.kh.sm.lead.model.vo.LeadContactHistory;
import com.kh.sm.lead.model.vo.Search;

@Controller
public class LeadController {
	
	@Autowired
	private LeadService ls;
	
	@GetMapping("goInsert.le")
	public String goInsert() {
		return "sales/clientManagement/lead/LeadInsert";
	}
//	@GetMapping("goInsert.le")
//	public String goInsert() {
//		
//		return "sales/clientManagement/lead/LeadInsert";
//	}
	
	@PostMapping("insert.le")
	public String insertLead(Model model, Lead lead) {
		
		System.out.println("lead :" + lead);
		String address = lead.getZipNo() + "/" + lead.getRoadAddrPart1() + "/" + lead.getAddrDetail();
		lead.setAddress(address);
		int result = ls.insertLead(lead);
		
		
		return "redirect:/selList.le";	
		
	}
	@GetMapping("selList.le")
	public String selListLead(Model model) {
		
		List<Lead> list = ls.selListLead();
		model.addAttribute("leadList", list);
		
		
		return "sales/clientManagement/lead/LeadList2";
	}
	
	@GetMapping("selOne.le")
	public String selOneLead(Model model, Lead lead) {
		
		Lead leadOne = ls.selOneLead(lead);
		List<LeadContactHistory> list = ls.selectConHis(lead.getLeadId());
		System.out.println("leadOne ;" + leadOne);
		model.addAttribute("lead", leadOne);
		model.addAttribute("list", list);
		
		
		return "sales/clientManagement/lead/leadDetail";
	}
	@PostMapping("insertConHis.le")
	public String insertHis(LeadContactHistory lch,ModelAndView mv) {
		System.out.println("lch :" + lch);
		int result = ls.insertConHis(lch);
		System.out.println("result : " + result);
		System.out.println("leadId :" + lch.getLeadId());
		List<LeadContactHistory> list = ls.selectConHis(lch.getLeadId());
		System.out.println("list : " + list);
		
		return "redirect:/selOne.le?leadId="+lch.getLeadId();
		
	}
	
	@GetMapping("search.le")
	public void searchLead(Search search, HttpServletRequest request, HttpServletResponse response) {
//		response.setCharacterEncoding("UTF-8");
//		Search search = new Search();
//		search.setName(request.getParameter("name"));
		System.out.println("search name : " + search.getLeadName());
		List<Lead> list = ls.searchLead(search);
//		model.addAttribute("leadList", list);
		System.out.println("list : " + list);
//		mv.addObject("list", list);
//		mv.setViewName("jsonView");
		
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
