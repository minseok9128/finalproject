package com.kh.sm.lead.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.lead.model.vo.Lead;
import com.kh.sm.lead.model.vo.LeadContactHistory;
import com.kh.sm.lead.model.vo.Search;


public interface LeadDao {

	int insertLead(SqlSessionTemplate sqlSession, Lead lead);

	List<Lead> selListLead(SqlSessionTemplate sqlSession);

	Lead selOneLead(SqlSessionTemplate sqlSession, Lead lead);

	int insertConHis(SqlSessionTemplate sqlSession, LeadContactHistory lch);

	List<LeadContactHistory> selectConHis(SqlSessionTemplate sqlSession, int leadId);

	List<Lead> selListLead(SqlSessionTemplate sqlSession, Search search);

}
