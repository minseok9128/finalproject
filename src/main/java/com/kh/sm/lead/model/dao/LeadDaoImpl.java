package com.kh.sm.lead.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.lead.model.vo.Lead;
import com.kh.sm.lead.model.vo.LeadContactHistory;
import com.kh.sm.lead.model.vo.Search;

@Repository
public class LeadDaoImpl implements LeadDao {
	
	
	
	@Override
	public int insertLead(SqlSessionTemplate sqlSession, Lead lead) {
		
		return sqlSession.insert("Lead.insertLead",lead);
	}

	@Override
	public List<Lead> selListLead(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Lead.selectList");
	}

	@Override
	public Lead selOneLead(SqlSessionTemplate sqlSession, Lead lead) {
		return sqlSession.selectOne("Lead.selectOne", lead.getLeadId());
	}

	@Override
	public int insertConHis(SqlSessionTemplate sqlSession, LeadContactHistory lch) {
		return sqlSession.insert("Lead.insertConHis",lch);
	}

	@Override
	public List<LeadContactHistory> selectConHis(SqlSessionTemplate sqlSession, int leadId) {
		return sqlSession.selectList("Lead.selectConHis", leadId);
	}

	@Override
	public List<Lead> selListLead(SqlSessionTemplate sqlSession, Search search) {
		return sqlSession.selectList("Lead.searchList", search);
	}
}
