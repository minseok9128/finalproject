package com.kh.sm.lead.model.service;

import java.util.List;

import com.kh.sm.lead.model.vo.Lead;
import com.kh.sm.lead.model.vo.LeadContactHistory;
import com.kh.sm.lead.model.vo.Search;

public interface LeadService {

	int insertLead(Lead lead);

	List<Lead> selListLead();

	Lead selOneLead(Lead lead);

	int insertConHis(LeadContactHistory lch);

	List<LeadContactHistory> selectConHis(int leadId);

	List<Lead> searchLead(Search search);

}
