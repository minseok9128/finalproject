package com.kh.sm.lead.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.lead.model.dao.LeadDao;
import com.kh.sm.lead.model.vo.Lead;
import com.kh.sm.lead.model.vo.LeadContactHistory;
import com.kh.sm.lead.model.vo.Search;

@Service
public class LeadServiceImpl implements LeadService {
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private LeadDao ld;
	
	@Override
	public int insertLead(Lead lead) {
		
		return ld.insertLead(sqlSession, lead);
	}

	@Override
	public List<Lead> selListLead() {
		
		return ld.selListLead(sqlSession);
	}

	@Override
	public Lead selOneLead(Lead lead) {
		return ld.selOneLead(sqlSession, lead);
	}

	@Override
	public int insertConHis(LeadContactHistory lch) {
		
		return ld.insertConHis(sqlSession, lch);
	}

	@Override
	public List<LeadContactHistory> selectConHis(int leadId) {
		return ld.selectConHis(sqlSession, leadId);
	}

	@Override
	public List<Lead> searchLead(Search search) {
		return ld.selListLead(sqlSession, search);
	}
}

