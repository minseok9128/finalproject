package com.kh.sm.lead.model.vo;

import java.sql.Date;
import java.util.ArrayList;

import com.kh.sm.contract.model.vo.Attachment;

public class Lead implements java.io.Serializable {
	
	private int leadId;
	private String userName;
	private String company;
	private String department;
	private String job;
	private int contCategory;
	private int contStatus;
	private String rank; 
	private String phone;
	private String tel;
	private String email;
	private String fax;
	private String address;
	private String zipNo;
	private String roadAddrPart1;
	private String addrDetail;
	private String remark;
	private Date enrollDate;
	private Date modifyDate;
	private int empNo;
	private ArrayList<Attachment> attList;
	
	public Lead() {}

	public Lead(int leadId, String userName, String company, String department, String job, int contCategory,
			int contStatus, String rank, String phone, String tel, String email, String fax, String address,
			String zipNo, String roadAddrPart1, String addrDetail, String remark, Date enrollDate, Date modifyDate,
			int empNo, ArrayList<Attachment> attList) {
		super();
		this.leadId = leadId;
		this.userName = userName;
		this.company = company;
		this.department = department;
		this.job = job;
		this.contCategory = contCategory;
		this.contStatus = contStatus;
		this.rank = rank;
		this.phone = phone;
		this.tel = tel;
		this.email = email;
		this.fax = fax;
		this.address = address;
		this.zipNo = zipNo;
		this.roadAddrPart1 = roadAddrPart1;
		this.addrDetail = addrDetail;
		this.remark = remark;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.empNo = empNo;
		this.attList = attList;
	}

	public int getLeadId() {
		return leadId;
	}

	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public int getContCategory() {
		return contCategory;
	}

	public void setContCategory(int contCategory) {
		this.contCategory = contCategory;
	}

	public int getContStatus() {
		return contStatus;
	}

	public void setContStatus(int contStatus) {
		this.contStatus = contStatus;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipNo() {
		return zipNo;
	}

	public void setZipNo(String zipNo) {
		this.zipNo = zipNo;
	}

	public String getRoadAddrPart1() {
		return roadAddrPart1;
	}

	public void setRoadAddrPart1(String roadAddrPart1) {
		this.roadAddrPart1 = roadAddrPart1;
	}

	public String getAddrDetail() {
		return addrDetail;
	}

	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public ArrayList<Attachment> getAttList() {
		return attList;
	}

	public void setAttList(ArrayList<Attachment> attList) {
		this.attList = attList;
	}

	@Override
	public String toString() {
		return "Lead [leadId=" + leadId + ", userName=" + userName + ", company=" + company + ", department="
				+ department + ", job=" + job + ", contCategory=" + contCategory + ", contStatus=" + contStatus
				+ ", rank=" + rank + ", phone=" + phone + ", tel=" + tel + ", email=" + email + ", fax=" + fax
				+ ", address=" + address + ", zipNo=" + zipNo + ", roadAddrPart1=" + roadAddrPart1 + ", addrDetail="
				+ addrDetail + ", remark=" + remark + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate
				+ ", empNo=" + empNo + ", attList=" + attList + "]";
	}

		
}
