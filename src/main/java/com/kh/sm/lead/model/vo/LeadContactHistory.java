package com.kh.sm.lead.model.vo;

import java.sql.Date;

public class LeadContactHistory {
	private int leadContHisId;
	private int leadId;
	private Date contactDate;
	private  int category;
	private String contactContent;
	
	public LeadContactHistory() {}

	public LeadContactHistory(int leadContHisId, int leadId, Date contactDate, int category, String contactContent) {
		super();
		this.leadContHisId = leadContHisId;
		this.leadId = leadId;
		this.contactDate = contactDate;
		this.category = category;
		this.contactContent = contactContent;
	}

	public int getLeadContHisId() {
		return leadContHisId;
	}

	public void setLeadContHisId(int leadContHisId) {
		this.leadContHisId = leadContHisId;
	}

	public int getLeadId() {
		return leadId;
	}

	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	public Date getContactDate() {
		return contactDate;
	}

	public void setContactDate(Date contactDate) {
		this.contactDate = contactDate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getContactContent() {
		return contactContent;
	}

	public void setContactContent(String contactContent) {
		this.contactContent = contactContent;
	}

	@Override
	public String toString() {
		return "LeadContactHistory [leadContHisId=" + leadContHisId + ", leadId=" + leadId + ", contactDate="
				+ contactDate + ", category=" + category + ", contactContent=" + contactContent + "]";
	}
	
	

}
