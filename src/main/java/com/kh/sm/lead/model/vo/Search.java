package com.kh.sm.lead.model.vo;

public class Search {
	private int status;
	private String leadName;
	
	public Search() {}

	public Search(int status, String leadName) {
		super();
		this.status = status;
		this.leadName = leadName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getLeadName() {
		return leadName;
	}

	public void setLeadName(String leadName) {
		this.leadName = leadName;
	}

	@Override
	public String toString() {
		return "Search [status=" + status + ", leadName=" + leadName + "]";
	}

	
	
}
