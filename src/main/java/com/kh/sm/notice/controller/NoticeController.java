package com.kh.sm.notice.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.kh.sm.common.Attachment;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.notice.model.service.SalesNoticeService;
import com.kh.sm.notice.model.vo.Notice;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.notice.model.vo.ajaxNotice;

@Controller
public class NoticeController {
	@Autowired
	private final SalesNoticeService service;
	
	public NoticeController(SalesNoticeService service) {
		this.service = service;
	}

	@RequestMapping(value="showNoticeList.sn", method=RequestMethod.GET) 
	public ModelAndView showNoticeList(@ModelAttribute Notice notice, HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		
		List<Notice> noticeList = service.selectNoticeList();
		int count = service.selectNoticeCount();
		
		ModelAndView mv = new ModelAndView("sales/salesNotice/salesNotice");
		mv.addObject("noticeList", noticeList);
		mv.addObject("count", count);
		
		return mv;
	}
	
	@RequestMapping(value="showNoticeDetail.sn", method=RequestMethod.GET) 
	public ModelAndView showNoticeDetail(Model model, @RequestParam("notNum") int notNum) {
		
		ModelAndView mv = new ModelAndView("sales/salesNotice/salesNoticeDetail");
		
		List<Notice> noticeContent = service.selectNoticeContent(notNum);
		List<Reply> ReplyList = service.selectReplyList(notNum);
		int replyCount = service.selectReplyCount(notNum);
		int noticeClickCount = service.insertNoticeClickCount(notNum);
		List<Attachment> AttchList = service.selectDetailAttachList(notNum);
		
		mv.addObject("noticeContent", noticeContent);
		mv.addObject("ReplyList", ReplyList);
		mv.addObject("replyCount", replyCount);
		mv.addObject("noticeClickCount", noticeClickCount);
		if(AttchList != null) {
			mv.addObject("AttchList", AttchList);
		}
		return mv;
	}
	
	@RequestMapping(value="showInsertNotice.sn", method=RequestMethod.GET) 
	public String ShowinsertNotice() {
		
		return "sales/salesNotice/insertSalesNotice";
	}
	
	@PostMapping(value="showUpdateNotice.sn") 
	public ModelAndView showUpdateNotice(HttpServletRequest request, MultipartHttpServletRequest multiRequest) {
		Notice notice = new Notice();
		
		notice.setNotId(Integer.parseInt(request.getParameter("notId")));
		notice.setNotTitle(request.getParameter("notTitle"));
		notice.setEssent(request.getParameter("essent"));
		notice.setNotContent(request.getParameter("notContent"));
		notice.setEmpNo(Integer.parseInt(request.getParameter("empNo")));
		notice.setEmpName(request.getParameter("empName"));
		
		List<Attachment> attachList = null;
		if(multiRequest.getFiles("files") != null) {
			attachList = service.selectAttachList(notice);
		}
		
		ModelAndView mv = new ModelAndView("sales/salesNotice/updateSalesNotice");
		mv.addObject("notice", notice);
		if(attachList != null) {
			mv.addObject("attachList", attachList);
		}
		
		return mv;
	}
	
	@PostMapping("insertNotice.sn")
	public String insertNotice(Model model, Notice notice, HttpServletRequest request, MultipartHttpServletRequest multiRequest) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		
		notice.setEmpNo(empNo);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		
		List<MultipartFile> fileList = multiRequest.getFiles("files");
		
		ArrayList<String> changeNames = new ArrayList<>();
		
		for(int i = 0; i < fileList.size(); i ++) {
			String originFileName = fileList.get(i).getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonUtils.getRandomString();
			
			try {
				fileList.get(i).transferTo(new File(filePath + "\\" + changeName + ext));
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
			
			changeNames.add(changeName);
		}
		
		int result1 = service.insertNotice(notice);
		int result2 = 0;
		int selectNotId = 0;
		
		if(result1 > 0) {
			selectNotId = service.selectNotId();
			
			if(fileList != null && selectNotId > 0) {
				result2 = service.insertAttach(fileList, changeNames, filePath, selectNotId);
			}
		}
		return "redirect:showNoticeList.sn";

	}
	
	@PostMapping("replyInsert.sn")
	public String replyInsert(Model model, HttpServletRequest request) {
		Reply reply = new Reply();
		
		String content = request.getParameter("content");
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int writer = emp.getEmpNo();
		int categoryId = Integer.parseInt(request.getParameter("categoryId"));
		
		reply.setContent(content);
		reply.setWriter(writer);
		reply.setCategoryId(categoryId);
		
		int result = service.insertReply(reply);
		
		if(result > 0 ) {
			return "redirect:showNoticeDetail.sn?notNum="+categoryId;
		}
		else {
			return "common/errorPage";
		}
	}
	
	@GetMapping("delReply.sn")
	public String replyDeleteReply(Model model, @RequestParam("replyId") int replyId, @RequestParam("notId") int notId) {
		Reply reply = new Reply();
		
		reply.setReplyId(replyId);
		
		int result = service.deleteReply(reply);
		
		if(result > 0) {
			return "redirect:showNoticeDetail.sn?notNum="+notId;
		}else {
			return "common/errorPage";
		}
	}
	
	@PostMapping("updateNoticeContent.sn")
	public ModelAndView updateNoticeContent(Model model, HttpServletRequest request, MultipartHttpServletRequest multiRequest) {
		
		Notice notice = new Notice();
		
		notice.setNotTitle(request.getParameter("notTitle"));
		notice.setEmpNo(Integer.parseInt(request.getParameter("empNo")));
		notice.setEssent(request.getParameter("essent"));
		notice.setNotContent(request.getParameter("notContent"));
		notice.setNotId(Integer.parseInt(request.getParameter("notId")));
		int notId = Integer.parseInt(request.getParameter("notId"));
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		
		List<MultipartFile> fileList = multiRequest.getFiles("files");
		
		ArrayList<String> changeNames = new ArrayList<>();
		
		for(int i = 0; i < fileList.size(); i ++) {
			String originFileName = fileList.get(i).getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonUtils.getRandomString();
			
			try {
				fileList.get(i).transferTo(new File(filePath + "\\" + changeName + ext));
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
			
			changeNames.add(changeName);
		}
		
		int result = service.updateNoticeContent(notice);
		int result1 = service.insertAttach(fileList, changeNames, filePath, notId);
		
			
		ModelAndView mv = new ModelAndView("redirect:showNoticeList.sn");
		
		return mv;
	}
	
	@PostMapping("searchNoticeList.sn")
	public ModelAndView searchNoticeList(ModelAndView mv,  @RequestParam("titleContent") String titleContent, @RequestParam("empName") String empName) {
		
		String content = titleContent;
		String notTitle = titleContent;
		
		List<ajaxNotice> searchNoticeList = new ArrayList<ajaxNotice>();
		List<ajaxNotice> containContentNoticeList = new ArrayList<ajaxNotice>();
		List<ajaxNotice> containEmpNameNoticeList = new ArrayList<ajaxNotice>();
		Notice notice = new Notice();
		
		if(content != "" && empName != "") {
				notice.setNotContent(content);
				notice.setNotTitle(notTitle);
				notice.setEmpName(empName);
				
				searchNoticeList = service.searchNoticeList(notice);
				mv.addObject("searchNoticeList", searchNoticeList);
		}
		
		if(content != "" && empName == "") {
			notice.setNotContent(content);
			notice.setNotTitle(notTitle);
			
			containContentNoticeList = service.containContentNoticeList(content);
			mv.addObject("containContentNoticeList", containContentNoticeList);
		}
		
		if(content == "" && empName != ""){
			notice.setEmpName(empName);
			
			containEmpNameNoticeList = service.containEmpNameNoticeList(empName);
			mv.addObject("containEmpNameNoticeList", containEmpNameNoticeList);
		}
		
		int count = service.ajaxListCount(notice);
		mv.addObject("count", count);
		
		mv.setViewName("jsonView");
		
		return mv;
	}
}
