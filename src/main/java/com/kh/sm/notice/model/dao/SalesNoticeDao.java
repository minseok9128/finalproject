package com.kh.sm.notice.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.common.Attachment;
import com.kh.sm.notice.model.vo.Notice;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.notice.model.vo.ajaxNotice;

public interface SalesNoticeDao {

	int insertNotice(SqlSessionTemplate sqlSession, Notice notice);

	List<Notice> selectNoticeList(SqlSessionTemplate sqlSession);

	int selectNoticeCount(SqlSessionTemplate sqlSession);

	List<Notice> selectNoticeContent(SqlSessionTemplate sqlSession, int notNum);

	int insertReply(SqlSessionTemplate sqlSession, Reply reply);

	List<Reply> selectReplyList(SqlSessionTemplate sqlSession, int notNum);

	int selectReplyCount(SqlSessionTemplate sqlSession, int notNum);

	int insertNoticeClickCount(SqlSessionTemplate sqlSession, int notNum);

	int insertAttach(SqlSessionTemplate sqlSession, Attachment attach);

	int deleteReply(SqlSessionTemplate sqlSession, Reply reply);

	int updateNoticeContent(SqlSessionTemplate sqlSession, Notice notice);

	List<Attachment> selectAttachList(SqlSessionTemplate sqlSession, Notice notice);

	List<Attachment> selectDetailAttachList(SqlSessionTemplate sqlSession, int notNum);

	List<ajaxNotice> searchNoticeList(SqlSessionTemplate sqlSession, Notice notice);

	List<ajaxNotice> containContentNoticeList(SqlSessionTemplate sqlSession, String content);

	List<ajaxNotice> containEmpNameNoticeList(SqlSessionTemplate sqlSession, String empName);

	int ajaxListCount(SqlSessionTemplate sqlSession, Notice notice);

	int selectNoticeId(SqlSessionTemplate sqlSession);

}