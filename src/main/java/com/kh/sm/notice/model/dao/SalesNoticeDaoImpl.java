package com.kh.sm.notice.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.common.Attachment;
import com.kh.sm.notice.model.vo.Notice;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.notice.model.vo.ajaxNotice;

@Repository
public class SalesNoticeDaoImpl implements SalesNoticeDao{

	@Override
	public int insertNotice(SqlSessionTemplate sqlSession, Notice notice) {
		
		return sqlSession.insert("Notice.insertNotice", notice);
	}

	@Override
	public List<Notice> selectNoticeList(SqlSessionTemplate sqlSession) {
		
		List<Notice> list = sqlSession.selectList("Notice.selectNoticeList");
		
		return list;
	}

	@Override
	public int selectNoticeCount(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectOne("Notice.selectNoticeCount");
	}

	@Override
	public List<Notice> selectNoticeContent(SqlSessionTemplate sqlSession, int notNum) {
		
		List<Notice> noticeContent = sqlSession.selectList("Notice.selectNoticeContent", notNum);
		
		return noticeContent;
	}

	@Override
	public int insertReply(SqlSessionTemplate sqlSession, Reply reply) {
		
		return sqlSession.insert("Notice.insertReply", reply);
	}

	@Override
	public List<Reply> selectReplyList(SqlSessionTemplate sqlSession, int notNum) {
		
		List<Reply> selectReplyList = sqlSession.selectList("Notice.selectReplyList",notNum);
		
		return selectReplyList;
	}

	@Override
	public int selectReplyCount(SqlSessionTemplate sqlSession, int notNum) {
		
		int replyCount = sqlSession.selectOne("Notice.selectReplyCount", notNum);
		
		return replyCount;
	}

	@Override
	public int insertNoticeClickCount(SqlSessionTemplate sqlSession, int notNum) {
		
		return sqlSession.update("Notice.insertNoticeClickCount", notNum);
	}

	@Override
	public int insertAttach(SqlSessionTemplate sqlSession, Attachment attach) {
		
		return sqlSession.insert("Notice.insertAttach", attach);
	}

	@Override
	public int deleteReply(SqlSessionTemplate sqlSession, Reply reply) {
		
		return sqlSession.delete("Notice.deleteReply", reply);
	}

	@Override
	public int updateNoticeContent(SqlSessionTemplate sqlSession, Notice notice) {
		
		return sqlSession.update("Notice.updateNoticeContent", notice);
	}

	@Override
	public List<Attachment> selectAttachList(SqlSessionTemplate sqlSession, Notice notice) {
		
		List<Attachment> list = sqlSession.selectList("Notice.selectAttachList", notice);
		
		return list;
	}

	@Override
	public List<Attachment> selectDetailAttachList(SqlSessionTemplate sqlSession, int notNum) {
		
		List<Attachment> selectDetailAttachList = sqlSession.selectList("Notice.selectDetailAttachList", notNum);
		
		return selectDetailAttachList;
	}

	@Override
	public List<ajaxNotice> searchNoticeList(SqlSessionTemplate sqlSession, Notice notice) {
		
		List<ajaxNotice> searchNoticeList = sqlSession.selectList("Notice.searchNoticeList", notice);
		
		return searchNoticeList;
	}

	@Override
	public List<ajaxNotice> containContentNoticeList(SqlSessionTemplate sqlSession, String content) {
		
		List<ajaxNotice> containContentNoticeList = sqlSession.selectList("Notice.containContentNoticeList", content);
				
		return containContentNoticeList;
	}

	@Override
	public List<ajaxNotice> containEmpNameNoticeList(SqlSessionTemplate sqlSession, String empName) {
		
		List<ajaxNotice> containEmpNameNoticeList = sqlSession.selectList("Notice.containEmpNameNoticeList", empName);
		
		return containEmpNameNoticeList;
	}

	@Override
	public int ajaxListCount(SqlSessionTemplate sqlSession, Notice notice) {
		
		return sqlSession.selectOne("Notice.ajaxListCount", notice);
	}

	@Override
	public int selectNoticeId(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectOne("Notice.selectNoticeId");
	}
	
}
