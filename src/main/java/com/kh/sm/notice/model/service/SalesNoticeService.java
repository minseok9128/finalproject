package com.kh.sm.notice.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.notice.model.vo.Notice;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.notice.model.vo.ajaxNotice;

public interface SalesNoticeService {
	
	int insertNotice(Notice notice);

	List<Notice> selectNoticeList();

	int selectNoticeCount();

	List<Notice> selectNoticeContent(int notNum);

	int insertReply(Reply reply);

	List<Reply> selectReplyList(int notNum);

	int selectReplyCount(int notNum);

	int insertAttach(List<MultipartFile> fileList, ArrayList<String> changeNames, String filePath, int selectNotId);
	
	int insertNoticeClickCount(int notNum);

	int deleteReply(Reply reply);

	int updateNoticeContent(Notice notice);

	List<Attachment> selectAttachList(Notice notice);

	List<Attachment> selectDetailAttachList(int notNum);

	List<ajaxNotice> searchNoticeList(Notice notice);

	List<ajaxNotice> containContentNoticeList(String content);

	List<ajaxNotice> containEmpNameNoticeList(String empName);

	int ajaxListCount(Notice notice);

	int selectNotId();

}
