package com.kh.sm.notice.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.notice.model.dao.SalesNoticeDao;
import com.kh.sm.notice.model.vo.Notice;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.notice.model.vo.ajaxNotice;

@Service
public class SalesNoticeServiceImpl implements SalesNoticeService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private SalesNoticeDao nd;

	@Override
	public int insertNotice(Notice notice) {
		
		return nd.insertNotice(sqlSession, notice);
	}

	@Override
	public List<Notice> selectNoticeList() {
		List<Notice> noticeList = null;
		
		noticeList = nd.selectNoticeList(sqlSession);
		
		return noticeList;
	}

	@Override
	public int selectNoticeCount() {
		
		int count = nd.selectNoticeCount(sqlSession);
		return count;
	}

	@Override
	public List<Notice> selectNoticeContent(int notNum) {
		
		return nd.selectNoticeContent(sqlSession, notNum);
	}

	@Override
	public int insertReply(Reply reply) {
		
		return nd.insertReply(sqlSession, reply);
	}

	@Override
	public List<Reply> selectReplyList(int notNum) {
		
		return nd.selectReplyList(sqlSession, notNum);
	}

	@Override
	public int selectReplyCount(int notNum) {
		
		return nd.selectReplyCount(sqlSession, notNum);
	}

	
	@Override
	public int insertAttach(List<MultipartFile> fileList, ArrayList<String> changeNames, String filePath, int selectNotId) {
		int result = 0;
		Attachment attach = null;
		
		for(int i = 0; i < fileList.size(); i++) {
			attach = new Attachment();
			attach.setCategory(8);
			attach.setChangeName(changeNames.get(i));
			attach.setFilePath(filePath);
			attach.setOriginName(fileList.get(i).getOriginalFilename());
			attach.setDelYn("N");
			attach.setCategoryId(selectNotId);
			result += nd.insertAttach(sqlSession, attach);
		}
		
		return result;
	}
	
	@Override
	public int insertNoticeClickCount(int notNum) {
		
		return nd.insertNoticeClickCount(sqlSession, notNum);
	}

	@Override
	public int deleteReply(Reply reply) {
		
		return nd.deleteReply(sqlSession, reply);
	}

	@Override
	public int updateNoticeContent(Notice notice) {
		
		return nd.updateNoticeContent(sqlSession, notice);
	}

	@Override
	public List<Attachment> selectAttachList(Notice notice) {
		
		return nd.selectAttachList(sqlSession, notice);
	}

	@Override
	public List<Attachment> selectDetailAttachList(int notNum) {
		
		return nd.selectDetailAttachList(sqlSession, notNum);
	}

	@Override
	public List<ajaxNotice> searchNoticeList(Notice notice) {
		
		return nd.searchNoticeList(sqlSession, notice);
	}

	@Override
	public List<ajaxNotice> containContentNoticeList(String content) {
		
		return nd.containContentNoticeList(sqlSession, content);
	}

	@Override
	public List<ajaxNotice> containEmpNameNoticeList(String empName) {
		
		return nd.containEmpNameNoticeList(sqlSession, empName);
	}

	@Override
	public int ajaxListCount(Notice notice) {
		
		return nd.ajaxListCount(sqlSession, notice);
	}

	@Override
	public int selectNotId() {
		
		return nd.selectNoticeId(sqlSession);
	}

}
