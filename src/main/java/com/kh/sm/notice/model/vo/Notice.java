package com.kh.sm.notice.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Notice implements Serializable{
	private int notId;
	private String notTitle;
	private String essent;
	private String notContent;
	private int count;
	private Date enrollDate;
	private Date modifyDate;
	private int empNo;
	private String empName;
	private String delYN;
	
	public Notice() {}

	public Notice(int notId, String notTitle, String essent, String notContent, int count, Date enrollDate,
			Date modifyDate, int empNo, String empName, String delYN) {
		super();
		this.notId = notId;
		this.notTitle = notTitle;
		this.essent = essent;
		this.notContent = notContent;
		this.count = count;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.empNo = empNo;
		this.empName = empName;
		this.delYN = delYN;
	}

	public int getNotId() {
		return notId;
	}

	public void setNotId(int notId) {
		this.notId = notId;
	}

	public String getNotTitle() {
		return notTitle;
	}

	public void setNotTitle(String notTitle) {
		this.notTitle = notTitle;
	}

	public String getEssent() {
		return essent;
	}

	public void setEssent(String essent) {
		this.essent = essent;
	}

	public String getNotContent() {
		return notContent;
	}

	public void setNotContent(String notContent) {
		this.notContent = notContent;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	@Override
	public String toString() {
		return "Notice [notId=" + notId + ", notTitle=" + notTitle + ", essent=" + essent + ", notContent=" + notContent
				+ ", count=" + count + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate + ", empNo=" + empNo
				+ ", empName=" + empName + ", delYN=" + delYN + "]";
	}

}