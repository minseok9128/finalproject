package com.kh.sm.notice.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Reply implements Serializable{
	private int replyId;
	private String content;
	private int writer;
	private String empName;
	private Date enrollDate;
	private Date modifyDate;
	private String delYN;
	private int category;
	private int categoryId;
	
	public Reply() {}

	public Reply(int replyId, String content, int writer, String empName, Date enrollDate, Date modifyDate,
			String delYN, int category, int categoryId) {
		super();
		this.replyId = replyId;
		this.content = content;
		this.writer = writer;
		this.empName = empName;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.delYN = delYN;
		this.category = category;
		this.categoryId = categoryId;
	}

	public int getReplyId() {
		return replyId;
	}

	public void setReplyId(int replyId) {
		this.replyId = replyId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getWriter() {
		return writer;
	}

	public void setWriter(int writer) {
		this.writer = writer;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "Reply [replyId=" + replyId + ", content=" + content + ", writer=" + writer + ", empName=" + empName
				+ ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate + ", delYN=" + delYN + ", category="
				+ category + ", categoryId=" + categoryId + "]";
	}

}