package com.kh.sm.offer.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.kh.sm.common.Attachment;
import com.kh.sm.common.SaveFile;
import com.kh.sm.offer.model.exception.DeleteFileException;
import com.kh.sm.offer.model.exception.InsertOfferEnrollmentException;
import com.kh.sm.offer.model.service.OfferService;
import com.kh.sm.offer.model.vo.Offer;


@Controller
public class OfferController {

	@Autowired
	private final OfferService os;

	private final SaveFile sf;


	public OfferController(OfferService os, SaveFile sf) {
		this.os = os;
		this.sf = sf;
	}
	@PostMapping("deletAttachment.of")
	public void deletAttachment(HttpServletResponse response, @RequestParam("list")ArrayList<Integer> fileList) {

		if(fileList.get(0) > 0) {
			try {
				System.out.println("실행함");
				int deletFile = os.deletFile(fileList.get(0));

				response.setContentType("application/json");
				response.setCharacterEncoding("utf-8");
			} catch (DeleteFileException e1) {
				try {
					new Gson().toJson(e1.getMessage(), response.getWriter());
				} catch (JsonIOException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				e1.printStackTrace();
			} catch (JsonIOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
	}

	@PostMapping("insertOfferEnrollment.of")
	public String insertOfferEnrollment(Model model, Offer of, 
			MultipartHttpServletRequest multiRequest,	
			@RequestParam("reday") String reday,
			@RequestParam("suday") String suday,
			@RequestParam("processId") int processId,
			Model mo,
			HttpServletRequest request) {


		if(!reday.equals("")) {
			of.setRequestDate(Date.valueOf(reday));
		} 
		if(!suday.equals("")) {
			of.setSubmitDate(Date.valueOf(suday));
		} 

		//제안 요청서
		List<MultipartFile> requestFile = multiRequest.getFiles("requestFile");
		List<MultipartFile> reportFile = multiRequest.getFiles("reportFile");

		try {
			int result = os.insertOfferEnrollment(of); 
			os.updateProcess(processId, of.getOppId());
			for(int i=0; i<requestFile.size(); i++) {

				Attachment ac = new Attachment();

				ac.setCategoryId(of.getPpsId());
				ac.setCategory(11);
				sf.saveFile(requestFile.get(i), request, ac);
				int requestFileInsert = os.insertAttachment(ac);
			}
			for(int i=0; i<reportFile.size(); i++) {
				Attachment ac = new Attachment();
				ac.setCategoryId(of.getPpsId());
				ac.setCategory(5);
				sf.saveFile(reportFile.get(i), request, ac);
				int reportFileInsert = os.insertAttachment(ac);
			}

			mo.addAttribute("msg", "등록이 완료되었습니다.");
			mo.addAttribute("aa", 1);
			return "sales/salesManagement/successPage";

		} catch (InsertOfferEnrollmentException e) {
			//에러페이지
			request.setAttribute("msg", e.getMessage());
			return "";
		}



	};

	//제안 메인페이지 
	@GetMapping("offerMain.of")
	public String selectOfferMain(HttpServletRequest request, @RequestParam("memberId") int memberId) {

		List<Offer> list = os.selectOfferMain(memberId);
		int offerCount = os.selectOfferCount(memberId);

		request.setAttribute("list", list);
		request.setAttribute("listCount", offerCount);
		return "sales/salesManagement/offerMain";
	}

	@GetMapping("offerDetail.of")
	public String offerDetail(@RequestParam("proposeId") int proposeId, HttpServletRequest request) {

		List<Offer> of = os.selectOfferDetail(proposeId);
		List<Attachment> at = os.selectAttachment(proposeId);
		ArrayList<Attachment> reportList = new ArrayList<Attachment>();
		ArrayList<Attachment> requestList = new ArrayList<Attachment>();

		if(at != null) {
			for(int i=0; i<at.size(); i++) {
				if(at.get(i).getCategory() == 5) {
					//report 제안서
					reportList.add(at.get(i));

				} else {
					//request 제안요청서
					requestList.add(at.get(i));
				}
			}
			request.setAttribute("reportList", reportList);
			request.setAttribute("requestList", requestList);
		}

		request.setAttribute("offerlist", of);
		return "sales/salesManagement/offerDetail";
	}
	@GetMapping("offerModify.of")
	public String ModifyOffer(@RequestParam("ppsId")int ppsId, HttpServletRequest request) {

		List<Offer> list = os.selectOfferDetail(ppsId);
		int ppsIdM = list.get(0).getPpsId();

		List<Attachment> at = os.selectAttachment(ppsId);
		ArrayList<Attachment> reportList = new ArrayList<Attachment>();
		ArrayList<Attachment> requestList = new ArrayList<Attachment>();

		if(at != null) {
			for(int i=0; i<at.size(); i++) {
				if(at.get(i).getCategory() == 5) {
					//report 제안서
					reportList.add(at.get(i));

				} else {
					//request 제안요청서
					requestList.add(at.get(i));
				}
			}
			request.setAttribute("reportList", reportList);
			request.setAttribute("requestList", requestList);
		}

		request.setAttribute("list", list);
		request.setAttribute("ppsId", ppsIdM);

		return "sales/salesManagement/offerEnrollment";
	}
	//제안 수정 
	@PostMapping("modifyOfferEnrollment.of")
	public String modifyOfferEnrollment(Model mo, Offer of, 
			MultipartHttpServletRequest multiRequest,	
			@RequestParam("reday") String reday,
			@RequestParam("suday") String suday,
			@RequestParam("processId") int processId,
			HttpServletRequest request) {

		List<MultipartFile> requestFile = multiRequest.getFiles("requestFile");
		List<MultipartFile> reportFile = multiRequest.getFiles("reportFile");

		if(!reday.equals("")) {
			of.setRequestDate(Date.valueOf(reday));
		}
		of.setSubmitDate(Date.valueOf(suday));

		System.out.println(of);
		
		int updateOffer = os.updateOffer(of);
		
		for(int i=0; i<requestFile.size(); i++) {

			Attachment ac = new Attachment();

			ac.setCategoryId(of.getPpsId());
			ac.setCategory(11);
			sf.saveFile(requestFile.get(i), request, ac);
			int requestFileInsert = os.insertAttachment(ac);
		}
		for(int i=0; i<reportFile.size(); i++) {
			Attachment ac = new Attachment();
			ac.setCategoryId(of.getPpsId());
			ac.setCategory(5);
			sf.saveFile(reportFile.get(i), request, ac);
			int reportFileInsert = os.insertAttachment(ac);
		}
		
		mo.addAttribute("msg", "수정이 완료되었습니다.");
		mo.addAttribute("aa", 2);
		mo.addAttribute("Id", of.getPpsId());
		return "redirect:/showSuccessPage.of";
	}
	@PostMapping("offerMainscroll.of")
	public void offerMainscroll(ModelAndView mv, @RequestParam("lastCno") int lastCno, @RequestParam("memberId") int memberId, HttpServletResponse response) {
		
		List<Offer> list = os.selectOfferScroll(lastCno, memberId);
		
		
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			new Gson().toJson(list, response.getWriter());
		} catch (JsonIOException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@PostMapping("filter.of")
	public void offerFilter1_1(ModelAndView mv, HttpServletResponse response, @RequestParam("filter") int filter){
		
		List<Offer> list = os.selectOfferFilter(filter);
		System.out.println(list);
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			new Gson().toJson(list, response.getWriter());
		} catch (JsonIOException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("showSuccessPage.of")
	public String successPage(HttpServletRequest request, RedirectAttributes rttr, @RequestParam("msg")String msg, @RequestParam("aa")int aa, @RequestParam("Id")int Id) {
		
		  request.setAttribute("msg",msg); 
		  request.setAttribute("aa",aa); 
		  request.setAttribute("Id",Id);
		 return "sales/salesManagement/successPage";
	}

}
