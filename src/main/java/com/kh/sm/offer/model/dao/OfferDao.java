package com.kh.sm.offer.model.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import com.kh.sm.common.Attachment;
import com.kh.sm.offer.model.vo.Offer;

public interface OfferDao {

	int insertOfferEnrollment(Offer of, SqlSessionTemplate sqlSession);
	int insertAttachment(Attachment ac, SqlSessionTemplate sqlSession);
	List<Offer> selectOfferMain(int memberId,SqlSessionTemplate sqlSession);
	List<Offer> selectOfferDetail(int proposeId, SqlSessionTemplate sqlSession);
	List<Attachment> selectAttachment(int proposeId, SqlSessionTemplate sqlSession);
	int deletFile(Integer fileId, SqlSessionTemplate sqlSession);
	int updateOffer(Offer of, SqlSessionTemplate sqlSession);
	List<Offer> selectOfferScroll(int lastCno, int memberId, SqlSessionTemplate sqlSession);
	int selectOfferCount(int memberId, SqlSessionTemplate sqlSession);
	List<Offer> selectOfferFilter(int filter, SqlSessionTemplate sqlSession);
	void updateProcess(int processId, int oppId, SqlSessionTemplate sqlSession);

}
