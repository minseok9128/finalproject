package com.kh.sm.offer.model.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import com.kh.sm.common.Attachment;
import com.kh.sm.offer.model.vo.Offer;
import com.kh.sm.process.model.vo.OppProcess;


@Repository
public class OfferDaoImpl implements OfferDao{

	@Override
	public int insertOfferEnrollment(Offer of, SqlSessionTemplate sqlSession) {
		
		int result = sqlSession.insert("Offer.insertEnrollment", of);
			
		return result;
	}

	@Override
	public int insertAttachment(Attachment ac, SqlSessionTemplate sqlSession) {	
		System.out.println(ac);
		return sqlSession.insert("Offer.attInsert", ac);
	}

	@Override
	public List<Offer> selectOfferMain(int memberId,SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Offer.selectMain",memberId);
	}

	@Override
	public List<Offer> selectOfferDetail(int proposeId, SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Offer.selectOfferDetail", proposeId);
	}


	@Override
	public List<Attachment> selectAttachment(int proposeId, SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Offer.selectAttachment",proposeId);
	}

	@Override
	public int deletFile(Integer fileId, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Offer.deleFile", fileId);
	}

	@Override
	public int updateOffer(Offer of, SqlSessionTemplate sqlSession) {
		System.out.println(of);
		return sqlSession.update("Offer.updateOffer", of);
	}

	@Override
	public List<Offer> selectOfferScroll(int lastCno, int memberId, SqlSessionTemplate sqlSession) {
		RowBounds row = new RowBounds(lastCno+1, 20);
		return sqlSession.selectList("Offer.selectScroll", memberId ,row);
	}

	@Override
	public int selectOfferCount(int memberId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Offer.selectCount", memberId);
	}

	@Override
	public List<Offer> selectOfferFilter(int filter,SqlSessionTemplate sqlSession) {
		Offer of = new Offer();
		of.setPpsId(filter);
		return sqlSession.selectList("Offer.selectFilter", of);
	}

	@Override
	public void updateProcess(int processId, int oppId, SqlSessionTemplate sqlSession) {
		int prosu = sqlSession.selectOne("Offer.processSucces", processId);
		
		OppProcess op = new OppProcess();
		op.setProcessId(processId);
		op.setProcessSuccess(prosu);
		
		sqlSession.update("Offer.process", op);
		
	}

}
