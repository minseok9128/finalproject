package com.kh.sm.offer.model.exception;

public class DeleteFileException extends Exception {
	
	public DeleteFileException(String msg) {
		super(msg);
	}
}
