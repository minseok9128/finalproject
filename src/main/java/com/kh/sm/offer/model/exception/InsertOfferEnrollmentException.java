package com.kh.sm.offer.model.exception;

public class InsertOfferEnrollmentException extends Exception {

	public InsertOfferEnrollmentException(String msg) {
		super(msg);
	}
}
