package com.kh.sm.offer.model.service;

import java.util.List;

import com.kh.sm.common.Attachment;
import com.kh.sm.offer.model.exception.DeleteFileException;
import com.kh.sm.offer.model.exception.InsertOfferEnrollmentException;
import com.kh.sm.offer.model.vo.Offer;

public interface OfferService {

	int insertOfferEnrollment(Offer of) throws InsertOfferEnrollmentException;

	int insertAttachment(Attachment ac);

	List<Offer> selectOfferDetail(int proposeId);


	List<Attachment> selectAttachment(int proposeId);

	int deletFile(Integer integer) throws DeleteFileException;

	int updateOffer(Offer of);

	List<Offer> selectOfferScroll(int lastCno, int memberId);

	int selectOfferCount(int memberId);

	List<Offer> selectOfferFilter(int filter);

	List<Offer> selectOfferMain(int memberId);

	void updateProcess(int processId, int oppId);



}
