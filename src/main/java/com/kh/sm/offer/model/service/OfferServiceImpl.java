package com.kh.sm.offer.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kh.sm.common.Attachment;
import com.kh.sm.offer.model.dao.OfferDao;
import com.kh.sm.offer.model.exception.DeleteFileException;
import com.kh.sm.offer.model.exception.InsertOfferEnrollmentException;
import com.kh.sm.offer.model.vo.Offer;

@Service
public class OfferServiceImpl implements OfferService {
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private final OfferDao od;
	
	public OfferServiceImpl(OfferDao od) {
		this.od = od;
	}
	@Override
	public int insertOfferEnrollment(Offer of) throws InsertOfferEnrollmentException {
		
		int result = od.insertOfferEnrollment(of, sqlSession);
		
		if(result < 0 ) {
			throw new InsertOfferEnrollmentException("등록 실패");
		}
		
		return result;
	}
	@Override
	public int insertAttachment(Attachment ac) {
		
		int reuslt = od.insertAttachment(ac, sqlSession);
		
		return reuslt;
	}
	@Override
	public List<Offer> selectOfferMain(int memberId) {

		List<Offer> list = od.selectOfferMain(memberId,sqlSession);
		
		return list;
	}
	@Override
	public List<Offer> selectOfferDetail(int proposeId) {
		
		List<Offer> list = od.selectOfferDetail(proposeId, sqlSession);
		
		if(list == null) {
			
		}
		
		return list;
	}
	@Override
	public List<Attachment> selectAttachment(int proposeId) {
		
		List<Attachment> list = od.selectAttachment(proposeId, sqlSession);
		
		
		return list;
	}
	@Override
	public int deletFile(Integer fileId) throws DeleteFileException {
		int result = od.deletFile(fileId,sqlSession);
		
		if(result < 0) {
			throw new DeleteFileException("파일 조회 실패");
		}
		
		return result;
	}
	@Override
	public int updateOffer(Offer of) {
		int result = od.updateOffer(of,sqlSession);
		
		if(result < 0) {
			
		}
		
		return result;
	}
	@Override
	public List<Offer> selectOfferScroll(int lastCno, int memberId) {
		List<Offer> list = od.selectOfferScroll(lastCno, memberId, sqlSession);
		
		if(list == null) {
			
		}
		
		return list;
	}
	@Override
	public int selectOfferCount(int memberId) {
		int offerCount = od.selectOfferCount(memberId, sqlSession);
		return offerCount;
	}
	@Override
	public List<Offer> selectOfferFilter(int filter) {
		
		List<Offer> list = od.selectOfferFilter(filter,sqlSession);
		
		return list;
	}
	@Override
	public void updateProcess(int processId, int oppId) {
		od.updateProcess(processId, oppId, sqlSession);
		
	}



	
}
