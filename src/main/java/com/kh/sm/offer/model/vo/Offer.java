package com.kh.sm.offer.model.vo;

import java.sql.Date;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.Attachment;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.employee.model.vo.Employee;

public class Offer {
	private int ppsId; //제안ID
	private String ppsName; //제안명
	private int companyId; //고객사
	private int clientId; //고객
	private int oppId; //영업기회
	private String content; //내용
	private Date requestDate; //요청일
	private Date submitDate; //제출일
	private int empNo; // 담당자
	private String remark; //비고
	private Company company;
	private Client client;
	private Employee employee;
	public Offer() {}
	public Offer(int ppsId, String ppsName, int companyId, int clientId, int oppId, String content, Date requestDate,
			Date submitDate, int empNo, String remark, Company company, Client client, Employee employee) {
		super();
		this.ppsId = ppsId;
		this.ppsName = ppsName;
		this.companyId = companyId;
		this.clientId = clientId;
		this.oppId = oppId;
		this.content = content;
		this.requestDate = requestDate;
		this.submitDate = submitDate;
		this.empNo = empNo;
		this.remark = remark;
		this.company = company;
		this.client = client;
		this.employee = employee;
	}
	public int getPpsId() {
		return ppsId;
	}
	public void setPpsId(int ppsId) {
		this.ppsId = ppsId;
	}
	public String getPpsName() {
		return ppsName;
	}
	public void setPpsName(String ppsName) {
		this.ppsName = ppsName;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getOppId() {
		return oppId;
	}
	public void setOppId(int oppId) {
		this.oppId = oppId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Date getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	@Override
	public String toString() {
		return "Offer [ppsId=" + ppsId + ", ppsName=" + ppsName + ", companyId=" + companyId + ", clientId=" + clientId
				+ ", oppId=" + oppId + ", content=" + content + ", requestDate=" + requestDate + ", submitDate="
				+ submitDate + ", empNo=" + empNo + ", remark=" + remark + ", company=" + company + ", client=" + client
				+ ", employee=" + employee + "]";
	}

}
