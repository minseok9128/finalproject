package com.kh.sm.process.model.vo;

public class OppProcess {

	private int processId;
	private String process;
	private int processStage;
	private int processSuccess;
	
	public OppProcess() {}

	public OppProcess(int processId, String process, int processStage, int processSuccess) {
		super();
		this.processId = processId;
		this.process = process;
		this.processStage = processStage;
		this.processSuccess = processSuccess;
	}

	public int getProcessId() {
		return processId;
	}

	public void setProcessId(int processId) {
		this.processId = processId;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public int getProcessStage() {
		return processStage;
	}

	public void setProcessStage(int processStage) {
		this.processStage = processStage;
	}

	public int getProcessSuccess() {
		return processSuccess;
	}

	public void setProcessSuccess(int processSuccess) {
		this.processSuccess = processSuccess;
	}

	@Override
	public String toString() {
		return "OppProcess [processId=" + processId + ", process=" + process + ", processStage=" + processStage
				+ ", processSuccess=" + processSuccess + "]";
	}
	
	
	
}
