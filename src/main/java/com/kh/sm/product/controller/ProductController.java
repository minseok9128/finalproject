package com.kh.sm.product.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.EmployeeAjax;
import com.kh.sm.product.model.service.ProductService;
import com.kh.sm.product.model.vo.Product;
import com.kh.sm.product.model.vo.ProductAjax;
import com.kh.sm.product.model.vo.ProductHistory;

@Controller
public class ProductController {

	@Autowired
	private ProductService ps;

	public ProductController(ProductService ps) {

		this.ps = ps;
	}

	@PostMapping("insert.pr")
	public String insertProduct(ProductHistory proHis,Model model, Product pr,MultipartHttpServletRequest multiRequest,HttpServletRequest request) {
		
		System.out.println("제품 : " + pr);
		
		//경로
		String root = request.getSession().getServletContext().getRealPath("resources");
		//업로드 경로
		String filePath = root +"\\uploadFiles\\files";
		//객체에 담을 리스트
		List<MultipartFile> fList = multiRequest.getFiles("files");
		
		ArrayList<String> changeNames = new ArrayList<>();
		for(int i = 0; i < fList.size(); i++) {
			System.out.println("fListName : " + fList.get(i).getOriginalFilename());
			String originFileName = fList.get(i).getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonUtils.getRandomString();
			System.out.println("changeName : " + changeName);
			try {
				fList.get(i).transferTo(new File(filePath + "\\" + changeName + ext));
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			changeNames.add(changeName);
		}
		
		

			if(pr.getUnit().equals("BOX")) {
				pr.setUnit("BOX");
			}else if(pr.getUnit().equals("EA")) {
				pr.setUnit("EA");
			}else if(pr.getUnit().equals("SET")) {
				pr.setUnit("SET");
			}
			
			if(pr.getStandard().equals("pro1")) {
				pr.setStandard("EA");
			}else if(pr.getStandard().equals("pro2")) {
				pr.setStandard("PCS");
			}else if(pr.getStandard().equals("pro3")) {
				pr.setStandard("QTY");
			}else if(pr.getStandard().equals("pro4")) {
				pr.setStandard("SET");
			}
			
			
			
			proHis.setPrice(pr.getPrice());
			proHis.setEnroller(pr.getEmpNo());
			proHis.setPdtId(pr.getPdtId());
		
			
			if(request.getParameter("modifyer") != null) {
				
				if(Integer.parseInt(request.getParameter("modifyer")) == pr.getEmpNo()) {
					proHis.setModifyer(pr.getEmpNo());
				}else {
					
					proHis.setModifyer(Integer.parseInt(request.getParameter("modifyer")));
					
				}
			}else {
				
				proHis.setModifyer(pr.getEmpNo());
				
			}
				
			int result = ps.insertProduct(pr);
			int result2 = 0;
			int result3 = 0;
			
			if(result > 0) {
			
				
				 result2 = ps.insertAttach(fList, changeNames, filePath);
				
				 if(result2 >0) {
					 
					 result3 = ps.insertHisProduct(proHis);
					
					 if(result3>0) {
						 
						 model.addAttribute("msg","제품등록이 완료되었습니다.");
						model.addAttribute("aa",1);
						return "admin/salesManagement/productSuccessPage";
						 
					 }else {
							model.addAttribute("msg","제품 등록  이력 남기기 실패!");
							return "common/errorPage1"; 
					 }
						
					}
					else {
						model.addAttribute("msg","제품 첨부파일 등록 실패!");
						return "common/errorPage1";
					}
				 
			}	else {
				model.addAttribute("msg","제품등록 실패!");
				return "common/errorPage2";
			}
			
			
			 
			 
		
		
		
	
	
	}

	// 제품 등록시 제품코드 중복체크 ajax
	@PostMapping("duplicationCheck.pr")
	public void duplicationCheck(@RequestParam String pdtCode, HttpServletResponse response) {

		try {
			Product pro = new Product();
			pro.setPdtCode(pdtCode);

			int result = ps.selectProductCode(pro);

			if (result > 0) {
				response.getWriter().print("success");
			} else {
				response.getWriter().print("fail");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@PostMapping("searchProName.pr")
	public ModelAndView searchProName(HttpServletRequest request, ModelAndView mv) {
		Product pdt = new Product();
		pdt.setPdtName(request.getParameter("pdtName"));
		
		List<Product> list = ps.searchProName(pdt);

		
		
		mv.addObject("list", list);
		mv.setViewName("jsonView");
		
		return mv;

	}
	
	//제품 검색 시 리스트 불러오는 메소드
	@GetMapping("search.pr")
	public void searchProduct(Product prod, HttpServletResponse response) {
		List<Product> list = ps.selectSearch(prod);
		System.out.println("list : " + list);
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	// 관리자 직원관리 해당 직원클릭하면 정보 select
		@PostMapping("searchPro.pr")
		public ModelAndView SearchEmp(HttpServletRequest request, ModelAndView mv) {

			ProductAjax pro = new ProductAjax();
		pro.setPdtId(Integer.parseInt(request.getParameter("pdtId")));

		ProductAjax searchPro = ps.searchPro(pro);

			

			mv.addObject(searchPro);
			mv.setViewName("jsonView");

		
			return mv;
		}
		
		@PostMapping("update.pr")
		public String updateProduct(Model model, Product pr,HttpServletRequest request) {
			
			if(pr.getUnit().equals("BOX")) {
				pr.setUnit("BOX");
			}else if(pr.getUnit().equals("EA")) {
				pr.setUnit("EA");
			}else if(pr.getUnit().equals("SET")) {
				pr.setUnit("SET");
			}
			
			if(pr.getStandard().equals("pro1")) {
				pr.setStandard("EA");
			}else if(pr.getStandard().equals("pro2")) {
				pr.setStandard("PCS");
			}else if(pr.getStandard().equals("pro3")) {
				pr.setStandard("QTY");
			}else if(pr.getStandard().equals("pro4")) {
				pr.setStandard("SET");
			}
			 
			

			ProductHistory proHis = new ProductHistory();
			
			proHis.setPrice(pr.getPrice());
			proHis.setEnroller(pr.getEmpNo());
			proHis.setPdtId(pr.getPdtId());
			
			if(request.getParameter("modifyer") != null) {
				
				if(Integer.parseInt(request.getParameter("modifyer")) == pr.getEmpNo()) {
					proHis.setModifyer(pr.getEmpNo());
				}else {
					
					proHis.setModifyer(Integer.parseInt(request.getParameter("modifyer")));
					
				}
			}else {
				
				proHis.setModifyer(pr.getEmpNo());
				
			}
			
			
			
			int result = ps.updateProduct(pr);
			int result2 =0;
			
			if (result > 0) {
				
				result2 = ps.updateHisProduct(proHis);
				
				if(result2 > 0) {
					
					 model.addAttribute("updateMsg","제품수정이 완료되었습니다.");
						model.addAttribute("bb",1);
						return "admin/salesManagement/productSuccessPage";
					
				}else {
					model.addAttribute("msg", "제품정보 수정 이력남기기 실패!");
					return "common/errorPage";
					
				}
				
				

			} else {
				model.addAttribute("msg", "제품정보 수정실패!");
				return "common/errorPage";

			}

		}
		
		@PostMapping("delete.pr")
		public String deleteProduct(Model model , Product pro ) {
			
			int result = ps.deleteProduct(pro);
			
			
			
			  if (result > 0) { 
				  
				  model.addAttribute("deleteMsg","제품삭제 완료되었습니다.");
					model.addAttribute("cc",1);
					return "admin/salesManagement/productSuccessPage";
				 
			  }else { 
				  model.addAttribute("msg", "제품삭제 실패!"); 
				  return "common/errorPage";
			  }
			
		}
		
		
		@PostMapping("selectProHis.pr")
		public ModelAndView SelectProHis(HttpServletRequest request, ModelAndView mv) {
			
			ProductHistory proHis = new ProductHistory();
			proHis.setPdtId(Integer.parseInt(request.getParameter("pdtId")));
			
			List<ProductHistory> SearchProHis = ps.selectProHis(proHis);
			int hisCount = ps.selectHisCount(proHis);
			
	
			
		
			
			mv.addObject(SearchProHis);
			mv.addObject(hisCount);
			mv.setViewName("jsonView");
			return mv;
		}

}
