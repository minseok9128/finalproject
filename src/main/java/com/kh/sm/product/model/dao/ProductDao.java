package com.kh.sm.product.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.product.model.vo.Product;
import com.kh.sm.product.model.vo.ProductAjax;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.product.model.vo.ProductHistory;

public interface ProductDao {

	int insertProduct(SqlSessionTemplate sqlSession, Product pr);

	//제품 등록시 제품코드 중복확인 ajax
	int selectProductCode(SqlSessionTemplate sqlSession, Product pro);

	List<Product> selectSearch(SqlSessionTemplate sqlSession, Product prod);
	//제품 등록시 첨부파일 등록
	int insertAttach(SqlSessionTemplate sqlSession, com.kh.sm.common.Attachment attc);

	List<Product> searchProName(SqlSessionTemplate sqlSession, Product pdt);

	ProductAjax searchPro(SqlSessionTemplate sqlSession, ProductAjax pro);

	int updateProduct(SqlSessionTemplate sqlSession, Product pr);

	int deleteProduct(SqlSessionTemplate sqlSession, Product pro);

	int insertProductDetail(SqlSessionTemplate sqlSession, ProductDetail prDList);

	int insertHisProduct(SqlSessionTemplate sqlSession, ProductHistory proHis);

	int updateHisProduct(SqlSessionTemplate sqlSession, ProductHistory proHis);

	List<ProductHistory> selectProHis(SqlSessionTemplate sqlSession, ProductHistory proHis);

	int selectHisCount(SqlSessionTemplate sqlSession, ProductHistory proHis);
	

	

}
