package com.kh.sm.product.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.common.Attachment;
import com.kh.sm.product.model.vo.Product;
import com.kh.sm.product.model.vo.ProductAjax;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.product.model.vo.ProductHistory;

@Repository
public class ProductDaoImpl implements ProductDao{

	@Override
	public int insertProduct(SqlSessionTemplate sqlSession, Product pr) {
	
		
		return sqlSession.insert("Product.insertProduct",pr);
		
		
	}

	//제품 등록시 제품코드 중복확인 ajax
	@Override
	public int selectProductCode(SqlSessionTemplate sqlSession, Product pro) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Product.selectProCode",pro);
		
		
	}

	@Override
	public List<Product> selectSearch(SqlSessionTemplate sqlSession, Product prod) {
		System.out.println("prod dao : " + prod);
		return sqlSession.selectList("Product.selectSearch", prod);
	}
		
	public int insertAttach(SqlSessionTemplate sqlSession, Attachment attc) {
		int result = 0;
		
	
		result = sqlSession.insert("Product.insertFile", attc);
		
		return result;
	}

	@Override
	public List<Product> searchProName(SqlSessionTemplate sqlSession, Product pdt) {
		return sqlSession.selectList("Product.searchProName",pdt);
	}

	@Override
	public ProductAjax searchPro(SqlSessionTemplate sqlSession, ProductAjax pro) {
		return sqlSession.selectOne("Product.searchPro",pro);
	}

	@Override
	public int updateProduct(SqlSessionTemplate sqlSession, Product pr) {
		return sqlSession.update("Product.updateProduct", pr);
	}

	@Override
	public int deleteProduct(SqlSessionTemplate sqlSession, Product pro) {
		return sqlSession.update("Product.deleteProduct",pro);
	}

	@Override
	public int insertProductDetail(SqlSessionTemplate sqlSession, ProductDetail prDList) {
			System.out.println(prDList);
		return sqlSession.insert("Product.insertPoroductDetail", prDList);
	}

	@Override
	public int insertHisProduct(SqlSessionTemplate sqlSession, ProductHistory proHis) {
		
		return sqlSession.insert("Product.insertHisProduct",proHis);
	}

	@Override
	public int updateHisProduct(SqlSessionTemplate sqlSession, ProductHistory proHis) {
		// TODO Auto-generated method stub
		return sqlSession.insert("Product.updateHisProduct",proHis);
	}

	@Override
	public List<ProductHistory> selectProHis(SqlSessionTemplate sqlSession, ProductHistory proHis) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("Product.selectHisProduct",proHis);
	}

	@Override
	public int selectHisCount(SqlSessionTemplate sqlSession, ProductHistory proHis) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Product.selectHisCount",proHis);
	}
}
