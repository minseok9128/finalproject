package com.kh.sm.product.model.exception;

public class InsertProductDetailException extends Exception {
	public InsertProductDetailException(String msg) {
		super(msg);
	}
}
