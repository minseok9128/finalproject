package com.kh.sm.product.model.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.product.model.exception.InsertProductDetailException;
import com.kh.sm.product.model.vo.Product;
import com.kh.sm.product.model.vo.ProductAjax;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.product.model.vo.ProductHistory;

public interface ProductService {

	int insertProduct(Product pr);

	//제품 등록시 제품코드 중복확인 ajax
	int selectProductCode(Product pro);

	List<Product> selectSearch(Product prod);
	//제품 등록시 첨부파일 등록
	int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath);

	//제품명 검색시 select
	List<Product> searchProName(Product pdt);

	//검색 후 해당 제품 가져오기
	ProductAjax searchPro(ProductAjax pro);

	
	//제품 수정
	int updateProduct(Product pr);

	int deleteProduct(Product pro);

	
	int insertHisProduct(ProductHistory proHis);

	void insertProductDetail(ProductDetail prDList);

	int updateHisProduct(ProductHistory proHis);

	List<ProductHistory> selectProHis(ProductHistory proHis);

	int selectHisCount(ProductHistory proHis);



	

}
