package com.kh.sm.product.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.product.model.dao.ProductDao;
import com.kh.sm.product.model.exception.InsertProductDetailException;
import com.kh.sm.product.model.vo.Product;
import com.kh.sm.product.model.vo.ProductAjax;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.product.model.vo.ProductHistory;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ProductDao pd;

	@Override
	public int insertProduct(Product pr) {
		
		return pd.insertProduct(sqlSession,pr);
	
		
	}

	//제품 등록시 제품코드 중복확인 ajax	
	@Override
	public int selectProductCode(Product pro) {
		
		return pd.selectProductCode(sqlSession,pro);
	}

	@Override
	public List<Product> selectSearch(Product prod) {
		return pd.selectSearch(sqlSession, prod);
	}
	//제품 등록시 첨부파일 저장 메소드
	@Override
	public int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath) {
		int result = 0;
		
		
		Attachment attc = null;
		for(int i = 0; i < fList.size(); i++) {
			attc = new Attachment();
			attc.setCategory(1);
			attc.setChangeName(changeNames.get(i));
			attc.setFilePath(filePath);
			attc.setOriginName(fList.get(i).getOriginalFilename());
			attc.setDelYn("N");
			attc.setFileId(1);
			result += pd.insertAttach(sqlSession, attc);	
			
			
		}
		
		
		return result;
	}

	
	//관리자 제품관리 제품명 등록시 select
	@Override
	public List<Product> searchProName(Product pdt) {
		// TODO Auto-generated method stub
		return pd.searchProName(sqlSession, pdt);
	}

	@Override
	public ProductAjax searchPro(ProductAjax pro) {
		
		return pd.searchPro(sqlSession, pro);
	}

	@Override
	public int updateProduct(Product pr) {
		
		return pd.updateProduct(sqlSession,pr);
	}

	@Override
	public int deleteProduct(Product pro) {
		
		return pd.deleteProduct(sqlSession,pro);
	}

	@Override
	public int insertHisProduct(ProductHistory proHis) {
		// TODO Auto-generated method stub
		return pd.insertHisProduct(sqlSession,proHis);
	}

	@Override
	public void insertProductDetail(ProductDetail prDList) {
	
		int result = pd.insertProductDetail(sqlSession, prDList);
	}

	@Override
	public int updateHisProduct(ProductHistory proHis) {
		// TODO Auto-generated method stub
		return pd.updateHisProduct(sqlSession,proHis);
	}

	@Override
	public List<ProductHistory> selectProHis(ProductHistory proHis) {
		// TODO Auto-generated method stub
		return pd.selectProHis(sqlSession,proHis);
	}

	@Override
	public int selectHisCount(ProductHistory proHis) {
		// TODO Auto-generated method stub
		return pd.selectHisCount(sqlSession,proHis);
	}


}
