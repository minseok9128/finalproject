package com.kh.sm.product.model.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

import com.kh.sm.common.Attachment;





public class Product implements Serializable{
	private int pdtId;
	private String pdtName;
	private Date date;
	private int quantity;
	private String standard;
	private String unit;
	private int price;
	private String info;
	private int empNo;
	private String pdtCode;
	private String pdtDelYn;
	private ArrayList<Attachment> attList;
	
	public Product() {}

	public Product(int pdtId, String pdtName, Date date, int quantity, String standard, String unit, int price,
			String info, int empNo, String pdtCode, String pdtDelYn, ArrayList<Attachment> attList) {
		super();
		this.pdtId = pdtId;
		this.pdtName = pdtName;
		this.date = date;
		this.quantity = quantity;
		this.standard = standard;
		this.unit = unit;
		this.price = price;
		this.info = info;
		this.empNo = empNo;
		this.pdtCode = pdtCode;
		this.pdtDelYn = pdtDelYn;
		this.attList = attList;
	}

	public int getPdtId() {
		return pdtId;
	}

	public void setPdtId(int pdtId) {
		this.pdtId = pdtId;
	}

	public String getPdtName() {
		return pdtName;
	}

	public void setPdtName(String pdtName) {
		this.pdtName = pdtName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getPdtCode() {
		return pdtCode;
	}

	public void setPdtCode(String pdtCode) {
		this.pdtCode = pdtCode;
	}

	public String getPdtDelYn() {
		return pdtDelYn;
	}

	public void setPdtDelYn(String pdtDelYn) {
		this.pdtDelYn = pdtDelYn;
	}

	public ArrayList<Attachment> getAttList() {
		return attList;
	}

	public void setAttList(ArrayList<Attachment> attList) {
		this.attList = attList;
	}

	@Override
	public String toString() {
		return "Product [pdtId=" + pdtId + ", pdtName=" + pdtName + ", date=" + date + ", quantity=" + quantity
				+ ", standard=" + standard + ", unit=" + unit + ", price=" + price + ", info=" + info + ", empNo="
				+ empNo + ", pdtCode=" + pdtCode + ", pdtDelYn=" + pdtDelYn + ", attList=" + attList + "]";
	}

	
	
	
}