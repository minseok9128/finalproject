package com.kh.sm.product.model.vo;

import javax.tools.JavaCompiler;

public class ProductDetail implements java.io.Serializable{ 
	
	private int pdtDtailId; 			//영업기회제품ID
	private int discountRate;			//할인율
	private int totalPrice;				//합계
	private int quantity;				//수량
	private int amount;					//민석 수량
	private int pdtId;					//제품코드
	private String category;			//제품구분
	private int categoryId;				//제품구분ID
	private int oppId;					//영업기회ID
	private int estId;					//견적ID
	private int detailPrice;			//단가
	private ProductAjax productAjax;
	private int revId;
	private String deliveryDate;
	
	public ProductDetail() {}

	public ProductDetail(int pdtDtailId, int discountRate, int totalPrice, int quantity, int amount, int pdtId,
			String category, int categoryId, int oppId, int estId, int detailPrice, ProductAjax productAjax, int revId,
			String deliveryDate) {
		super();
		this.pdtDtailId = pdtDtailId;
		this.discountRate = discountRate;
		this.totalPrice = totalPrice;
		this.quantity = quantity;
		this.amount = amount;
		this.pdtId = pdtId;
		this.category = category;
		this.categoryId = categoryId;
		this.oppId = oppId;
		this.estId = estId;
		this.detailPrice = detailPrice;
		this.productAjax = productAjax;
		this.revId = revId;
		this.deliveryDate = deliveryDate;
	}

	public int getPdtDtailId() {
		return pdtDtailId;
	}

	public void setPdtDtailId(int pdtDtailId) {
		this.pdtDtailId = pdtDtailId;
	}

	public int getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(int discountRate) {
		this.discountRate = discountRate;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getPdtId() {
		return pdtId;
	}

	public void setPdtId(int pdtId) {
		this.pdtId = pdtId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public int getEstId() {
		return estId;
	}

	public void setEstId(int estId) {
		this.estId = estId;
	}

	public int getDetailPrice() {
		return detailPrice;
	}

	public void setDetailPrice(int detailPrice) {
		this.detailPrice = detailPrice;
	}

	public ProductAjax getProductAjax() {
		return productAjax;
	}

	public void setProductAjax(ProductAjax productAjax) {
		this.productAjax = productAjax;
	}

	public int getRevId() {
		return revId;
	}

	public void setRevId(int revId) {
		this.revId = revId;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Override
	public String toString() {
		return "ProductDetail [pdtDtailId=" + pdtDtailId + ", discountRate=" + discountRate + ", totalPrice="
				+ totalPrice + ", quantity=" + quantity + ", amount=" + amount + ", pdtId=" + pdtId + ", category="
				+ category + ", categoryId=" + categoryId + ", oppId=" + oppId + ", estId=" + estId + ", detailPrice="
				+ detailPrice + ", productAjax=" + productAjax + ", revId=" + revId + ", deliveryDate=" + deliveryDate
				+ "]";
	}

		
}
