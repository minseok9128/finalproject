package com.kh.sm.product.model.vo;

import java.util.Date;

import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.Employee2;

public class ProductHistory {
	
	private int pdtHisId;
	private int price;
	private int enroller;
	private int modifyer;
	private int pdtId;
	private String hisStatus;
	private String hisDate;
	
	private Employee employee;
	private Employee2 employee2;
	
	public ProductHistory() {}

	public ProductHistory(int pdtHisId, int price, int enroller, int modifyer, int pdtId, String hisStatus,
			String hisDate, Employee employee, Employee2 employee2) {
		super();
		this.pdtHisId = pdtHisId;
		this.price = price;
		this.enroller = enroller;
		this.modifyer = modifyer;
		this.pdtId = pdtId;
		this.hisStatus = hisStatus;
		this.hisDate = hisDate;
		this.employee = employee;
		this.employee2 = employee2;
	}

	public int getPdtHisId() {
		return pdtHisId;
	}

	public void setPdtHisId(int pdtHisId) {
		this.pdtHisId = pdtHisId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getEnroller() {
		return enroller;
	}

	public void setEnroller(int enroller) {
		this.enroller = enroller;
	}

	public int getModifyer() {
		return modifyer;
	}

	public void setModifyer(int modifyer) {
		this.modifyer = modifyer;
	}

	public int getPdtId() {
		return pdtId;
	}

	public void setPdtId(int pdtId) {
		this.pdtId = pdtId;
	}

	public String getHisStatus() {
		return hisStatus;
	}

	public void setHisStatus(String hisStatus) {
		this.hisStatus = hisStatus;
	}

	public String getHisDate() {
		return hisDate;
	}

	public void setHisDate(String hisDate) {
		this.hisDate = hisDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Employee2 getEmployee2() {
		return employee2;
	}

	public void setEmployee2(Employee2 employee2) {
		this.employee2 = employee2;
	}

	@Override
	public String toString() {
		return "ProductHistory [pdtHisId=" + pdtHisId + ", price=" + price + ", enroller=" + enroller + ", modifyer="
				+ modifyer + ", pdtId=" + pdtId + ", hisStatus=" + hisStatus + ", hisDate=" + hisDate + ", employee="
				+ employee + ", employee2=" + employee2 + "]";
	}

	

	
	

}
