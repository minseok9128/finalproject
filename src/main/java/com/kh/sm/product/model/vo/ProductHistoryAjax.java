package com.kh.sm.product.model.vo;

import java.util.Date;

public class ProductHistoryAjax {
	
	private int pdtHisId;
	private int price;
	private int enroller;
	private String enrollDate;
	private int modifyer;
	private String modifyDate;
	private String startDate;
	private String endDate;
	private int pdtId;
	
	public ProductHistoryAjax() {}

	public ProductHistoryAjax(int pdtHisId, int price, int enroller, String enrollDate, int modifyer, String modifyDate,
			String startDate, String endDate, int pdtId) {
		super();
		this.pdtHisId = pdtHisId;
		this.price = price;
		this.enroller = enroller;
		this.enrollDate = enrollDate;
		this.modifyer = modifyer;
		this.modifyDate = modifyDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.pdtId = pdtId;
	}

	public int getPdtHisId() {
		return pdtHisId;
	}

	public void setPdtHisId(int pdtHisId) {
		this.pdtHisId = pdtHisId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getEnroller() {
		return enroller;
	}

	public void setEnroller(int enroller) {
		this.enroller = enroller;
	}

	public String getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(String enrollDate) {
		this.enrollDate = enrollDate;
	}

	public int getModifyer() {
		return modifyer;
	}

	public void setModifyer(int modifyer) {
		this.modifyer = modifyer;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getPdtId() {
		return pdtId;
	}

	public void setPdtId(int pdtId) {
		this.pdtId = pdtId;
	}

	@Override
	public String toString() {
		return "ProductHistoryAjax [pdtHisId=" + pdtHisId + ", price=" + price + ", enroller=" + enroller
				+ ", enrollDate=" + enrollDate + ", modifyer=" + modifyer + ", modifyDate=" + modifyDate
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", pdtId=" + pdtId + "]";
	}
	
	

}
