package com.kh.sm.relationClient.model.vo;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;

public class RelationClient {

	
	private int relClientId;
	private int oppId;
	private int clientId;
	private Client client;
	
	private Company company;
	private Employee employee;
	
	public RelationClient() {}

	public RelationClient(int relClientId, int oppId, int clientId, Client client, Company company, Employee employee) {
		super();
		this.relClientId = relClientId;
		this.oppId = oppId;
		this.clientId = clientId;
		this.client = client;
		this.company = company;
		this.employee = employee;
	}

	public int getRelClientId() {
		return relClientId;
	}

	public void setRelClientId(int relClientId) {
		this.relClientId = relClientId;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "RelationClient [relClientId=" + relClientId + ", oppId=" + oppId + ", clientId=" + clientId
				+ ", client=" + client + ", company=" + company + ", employee=" + employee + "]";
	}


	
}
