package com.kh.sm.sales.controller;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kh.sm.common.Attachment;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.contract.model.vo.ContProduct;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.sales.model.service.SalesService;
import com.kh.sm.sales.model.vo.Sales;

@Controller
public class SalesController {
	
	@Autowired
	private SalesService ss; 
	
	@GetMapping("selList.sal")
	public String selListSales(Model model, HttpServletRequest request) {
		
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		
		List<Sales> list = ss.selListSales(empNo);
		System.out.println("list : " + list);
		model.addAttribute("list", list);
		return "sales/salesManagement/salesMain";
	}
	
	@GetMapping("selectOne.sal")
	public String selectOneSales(Model model, int revId) {
		System.out.println("revId : "+ revId);
		List<HashMap<String,Object>> sales = ss.selectOne(revId);
		System.out.println(sales);
		model.addAttribute("list",sales);
		return "sales/salesManagement/salesDetail";
	}
	
	@GetMapping("goInsert.sal")
	public String goInsertSales(Model model) {
		model.addAttribute("revId" , 0);
		return "sales/salesManagement/salesEnrollment";
	}
	
	@PostMapping("insert.sal")
	public String insertSales(Sales sales, HttpServletRequest request, Model model) {
			
		String[] id = request.getParameterValues("prId");
		String[] price = request.getParameterValues("price");
		String[] amount = request.getParameterValues("amount");
		String[] rD = request.getParameterValues("revDate");
		
		ArrayList<ProductDetail> pdList = new ArrayList<ProductDetail>();
		
		for(int i = 0 ; i < id.length ; i ++) {
			ProductDetail pd = new ProductDetail();
			pd.setPdtId(Integer.parseInt(id[i]));
			pd.setDetailPrice(Integer.parseInt(price[i]));
			pd.setQuantity(Integer.parseInt(amount[i]));
			int totalPrice = Integer.parseInt(amount[i]) * Integer.parseInt(price[i]);
			pd.setTotalPrice(totalPrice);
			pd.setDeliveryDate(rD[i]);
			pdList.add(pd);
		}
		for(ProductDetail pd : pdList ) {
			System.out.println(pd);
		}
		sales.setPdList(pdList);
		System.out.println("sales : " + sales);
		
		if(sales.getRevVatYn() == null) {
			sales.setRevVatYn("N");
		}
		
		int result = ss.insertSales(sales);
		
		return "redirect:/selList.sal";
	}
	
}
