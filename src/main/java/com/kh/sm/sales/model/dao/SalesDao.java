package com.kh.sm.sales.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.sales.model.vo.Sales;

public interface SalesDao {

	int insertSales(SqlSessionTemplate sqlSession, Sales sales);

	int insertProductDetail(SqlSessionTemplate sqlSession, ProductDetail pd);

	List<Sales> selListSales(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectOne(SqlSessionTemplate sqlSession, int revId);


}
