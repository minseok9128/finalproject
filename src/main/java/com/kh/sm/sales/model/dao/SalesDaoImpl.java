package com.kh.sm.sales.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.sales.model.vo.Sales;

@Repository
public class SalesDaoImpl implements SalesDao {

	@Override
	public int insertSales(SqlSessionTemplate sqlSession, Sales sales) {
		return sqlSession.insert("Sales.insert", sales);
	}

	@Override
	public int insertProductDetail(SqlSessionTemplate sqlSession, ProductDetail pd) {
		return sqlSession.insert("Sales.insertPd", pd);
		
	}

	@Override
	public List<Sales> selListSales(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Sales.selListSales",empNo);
	}

	@Override
	public List<HashMap<String, Object>> selectOne(SqlSessionTemplate sqlSession, int revId) {
		return sqlSession.selectList("Sales.selectOne", revId);
	}
	
	
	
	

}
