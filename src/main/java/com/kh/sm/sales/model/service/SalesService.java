package com.kh.sm.sales.model.service;

import java.util.HashMap;
import java.util.List;

import com.kh.sm.sales.model.vo.Sales;

public interface SalesService {

	int insertSales(Sales sales);

	List<Sales> selListSales(int empNo);

	List<HashMap<String, Object>> selectOne(int revId);


}
