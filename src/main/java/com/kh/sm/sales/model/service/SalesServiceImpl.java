package com.kh.sm.sales.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.sales.model.dao.SalesDao;
import com.kh.sm.sales.model.vo.Sales;

@Service
public class SalesServiceImpl  implements SalesService {
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private SalesDao sd;

	@Override
	public int insertSales(Sales sales) {
		int result = sd.insertSales(sqlSession, sales);
		
		for(ProductDetail pd : sales.getPdList()) {
			result += sd.insertProductDetail(sqlSession, pd);
		}
				
		return result;
	}

	@Override
	public List<Sales> selListSales(int empNo) {
		return sd.selListSales(sqlSession,empNo);
	}

	@Override
	public List<HashMap<String, Object>> selectOne(int revId) {
		return sd.selectOne(sqlSession, revId);
	}

	


//	@Override
//	public int insertSales(Sales sales) {
//		//매출 넣는 부분
//		int result = sd.insertSales(sqlSession, sales);
//		
//		for(ProductDetail pd : sales.getPdList()) {
//			result += sd.insertProductDetail(sqlSession, pd);
//		}
//				
//		return result;
//	}
	
	
	

}
