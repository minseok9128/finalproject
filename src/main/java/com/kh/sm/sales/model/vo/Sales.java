package com.kh.sm.sales.model.vo;

import java.sql.Date;
import java.util.List;

import com.kh.sm.common.Attachment;
import com.kh.sm.product.model.vo.ProductDetail;

public class Sales implements java.io.Serializable {
	
	private int revId;
	private String revCategory;
	private int comId;
	private String comName;
	private int clientId;
	private String clientName;
	private int estId;
	private String estName;
	private int contractId;
	private String contractName;
	private Date revOrder;
	private int revQuantity;
	private int revTotalValue;
	private int revTax;
	private int revTotal;
	private String revVatYn;
	private int empId;
	private String empName;
	private String revDetail;
	private List<ProductDetail> pdList;
	
	public Sales() {}

	public Sales(int revId, String revCategory, int comId, String comName, int clientId, String clientName, int estId,
			String estName, int contractId, String contractName, Date revOrder, int revQuantity, int revTotalValue,
			int revTax, int revTotal, String revVatYn, int empId, String empName, String revDetail,
			List<ProductDetail> pdList) {
		super();
		this.revId = revId;
		this.revCategory = revCategory;
		this.comId = comId;
		this.comName = comName;
		this.clientId = clientId;
		this.clientName = clientName;
		this.estId = estId;
		this.estName = estName;
		this.contractId = contractId;
		this.contractName = contractName;
		this.revOrder = revOrder;
		this.revQuantity = revQuantity;
		this.revTotalValue = revTotalValue;
		this.revTax = revTax;
		this.revTotal = revTotal;
		this.revVatYn = revVatYn;
		this.empId = empId;
		this.empName = empName;
		this.revDetail = revDetail;
		this.pdList = pdList;
	}

	public int getRevId() {
		return revId;
	}

	public void setRevId(int revId) {
		this.revId = revId;
	}

	public String getRevCategory() {
		return revCategory;
	}

	public void setRevCategory(String revCategory) {
		this.revCategory = revCategory;
	}

	public int getComId() {
		return comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getEstId() {
		return estId;
	}

	public void setEstId(int estId) {
		this.estId = estId;
	}

	public String getEstName() {
		return estName;
	}

	public void setEstName(String estName) {
		this.estName = estName;
	}

	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public Date getRevOrder() {
		return revOrder;
	}

	public void setRevOrder(Date revOrder) {
		this.revOrder = revOrder;
	}

	public int getRevQuantity() {
		return revQuantity;
	}

	public void setRevQuantity(int revQuantity) {
		this.revQuantity = revQuantity;
	}

	public int getRevTotalValue() {
		return revTotalValue;
	}

	public void setRevTotalValue(int revTotalValue) {
		this.revTotalValue = revTotalValue;
	}

	public int getRevTax() {
		return revTax;
	}

	public void setRevTax(int revTax) {
		this.revTax = revTax;
	}

	public int getRevTotal() {
		return revTotal;
	}

	public void setRevTotal(int revTotal) {
		this.revTotal = revTotal;
	}

	public String getRevVatYn() {
		return revVatYn;
	}

	public void setRevVatYn(String revVatYn) {
		this.revVatYn = revVatYn;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getRevDetail() {
		return revDetail;
	}

	public void setRevDetail(String revDetail) {
		this.revDetail = revDetail;
	}

	public List<ProductDetail> getPdList() {
		return pdList;
	}

	public void setPdList(List<ProductDetail> pdList) {
		this.pdList = pdList;
	}

	@Override
	public String toString() {
		return "Sales [revId=" + revId + ", revCategory=" + revCategory + ", comId=" + comId + ", comName=" + comName
				+ ", clientId=" + clientId + ", clientName=" + clientName + ", estId=" + estId + ", estName=" + estName
				+ ", contractId=" + contractId + ", contractName=" + contractName + ", revOrder=" + revOrder
				+ ", revQuantity=" + revQuantity + ", revTotalValue=" + revTotalValue + ", revTax=" + revTax
				+ ", revTotal=" + revTotal + ", revVatYn=" + revVatYn + ", empId=" + empId + ", empName=" + empName
				+ ", revDetail=" + revDetail + ", pdList=" + pdList + "]";
	}

		
}
