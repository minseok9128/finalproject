package com.kh.sm.salesActivity.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.Attachment;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.salesActivity.model.service.SalesActivityService;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesActivity.model.vo.Calendar;


@Controller
public class SalesActivityController {
	
	@Autowired
	private SalesActivityService ss;
	
	
	//영업활동 을 등록할 고객의 목록을 조회하는 메소드
	@RequestMapping(value="actCliList.sa", method=RequestMethod.GET)
	public String actClientList(Model model,HttpServletRequest request) {
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
	
		
		//영업활동을 등록할 고객의 리스트, 고객당 영업활동 갯수, 고객사이름을 가져오는 HashMap
		HashMap<String, Object> hmap =(HashMap<String, Object>) ss.clientList(empNo);
		
		ArrayList<Client> clientList = (ArrayList<Client>) hmap.get("clientList");
		String[] comNameArr = (String[]) hmap.get("comName");
		int[] count = (int[]) hmap.get("count");
		ArrayList<Activity> activityList = (ArrayList) hmap.get("activityList");
		model.addAttribute("clientList", clientList);
		model.addAttribute("comNameArr", comNameArr);
		model.addAttribute("count", count);
		model.addAttribute("activityList", activityList);
		return "sales/salesActivity/salesActivitiesList";
	}
	
	@RequestMapping(value="actListTable.sa", method=RequestMethod.GET)
	public ModelAndView actListTable(ModelAndView mv, HttpServletRequest request) {
		Employee emp =  (Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		List<HashMap<String, Object>> actListTable = ss.actListTable(empNo);

		
		mv.addObject("actListTable", actListTable);
		mv.setViewName("sales/salesActivity/salesActivitiesTable");
		
		return mv;
	}
	
	//영업활동 등록페이지로 이동하는 메소드
	@PostMapping("actEnrollView.sa")
	public String goEnrollView(Model model, String comName, Client c) {
	
		
		
		model.addAttribute("client", c);
		model.addAttribute("comName", comName);
		
		return "sales/salesActivity/salesActivitiesEnroll";
	}
	
	//영업활동을 등록하는 메소드
	@PostMapping("actEnroll.sa")
	public String enrollActivity(Activity activity, Model model, HttpServletRequest request, MultipartHttpServletRequest multiRequest) {
		String activityDateStr = request.getParameter("activityDateStr");
		String[] pdtId = request.getParameterValues("pdtId");
		if(pdtId != null) {
			for(int i = 0; i < pdtId.length; i++) {
				System.out.println("pdtId : " + pdtId[i]);				
			}
		}
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		String[] companionArr =  request.getParameterValues("companion");
		String[] participantArr = request.getParameterValues("participant");
		
		
		//다중 파일을 저장할 List생성
		List<MultipartFile> fList = multiRequest.getFiles("files");
		
		ArrayList<String> changeNames = new ArrayList<>();
		for(int i = 0; i < fList.size(); i++) {
			System.out.println("fListName : " + fList.get(i).getOriginalFilename());
			String originFileName = fList.get(i).getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonUtils.getRandomString();
			System.out.println("changeName : " + changeName);
			try {
				fList.get(i).transferTo(new File(filePath + "\\" + changeName + ext));
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			changeNames.add(changeName);
		}
		
		
		
		if(activity.getCompleteYn().equals("N")) {
			activity.setCompleteYn("N");
		}else {
			activity.setCompleteYn("Y");
		}
		
		activity.setActivityDate(Date.valueOf(activityDateStr));
		
		System.out.println("activity : " + activity);
		
		int result = ss.insertActivity(activity);
		int result2 = 0;
		if(result > 0) {
			result2 = ss.insertAttach(fList, changeNames, filePath);
			if(companionArr != null) {
				result2 += ss.insertCompanion(companionArr);				
			}
			if(participantArr != null) {
				result2 += ss.insertParticipant(participantArr);				
			}
			if(pdtId != null) {
				result2 += ss.insertProduct(pdtId);				
			}
		}
		
		if(result2 > 0) {
			return "sales/salesActivity/salesActivitiesCal";							
		}else {
			return "sales/salesActivity/salesActivitiesList";				
		}
	}
	
	//영업활동 상세보기 메소드
	@RequestMapping(value="detail.sa", method=RequestMethod.GET)
	public String detailActivity(Activity activity, Model model, String clientName, String comName) {
		
		int activityId = activity.getActivityId();
		
		activity = ss.selectActivity(activityId);
		
		List<Attachment> at = ss.selectAttachment(activityId);
		model.addAttribute("attachList", at);

		
		model.addAttribute("activity",activity);
		model.addAttribute("clientName", clientName);
		model.addAttribute("comName", comName);
		return "sales/salesActivity/salesActivitiesDetail";
	}
	
	//영업활동 상세보기 메소드
	@GetMapping("actDetail.sa")
	public ModelAndView detailAct(ModelAndView mv, int activityId) {
		System.out.println("activityId : " + activityId);
		
		HashMap<String, Object> actDetail = ss.selectActDetail(activityId);
		List<HashMap<String, Object>> participant = ss.selectParticipant(activityId);
		List<HashMap<String, Object>> companion = ss.selectCompanion(activityId);
		List<Attachment> at = ss.selectAttachment(activityId);
		List<HashMap<String, Object>> productList = ss.selectProduct(activityId);
		mv.addObject("productList", productList);
		mv.addObject("attachList", at);
		mv.addObject("companion", companion);
		mv.addObject("participant", participant);
		mv.addObject("actDetail", actDetail);
		mv.setViewName("sales/salesActivity/salesActivitiesDetail");
		return mv;
	}
	
	@PostMapping("scrollDownClient.sa")
	public ModelAndView scrollDownClient(ModelAndView mv, int lastCno, HttpServletResponse response, int empNo) {
		response.setCharacterEncoding("UTF-8");
		HashMap<String, Object> hmap =(HashMap<String, Object>) ss.clientListScroll(empNo, lastCno);
		ArrayList<Client> clientList = (ArrayList<Client>) hmap.get("clientList");
		String[] comNameArr = (String[]) hmap.get("comName");
		int[] count = (int[]) hmap.get("count");
		ArrayList<HashMap<String, Object>> activityList = (ArrayList) hmap.get("activityList");
		mv.addObject("clientList", clientList);
		mv.addObject("comNameArr", comNameArr);
		mv.addObject("count", count);
		mv.addObject("activityList", activityList);
		System.out.println("activityList : " + activityList);
		
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	//무한스크롤 구현 메소드
	@PostMapping("scrollDopwnClient.sa")
	public ModelAndView scrollUpClient(ModelAndView mv, int firstCno, HttpServletResponse response, int empNo) {
		response.setCharacterEncoding("UTF-8");
		System.out.println("lastCnoAjax : " + firstCno);
		System.out.println("empNo : " + empNo);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	//고객,고객사 검색메소드
	@PostMapping("ccNameSrch.sa")
	public ModelAndView ccNameSrch(ModelAndView mv, String name, HttpServletResponse response, int empNo) {
		response.setCharacterEncoding("UTF-8");
		
		HashMap<String, Object> hmap =(HashMap<String, Object>) ss.clientListByName(empNo, name);
		ArrayList<Client> clientList = (ArrayList<Client>) hmap.get("clientList");
		String[] comNameArr = (String[]) hmap.get("comName");
		int[] count = (int[]) hmap.get("count");
		ArrayList<HashMap<String, Object>> activityList = (ArrayList) hmap.get("activityList");
		mv.addObject("clientList", clientList);
		mv.addObject("comNameArr", comNameArr);
		mv.addObject("count", count);
		mv.addObject("activityList", activityList);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	//영업활동 캘린더 메인 이동 메소드
	@RequestMapping(value="actCalendar.sa", method=RequestMethod.GET)
	public ModelAndView actCalendar(ModelAndView mv, HttpServletRequest request) {
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		
		int empNo = emp.getEmpNo();
		
		List<HashMap<String, Object>> priDateList = ss.selectPriDateList(empNo);
	
		
		System.out.println("empNoAct :" + empNo);
		
		mv.addObject("priList", priDateList);
		mv.setViewName("sales/salesActivity/salesActivitiesCal");
		
		return mv;
	}
	
	//캘린더 개인 일정 등록 및 업데이트 메소드
	@PostMapping("insertPriDate.sa")
	public ModelAndView insertPriDate(ModelAndView mv, HttpServletRequest request, Calendar cal) {
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		cal.setEmpNo(emp.getEmpNo());
		System.out.println("cal" + cal);
		int result = 0;
		if(cal.getCalId() == 0) {
			result = ss.insertPridate(cal);			
		}else {
			result = ss.updatePriDate(cal);
		}
		
		
		if(result > 0) {
			mv.setViewName("redirect:actCalendar.sa");
		}else {
			mv.setViewName("sales/salesActivity/salesActivitiesList");
		}
		
		return mv;
	}
	
	//달력에 표시할 데이터 가져오는 메소드
	@PostMapping("priDateList.sa")
	public ModelAndView priDateList(ModelAndView mv, HttpServletResponse response, int empNo) {
		response.setCharacterEncoding("UTF-8");
		
		//개인 일정 달력에 표시할 메소드
		List<HashMap<String, Object>> priDateList = ss.selectPriDateList(empNo);
		List<HashMap<String, Object>> actList = ss.selectActList(empNo);
		List<HashMap<String, Object>> planList = ss.selectPlanList(empNo);
//		List<HashMap<String, Object>> workList = ss.selectworkList(empNo);
		List<HashMap<String, Object>> proposeList = ss.selectProposeList(empNo);
		List<HashMap<String, Object>> estList = ss.selectEstList(empNo);
		List<HashMap<String, Object>> contList = ss.selectContList(empNo);

		
		
		mv.addObject("proposeList", proposeList);
		mv.addObject("estList", estList);
		mv.addObject("contList", contList);
//		mv.addObject("workList", workList);
		mv.addObject("planList", planList);
		mv.addObject("actList", actList);
		mv.addObject("priList", priDateList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//선택한 날짜의 정보를 목록으로 가져오는 메소드
	@PostMapping("dateInfo.sa")
	public ModelAndView dateInfo(ModelAndView mv, HttpServletResponse response, int empNo, String date) {
		response.setCharacterEncoding("UTF-8");
		
		List<HashMap<String, Object>> calList = ss.selectCalList(empNo, date);
		List<HashMap<String, Object>> actList = ss.selectActList(empNo, date);
		List<HashMap<String, Object>> proposeList = ss.selectProposeList(empNo, date);
		List<HashMap<String, Object>> estList = ss.selectActEstList(empNo, date);
		List<HashMap<String, Object>> contList = ss.selectContList(empNo, date);
		
		mv.addObject("proposeList", proposeList);
		mv.addObject("estList", estList);
		mv.addObject("contList", contList);
		mv.addObject("actList", actList);
		mv.addObject("calList", calList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//개인일정 상세보기 ajax
	@PostMapping("priDetail.sa")
	public ModelAndView priDetail(ModelAndView mv, HttpServletResponse response, int calId) {
		response.setCharacterEncoding("UTF-8");
		
		HashMap<String, Object> priDetail = ss.selectPriDetail(calId);
		
		
		mv.addObject("priDetail", priDetail);
		mv.setViewName("jsonView");
		return mv;
	}
	
	
	@GetMapping("delPridate.sa")
	public ModelAndView delPridate(ModelAndView mv, int calId) {
		int result = 0;
		result = ss.delPridate(calId);
		
		if(result > 0) {
			mv.setViewName("redirect:actCalendar.sa");			
		}else {
			
		}
		
		return mv;
	}

	@GetMapping("deleteAct.sa")
	public ModelAndView deleteAct(ModelAndView mv, int actId) {
		int result = 0;
		result = ss.deleteAct(actId);
		
		if(result > 0) {
			mv.addObject("msg", "삭제완료되었습니다.");
			mv.addObject("code", "deleteAct");
			mv.setViewName("common/successPage");			
		}else {
			mv.addObject("msg", "삭제에 실패하였습니다.");
			mv.setViewName("common/errorPage");
		}
		
		return mv;
	}
	
	
	@PostMapping("completeAct.sa")
	public ModelAndView completeAct(ModelAndView mv, Activity act) {
		int result = ss.completeAct(act);
		if(result > 0) {
			mv.addObject("msg", "활동완료 되었습니다.");
			mv.addObject("code", "completeAct");
			mv.setViewName("common/successPage");			
		}else {
			mv.addObject("msg", "활동완료 업데이트 실패!");
			mv.setViewName("common/errorPage");
		}
		return mv;
	}
	
	
	
}












