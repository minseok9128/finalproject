package com.kh.sm.salesActivity.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.Attachment;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesActivity.model.vo.Calendar;

public interface SalesActivityDao {

	HashMap<String, Object> clientList(SqlSessionTemplate sqlSession, int empNo);

	int insertAttach(SqlSessionTemplate sqlSession, Attachment attc);

	int insertActivity(SqlSessionTemplate sqlSession, Activity activity);

	Activity selectActivity(SqlSessionTemplate sqlSession, int activityId);

	HashMap<String, Object> clientListScroll(SqlSessionTemplate sqlSession, int empNo, int lastCno);

	HashMap<String, Object> clientListByName(SqlSessionTemplate sqlSession, int empNo, String name);

	List<HashMap<String, Object>> actListTable(SqlSessionTemplate sqlSession, int empNo);

	int insertCompanion(SqlSessionTemplate sqlSession, int companion);

	int insertParticipant(SqlSessionTemplate sqlSession, int participant);

	int insertPriDate(SqlSessionTemplate sqlSession, Calendar cal);

	List<Attachment> selectAttachment(int activityId, SqlSessionTemplate sqlSession);

	List<HashMap<String, Object>> selectPriDateList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectActList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectCalList(SqlSessionTemplate sqlSession, int empNo, String date);

	List<HashMap<String, Object>> selectDateActList(SqlSessionTemplate sqlSession, int empNo, String date);

	List<HashMap<String, Object>> selectPlanList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectProposeList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectEstList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectContList(SqlSessionTemplate sqlSession, int empNo);

	List<HashMap<String, Object>> selectProposeList(SqlSessionTemplate sqlSession, int empNo, String date);

	List<HashMap<String, Object>> selectActEstList(SqlSessionTemplate sqlSession, int empNo, String date);

	List<HashMap<String, Object>> selectContList(SqlSessionTemplate sqlSession, int empNo, String date);

	List<HashMap<String, Object>> selectWorkList(SqlSessionTemplate sqlSession, int empNo);

	HashMap<String, Object> selectPriDetail(SqlSessionTemplate sqlSession, int calId);

	int updatePriDate(SqlSessionTemplate sqlSession, Calendar cal);

	int delPridate(SqlSessionTemplate sqlSession, int calId);

	HashMap<String, Object> selectActDetail(SqlSessionTemplate sqlSession, int activityId);

	List<HashMap<String, Object>> selectParticipant(SqlSessionTemplate sqlSession, int activityId);

	List<HashMap<String, Object>> selectCompanion(SqlSessionTemplate sqlSession, int activityId);

	int insertProduct(SqlSessionTemplate sqlSession, int pdtId);

	List<HashMap<String, Object>> selectProduct(SqlSessionTemplate sqlSession, int activityId);

	int completeAct(SqlSessionTemplate sqlSession, Activity act);

	int deleteAct(SqlSessionTemplate sqlSession, int actId);


}
