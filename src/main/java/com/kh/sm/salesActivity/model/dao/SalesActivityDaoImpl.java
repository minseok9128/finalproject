package com.kh.sm.salesActivity.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.Attachment;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesActivity.model.vo.Calendar;

@Repository
public class SalesActivityDaoImpl implements SalesActivityDao {
	
	//영업활동을 등록할 고객리스트 정보를 불러오는 메소드
	@Override
	public HashMap<String, Object> clientList(SqlSessionTemplate sqlSession, int empNo) {
		List<Client> clientList = null;
		HashMap<String, Object> hmap = new HashMap<>();
		clientList = sqlSession.selectList("Client.clientList", empNo);
		int[] count = new int[clientList.size()];
		String comName[] = new String[clientList.size()];
		ArrayList<ArrayList<Activity>> activityList = new ArrayList<>();
		for(int i = 0; i < clientList.size(); i++) {
			comName[i] = sqlSession.selectOne("Company.clientCompany", clientList.get(i).getClientId());	
			count[i] = sqlSession.selectOne("Client.activeCount", clientList.get(i).getClientId());
			activityList.add((ArrayList) sqlSession.selectList("Activity.selectAct", clientList.get(i).getClientId()));
		}
		hmap.put("activityList", activityList);
		hmap.put("clientList", clientList);
		hmap.put("comName",comName);
		hmap.put("count", count);
		
		
		
		return hmap;
	}
	
	//영업활동 등록시 첨부파일 등록 메소드
	@Override
	public int insertAttach(SqlSessionTemplate sqlSession, Attachment attc) {
		int result = 0;
		result = sqlSession.insert("Activity.insertFile", attc);

		return result;
		
	}
	
	//영업활동 등록 메소드
	@Override
	public int insertActivity(SqlSessionTemplate sqlSession, Activity activity) {

		return sqlSession.insert("Activity.insertActivity", activity);
	}

	//영업활동 조회 메소드
	@Override
	public Activity selectActivity(SqlSessionTemplate sqlSession, int activityId) {
		return sqlSession.selectOne("Activity.selectActivity", activityId);
	}
	
	//영업활동 고객목록 무한스크롤형식으로 불러오는 메소드
	@Override
	public HashMap<String, Object> clientListScroll(SqlSessionTemplate sqlSession, int empNo, int lastCno) {
		List<HashMap<String, Object>> clientList = null;
		HashMap<String, Object> hmap = new HashMap<>();
		HashMap<String, Object> map = new HashMap<>();
		map.put("empNo", empNo);
		map.put("lastCno", lastCno);
		
		clientList = sqlSession.selectList("Client.clientListScroll", map);
		System.out.println(clientList.toString());
		
		int[] count = new int[clientList.size()];
		String comName[] = new String[clientList.size()];
		ArrayList<ArrayList<HashMap<String, Object>>> activityList = new ArrayList<>();
		for(int i = 0; i < clientList.size(); i++) {
			int clientId = Integer.parseInt(clientList.get(i).get("CLIENTID").toString());
			comName[i] = sqlSession.selectOne("Company.clientCompanyAjax",clientId );	
			count[i] = sqlSession.selectOne("Client.activeCountAjax", clientId);
			ArrayList<HashMap<String, Object>> selectActAjax = (ArrayList) sqlSession.selectList("Activity.selectActAjax",clientId);
			activityList.add(selectActAjax);
		}
		
		
		hmap.put("activityList", activityList);
		hmap.put("clientList", clientList);
		hmap.put("comName",comName);
		hmap.put("count", count);
		
		
		
		return hmap;
	}

	//고객,고객사 목록 이름 검색 메소드
	@Override
	public HashMap<String, Object> clientListByName(SqlSessionTemplate sqlSession, int empNo, String name) {
		List<HashMap<String, Object>> clientList = null;
		HashMap<String, Object> hmap = new HashMap<>();
		HashMap<String, Object> map = new HashMap<>();
		map.put("empNo", empNo);
		map.put("name", name);
		
		clientList = sqlSession.selectList("Client.clientListByName", map);
		System.out.println(clientList.toString());
		
		int[] count = new int[clientList.size()];
		String comName[] = new String[clientList.size()];
		ArrayList<ArrayList<HashMap<String, Object>>> activityList = new ArrayList<>();
		for(int i = 0; i < clientList.size(); i++) {
			int clientId = Integer.parseInt(clientList.get(i).get("CLIENTID").toString());
			comName[i] = sqlSession.selectOne("Company.clientCompanyAjax",clientId );	
			count[i] = sqlSession.selectOne("Client.activeCountAjax", clientId);
			ArrayList<HashMap<String, Object>> selectActAjax = (ArrayList) sqlSession.selectList("Activity.selectActAjax",clientId);
			activityList.add(selectActAjax);
		}
		
		
		hmap.put("activityList", activityList);
		hmap.put("clientList", clientList);
		hmap.put("comName",comName);
		hmap.put("count", count);
		
		
		
		return hmap;
	}

	//날짜별 영업활동 테이블 형식으로 목록 조회 메소드
	@Override
	public List<HashMap<String, Object>> actListTable(SqlSessionTemplate sqlSession, int empNo) {
		List<HashMap<String, Object>> actListTable = sqlSession.selectList("Activity.actListTable", empNo);
		return actListTable;
	}

	//동반인력 등록 메소드
	@Override
	public int insertCompanion(SqlSessionTemplate sqlSession, int companion) {
		int result = 0;
		System.out.println("companion : " + companion);
		result = sqlSession.insert("Activity.insertCompanion", companion);
		System.out.println("result : " + result);
		return result;
	}
	
	//참가자(다중고객) 등록 메소드
	@Override
	public int insertParticipant(SqlSessionTemplate sqlSession, int participant) {
		int result = 0;
		System.out.println("participant : " + participant);
		result = sqlSession.insert("Activity.insertParticipant", participant);
		System.out.println("result : " + result);
		return result;
	}

	@Override
	public int insertPriDate(SqlSessionTemplate sqlSession, Calendar cal) {
		
		
		return sqlSession.insert("Activity.insertPriDate", cal);
	}

	@Override
	public List<Attachment> selectAttachment(int activityId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Activity.selectAttachment",activityId);
	}
	
	//개인일정 캘린더로 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectPriDateList(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectList("Activity.selectPriCal", empNo);
	}
	
	//영업활동 캘린더로 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectActList(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectList("Activity.selectActCal", empNo);
	}

	@Override
	public List<HashMap<String, Object>> selectCalList(SqlSessionTemplate sqlSession, int empNo, String date) {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("empNo", empNo);
		hmap.put("date", date);
		
		return sqlSession.selectList("Activity.selectCalList", hmap);
	}

	@Override
	public List<HashMap<String, Object>> selectDateActList(SqlSessionTemplate sqlSession, int empNo, String date) {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("empNo", empNo);
		hmap.put("date", date);
		
		return sqlSession.selectList("Activity.selectDateActList", hmap);
	}

	@Override
	public List<HashMap<String, Object>> selectPlanList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Activity.selectPlanCal", empNo);
	}

	//캘린더에 뿌려줄 제안을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectProposeList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Activity.selectProposeCal", empNo);
	}
	
	//캘린더에 뿌려줄 견적을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectEstList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Activity.selectEstCal", empNo);
	}
	
	//캘린더에 뿌려줄 계약을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectContList(SqlSessionTemplate sqlSession, int empNo) {
		return sqlSession.selectList("Activity.selectContCal", empNo);
	}
	
	//선택한 날짜의 제안 정보를 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectProposeList(SqlSessionTemplate sqlSession, int empNo, String date) {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("empNo", empNo);
		hmap.put("date", date);
		
		return sqlSession.selectList("Activity.selectProposeList", hmap);
	}
	
	//선택한 날짜의 견적 정보를 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectActEstList(SqlSessionTemplate sqlSession, int empNo, String date) {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("empNo", empNo);
		hmap.put("date", date);
		
		return sqlSession.selectList("Activity.selectEstList", hmap);
	}

	//선택한 날짜의 계약 정보를 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectContList(SqlSessionTemplate sqlSession, int empNo, String date) {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("empNo", empNo);
		hmap.put("date", date);
		
		return sqlSession.selectList("Activity.selectContList", hmap);
	}

	@Override
	public List<HashMap<String, Object>> selectWorkList(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectList("Activity.selectWorkCal", empNo);
	}

	@Override
	public HashMap<String, Object> selectPriDetail(SqlSessionTemplate sqlSession, int calId) {
		
		return sqlSession.selectOne("Activity.selectPriDetail", calId);
	}

	@Override
	public int updatePriDate(SqlSessionTemplate sqlSession, Calendar cal) {
		return sqlSession.update("Activity.updatePriDate", cal);
	}

	@Override
	public int delPridate(SqlSessionTemplate sqlSession, int calId) {
		return sqlSession.update("Activity.delPridate", calId);
	}

	@Override
	public HashMap<String, Object> selectActDetail(SqlSessionTemplate sqlSession, int activityId) {
		return sqlSession.selectOne("Activity.selectActDetail", activityId);
	}

	//참가자 조회 메소드
	@Override
	public List<HashMap<String, Object>> selectParticipant(SqlSessionTemplate sqlSession, int activityId) {
		return sqlSession.selectList("Activity.selectParticipant", activityId);
	}

	//동반인력 조회 메소드
	@Override
	public List<HashMap<String, Object>> selectCompanion(SqlSessionTemplate sqlSession, int activityId) {
		return sqlSession.selectList("Activity.selectCompanion", activityId);
	}

	//제품등록 메소드
	@Override
	public int insertProduct(SqlSessionTemplate sqlSession, int pdtId) {
		return sqlSession.insert("Activity.insertProduct", pdtId);
	}

	@Override
	public List<HashMap<String, Object>> selectProduct(SqlSessionTemplate sqlSession, int activityId) {
		return sqlSession.selectList("Activity.selectProduct", activityId);
	}

	@Override
	public int completeAct(SqlSessionTemplate sqlSession, Activity act) {
		return sqlSession.update("Activity.completeAct", act);
	}

	@Override
	public int deleteAct(SqlSessionTemplate sqlSession, int actId) {
		return sqlSession.update("Activity.deleteAct", actId);
	}



}
