package com.kh.sm.salesActivity.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.Attachment;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesActivity.model.vo.Calendar;

public interface SalesActivityService {

	HashMap<String, Object> clientList(int empNo);

	int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath);

	int insertActivity(Activity activity);

	Activity selectActivity(int activityId);

	HashMap<String, Object> clientListScroll(int empNo, int lastCno);

	HashMap<String, Object> clientListByName(int empNo, String name);

	List<HashMap<String, Object>> actListTable(int empNo);

	int insertCompanion(String[] companionArr);

	int insertParticipant(String[] participantArr);

	int insertPridate(Calendar cal);

	List<Attachment> selectAttachment(int activityId);

	List<HashMap<String, Object>> selectPriDateList(int empNo);

	List<HashMap<String, Object>> selectActList(int empNo);

	List<HashMap<String, Object>> selectCalList(int empNo, String date);

	List<HashMap<String, Object>> selectActList(int empNo, String date);

	List<HashMap<String, Object>> selectPlanList(int empNo);

	List<HashMap<String, Object>> selectProposeList(int empNo);

	List<HashMap<String, Object>> selectEstList(int empNo);

	List<HashMap<String, Object>> selectContList(int empNo);

	List<HashMap<String, Object>> selectProposeList(int empNo, String date);

	List<HashMap<String, Object>> selectActEstList(int empNo, String date);

	List<HashMap<String, Object>> selectContList(int empNo, String date);

	List<HashMap<String, Object>> selectworkList(int empNo);

	HashMap<String, Object> selectPriDetail(int calId);

	int updatePriDate(Calendar cal);

	int delPridate(int calId);

	HashMap<String, Object> selectActDetail(int activityId);

	List<HashMap<String, Object>> selectParticipant(int activityId);

	List<HashMap<String, Object>> selectCompanion(int activityId);

	int insertProduct(String[] pdtId);

	List<HashMap<String, Object>> selectProduct(int activityId);

	int completeAct(Activity act);

	int deleteAct(int actId);


}
