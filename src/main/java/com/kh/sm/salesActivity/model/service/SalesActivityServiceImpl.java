package com.kh.sm.salesActivity.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.common.Attachment;
import com.kh.sm.salesActivity.model.dao.SalesActivityDao;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesActivity.model.vo.Calendar;

@Service
public class SalesActivityServiceImpl implements SalesActivityService {
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	public SalesActivityDao sd;
	
	//고객목록 조회 메소드
	@Override
	public HashMap<String, Object> clientList(int empNo) {
		HashMap<String, Object> hmap = sd.clientList(sqlSession, empNo);
		
		return hmap;
	}
	
	//첨부파일 저장 메소드
	@Override
	public int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath) {
		int result = 0;
		Attachment attc = null;
		for(int i = 0; i < fList.size(); i++) {
			attc = new Attachment();
			attc.setCategory(7);
			attc.setChangeName(changeNames.get(i));
			attc.setFilePath(filePath);
			attc.setOriginName(fList.get(i).getOriginalFilename());
			attc.setDelYn("N");
			result += sd.insertAttach(sqlSession, attc);			
		}
		
		
		return result;
	}

	@Override
	public int insertActivity(Activity activity) {
		int result = 0;
		result = sd.insertActivity(sqlSession, activity);
		
		return result;
	}

	@Override
	public Activity selectActivity(int activityId) {
		Activity activity = sd.selectActivity(sqlSession, activityId);
		
		return activity;
	}

	@Override
	public HashMap<String, Object> clientListScroll(int empNo, int lastCno) {
		HashMap<String, Object> hmap = sd.clientListScroll(sqlSession, empNo, lastCno);
	
		return hmap;
	}

	@Override
	public HashMap<String, Object> clientListByName(int empNo, String name) {
		HashMap<String, Object> hmap = sd.clientListByName(sqlSession, empNo, name);
	
		return hmap;
	}

	@Override
	public List<HashMap<String, Object>> actListTable(int empNo) {
		List<HashMap<String, Object>> actListTable = sd.actListTable(sqlSession, empNo);
		
		return actListTable;
	}

	@Override
	public int insertCompanion(String[] companionArr) {
		int result = 0;
		for(int i = 0; i < companionArr.length; i++) {
			int companion = Integer.parseInt(companionArr[i]);
			result += sd.insertCompanion(sqlSession, companion);
		}
		return result;
	}

	@Override
	public int insertParticipant(String[] participantArr) {
		int result = 0;
		for(int i = 0; i < participantArr.length; i++) {
			int participant = Integer.parseInt(participantArr[i]);
			result += sd.insertParticipant(sqlSession, participant);
		}
		return result;
	}

	@Override
	public int insertPridate(Calendar cal) {
		int result = 0;
		
		result = sd.insertPriDate(sqlSession, cal);
		
		return result;
	}

	@Override
	public List<Attachment> selectAttachment(int activityId) {
		List<Attachment> list = sd.selectAttachment(activityId, sqlSession);
		return list;
	}
	
	//개인일정 캘린더로 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectPriDateList(int empNo) {
		List<HashMap<String, Object>> priDateList = sd.selectPriDateList(sqlSession, empNo);
		return priDateList;
	}
	
	//영업활동 캘린더로 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectActList(int empNo) {
		List<HashMap<String, Object>> actList = sd.selectActList(sqlSession, empNo);
		return actList;
	}

	//선택한 날짜에 대한 정보들을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectCalList(int empNo, String date) {
		List<HashMap<String, Object>> calList = sd.selectCalList(sqlSession, empNo, date);
		return calList;
	}
	
	//선택한 날짜에 대한 영업활동 정보 조회 메소드
	@Override
	public List<HashMap<String, Object>> selectActList(int empNo, String date) {
		List<HashMap<String, Object>> actList = sd.selectDateActList(sqlSession, empNo, date);
		return actList;
	}

	//캘린더에 뿌려줄 영업계획을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectPlanList(int empNo) {
		List<HashMap<String, Object>> planList = sd.selectPlanList(sqlSession, empNo);
		return planList;
	}

	//캘린더에 뿌려줄 제안을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectProposeList(int empNo) {
		List<HashMap<String, Object>> proposeList = sd.selectProposeList(sqlSession, empNo);
		return proposeList;
	}
	
	//캘린더에 뿌려줄 견적을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectEstList(int empNo) {
		List<HashMap<String, Object>> estList = sd.selectEstList(sqlSession, empNo);
		return estList;
	}
	
	//캘린더에 뿌려줄 계약을 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectContList(int empNo) {
		List<HashMap<String, Object>> contList = sd.selectContList(sqlSession, empNo);
		return contList;
	}
	
	//선택한 날짜의 제안정보를 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectProposeList(int empNo, String date) {
		List<HashMap<String, Object>> proposeDateList = sd.selectProposeList(sqlSession, empNo, date);
		return proposeDateList;
	}

	//선택한 날짜의 견적정보를 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectActEstList(int empNo, String date) {
		List<HashMap<String, Object>> estDateList = sd.selectActEstList(sqlSession, empNo, date);
		return estDateList;
	}

	//선택한 날짜의 계약정보를 조회하는 메소드
	@Override
	public List<HashMap<String, Object>> selectContList(int empNo, String date) {
		List<HashMap<String, Object>> contDateList = sd.selectContList(sqlSession, empNo, date);
		return contDateList;
	}

	@Override
	public List<HashMap<String, Object>> selectworkList(int empNo) {
		List<HashMap<String, Object>> workList = sd.selectWorkList(sqlSession, empNo);
		return workList;
	}

	@Override
	public HashMap<String, Object> selectPriDetail(int calId) {
		HashMap<String, Object> priDetail = sd.selectPriDetail(sqlSession, calId);
		return priDetail;
	}

	@Override
	public int updatePriDate(Calendar cal) {
		int result = sd.updatePriDate(sqlSession, cal);
		return result;
	}

	@Override
	public int delPridate(int calId) {
		int result = sd.delPridate(sqlSession, calId);
		return result;
	}

	@Override
	public HashMap<String, Object> selectActDetail(int activityId) {
		HashMap<String, Object> actDetail = sd.selectActDetail(sqlSession, activityId);
		return actDetail;
	}
	
	//참가자 조회 메소드
	@Override
	public List<HashMap<String, Object>> selectParticipant(int activityId) {
		List<HashMap<String, Object>> participant = sd.selectParticipant(sqlSession, activityId);
		return participant;
	}

	//동반 인력 조회 메소드
	@Override
	public List<HashMap<String, Object>> selectCompanion(int activityId) {
		List<HashMap<String, Object>> companion = sd.selectCompanion(sqlSession, activityId);
		return companion;
	}

	@Override
	public int insertProduct(String[] pdtId) {
		int result = 0; 
		for(int i = 0; i < pdtId.length; i++) {
			int pdtIdd = Integer.parseInt(pdtId[i]);
			result += sd.insertProduct(sqlSession, pdtIdd);
		}
		return result;
	}

	@Override
	public List<HashMap<String, Object>> selectProduct(int activityId) {
		List<HashMap<String, Object>> productList = sd.selectProduct(sqlSession, activityId);
		return productList;
	}

	@Override
	public int completeAct(Activity act) {
		int result = sd.completeAct(sqlSession, act);
		return result;
	}

	@Override
	public int deleteAct(int actId) {
		int result = sd.deleteAct(sqlSession, actId);
		return result;
	}
	
}
