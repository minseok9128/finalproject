package com.kh.sm.salesActivity.model.vo;

import java.sql.Date;
//영업활동 객체
public class Activity {
	private int activityId;
	private int oppId;			//영업기회ID
	private int category; 		//활동분류
	private int purpose; 		//활동목적
	private Date activityDate; 	//활동날짜
	private String activityStart;	//활동시작시간
	private String activityEnd;	//활동종료시간
	private String completeYn;	//완료여부
	private String planCon;		//계획내용
	private String activityCon;	//활동내용
	private int empNo;			//담당자
	private int clientId;		//고객
	
	public Activity() {}

	public Activity(int activityId, int oppId, int category, int purpose, Date activityDate, String activityStart,
			String activityEnd, String completeYn, String planCon, String activityCon, int empNo, int clientId) {
		super();
		this.activityId = activityId;
		this.oppId = oppId;
		this.category = category;
		this.purpose = purpose;
		this.activityDate = activityDate;
		this.activityStart = activityStart;
		this.activityEnd = activityEnd;
		this.completeYn = completeYn;
		this.planCon = planCon;
		this.activityCon = activityCon;
		this.empNo = empNo;
		this.clientId = clientId;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPurpose() {
		return purpose;
	}

	public void setPurpose(int purpose) {
		this.purpose = purpose;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityStart() {
		return activityStart;
	}

	public void setActivityStart(String activityStart) {
		this.activityStart = activityStart;
	}

	public String getActivityEnd() {
		return activityEnd;
	}

	public void setActivityEnd(String activityEnd) {
		this.activityEnd = activityEnd;
	}

	public String getCompleteYn() {
		return completeYn;
	}

	public void setCompleteYn(String completeYn) {
		this.completeYn = completeYn;
	}

	public String getPlanCon() {
		return planCon;
	}

	public void setPlanCon(String planCon) {
		this.planCon = planCon;
	}

	public String getActivityCon() {
		return activityCon;
	}

	public void setActivityCon(String activityCon) {
		this.activityCon = activityCon;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	@Override
	public String toString() {
		return "Activity [activityId=" + activityId + ", oppId=" + oppId + ", category=" + category + ", purpose="
				+ purpose + ", activityDate=" + activityDate + ", activityStart=" + activityStart + ", activityEnd="
				+ activityEnd + ", completeYn=" + completeYn + ", planCon=" + planCon + ", activityCon=" + activityCon
				+ ", empNo=" + empNo + ", clientId=" + clientId + "]";
	}
	
	
}
