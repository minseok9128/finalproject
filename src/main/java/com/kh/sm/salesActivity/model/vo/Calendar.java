package com.kh.sm.salesActivity.model.vo;

import java.sql.Date;

public class Calendar {
	private int calId;
	private int calCategory;
	private Date calDate;
	private String calStart;
	private String calEnd;
	private String calContent;
	private int empNo;
	private String calDelYn;
	
	public Calendar() {}

	public Calendar(int calId, int calCategory, Date calDate, String calStart, String calEnd, String calContent,
			int empNo, String calDelYn) {
		super();
		this.calId = calId;
		this.calCategory = calCategory;
		this.calDate = calDate;
		this.calStart = calStart;
		this.calEnd = calEnd;
		this.calContent = calContent;
		this.empNo = empNo;
		this.calDelYn = calDelYn;
	}

	public int getCalId() {
		return calId;
	}

	public void setCalId(int calId) {
		this.calId = calId;
	}

	public int getCalCategory() {
		return calCategory;
	}

	public void setCalCategory(int calCategory) {
		this.calCategory = calCategory;
	}

	public Date getCalDate() {
		return calDate;
	}

	public void setCalDate(Date calDate) {
		this.calDate = calDate;
	}

	public String getCalStart() {
		return calStart;
	}

	public void setCalStart(String calStart) {
		this.calStart = calStart;
	}

	public String getCalEnd() {
		return calEnd;
	}

	public void setCalEnd(String calEnd) {
		this.calEnd = calEnd;
	}

	public String getCalContent() {
		return calContent;
	}

	public void setCalContent(String calContent) {
		this.calContent = calContent;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getCalDelYn() {
		return calDelYn;
	}

	public void setCalDelYn(String calDelYn) {
		this.calDelYn = calDelYn;
	}

	@Override
	public String toString() {
		return "Calendar [calId=" + calId + ", calCategory=" + calCategory + ", calStart=" + calStart + ", calEnd="
				+ calEnd + ", calContent=" + calContent + ", empNo=" + empNo + ", calDelYn=" + calDelYn + "]";
	}
	
	
}
