package com.kh.sm.salesActivity.model.vo;

public class Companion {
	private int activityId;
	private int empNo;
	
	public Companion() {}

	public Companion(int activityId, int empNo) {
		super();
		this.activityId = activityId;
		this.empNo = empNo;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "Companion [activityId=" + activityId + ", empNo=" + empNo + "]";
	}
	
	
}
