package com.kh.sm.salesGoal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.salesGoal.model.service.GoalManagementService;
import com.kh.sm.salesGoal.model.vo.Goal;
import com.kh.sm.salesGoal.model.vo.PersonalGoal;

@Controller
public class SalesGoalManagementController {

	@Autowired
	private final GoalManagementService service;
	
	public SalesGoalManagementController(GoalManagementService service) {
		this.service = service;
	}
	
	@RequestMapping(value="showGoalManagement.sgm", method=RequestMethod.GET) 
	public ModelAndView showGoalManagement(@ModelAttribute("Goal") Goal goal, HttpServletRequest request, Model model) {
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		
		List<Goal> goalList = service.selectGoalList(empNo);
		
		ModelAndView mv = new ModelAndView("sales/salesManagement/goalManagement");
		mv.addObject("goalList", goalList);
		
		return mv;
	}
	
	@PostMapping("updateGoal.sgm") 
	public String updateGoalManagement(Model model, HttpServletRequest request, Goal goal) {
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		goal.setEmpNo(empNo);
		
		int result = service.updateGoal(goal);
		
		if(result > 0) {
			
			return "redirect:showGoalManagement.sgm";
		}else {
			model.addAttribute("msg", "목표 업데이트 실패!");
			
			return "common/errorPage";
		}
	}
	
	@PostMapping("searhGoalList.sgm")
	public ModelAndView searhGoalList(ModelAndView mv, @RequestParam("date") String date, @RequestParam("selectId") int selectId, HttpServletRequest request) {
		
		Goal g = new Goal();
		g.setYear(date);
		g.setPdtId(selectId);
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		g.setEmpNo(empNo);
		
		PersonalGoal pg = new PersonalGoal();
		
		List<PersonalGoal> searchGoalList= new ArrayList<PersonalGoal>();
		
		searchGoalList= service.searchGoalList(g);
		System.out.println(searchGoalList);
		
		mv.addObject("searchGoalList", searchGoalList);
		
		mv.setViewName("jsonView");
		
		return mv;
	}
}