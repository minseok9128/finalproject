package com.kh.sm.salesGoal.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.salesGoal.model.vo.Goal;
import com.kh.sm.salesGoal.model.vo.PersonalGoal;

public interface GoalManagementDao {

	int updateGoal(SqlSessionTemplate sqlSession, Goal g);

	List<Goal> selectGoalList(SqlSessionTemplate sqlSession, int empNo);

	List<PersonalGoal> searchGoalList(SqlSessionTemplate sqlSession, Goal g);

}