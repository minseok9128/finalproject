package com.kh.sm.salesGoal.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.salesGoal.model.vo.Goal;
import com.kh.sm.salesGoal.model.vo.PersonalGoal;

@Repository
public class GoalManagementDaoImpl implements GoalManagementDao{

	@Override
	public int updateGoal(SqlSessionTemplate sqlSession, Goal g) {
		
		int selectResult = sqlSession.selectOne("Goal.extCheck", g);
		
		int result = 0;
		
		if(selectResult > 0) {
			result = sqlSession.update("Goal.updateGoal", g);
		}else {
			result = sqlSession.insert("Goal.insertGoal", g);
		}
		
		return result;
	}

	@Override
	public List<Goal> selectGoalList(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectList("Goal.selectGoalLiset", empNo);
	}

	@Override
	public List<PersonalGoal> searchGoalList(SqlSessionTemplate sqlSession, Goal g) {
		
		return sqlSession.selectList("Goal.searchGoalList", g);
	}
	
}