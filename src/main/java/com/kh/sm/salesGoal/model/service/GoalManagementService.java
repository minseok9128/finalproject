package com.kh.sm.salesGoal.model.service;

import java.util.List;

import com.kh.sm.salesGoal.model.vo.Goal;
import com.kh.sm.salesGoal.model.vo.PersonalGoal;

public interface GoalManagementService {

	List<Goal> selectGoalList(int empNo);

	int updateGoal(Goal goal);

	List<PersonalGoal> searchGoalList(Goal g);
	
}