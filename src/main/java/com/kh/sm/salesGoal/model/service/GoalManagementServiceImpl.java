package com.kh.sm.salesGoal.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.salesGoal.model.dao.GoalManagementDao;
import com.kh.sm.salesGoal.model.vo.Goal;
import com.kh.sm.salesGoal.model.vo.PersonalGoal;

@Service
public class GoalManagementServiceImpl implements GoalManagementService{
	@Autowired
	private GoalManagementDao gmd;
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public int updateGoal(Goal g) {
		
		int result = gmd.updateGoal(sqlSession, g);
		
		return result;
	}

	@Override
	public List<Goal> selectGoalList(int empNo) {
		List<Goal> goalList = null;
		
		goalList = gmd.selectGoalList(sqlSession, empNo);
		
		return goalList;
	}

	@Override
	public List<PersonalGoal> searchGoalList(Goal g) {
		
		return gmd.searchGoalList(sqlSession, g);
	}
	
}