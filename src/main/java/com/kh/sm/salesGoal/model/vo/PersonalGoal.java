package com.kh.sm.salesGoal.model.vo;

public class PersonalGoal {
	private int goalId;
	private String cateogry;
	private int goalValue;
	private String year;
	private int pdtId;
	private String pdtName;
	private int empNo;
	private int one;
	private int two;
	private int three;
	private int four;
	private int five;
	private int six;
	private int seven;
	private int eight;
	private int nine;
	private int ten;
	private int eleven;
	private int twelve;
	
	public PersonalGoal() {}

	public PersonalGoal(int goalId, String cateogry, int goalValue, String year, int pdtId, String pdtName, int empNo,
			int one, int two, int three, int four, int five, int six, int seven, int eight, int nine, int ten,
			int eleven, int twelve) {
		super();
		this.goalId = goalId;
		this.cateogry = cateogry;
		this.goalValue = goalValue;
		this.year = year;
		this.pdtId = pdtId;
		this.pdtName = pdtName;
		this.empNo = empNo;
		this.one = one;
		this.two = two;
		this.three = three;
		this.four = four;
		this.five = five;
		this.six = six;
		this.seven = seven;
		this.eight = eight;
		this.nine = nine;
		this.ten = ten;
		this.eleven = eleven;
		this.twelve = twelve;
	}

	public int getGoalId() {
		return goalId;
	}

	public void setGoalId(int goalId) {
		this.goalId = goalId;
	}

	public String getCateogry() {
		return cateogry;
	}

	public void setCateogry(String cateogry) {
		this.cateogry = cateogry;
	}

	public int getGoalValue() {
		return goalValue;
	}

	public void setGoalValue(int goalValue) {
		this.goalValue = goalValue;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public int getPdtId() {
		return pdtId;
	}

	public void setPdtId(int pdtId) {
		this.pdtId = pdtId;
	}

	public String getPdtName() {
		return pdtName;
	}

	public void setPdtName(String pdtName) {
		this.pdtName = pdtName;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public int getOne() {
		return one;
	}

	public void setOne(int one) {
		this.one = one;
	}

	public int getTwo() {
		return two;
	}

	public void setTwo(int two) {
		this.two = two;
	}

	public int getThree() {
		return three;
	}

	public void setThree(int three) {
		this.three = three;
	}

	public int getFour() {
		return four;
	}

	public void setFour(int four) {
		this.four = four;
	}

	public int getFive() {
		return five;
	}

	public void setFive(int five) {
		this.five = five;
	}

	public int getSix() {
		return six;
	}

	public void setSix(int six) {
		this.six = six;
	}

	public int getSeven() {
		return seven;
	}

	public void setSeven(int seven) {
		this.seven = seven;
	}

	public int getEight() {
		return eight;
	}

	public void setEight(int eight) {
		this.eight = eight;
	}

	public int getNine() {
		return nine;
	}

	public void setNine(int nine) {
		this.nine = nine;
	}

	public int getTen() {
		return ten;
	}

	public void setTen(int ten) {
		this.ten = ten;
	}

	public int getEleven() {
		return eleven;
	}

	public void setEleven(int eleven) {
		this.eleven = eleven;
	}

	public int getTwelve() {
		return twelve;
	}

	public void setTwelve(int twelve) {
		this.twelve = twelve;
	}

	@Override
	public String toString() {
		return "PersonGoal [goalId=" + goalId + ", cateogry=" + cateogry + ", goalValue=" + goalValue + ", year=" + year
				+ ", pdtId=" + pdtId + ", pdtName=" + pdtName + ", empNo=" + empNo + ", one=" + one + ", two=" + two
				+ ", three=" + three + ", four=" + four + ", five=" + five + ", six=" + six + ", seven=" + seven
				+ ", eight=" + eight + ", nine=" + nine + ", ten=" + ten + ", eleven=" + eleven + ", twelve=" + twelve
				+ "]";
	}
	
}
