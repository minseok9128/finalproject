package com.kh.sm.salesManagement.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.kh.sm.offer.model.service.OfferService;
import com.kh.sm.offer.model.vo.Offer;



@Controller
public class SalesManagementController {
	
	@Autowired
	private final OfferService os;
	
	public SalesManagementController(OfferService os) {
		this.os = os;
	}
	
	
	//제안
	//===================================
	//이동하는 메소드
	@GetMapping("showOfferEnrollment.sm")
	public String showPropseEnrollmentView(Model model, HttpServletRequest request) {
		
		int ppsId = 0;
		
		request.setAttribute("ppsId", ppsId);
		return "sales/salesManagement/offerEnrollment";
	}
	@GetMapping("showOfferDetail.sm")
	public String showOfferDetail() {
		return "sales/salesManagement/offerDetail";
	}
	//==============
	//값 가져오는 메소드
	@PostMapping("insertOfferEnrollment.sm")
	public String insertOfferEnrollment(Model model, Offer of, 
			@RequestParam(name="reday") String reday, @RequestParam(name="suday") String suday) {

		return "sales/salesManagement/offerEnrollment";
		
	};
	
	//===================================
	//견적
	//===================================
	@GetMapping("showEstimateMain.sm")
	public String showEstimateMain() {
		return "sales/salesManagement/estimateMain";
	}
	@RequestMapping("showEstimateEnrollment.sm")
	public String showEstimateEnrollment(HttpServletRequest request) {
		
		request.setAttribute("estId", 0);
		return "sales/salesManagement/estimateEnrollment";
	}
	
	@RequestMapping("showEstimateEnrollment2.sm")
	public String showEstimateEnrollment2(HttpServletRequest request,int oppId) {
		
		request.setAttribute("oppId", oppId);
		request.setAttribute("estId", 0);
		
		
		
		
		return "sales/salesManagement/estimateEnrollment2";
	}
	@RequestMapping("showEstimateDetail.sm")
	public String showEstimateDetail() {
		return "sales/salesManagement/estimateDetail";
	}
	//===================================
	//계약
	//===================================
	@RequestMapping("showContractMain.sm")
	public String showContractMain() {
		return "sales/salesManagement/contractMain";
	}
	@RequestMapping("showContractEnrollment.sm")
	public String showContractEnrollment() {
		return "sales/salesManagement/contractEnrollment";
	}
	@RequestMapping("showContractDetail.sm")
	public String showContractDetail() {
		return "sales/salesManagement/contractDetail";
	}
	//===================================
	//매출조회 
	//===================================
	@RequestMapping("showSalesInquiry.sm")
	public String showSalesInquiry() {
		return "sales/salesManagement/salesInquiry";
	}
	//===================================
	@RequestMapping(value="showGoalManagement.ms", method=RequestMethod.GET) 
	public String showGoalManagement() {
		
		return "sales/salesManagement/goalManagement";
	} 
	//===================================
	@RequestMapping("showCustomerContactStatus.sm")
	public String showMonthCustomerContactStatus() {
		return "admin/salesStatus/monthCustomerContactStatus";
	}
	@RequestMapping("showMonthSalseStatus.sm")
	public String showSalseStatus() {
		return "admin/salesStatus/monthSealseStatus";
	}
	@RequestMapping("showMonthBusinessStatus.sm")
	public String showMonthBusinessStatus() {
		return "admin/salesStatus/monthbusinessStatus";
	}
	@RequestMapping("showCustomerStatus.sm")
	public String showCustomerStatus() {
		return "status/clientStatus/customerStatus";
	}
	
}
