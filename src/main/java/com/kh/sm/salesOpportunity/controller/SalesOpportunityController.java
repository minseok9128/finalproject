package com.kh.sm.salesOpportunity.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.kh.sm.common.Attachment;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.estimate.model.vo.Estimate;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.relationClient.model.vo.RelationClient;
import com.kh.sm.salesOpportunity.model.service.OpportunityService;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.support.model.vo.Support;

@Controller
public class SalesOpportunityController {

	@Autowired
	private final OpportunityService os;

	public SalesOpportunityController(OpportunityService os) {
		this.os = os;
	}

	@RequestMapping(value="select.so", method=RequestMethod.GET)
	public String loginCheck() {


		return "sales/salesOpportunity/salesOpportunity";
	}
	
	@GetMapping("opportunityMain.so")
	public String selectOpportunityMain(HttpServletRequest request ){
		
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		
		Opportunity opp = new Opportunity();
		opp.setEmpNo(empNo);
		
		
		List<Opportunity> list = os.selectOpporMain(opp);
		int OppCount = os.selectOppCount(opp);
		
		request.setAttribute("list", list);
		request.setAttribute("OppCount", OppCount);
		return "sales/salesOpportunity/salesOpportunity";
	}
	
	
	@PostMapping("oppMainscroll.so")
	public void estMainscroll(ModelAndView mv, @RequestParam("lastCno") int lastCno, HttpServletResponse response) {
		
		
		List<Opportunity> list = os.selectOpporMain(lastCno);
		
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			new Gson().toJson(list, response.getWriter());
		} catch (JsonIOException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	



	@RequestMapping(value="showOpprotunityEnroll.so", method=RequestMethod.GET) 
	public String showNoticeDetail() {

		return "sales/salesOpportunity/salesOpportunityEnroll";
	}

	@PostMapping("selectOpportunityModal.so")
	public void selectOpportunityModal(HttpServletResponse response, @RequestParam("memberId") int memberId) {
		List<Opportunity> list = null;
		
		list = os.selectOppertunityModal(memberId);
 

		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			new Gson().toJson(list, response.getWriter());
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@PostMapping("updateOpportunity.so")
	public String updateOpportunity(Opportunity opp,Model model) {
		
		
		System.out.println("수정할값 들어오나요? : " + opp);
		
		int updateOpp = os.updateOpp(opp);
		
		if(updateOpp > 0) {
			
			model.addAttribute("updateMsg","영업기회수정이 완료되었습니다.");
  			model.addAttribute("bb",1);
  			return "sales/salesOpportunity/successPage";
			
		}else { 
			  model.addAttribute("msg","영업기회수정 실패!"); 
			  return "common/errorPage";
		  
		  }
		
		
	}
	
	

	//영업기회 등록
	@PostMapping("insertOpportunityEnrollment.so")
	public String insertOpportunityEnrollment(Opportunity opp,ProductDetail proDetail,
			Model model, MultipartHttpServletRequest multiRequest,HttpServletRequest request ) {

	String root = request.getSession().getServletContext().getRealPath("resources");
	String filePath = root +"\\uploadFiles\\files";
	List<MultipartFile> fList = multiRequest.getFiles("files");
	
	ArrayList<String> changeNames = new ArrayList<>();
	
	for(int i = 0; i < fList.size(); i++) {
		
		String originFileName = fList.get(i).getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();
		
		try {
			fList.get(i).transferTo(new File(filePath + "\\" + changeName + ext));
		} catch (IllegalStateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		changeNames.add(changeName);
	}
	
	//연관 고객 insert 관련
	String[] ClientId = request.getParameterValues("clientId2");
	
	ArrayList<RelationClient> rList = new ArrayList<RelationClient>();
	
	if( ClientId != null) {
	for(int i = 0; i < ClientId.length; i++) {
		RelationClient rc = new RelationClient();
		rc.setClientId(Integer.parseInt(ClientId[i]));
		
		rList.add(rc);
	}
	}
	
	
	//프로세스 테이블 insert관련
	OppProcess process = new OppProcess();
	process.setProcessStage(Integer.parseInt(request.getParameter("processStage")));
	process.setProcessSuccess(Integer.parseInt(request.getParameter("processSuccess")));
	
	String[] Category = request.getParameterValues("CATEGORY");
	String[] Enroller = request.getParameterValues("enroller");
	String[] EmpNo = request.getParameterValues("empNo2");
	
	//지원이력 테이블 insert 관련
	ArrayList<Support> sList = new ArrayList<Support>();
	
	if(Enroller != null) {
		for(int i =0; i<Enroller.length; i++) {
			Support support = new Support();
			support.setCategory(Integer.parseInt(Category[i]));
			support.setEnroller(Integer.parseInt(Enroller[i]));
			support.setEmpNo(Integer.parseInt(EmpNo[i]));
			
			sList.add(support);
		}
	}
	
	
	
	
	String[] DiscountRate = request.getParameterValues("discountRate");
	String[] TotalPrice = request.getParameterValues("totalPrice");
	String[] Quantity = request.getParameterValues("quantity");
	String[] PdtId = request.getParameterValues("pdtId");
	String[] DetailPrice = request.getParameterValues("price");
	
	ArrayList<ProductDetail> pdList = new ArrayList<ProductDetail>();
	
	
	
	if(DiscountRate != null) {
		for(int i = 0; i < DiscountRate.length; i++) {
			 proDetail = new ProductDetail();
			 proDetail.setDiscountRate(Integer.parseInt(DiscountRate[i]));
			 proDetail.setTotalPrice(Integer.parseInt(TotalPrice[i]));
			 proDetail.setQuantity(Integer.parseInt(Quantity[i]));
			 proDetail.setPdtId(Integer.parseInt(PdtId[i]));
			 proDetail.setCategory("OPP");
			 proDetail.setDetailPrice(Integer.parseInt(DetailPrice[i]));
			
			 pdList.add(proDetail);
		}
		
	}
	

	
	
	//영업기회 등록시 순서
	//1.프로세스 테이블 insert
	//2.영업기회 테이블 insert
	//3.연관 고객 테이블insert(널값 처리) , 첨부파일(널값처리), 지원이력 테이블(널값 처리) , 영업기회제품(널값처리)
	
	
	// 프로세스 테이블에 insert부터 하고 기본키가져와서 영업기회 insert하기!
	int result = os.insertProcess(process);
	int result2 = 0;
	
	
		  if(result > 0) { 
			  //영업기회 등록
			  result2 = os.insertOpportunity(opp);
			  
			  
			  	if(ClientId != null) {
				  //연관고객 등록
				  int result3 = os.insertRelationClient(rList);
			  	}
			  	if(fList != null) {
			  		//첨부파일 등록
				  int result4 = os.insertAttach(fList, changeNames, filePath);
			  	}
			  	if(sList != null) {
			  		//지원인력 insert
			  		int result5 = os.insertSupport(sList);
			  		
			  	}
			  	if(pdList != null) {
			  		//영업기회제품 insert
			  		int result6 = os.insertProduct(pdList);
			  	}
			  
			  		if(result2 >0) {
			  			
			  			model.addAttribute("msg","영업기회등록이 완료되었습니다.");
			  			model.addAttribute("aa",1);
			  			return "sales/salesOpportunity/successPage";
			  			
			  		}else { 
			  			model.addAttribute("msg","영업기회 등록 실패!"); 
			  			return "common/errorPage";
		  
			  		}
		  
		  }else { 
			  model.addAttribute("msg","프로세스 등록 실패!"); 
			  return "common/errorPage";
		  
		  }
	
}
	
	
	@GetMapping("SelectOneUpdate.so")
	public String updateOpp(Model model, Opportunity opp) {
		
		Opportunity updateOppOne = os.selectUpdateOppOne(opp);
		
		model.addAttribute("updateOppOne",updateOppOne);
		
		return "sales/salesOpportunity/salesOpportunityUpdate";
	}
	
	
	@GetMapping("selectOne.so")
	public String selectOneOpp(Model model, Opportunity opp) {
		
		
		
		 Opportunity oppOne = os.selectOneOpp(opp); 
		 List<RelationClient> rcList = os.selectRelationClient(opp);
		 int rcCount = os.selectRcCount(opp);
		 
		 
		 //지원인력 select
		 List<Support> sList = os.selectSupport(opp);
		 
		 int sCount = os.selectSCount(opp);
		 
		 List<Attachment> oppoAtt = os.selectAtt(opp);
		 
		List<ProductDetail> pdList = os.selectProductDetail(opp);
		int pdCount = os.selectpdCount(opp);
		

		model.addAttribute("oppOne",oppOne);
		model.addAttribute("rcList",rcList);
		model.addAttribute("rcCount",rcCount);
		model.addAttribute("sList",sList);
		model.addAttribute("sCount",sCount);
		model.addAttribute("oppoAtt",oppoAtt);
		model.addAttribute("pdList",pdList);
		model.addAttribute("pdCount",pdCount);
		return "sales/salesOpportunity/salesOpportunityDetail";
		
	}
	
	
	@GetMapping("selectStatus.so")
	public String selectStatusOpp(Model model) {
		
		HashMap<String,Object> hmap = os.selectStatus();
		
		model.addAttribute("hmap" , hmap);
		return "status/OpportunityStatus/opportunityStatus";
	
	}
	//영업기회 데이터 
	@GetMapping("selectStatusData.so")
	public String selectStatusOppData(Model model) {
		
		List<HashMap<String,Object>> hmap = os.selectStatusData();
		System.out.println("hamp : " + hmap);
		model.addAttribute("hmap" , hmap);
		return "status/OpportunityStatus/opportunityDataStatus";
	
	}
	

}
