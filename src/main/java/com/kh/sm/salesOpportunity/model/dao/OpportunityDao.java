package com.kh.sm.salesOpportunity.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.relationClient.model.vo.RelationClient;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.support.model.vo.Support;

public interface OpportunityDao {

	List<Opportunity> selectOppertunityModal(int memberId, SqlSessionTemplate sqlSession);

	List<Opportunity> selectOpporMain(SqlSessionTemplate sqlSession,Opportunity opp);
	
	List<Opportunity> selectOpporMain(SqlSessionTemplate sqlSession,int lastCno);

	//영입기회등록시 프로세스 insert먼저!
	int insertProcess(SqlSessionTemplate sqlSession, OppProcess process);

	//영업기회 등록
	int insertOpportunity(SqlSessionTemplate sqlSession, Opportunity opp);

	//연관고객 insert
	int insertRelationClient(SqlSessionTemplate sqlSession, ArrayList<RelationClient> rList);

	//첨부파일 insert
	int insertAttach(SqlSessionTemplate sqlSession, Attachment attc);

	//지원인력 insert
	int insertSupport(SqlSessionTemplate sqlSession, ArrayList<Support> sList);

	//영업기회 세부조회
	Opportunity selectOneOpp(SqlSessionTemplate sqlSession, Opportunity opp);

	//영업기회 세부조회시 연관고객 조회
	List<RelationClient> selectRelationClient(SqlSessionTemplate sqlSession,Opportunity opp);

	int selectRcCount(SqlSessionTemplate sqlSession, Opportunity opp);

	//영업기회 세부조회시 지원인력 조회
	List<Support> selectSupport(SqlSessionTemplate sqlSession, Opportunity opp);

	//지원인력수 조회
	int selectSCount(SqlSessionTemplate sqlSession, Opportunity opp);

	//영업기회 첨부파일 select
	List<Attachment> selectAtt(SqlSessionTemplate sqlSession, Opportunity opp);

	//영업기회 관련제품 insert
	int insertProduct(SqlSessionTemplate sqlSession, ArrayList<ProductDetail> pdList);

	//영업기회제품 select
	List<ProductDetail> selectProductDetail(SqlSessionTemplate sqlSession, Opportunity opp);

	int selectpdCount(SqlSessionTemplate sqlSession, Opportunity opp);

	List<Opportunity> selectOpporMain(SqlSessionTemplate sqlSession);

	HashMap<String, Object> selectStatus(SqlSessionTemplate sqlSession);

	int selectOppCount(SqlSessionTemplate sqlSession,Opportunity opp);

	List<HashMap<String,Object>> selectStatusData(SqlSessionTemplate sqlSession);
	
	Opportunity selectUpdateOppOne(SqlSessionTemplate sqlSession, Opportunity opp);

	int updateOpp(SqlSessionTemplate sqlSession, Opportunity opp);
	

}
