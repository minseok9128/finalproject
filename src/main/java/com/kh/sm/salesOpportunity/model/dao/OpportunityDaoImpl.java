package com.kh.sm.salesOpportunity.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Controller;

import com.kh.sm.common.Attachment;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.relationClient.model.vo.RelationClient;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.support.model.vo.Support;

@Controller
public class OpportunityDaoImpl implements OpportunityDao{

	//영업기회 모달에 필요한 영업기회 정보 출력
	@Override
	public List<Opportunity> selectOppertunityModal(int memberId, SqlSessionTemplate sqlSession) {
		List<Opportunity> list = sqlSession.selectList("Opportunity.selectOpp", memberId);
		return list;
	}

	//영입기회 메인화면 select
	@Override
	public List<Opportunity> selectOpporMain(SqlSessionTemplate sqlSession,Opportunity opp) {
		
		
		return sqlSession.selectList("Opportunity.selectMain", opp);
	}
	
	@Override
	public List<Opportunity> selectOpporMain(SqlSessionTemplate sqlSession,int lastCno) {
		
		RowBounds row = new RowBounds(lastCno+1, 20);
		return sqlSession.selectList("Opportunity.selectScroll",  row);
	}

	@Override
	public int insertProcess(SqlSessionTemplate sqlSession, OppProcess process) {
		
		return sqlSession.insert("Opportunity.insertProcess",process);
	}

	//영업기회 등록
	@Override
	public int insertOpportunity(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		return sqlSession.insert("Opportunity.insertOpportunity",opp);
	}

	//연관고객 insert
	@Override
	public int insertRelationClient(SqlSessionTemplate sqlSession, ArrayList<RelationClient> rList) {
	
		int result = 0;
		
		for(RelationClient rc : rList) {
			result += sqlSession.insert("Opportunity.insertRelationClient",rc);
		}
		
		
		return result;
	}
	
	
	//지원인력 insert
	@Override
	public int insertSupport(SqlSessionTemplate sqlSession, ArrayList<Support> sList) {
		int result = 0;
		
		for(Support support : sList) {
			result += sqlSession.insert("Opportunity.insertSupport",support);
		}
		
		return result;
	}

	@Override
	public int insertAttach(SqlSessionTemplate sqlSession, Attachment attc) {
		int result = 0;
		
		
		result = sqlSession.insert("Opportunity.insertFile", attc);
		
		return result;
	}

	//영업기회 세부조회
	@Override
	public Opportunity selectOneOpp(SqlSessionTemplate sqlSession, Opportunity opp) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Opportunity.selectOneOpp",opp);
	}

	//영업기회 세부조회시 연관고객 조회
	@Override
	public List<RelationClient> selectRelationClient(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		return sqlSession.selectList("Opportunity.selectRelationClient",opp);
	}

	//연관고객 인원수 조회
	@Override
	public int selectRcCount(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		return sqlSession.selectOne("Opportunity.selectRcCount",opp);
	}

	@Override
	public List<Support> selectSupport(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		return sqlSession.selectList("Opportunity.selectSupport",opp);
	}

	@Override
	public int selectSCount(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		return sqlSession.selectOne("Opportunity.selectSCount",opp);
	}

	@Override
	public List<Attachment> selectAtt(SqlSessionTemplate sqlSession, Opportunity opp) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("Opportunity.selectAtt" , opp);
	}

	@Override
	public int insertProduct(SqlSessionTemplate sqlSession, ArrayList<ProductDetail> pdList) {
		int result = 0;
		
		for(ProductDetail productDetail : pdList) {
			result += sqlSession.insert("Opportunity.insertProduct",productDetail);
		}
		
		return result;
	}

	@Override
	public List<ProductDetail> selectProductDetail(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		
		
		return sqlSession.selectList("Opportunity.selectProductDetail",opp);
	}

	//영업기회제품 갯수 불러오기
	@Override
	public int selectpdCount(SqlSessionTemplate sqlSession, Opportunity opp) {
		
		return sqlSession.selectOne("Opportunity.selectpdCount",opp);
	}

	@Override
	public List<Opportunity> selectOpporMain(SqlSessionTemplate sqlSession) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("Opportunity.selectMain");
	}

	@Override
	public HashMap<String, Object> selectStatus(SqlSessionTemplate sqlSession) {
		HashMap<String,Object> hmap = new HashMap<>();
		//헤더
		hmap.put("header", sqlSession.selectOne("Opportunity.statusHeader")); 
		//테이블
		hmap.put("tablelist", sqlSession.selectList("Opportunity.statusTable"));
		//이미지
		hmap.put("box", sqlSession.selectList("Opportunity.statusBox"));
		//차트(사원별)
		hmap.put("emp", sqlSession.selectList("Opportunity.statusEmp"));
		//차트(월별)
		hmap.put("month", sqlSession.selectList("Opportunity.statusMonth"));
		
		return hmap;
	}

	@Override
	public int selectOppCount(SqlSessionTemplate sqlSession,Opportunity opp) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Opportunity.selectOppCount",opp);
	}

	@Override
	public List<HashMap<String,Object>> selectStatusData(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Opportunity.statusData");
	}
	public Opportunity selectUpdateOppOne(SqlSessionTemplate sqlSession, Opportunity opp) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Opportunity.selectUpdateOppOne",opp);
	}

	@Override
	public int updateOpp(SqlSessionTemplate sqlSession, Opportunity opp) {
		// TODO Auto-generated method stub
		return sqlSession.update("Opportunity.updateOpp",opp);
	}




	

}
