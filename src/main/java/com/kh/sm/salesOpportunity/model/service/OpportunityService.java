package com.kh.sm.salesOpportunity.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.relationClient.model.vo.RelationClient;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.support.model.vo.Support;

public interface OpportunityService {

	List<Opportunity> selectOppertunityModal(int memberId);

	//메인화면 select
	List<Opportunity> selectOpporMain(Opportunity opp);
	
	//메인화면 select
		List<Opportunity> selectOpporMain(int lastCno);
	//영업기회 등록시 프로세스 insert
	int insertProcess(OppProcess process);

	//영업기회 등록
	int insertOpportunity(Opportunity opp);

	//연관고객 insert
	int insertRelationClient(ArrayList<RelationClient> rList);

	//첨부파일 insert
	int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath);

	//지원인력 insert
	int insertSupport(ArrayList<Support> sList);

	//영업기회 세부조회
	Opportunity selectOneOpp(Opportunity opp);

	//영업기회 세부조회시 연관고객조회
	List<RelationClient> selectRelationClient(Opportunity opp);

	//연관고객 인원수
	int selectRcCount(Opportunity opp);

	//영업기회 세부조회시 지원인력 조회
	List<Support> selectSupport(Opportunity opp);

	//지원인력 인원수
	int selectSCount(Opportunity opp);

	//영업기회 첨부파일 
	List<Attachment> selectAtt(Opportunity opp);

	//영업기회제품 등록
	int insertProduct(ArrayList<ProductDetail> pdList);

	//영업기회제품 select
	List<ProductDetail> selectProductDetail(Opportunity opp);

	//영업기회제품 갯수
	int selectpdCount(Opportunity opp);

	List<Opportunity> selectOpporMain();

	HashMap<String, Object> selectStatus();

	int selectOppCount(Opportunity opp);

	List<HashMap<String,Object>> selectStatusData();
	
	Opportunity selectUpdateOppOne(Opportunity opp);

	int updateOpp(Opportunity opp);

	


}
