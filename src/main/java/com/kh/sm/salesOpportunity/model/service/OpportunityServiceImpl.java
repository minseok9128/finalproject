package com.kh.sm.salesOpportunity.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.product.model.vo.ProductDetail;
import com.kh.sm.relationClient.model.vo.RelationClient;
import com.kh.sm.salesOpportunity.model.dao.OpportunityDao;
import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.support.model.vo.Support;

@Service
public class OpportunityServiceImpl implements OpportunityService {
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private final OpportunityDao od;
	
	public OpportunityServiceImpl(OpportunityDao od) {
		this.od = od;
	}

	@Override
	public List<Opportunity> selectOppertunityModal(int memberId) {
		
		List<Opportunity> list = od.selectOppertunityModal(memberId, sqlSession);
		
		return list;
	}

	//영업기회 메인화면 select
	@Override
	public List<Opportunity> selectOpporMain(Opportunity opp) {
		
		return od.selectOpporMain(sqlSession,opp);
	}
	
	//영업기회 메인화면 select
		@Override
		public List<Opportunity> selectOpporMain(int lastCno) {
			
			return od.selectOpporMain(sqlSession,lastCno);
		}

	@Override
	public int insertProcess(OppProcess process) {
		
		return od.insertProcess(sqlSession,process);
	}

	//영업기회 등록
	@Override
	public int insertOpportunity(Opportunity opp) {
		
		return od.insertOpportunity(sqlSession,opp);
	}

	//연관고객 insert
	@Override
	public int insertRelationClient(ArrayList<RelationClient> rList) {
	
		return od.insertRelationClient(sqlSession,rList);
	}

	@Override
	public int insertAttach(List<MultipartFile> fList, ArrayList<String> changeNames, String filePath) {
int result = 0;
		
		
		Attachment attc = null;
		for(int i = 0; i < fList.size(); i++) {
			attc = new Attachment();
			attc.setCategory(1);
			attc.setChangeName(changeNames.get(i));
			attc.setFilePath(filePath);
			attc.setOriginName(fList.get(i).getOriginalFilename());
			attc.setDelYn("N");
			attc.setFileId(1);
			result += od.insertAttach(sqlSession, attc);	
			
			
		}
		
		return result;
	}

	//지원인력 insert
	@Override
	public int insertSupport(ArrayList<Support> sList) {
		// TODO Auto-generated method stub
		return od.insertSupport(sqlSession,sList);
	}

	//영업기회 세부조회
	@Override
	public Opportunity selectOneOpp(Opportunity opp) {
		
		return od.selectOneOpp(sqlSession,opp);
	}

	//영업기회 세부조회시 연관고객 조회
	@Override
	public List<RelationClient> selectRelationClient(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.selectRelationClient(sqlSession,opp);
	}

	@Override
	public int selectRcCount(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.selectRcCount(sqlSession,opp);
	}

	@Override
	public List<Support> selectSupport(Opportunity opp) {
		
		return od.selectSupport(sqlSession,opp);
	}

	//지원인력 인원수
	@Override
	public int selectSCount(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.selectSCount(sqlSession,opp);
	}

	@Override
	public List<Attachment> selectAtt(Opportunity opp) {
		
		return od.selectAtt(sqlSession,opp);
	}

	@Override
	public int insertProduct(ArrayList<ProductDetail> pdList) {
		// TODO Auto-generated method stub
		return od.insertProduct(sqlSession,pdList);
	}

	@Override
	public List<ProductDetail> selectProductDetail(Opportunity opp) {
		
		return od.selectProductDetail(sqlSession,opp);
	}

	//영업기회제품 갯수
	@Override
	public int selectpdCount(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.selectpdCount(sqlSession,opp);
	}

	@Override
	public List<Opportunity> selectOpporMain() {
		// TODO Auto-generated method stub
		return od.selectOpporMain(sqlSession);
	}

	@Override
	public HashMap<String, Object> selectStatus() {
		return od.selectStatus(sqlSession);
	}

	@Override
	public int selectOppCount(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.selectOppCount(sqlSession,opp);
	}

	@Override
	public List<HashMap<String,Object>> selectStatusData() {
		return od.selectStatusData(sqlSession);
	}
	public Opportunity selectUpdateOppOne(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.selectUpdateOppOne(sqlSession,opp);
	}

	@Override
	public int updateOpp(Opportunity opp) {
		// TODO Auto-generated method stub
		return od.updateOpp(sqlSession,opp);
	}

	
	

	

}
