package com.kh.sm.salesOpportunity.model.vo;

import java.sql.Date;

import com.kh.sm.client.model.vo.Client;
import com.kh.sm.company.model.vo.Company;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.process.model.vo.OppProcess;
import com.kh.sm.relationClient.model.vo.RelationClient;

public class Opportunity implements java.io.Serializable {
	private int oppId; 				//영업기회ID
	private String oppName;			//영업기회명
	private int oppProgress;		//진행상태
	private int expectRev;			//예상매출
	private int expectProfitRate;	//예상이익률
	private int expectProfitPrice;	//예상이익금액
	private String category;		//매출구분
	private int processId;			//프로세스
	private int recognitionPath;	//인지경로
	private Date oppStart;			//영업시작일
	private Date oppEnd;			//영업종료일
	private String remark;			//비고
	private String agentName;		//대표자명
	private int clientId;			//고객ID
	private int empNo; 				//담당자ID
	private int count;
	private Company company;
	private Client client;
	private OppProcess process;
	private Employee employee;
	private RelationClient relationClient;
	
	public Opportunity() {}

	public Opportunity(int oppId, String oppName, int oppProgress, int expectRev, int expectProfitRate,
			int expectProfitPrice, String category, int processId, int recognitionPath, Date oppStart, Date oppEnd,
			String remark, String agentName, int clientId, int empNo, int count, Company company, Client client,
			OppProcess process, Employee employee, RelationClient relationClient) {
		super();
		this.oppId = oppId;
		this.oppName = oppName;
		this.oppProgress = oppProgress;
		this.expectRev = expectRev;
		this.expectProfitRate = expectProfitRate;
		this.expectProfitPrice = expectProfitPrice;
		this.category = category;
		this.processId = processId;
		this.recognitionPath = recognitionPath;
		this.oppStart = oppStart;
		this.oppEnd = oppEnd;
		this.remark = remark;
		this.agentName = agentName;
		this.clientId = clientId;
		this.empNo = empNo;
		this.count = count;
		this.company = company;
		this.client = client;
		this.process = process;
		this.employee = employee;
		this.relationClient = relationClient;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public String getOppName() {
		return oppName;
	}

	public void setOppName(String oppName) {
		this.oppName = oppName;
	}

	public int getOppProgress() {
		return oppProgress;
	}

	public void setOppProgress(int oppProgress) {
		this.oppProgress = oppProgress;
	}

	public int getExpectRev() {
		return expectRev;
	}

	public void setExpectRev(int expectRev) {
		this.expectRev = expectRev;
	}

	public int getExpectProfitRate() {
		return expectProfitRate;
	}

	public void setExpectProfitRate(int expectProfitRate) {
		this.expectProfitRate = expectProfitRate;
	}

	public int getExpectProfitPrice() {
		return expectProfitPrice;
	}

	public void setExpectProfitPrice(int expectProfitPrice) {
		this.expectProfitPrice = expectProfitPrice;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getProcessId() {
		return processId;
	}

	public void setProcessId(int processId) {
		this.processId = processId;
	}

	public int getRecognitionPath() {
		return recognitionPath;
	}

	public void setRecognitionPath(int recognitionPath) {
		this.recognitionPath = recognitionPath;
	}

	public Date getOppStart() {
		return oppStart;
	}

	public void setOppStart(Date oppStart) {
		this.oppStart = oppStart;
	}

	public Date getOppEnd() {
		return oppEnd;
	}

	public void setOppEnd(Date oppEnd) {
		this.oppEnd = oppEnd;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public OppProcess getProcess() {
		return process;
	}

	public void setProcess(OppProcess process) {
		this.process = process;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public RelationClient getRelationClient() {
		return relationClient;
	}

	public void setRelationClient(RelationClient relationClient) {
		this.relationClient = relationClient;
	}

	@Override
	public String toString() {
		return "Opportunity [oppId=" + oppId + ", oppName=" + oppName + ", oppProgress=" + oppProgress + ", expectRev="
				+ expectRev + ", expectProfitRate=" + expectProfitRate + ", expectProfitPrice=" + expectProfitPrice
				+ ", category=" + category + ", processId=" + processId + ", recognitionPath=" + recognitionPath
				+ ", oppStart=" + oppStart + ", oppEnd=" + oppEnd + ", remark=" + remark + ", agentName=" + agentName
				+ ", clientId=" + clientId + ", empNo=" + empNo + ", count=" + count + ", company=" + company
				+ ", client=" + client + ", process=" + process + ", employee=" + employee + ", relationClient="
				+ relationClient + "]";
	}

	

	
	
	
}
