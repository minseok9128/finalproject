package com.kh.sm.salesReport.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.kh.sm.common.Attachment;
import com.kh.sm.common.CommonUtils;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesReport.model.service.SalesReportService;
import com.kh.sm.salesReport.model.vo.Plan;
import com.kh.sm.salesReport.model.vo.Report;
import com.kh.sm.salesReport.model.vo.ReportAjax;
import com.kh.sm.salesReport.model.vo.Result;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class SalesReportController {
	@Autowired
	private final SalesReportService srs;

	public SalesReportController(SalesReportService srs) {
		this.srs = srs;
	}

	@RequestMapping(value="showSalesReport.sr", method=RequestMethod.GET) 
	public ModelAndView showSalesReport(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();

		List<Report> ReportList = srs.selectRerpotList(empNo);
		int count = srs.selectReportCount(empNo);

		ModelAndView mv = new ModelAndView("sales/salesManagement/salesReport");
		mv.addObject("ReportList", ReportList);
		mv.addObject("count", count);

		return mv;
	}

	@RequestMapping(value="showSalesReportDetail.sr", method=RequestMethod.GET) 
	public String showSalesReportDetail(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		int bizId = Integer.parseInt(request.getParameter("bizId"));

		Result result = new Result();
		result.setEmpNo(empNo);
		result.setBizId(bizId);

		Report reportDetail = srs.selectReportDetail(bizId);
		List<Result> ResultList = srs.selectResultList(result);
		List<Plan> planList = srs.selectPlanList(result);
		List<Reply> replyList = srs.selectReplyList(bizId);
		int replyCount = srs.selectReplyCount(bizId);
		List<Attachment> reportAttachList = srs.selectAttachList(bizId);

		model.addAttribute("reportDetail", reportDetail);
		model.addAttribute("ResultList", ResultList);
		model.addAttribute("planList", planList);
		model.addAttribute("replyList", replyList);
		model.addAttribute("replyCount", replyCount);

		if(reportAttachList != null) {
			model.addAttribute("reportAttachList", reportAttachList);
		}

		return "sales/salesManagement/salesReportDetail";
	}


	@RequestMapping(value="selectSalesReport.sr", method=RequestMethod.GET) 
	public String selectSalesReport() {

		return "sales/salesManagement/selectSalesReport";
	}

	@RequestMapping(value="showInsertSalesReport.sr") 
	public ModelAndView showInsertSalesReport(Model model, @RequestParam("bizDate") Date bizDate, @RequestParam("bizStart") Date bizStart,
			@RequestParam("bizEnd") Date bizEnd, @RequestParam("category") String category, HttpServletRequest request) {

		Report report = new Report();

		report.setBizDate(bizDate);
		report.setBizStart(bizStart);
		report.setBizEnd(bizEnd);
		report.setCategory(category);

		ModelAndView mv = new ModelAndView("sales/salesManagement/insertSalesReport");
		mv.addObject("report", report);

		return mv;
	}

	@PostMapping(value="searchActivityPerformance.sr")
	public ModelAndView selectSearchActivityPerformance(ModelAndView mv, @RequestParam("start") String start, 
			@RequestParam("end") String end, HttpServletRequest request) {

		ReportAjax reportA = new ReportAjax();
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();

		reportA.setStart(start);
		reportA.setEnd(end);
		reportA.setEmpNo(empNo);

		List<Activity> searchList = srs.selectSearchActivityPerformance(reportA);

		mv.addObject("searchList", searchList);
		mv.setViewName("jsonView");

		return mv;
	}

	@PostMapping(value="searchActivityPlan.sr")
	public ModelAndView selectSearchActivitPlan(ModelAndView mv, @RequestParam("start") String start, 
			@RequestParam("end") String end, HttpServletRequest request) {

		ReportAjax reportA = new ReportAjax();
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();

		reportA.setStart(start);
		reportA.setEnd(end);
		reportA.setEmpNo(empNo);

		List<Activity> searchList = srs.selectSearchActivityPlan(reportA);

		mv.addObject("searchList", searchList);
		mv.setViewName("jsonView");

		return mv;
	}

	@PostMapping("inselectPerfor.sr")
	public ModelAndView selectPerfor(Model model, @RequestParam("activityIdArr") String activityIdArr, HttpServletRequest request) {

		List<Integer> activityIdList = new ArrayList<Integer>();
		String[] arr = activityIdArr.split(", ");

		for(int i= 0; i < arr.length; i++ ) {
			activityIdList.add(Integer.parseInt(arr[i]));
		}
		Activity activity = new Activity();

		ModelAndView mv = new ModelAndView();

		List<Activity> selectPerfor = new ArrayList<Activity>();

		selectPerfor = srs.inselectPerfor(activityIdList);

		mv.addObject("selectPerfor", selectPerfor);
		mv.setViewName("jsonView");

		return mv;
	}

	@PostMapping("inselectPlan.sr")
	public ModelAndView selectPlan(Model model, @RequestParam("ActivityIdArr") String ActivityIdArr, HttpServletRequest request) {

		List<Integer> activityIdList = new ArrayList<Integer>();
		String[] arr = ActivityIdArr.split(", ");

		for(int i= 0; i < arr.length; i++ ) {
			activityIdList.add(Integer.parseInt(arr[i]));
		}

		Activity activity = new Activity();

		ModelAndView mv = new ModelAndView();

		List<Activity> selectPlan = new ArrayList<Activity>();
		selectPlan = srs.inselectPlan(activityIdList);

		mv.addObject("selectPlan", selectPlan);
		mv.setViewName("jsonView");

		return mv;
	}

	@PostMapping("insertReport.sr")
	public ModelAndView insertReport(Model model, HttpServletRequest request, MultipartHttpServletRequest multiRequest) {
		String bizDate = request.getParameter("bizDate");
		String bizStart = request.getParameter("bizStart");
		String bizEnd = request.getParameter("bizEnd");
		String category = request.getParameter("category");
		String perContent = request.getParameter("perContent");
		String planContent = request.getParameter("planContent");
		String content = request.getParameter("content");
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();

		ReportAjax reportA = new ReportAjax();
		reportA.setDate(bizDate);
		reportA.setStart(bizStart);
		reportA.setEnd(bizEnd);
		reportA.setCategory(category);
		reportA.setPerContent(perContent);
		reportA.setPlanContent(planContent);
		reportA.setContent(content);
		reportA.setEmpNo(empNo);

		ModelAndView mv = new ModelAndView("redirect:showSalesReport.sr");

		List<Integer> perforList = new ArrayList<Integer>();
		List<Integer> planList = new ArrayList<Integer>();

		String[] perforArray = request.getParameterValues("perforActivityId");

		if(perforArray != null) {
			for(int i = 0; i < perforArray.length; i++) {
				perforList.add(Integer.parseInt(perforArray[i]));
			}
		}

		String[] planArray = request.getParameterValues("planActivityId");

		if(planArray != null) {
			for(int j = 0; j < planArray.length; j++) {
				planList.add(Integer.parseInt(planArray[j]));
			}
		}

		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";

		List<MultipartFile> fileList = multiRequest.getFiles("files");

		ArrayList<String> changeNames = new ArrayList<>();

		for(int i = 0; i < fileList.size(); i ++) {
			String originFileName = fileList.get(i).getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonUtils.getRandomString();

			try {
				fileList.get(i).transferTo(new File(filePath + "\\" + changeName + ext));
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}

			changeNames.add(changeName);
		}

		int result1 = 0;
		int result = 0;


		result = srs.insertReport(reportA);

		if(result > 0) {
			result1 = srs.selectBizId();

			if(result1 > 0) {
				if(fileList != null) {
					result1 = srs.ReportInsertAttach(fileList, changeNames, filePath, result1);
				}
			}

			if(perforList.size() > 0) {
				int perInsertResult = srs.insertPerforList(perforList);
			}

			if(planList.size() > 0) {
				int planInsertResult = srs.insertPlanList(planList);
			}
		}
		return mv;
	}

	@PostMapping("replyInsert.sr")
	public String replyInsert(Model model, HttpServletRequest request) {
		Reply reply = new Reply();

		String content = request.getParameter("content");
		int categoryId = Integer.parseInt(request.getParameter("categoryId"));
		Employee emp = (Employee) request.getSession().getAttribute("loginEmp");
		int writer = emp.getEmpNo();

		reply.setContent(request.getParameter("content"));
		reply.setWriter(writer);
		reply.setCategoryId(categoryId);

		int result = srs.insertReply(reply);

		if(result > 0) {
			return "redirect:showSalesReportDetail.sr?bizId="+categoryId;
		}else {
			return "common/errorPage";
		}
	}

	@GetMapping("delReply.sr")
	public String replyDeleteReply(Model model, @RequestParam("replyId") int replyId, 
			@RequestParam("reportId") int reportId) {
		Reply reply = new Reply();

		reply.setReplyId(replyId);
		reply.setCategoryId(reportId);

		int result = srs.delReply(reply);
		if(result > 0) {
			return "redirect:showSalesReportDetail.sr?bizId="+reportId;
		}else {
			return "common/errorPage";
		}
	}

	@PostMapping("searchReportList.sr")
	public ModelAndView searchReportList(ModelAndView mv, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();

		ReportAjax reportAjax = new ReportAjax();
		reportAjax.setStart(startDate);
		reportAjax.setEnd(endDate);
		reportAjax.setEmpNo(empNo);

		List<ReportAjax> reportList = srs.searchReportList(reportAjax);
		int reportListCount = srs.searchReportCount(reportAjax);

		mv.addObject("ReportList", reportList);
		mv.addObject("reportListCount", reportListCount);
		mv.setViewName("jsonView");

		return mv;
	}

	@RequestMapping(value="showSalesReportStatus.sr", method=RequestMethod.GET) 
	public ModelAndView showSalesReportStatus(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empType = emp.getEmpType();
		int empNo = emp.getEmpNo();

		Employee empl = new Employee();
		empl.setEmpNo(empNo);
		empl.setEmpType(empType);

		List<Report> ReportStatusList = srs.selectRerpotStatusList(empl);
		int count = srs.selectReportStatusCount(empl);

		ModelAndView mv = new ModelAndView("sales/salesManagement/salesReportStatus");
		mv.addObject("ReportStatusList", ReportStatusList);
		mv.addObject("count", count);

		return mv;
	}

	@RequestMapping(value="showSalesReportStatusDetail.sr", method=RequestMethod.GET) 
	public String showSalesReportStatusDetail(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();
		int bizId = Integer.parseInt(request.getParameter("bizId"));

		Result result = new Result();

		Report reportDetail = srs.selectReportDetail(bizId);
		List<Result> ResultList = srs.selectStatusResultList(bizId);
		List<Plan> planList = srs.selectStatusPlanList(bizId);
		List<Reply> replyList = srs.selectReplyList(bizId);
		int replyCount = srs.selectReplyCount(bizId);
		List<Attachment> reportAttachList = srs.selectAttachList(bizId);

		model.addAttribute("reportDetail", reportDetail);
		model.addAttribute("ResultList", ResultList);
		model.addAttribute("planList", planList);
		model.addAttribute("replyList", replyList);
		model.addAttribute("replyCount", replyCount);

		if(reportAttachList != null) {
			model.addAttribute("reportAttachList", reportAttachList);
		}

		return "sales/salesManagement/salesReportStatusDetail";
	}

	@PostMapping("sendMailSales.sr")
	public ModelAndView searchSales(ModelAndView mv, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		int empNo = emp.getEmpNo();

		List<Employee> emailSalesList = srs.emailSalesList(empNo);

		mv.addObject("emailSalesList", emailSalesList);
		mv.setViewName("jsonView");

		return mv;
	}


	@PostMapping("sendEmail.sr")
	public ModelAndView sendEmail(ModelAndView mv, HttpServletRequest request, ModelMap mo, @RequestBody String allArr) throws AddressException, MessagingException {
		HttpSession session1 = request.getSession();
		Employee emp =(Employee) request.getSession().getAttribute("loginEmp");
		String userEmail = emp.getEmail();
		String name = emp.getEmpName();
		String userPwd = emp.getEmpPwd();

		//보내는 사람 이메일
		String user = userEmail;
		//이메일 패스워드
		String password = "hosung123";

		JSONArray allArray = JSONArray.fromObject(allArr);

		JSONArray array = (JSONArray) allArray.get(2);

		JSONArray perArray = (JSONArray) allArray.get(0);

		JSONArray planArray = (JSONArray) allArray.get(1);

		for(int i=0; i<array.size(); i++){
			JSONObject jsonObject = array.getJSONObject(i);
			String email = jsonObject.getString("email");

			Properties prop = new Properties();
			prop.put("mail.smtp.host", "smtp.naver.com");
			//네이버 포트 587
			prop.put("mail.smtp.port", 587);
			//구글 포트 465
			//prop.put("mail.smtp.port", 465);
			prop.put("mail.smtp.auth", "true");
			prop.put("mxil.smtp.ssl.enable", "true");
			prop.put("mail.smtp.ssl.trust", "smtp.naver.com");

			Session session = Session.getDefaultInstance(prop, new javax.mail.Authenticator() { 
				String un = user;
				String pw = password;
				protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
					return new javax.mail.PasswordAuthentication(un, pw);
				} 
			});

			Message mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(user));
			mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("shrghktmd@naver.com"));

			mimeMessage.setSubject(name + "영업 보고서 제출");

			String form = "";

			if(perArray.size() > 0) {
				form += "<p class=\"subTitle\" style=\"text-align:left;\r\n" + 
						"      margin:0;\r\n" + 
						"      margin-top: 50px;\r\n" + 
						"       font-size: 23px;\r\n" + 
						"       font-weight: bold;\r\n" + 
						"       color: #3D4959;\r\n" + 
						"       margin-bottom: 20px;\">영업활동 실적</p>";

				for(int j = 0; j < perArray.size(); j ++) {
					JSONObject perfor = perArray.getJSONObject(j);
					String companyName = perfor.getString("companyName");
					String clientName = perfor.getString("clientName");
					String data = perfor.getString("data");
					String start = perfor.getString("start");
					String end = perfor.getString("end");
					String oppName = perfor.getString("oppName");
					int categoryNum = Integer.parseInt(perfor.getString("category"));
					String category = "";
					int purposeNum = Integer.parseInt(perfor.getString("purpose"));
					String purpose = "";
					String planContent = perfor.getString("planContent");
					String activityContent = perfor.getString("activityContent");

					if(categoryNum == 1) {
						category = "전화";
					}else if(categoryNum == 2) {
						category = "메일";
					}else if(categoryNum == 3) {
						category = "방문";
					}else if(categoryNum == 4) {
						category = "기타";
					}

					if(purposeNum == 1) {
						purpose = "인사";
					}else if(purposeNum == 2) {
						purpose = "제품소개";
					}else if(purposeNum == 3) {
						purpose = "데모시연";
					}else if(purposeNum == 4) {
						purpose = "정보수집";
					}else if(purposeNum == 5) {
						purpose = "제안";
					}else if(purposeNum == 6) {
						purpose = "업무협의";
					}else if(purposeNum == 7) {
						purpose = "협상";
					}else if(purposeNum == 8) {
						purpose = "계약";
					}else if(purposeNum == 9) {
						purpose = "기타";
					}

					form += "<h5 style=\"margin-top:10px; margin-bottom:10px; margin-left:10px; text-align:left;\">\r\n" + 
							"                           "+companyName+" / " + clientName +"\r\n" + "</h5>\r\n" + 
							"                        <div>\r\n" + 
							"                           <table style=\"margin-bottom:20px; border:1px solid #E1E6EB;\">\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">시간</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <input style=\"border:none; pointer-events: none; color:#5D5D5D; width:80px; text-align:center;\" value=\""+data+"\" readonly><input style=\"border:none; pointer-events: none; color:#5D5D5D; width:70px; text-align:center;\" value=\""+start+"\" readonly> ~ \r\n" + 
							"                                    <input style=\"border:none; pointer-events: none; color:#5D5D5D; width:70px; text-align:center;\" value=\""+ end +"\" readonly>\r\n" + 
							"</td>\r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">영업기회</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <input style=\"border:none; width:70%; pointer-events: none; color:#5D5D5D;\" value=\""+ oppName +"\">\r\n" + 
							"</td> \r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">활동 분류/목적</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    "+ category +"\r\n" + 
							" \r\n" + 
							"                                    "+ purpose +"\r\n" + 
							"</td>\r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">계획내용</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <p><input style=\"border:none; width:70%; pointer-events: none; color:#5D5D5D;\" value=\""+ planContent+"\" readonly></p>\r\n" + 
							"                                 </td>\r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">활동내용</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <p><input style=\"border:none; width:70%; pointer-events: none; color:#5D5D5D;\" value=\""+activityContent+"\" readonly></p>\r\n" + 
							"                                 </td>\r\n" + 
							"                              </tr>\r\n" + 
							"</table>\r\n" + 
							"</div>";
				}
			}
			
			if(planArray.size() > 0) {
				form += "<p class=\"subTitle\" style=\"text-align:left;\r\n" + 
						"      margin:0;\r\n" + 
						"      margin-top: 50px;\r\n" + 
						"       font-size: 23px;\r\n" + 
						"       font-weight: bold;\r\n" + 
						"       color: #3D4959;\r\n" + 
						"       margin-bottom: 20px;\">영업활동 계획</p>";

				for(int k = 0; k < planArray.size(); k ++) {
					JSONObject plan = planArray.getJSONObject(k);
					String companyName = plan.getString("companyName");
					String clientName = plan.getString("clientName");
					String data = plan.getString("data");
					String start = plan.getString("start");
					String end = plan.getString("end");
					String oppName = plan.getString("oppName");
					int categoryNum = Integer.parseInt(plan.getString("category"));
					String category = "";
					int purposeNum = Integer.parseInt(plan.getString("purpose"));
					String purpose = "";
					String planContent = plan.getString("planContent");
					String activityContent = plan.getString("activityContent");

					if(categoryNum == 1) {
						category = "전화";
					}else if(categoryNum == 2) {
						category = "메일";
					}else if(categoryNum == 3) {
						category = "방문";
					}else if(categoryNum == 4) {
						category = "기타";
					}

					if(purposeNum == 1) {
						purpose = "인사";
					}else if(purposeNum == 2) {
						purpose = "제품소개";
					}else if(purposeNum == 3) {
						purpose = "데모시연";
					}else if(purposeNum == 4) {
						purpose = "정보수집";
					}else if(purposeNum == 5) {
						purpose = "제안";
					}else if(purposeNum == 6) {
						purpose = "업무협의";
					}else if(purposeNum == 7) {
						purpose = "협상";
					}else if(purposeNum == 8) {
						purpose = "계약";
					}else if(purposeNum == 9) {
						purpose = "기타";
					}

					form += "<h5 style=\"margin-top:10px; margin-bottom:10px; margin-left:10px; text-align:left;\">\r\n" + 
							"                           "+companyName+" / " + clientName +"\r\n" + "</h5>\r\n" + 
							"                        <div>\r\n" + 
							"                           <table style=\"margin-bottom:20px; border:1px solid #E1E6EB;\">\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">시간</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <input style=\"border:none; pointer-events: none; color:#5D5D5D; width:80px; text-align:center;\" value=\""+data+"\" readonly><input style=\"border:none; pointer-events: none; color:#5D5D5D; width:70px; text-align:center;\" value=\""+start+"\" readonly> ~ \r\n" + 
							"                                    <input style=\"border:none; pointer-events: none; color:#5D5D5D; width:70px; text-align:center;\" value=\""+ end +"\" readonly>\r\n" + 
							"</td>\r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">영업기회</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <input style=\"border:none; width:70%; pointer-events: none; color:#5D5D5D;\" value=\""+ oppName +"\">\r\n" + 
							"</td> \r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">활동 분류/목적</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    "+ category +"\r\n" + 
							" \r\n" + 
							"                                    "+ purpose +"\r\n" + 
							"</td>\r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">계획내용</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <p><input style=\"border:none; width:70%; pointer-events: none; color:#5D5D5D;\" value=\""+ planContent+"\" readonly></p>\r\n" + 
							"                                 </td>\r\n" + 
							"                              </tr>\r\n" + 
							"<tr>\r\n" + 
							"<th class=\"th\" style=\"height:60px;vertical-align:middle;background-color:#FBFCFD;text-align:center;border-right:1px solid #E1E6EB;border-bottom:1px solid #E1E6EB;font-weight:bold;\">활동내용</th>\r\n" + 
							"                                 <td class=\"insertTableTd\" style=\"text-align:left;width:700px;margin-right:auto;margin-left:auto;vertical-align:middle;border-bottom:1px solid #E1E6EB;padding-left:15px;\">\r\n" + 
							"                                    <p><input style=\"border:none; width:70%; pointer-events: none; color:#5D5D5D;\" value=\""+activityContent+"\" readonly></p>\r\n" + 
							"                                 </td>\r\n" + 
							"                              </tr>\r\n" + 
							"</table>\r\n" + 
							"</div>";
				}
			}
			
			mimeMessage.setContent(form, "text/html; charset=utf-8");
			Transport.send(mimeMessage);
		}

		mv.setViewName("jsonView");
		return mv;
	}
}