package com.kh.sm.salesReport.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesReport.model.vo.Plan;
import com.kh.sm.salesReport.model.vo.Report;
import com.kh.sm.salesReport.model.vo.ReportAjax;
import com.kh.sm.salesReport.model.vo.Result;

public interface SalesReportDao {

	List<Activity> selectSearchActivityPerformance(SqlSessionTemplate sqlSession, ReportAjax reportA);

	List<Activity> selectSearchActivityPlan(SqlSessionTemplate sqlSession, ReportAjax reportA);

	List<Activity> inselectPerfor(SqlSessionTemplate sqlSession, List<Integer> activityIdList);

	List<Activity> inselectPlan(SqlSessionTemplate sqlSession, List<Integer> activityIdList);

	int insertReport(SqlSessionTemplate sqlSession, ReportAjax reportA);

	int ReportInsertAttach(SqlSessionTemplate sqlSession, Attachment attach);

	List<Report> selectRerpotList(SqlSessionTemplate sqlSession, int empNo);

	int selectReportCount(SqlSessionTemplate sqlSession, int empNo);

	Report selectReportDetail(SqlSessionTemplate sqlSession, int bizId);

	int insertPerforList(SqlSessionTemplate sqlSession, int perforActivityId);

	int insertPlanList(SqlSessionTemplate sqlSession, int planActivityId);

	List<Result> selectResultList(SqlSessionTemplate sqlSession, Result result);

	List<Plan> selectPlanList(SqlSessionTemplate sqlSession, Result result);

	int insertReply(SqlSessionTemplate sqlSession, Reply reply);

	List<Reply> selectReplyList(SqlSessionTemplate sqlSession, int bizId);

	int selectReplyCount(SqlSessionTemplate sqlSession, int bizId);

	int delReply(SqlSessionTemplate sqlSession, Reply reply);

	List<Attachment> selectAttachList(SqlSessionTemplate sqlSession, int bizId);

	List<ReportAjax> searchReportList(SqlSessionTemplate sqlSession, ReportAjax reportAjax);

	int searchReportCount(SqlSessionTemplate sqlSession, ReportAjax reportAjax);

	int selectBizId(SqlSessionTemplate sqlSession);

	List<Report> selectReportStatusList(SqlSessionTemplate sqlSession, Employee empl);

	int selectReportStatusCount(SqlSessionTemplate sqlSession, Employee empl);

	List<Result> selectStatusResultList(SqlSessionTemplate sqlSession, int bizId);

	List<Plan> selectStatusPlanList(SqlSessionTemplate sqlSession, int bizId);

	List<Employee> emailSalesList(SqlSessionTemplate sqlSession, int empNo);
	
}