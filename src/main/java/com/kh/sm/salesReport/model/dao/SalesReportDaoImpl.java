package com.kh.sm.salesReport.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesReport.model.vo.Plan;
import com.kh.sm.salesReport.model.vo.Report;
import com.kh.sm.salesReport.model.vo.ReportAjax;
import com.kh.sm.salesReport.model.vo.Result;

@Repository
public class SalesReportDaoImpl implements SalesReportDao{

	@Override
	public List<Activity> selectSearchActivityPerformance(SqlSessionTemplate sqlSession, ReportAjax reportA) {
		
		return sqlSession.selectList("Report.selectSearchPerfomance", reportA);
	}

	@Override
	public List<Activity> selectSearchActivityPlan(SqlSessionTemplate sqlSession, ReportAjax reportA) {
		
		List<Activity> list = sqlSession.selectList("Report.selectSearchPlan", reportA);
		
		return list;
	}

	@Override
	public List<Activity> inselectPerfor(SqlSessionTemplate sqlSession, List<Integer> activityIdList) {
		
		List<Activity> selectPerfor = null;
		
		for(int i = 0; i < activityIdList.size(); i++) {
			int activityId = activityIdList.get(i);
			selectPerfor = sqlSession.selectList("Report.selectPerfor", activityId);
		}
		return selectPerfor;
	}

	@Override
	public List<Activity> inselectPlan(SqlSessionTemplate sqlSession, List<Integer> activityIdList) {

		List<Activity> selectPlan = null;
		
		for(int i = 0; i < activityIdList.size(); i++) {
			int activityId = activityIdList.get(i);
			selectPlan = sqlSession.selectList("Report.selectPlan", activityId);
		}
		
		return selectPlan;
	}

	@Override
	public int insertReport(SqlSessionTemplate sqlSession, ReportAjax reportA) {
		
		return sqlSession.insert("Report.insertReport", reportA);
	}

	@Override
	public int ReportInsertAttach(SqlSessionTemplate sqlSession, Attachment attach) {
		
		return sqlSession.insert("Report.ReportInsertAttach", attach);
	}

	@Override
	public List<Report> selectRerpotList(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectList("Report.selectReportList", empNo);
	}

	@Override
	public int selectReportCount(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectOne("Report.selectReportCount", empNo);
	}

	@Override
	public Report selectReportDetail(SqlSessionTemplate sqlSession, int bizId) {
		
		return sqlSession.selectOne("Report.selectReportDetail", bizId);
	}
	
	@Override
	public int insertPerforList(SqlSessionTemplate sqlSession, int perforActivityId) {
		
		return sqlSession.insert("Report.insertPerforList", perforActivityId);
	}

	@Override
	public int insertPlanList(SqlSessionTemplate sqlSession, int planActivityId) {
		
		return sqlSession.insert("Report.insertPlanList", planActivityId);
	}

	@Override
	public List<Result> selectResultList(SqlSessionTemplate sqlSession, Result result) {
		
		return sqlSession.selectList("Report.selectResultList", result);
	}

	@Override
	public List<Plan> selectPlanList(SqlSessionTemplate sqlSession, Result result) {
		
		return sqlSession.selectList("Report.selectPlanList", result);
	}

	@Override
	public int insertReply(SqlSessionTemplate sqlSession, Reply reply) {
		
		return sqlSession.insert("Report.insertReply", reply);
	}

	@Override
	public List<Reply> selectReplyList(SqlSessionTemplate sqlSession, int bizId) {
		
		return sqlSession.selectList("Report.selectReplyList", bizId);
	}

	@Override
	public int selectReplyCount(SqlSessionTemplate sqlSession, int bizId) {
		
		int count = sqlSession.selectOne("Report.selectReplyCount", bizId);
		
		return count;
	}

	@Override
	public int delReply(SqlSessionTemplate sqlSession, Reply reply) {
		
		return sqlSession.update("Report.delReply", reply);
	}

	@Override
	public List<Attachment> selectAttachList(SqlSessionTemplate sqlSession, int bizId) {
		
		return sqlSession.selectList("Report.selectAttachList", bizId);
	}

	@Override
	public List<ReportAjax> searchReportList(SqlSessionTemplate sqlSession, ReportAjax reportAjax) {
		
		return sqlSession.selectList("Report.searchReportList",reportAjax);
	}

	@Override
	public int searchReportCount(SqlSessionTemplate sqlSession, ReportAjax reportAjax) {
		
		return sqlSession.selectOne("Report.searchReportCount", reportAjax);
	}

	@Override
	public int selectBizId(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectOne("Report.selectBizId");
	}

	@Override
	public List<Report> selectReportStatusList(SqlSessionTemplate sqlSession, Employee empl) {
		
		return sqlSession.selectList("Report.selectReportStatusList", empl);
	}

	@Override
	public int selectReportStatusCount(SqlSessionTemplate sqlSession, Employee empl) {
		
		return sqlSession.selectOne("Report.selectReportStatusCount", empl);
	}

	@Override
	public List<Result> selectStatusResultList(SqlSessionTemplate sqlSession, int bizId) {
		
		return sqlSession.selectList("Report.selectStatusResultList", bizId);
	}

	@Override
	public List<Plan> selectStatusPlanList(SqlSessionTemplate sqlSession, int bizId) {
		
		return sqlSession.selectList("Report.selectStatusPlanList", bizId);
	}

	@Override
	public List<Employee> emailSalesList(SqlSessionTemplate sqlSession, int empNo) {
		
		return sqlSession.selectList("Report.emailSalesList", empNo);
	}

}