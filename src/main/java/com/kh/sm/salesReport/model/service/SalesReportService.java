package com.kh.sm.salesReport.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesReport.model.vo.Plan;
import com.kh.sm.salesReport.model.vo.Report;
import com.kh.sm.salesReport.model.vo.ReportAjax;
import com.kh.sm.salesReport.model.vo.Result;

public interface SalesReportService {

	List<Activity> selectSearchActivityPerformance(ReportAjax reportA);

	List<Activity> selectSearchActivityPlan(ReportAjax reportA);

	List<Activity> inselectPerfor(List<Integer> activityIdList);

	List<Activity> inselectPlan(List<Integer> activityIdList);

	int insertReport(ReportAjax reportA);

	int ReportInsertAttach(List<MultipartFile> fileList, ArrayList<String> changeNames, String filePath, int result);

	List<Report> selectRerpotList(int empNo);

	int selectReportCount(int empNo);

	Report selectReportDetail(int bizId);

	int insertPerforList(List<Integer> perforList);

	int insertPlanList(List<Integer> planList);

	List<Result> selectResultList(Result result);

	List<Plan> selectPlanList(Result result);

	int insertReply(Reply reply);

	List<Reply> selectReplyList(int bizId);

	int selectReplyCount(int bizId);

	int delReply(Reply reply);

	List<Attachment> selectAttachList(int bizId);

	List<ReportAjax> searchReportList(ReportAjax reportAjax);

	int searchReportCount(ReportAjax reportAjax);

	int selectBizId();

	List<Report> selectRerpotStatusList(Employee empl);

	int selectReportStatusCount(Employee empl);

	List<Result> selectStatusResultList(int bizId);

	List<Plan> selectStatusPlanList(int bizId);

	List<Employee> emailSalesList(int empNo);

}