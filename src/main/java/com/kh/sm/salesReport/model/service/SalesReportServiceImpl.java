package com.kh.sm.salesReport.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.kh.sm.common.Attachment;
import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.notice.model.vo.Reply;
import com.kh.sm.salesActivity.model.vo.Activity;
import com.kh.sm.salesReport.model.dao.SalesReportDao;
import com.kh.sm.salesReport.model.vo.Plan;
import com.kh.sm.salesReport.model.vo.Report;
import com.kh.sm.salesReport.model.vo.ReportAjax;
import com.kh.sm.salesReport.model.vo.Result;

@Service
public class SalesReportServiceImpl implements SalesReportService{

	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired 
	private SalesReportDao srd;
	
	@Override
	public List<Activity> selectSearchActivityPerformance(ReportAjax reportA) {
		
		List<Activity> searchList = srd.selectSearchActivityPerformance(sqlSession, reportA);
		
		return searchList;
	}

	@Override
	public List<Activity> selectSearchActivityPlan(ReportAjax reportA) {
		
		List<Activity> searchList = srd.selectSearchActivityPlan(sqlSession, reportA);
		
		return searchList;
	}

	@Override
	public List<Activity> inselectPerfor(List<Integer> activityIdList) {
		
		List<Activity> selectPerfor = srd.inselectPerfor(sqlSession, activityIdList);
		
		return selectPerfor;
	}

	@Override
	public List<Activity> inselectPlan(List<Integer> activityIdList) {

		List<Activity> selectPlan = srd.inselectPlan(sqlSession, activityIdList);
		
		return selectPlan;
	}

	@Override
	public int insertReport(ReportAjax reportA) {
		
		return srd.insertReport(sqlSession, reportA);
	}

	@Override
	public int ReportInsertAttach(List<MultipartFile> fileList, ArrayList<String> changeNames, String filePath, int result) {
		int insertResult = 0;
		Attachment attach = null;
		
		for(int i = 0; i < fileList.size(); i++) {
			attach = new Attachment();
			attach.setCategory(8);
			attach.setChangeName(changeNames.get(i));
			attach.setFilePath(filePath);
			attach.setOriginName(fileList.get(i).getOriginalFilename());
			attach.setCategoryId(result);
			attach.setDelYn("N");
			insertResult += srd.ReportInsertAttach(sqlSession, attach);
		}
		
		return insertResult;
	}

	@Override
	public List<Report> selectRerpotList(int empNo) {
		
		List<Report> selectRerpotList = srd.selectRerpotList(sqlSession, empNo);
		
		return selectRerpotList;
	}

	@Override
	public int selectReportCount(int empNo) {
		
		return srd.selectReportCount(sqlSession, empNo);
	}

	@Override
	public Report selectReportDetail(int bizId) {
		
		return srd.selectReportDetail(sqlSession, bizId);
	}

	@Override
	public int insertPerforList(List<Integer> perforList) {
		
		int result = 0;
		
		for(int i = 0; i < perforList.size(); i++) {
			int perforActivityId = perforList.get(i);
			result = srd.insertPerforList(sqlSession, perforActivityId);
			
			if(result <= 0) {
				result = 0;
				break;
			}
		}
		
		return result;
	}

	@Override
	public int insertPlanList(List<Integer> planList) {

		int result = 0;
		
		for(int i = 0; i < planList.size(); i++) {
			int planActivityId = planList.get(i);
			result = srd.insertPlanList(sqlSession, planActivityId);
			
			if(result <= 0) {
				result = 0;
				break;
			}
		}
		
		return result;
	}

	@Override
	public List<Result> selectResultList(Result result) {
		
		List<Result> list = srd.selectResultList(sqlSession, result);
		
		return list;
	}

	@Override
	public List<Plan> selectPlanList(Result result) {
		
		List<Plan> list = srd.selectPlanList(sqlSession, result);
		
		return list;
	}

	@Override
	public int insertReply(Reply reply) {
		
		return srd.insertReply(sqlSession, reply);
	}

	@Override
	public List<Reply> selectReplyList(int bizId) {

		List<Reply> list = srd.selectReplyList(sqlSession, bizId);
		
		return list;
	}

	@Override
	public int selectReplyCount(int bizId) {
		
		return srd.selectReplyCount(sqlSession, bizId);
	}

	@Override
	public int delReply(Reply reply) {
		
		return srd.delReply(sqlSession, reply);
	}

	@Override
	public List<Attachment> selectAttachList(int bizId) {
		
		return srd.selectAttachList(sqlSession, bizId);
	}

	@Override
	public List<ReportAjax> searchReportList(ReportAjax reportAjax) {
		
		return srd.searchReportList(sqlSession, reportAjax);
	}

	@Override
	public int searchReportCount(ReportAjax reportAjax) {
		
		return srd.searchReportCount(sqlSession, reportAjax);
	}

	@Override
	public int selectBizId() {
		
		return srd.selectBizId(sqlSession);
	}

	@Override
	public List<Report> selectRerpotStatusList(Employee empl) {
		
		return srd.selectReportStatusList(sqlSession, empl);
	}

	@Override
	public int selectReportStatusCount(Employee empl) {
		
		return srd.selectReportStatusCount(sqlSession, empl);
	}

	@Override
	public List<Result> selectStatusResultList(int bizId) {
		
		return srd.selectStatusResultList(sqlSession, bizId);
	}

	@Override
	public List<Plan> selectStatusPlanList(int bizId) {
		
		return srd.selectStatusPlanList(sqlSession, bizId);
	}

	@Override
	public List<Employee> emailSalesList(int empNo) {
		
		return srd.emailSalesList(sqlSession, empNo);
	}
  
}