package com.kh.sm.salesReport.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Report implements Serializable{
	private int bizId;
	private Date bizDate;
	private String category;
	private String content;
	private int empNo;
	private String empName;
	private Date bizSubmit;
	private Date bizStart;
	private Date bizEnd;
	private String perContent;
	private String planContent;
	 
	public Report() {}

	public Report(int bizId, Date bizDate, String category, String content, int empNo, String empName, Date bizSubmit,
			Date bizStart, Date bizEnd, String perContent, String planContent) {
		super();
		this.bizId = bizId;
		this.bizDate = bizDate;
		this.category = category;
		this.content = content;
		this.empNo = empNo;
		this.empName = empName;
		this.bizSubmit = bizSubmit;
		this.bizStart = bizStart;
		this.bizEnd = bizEnd;
		this.perContent = perContent;
		this.planContent = planContent;
	}

	public int getBizId() {
		return bizId;
	}

	public void setBizId(int bizId) {
		this.bizId = bizId;
	}

	public Date getBizDate() {
		return bizDate;
	}

	public void setBizDate(Date bizDate) {
		this.bizDate = bizDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Date getBizSubmit() {
		return bizSubmit;
	}

	public void setBizSubmit(Date bizSubmit) {
		this.bizSubmit = bizSubmit;
	}

	public Date getBizStart() {
		return bizStart;
	}

	public void setBizStart(Date bizStart) {
		this.bizStart = bizStart;
	}

	public Date getBizEnd() {
		return bizEnd;
	}

	public void setBizEnd(Date bizEnd) {
		this.bizEnd = bizEnd;
	}

	public String getPerContent() {
		return perContent;
	}

	public void setPerContent(String perContent) {
		this.perContent = perContent;
	}

	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}

	@Override
	public String toString() {
		return "Report [bizId=" + bizId + ", bizDate=" + bizDate + ", category=" + category + ", content=" + content
				+ ", empNo=" + empNo + ", empName=" + empName + ", bizSubmit=" + bizSubmit + ", bizStart=" + bizStart
				+ ", bizEnd=" + bizEnd + ", perContent=" + perContent + ", planContent=" + planContent + "]";
	}

}