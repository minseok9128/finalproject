package com.kh.sm.salesReport.model.vo;

import java.io.Serializable;

public class ReportAjax implements Serializable {
	private int bizId;
	private String date;
	private String category;
	private String content;
	private int empNo;
	private String empName;
	private String sumit;
	private String start;
	private String end;
	private String perContent;
	private String planContent;
	
	public ReportAjax() {}

	public int getBizId() {
		return bizId;
	}

	public void setBizId(int bizId) {
		this.bizId = bizId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getSumit() {
		return sumit;
	}

	public void setSumit(String sumit) {
		this.sumit = sumit;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getPerContent() {
		return perContent;
	}

	public void setPerContent(String perContent) {
		this.perContent = perContent;
	}

	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}

	@Override
	public String toString() {
		return "ReportAjax [bizId=" + bizId + ", date=" + date + ", category=" + category + ", content=" + content
				+ ", empNo=" + empNo + ", empName=" + empName + ", sumit=" + sumit + ", start=" + start + ", end=" + end
				+ ", perContent=" + perContent + ", planContent=" + planContent + "]";
	}
	
}