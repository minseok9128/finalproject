package com.kh.sm.salesReport.model.vo;

public class Result {
	private int ResultPlanId;
	private int activityId;
	private int bizId;
	private int category;
	private int purpose;
	private int empNo;
	private String date;
	private String start;
	private String end;
	private String planContent;
	private String activityContent;
	private String completeYN;
	private String clientName;
	private String companyName;
	private String oppName;
	private int oppProcess;
	
	public Result() {}

	public Result(int resultPlanId, int activityId, int bizId, int category, int purpose, int empNo, String date,
			String start, String end, String planContent, String activityContent, String completeYN, String clientName,
			String companyName, String oppName, int oppProcess) {
		super();
		ResultPlanId = resultPlanId;
		this.activityId = activityId;
		this.bizId = bizId;
		this.category = category;
		this.purpose = purpose;
		this.empNo = empNo;
		this.date = date;
		this.start = start;
		this.end = end;
		this.planContent = planContent;
		this.activityContent = activityContent;
		this.completeYN = completeYN;
		this.clientName = clientName;
		this.companyName = companyName;
		this.oppName = oppName;
		this.oppProcess = oppProcess;
	}

	public int getResultPlanId() {
		return ResultPlanId;
	}

	public void setResultPlanId(int resultPlanId) {
		ResultPlanId = resultPlanId;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public int getBizId() {
		return bizId;
	}

	public void setBizId(int bizId) {
		this.bizId = bizId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPurpose() {
		return purpose;
	}

	public void setPurpose(int purpose) {
		this.purpose = purpose;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}

	public String getActivityContent() {
		return activityContent;
	}

	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}

	public String getCompleteYN() {
		return completeYN;
	}

	public void setCompleteYN(String completeYN) {
		this.completeYN = completeYN;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getOppName() {
		return oppName;
	}

	public void setOppName(String oppName) {
		this.oppName = oppName;
	}

	public int getOppProcess() {
		return oppProcess;
	}

	public void setOppProcess(int oppProcess) {
		this.oppProcess = oppProcess;
	}

	@Override
	public String toString() {
		return "Result [ResultPlanId=" + ResultPlanId + ", activityId=" + activityId + ", bizId=" + bizId
				+ ", category=" + category + ", purpose=" + purpose + ", empNo=" + empNo + ", date=" + date + ", start="
				+ start + ", end=" + end + ", planContent=" + planContent + ", activityContent=" + activityContent
				+ ", completeYN=" + completeYN + ", clientName=" + clientName + ", companyName=" + companyName
				+ ", oppName=" + oppName + ", oppProcess=" + oppProcess + "]";
	}

}