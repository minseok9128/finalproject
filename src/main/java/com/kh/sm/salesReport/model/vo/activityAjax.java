package com.kh.sm.salesReport.model.vo;

import java.io.Serializable;

public class activityAjax implements Serializable {
	private int activityId;
	private int oppId;
	private int category;
	private int purpose;
	private String aDate;
	private String completeYN;
	private String planCon;
	private String activityCon;
	private int empNo;
	private String empName;
	private int clientId;
	private String clientName;
	private String companyName;
	private String aStart;
	private String aEnd;
	
	public activityAjax() {}

	public activityAjax(int activityId, int oppId, int category, int purpose, String aDate, String completeYN,
			String planCon, String activityCon, int empNo, String empName, int clientId, String clientName,
			String companyName, String aStart, String aEnd) {
		super();
		this.activityId = activityId;
		this.oppId = oppId;
		this.category = category;
		this.purpose = purpose;
		this.aDate = aDate;
		this.completeYN = completeYN;
		this.planCon = planCon;
		this.activityCon = activityCon;
		this.empNo = empNo;
		this.empName = empName;
		this.clientId = clientId;
		this.clientName = clientName;
		this.companyName = companyName;
		this.aStart = aStart;
		this.aEnd = aEnd;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPurpose() {
		return purpose;
	}

	public void setPurpose(int purpose) {
		this.purpose = purpose;
	}

	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public String getCompleteYN() {
		return completeYN;
	}

	public void setCompleteYN(String completeYN) {
		this.completeYN = completeYN;
	}

	public String getPlanCon() {
		return planCon;
	}

	public void setPlanCon(String planCon) {
		this.planCon = planCon;
	}

	public String getActivityCon() {
		return activityCon;
	}

	public void setActivityCon(String activityCon) {
		this.activityCon = activityCon;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getaStart() {
		return aStart;
	}

	public void setaStart(String aStart) {
		this.aStart = aStart;
	}

	public String getaEnd() {
		return aEnd;
	}

	public void setaEnd(String aEnd) {
		this.aEnd = aEnd;
	}

	@Override
	public String toString() {
		return "activityAjax [activityId=" + activityId + ", oppId=" + oppId + ", category=" + category + ", purpose="
				+ purpose + ", aDate=" + aDate + ", completeYN=" + completeYN + ", planCon=" + planCon
				+ ", activityCon=" + activityCon + ", empNo=" + empNo + ", empName=" + empName + ", clientId="
				+ clientId + ", clientName=" + clientName + ", companyName=" + companyName + ", aStart=" + aStart
				+ ", aEnd=" + aEnd + "]";
	}

}