package com.kh.sm.status.activityStatus.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kh.sm.status.activityStatus.model.service.StatusActivityStatusService;
import com.kh.sm.status.activityStatus.model.vo.StatusActivity;
import com.kh.sm.status.activityStatus.model.vo.StatusActivityData;

@Controller
public class StatusActivityStatusController {
	
	@Autowired
	private final StatusActivityStatusService service;
	
	public StatusActivityStatusController(StatusActivityStatusService service) {
		this.service = service;
	}
	
	@RequestMapping(value="showActivityStatus.sas", method=RequestMethod.GET)
	public String showActivityStatus(Model model) {
		
		StatusActivity activity = new StatusActivity();
		List<StatusActivity> activityList = service.selectActivity();
		List<StatusActivity> empList = service.selectEmpNameList();
		List<StatusActivity> monthActivity = service.selectMothActivity();
		List<StatusActivity> salesManageList = service.salesManageList();
		
		Map<String, Object> categoryCount = new HashMap<String, Object>();
		Map<String, Object> month = new HashMap<String, Object>();
		
		for(int i = 0; i <= 4; i++) {
			if(i < activityList.size()) {
				categoryCount.put(activityList.get(i).getCategory()+"", activityList.get(i).getCount());
			}
		}
		
		for(int j = 0; j < monthActivity.size(); j++) {
			month.put(monthActivity.get(j).getMonth(), monthActivity.get(j).getCount());
		}
		
		for(int k = 0; k < 12; k++) {
			String K = null;
			
			if(k < 9) {
				K = "0" + (k + 1);
				if(month.get(K) == null) {
					month.put(K, 0);
				}
			}else {
				if(month.get((k+1)+"") == null) {
					month.put((k+1)+"", 0);
				}
			}
			
		}
		String names = null;
		List<Integer> countList = new ArrayList<Integer>();
		int count = 0;
		
		for(int m = 0; m < salesManageList.size(); m++) {
			if(m == 0) {
				names = salesManageList.get(m).getEmpName();
				countList.add(salesManageList.get(m).getCount());
			}else if(m == 1){
				names += ", " + salesManageList.get(m).getEmpName();
				countList.add(salesManageList.get(m).getCount());
			}else if(m == 2){
				names += ", " + salesManageList.get(m).getEmpName();
				countList.add(salesManageList.get(m).getCount());
			}else {
				names += ", 기타";
				count += salesManageList.get(m).getCount();
			}
			if(m == salesManageList.size() - 1) {
				if(count == 0) {
					countList.add(count);
				}
			}
		}
		
		model.addAttribute("categoryCount", categoryCount);
		model.addAttribute("empList", empList);
		model.addAttribute("month", month);
		model.addAttribute("names", names);
		model.addAttribute("countList", countList);
		model.addAttribute("salesManageList", salesManageList);
		
		return "status/ActivityStatus/statusActivityStatus";
	}
	
	@RequestMapping(value="dataActivityStatus.sas", method=RequestMethod.GET)
	public String showDataActivityStatus(Model model) {
		
		List<StatusActivityData> activityDataList = service.selectActivityDataList();
		int activityDataCount = service.selectActivityCount();
		List<StatusActivity> empList = service.selectEmpNameList();
		
		model.addAttribute("activityDataList", activityDataList);
		model.addAttribute("activityDataCount", activityDataCount);
		model.addAttribute("empList", empList);
		
		return "status/ActivityStatus/ActivityStatusData";
	}
}
