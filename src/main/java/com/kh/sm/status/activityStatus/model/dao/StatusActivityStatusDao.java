package com.kh.sm.status.activityStatus.model.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.status.activityStatus.model.vo.StatusActivity;
import com.kh.sm.status.activityStatus.model.vo.StatusActivityData;

public interface StatusActivityStatusDao {

	List<StatusActivity> selectActivity(SqlSessionTemplate sqlSession);

	List<StatusActivity> selectEmpNameList(SqlSessionTemplate sqlSession);

	List<StatusActivity> selectMothActivity(SqlSessionTemplate sqlSession);

	List<StatusActivity> salesManageList(SqlSessionTemplate sqlSession);

	List<StatusActivityData> selectActivityDataList(SqlSessionTemplate sqlSession);

	int selectActivityCount(SqlSessionTemplate sqlSession);

}