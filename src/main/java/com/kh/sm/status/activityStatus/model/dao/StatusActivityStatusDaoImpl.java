package com.kh.sm.status.activityStatus.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.status.activityStatus.model.vo.StatusActivity;
import com.kh.sm.status.activityStatus.model.vo.StatusActivityData;

@Repository
public class StatusActivityStatusDaoImpl implements StatusActivityStatusDao{

	@Override
	public List<StatusActivity> selectActivity(SqlSessionTemplate sqlSession) {
		
		List<StatusActivity> activityList = sqlSession.selectList("StatusActivity.selectActivity");
		
		return activityList;
	}

	@Override
	public List<StatusActivity> selectEmpNameList(SqlSessionTemplate sqlSession) {
		
		List<StatusActivity> selectEmpNameList = sqlSession.selectList("StatusActivity.selectEmpNameList");
		
		return selectEmpNameList;
	}

	@Override
	public List<StatusActivity> selectMothActivity(SqlSessionTemplate sqlSession) {

		List<StatusActivity> selectMothActivity = sqlSession.selectList("StatusActivity.selectMothActivity");
		
		return selectMothActivity;
	}

	@Override
	public List<StatusActivity> salesManageList(SqlSessionTemplate sqlSession) {
		
		List<StatusActivity> salesManageList = sqlSession.selectList("StatusActivity.salesManageList");
		
		return salesManageList;
	}

	@Override
	public List<StatusActivityData> selectActivityDataList(SqlSessionTemplate sqlSession) {
		
		List<StatusActivityData> selectActivityDataList = sqlSession.selectList("StatusActivity.selectActivityDataList");
		
		return selectActivityDataList;
	}

	@Override
	public int selectActivityCount(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectOne("StatusActivity.selectActivityCount");
	}


}