package com.kh.sm.status.activityStatus.model.service;

import java.util.List;

import com.kh.sm.status.activityStatus.model.vo.StatusActivity;
import com.kh.sm.status.activityStatus.model.vo.StatusActivityData;

public interface StatusActivityStatusService {

	List<StatusActivity> selectActivity();

	List<StatusActivity> selectEmpNameList();

	List<StatusActivity> selectMothActivity();

	List<StatusActivity> salesManageList();

	List<StatusActivityData> selectActivityDataList();

	int selectActivityCount();

}
