package com.kh.sm.status.activityStatus.model.service;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.status.activityStatus.model.dao.StatusActivityStatusDao;
import com.kh.sm.status.activityStatus.model.vo.StatusActivity;
import com.kh.sm.status.activityStatus.model.vo.StatusActivityData;

@Service
public class StatusActivityStatusServiceImpl implements StatusActivityStatusService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private StatusActivityStatusDao sasd;
	
	@Override
	public List<StatusActivity> selectActivity() {
		
		List<StatusActivity> activityList = sasd.selectActivity(sqlSession);
		
		return activityList;
	}

	@Override
	public List<StatusActivity> selectEmpNameList() {
		
		List<StatusActivity> empNameList = sasd.selectEmpNameList(sqlSession);
		
		return empNameList;
	}

	@Override
	public List<StatusActivity> selectMothActivity() {

		List<StatusActivity> selectMothActivity = sasd.selectMothActivity(sqlSession);
		
		return selectMothActivity;
	}

	@Override
	public List<StatusActivity> salesManageList() {
		
		List<StatusActivity> salesManageList = sasd.salesManageList(sqlSession);
		
		return salesManageList;
	}

	@Override
	public List<StatusActivityData> selectActivityDataList() {
		
		List<StatusActivityData> activityDataList = sasd.selectActivityDataList(sqlSession);
		
		return activityDataList;
	}

	@Override
	public int selectActivityCount() {
		
		return sasd.selectActivityCount(sqlSession);
	}
}
