package com.kh.sm.status.activityStatus.model.vo;

public class StatusActivityData {
	private int activityId;
	private String activityDate;
	private int category;
	private int purpose;
	private int clientId;
	private String clientName;
	private int companyId;
	private String companyName;
	private int oppId;
	private String oppName;
	private String activityCon;
	private int empNo;
	private String empName;
	
	public StatusActivityData() {}

	public StatusActivityData(int activityId, String activityDate, int category, int purpose, int clientId,
			String clientName, int companyId, String companyName, int oppId, String oppName, String activityCon,
			int empNo, String empName) {
		super();
		this.activityId = activityId;
		this.activityDate = activityDate;
		this.category = category;
		this.purpose = purpose;
		this.clientId = clientId;
		this.clientName = clientName;
		this.companyId = companyId;
		this.companyName = companyName;
		this.oppId = oppId;
		this.oppName = oppName;
		this.activityCon = activityCon;
		this.empNo = empNo;
		this.empName = empName;
	}


	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPurpose() {
		return purpose;
	}

	public void setPurpose(int purpose) {
		this.purpose = purpose;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public String getOppName() {
		return oppName;
	}

	public void setOppName(String oppName) {
		this.oppName = oppName;
	}

	public String getActivityCon() {
		return activityCon;
	}

	public void setActivityCon(String activityCon) {
		this.activityCon = activityCon;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "StatusActivityData [activityId=" + activityId + ", activityDate=" + activityDate + ", category="
				+ category + ", purpose=" + purpose + ", clientId=" + clientId + ", clientName=" + clientName
				+ ", companyId=" + companyId + ", companyName=" + companyName + ", oppId=" + oppId + ", oppName="
				+ oppName + ", activityCon=" + activityCon + ", empNo=" + empNo + ", empName=" + empName + "]";
	}

}