package com.kh.sm.status.clientStatus.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kh.sm.status.clientStatus.model.service.StatusClientService;

@Controller
public class StatusClientController {
	
	@Autowired
	public StatusClientService ss;

	@RequestMapping("showClientStatus.cs")
	public String showClientStatus(HttpServletRequest request) {
		
		HashMap<String, Integer> nowClientSatusValue = ss.selectNowClientStatusValue();
		HashMap<String, Integer> lastMonthClientSatusValue = ss.selectlastMonthClientStatusValue();
		HashMap<String, Integer> threeMonthAgoClientStatusValue = ss.selectThreeMonthAgoClientStatusValue();
		List<Integer> clientRankList = ss.selectClientRankList();
		 
		System.out.println(threeMonthAgoClientStatusValue);
		
		request.setAttribute("nowClientStatusValue", nowClientSatusValue);
		request.setAttribute("lastMonthClientSatusValue", lastMonthClientSatusValue);
		request.setAttribute("threeMonthClientSatusValue", threeMonthAgoClientStatusValue);
		request.setAttribute("clientRankList", clientRankList);
		return "status/clientStatus/customerStatus";
	}
	@RequestMapping("showClientDataStatus.cs")
	public String showClientDataStatus(HttpServletRequest request) {
		
		List<Object> clientDataStatus = ss.selectClientDataStatus();
		int length = clientDataStatus.size();
		
		
		request.setAttribute("list", clientDataStatus);
		request.setAttribute("length", length);
		return "status/clientStatus/custormerDataStatus";
	}
}
