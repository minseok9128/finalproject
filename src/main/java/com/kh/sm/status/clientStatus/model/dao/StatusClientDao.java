package com.kh.sm.status.clientStatus.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

public interface StatusClientDao {

	HashMap<String, Integer> selectNowClientStatusValue(SqlSessionTemplate sqlSession);

	HashMap<String, Integer> selectLastMonthClientStatusValue(SqlSessionTemplate sqlSession);

	HashMap<String, Integer> selectThreeMonthAgoClientStatusValue(SqlSessionTemplate sqlSession);

	List<Integer> selectCllientRankList(SqlSessionTemplate sqlSession);

	List<Object> selectClientDataStatus(SqlSessionTemplate sqlSession);

}
