package com.kh.sm.status.clientStatus.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StatusClientDaoImpl implements StatusClientDao {

	@Override
	public HashMap<String, Integer> selectNowClientStatusValue(SqlSessionTemplate sqlSession) {
		
		//현재 고객 인원 검색
		int selectNowClientValue = sqlSession.selectOne("statusClient.selectNowClientValue");
		//현재 고객사 갯수 검색
		int selectNowCompanyValue = sqlSession.selectOne("statusClient.selectNowCompanyValue");
		//현재 잠재고객 인원 검색
		int selectNowLeadClientValue = sqlSession.selectOne("statusClient.selectNowLeadClientValue");
		
		HashMap<String, Integer> statusClientList = new HashMap<String, Integer>();
		
		statusClientList.put("NowClient", selectNowClientValue);
		statusClientList.put("NowCompany", selectNowCompanyValue);
		statusClientList.put("NowLeadClient", selectNowLeadClientValue);
		
		return statusClientList;
	}

	@Override
	public HashMap<String, Integer> selectLastMonthClientStatusValue(SqlSessionTemplate sqlSession) {
		
		//전월 고객 인원 검색
		int selectLastMonthClientValue = sqlSession.selectOne("statusClient.selectLastMonthClientValue");
		//전원 고객사 갯수 검색
		int selectLastMonthCompanyValue = sqlSession.selectOne("statusClient.selectLastMonthCompanyValue");
		//전월 잠재고객 인원 검색
		int selectLastMonthLeadClientValue = sqlSession.selectOne("statusClient.selectLastMonthLeadClientValue");
		
		HashMap<String, Integer> selectLastMonthClientList = new HashMap<String, Integer>();
		
		selectLastMonthClientList.put("lastMonthClient", selectLastMonthClientValue);
		selectLastMonthClientList.put("lastMonthCompany", selectLastMonthCompanyValue);
		selectLastMonthClientList.put("lastMonthLeadClient", selectLastMonthLeadClientValue);
		
		return selectLastMonthClientList;
	}

	@Override
	public HashMap<String, Integer> selectThreeMonthAgoClientStatusValue(SqlSessionTemplate sqlSession) {
		
		//3개월전 고객 인원 검색
		int selectThreeMonthAgoClientStatusValue = sqlSession.selectOne("statusClient.selectThreeMonthAgoClientStatusValue");
		//3개월전 고객사 갯수 검색
		int selectThreeMonthAgoCompanyStatusValue = sqlSession.selectOne("statusClient.selectThreeMonthAgoComapnyStatusValue");
		//3개월전 잠재고객 인원 검색
		int selectThreeMonthAgoLeadClientStatusValue = sqlSession.selectOne("statusClient.selectThreeMonthAgoLeadClientValue");
		
		HashMap<String, Integer> selectThreeMonthClientList = new HashMap<String, Integer>();
		
		selectThreeMonthClientList.put("threeMonthClient", selectThreeMonthAgoClientStatusValue);
		selectThreeMonthClientList.put("threeMonthCompany", selectThreeMonthAgoCompanyStatusValue);
		selectThreeMonthClientList.put("threeMonthLeadClient", selectThreeMonthAgoLeadClientStatusValue);
		
		return selectThreeMonthClientList;
	}

	@Override
	public List<Integer> selectCllientRankList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("statusClient.selectClientRank");
	}

	@Override
	public List<Object> selectClientDataStatus(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("statusClient.selectClientDataStatus");
	}

}
