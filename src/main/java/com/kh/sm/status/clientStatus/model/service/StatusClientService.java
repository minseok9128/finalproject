package com.kh.sm.status.clientStatus.model.service;

import java.util.HashMap;
import java.util.List;

public interface StatusClientService {

	HashMap<String, Integer> selectNowClientStatusValue();

	HashMap<String, Integer> selectlastMonthClientStatusValue();

	HashMap<String, Integer> selectThreeMonthAgoClientStatusValue();

	List<Integer> selectClientRankList();

	List<Object> selectClientDataStatus();

}
