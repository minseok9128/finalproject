package com.kh.sm.status.clientStatus.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.status.clientStatus.model.dao.StatusClientDao;

@Service
public class StatusClientServiceImpl implements StatusClientService{
	
	@Autowired
	public StatusClientDao sd;
	
	@Autowired
	public SqlSessionTemplate sqlSession;
	
	@Override
	public HashMap<String, Integer> selectNowClientStatusValue() {
		return sd.selectNowClientStatusValue(sqlSession);
	}

	@Override
	public HashMap<String, Integer> selectlastMonthClientStatusValue() {
		return sd.selectLastMonthClientStatusValue(sqlSession);
	}

	@Override
	public HashMap<String, Integer> selectThreeMonthAgoClientStatusValue() {
		return sd.selectThreeMonthAgoClientStatusValue(sqlSession);
	}

	@Override
	public List<Integer> selectClientRankList() {
		return sd.selectCllientRankList(sqlSession);
	}

	@Override
	public List<Object> selectClientDataStatus() {
		return sd.selectClientDataStatus(sqlSession);
	}

}
