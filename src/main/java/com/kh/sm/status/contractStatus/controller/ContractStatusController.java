package com.kh.sm.status.contractStatus.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.status.contractStatus.model.service.ContractStatusService;

@Controller
public class ContractStatusController {
	
	@Autowired
	private final ContractStatusService service;
	
	public ContractStatusController(ContractStatusService service) {
		
		this.service = service;
		
	}
	
	@RequestMapping(value="showContractStatus.scs", method=RequestMethod.GET)
	public String showContractStatus(Model model) {
		
		int oppTotalCount = service.selectOppCount();
		Opportunity opp = service.selectAvgOpp();
		List<Opportunity> oppList = service.selectChart1();
		
		HashMap<String,Object> hmap = service.selectStatus();
		
		

		
		
		model.addAttribute("oppTotalCount",oppTotalCount);
		model.addAttribute("opp",opp);
		model.addAttribute("oppList",oppList);
		
		model.addAttribute("hmap",hmap);
		
		
		return "status/contractStatus/contractStatus";
	}
	
	@RequestMapping(value="dataContractStatus.scs", method=RequestMethod.GET)
	public String dataContractStatus(Model model) {
		
		int oppTotalCount = service.selectOppCount();
		List<Opportunity> oppList = service.selectChart1();
		List<Opportunity> oppList2 = service.selectOpp();
		
		model.addAttribute("oppTotalCount",oppTotalCount);
		model.addAttribute("oppList",oppList);
		model.addAttribute("oppList2",oppList2);
		
		
		
		
		return "status/contractStatus/dataContractStatus";
	}
	
	
	
	
	
}
