package com.kh.sm.status.contractStatus.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.sm.salesOpportunity.model.vo.Opportunity;

public interface ContractStatusDao {

	int selectOppCount(SqlSessionTemplate sqlSession);

	Opportunity selectAvgOpp(SqlSessionTemplate sqlSession);

	List<Opportunity> selectChart1(SqlSessionTemplate sqlSession);

	HashMap<String, Object> selectStatus(SqlSessionTemplate sqlSession);

	List<Opportunity> selectOpp(SqlSessionTemplate sqlSession);



}
