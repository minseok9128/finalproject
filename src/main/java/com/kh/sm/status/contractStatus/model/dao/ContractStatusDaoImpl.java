package com.kh.sm.status.contractStatus.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.kh.sm.salesOpportunity.model.vo.Opportunity;

@Repository
public class ContractStatusDaoImpl implements ContractStatusDao{

	@Override
	public int selectOppCount(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectOne("StatusContract.selectOppCount");
	}

	@Override
	public Opportunity selectAvgOpp(SqlSessionTemplate sqlSession) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("StatusContract.selectAvgOpp");
	}

	@Override
	public List<Opportunity> selectChart1(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("StatusContract.selectChart1");
	}

	@Override
	public HashMap<String, Object> selectStatus(SqlSessionTemplate sqlSession) {
		HashMap<String,Object> hmap = new HashMap<>();
		
		hmap.put("month", sqlSession.selectList("StatusContract.statusMonth"));
		
		return hmap;
	}

	@Override
	public List<Opportunity> selectOpp(SqlSessionTemplate sqlSession) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("StatusContract.selectOpp");
	}

	

}
