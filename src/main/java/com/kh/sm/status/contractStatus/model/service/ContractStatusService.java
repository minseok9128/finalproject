package com.kh.sm.status.contractStatus.model.service;

import java.util.HashMap;
import java.util.List;

import com.kh.sm.salesOpportunity.model.vo.Opportunity;

public interface ContractStatusService {

	//종료(성공) 영업기회 갯수
	int selectOppCount();

	//평균값 가져오기
	Opportunity selectAvgOpp();

	List<Opportunity> selectChart1();

	HashMap<String, Object> selectStatus();

	List<Opportunity> selectOpp();



}
