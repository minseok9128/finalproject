package com.kh.sm.status.contractStatus.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.sm.salesOpportunity.model.vo.Opportunity;
import com.kh.sm.status.contractStatus.model.dao.ContractStatusDao;

@Service
public class ContractStatusServiceImpl implements ContractStatusService{

	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ContractStatusDao dao;

	@Override
	public int selectOppCount() {
		// TODO Auto-generated method stub
		return dao.selectOppCount(sqlSession);
	}

	@Override
	public Opportunity selectAvgOpp() {
		// TODO Auto-generated method stub
		return dao.selectAvgOpp(sqlSession);
	}

	@Override
	public List<Opportunity> selectChart1() {
		// TODO Auto-generated method stub
		return dao.selectChart1(sqlSession);
	}

	@Override
	public HashMap<String, Object> selectStatus() {
		
		return dao.selectStatus(sqlSession);
	}

	@Override
	public List<Opportunity> selectOpp() {
		// TODO Auto-generated method stub
		return dao.selectOpp(sqlSession);
	}


	
	
}
