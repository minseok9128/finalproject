package com.kh.sm.support.model.vo;

import com.kh.sm.employee.model.vo.Employee;
import com.kh.sm.employee.model.vo.Employee2;

public class Support { 
	
	private int supportId;
	private int category;
	private int enroller;
	private int oppId;
	private int empNo;
	private Employee employee;
	private Employee2 employee2;
	
	public Support() {}

	public Support(int supportId, int category, int enroller, int oppId, int empNo, Employee employee,
			Employee2 employee2) {
		super();
		this.supportId = supportId;
		this.category = category;
		this.enroller = enroller;
		this.oppId = oppId;
		this.empNo = empNo;
		this.employee = employee;
		this.employee2 = employee2;
	}

	public int getSupportId() {
		return supportId;
	}

	public void setSupportId(int supportId) {
		this.supportId = supportId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getEnroller() {
		return enroller;
	}

	public void setEnroller(int enroller) {
		this.enroller = enroller;
	}

	public int getOppId() {
		return oppId;
	}

	public void setOppId(int oppId) {
		this.oppId = oppId;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Employee2 getEmployee2() {
		return employee2;
	}

	public void setEmployee2(Employee2 employee2) {
		this.employee2 = employee2;
	}

	@Override
	public String toString() {
		return "Support [supportId=" + supportId + ", category=" + category + ", enroller=" + enroller + ", oppId="
				+ oppId + ", empNo=" + empNo + ", employee=" + employee + ", employee2=" + employee2 + "]";
	}



	
	
	

}
