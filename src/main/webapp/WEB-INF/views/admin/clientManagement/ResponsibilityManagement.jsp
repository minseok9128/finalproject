<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>


<style>
#contentDiv {
	border: 1px solid #E5E5E5;
	width: 30%;
	display: inline-block;
	height: 665px;
	margin-right: 20px;
	float: left;
}

#contentDiv2_2 {
	border: 1px solid #E5E5E5;
	width: 67%;
	float: left;
	height: 665px;
	white-space: nowrap;
	overflow: auto;
	display: inline-block;
}

#table {
	width: 100%;
	border-top: 3px solid #27AAE1;
	text-align: center;
}

#table td {
	vertical-align: middle;
	border-bottom: 1px solid #E5E5E5;
	height: 40px;
}

#table tr:nth-child(1) {
	height: 60px;
	border-bottom: 2px solid #E5E5E5;
}

#table2 {
	width: 100%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 7px;
}

#table2 td:nth-child(1) {
	color: #0474AF;
}

#table2 td:nth-child(3) {
	color: #0474AF;
}

#table3 {
	width: 300%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 7px;
}

#table3 td {
	border-bottom: 1px solid #E5E5E5;
	height: 30px;
}

#table3 th {
	border-bottom: 1px solid #E5E5E5;
	height: 30px;
}

#company_table_modal {
	width: 100%;
}

.content2 {
	width: 100%;
	height: 500px
}

#contain {
	width: 90%;
	height: 87%;
	margin: 0auto;
}

#contain>div:nth-child(1) {
	width: 100%;
	height: 60px;
	margin-top: 20px;
	margin-left: auto;
	margin-right: auto;
}

#text_div {
	width: 85%;
	height: 45px;
	border: 1px solid #D1D6DE;
	float: left;
}

#text_div input[type='text'] {
	width: 98%;
	height: 35px;
	margin-left: 5px;
	margin-top: 5px;
	border: none;
	float: left;
}

#text_div input[type='text']:focus {
	outline: none;
}

#check_div {
	width: 15%;
	height: 45px;
	float: left;
	text-align: center;
	border-bottom: 1px solid #D1D6DE;
	border-top: 1px solid #D1D6DE;
	border-right: 1px solid #D1D6DE;
	padding-top: 14px;
}

#check_div input[type='checkbox'] {
	vertical-align: middle;
	height: 16px;
}

.result_div {
	height: 70px;
	border-bottom: 1px solid #D1D6DE;
}

.result_div h4:nth-child(1) {
	padding-top: 10px;
}

.result_div h4 {
	margin-top: 12px;
}

#contain {
	width: 90%;
	height: 87%;
	margin: 0 auto;
}

.search_box {
	margin-top: 10px;
	border: 1px solid #D7DBDF;
	margin-bottom: 10px;
	border-radius: .28571429rem;
	background: #FBFCFD;
	width: 758px;
}

.second_line {
	margin-top: 10px;
	margin-bottom: 10px;
	margin-left: 10px;
}

.ui.calendar {
	margin-left: 0;
}

.ui.form {
	display: inline-block;
	margin: 0;
}

.ui.form>.two.fields {
	min-width: 280px;
	margin: 0;
}

.ui.form>.two.fields>.field {
	padding: 0;
}

.btn_box {
	float: right;
	display: inline-block;
	margin-top:5px;
	margin-bottom:5px;
}
th{
 		color:#27AAE1 !important;
 		
 	}
</style>


</head>
<body>

	<jsp:include page="../../common/adminMenubar.jsp" />

	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="title"></div>

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>고객관리 > 담당자 관리
				</div>

				<div style="margin-top: 20px;">
					<div class="search_box">

						<div class="second_line">
							<div class="ui input right icon" style="margin-right: 10px;">
								<i class="ellipsis vertical icon"></i> <input type="text"
									placeholder="고객사">
							</div>
							<div class="ui input right icon">
								<i class="ellipsis vertical icon"></i> <input type="text"
									placeholder="고객">
							</div>
							<button class="ui inverted primary button"
								style="width: 70px; float: right;">조회</button>
						</div>
					</div>
					<div id="contentDiv">
						<h3 style="margin-left: 5px; margin-top: 10px;">담당자</h3>

						<table id="table">
							<tr>
								<td style="color: #0474AF">담당자명</td>
								<td><div class="ui input">
										<input type="text" style="width: 100px; height: 28px;">
									</div></td>
								<td><button id="submit_Btn">조회</button></td>
							</tr>

							<tr>
								<td colspan="3">서범수</td>


							</tr>
							<tr>
								<td colspan="3">황규환</td>


							</tr>
							<tr>
								<td colspan="3">김민석</td>


							</tr>
						</table>

					</div>





					<div id="contentDiv2_2">
						<div style="margin-left: 5px; margin-top:10px; display:inline-block;"><label><font size="3"><b>고객</b></font></label> </div>
						<div class="btn_box" >
						
							<button class="ui inverted green button">
								<i class="exclamation circle icon"></i>담당자이관
							</button>
							<button class="ui inverted green button">
								<i class="exclamation circle icon"></i>고객해제
							</button>
							<button class="ui inverted green button">
								<i class="file excel icon"></i>엑셀
							</button>
							<button class="ui inverted red button">
								<i class="undo alternate icon"></i>초기화
							</button>
						</div>
						<table id="table3">

							<tr>	
								<th><input type="checkbox"></th>
								<th>고객사</th>
								<th>고객명</th>
								<th>부서</th>
								<th>직책</th>
								<th>휴대번호</th>
								<th>유선번호</th>
								<th>메일</th>
								<th>고객등급</th>
								<th>KeyMan</th>
								<th>활동일시</th>
								
								<th>담당자</th>
								<th>등록자</th>
								<th>등록일</th>
								<th>변경자</th>
								<th>변경일</th>
							</tr>
							


						</table>
					</div>

				</div>


			</div>


		</div>




	</div>



</body>
</html>