<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

 
 <style>
 

 #contentDiv{
 border: 1px solid #E5E5E5;
  width:100%;
 float: left;
 height:300px;
white-space:nowrap; 
overflow:auto;

 
 }
 
  #contentDiv2{
 border: 1px solid #E5E5E5;
  width:100%;
 float: left;
 height:450px;
 margin-top:20px;
white-space:nowrap; 
overflow:auto;

 
 }
 
       #table3 {
        width: 300%;
             border-top: 3px solid #27AAE1;
             text-align: center;
              border-collapse: separate;
  				border-spacing: 0 7px;
              
       }
       
       #table3 td {
       border-bottom: 1px solid #E5E5E5;
       height:30px;
       
       }
        #table3 th {
       border-bottom: 1px solid #E5E5E5;
        height:30px;
       }
       
 #company_table_modal{width: 100%;}
.content2{width: 100%;height: 500px}
#contain {width: 90%;height:87%;margin:0auto;}
#contain > div:nth-child(1) {width: 100%;height: 60px;margin-top: 20px;margin-left: auto;margin-right: auto;}
#text_div{width: 85%;height: 45px;border: 1px solid #D1D6DE;float: left;}
#text_div input[type='text'] {width: 98%;height: 35px;margin-left: 5px;margin-top: 5px;border: none;float: left;}
#text_div input[type='text']:focus {outline: none;} 
#check_div {width: 15%;height: 45px;float: left;text-align: center;border-bottom: 1px solid #D1D6DE;border-top: 1px solid #D1D6DE;border-right: 1px solid #D1D6DE;padding-top: 14px;}
#check_div input[type='checkbox'] {vertical-align: middle;height: 16px;}
.result_div {height: 70px;border-bottom: 1px solid #D1D6DE;}
.result_div h4:nth-child(1) {padding-top: 10px;}
.result_div h4 {margin-top: 12px;}
#contain {width: 90%;height: 87%;margin: 0 auto;}
     
 .btn_box{
 		float:right;
 		display:inline-block;
 		margin-top:5px;
 	}
 	
 	.search_box{
 		border:1px solid #D7DBDF;
 		margin-bottom:10px;
 		border-radius:.28571429rem;
 		background:#FBFCFD;
 	}
 		.second_line {
 	margin-top:10px;
 		margin-bottom:10px;
 		margin-left:10px;
 	}
 	.ui.calendar{
 		margin-left:0;
 	}
 	
 	.ui.form {
 		display:inline-block;
 		margin:0;
 	}
 	.ui.form > .two.fields{
 		min-width: 280px;
 		margin: 0;
 	}
 	.ui.form > .two.fields > .field{
 		padding: 0;
 	}
 	
 	
 
 </style>


</head>
<body>

<jsp:include page="../../common/adminMenubar.jsp" />

<div class="inner_ct">
<div class="main_ctn" style="background: white;">

<div class="title">



</div>

<div class="tb_cnt" >
<div class="tb_title" ><i class="home icon"></i>고객관리 > 고객사/고객 업로드</div>

<div style="margin-top:20px;">

<div class="search_box">
					
					<div class="second_line">
					
					<div class="ui form" style="margin-right:20px;">
						<div class="two fields">
						    <div class="field " style="margin-right:20px;">
						      <div class="ui calendar" id="rangestart" >
						        <div class="ui input right icon">
						          <i class="calendar icon" ></i>
						          <input class="cld" type="text" placeholder="시작일" >
						        </div>
						      </div>
						    </div>
						    
						    <div class="field">
						      <div class="ui calendar" id="rangeend" >
						        <div class="ui input right icon">
						          <i class="calendar icon" ></i>
						          <input class="cld" type="text" placeholder="종료일" >
						        </div>
						      </div>
						    </div>
				  		</div>
				  		</div>
					<div class="ui input right icon">
							<input type="text" placeholder="고객사" >
						</div>
						
						
					
						
					<button class="ui inverted primary button" style="width:70px; float:right;">조회</button>
					</div>
				</div>


<div id="contentDiv">
<h3 style=" margin-left:5px; margin-top:10px;">고객사/고객 업로드</h3>
<table id="table3">

<tr>
	<th>고객사명</th>
	<th>고객사 구분</th>
	<th>고객사 등급</th>
	<th>매출(년)</th>
	<th>사원수</th>
	<th>유선번호</th>
	<th>팩스번호</th>
	<th>웹사이트</th>
	<th>주소</th>
	<th>고객명</th>
	<th>메일</th>
	<th>휴대번호</th>
	<th>유선번호</th>
	<th>고객등급</th>
	<th>부서</th>
	<th>직책</th>
	<th>담당자명</th>
	
	
	
	
</tr>

<tr>
	<td>2020.01.17</td>
	<td>관리자</td>
	<td>admin</td>
	<td>a123</td>
	<td>010-0000-0000</td>
	<td>rbghks112@naver.com</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>

</tr>

<tr>
	<td>2020.01.17</td>
	<td>관리자</td>
	<td>admin</td>
	<td>a123</td>
	<td>010-0000-0000</td>
	<td>rbghks112@naver.com</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>

</tr>


</table>
</div>

<div id="contentDiv2">
<div style="display:inline-block; margin-bottom:15px;"><h3 style=" margin-left:5px; margin-top:10px;">고객/고객사 임시저장</h3></div>
<div class="btn_box">

  <button class="ui inverted green button"><i class="file excel icon"></i>양식다운로드</button>
    <button class="ui inverted green button"><i class="file excel icon"></i>가져오기</button>
							
							<button class="ui inverted red button"><i class="undo alternate icon"></i>초기화</button>
							
 <button class="ui inverted primary button" style="width:100px;">등록</button>
						</div>
<table id="table3">

<tr>

	<th style="width:10%">업로드결과</th>
	<th>임시저장일</th>
	<th>고객사명</th>
	<th>고객사 구분</th>
	<th>고객사 등급</th>
	<th>매출(년)</th>
	<th>사원수</th>
	<th>유선번호</th>
	<th>팩스번호</th>
	<th>웹사이트</th>
	<th>주소</th>
	<th>고객명</th>
	<th>메일</th>
	<th>휴대번호</th>
	<th>유선번호</th>
	<th>고객등급</th>
	<th>부서</th>
	<th>직책</th>
	<th>담당자명</th>
	
	
</tr>

<tr>
	<td>업로드 성공</td>
	<td>2020-01-20</td>
	<td>2020.01.17</td>
	<td>관리자</td>
	<td>admin</td>
	<td>a123</td>
	<td>010-0000-0000</td>
	<td>rbghks112@naver.com</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>

</tr>

<tr>
<td>업로드 성공</td>
	<td>2020-01-20</td>
	<td>2020.01.17</td>
	<td>관리자</td>
	<td>admin</td>
	<td>a123</td>
	<td>010-0000-0000</td>
	<td>rbghks112@naver.com</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
	<td>admin</td>
</tr>


</table>
</div>

</div>


</div>


</div>




</div>



</body>
</html>