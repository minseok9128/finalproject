<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
#contentDiv2 {
	border: 1px solid #E5E5E5;
	width: 100%;
	height: 390px;
	margin-bottom: 10px;
}

#table2 {
	width: 100%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 20px;
}

#table2 td:nth-child(1) {
	color: #0474AF;
}

#table2 td:nth-child(3) {
	color: #0474AF;
}
</style>


</head>
<body>

	<jsp:include page="../../common/adminMenubar.jsp" />

	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="title"></div>

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>직원 > 직원등록
				</div>
				<form action="insert.em" method="post" id="empInsert">
					<div style="margin-top: 20px;">


						<div id="contentDiv2">
							<h3 style="margin-left: 5px; margin-top: 10px;">직원 정보입력</h3>




							<table id="table2">
								<tr>
									<td>사원명</td>
									<td><div class="ui input">
											<input type="text" id="empName" name="empName"
												style="width: 200px; height: 32px;">
										</div></td>
									<td>부서명</td>
									<td><div class="ui input">
											<input type="text" id="deptName" name="deptName" value="영업부"
												style="width: 200px; height: 32px;">
										</div></td>
								</tr>

								<tr>

									<td>회원구분</td>
									<td><select class="ui dropdown" name="empType"
										id="day_select" style="width: 200px; height: 32px;">
											<option value=2>영업책임자</option>
											<option value=3>영업사원</option>

									</select></td>

									<td>비밀번호</td>
									<td><div class="ui input">
											<input type="password" id="empPwd" name="empPwd"
												style="width: 200px; height: 32px;">
										</div></td>
								</tr>

								<tr>
									<td>아이디</td>
									<td>
										<div style="position: relative;">
											<div class="ui input">
												<input type="text" id="empId" name="empId"
													style="width: 200px; height: 32px;">

											</div>

											<div style="position: absolute; margin-left: 50px;">
												<label id="id-success" style="color: green;"><font
													size="1">사용가능한아이디입니다.</font></label> <label id="id-danger"
													style="color: red;"><font size="1">사용불가능한아이디입니다.</font></label>
											</div>
										</div>
									</td>

									<td>비밀번호확인</td>
									<td>
										<div style="position: relative;">
											<div class="ui input">
												<input type="password" id="empPwd2" name="empPwd2"
													style="width: 200px; height: 32px;">

											</div>

											<div style="position: absolute; margin-left: 50px;">
												<label id="alert-success" style="color: green;"><font
													size="1">사용가능한비밀번호입니다.</font></label> <label id="alert-danger"
													style="color: red;"><font size="1">사용불가능한비밀번호입니다.</font></label>
											</div>
										</div>
									</td>


								</tr>






								<tr>
									<td>이메일</td>
									<td><div class="ui input">
											<input type="email" id="email" name="email"
												style="width: 200px; height: 32px;">
										</div></td>
									<td>휴대폰</td>
									<td><div class="ui input">
											<input type="text" id="empPhone" name="empPhone"
												style="width: 200px; height: 32px;">
										</div></td>

								</tr>

								<tr>

									<td>유선번호</td>
									<td><div class="ui input">
											<input type="text" id="comPhone" name="comPhone"
												style="width: 200px; height: 32px;">
										</div></td>

								</tr>

								<tr>
									<td>비고</td>
									<td colspan="3"><div class="ui input">
											<input type="text" id="empRemark" name="empRemark"
												style="width: 610px; height: 40px;">
										</div></td>

								</tr>

							</table>


						</div>

						<div style="float: right">
							<button class="ui inverted red button" type="reset"
								style="width: 100px;" id="resetBtn">취소</button>
							<input type="button" class="ui inverted primary button" style="width: 100px;" value="등록" id="enroll">
						</div>


					</div>

				</form>
			</div>


		</div>

		<script>
		
		$(function(){
	        // 열리지 않는 메뉴
	       $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
	        
	        // 열리는 메뉴
	        $(".menu_cnt .main_menu").eq(5).addClass("on").addClass("open");
	        $(".menu_cnt .main_menu").eq(5).find(".sub_menu_list").eq(1).addClass("on");
	   				
				$("#enroll").click(function(){
					
					
				
					$("#empInsert").submit();
					
					
				})
				
				
				$("#id-success").hide();
				$("#id-danger").hide();
				$("#alert-success").hide();
				$("#alert-danger").hide();

				$("input[type=password]").keyup(function() {

					var pwd1 = $("#empPwd").val();
					var pwd2 = $("#empPwd2").val();

					if (pwd1 != "" || pwd2 != "") {

						if (pwd1 == pwd2) {

							$("#alert-success").show();
							$("#alert-danger").hide();

						} else {

							$("#alert-success").hide();
							$("#alert-danger").show();

						}

					}

				})

				$("#empId").keyup(function() {

					var empId = $("#empId").val();

					$.ajax({

						url : "duplicationCheck.em",
						type : "post",
						data : {
							empId : empId
						},
						success : function(data) {

							console.log(data);

							//중복된경우
							if (data === "success") {
								$("#id-success").hide();
								$("#id-danger").show();
								document.getElementById('empId').select();
							

							} else if (data === "fail") {

								if (empId == "") {
									$("#id-success").hide();
									$("#id-danger").hide();
								} else {
									$("#id-success").show();
									$("#id-danger").hide();
								}

							}

						},
						error : function(status) {

						}

					})

				})

			})
		</script>
</body>
</html>