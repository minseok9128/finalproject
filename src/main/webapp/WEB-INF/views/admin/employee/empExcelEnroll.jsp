<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>


<style>
#contentDiv {
	border: 1px solid #E5E5E5;
	width: 100%;
	float: left;
	height: 300px;
	white-space: nowrap;
	overflow: auto;
}

#contentDiv2 {
	border: 1px solid #E5E5E5;
	width: 100%;
	float: left;
	    height: 498px;
	margin-top: 20px;
	
}

#contentDiv3 {
	border: 1px solid #E5E5E5;
	width: 100%;
	float: left;
	height: 450px;

	white-space: nowrap;
	overflow: auto;
}

#table3 {
	width: 180%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 7px;
}

#table3 td {
	border-bottom: 1px solid #E5E5E5;
	height: 30px;
}

#table3 th {
	border-bottom: 1px solid #E5E5E5;
	height: 30px;
}

#company_table_modal {
	width: 100%;
}

.content2 {
	width: 100%;
	height: 500px
}

#contain {
	width: 90%;
	height: 87%;
	margin: 0auto;
}

#contain>div:nth-child(1) {
	width: 100%;
	height: 60px;
	margin-top: 20px;
	margin-left: auto;
	margin-right: auto;
}

#text_div {
	width: 85%;
	height: 45px;
	border: 1px solid #D1D6DE;
	float: left;
}

#text_div input[type='text'] {
	width: 98%;
	height: 35px;
	margin-left: 5px;
	margin-top: 5px;
	border: none;
	float: left;
}

#text_div input[type='text']:focus {
	outline: none;
}

#check_div {
	width: 15%;
	height: 45px;
	float: left;
	text-align: center;
	border-bottom: 1px solid #D1D6DE;
	border-top: 1px solid #D1D6DE;
	border-right: 1px solid #D1D6DE;
	padding-top: 14px;
}

#check_div input[type='checkbox'] {
	vertical-align: middle;
	height: 16px;
}

.result_div {
	height: 70px;
	border-bottom: 1px solid #D1D6DE;
}

.result_div h4:nth-child(1) {
	padding-top: 10px;
}

.result_div h4 {
	margin-top: 12px;
}

#contain {
	width: 90%;
	height: 87%;
	margin: 0 auto;
}

.btn_box {
	float: right;
	display: inline-block;
	margin-top: 5px;
}
</style>


</head>
<body>

	<jsp:include page="../../common/adminMenubar.jsp" />

	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="title"></div>

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>직원 > 직원엑셀등록
					<button class="ui inverted green button" id="yangdown" style="float:right;">
								<i class="file excel icon"></i>양식다운로드
							</button>
				</div>

				<div style="margin-top: 20px;">




					<div id="contentDiv">
						<h3 style="margin-left: 5px; margin-top: 10px;">직원업로드</h3>
						<table id="table3">

							<tr>
								<th>사원명</th>
								<th>부서명</th>
								<th>아이디</th>
								<th>비밀번호</th>
								<th>휴대폰번호</th>
								<th>이메일</th>

							</tr>

							<tr>
								<td>2020.01.17</td>
								<td>관리자</td>
								<td>admin</td>
								<td>a123</td>
								<td>010-0000-0000</td>
								<td>rbghks112@naver.com</td>

							</tr>

							<tr>
								<td>2020.01.17</td>
								<td>관리자</td>
								<td>admin</td>
								<td>a123</td>
								<td>010-0000-0000</td>
								<td>rbghks112@naver.com</td>

							</tr>


						</table>
					</div>
					
					<form id="excelUploadForm" enctype="multipart/form-data" method="post" action="insertExcel.em">
							<input type="file" id="file" name="excelFile" style="display:none;">
					</form>

					<div id="contentDiv2">
						<div style="display: inline-block; margin-bottom: 15px;">
							<h3 style="margin-left: 5px; margin-top: 10px;">직원 임시저장</h3>
						</div>
						<div class="btn_box">
						
							

							
							
							
							
							<button class="ui inverted green button" id="excelDown">
								<i class="file excel icon"></i>가져오기
							</button>
						
							
							<button class="ui inverted red button">
								<i class="undo alternate icon"></i>초기화
							</button>
							
							

							<button class="ui inverted primary button" style="width: 100px;">등록</button>
							
						
						</div>
						
						<div id="contentDiv3">
						<table id="table3">

							<tr>

								<th style="width: 15%">업로드결과</th>
								
								<th>사원명</th>
								<th>아이디</th>
								<th>비밀번호</th>
								<th>부서명</th>
								<th>휴대폰번호</th>
								<th>이메일</th>
								<th>회사유선번호</th>
								<th>회원구분</th>
								<th>등록일</th>
								
								

							</tr>

							<tr>
								<td>업로드 성공</td>
								<td>황규환</td>
								<td>rbghks1121</td>
								<td>rb1121</td>
								<td>영업부</td>
								<td>010-0000-0000</td>
								<td>rbghks1121@naver.com</td>
								<td>020000000</td>
								<td>영업사원</td>
								<td>2020-01-30</td>
								

							</tr>

								<tr>
								<td>업로드 성공</td>
								<td>황규환</td>
								<td>rbghks1121</td>
								<td>rb1121</td>
								<td>영업부</td>
								<td>010-0000-0000</td>
								<td>rbghks1121@naver.com</td>
								<td>020000000</td>
								<td>영업사원</td>
								<td>2020-01-30</td>
								

							</tr>

						</table>
						
						<table class="download_Table" id="reportTable">
						
						 <c:set var="att" value="${att}"/>
						 
						 <tr>
						 	<td><input type="hidden" value="${att.originName} "></td>
						 	<td><input type="hidden" value="${att.changeName} "></td>
						 	<td><input type="hidden" value="${att.filePath} "></td>
						 </tr>
						
						
						</table>
						</div>
					</div>

				</div>


			</div>


		</div>




	</div>
	
	<script>
	$("#yangdown").click(function(){
		
			var originName = $("#reportTable tr").children().eq(0).children().eq(0).val();
			var fileName = $("#reportTable tr").children().eq(1).children().eq(0).val();
			var filePath = $("#reportTable tr").children().eq(2).children().eq(0).val();
		
			var extension = originName.split(".");
		
			var encodefilePath = encodeURI(filePath);
			fileName = fileName + '.' + extension[1];
			
			location.href = "download.do?fileName="
					+ fileName + "&filePath="
					+ encodefilePath + "&originName="
					+ originName; 
	
	})
	$(function(){
		
		$("#excelDown").click(function(e){
			
			e.preventDefault();

			$('#file').click();

			$('#file').on('change', function(){
		
				 var file = $("#file").val();
			
				 if (file == "" || file == null) {
	                    alert("파일을 선택해주세요.");
	                    return false;
	                } else if (!checkFileType(file)) {
	                    alert("엑셀 파일만 업로드 가능합니다.");
	                    return false;
	                }
	 
				 if (confirm("업로드 하시겠습니까?")) {
					 
					 var options = {
		                        success : function(data) {
		                            alert("모든 데이터가 업로드 되었습니다.");
		 
		                        },
		                        type : "POST"
		                    };
		                    $("#excelUploadForm").ajaxSubmit(options);


					 
				 }
				 
				 
				 
				 
			})
			
			
		

		
		
		
	})	
		
	})
	
	
	function checkFileType(filePath) {
                var fileFormat = filePath.split(".");
                if (fileFormat.indexOf("xlsx") > -1) {
                    return true;
                } else {
                    return false;
                }
 
            }


	
	
	</script>



</body>
</html>