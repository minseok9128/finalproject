<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

 
 <style>
 #contentDiv {

 overflow: auto;
 border: 1px solid #E5E5E5;
 width:30%;
 display:inline-block;
   height:665px;
   margin-right:20px;
     float: left;
 }
 
  #contentDiv2 {
 

 border: 1px solid #E5E5E5;
 width:65%;
 display:inline-block;
height:290px;
margin-bottom:10px;

 }
 
 #contentDiv2_1{
 
 width:65%;
 float: left;
 margin-bottom:10px;
  height:30px;
 }
 
 
 #contentDiv2_2{
 border: 1px solid #E5E5E5;
  width:65%;
 float: left;
 height:320px;
white-space:nowrap; 
overflow:auto;

 
 }
 
      
       
     #table {
             width: 100%;
             border-top: 3px solid #27AAE1;
             text-align: center;
             
           
       }
       
          #table td {
             vertical-align: middle;
             border-bottom: 1px solid #E5E5E5;
             height:40px;
       }
       #table tr:nth-child(1) {
       
       height:60px;
        border-bottom: 2px solid #E5E5E5;
       
       }
       
       #table2 {
    width: 100%;
             border-top: 3px solid #27AAE1;
             text-align: center;
              border-collapse: separate;
              border-spacing: 0 7px;
       }
 
  #table2 td:nth-child(1) {
       
      color:#0474AF;
       
       }
       #table2 td:nth-child(3) {
       
      color:#0474AF;
       
       }
       
       #table3 {
        width: 150%;
             border-top: 3px solid #27AAE1;
             text-align: center;
              border-collapse: separate;
              border-spacing: 0 7px;
              
       }
       
       #table3 td {
       border-bottom: 1px solid #E5E5E5;
       height:30px;
       
       }
        #table3 th {
       border-bottom: 1px solid #E5E5E5;
        height:30px;
       }
       
 #company_table_modal{width: 100%;}
.content2{width: 100%;height: 500px}
#contain {width: 90%;height:87%;margin:0auto;}
#contain > div:nth-child(1) {width: 100%;height: 60px;margin-top: 20px;margin-left: auto;margin-right: auto;}
#text_div{width: 85%;height: 45px;border: 1px solid #D1D6DE;float: left;}
#text_div input[type='text'] {width: 98%;height: 35px;margin-left: 5px;margin-top: 5px;border: none;float: left;}
#text_div input[type='text']:focus {outline: none;} 
#check_div {width: 15%;height: 45px;float: left;text-align: center;border-bottom: 1px solid #D1D6DE;border-top: 1px solid #D1D6DE;border-right: 1px solid #D1D6DE;padding-top: 14px;}
#check_div input[type='checkbox'] {vertical-align: middle;height: 16px;}
.result_div {height: 70px;border-bottom: 1px solid #D1D6DE;}
.result_div h4:nth-child(1) {padding-top: 10px;}
.result_div h4 {margin-top: 12px;}
#contain {width: 90%;height: 87%;margin: 0 auto;}
     
    .ui.grey.basic.button.srch{font-size: 13px;padding: 9px; margin-bottom: 10px;}
.ui.grey.basic.button.srch:hover{background: #767676;color: white;}
 
 </style>


</head>
<body>

<jsp:include page="../../common/adminMenubar.jsp" />

<div class="inner_ct">
<div class="main_ctn" style="background: white;">

<div class="title">



</div>

<div class="tb_cnt" >
<div class="tb_title" ><i class="home icon"></i>직원 > 직원관리</div>

<div style="margin-top:20px;">
<div id="contentDiv" >
<h3 style=" margin-left:5px; margin-top:10px;">사원</h3>

<!-- <form action="searchEmpName.em" method="post"> -->

<table id="table" >
   <tr id="nameSearch" >
      <td style="color:#0474AF">사원명</td>
      <td><div class="ui input"><input type="text" style="width:100px; height:28px;" id="empName" name="empName"></div></td>
      <td> <input type="button" value="조회" id="submit_Btn" class="ui grey basic button srch searchBtn" style="margin-top:10px;"></td>
   </tr>
   
   
   

   
</table>

<!-- </form> -->

<script>





 $("#submit_Btn").click(function(){
    

    $("td.SEARCH" ).parent().remove();
   
    $("#count").html("");
    $("#submit_Btn2").attr('style', "display:none; ");
	$("#modify_Btn").attr('style', "display:inline-block; width:70px;");
	
	$("#empName2").val("");
	$("#deptName").val("");
	$("#empId").val("");
	$("#empPwd").val("");
	$("#empPhone").val("");
	$("#email").val("");
	$("#comPhone").val("");
	$("#empType").val("");
	$("#enrollDate").val("");
	$("#modifyDate").val("");
	$("#empRemark").val("");
   
	
   
   var empName = $("#empName").val();
   
   $.ajax({
      
      url : "searchEmpName.em",
      type : "post",
      data : {
         empName : empName
      },
      success:function(data) {
    	  
    	 
         
         console.log(data);
         var result = data.list;
         console.log(result[0].empName);
         
         
         var str = "<TR CLASS='TR'>";
         
         $.each(result, function(i){
         
            str += "<TD CLASS='SEARCH' COLSPAN='3'>" + result[i].EMP_NAME + "</TD><TD CLASS='HIDDENNO'><INPUT TYPE='HIDDEN' VALUE='" + result[i].EMP_NO + "'></TD>";
            str += "</TR>";
            
            
         })
          $("#table").append(str); 
         
         $("td.SEARCH").click(function(){
        	 
        		$("#count").html("");
        		
        		var strth = "<tr><th>변경일</th> <th>변경자</th> <th>아이디</th><th>분류</th></tr>";
        		
        		
        		$("#table3").html("");
        		$("#table3").append(strth);
        	 
        	 $("#modify_Btn").click(function(){
        			
        			$("#empName2").attr( 'disabled', false );
        			$("#deptName").attr( 'disabled', false );
        			$("#empId").attr( 'disabled', false );
        			$("#empPwd").attr( 'disabled', false );
        			$("#empPhone").attr( 'disabled', false );
        			$("#email").attr( 'disabled', false );
        			$("#comPhone").attr( 'disabled', false );
        			$("#empType").attr( 'disabled', false );
        			
        			
        			$("#empRemark").attr( 'disabled', false );
        			$("#submit_Btn2").attr('style', "display:inline-block; width:70px;");
        			$("#modify_Btn").attr('style', "display:none; ");
        			
        			
        			
        		})
            
            var empNo = $(this).parent().children().children("input[type='hidden']").val();
            console.log('클릭');
            console.log(empNo);
            
            $.ajax({
            	url:"searchEmp.em",
            	type:"post",
            	data:{
            		empNo:empNo
            	},
            	success:function(data) {
            		
            		console.log(data.employeeAjax.empName);
            		
            		$("#empName2").val(data.employeeAjax.empName);
            		$("#deptName").val(data.employeeAjax.deptName);
            		$("#empId").val(data.employeeAjax.empId);
            		$("#empPwd").val(data.employeeAjax.empPwd);
            		$("#empPhone").val(data.employeeAjax.empPhone);
            		$("#email").val(data.employeeAjax.email);
            		$("#comPhone").val(data.employeeAjax.comPhone);
            		
            		if(data.employeeAjax.empType == 3) {
            			$("#empType").val("3");
            		}else if(data.employeeAjax.empType == 2) {
            			$("#empType").val("2");
            		}
            		
            		$("#enrollDate").val(data.employeeAjax.enrollDate);
            		$("#modifyDate").val(data.employeeAjax.modifyDate);
            		$("#empRemark").val(data.employeeAjax.empRemark);
            		$("#empNo2").val(data.employeeAjax.empNo);
            		$("#empNo3").val(data.employeeAjax.empNo);
            		
            		$("#empName2").attr( 'disabled', true );
            		$("#deptName").attr( 'disabled', true );
            		$("#empId").attr( 'disabled', true );
            		$("#empPwd").attr( 'disabled', true );
            		$("#empPhone").attr( 'disabled', true );
            		$("#email").attr( 'disabled', true );
            		$("#comPhone").attr( 'disabled', true );
            		$("#empType").attr( 'disabled', true );
            		$("#enrollDate").attr( 'disabled', true );
            		$("#modifyDate").attr( 'disabled', true );
            		$("#empRemark").attr( 'disabled', true );
            		
            		var empNo = data.employeeAjax.empNo;
            		
            		
            		
            		 $.ajax({
            			 url:"selectEmpHis.em",
            			 type:"post",
                     	data:{
                     		empNo:empNo
                     	},
                     	success:function(data) {
                     		
                     		console.log(data);
                     		var result = data.employeeHistoryAjaxList;
                     		console.log(result);
                     		
                     		console.log(data.integer);
                     		
                     		$("#count").html(data.integer);
                     		
                     		var str = "<TR>";
                     		var choose = "";
                            $.each(result, function(i){
                            	
                            	
                            	if(result[i].empHisStatus=="MOD") {
                             	    choose = "<TD>수정</TD>";
                                }else if(result[i].empHisStatus=="CRE") {
                                	 choose = "<TD>가입</TD>";
                                }else if(result[i].empHisStatus=="DEL") {
                                	 choose ="<TD>삭제</TD>";
                                };
                           
                            
                               str += "<TD CLASS='SEARCH' >" + result[i].empHisDate + "</TD><TD>관리자</TD><TD>admin</TD>"  + choose
                               
                               str += "</TR>";
                               
                               
                            })
                             $("#table3").append(str); 
                          
                     		
                     	},
                     	error:function(status) {
                     		
                     	}
            			 
            			 
            		 })
            		
            		
            		
            		
            	},
            	error:function(status) {
            		
            	}
            	
            	
            })
            
           
            
         })
         
         
          
      },
      error:function(status) {

      }

      
      
   }) 
   
   
 })






</script>
</div>
<form action="update.em" method="post" id="updateForm">
<div id="contentDiv2" >
<h3 style=" margin-left:5px; margin-top:10px;">정보</h3>

<table id="table2" >
   <tr>
      <td>사원명</td>
      <td><div class="ui input"><input type="text" style="width:150px; height:32px;" id="empName2" name="empName"></div></td>
      <td>부서명</td>
      <td><div class="ui input"><input type="text" style="width:150px; height:32px;" id="deptName" name="deptName"></div></td>
   </tr>
   
   <tr>
      <td>아이디</td>
      <td><div class="ui input"><input type="text" style="width:150px; height:32px;" id="empId" name="empId"></div></td>
      <td>비밀번호</td>
      <td><div class="ui input"><input type="password" style="width:150px; height:32px;" id="empPwd" name="empPwd"></div></td>
   </tr>
   
   <tr>
      <td>휴대폰</td>
      <td><div class="ui input"><input type="text" style="width:150px; height:32px;" id="empPhone" name="empPhone"></div></td>
      <td>이메일</td>
      <td><div class="ui input"><input type="email" style="width:150px; height:32px;" id="email" name="email"></div></td>
   </tr>
   
   <tr>
      <td>회사번호</td>
      <td><div class="ui input"><input type="text" style="width:150px; height:32px;" id="comPhone" name="comPhone"></div></td>
      <td>회원구분</td>
         <td><select class="ui dropdown" name="empType" id="empType" style="width:150px;  height: 32px;">
                <option value="3">영업사원</option>
                <option value="2">영업책임자</option>
                
            </select></td>
            
            
   </tr>
   
   <tr>
      <td>등록일</td>
      <td><div class="ui input"><input type="text" style="width:150px; height:32px;" id="enrollDate" disabled></div></td>
      
   </tr>
   
   <tr>
   <td>비고</td>
   <td colspan="3"><div class="ui input"><input type="text" style="width:400px; height:32px;" id="empRemark" name="empRemark"></div></td>
   <td><input type="hidden" id="empNo2" name="empNo"></td>
   
   </tr>
   
</table>


</div>

<div id="contentDiv2_1">

  <input type="button" id="modify_Btn" value="수정" class="ui grey basic button srch searchBtn" style="width:70px;">
 <input type="button" id="submit_Btn2" value="변경"  class="ui grey basic button srch searchBtn" style=" display:none; ">
 <input type="button" id="cancle_Btn" value="삭제" class="ui grey basic button srch searchBtn" style="width:70px;">
 

</div>
</form>

<form action="delete.em" method="post" id="deleteForm">
<input type="hidden" id="empNo3" name="empNo">

</form>

<div id="contentDiv2_2">
<h3 style=" margin-left:5px; margin-top:10px;">이력 [<label id="count"></label>]건</h3>
<table id="table3">

<tr>
   <th>변경일</th>
   <th>변경자</th>
   <th>아이디</th>
   <th>분류</th>
   
   
</tr>




</table>
</div>

</div>


</div>


</div>




</div>

<script>

$(function(){
	
	
	 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
     
     // 열리는 메뉴
     $(".menu_cnt .main_menu").eq(5).addClass("on").addClass("open");
     $(".menu_cnt .main_menu").eq(5).find(".sub_menu_list").eq(0).addClass("on");
	
	
	$("#cancle_Btn").click(function(){
		
		var con_test = confirm("해당 직원을 정말로 삭제하시겠습니까?");
		if(con_test == true){
			
			$("#deleteForm").submit();
		}
		else if(con_test == false){
		  
		}
		
		
		
		
	})
	
	
})



$("#submit_Btn2").click(function(){
	
	
		
		$("#updateForm").submit();
		
		
	})



</script>



</body>
</html>