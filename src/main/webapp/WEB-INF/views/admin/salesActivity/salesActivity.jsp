<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.title{
		display:inline-block;
	}
 	.btn_box{
 		float:right;
 		display:inline-block;
 	}
 	.search_box{
 		border:1px solid #D7DBDF;
 		margin-bottom:10px;
 		border-radius:.28571429rem;
 		background:#FBFCFD;
 	}
 	.first_line {
 		margin-top:10px;
 		margin-bottom:10px;
 		margin-left:10px;
 	}
 	.second_line {
 		margin-bottom:10px;
 		margin-left:10px;
 	}
 	.search_box *{
 		/* margin-left:10px; */
 		margin-right:10px;
 	}
 	.ui.form {
 		display:inline-block;
 		margin:0;
 	}
 	.ui.form > .two.fields{
 		min-width: 280px;
 		margin: 0;
 	}
 	.ui.form > .two.fields > .field{
 		padding: 0;
 	}
 	.ui.calendar{
 		margin-left:0;
 	}
 	.first_line *{
 	}
 	.table_border{
 		border:1px solid #D7DBDF;
 		border-radius:.28571429rem;
 	}
 	h3 {
 		padding-top:10px;
 	}
 	.table_head{
 		background:#FBFCFD;
 		padding-top:10px;
 		padding-left:10px;
 	}
 	.table_head * *{
 		vertical-align:middle;
 		
 	}
 	.table_content{
 		overflow:auto;
 	}
 	table{
 		width:150%;
 		height:150%;
 	}
 	th ,td{
 		text-align:center;
 		vertical-align:middle !important;
 		height:30px;
 	}
 	th{
 		color:#27AAE1 !important;
 		
 	}
 	.table_content tr *{
 		padding-left:10px;
 	}
 	tr:nth-child(even){
 		background:#F9F9F9;
 	}
 	.line{
 		text-decoration:underline;
 		cursor:pointer;
 		color:#27AAE1;
 	}
</style>
</head>
<body>
	<jsp:include page="../../common/adminMenubar.jsp" />
	<div class="main_ctn" style="background: white;">
	<div class="tb_title" ><i class="home icon"></i>영업활동 > 영업활동</div>
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<div class="search_box">
					<div class="first_line">
						<div class="ui form">
						<div class="two fields">
						    <div class="field">
						      <div class="ui calendar" id="rangestart">
						        <div class="ui input right icon">
						          <i class="calendar icon" ></i>
						          <input class="cld" type="text" placeholder="시작일">
						        </div>
						      </div>
						    </div>
						    <div class="field">
						      <div class="ui calendar" id="rangeend">
						        <div class="ui input right icon">
						          <i class="calendar icon" ></i>
						          <input class="cld" type="text" placeholder="종료일">
						        </div>
						      </div>
						    </div>
				  		</div>
				  		</div> 	
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="고객사">
						</div>
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="고객">
						</div>
					</div>
					<div class="second_line">
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="영업기회">
						</div>
						<div class="ui input right icon">
							<input type="text" placeholder="부서">
						</div>
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="담당자">
						</div>
					<button class="ui basic button">조회</button>
					</div>
				</div>
				
				
				<div class="table_border">
					<div class="table_head">
						<h3 class="title">영업활동[<c:out value="9"/>]건</h3>
						<div class="btn_box">
							<button class="ui green basic button">엑셀</button>
							<button class="ui red basic button">초기화</button>
						</div>
					</div>
					<div class="table_content">
						<table>
							<tr>	
								<th><input type="checkbox"></th>
								<th>활동</th>
								<th>활동일시</th>
								<th>활동시간</th>
								<th>활동분류</th>
								<th>활동목적</th>
								<th>고객사</th>
								<th>고객</th>
								<th>담당자</th>
								<th>영업기회</th>
								<th>계획내용</th>
								<th>활동내용</th>
								<th>등록자</th>
								<th>등록일</th>
								<th>변경자</th>
								<th>변경일</th>
							</tr>
							<c:forEach var="i" begin="0"  end="300">
								<tr>
									<td><input type="checkbox"></td>
									<td><c:out value="Y"/></td>
									<td class="line"><c:out value="2020.01.15"/></td>
									<td><c:out value="22:00"/></td>
									<td><c:out value="전화"/></td>
									<td><c:out value="인사"/></td>
									<td class="line"><c:out value="(주)안동환"/></td>
									<td class="line"><c:out value="안동환"/></td>
									<td><c:out value="황규환"/></td>
									<td class="line"><c:out value="신규서비스"/></td>
									<td><c:out value="전화"/></td>
									<td><c:out value="20일 방문 미팅"/></td>
									<td><c:out value="황규환"/></td>
									<td><c:out value="2020.01.17"/></td>
									<td><c:out value=""/></td>
									<td><c:out value=""/></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
				
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	<script>
		$('#rangestart').calendar({
			  type: 'date',
			  endCalendar: $('#rangeend')
		});
		$('#rangeend').calendar({
			  type: 'date',
			  startCalendar: $('#rangestart')
		});
		
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
            
            // 열리는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on").addClass("open");
            $(".menu_cnt .main_menu").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        });
        //전체 체크 기능
        var j = $("th input[type='checkbox']");
        j.click(function(){
        	if(j.is(":checked")){
        		$("td input[type='checkbox']").prop("checked", true);
        	} else{
        		$("td input[type='checkbox']").prop("checked", false);
        	}
        })
    </script>
		
	</script>
</body>
</html>