<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">

<style>
#contentDiv {
overflow: auto;
	border: 1px solid #E5E5E5;
	width: 30%;
	display: inline-block;
	height: 665px;
	margin-right: 20px;
	float: left;
}

#contentDiv2 {
	border: 1px solid #E5E5E5;
	width: 65%;
	display: inline-block;
	height: 290px;
	margin-bottom: 10px;
}

#contentDiv2_1 {
	width: 65%;
	float: left;
	margin-bottom: 10px;
	height: 30px;
}

#contentDiv2_2 {
	border: 1px solid #E5E5E5;
	width: 65%;
	float: left;
	height: 320px;
	white-space: nowrap;
	overflow: auto;
}



 .ui.grey.basic.button.srch{font-size: 13px;padding: 9px; margin-bottom: 10px;}
.ui.grey.basic.button.srch:hover{background: #767676;color: white;}
 

#table {
	width: 100%;
	border-top: 3px solid #27AAE1;
	text-align: center;
}

#table td {
	vertical-align: middle;
	border-bottom: 1px solid #E5E5E5;
	height: 40px;
}

#table tr:nth-child(1) {
	height: 60px;
	border-bottom: 2px solid #E5E5E5;
}

#table2 {
	width: 100%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 7px;
}

#table2 td:nth-child(1) {
	color: #0474AF;
}

#table2 td:nth-child(3) {
	color: #0474AF;
}

#table3 {
	width: 150%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 7px;
}

#table3 td {
	border-bottom: 1px solid #E5E5E5;
	height: 30px;
}

#table3 th {
	border-bottom: 1px solid #E5E5E5;
	height: 30px;
}
</style>


</head>
<body>

	<jsp:include page="../../common/adminMenubar.jsp" />


	<jsp:include page="../../common/modal/adminProductEnroll.jsp"></jsp:include>

	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="title"></div>

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>영업관리 > 제품관리 <input type="button"
						id="productAdd_Btn" class="ui inverted primary button"
						value="제품추가" style="float: right; margin-right: 20px;">
				</div>

				<div style="margin-top: 20px;">
					<div id="contentDiv">
						<h3 style="margin-left: 5px; margin-top: 10px;">제품</h3>

						
						<form method="post" >
							<table id="table">
								<tr id="nameSearch">
									<td style="color: #0474AF">제품명</td>
									<td><div class="ui input">
											<input type="text" style="width: 100px; height: 28px;"
												id="pdtName" name="pdtName">
										</div></td>
									<td>
										<!-- <button id="submit_Btn" onclick="killdh()">조회</button> -->
										<input type="button" value="조회" id="submit_Btn" class="ui grey basic button srch searchBtn" style="margin-top:10px;">
									</td>
								</tr>





							</table>
						</form>
							

					</div>

					<form action="update.pr" method="post" id="updateForm">
						<div id="contentDiv2">
							<h3 style="margin-left: 5px; margin-top: 10px;">상세</h3>

							<table id="table2">
								<tr>
									<td>제품코드</td>
									<td><div class="ui input">
											<input type="text" style="width: 150px; height: 32px;"
												id="pdtCode2" name="pdtCode">
										</div></td>
									<td>제품명</td>
									<td><div class="ui input">
											<input type="text" style="width: 150px; height: 32px;"
												id="pdtName2" name="pdtName">
										</div></td>
								</tr>

								<tr>
									<td>출시일</td>
									<td><div class="ui input">
											<input type="text" style="width: 150px; height: 32px;"
												id="Date">
										</div></td>
									<td>단가</td>
									<td><div class="ui input">
											<input type="text" style="width: 150px; height: 32px;"
												id="price" name="price">
										</div></td>
								</tr>

								<tr>
									<td>포장수량</td>
									<td><div class="ui input">
											<input type="text" style="width: 150px; height: 32px;"
												id="quantity" name="quantity">
										</div></td>
									<td>포장단위</td>
									<td><select class="ui dropdown"
										style="width: 150px; height: 35px;" id="unit" name="unit">
											<option value="BOX">BOX</option>
											<option value="EA">EA</option>

											<option value="SET">SET</option>

									</select></td>
								</tr>

								<tr>
									<td>규격</td>
									<td><select class="ui dropdown"
										style="width: 150px; height: 35px;" id="standard"
										name="standard">
											<option value="EA">EA</option>
											<option value="PCS">PCS</option>
											<option value="QTY">QTY</option>
											<option value="SET">SET</option>

									</select></td>




									<td>등록자</td>
									<td><div class="ui input">
									<input type="text" style="width: 150px; height: 32px;"id="empName" name="empName" readonly>
									
											<input type="hidden" style="width: 150px; height: 32px;"id="empNo" name="empNo">
										</div></td>


								</tr>



								<tr>
									<td>제품설명</td>
									<td colspan="3"><div class="ui left corner labeled input">

											<textarea style="width: 400px; height: 70px;" id="info"
												name="info"></textarea>
										</div></td>
										<td><input type="hidden" id="pdtId2" name="pdtId"></td>
										<td><input type="hidden" name="modifyer" value="${sessionScope.loginEmp.empNo}"></td>
										

								</tr>

							</table>


						</div>

					<div id="contentDiv2_1">

  <input type="button" id="modify_Btn" value="수정" class="ui grey basic button srch searchBtn" style="width:70px;">
 <input type="button" id="submit_Btn2" value="변경"  class="ui grey basic button srch searchBtn" style=" display:none; ">
 <input type="button" id="cancle_Btn" value="삭제" class="ui grey basic button srch searchBtn" style="width:70px;">
 

</div>
						
					</form>
					
					

<form action="delete.pr" method="post" id="deleteForm">
<input type="hidden" id="pdtId3" name="pdtId">

</form>

					<div id="contentDiv2_2">
						<h3 style="margin-left: 5px; margin-top: 10px;">이력 [<label id="count"></label>]건</h3>
						<table id="table3">

							<tr>
								<th>이력발생일</th>
								
								<th>변경자</th>
								<th>단가</th>
								<th>이력상태</th>
							

							</tr>

							


						</table>
					</div>

				</div>


			</div>


		</div>




	</div>
	</main>
	</div>
	</div>
	</div>
	<script>
		$(function() {
			
			 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
		        
		        // 열리는 메뉴
		        $(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
		        $(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(5).addClass("on");

			

			$("#submit_Btn2").click(function() {

				
				$("#updateForm").submit();

			})

			//제품등록 버튼
			$("#productAdd_Btn").click(function() {

				$('.ui.modal.adminProductEnroll').modal({
					onShow : function(value){
						$('#standard_calendar4').calendar({
							type: 'date',
							formatter : {
							date: function(date, settings){
								 if (!date) return '';
							        var day = date.getDate();
							        var month = date.getMonth() + 1;
							        var year = date.getFullYear();
							        return year + '-' + month + '-' + day;
								}
							}
						}
						)
						}}).modal('show');

			})
			//제품등록 팝업 끄기
			$(".exit_Btn").click(function() {
				$('.ui.modal.adminProductEnroll').modal('hide');
			})
			
			$("#submit_Btn").click(function() {
				$("#count").html("");
				$("#submit_Btn2").attr('style', "display:none; ");
				$("#modify_Btn").attr('style', "display:inline-block; width:70px;");
				
				$("#pdtName2").val("");
				$("#pdtCode2").val("");
				$("#Date").val("");
				$("#price").val("");
				$("#quantity").val("");
				$("#unit").val("");
				$("#standard").val("");
				$("#empNo").val("");
				$("#info").val("");

							$("td.SEARCH").parent().remove();

							var pdtName = $("#pdtName").val();

							console.log(pdtName);

							$.ajax({
								url : "searchProName.pr",
								type : "post",
								data : {
											pdtName : pdtName
										},
										success : function(data) {

											var result = data.list;

											var str = "<TR CLASS='TR'>";

											$.each(result,function(i) {

																str += "<TD CLASS='SEARCH' COLSPAN='3'>"
																		+ result[i].pdtName
																		+ "</TD><TD CLASS='HIDDENNO'><INPUT TYPE='HIDDEN' VALUE='" + result[i].pdtId + "'></TD>";
																str += "</TR>";

															})
											$("#table").append(str);

											$("td.SEARCH").click(
													function() {
														
														$("#count").html("");
														
														var strth = "<tr><th>이력발생일</th> <th>변경자</th> <th>단가</th><th>이력상태</th></tr>";
										        		
										        		
										        		$("#table3").html("");
										        		$("#table3").append(strth);
																
																$("#modify_Btn").click(function(){
																	
																	$("#pdtCode2").attr('disabled',false);
																	$("#pdtName2").attr('disabled',false);
																	$("#price").attr('disabled',false);
																	$("#quantity").attr('disabled',false);
																	$("#unit").attr('disabled',false);
																	$("#standard").attr('disabled',false);
																	$("#empNo").attr('disabled',false);
																	$("#info").attr('disabled',false);
																	$("#Date")	.attr('disabled',false);
																	$("#submit_Btn2").attr('style', "display:inline-block;width:70px;");
																	$("#modify_Btn").attr('style', "display:none; ");
											
											
											
										})

																var pdtId = $(this).parent().children().children("input[type='hidden']").val();

																$.ajax({

																			url : "searchPro.pr",
																			type : "post",
																			data : {
																				pdtId : pdtId
																			},
																			success : function(
																					data) {
																				
																				

																				$("#pdtName2").val(data.productAjax.pdtName);
																				$("#pdtCode2").val(data.productAjax.pdtCode);
																				$("#Date").val(data.productAjax.date);
																				$("#price").val(data.productAjax.price);
																				$("#quantity").val(data.productAjax.quantity);
																				$("#pdtId2").val(data.productAjax.pdtId);

																				$("#pdtId3").val(data.productAjax.pdtId);
																				
																				if (data.productAjax.unit === "BOX") {
																					$("#unit").val("BOX");
																				} else if (data.productAjax.unit === "EA") {
																					$("#unit").val("EA");
																				} else if (data.productAjax.unit === "SET") {
																					$("#unit").val("SET");
																				}

																				if (data.productAjax.standard == "EA") {
																					$("#standard")	.val("EA");
																				} else if (data.productAjax.standard == "PCS") {
																					$("#standard").val("PCS");
																				} else if (data.productAjax.standard == "QTY") {
																					$("#standard").val("QTY");
																				} else if (data.productAjax.standard == "SET") {
																					$("#standard").val("SET");
																				}

																				$("#empName").val(data.productAjax.employee.empName);
																				
																				
																				$("#empNo").val(data.productAjax.employee.empNo);

																				$("#info").val(data.productAjax.info);

																				$("#pdtName2").attr('disabled',true);
																				$("#pdtCode2").attr('disabled',true);
																				$("#Date")	.attr('disabled',true);
																				$("#price").attr('disabled',true);
																				$("#quantity").attr('disabled',true);
																				$("#unit").attr('disabled',true);
																				$("#standard").attr('disabled',true);
																				$("#empNo").attr('disabled',true);
																				$("#info").attr('disabled',true);

																				
																				var pdtId = data.productAjax.pdtId;
																				
																				$.ajax({
																					
																					url:"selectProHis.pr",
																					type:"post",
																					data:{
																						pdtId:pdtId
																						
																					},
																					success:function(data) {
																						
																				
																						
																						$("#count").html(data.integer);
																						
																						var result = data.productHistoryList;
																						
																						console.log(result);
																						
																						var str = "<TR>";
															                     		var choose = "";
															                            $.each(result, function(i){
															                            	
															                            	
															                            	if(result[i].hisStatus=="MOD") {
															                             	    choose = "<TD>수정</TD>";
															                                }else if(result[i].hisStatus=="CRE") {
															                                	 choose = "<TD>등록</TD>";
															                                }else if(result[i].hisStatus=="DEL") {
															                                	 choose ="<TD>삭제</TD>";
															                                };
															                           
															                            
															                               str += "<TD CLASS='SEARCH' >" + result[i].hisDate + "</TD><TD>" +result[i].employee2.empName + "</TD><TD>" +result[i].price+"</TD>"  + choose
															                               
															                               str += "</TR>";
															                               
															                               
															                            })
															                            $("#table3").append(str); 
																						
																						
																						
																					},
																					error:function(status) {
																						
																					}
																					
																					
																				})
																				
																				
																				
																				
																				
																				
																				
																				
																				
																				
																			},
																			error : function(
																					status) {

																			}

																		})

															})

										},
										error : function(status) {
											console.log(status);
										}

									})

						})

		})

		
						
						
					
	$("#cancle_Btn").click(function(){
		
		var con_test = confirm("해당 제품을 정말로 삭제하시겠습니까?");
		if(con_test == true){
		
			$("#deleteForm").submit();
		}
		else if(con_test == false){
		  
		}
		
		
		
		
	})
						
						
						
	</script>



</body>
</html>