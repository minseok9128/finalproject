<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
</head>
<style>

.con {border: none !important; padding-left: 10px;}
</style>
<jsp:include page="../../common/adminMenubar.jsp" />
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title">
				<div class="tb_title">
					<i class="home icon"></i>영업현황 > 월별고객접점현황
				</div>
			</div>
			<div id="admin_search_div">
				<h5>년</h5>
				<select>
					<option>2020</option>
				</select>
				<button class="submit_Btn">조회</button>
			</div>
			<div id="contentDiv">
				<div>
					<h3 style="margin-left: 5px; margin-top: 5px; margin-bottom: 5px;">담당자</h3>
				</div>
				<div>
					<p>담당자명</p>
					<input type="text"><input type="button" value="조회">
				</div>
				<div class="tree_box" id="tree_box">
					<div class="con" id="con">
						<ul id="tree_menu" class="tree_menu">
							<li class="depth_1">
								<i class="building icon"></i>(주)KH전자
								<ul class="depth_2">
									<li><a href="#none"><em>폴더</em>관리자</a>
										<ul class="depth_3">
											<li><a href="#none">서범수</a></li>
										</ul></li>
									<li class="last"><a href="#none"><em>폴더</em>영업부</a>
										<ul class="depth_3">
											<li><a href="#none">김민석</a></li>
											<li><a href="#none">박재영</a></li>
											<li><a href="#none">문호승</a></li>
											<li><a href="#none">황규환</a></li>
											<li class="end"><a href="#none">안동환</a></li>
										</ul></li>
								</ul></li>
						</ul>
					</div>
				</div>
			</div>
			<div id="contentDiv2">
				<div>
					<h3 style="margin-left: 5px; margin-top: 5px; margin-bottom: 5px;">담당자</h3>
				</div>
				<div>
					<p>영업활동</p>
					<button class="reset_Btn">초기화</button>
					<button class="exel_Btn">엑셀</button>
				</div>
				<div>
				<table>
				<tr>
					<th>활동분류</th>
					<th>합계</th>
					<th>01</th>
					<th>02</th>
					<th>03</th>
					<th>04</th>
					<th>05</th>
					<th>06</th>
					<th>07</th>
					<th>08</th>
					<th>09</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
				</tr>
				<tr>
					<td>잠재고객 (합계)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>잠재고객 (신규)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>고객사 (합계)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>고객사 (신규)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>고객 (합계)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>고객 (신규)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>영업기회 (신규)</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>영업활동</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>견적</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
				<tr>
					<td>계약</td>
					<td>합계</td>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
				</tr>
			</table>
			</div>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	<script>
		$(function() {
			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(3)
					.addClass("on");

			/* 다른 페이지로 이동하는 함수 */
			$('#enrollment_estimate_Btn').click(function() {
				location.href = "showEstimateEnrollment.sm";
			})
			$('.resultContents').click(function() {
				location.href = "showEstimateDetail.sm";
			})
			/* ====================================================== */
			/* 트리메뉴 함수 */
			function tree_menu() {
				$('ul.depth_2 >li > a').click(function(e) {
					var temp_el = $(this).next('ul');
					var depth_3 = $('.depth_3');
					depth_3.slideUp(300);
					depth_3.parent().find('em').removeClass('on');

					if (temp_el.is(':hidden')) {
						temp_el.slideDown(300);

						$(this).find('em').addClass('on').html('하위 폴더 열림');
					} else {
						temp_el.slideUp(300);
						$(this).find('em').removeClass('on').html('하위폴더 닫힘');
					}
					return false;
				});
			}
			if ($('#tree_menu').is(':visible')) {
				tree_menu();
			}
			/* =====================================================- */
			
			
		});
	</script>
</body>
</html>