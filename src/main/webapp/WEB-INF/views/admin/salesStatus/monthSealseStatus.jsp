<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
	
</head>
<jsp:include page="../../common/adminMenubar.jsp" />
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title">
				<div class="tb_title">
					<i class="home icon"></i>영업현황 > 월별매출현황
				</div>
			</div>
			<div id="admin_search_div">
				<h5>년</h5>
				<div class="ui calendar" id="standard_calendar">
					<div class="ui input right icon" id="monthSealse_calendar">
						<i class="calendar icon"></i> <input type="text"
							placeholder="오늘 날짜">
					</div>
					<div>
						<h5>제품명</h5>
						<input type="text">
					</div>
					<div>
						<h5>담당자</h5>
						<input type="text">
					</div>
				</div>
					<button class="submit_Btn">조회</button>
			</div>
			<div id="monthSalse_contant_div">
				<canvas id="canvas" width="800" height="400"></canvas>
				<div>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<div id="table2_div">
					<table class="table2">
						<tr>
							<th>이름</th>
							<th>목표(년)</th>
							<th>달성(년)</th>
							<c:forEach var="i" begin="1" end="12">
								<th>${ i } 목표</th>
								<th>${ i } 실적</th>
								<th>${ i } 달성(%)</th>
							</c:forEach>
						</tr>
						<tr>
							<td>김민석</td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							<c:forEach var="i" begin="1" end="12">
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							</c:forEach>
						</tr>
						<tr>
							<td>서범수</td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							<c:forEach var="i" begin="1" end="12">
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							</c:forEach>
						</tr>
						<tr>
							<td>합계</td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							<c:forEach var="i" begin="1" end="12">
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							</c:forEach>
						</tr>
						<tr>
							<td>누계</td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							<c:forEach var="i" begin="1" end="12">
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
								<td><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></td>
							</c:forEach>
						</tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr>
							<th>Total</th>
							<th><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></th>
								<th><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></th>
							<c:forEach var="i" begin="1" end="12">
								<th><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></th>
								<th><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></th>
								<th><c:out value="<%= Math.round((Math.random()*10000)) %>"></c:out></th>
							</c:forEach>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

	</main>
	</div>
	</div>
	</div>
	<script>
		$(function() {
			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(5)
					.addClass("on");

			/* 다른 페이지로 이동하는 함수 */
			$('#enrollment_estimate_Btn').click(function() {
				location.href = "showEstimateEnrollment.sm";
			})
			$('.resultContents').click(function() {
				location.href = "showEstimateDetail.sm";
			})
			/* ====================================================== */
			'use strict';

			window.chartColors = {
				red: 'rgb(255, 99, 132)',
				orange: 'rgb(255, 159, 64)',
				yellow: 'rgb(255, 205, 86)',
				green: 'rgb(75, 192, 192)',
				blue: 'rgb(54, 162, 235)',
				purple: 'rgb(153, 102, 255)',
				black: 'rgb(67, 67, 72)',
				bgreen: 'rgb(147, 235, 130)',
				grey: 'rgb(201, 203, 207)'
			};

			(function(global) {
				var MONTHS = [
					'January',
					'February',
					'March',
					'April',
					'May',
					'June',
					'July',
					'August',
					'September',
					'October',
					'November',
					'December'
				];

				var COLORS = [
					'#4dc9f6',
					'#f67019',
					'#f53794',
					'#537bc4',
					'#acc236',
					'#166a8f',
					'#00a950',
					'#58595b',
					'#8549ba'
				];

				var Samples = global.Samples || (global.Samples = {});
				var Color = global.Color;

				Samples.utils = {
					// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
					srand: function(seed) {
						this._seed = seed;
					},

					rand: function(min, max) {
						var seed = this._seed;
						min = min === undefined ? 0 : min;
						max = max === undefined ? 1 : max;
						this._seed = (seed * 9301 + 49297) % 233280;
						return min + (this._seed / 233280) * (max - min);
					},

					numbers: function(config) {
						var cfg = config || {};
						var min = cfg.min || 0;
						var max = cfg.max || 1;
						var from = cfg.from || [];
						var count = cfg.count || 8;
						var decimals = cfg.decimals || 8;
						var continuity = cfg.continuity || 1;
						var dfactor = Math.pow(10, decimals) || 0;
						var data = [];
						var i, value;

						for (i = 0; i < count; ++i) {
							value = (from[i] || 0) + this.rand(min, max);
							if (this.rand() <= continuity) {
								data.push(Math.round(dfactor * value) / dfactor);
							} else {
								data.push(null);
							}
						}

						return data;
					},

					labels: function(config) {
						var cfg = config || {};
						var min = cfg.min || 0;
						var max = cfg.max || 100;
						var count = cfg.count || 8;
						var step = (max - min) / count;
						var decimals = cfg.decimals || 8;
						var dfactor = Math.pow(10, decimals) || 0;
						var prefix = cfg.prefix || '';
						var values = [];
						var i;

						for (i = min; i < max; i += step) {
							values.push(prefix + Math.round(dfactor * i) / dfactor);
						}

						return values;
					},

					months: function(config) {
						var cfg = config || {};
						var count = cfg.count || 12;
						var section = cfg.section;
						var values = [];
						var i, value;

						for (i = 0; i < count; ++i) {
							value = MONTHS[Math.ceil(i) % 12];
							values.push(value.substring(0, section));
						}

						return values;
					},

					color: function(index) {
						return COLORS[index % COLORS.length];
					},

					transparentize: function(color, opacity) {
						var alpha = opacity === undefined ? 0.5 : 1 - opacity;
						return Color(color).alpha(alpha).rgbString();
					}
				};

				// DEPRECATED
				window.randomScalingFactor = function() {
				return Math.round(Samples.utils.rand(0, 100));
			};

				// INITIALIZATION

				Samples.utils.srand(Date.now());

				// Google Analytics
				/* eslint-disable */
				if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
					ga('create', 'UA-28909194-3', 'auto');
					ga('send', 'pageview');
				}
				/* eslint-enable */

			}(this));
			
			/* =========================================================================== */
			$('#year_calendar')
			  .calendar({
			    type: 'year'
			  })
			;
			
			var chartData = {
					labels: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
					datasets: [{
						type: 'line',
						label: '달성',
						borderColor: window.chartColors.bgreen,
						borderWidth: 2,
						fill: false,
						data: [
							10,
							20,
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor()
						]
					}, {
						type: 'bar',
						label: '목표',
						backgroundColor: window.chartColors.blue,
						data: [
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor()
						],
						borderColor: 'white',
						borderWidth: 2
					}, {
						type: 'bar',
						label: '실적',
						backgroundColor: window.chartColors.black,
						data: [
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor()
						]
					}]

				};
				window.onload = function() {
					var ctx = document.getElementById('canvas').getContext('2d');
					window.myMixedChart = new Chart(ctx, {
						type: 'bar',
						data: chartData,
						options: {
							responsive: false,
							title: {
								display: true
							},
							tooltips: {
								mode: 'index',
								intersect: true
							}
						}
					});
				};
			
		});
		
	</script>
</body>
</html>