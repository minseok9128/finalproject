<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
.calenderPeriod {
	margin-top: auto;
	margin-bottom: auto;
	margin-left: 5px;
	margin-right: 5px;
}

.calenderWidth {
	width: 400px;
	display: inline-block;
	margin-top: 20px;
}

#select {
	width: 200px;
	display: inline-block;
}

.subPTitle {
	margin-bottom: 0;
	display: inline-block;
	margin-right: 10px;
	margin-left: 15px;
	margin-top: 20px;
}

#divColor {
	background-color: #FBFCFD;
	border: 1px solid #E1E6EB;
}

#inLine {
	display: inline-block;
}

.contentHead {
	background-color: #FBFCFD;
	font-weight: bold;
	font-size: 15px;
	vertical-align: middle;
	border-bottom: 1px solid #27AAE1;
}

#btnRight {
	float: right;
	margin-top: 10px;
	vertical-align: middle;
}

#subHeadTitle {
	display: inline-block;
	margin-top: 15px;
	margin-left: 10px;
}

.th {
	border-bottom: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	border-right: 1px solid #DDDDDD;
	height: 30px;
	vertical-align: middle;
	color: #00A1DB;
}

.titleCount {
	color: #E34B55;
	display: inline-block;
	font-size: 14px;
}

.odd>.naltd {
	font-size: 13px;
	text-align: center;
	vertical-align: middle;
	height: 30px;
}

.odd>.linektd {
	font-size: 13px;
	text-align: center;
	color: #2385B9;
	vertical-align: middle;
	text-decoration: underline;
	height: 30px;
}

.even>.naltd {
	font-size: 13px;
	text-align: center;
	background-color: #FBFCFD;
	vertical-align: middle;
	height: 30px;
}

.even>.linektd {
	font-size: 13px;
	text-align: center;
	color: #2385B9;
	background-color: #FBFCFD;
	vertical-align: middle;
	height: 30px;
	text-decoration: underline;
	cursor: pointer;
}

.recordTArea {
	margin-top: 20px;
	width: 370px;
	height: 220px;
	overflow: auto;
	display: inline-block;
	border: 1px solid #D7DBDF;
}

.clientTArea {
	margin-top: 20px;
	margin-right: 15px;
	width: 370px;
	height: 220px;
	overflow: auto;
	display: inline-block;
	border: 1px solid #D7DBDF;
}
.table{
 	width:170%;
 	height:150%;
 }
.clientIn {
	background-color: #CCCCCC;
	vertical-align: middle;
	height: 30px;
	width: 240px;
}

#inColor {
	background-color: #CCCCCC;
}

.reportTh {
	height: 40px;
	vertical-align: middle;
	color: #00A1DB;
}

.reportTd {
	vertical-align: middle;
	height: 30px;
	text-align: center;
}
</style>
</head>
<body>
	<jsp:include page="../../common/adminMenubar.jsp" />
	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>영업현황 > 기간별영업활동
				</div>

				<div id="divColor">
					<div class="searchArea" id="inLine">
						<p class="subPTitle">기간</p>
						<div class="ui form calenderWidth">
							<div class="two fields">
								<div class="field">
									<div class="ui calendar" id="rangestart">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="2019.01.01">
										</div>
									</div>
								</div>
								<label class="calenderPeriod">~</label>
								<div class="field">
									<div class="ui calendar" id="rangeend">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="2019.12.01">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div style="width: 38%;" id="inLine">
						<p class="subPTitle">담당자</p>
						<select class="ui dropdown" id="select">
							<option value="">담당자명</option>
							<option value="1">서범수</option>
							<option value="0">김영수</option>
						</select>
					</div>

					<div id="inLine" style="margin-left: 15px;">
						<p class="subPTitle" style="margin-left: 0;">고객</p>
						<select class="ui dropdown" id="select">
							<option value="">고객</option>
							<option value="1">안동환</option>
							<option value="0">김민석</option>
						</select>
					</div>

					<div id="inLine" style="margin-left: 15px;">
						<p class="subPTitle">고객사</p>
						<select class="ui dropdown" id="select">
							<option value="">고객사</option>
							<option value="1">orangered</option>
							<option value="0">yellowgreen</option>
						</select>
					</div>
					<button class="medium ui inverted blue button right"
						style="margin-bottom: 20px; margin-left: 110px;">검색</button>

				</div>

				<div style="border: 1px solid #E1E6EB; margin-top: 30px;">
					<div class="contentHead">
						<p id="subHeadTitle">영업활동</p>
						<p class="titleCount">
							[<c:out value="36" />]건
						</p>
						<button id="btnRight" class="ui inverted green button">엑셀</button>
						<button id="btnRight" class="ui inverted red button">초기화</button>
					</div>
					<div style="overflow: auto; height:400px; width:760px;">
						<table class="table" style="border-bottom:1px solid #DFDBDF;">
								<tr>
									<th class="th" style="width: 100px;">담당자</th>
									<th class="th" style="width: 100px;">부서</th>
									<th class="th" style="width: 150px;">날짜</th>
									<th class="th" style="width: 200px;">시간</th>
									<th class="th" style="width: 80px;">활동</th>
									<th class="th" style="width: 80px;">활동분류</th>
									<th class="th" style="width: 100px;">고객사</th>
									<th class="th" style="width: 100px;">고객</th>
									<th class="th" style="width: 200px;">영업기회</th>
									<th class="th" style="width: 200px;">활동내용</th>
									<th class="th" style="width: 80px;">등록자</th>
									<th class="th" style="width: 150px;">등록일</th>
								</tr>
							<c:forEach var="i" begin="0"  end="18">
								<tr class="odd">
									<td class="naltd"><c:out value="김영수" /></td>
									<td class="naltd"><c:out value="영업1팀" /></td>
									<td class="linektd"><c:out value="2020.01.18" /></td>
									<td class="linektd"><c:out value="14:00~14:30" /></td>
									<td class="naltd"><c:out value="Y" /></td>
									<td class="naltd"><c:out value="전화" /></td>
									<td class="linektd"><c:out value="핑거포스트" /></td>
									<td class="linektd"><c:out value="홍길동" /></td>
									<td class="linektd"><c:out value="핑거포스트 서비스 운영 개발" /></td>
									<td class="naltd"><c:out value="미팅 일정 수립" /></td>
									<td class="naltd"><c:out value="김영수" /></td>
									<td class="naltd"><c:out value="2020.01.17" /></td>
								</tr>
								<tr class="even">
									<td class="naltd"><c:out value="김영수" /></td>
									<td class="naltd"><c:out value="영업1팀" /></td>
									<td class="linektd"><c:out value="2020.01.18" /></td>
									<td class="linektd"><c:out value="14:00~14:30" /></td>
									<td class="naltd"><c:out value="Y" /></td>
									<td class="naltd"><c:out value="전화" /></td>
									<td class="linektd"><c:out value="핑거포스트" /></td>
									<td class="linektd"><c:out value="홍길동" /></td>
									<td class="linektd"><c:out value="핑거포스트 서비스 운영 개발" /></td>
									<td class="naltd"><c:out value="미팅 일정 수립" /></td>
									<td class="naltd"><c:out value="김영수" /></td>
									<td class="naltd"><c:out value="2020.01.17" /></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
				<div class="clientTArea">
					<div class="contentHead">
						<p id="subHeadTitle"
							style="margin-top: 15px; margin-bottom: 15px;">고객</p>
					</div>
					<table class="clien">
						<tr>
							<th class="reportTh">고객사</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="핑거포스트" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th class="reportTh">고객</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="홍길동" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th class="reportTh">부서</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="경영팀" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th class="reportTh">직책</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="대리" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th class="reportTh">메일</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="inger@finger-post.com" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th class="reportTh">휴대폰</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="010-1234-0000" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th class="reportTh">유선번호</th>
							<td style="vertical-align: middle;">
								<div class="ui input">
									<input type="text" class="clientIn"
										style="margin-left: 15px; background-color: #CCCCCC;"
										value="070-1234-0000" readonly>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="recordTArea">
					<div class="contentHead">
						<p id="subHeadTitle"
							style="margin-top: 15px; margin-bottom: 15px;">이력</p>
					</div>
					<table class="recordTable">
						<tr style="border-bottom: 1px solid #D7DBDF;">
							<th class="reportTh"
								style="border-right: 1px solid #D7DBDF; width: 100px;">날짜</th>
							<th class="reportTh"
								style="border-right: 1px solid #D7DBDF; border-left: 1px solid #D7DBDF; width: 85px;">담당자</th>
							<th class="reportTh"
								style="border-left: 1px solid #D7DBDF; width: 180px;">활동내역</th>
						</tr>
						<tr style="border-bottom:1px solid #D7DBDF;">
							<td class="reportTd">2020.01.23</td>
							<td class="reportTd">김영수</td>
							<td class="reportTd">업무 미팅 제안</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function() {
			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(0).addClass("on");

			$('#rangestart').calendar({
				type : 'date',
				endCalendar : $('#rangeend')
			});
			$('#rangeend').calendar({
				type : 'date',
				startCalendar : $('#rangestart')
			});
		});
	</script>
</body>
</html>