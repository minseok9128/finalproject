<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.calenderPeriod {
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 5px;
		margin-right: 5px;
	}
	.calenderWidth {
		width: 400px;
		display: inline-block;
		margin-top: 10px;
	}
	.subPTitle {
		margin-bottom: 0;
		display: inline-block;
		margin-right: 10px;
		margin-left: 10px;
		margin-top: 20px;
	}
	#divColor {
		background-color: #FBFCFD;
		border: 1px solid #E1E6EB;
	}
	#inLine {
		display: inline-block;
	}
	.title{
		display:inline-block;
		vertical-align:middle;
		margin-bottom:10px;
		margin-left:10px;
		font-size:15px;
		margin-top:10px;
	}
	.titleT {
		display:inline-block;
		color:#E34B55;
		vertical-align:middle;
		margin-top:0;
		margin-bottom:0;
		font-size:15px;
	}
	.btn_box{
 		float:right;
 		display:inline-block;
 	}
	.table_border{
		margin-top:30px;
 		border:1px solid #D7DBDF;
 		border-radius:.28571429rem;
 		z-index:1;
 	}
	.table_head{
 		background:#FBFCFD;
 		margin-top:10px;
 		border-bottom:1px solid #27AAE1;
 		
 	}
	.table_content{
 		overflow:auto;
 	}
	table{
 		width:150%;
 		height:150%;
 	}
 	th ,td{
 		text-align:center;
 		vertical-align:middle !important;
 		height:30px;
 	}
 	th{
 		color:#27AAE1 !important;
 		
 	}
 	.table_content tr *{
 		margin-left:10px;
 	}
 	tr:nth-child(even){
 		background:#F9F9F9;
 	}
 	.line{
 		text-decoration:underline;
 		cursor:pointer;
 		color:#27AAE1;
 	}
</style>
</head>
<body>
	<jsp:include page="../../common/adminMenubar.jsp" />
	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>영업현황 > 제품별영업활동
				</div>

				<div id="divColor">
					<div class="searchArea" id="inLine">
						<p class="subPTitle">기간</p>
						<div class="ui form calenderWidth">
							<div class="two fields">
								<div class="field">
									<div class="ui calendar" id="rangestart">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="2019.01.01">
										</div>
									</div>
								</div>
								<label class="calenderPeriod">~</label>
								<div class="field">
									<div class="ui calendar" id="rangeend">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="2019.12.01">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="inLine">
						<p class="subPTitle">제품명</p>
						<select class="ui dropdown" id="select">
							<option value="">제품명</option>
							<option value="1">아이패드</option>
							<option value="0">에어팟</option>
						</select>
					</div>

					<button class="medium ui inverted blue button" style="margin-left: 10px;">검색</button>
				</div>
				
				<div class="table_border">
					<div class="table_head">
						<h3 class="title">영업활동</h3><h3 class="titleT">[<c:out value="10"/>]건</h3>
						<div class="btn_box">
							<button class="ui green basic button">엑셀</button>
							<button class="ui red basic button">초기화</button>
						</div>
					</div>
					<div class="table_content">
						<table>
							<tr>	
								<th>제품구분</th>
								<th>제품명</th>
								<th>매출목표(만원)</th>
								<th>매출 실적(년)</th>
								<th>영업기회</th>
								<th>영업활동</th>
								<th>견적</th>
								<th>계약</th>
							</tr>
							<c:forEach var="i" begin="0"  end="10">
								<tr>
									<td><c:out value="제품"/></td>
									<td><c:out value="PUERTA"/></td>
									<td><c:out value="9,996.00"/></td>
									<td><c:out value="0.00"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<script>
		$(function(){
			$(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(1).addClass("on");
		
			$('#rangestart').calendar({
				type : 'date',
				endCalendar : $('#rangeend')
			});
			$('#rangeend').calendar({
				type : 'date',
				startCalendar : $('#rangestart')
			});
		 });
		</script>
</body>
</html>