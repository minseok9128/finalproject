<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#divColor {
		background-color: #FBFCFD;
		border: 1px solid #E1E6EB;
	}
	#inLine, .inLine {
		display: inline-block;
	}
	.subPTitle {
		margin-top:10px;
		margin-bottom:10px;
		display: inline-block;
		margin-right: 10px;
		font-size:15px;
		font-weight:bold;
		margin-left:10px;
	}
	.right {
		margin-right:15px;
		float:right;
		vertical-align:middle;
		margin-top:15px;
		margin-bottom:15px;
	}
	.searchArea {
		margin-top:10px;
		margin-bottom:10px;
	}
	.btn_box{
 		float:right;
 		display:inline-block;
 	}
	.table_border{
 		border:1px solid #D7DBDF;
 		border-radius:.28571429rem;
 		width:510px;
 	}
	.table_head{
 		background:#FBFCFD;
 		border-bottom:1px solid #27AAE1;
 		margin-top:10px;
 		
 	}
	.table_content{
 		overflow:auto;
 	}
	table{
 		width:150%;
 		height:150%;
 	}
 	th ,td{
 		text-align:center;
 		vertical-align:middle !important;
 		height:30px;
 	}
 	th{
 		color:#27AAE1 !important;
 		
 	}
 	.table_content tr *{
 		margin-left:10px;
 	}
 	tr:nth-child(even){
 		background:#F9F9F9;
 		border-collapse:seperate;
 	}
 	.line{
 		text-decoration:underline;
 		cursor:pointer;
 		color:#27AAE1;
 	}
 	.title{
		display:inline-block;
		vertical-align:middle;
		margin-bottom:10px;
		margin-left:10px;
		font-size:15px;
		margin-top:10px;
	}
	#contentDiv {
		border: 1px solid #E5E5E5;
		width: 230px;
		display: inline-block;
		height: 665px;
		margin-right: 10px;
		float: left;
	}
	#table {
		width: 100%;
		border-top: 3px solid #27AAE1;
		text-align: center;
	}
	
	#table td {
		vertical-align: middle;
		border-bottom: 1px solid #E5E5E5;
		height: 40px;
	}
	
	#table tr:nth-child(1) {
		height: 60px;
		border-bottom: 2px solid #E5E5E5;
	}
	.category {
		font-size:13px;
		font-weight:bold;
		display:inline-block;
	}
	#inLine {
		display:inline-block;
	}
	.divColor {
		border-top:2px solid #27AAE1;
	}
</style>
</head>
<body>
	<jsp:include page="../../common/adminMenubar.jsp" />
	
	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">

			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>영업현황 > 사원별영업활동현황
				</div>
				<div id="divColor">
					<div class="searchArea" id="inLine">
						<p class="subPTitle" id="inLine">년</p>
						<div class="ui calendar inLine" id="standard_calendar">
							<div class="ui input left icon">
						    	<i class="calendar icon"></i>
						    	<input type="text" placeholder="">
						  	</div>
						</div>
					</div>
					<div class="right">
						<button class="ui inverted primary button">조회</button>
					</div>
				</div>
				
				<div style="margin-top: 20px; margin-right:20px;">
					<div id="contentDiv">
						<h3 style="margin-left: 5px; margin-top: 10px;">담당자</h3>
						<div id="divColor">
							<div class="searchArea" id="inLine">
								<p class="subPTitle" style="margin-left:10px;" id="inLine">담당자</p>
								<div class="ui calendar inLine" id="standard_calendar">
									<div class="ui input left icon" id="inline">
								    	<input type="text" style="width: 90px; height: 28px;" placeholder="">
								  	</div>
								</div>
								<div style="display:inline-block; margin-left:10px;">
									<button class="mini ui inverted primary button">조회</button>
								</div>
							</div>
						</div>
						
						<!-- <div class="grid overflow">
							<div role="tree" class="jstree jstree-proton">
								<ul class="jstree-container-ul" role="group">
									<li role="treeitem" class="jstree-open">
										<i class="jstree-icon jstree-ocl" role="presentation"></i>
										<a class="jstree-anchor" href="#">
											<i class="jstree-icon jstree-themeicon jstree-themeicon-custom" role="presentation" style="background-image: url(&quot;../res/images/all/tree_ico_root.png&quot;); background-position: center center; background-size: auto;"></i>
											(주)orangered
										</a>
										
										<ul role="group" class="jstree-children">
											<li role="treeitem" id="fingeradmin" class="jstree-node jstree-leaf">
											
											</li>
										</ul>
									</li>
								</ul>
							</div>
						</div> -->
					</div>
				</div>
				
				<div class="table_border right" style="margin-top:0; margin-right:0; border-collapse:seperate;">
					<div class="table_head">
						<h3 class="title">영업활동</h3>
						<div class="btn_box">
							<button class="ui green basic button">엑셀</button>
							<button class="ui red basic button">초기화</button>
						</div>
					</div>
					<div class="table_content">
						<table>
							<tr>	
								<th>활동분류</th>
								<th>합계</th>
								<th>01</th>
								<th>02</th>
								<th>03</th>
								<th>04</th>
								<th>05</th>
								<th>06</th>
								<th>07</th>
								<th>08</th>
								<th>09</th>
								<th>10</th>
								<th>11</th>
								<th>12</th>
							</tr>
							<c:forEach var="i" begin="0"  end="5">
								<tr>
									<td colspan="2">
										<p class="category">이름:</p><c:out value="김영수(4)"/>
									</td>
								</tr>
								<tr>
									<td><c:out value="전화"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
								</tr>
								<tr>
									<td><c:out value="메일"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
								</tr>
								<tr>
									<td><c:out value="방문"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
								</tr>
								<tr>
									<td><c:out value="기타"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
									<td><c:out value="0"/></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
	
	<script>
		$(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
		$(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(2).addClass("on");
		
		$('#standard_calendar').calendar({type: 'year'});
	</script>
</body>
</html>