<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
	<main id="container">
            <div class="inner_ct">
                <div class="menu">
                    <ul class="menu_cnt">
 						<li class="main_menu"><a href="60.go">영업기회</a></li>
                        <li class="main_menu"><a href="56.go">영업활동</a></li>
                        <li class="main_menu add"><a href="#">고객관리</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="48.go">고객</a></li>
                                <li class="sub_menu_list"><a href="49.go">고객사</a></li>
                                <li class="sub_menu_list"><a href="50.go">고객사/고객업로드</a></li>
                                <li class="sub_menu_list"><a href="51.go">담당자관리</a></li>
                                <li class="sub_menu_list"><a href="#">잠재고객</a></li>
                                <li class="sub_menu_list"><a href="#">잠재고객업로드</a></li>
                            </ul>
                        </li>
                        <li class="main_menu add"><a href="#">영업관리</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="58.go">견적</a></li>
                                <li class="sub_menu_list"><a href="57.go">계약</a></li>
                                <li class="sub_menu_list"><a href="#">매출조회</a></li>
                                <li class="sub_menu_list"><a href="#">매출업로드</a></li>
                                <li class="sub_menu_list"><a href="#">매출목표관리</a></li>
                                <li class="sub_menu_list"><a href="59.go">제품관리</a></li>
                            </ul>
                        </li>
                        <li class="main_menu add"><a href="#">영업현황</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="64.go">기간별영업활동</a></li>
                                <li class="sub_menu_list"><a href="65.go">제품별영업활동</a></li>
                                <li class="sub_menu_list"><a href="66.go">사원별영업활동현황</a></li>
                                <li class="sub_menu_list"><a href="62.go">월별고객접점현황</a></li>
                                <li class="sub_menu_list"><a href="61.go">월별매출현황</a></li>
                                <li class="sub_menu_list"><a href="63.go">월별영업현황</a></li>
                            </ul>
                        </li>
                        <li class="main_menu add"><a href="#">직원</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="55.go">직원관리</a></li>
                                <li class="sub_menu_list"><a href="53.go">직원등록</a></li>
                                <li class="sub_menu_list"><a href="showAdminEmpExcelEnroll.em">직원엑셀등록</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
                    

<!--     <script>
        $(function(){
            // 열리지 않는 메뉴
            //$(".menu_cnt .main_menu").eq(0).addClass("on");
            
            // 열리는 메뉴
            $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        });
    </script> -->
    <script>
	$(function(){
		$('.nav').children().removeClass('navClick');
		$('.nav').children().eq(1).addClass('navClick');
	});
    </script>
</body>
</html>