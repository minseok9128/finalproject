<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <!-- 공통 css-->
 <link rel="stylesheet" href="${ contextPath }/resources/css/common.css">
 <!-- 폰트 -->
 <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&display=swap&subset=korean" rel="stylesheet">
 <!-- jquery -->
 <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
 <!-- 공통 js -->
 <script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
 <!-- fomantic  -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.2/dist/semantic.min.css">
<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.2/dist/semantic.min.js"></script>
</head>
<body>
	<div class="wrap">
        <header id="header">
            <div class="inner_ct">
                <div class="logo"><a href="noticeMain.em"><img src="${ contextPath }/resources/images/logo.png"></a></div>
                <div class="nav">
                    <a href="noticeMain.em?code=1">sales</a>
                	<c:if test="${ sessionScope.loginEmp.empType eq '1' or sessionScope.loginEmp.empType eq '2' }"><a href="noticeMain.em?code=2">admin</a></c:if>
                	<c:if test="${ sessionScope.loginEmp.empType eq '1' or sessionScope.loginEmp.empType eq '2' }"><a href="showClientStatus.cs">status</a></c:if>                    
                </div>
                <div class="navRight">
                    <a href="#"><img src="${ contextPath }/resources/images/bell.png"></a>
                    <a href="logout.em"><img src="${ contextPath }/resources/images/logout.png"></a>
                </div>
                <div class="name"><c:if test="${ sessionScope.loginEmp.empType eq '3' or sessionScope.loginEmp.empType eq '2' }"><c:out value="${ sessionScope.loginEmp.deptName }"/></c:if> <a href="#"><c:out value="${ sessionScope.loginEmp.empName }"/></a></div>
            </div>
        </header>
</body>
<script>

</script>
</html>