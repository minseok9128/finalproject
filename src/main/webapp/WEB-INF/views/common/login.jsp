<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- 공통 css-->
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
    <!-- 폰트 -->
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&display=swap&subset=korean" rel="stylesheet">
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- 공통 js -->
    <script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
</head>
<body>
    <main id="container">
        <div class="loginMain">
            
            <div class="logoArea">
                <div class="logoImg"><img src="${ contextPath }/resources/images/mainLogo.png"></div>
                <div class="logoTxt"><img src="${ contextPath }/resources/images/mainSELLMORE.png"></div>
            </div>
            <div class="inputArea">
                <form action="login.em" class="loginForm" method="post">
                    <table class="loginTable">
                        <tr>
                            <th>ID</th>
                            <td><input class="input" type="text" name="empId"></td>
                        </tr>
                        <tr>
                            <th>PASSWORD</th>
                            <td><input class="input" type="password" name="empPwd"></td>
                        </tr>
                    </table>
                    <button class="loginBtn">로그인</button>
                </form>

            </div>
        </div>
    </main>
    <script>
    	$(function () {
    		$(".logoImg").click(function(){
    			location.href="${contextPath}/1.sa";
    		})	
    	})
    	var msg = '${msg}';
    	if(msg != ''){
    		alert(msg);
    	}
    </script>
</body>
</html>