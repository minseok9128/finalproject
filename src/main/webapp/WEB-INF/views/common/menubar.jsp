<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
	<main id="container">
            <div class="inner_ct">
                <div class="menu">
                    <ul class="menu_cnt">
                        <li class="main_menu"><a href="opportunityMain.so?empNo=${sessionScope.loginEmp.empNo}">영업기회</a></li>
                        <li class="main_menu"><a href="actCalendar.sa">영업활동</a></li>
                        <li class="main_menu add"><a href="#">고객관리</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="clientList.cl">고객</a></li>
                                <li class="sub_menu_list"><a href="selList.co">고객사</a></li>
                                <li class="sub_menu_list"><a href="selList.le">잠재고객</a></li>
                            </ul>
                        </li>
                        <li class="main_menu add"><a href="#">영업관리</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="offerMain.of?memberId=${sessionScope.loginEmp.empNo}">제안</a></li>
                                <li class="sub_menu_list"><a href="estimateMain.est?memberId=${sessionScope.loginEmp.empNo}">견적</a></li>
                                <li class="sub_menu_list"><a href="selList.con">계약</a></li>
                                <li class="sub_menu_list"><a href="selList.sal">매출</a></li>
                                <!-- <li class="sub_menu_list"><a href="45.go">매출업로드</a></li> -->
                                <li class="sub_menu_list"><a href="showGoalManagement.sgm">매출목표</a></li>
                                <!-- <li class="sub_menu_list"><a href="43.go">제품관리</a></li> -->
                               	 <li class="sub_menu_list"><a href="showSalesReport.sr">영업보고관리</a></li>
                                <c:if test="${sessionScope.loginEmp.empType != 3}">
                                	<li class="sub_menu_list"><a href="showSalesReportStatus.sr">영업보고관리현황</a></li>
                                </c:if>
                            </ul>
                        </li>
                        <li class="main_menu add"><a href="#">영업현황</a>
                            <ul class="sub_menu">
                                <li class="sub_menu_list"><a href="29.go">기간별영업활동</a></li>
                                <li class="sub_menu_list"><a href="30.go">제품별영업활동</a></li>
                                <li class="sub_menu_list"><a href="27.go">영업기회현황</a></li>
                                <li class="sub_menu_list"><a href="26.go">고객접점현황</a></li>
                                <li class="sub_menu_list"><a href="31.go">매출현황</a></li>
                                <li class="sub_menu_list"><a href="28.go">매출달성현황</a></li>
                            </ul>
                        </li>
                        <li class="main_menu"><a href="showNoticeList.sn">영업공지</a></li>
                    </ul>
                </div>
                
                    

<!--     <script>
        $(function(){
            // 열리지 않는 메뉴
            //$(".menu_cnt .main_menu").eq(0).addClass("on");
            
            // 열리는 메뉴
            $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        });
    </script> -->
    <script>
		$(function(){
			$('.nav').children().removeClass('navClick');
			$('.nav').children().eq(0).addClass('navClick');
		});
    </script>
</body>
</html>