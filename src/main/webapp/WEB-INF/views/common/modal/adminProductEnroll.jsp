<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/modal.css">
<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>

	<style>
	#file_table {
	width: 100%;
   
	
	
	}
	
	#table5 {
	width: 100%;
	border-top: 3px solid #27AAE1;
	text-align: center;
	border-collapse: separate;
	border-spacing: 0 7px;
}
	#productFile {
		width: 100%;
	}
	</style>
	
</head>
<body>


	<div class="ui modal adminProductEnroll" style="height: 720px;">
		<div class="content_Window" style="height: 650px;">
			<div class="header_Modal">
				<div>
					<h1>
						상품 등록<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div>

			<form action="insert.pr" method="post" enctype="multipart/form-data">
				<div class="contents_Modal">
					<div class="contents_Modal_Div" style="width: 100%; height: 310px;">

						<div style="border: 1px solid #E5E5E5;">

							<h3 style="margin-left: 10px; margin-top: 10px;">정보입력</h3>
							<table id="table5" style="border-collapse: separate; border-spacing: 0 16px;">
								<tr>
									<td>제품코드<b class="point">*</b></td>
									<td>
										<div style="position: relative;">
											<div class="ui input">
												<input type="text"
													style="width: 250px; height: 32px; margin-right: 95px;"
													name="pdtCode" id="pdtCode">

											</div>
											<div style="position: absolute; margin-left: 25px;">
												<label id="product-success" style="color: green;"><font
													size="1">사용가능한 제품코드입니다.</font></label> <label id="product-danger"
													style="color: red;"><font size="1">사용불가능한
														제품코드입니다.</font></label>
											</div>
										</div>
									</td>
									<td>제품명<b class="point">*</b></td>
									<td><div class="ui input">
											<input type="text" style="width: 250px; height: 32px;"
												name="pdtName">
										</div></td>
								</tr>

								<tr>
									<td>출시일</td>
									<td >

										
                        						<div class="ui calendar" id="date_calendar">
                          							<div class="ui input right icon" id="standard_calendar4" >
                            							<i class="calendar icon" style="margin-right: 95px;"></i>
                            						<input type="text"style="height: 32px;width:250px;margin-right: 95px; " name="date" readonly>
                          							</div>
                        						</div>
                      						
										
									</td>




									<td>단가<b class="point">*</b></td>
									<td><div class="ui input">
											<input type="text" style="width: 250px; height: 32px;"
												name="price">
										</div></td>
								</tr>

								<tr>
									<td>포장단위</td>
									<td><select class="ui dropdown" name="unit" id="po"
										style="width: 250px; height: 35px; margin-right: 95px;">
											<option value="BOX">BOX</option>
											<option value="EA">EA</option>

											<option value="SET">SET</option>

									</select></td>
									<td>포장수량</td>
									<td><div class="ui input">
											<input type="text" style="width: 250px; height: 32px;"
												name="quantity">
										</div></td>
								</tr>

								<tr>
									<td>규격</td>
									<td><select class="ui dropdown" name="standard" id="gu"
										style="width: 250px; height: 35px; margin-right: 95px;">
											<option value="pro1">EA</option>
											<option value="pro2">PCS</option>
											<option value="pro3">QTY</option>
											<option value="pro4">SET</option>

									</select></td>




									<td>등록자</td>
									<td><div class="ui input">
											<input type="text" style="width: 250px; height: 32px;"
												value="${sessionScope.loginEmp.empName }" readonly> <input
												type="hidden" style="width: 250px; height: 32px;"
												name="empNo" value="${sessionScope.loginEmp.empNo }">
										</div></td>


								</tr>



								<tr>
									<td>제품설명</td>
									<td colspan="3"><div class="ui left corner labeled input">

											<textarea style="width: 700px; height: 70px;" name="info"></textarea>
										</div></td>

								</tr>

							</table>
						</div>

					</div>
					<div class="contents_Modal_Div"
						style="width: 100%; height: 250px; margin-top: 50px;">
						<div class="attc_btn_div" style="border: 1px solid #E5E5E5; height: 200px;">
							
							<h3 style="margin-left: 10px; margin-top: 10px;margin-bottom: 20px; ">
								첨부파일
								<button type="button" class="ui inverted primary button"
									style="width: 100px; float: right; margin-bottom: 10px;"
									id="productFile_btn">업로드</button>
							</h3>
							
							<input type="file" id="productFile" hidden="true" width="500px"	name="productFile">
							
							<hr>


							<table id="file_table">
								
								

							</table>

						
						
</div>
					</div>
				</div>
				<div style="float: right; margin-top: -20px;">
							<input type="button" class="ui inverted secondary button"
								style="width: 100px;" value="취소" id="modalCancle">
							<button class="ui inverted primary button" type="submit"
								style="width: 100px;">등록</button>
						</div>
			</form>
		</div>
		
		
		
		
	</div>
	
	<script type="text/javascript">

</script>

	<script>
	  var count=1;
		$(function() {
			
			
			$("#product-success").hide();
			$("#product-danger").hide();
			
			 $('#productFile_btn').click(function (e) {
                 e.preventDefault();
                 var str = '<tr><td><input type="file" name="files" style="display:none" ><input class="upload_name" id="upload_name' 
                + (count++) + '" readonly style="border:0""></td><td><i class="trash alternate outline icon" style="float: right" onclick="remove_btn($(this))"></i></td></tr>';
                
                $("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
                
                $("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
                     var cur=$(this).val();
                     var arSplitUrl = cur.split('\\');
                     var nArLength = arSplitUrl.length;
                     var fileName = arSplitUrl[nArLength-1];
                       
                       $("#upload_name" + (count-1)).val(fileName);
                     console.log(cur);
                  }); 
               }); 
			
			 
	
			$("#modalCancle").click(function() {

				$('.ui.modal.adminProductEnroll').modal('hide');

			})

			

		/* 	$('#productFile_btn').click(function(e) {
				e.preventDefault();
				$('#productFile').click();
			}); */

			$("#pdtCode").keyup(function() {

				var pdtCode = $("#pdtCode").val();

				$.ajax({

					url : "duplicationCheck.pr",
					type : "post",
					data : {
						pdtCode : pdtCode
					},
					success : function(data) {

						console.log(data);

						//제품코드 중복인 경우
						if (data === "success") {
							$("#product-success").hide();
							$("#product-danger").show();
							document.getElementById('pdtCode').select();

						} else if (data === "fail") {
							if (pdtCode == "") {
								$("#product-success").hide();
								$("#product-danger").hide();
							} else {
								$("#product-success").show();
								$("#product-danger").hide();
							}

						}

					},
					error : function(status) {

					}

				})

			})

		})
		
		
		function remove_btn(obj) {
			 obj.parent().parent().remove();   
		}


	
	</script>


</body>
</html>