<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/modal.css">
</head>
<body>
	<div class="ui modal client">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						고객 조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr > 
							<td><input type="text" placeholder="고객"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내고객</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal" id="result_client_Modal">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			var company_hidden = $('#company_hidden')[0].value 
			var memberId = ${sessionScope.loginEmp.empNo};
			switch(btnName) {
			case 'client_Modal':
				$.ajax({
					url:"selectClient.cl",
					data:{companyId:company_hidden, memberId:memberId},
					type:"post",
					success:function(data) {
						$('.ui.modal.client').modal('show');
						$result_Modal = $('#result_client_Modal');
						$result_Modal.html('');
						for(var i=0; i<data.length; i++) {
							$result_Modal.append('<div><h4>'+data[i].comName+' / '+
							data[i].clientName +'</h4><input type="hidden" id="ClientId" value="'+data[i].clientId+'"><input type="hidden" id="ComId" value="'+data[i].comId+'"></div>')
						}
						$('#result_client_Modal h4').click(function(e){
						
							
							console.log(e.target.parentElement.children[1].value);
							var company = e.target.innerText.split('/');
							var companyId = e.target.parentElement.children[2].value;
							var clientId = e.target.parentElement.children[1].value;
							$('#company').val(company[0]);
							$('#company_hidden').val(companyId);
							$('#client').val(company[1]);
							$('#client_hidden').val(clientId);
							$('.ui.modal.client').modal('hide');
							})
					},
					error:function(status) {
						
					}
				});
				return false;
				break;
			}
		})
		$(".exit_Btn").click(function() {
				$('.ui.modal.client').modal('hide');
			})
	</script>
</body>
</html>