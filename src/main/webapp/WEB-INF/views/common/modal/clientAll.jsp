<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/modal.css">

<style>

.result_Modal2{height:300px; margin-top: 41px; border-top: 2px solid #6199B9; overflow: auto;}
.result_Modal2 div{height: 60px;  border-bottom: 1px solid #E5E5E5;}
.result_Modal2 div {padding-top: 20px; padding-left: 2%;}
</style>
</head>
<body>
	<div class="ui modal clientAll">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						연관고객 조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr > 
							<td><input type="text" placeholder="고객"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내고객</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal2" id="result_client_Modal2">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			var company_hidden = $('#company_hidden2')[0].value;
			
			switch(btnName) {
			case 'clientAll_Modal':
				$.ajax({
					url:"selectClient.cl",
					data:{companyId:company_hidden},
					type:"post",
					success:function(data) {
						$('.ui.modal.clientAll').modal('show');
						$result_Modal2 = $('#result_client_Modal2');
						$result_Modal2.html('');
						
						
						
						for(var i=0; i<data.length; i++) {
							$result_Modal2.append('<div><h4>'+data[i].comName+' / '+
							data[i].clientName +'</h4><input type="hidden" id="ClientId" value="'+data[i].clientId+'"><input type="hidden" id="ComId" value="'+data[i].clientName+'"><input type="hidden" id="ComId" value="'+data[i].phone+'"><input type="hidden" id="ComId" value="'+data[i].tel+'"><input type="hidden" id="ComId" value="'+data[i].comName+'"><input type="hidden" id="ComId" value="'+data[i].empName+'"></div>')
						}
						$('#result_client_Modal2 h4').click(function(e){
							
						
							//고객번호
							console.log(e.target.parentElement.children[1].value);
							//고객이름
							console.log(e.target.parentElement.children[2].value);
							//휴대번호
							console.log(e.target.parentElement.children[3].value);
							//유선번호
							console.log(e.target.parentElement.children[4].value);
							//회사이름
							console.log(e.target.parentElement.children[5].value);
							//담당자
							console.log(e.target.parentElement.children[6].value);
							
							var tr1 = '<tr class="td" style="border-top:2px solid #E5E5E5">';
							
							var td1 = ' <td colspan="2" style="width:40%; border-right: 1px solid #E5E5E5;">'+e.target.parentElement.children[2].value+'</td>';
							var td2 = '<td style="border-right: 1px solid #E5E5E5;">'+e.target.parentElement.children[3].value+'</td>';
							var td3 = '<td style="border-right: 1px solid #E5E5E5;">'+e.target.parentElement.children[4].value+'</td>';
							var td4 = '<td rowspan="2" style="border-right: 1px solid #E5E5E5;"><i class="trash alternate outline icon" onclick="remove_btn2(this)"></i></td>';
							var tr2 = '</tr>';
							var tr3 = '<tr>';
							var td5 = '<td colspan="2" style="border-right: 1px solid #E5E5E5;">'+e.target.parentElement.children[5].value+'</td>';
							var td6 = '<td colspan="2" style="border-right: 1px solid #E5E5E5;">'+e.target.parentElement.children[6].value+'</td>';
							var td0 = '<td><input type="hidden" name="clientId2" value="'+e.target.parentElement.children[1].value+'"></td>';
							var tr4 = '</tr>';
							var str = tr1 + td1 + td2 + td3+ td4+ tr2 + tr3+ td5 + td6 +td0+tr4;
							
							$("#table2").append(str);
							
							
							
							$('.ui.modal.clientAll').modal('hide');
							})
					},
					error:function(status) {
						
					}
				});
				return false;
				break;
			}
		})
		
		function remove_btn2(obj) {
		var ts = $(obj);
			ts.parent().parent('tr').next().remove(); 
			ts.parent().parent().remove();
		}

		$(".exit_Btn").click(function() {
				$('.ui.modal.clientAll').modal('hide');
			})
	</script>
</body>
</html>