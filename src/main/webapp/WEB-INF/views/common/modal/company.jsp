<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/modal.css">
</head>
<body>
	<div class="ui modal company">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						고객사 조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div> 
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr> 
							<td><input type="text" placeholder="고객사"></td>
							<td><label> <input type="checkbox"
									class="noCustormer" name="customerCheck"> <strong>고객없음</strong>
							</label> <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내고객</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal" id="result_company_Modal">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			switch(btnName) {
			case 'company_Modal':
				$.ajax({
					url:"selectCompany.co",
					type:"post",
					success:function(data) {
						$('.ui.modal.company').modal('show');
						$result_Modal = $("#result_company_Modal");
						$result_Modal.html('');
						for(var i=0; i<data.length; i++){
							$result_Modal.append('<div><h4>'+data[i].comName+' / 고객사 </h4><input type="hidden" value="'+data[i].comId+'"></div>');
						}
						$('#result_company_Modal h4').click(function(e){
							var company = e.target.innerText.split('/');
							var companyId;
							companyId = e.target.parentElement.children[1].value
							$('#company').val(company[0]);
							$('#company_hidden').val(companyId);
							$('.ui.modal.company').modal('hide');
							})
					},
					error:function(status) { 
						console.log("실패");
					}
				});
				return false;
				break;
			}
		})
		$(".exit_Btn").click(function() {
				$('.ui.modal.company').modal('hide');
			})
	</script>
</body>
</html>