<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
</head>
<body>
	<div class="ui modal completeAct">
		<div class="content_WindowCal">
			<div class="header_ModalCal">
				<div>
					<h2>
						활동완료<i class="exit_Btn times icon "></i>
					</h2> 
				</div>
			</div>
			<div class="contents_ModalCal">
				<div class="contents_Modal_DivCal">
					<form action="completeAct.sa" method="post">
						<input type="hidden" name="activityId" id="updateConId" value="0">
						<table class="htable">
							<colgroup>
								<col style="width:80px;">
								<col style="width:auto;">
							</colgroup>
							<tbody>
								<tr>
									<th>활동내용</th>
									<td><textarea  cols="60" rows="7" name="activityCon" id="updateCon"></textarea></td>
								</tr>
							</tbody>	
						</table>
						<div class="priCalBtn">
							<button class="ui basic gray button" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>

		/* $('#privateDate').on('click', function(){
			$('.ui.modal.completeAct').modal('show');
		}); */
		

		
	</script>
</body>
</html>