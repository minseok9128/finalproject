<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
<div class="ui modal contract">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						계약 조회<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div> 
			<div class="contents_Modal"> 
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr>
							<td><input type="text" placeholder="계약명"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내계약</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal">
						<table class="result_Table" id="result_Table_est">
							<thead>
								<tr>
									<th>계약일</th>
									<th>계약명</th>
									<th>고객사</th>
									<th>고객</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	$(document).click(function(e){
		var btnName = (e.target.id);
		switch(btnName) {
		case 'contract_Modal':
			$.ajax({
				url:"selListModal.con",
				type:"post",
				success:function(data) {
					console.log(data);
					$('.ui.modal.contract').modal('show');
					$table_tbody = $('#result_Table_est tbody:not(first-child)');
					$table_tbody.html('');
					console.log("ㅇㅇ"+data.length);
					for(var i=0; i<data.length; i++) {
						$table_tbody.append('<tr><td>'+data[i].contDate + 
								'</td><td>' + data[i].contName
								/* + '</td><td>' + data[i].comName */
								+ '</td><td>' + data[i].comName
								+ '</td><td>' + data[i].clientName + '<input type="hidden" value="'+data[i].contId+'/'+data[i].comId+'/'+data[i].clientId+'"></td></tr>' 
								/* +'<input type="hidden" value="'+data[i].oppId+'/'+data[i].clientId+'/'+data[i].company.comId+ '/' + data[i].processId+'">'
								+'<input type="hidden" value="'+data[i].oppName+'/'+data[i].agentName+'/'+data[i].company.comName+'"></td></tr>'*/ );
														
					}
					$('#result_Table_est tbody tr td').click(function(e){
						var contName = e.target.parentNode.childNodes[1].innerHTML;
						var clientName = e.target.parentNode.childNodes[3].innerText;
						var comName = e.target.parentNode.childNodes[2].innerHTML;
						var id = e.target.parentNode.childNodes[3].childNodes[1].value.split('/');
						console.log(contName,clientName,comName)
						//계약/고객사/고객
						console.log(id[0],id[1],id[2] )
						/* var name = e.target.childNodes[2].value.split('/');
						console.log(name[0],name[1],name[2]) */
						$('#company').val(comName);
						$('#company_hidden').val(id[1]);
						$('#client').val(clientName);
						$('#client_hidden').val(id[2]);
						$('#contract').val(contName);
						$('#contract_hidden').val(id[0]);
						
						$('.ui.modal.contract').modal('hide');
						/*if(name[3] == 1){
							$('#pro_select').find('option').eq(1).prop('selected', true);
						}else if(name[3] == 2){
							$('#pro_select').find('option').eq(2).prop('selected', true);
						}else if(name[3] == 3){
							$('#pro_select').find('option').eq(3).prop('selected', true);
						}else if(name[3] == 4){
							$('#pro_select').find('option').eq(4).prop('selected', true);
						} */
					});
				},
				error:function(status) {
					
				}
			})
			
			return false;
			break;
		}
		$(".exit_Btn").click(function() {
			$('.ui.modal.contract').modal('hide');
		})
	})
	</script>
</body>
</html>