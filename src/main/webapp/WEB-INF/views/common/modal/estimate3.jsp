<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
<div class="ui modal estimate">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						견적 조회<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div> 
			<div class="contents_Modal"> 
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr>
							<td><input type="text" placeholder="견적명"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내견적</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal">
						<table class="result_Table" id="result_Table_est">
							<thead>
								<tr>
									<th>견적일</th>
									<th>견적명</th>
									<th>고객사</th>
									<th>고객</th>
									<th>견적금액</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	$(document).click(function(e){
		var btnName = (e.target.id);
		switch(btnName) {
		case 'estimate_Modal':
			$.ajax({
				url:"selList.est",
				type:"post",
				success:function(data) {
					console.log(data);
					$('.ui.modal.estimate').modal('show');
					$table_tbody = $('#result_Table_est tbody:not(first-child)');
					$table_tbody.html('');
					console.log(data.length);
					var clkPrArr = [];
					for(var i=0; i<data.length; i++) {
						clkPrArr.push(data[i].prList);
						$table_tbody.append('<tr><td>'+data[i].estDate + 
								'</td><td>' + data[i].estName
								+ '</td><td>' + data[i].comName
								+ '</td><td>' + data[i].clientName
								+ '</td><td>' + data[i].estTotal + '<input type="hidden" value="'+data[i].oppId+'/'+data[i].clientId+'/'+data[i].companyId+'/'+data[i].estId+'/'+ i +'/'+data[i].oppName+'/'+data[i].estVatYn+'"></td></tr>')
					}
					$('#result_Table_est tbody tr td').click(function(e){
						for(var i = 0;  i < clkPrArr.length; i++){
							console.log("제품리스트 : " + clkPrArr[i]);
						}
						var estName = e.target.parentNode.childNodes[1].innerHTML;
						var comName = e.target.parentNode.childNodes[2].innerHTML;
						var clientName = e.target.parentNode.childNodes[3].innerHTML;
						var id = e.target.parentNode.childNodes[4].childNodes[1].value.split('/');
						console.log(estName,comName,clientName)
						//기회 아이디/고객아이디/고객사아이디/견적아이디/기회 이름
						console.log("헬로")
						console.log(id[0],id[1],id[2],id[3],id[4],id[5],id[6] +"마지막이 담긴 제품순번" )
						$('#company').val(comName);
						$('#company_hidden').val(id[2]);
						$('#client').val(clientName);
						$('#client_hidden').val(id[1]);
						$('#opportunity').val(id[5]);
						$('#opportunity_hidden').val(id[0]);
						$('#estimate').val(estName);
						$('#estimate_hidden').val(id[3]);
						if(id[6] == 'N'){
							console.log(id[6]);
							$("#vatYn option:eq(0)").prop("selected", true);
							console.log('체크no됨');
						} else {
							console.log(id[6]);
							$("#vatYn option:eq(1)").prop("selected", true);
							console.log('체크됨');
						}
					
						
						 var $input = '<input type="text" name="remark" value="">';
						 var $option1 = '<option value="ONCE">1회</option>';
						 var $option2 = '<option value="MONTH">월별</option>';
						 var $option3 = '<option value="QUARTER">분기별</option>';
						 var $option4 = '<option value="HALF">반기별</option>';		 
						 var $select = '<select name="revenuePeriod">'+ $option1 + $option2 + $option3 + $option4 +'</select>'; 
						 
						 var cal = '<div class="ui calendar standard_calendar procal" id="standard_calendar"> <div class="ui input right icon"> <i class="calendar icon"></i> <input name="revDate" type="text" readonly> </div></div>'
						
						for(var i = 0; i <clkPrArr[id[4]].length; i++ ){
							var num = 1;
							console.log("이거 나오면 성공");
							console.log(clkPrArr[id[4]][i].pdtDtailId);
							var prId = clkPrArr[id[4]][i].pdtId;
							//이름 조인해서 가져오기;
							var prName = clkPrArr[id[4]][i].category;
							var discountRate = (100 -clkPrArr[id[4]][i].discountRate) * 0.01;
							var price = clkPrArr[id[4]][i].detailPrice * discountRate;
							var totalPrice = clkPrArr[id[4]][i].totalPrice;
							var amount = clkPrArr[id[4]][i].amount;
							var num = amount;
							
							//standard
							var standard = clkPrArr[id[4]][i].deliveryDate;
							$table_tbody = $('#contract_Product_Table tbody');
							$table_tbody.before('<tr><td><input type="hidden" name="prId" value="'+prId +'">'+ (prName) +'</td><td><input type="text" class="price" value="'+ price +'" ></td><td><input class="amount" type="text" name="amount" value="'+ num +'"></td><td class="sum">' + (num * price) +'</td><td rowspan="2"><i class="trash alternate outline icon center"/></td></tr>' );
							$table_tbody.before('<tr><td>'+ $select +'</td><td>'+ cal +'</td><td colspan="2">'+ $input +'</td></tr>' );
						}
						 $table_tfoot = $('#contract_Product_Table tfoot');
						
				 		 var tf = $("#resultSum");
						  var resultA = 0;
						  var resultS = 0;
						  var amountArr  = $(".amount");
						  var sumArr  = $(".sum");
						  for(var i = 0; i < amountArr.length; i++){
							  console.log(amountArr[i].value);
							  resultA += Number(amountArr[i].value);
							}
						  for(var i = 0; i < sumArr.length; i++){
							  console.log(sumArr[i].value);
							  resultS += Number(sumArr[i].innerHTML);
							}
						  //수량 , 합계 바꾸는 부분
						  if($table_tbody[0].rows.length == 2){
							  $table_tfoot.append("<tr id='resultSum'><th colspan='2'>수량합계</th><th>"+resultA+"</th><th>제안금액 합계</th><th>"+resultS+"</th><th></th></tr>");
		                         /* $table_tfoot.append("<tr><th colspan='2'>수량합계</th><th>"+comma(totalQuantity)+"</th><th>제안금액 합계</th><th>"+comma(totalQuantity)+"</th><th></th></tr>"); */
		                   }
						  /* if($table_tfoot){
						   	$table_tfoot.append("<tr id='resultSum'><th colspan='2'>수량합계</th><th>"+resultA+"</th><th>제안금액 합계</th><th>"+resultS+"</th><th></th></tr>");
						  } */
						  /* tf.children().eq(1).html(resultA)
						  tf.children().eq(3).html(resultS) */
						  //totalAmount
						  //totalValue
						  //tax
						  //total
						  console.log("resultA : " + resultA);
						  console.log("resultS : " + resultS);
						  var ttA = $("#totalAmount");
						  var ttV = $("#totalValue");
						  var tax = $("#tax");
						  var tt = $("#total");
						  //수량 , 합계 바꾸는 부분
						  tf.children().eq(1).html(resultA.toFixed()) 
						  tf.children().eq(3).html(resultS.toFixed())
						  
						  ttA.val(resultA.toFixed());
						  ttV.val(resultS.toFixed());
						  tax.val((resultS*0.1).toFixed());
						  tt.val((1.1 * resultS).toFixed());
						
				$('.standard_calendar').calendar({
					type : 'date',
					formatter: {
						  date : function (date, settings) {
						        if (!date) return '';
						        var day = date.getDate();
						        var month = date.getMonth() + 1;
						        var year = date.getFullYear();
						        return year + '-' + month + '-' + day;
						      }
						},
				});
						
						$('.ui.modal.estimate').modal('hide');
					});
				},
				error:function(status) {
					
				}
			})
			
			return false;
			break;
		}
		$(".exit_Btn").click(function() {
			$('.ui.modal.estimate').modal('hide');
		})
	})
	</script>
</body>
</html>