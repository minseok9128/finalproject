<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/modal.css">
<style>
	#duswn {
		border:1px solid lightgrey;
		text-align:center !important;
	}
	#contDate{
		
		background:#F8FCFF;
	}
	#contactDate{
		text-align:center !important;
		background:#F8FCFF;
		vertical-align: middle;
	}
	#contactContent{
		background:#F8FCFF;
		vertical-align:middle;
	}
	#alstjr{
	
	}
</style>

</head>
<body>
	<div class="ui modal history">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						&nbsp;&nbsp;&nbsp;접촉 등록<i class="exit_Btn times icon "></i>
					</h1> 
				</div> 
			</div>
			
			<div class="contents_Modal">
						<form action="insertConHis.le" method="post">
						<input id="leadId" type="hidden" name="leadId" >
							<table id="duswn" class="table_modal oneText" border="1">
								<tr>
									<td id="contDate">접촉일</td><!-- contactDate -->
									<td width="218px">
										<div class="ui calendar" id="year_calendar">
										  <div class="ui input left icon">
										    <i class="calendar icon"></i>
										    <input type="text" placeholder="Date" name="contactDate" readonly>
										  </div>
										</div>
									</td>
									<td id="contactDate">접촉구분</td><!-- contactDate -->
									<td id="alstjr">
										<select name="category" style="position: relative; bottom: 12px;">
											<option value="1">전화</option>
											<option value="2">메일</option>
											<option value="3">방문</option>
											<option value="4">기타</option>
										</select>
									</td><!-- contactDate -->
								</tr>
							<!-- 	<tr>
									<td>접촉구분</td>
									<td>
										<select name="category">
											<option value="1">전화</option>
											<option value="2">메일</option>
											<option value="3">방문</option>
											<option value="4">기타</option>
										</select>
									</td>
								</tr> -->
								<tr>
									<td id="contactContent">접촉내용</td>
									<td colspan="3">
										<textarea  name="contactContent" style="resize: none; border-color:lightgrey;" rows="7" cols="100"></textarea>
									</td>
								</tr>
							</table>
							<!-- button  -->
							<div>
								<!-- <input type="submit" value="등록"> -->
								<button class="ui inverted primary button float" style="float:right;" onclick="modalInsertList()">등록</button>
							</div>
						</form>
					</div><!-- content end -->
					
				</div>
			</div>
	<script>
	$(function(){
		$('#year_calendar').calendar({
		    type: 'year'
		  });
		$("#year_calendar").click(function(){
			console.log("clicked");
		})
	})
	/*  function modalInsertList(){
		         
		         $.ajax({
		            url:"insertConHis.le",
		            type:"post",
		            data:{LeadContactHistory:LeadContactHistory},
		            success:function(data){
		               console.log(data);
		            },
		            error:function(data){
		               console.log(status);
		            }
		         });
		         
		         return false;
		 }  */
	
	
	
	</script>
</body>
</html>