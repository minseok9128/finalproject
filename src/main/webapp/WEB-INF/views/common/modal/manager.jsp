<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
<div class="ui modal manager">
  <div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						담당자 조회<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr >
							<td><input type="text" placeholder="고객"></td>
						</tr> 
					</table>
					<div class="result_Modal">
						<table class="result_Table right" id="result_Table_Modal">
							<thead>
								<tr>
									<th>부서명</th>	
									<th>담당자</th>	
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			switch(btnName) {
			case 'manager':
				$.ajax({
					url:"selectEmployeeModal.em",
					data:{},
					type:"post",
					success:function(data){
						$('.ui.modal.manager').modal('show');
						$table_tbody = $('#result_Table_Modal tbody');
						$table_tbody.html('');
						 for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].deptName+'</td><td>'+data[i].empName+'<input type="hidden" value="'+
									data[i].empNo+'"></td>');
						 } 
						 $table_tbody.click(function(e){
							 var name = e.target.parentElement.children[1].innerText;
							 var id = e.target.parentElement.children[1].children[0].value;
							 $('#managerText').val(name);
							 $('#manager_hidden').val(id);
							 $('.ui.modal.manager').modal('hide');
						 })
						 console.log(data);
					},
					error:function(status){
						
					}
				})
				return false;
				break;
			}
		})
		$(function() {
			//모달 뛰우는 스크립트
			$(".exit_Btn").click(function() {
				$('.ui.modal.manager').modal('hide');
			})
		})
	</script>
</body>
</html>