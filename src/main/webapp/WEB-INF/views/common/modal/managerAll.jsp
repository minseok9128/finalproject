<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
<div class="ui modal manager2">
  <div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						지원인력 조회<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr >
							<td><input type="text" placeholder="고객"></td>
						</tr> 
					</table>
					<div class="result_Modal3">
						<table class="result_Table right" id="result_Table_Modal2">
							<thead>
								<tr>
									<th>부서명</th>	
									<th>담당자</th>	
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			switch(btnName) {
			case 'manager2':
				$.ajax({
					url:"selectEmployeeModal.em",
					data:{},
					type:"post",
					success:function(data){
						
					
						
						$('.ui.modal.manager2').modal('show');
						$table_tbody = $('#result_Table_Modal2 tbody');
						$table_tbody.html('');
						 for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].deptName+'</td><td><p>'+data[i].empName+'</p><input type="hidden" value="'+
									data[i].empNo+'"></td>');
						 } 
						 $("#result_Table_Modal2 tbody p").click(function(e){
							 
							 //부서명
							 var depart = e.target.parentElement.parentNode.children[0].innerHTML;
							 var empName = e.target.parentElement.parentNode.children[1].children[0].innerHTML;
							 var empNo = e.target.parentElement.parentNode.children[1].children[1].value;
							 
							
								
								var tr1 = ' <tr class="td">';
								var td1 = '<td colspan="2" style=" border-right: 1px solid #E5E5E5; width:35%">'+depart+' '+ empName+'</td>';
							 	var td2 = '<td style="border-right: 1px solid #E5E5E5;">';
							 	var td3 = '<select name="CATEGORY"  style="width:120px;  height: 33px;"><option value="1">영업</option>';
							 	var td4 = '<option value="2">기술지원</option>';
							 	var td5 = '<option value="3">기타</option></select></td>';
							 	var td6 = '<td style="border-right: 1px solid #E5E5E5;">${sessionScope.loginEmp.empName }<input type="hidden" name="enroller" value="${sessionScope.loginEmp.empNo }"><input type="hidden" name="empNo2" value="'+empNo+'"></td>';
							 	var tr2 = '<td style="border-right: 1px solid #E5E5E5;"><i class="trash alternate outline icon" onclick="remove_btn3(this)"></i></td> </tr>';
							 	
							 	
							 	
							 	var str = tr1 + td1 + td2 + td3+ td4+ td5+ td6+tr2 ;
							 	
							 	$("#managerTable").append(str);
							 	
							 	$('.ui.modal.manager2').modal('hide');
							 
						 })
						
					},
					error:function(status){
						
					}
				})
				return false;
				break;
			}
		})
		
		function remove_btn3(obj) {
		var ts = $(obj);
	 
			ts.parent().parent().remove();
		}
		$(function() {
			//모달 뛰우는 스크립트
			$(".exit_Btn").click(function() {
				$('.ui.modal.manager2').modal('hide');
			})
		})
	</script>
</body>
</html>