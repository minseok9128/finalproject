<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/modal.css">
</head>
<body>
	<div class="ui modal multiClient">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						고객 조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr > 
							<td><input type="text" placeholder="고객"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내고객</strong>
							</label></td>
						</tr>
					</table>
					<div class="clientArea">
						<ul>
							
						</ul>
					</div>
					<div class="result_Modal" id="result_client_Modal">
					</div>
					<div class="subBtnArea">
						<button class="ui basic gray button" id="submitPartBtn">완료</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			var company_hidden = 0
			switch(btnName) {
			case 'multiClient':
				$.ajax({
					url:"selectClient.cl",
					data:{companyId:company_hidden},
					type:"post",
					success:function(data) {
						var partArr = ''; //다중 선택한 고객 이름들을 저장
						
						$('.ui.modal.multiClient').modal('show');
						$result_Modal = $('#result_client_Modal');
						$result_Modal.html('');
						for(var i=0; i<data.length; i++) {
							$result_Modal.append('<div><h4>'+data[i].comName+' / '+
							data[i].clientName +'</h4><input type="hidden" id="ClientId" value="'+data[i].clientId+'"><input type="hidden" id="ComId" value="'+data[i].comId+'"></div>')
						}
						//고객 이름을 추가할 때마다 고객 추가
						$('#result_client_Modal h4').off('click').on('click', function(e){
							var ckpart = 0;
							console.log(e.target.parentElement.children[1].value);
							var company = e.target.innerText.split('/');
							var companyId = e.target.parentElement.children[2].value;
							var clientId = e.target.parentElement.children[1].value;
							//$('#company').val(company[0]);
							//$('#company_hidden').val(companyId);
							//$('#participant').val(company[1]);
							//$('#participantId').val(clientId);
							//$('.ui.modal.multiClient').modal('hide');
							
							
							 $('.clientArea ul li').each(function(){
								 if(company[1] == $(this).find($('.clBtn')).text()){
									 ckpart += 1;
								 }
							 });
							 
							 var addpart = '<li class="clientName"><input type="hidden" class="clientId" value='+ clientId +'><button type="button" class="clBtn">'+ company[1] +'<i><img src="${ contextPath }/resources/images/close_icon.png"></i></button></li>';								 
							 if(ckpart == 0){
							 	$('.clientArea ul').append(addpart);
							 }
						});
						
						 //완료 버튼클릭시 입력폼에 이름과 사원번호 뿌려줌
						 $(document).off("click","#submitPartBtn").on("click","#submitPartBtn",function(){
							    //$("#result_Table_Modal tbody").off().on('click', function() {});
							    
							    $('.client_srch .partIdVal').remove();
							    $('.clientArea ul li').each(function(){
							    	console.log($(this).find($('.clBtn')).text());
							    	if(partArr == ''){
							    		partArr += $(this).find($('.clBtn')).text();
							    	}else{
							    		partArr += ', ' + $(this).find($('.clBtn')).text();
							    	}
							    	console.log($(this).find('.clientId').val());
							    	
							    	$('.emp_srch').append('<input type="hidden" class="partIdVal" name="participant" value='+ $(this).find('.clientId').val() +'>');
							    });
							    
								$('#participantText').val(partArr);
								$('.ui.modal.multiClient').modal('hide');
								partArr = '';
							});
						
					},
					error:function(status) {
						
					}
				});
				return false;
				break;
			}
		})
		
		//모달창에서 추가되어있는 이름버튼 누르면 이름제거
		$(document).on("click",".clBtn",function(){
			$(this).parent().remove();
		});
		
		//취소 버튼
		$(document).on("click",".exit_Btn",function(){
			 $("#result_client_Modal h4").off().on('click', function() {});
			 $('.clientArea ul *').remove();
			 $('.ui.modal.multiClient').modal('hide');
			
        });
	</script>
</body>
</html>