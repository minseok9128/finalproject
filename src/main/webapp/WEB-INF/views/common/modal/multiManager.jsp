<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
</head>
<body>
<div class="ui modal multiManager">
  <div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						동반인력 조회<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr >
							<td><input type="text" placeholder="담당자"></td>
						</tr> 
					</table>
					<div class="managerArea">
						<ul>
							
						</ul>
					</div>
					<div class="result_Modal">
						<table class="result_Table right">
							<colgroup>
								<col style="width:40%">
								<col style="width:60%">
							</colgroup>
							<thead>
								<tr>
									<th>부서명</th>	
									<th>담당자</th>	
								</tr>
							</thead>
						</table>
						<table class="result_Table right" id="result_Table_Modal">
							<colgroup>
								<col style="width:40%">
								<col style="width:60%">
							</colgroup>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="subBtnArea">
						<button class="ui basic gray button" id="submitBtn">완료</button>
					</div>
				</div>
			</div>
		</div>
</div>
<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			switch(btnName) {
			case 'multiManager':
				$.ajax({
					url:"selectEmployeeModal.em",
					data:{},
					type:"post",
					success:function(data){						
						var nameArr = ''; //다중 선택한 담당자 이름들을 저장
						
						$('.ui.modal.multiManager').modal('show');
						$table_tbody = $('#result_Table_Modal tbody');
						$table_tbody.html('');
						 for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].deptName+'</td><td>'+data[i].empName+'<input type="hidden" value="'+
									data[i].empNo+'"></td>');
						 } 
						 //담당자 행을 클릭할때 마다 이름추가
						 $table_tbody.off('click').on('click', function(e){
							 var ckname = 0;
							 var deptName = e.target.parentElement.children[0].innerText;
							 var name = e.target.parentElement.children[1].innerText;
							 var id = e.target.parentElement.children[1].children[0].value;
							 console.log('name' + name);
							 $('.managerArea ul li').each(function(){
								 if(name == $(this).find($('.mnBtn')).text()){
									 ckname += 1;
								 }
							 });
							 
							 var addName = '<li class="managerName"><input type="hidden" class="empNo" value='+ id +'><button type="button" class="mnBtn">'+ name +'<i><img src="${ contextPath }/resources/images/close_icon.png"></i></button></li>';								 
							 if(ckname == 0){
							 	$('.managerArea ul').append(addName);
							 }

						 });
						 //완료 버튼클릭시 입력폼에 이름과 사원번호 뿌려줌
						 $(document).off("click","#submitBtn").on("click","#submitBtn",function(){
							    //$("#result_Table_Modal tbody").off().on('click', function() {});
							    
							    $('.emp_srch .empNoVal').remove();
							    $('.managerArea ul li').each(function(){
							    	console.log($(this).find($('.mnBtn')).text());
							    	if(nameArr == ''){
							    		nameArr += $(this).find($('.mnBtn')).text();
							    	}else{
								    	nameArr += ', ' + $(this).find($('.mnBtn')).text();
							    	}
							    	console.log($(this).find('.empNo').val());
							    	
							    	$('.emp_srch').append('<input type="hidden" class="empNoVal" name="companion" value='+ $(this).find('.empNo').val() +'>');
							    });
							    
								$('#managerText').val(nameArr);
								$('.ui.modal.multiManager').modal('hide');
								nameArr = '';
							});
					},
					error:function(status){
						
					}
				})
				
				return false;
				break;
			}
		})
		
		//모달창에서 추가되어있는 이름버튼 누르면 이름제거
		$(document).on("click",".mnBtn",function(){
			$(this).parent().remove();
		})
		
		//취소 버튼
		 $(document).on("click",".exit_Btn",function(){
			 $("#result_Table_Modal tbody").off().on('click', function() {});
			 $('.managerArea ul *').remove();
			 $('.ui.modal.multiManager').modal('hide');
			
         });
	</script>
</body>
</html>