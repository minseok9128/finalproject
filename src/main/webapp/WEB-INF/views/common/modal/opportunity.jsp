<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
	<div class="ui modal opportunity">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						영업기회조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr >
							<td><input type="text" placeholder="영업기회"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내영업</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal">
						<table class="result_Table" id="result_Table_Modal">
							<thead>
									<tr>
										<th>영업기회</th>
									</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			var memberId = ${sessionScope.loginEmp.empNo};
			switch(btnName) {
			case 'opportunity_Modal':
				$.ajax({
					url:"selectOpportunityModal.so",
					data:{
						memberId : memberId
					},
					type:"post",
					success:function(data) {
						$('.ui.modal.opportunity').modal('show');
						$table_tbody = $('#result_Table_Modal tbody');
						$table_tbody.html('');
						for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].oppName
									+'<input type="hidden" value="'+data[i].oppId+'/'+data[i].clientId+'/'+data[i].company.comId+ '/' + data[i].processId+'">'
									+'<input type="hidden" value="'+data[i].oppName+'/'+data[i].client.clientName+'/'+data[i].company.comName+'"></td></tr>');
															
						}
						$('#result_Table_Modal td').bind('click',function(e){
							var id = e.target.childNodes[1].value.split('/');
							var name = e.target.childNodes[2].value.split('/');
							console.log(name);
							console.log(id);
							
							$('#company').val(name[2]);
							$('#company_hidden').val(id[2]);
							$('#client').val(name[1]);
							$('#client_hidden').val(id[1]);
							$('#opportunity').val(name[0]);
							$('#opportunity_hidden').val(id[0]);
							var estId = ${estId}+'';
							var ppsId = ${ppsId}+'';
							var a = '';
							if(estId != ''){
								a = '견적';
								$.ajax({
									url:'selectOppPrd.est',
									data:{oppId : id[0]},
									type:'post',
									success:function(data){
										 $table_tbody = $('#contract_Product_Table tbody');
										 $table_tfoot = $('#contract_Product_Table tfoot');
										 var totalQuantitiy = 0;
										 var totalPrice = 0;
										 if($table_tbody.children().length != 0){
											 $table_tbody[0].innerHTML = '';
											 $table_tfoot[0].innerHTML = ''; 
										} 
										 for(var i=0; i<data.length; i++) {
											$table_tbody.append(
													'<tr><td colspan="2">'+data[i].PDT_NAME+'<input type="hidden" name="pdtName" value="'+data[i].PDT_NAME+'"><input type="hidden" name="pdtId" value="'+data[i].PDT_ID+'"></td>'+
													'<td><input type="text" value="'+comma(data[i].PRICE)+'"><input type="hidden" name="price" value="'+comma(data[i].PRICE)+'"></td>'+
													'<td><input type="text" name="discountRate" value="'+data[i].DISCOUNT_RATE+'">%</td>'+
													'<td><input type="text" value="'+comma(Math.abs(Math.round(data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)))+'"><input type="hidden" name="detailPrice" value="'+(Math.abs(Math.round(data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)))+'"></td>'+
													'<td rowspan="2"><i class="trash alternate outline icon center"/></td></tr>')
											$table_tbody.append(
													'<tr><td>'+comma(data[i].QUANTITY)+'</td>'+
													'<td>'+data[i].STANDARD+'</td><td><input type="text" name="quantity" value="'+data[i].QUANTITY_DETAIL+'"></td>'+
													'<td colspan="2"><input type="text" value="'+comma(Math.abs((data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)*data[i].QUANTITY_DETAIL))+'"></td></tr>')
													totalQuantitiy += (data[i].QUANTITY_DETAIL);
													totalPrice += Number(Math.abs((data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)*data[i].QUANTITY_DETAIL));
										}
										 
										if(data.length != 0){
											$table_tfoot.append("<tr><th colspan='2'>수량합계</th><th>"+comma(totalQuantitiy)+"</th><th>제안금액 합계</th><th>"+comma(totalPrice)+"</th><th></th></tr>");
										}
										if($('#VATCheckBox')[0].checked){
											$('#price').val(comma(totalQuantitiy));
											$('#totalValueOfSupply').val(comma(totalPrice- Math.abs(Math.round(totalPrice / 1.1 - totalPrice))));
											$('#suggestionValueOfSupply').val(comma(totalPrice- Math.abs(Math.round(totalPrice / 1.1 - totalPrice))));
											$('#tex').val(comma(Math.abs(Math.round(totalPrice / 1.1 - totalPrice))));
											$('#total').val(comma(totalPrice));
										} else {
											$('#price').val(comma(totalQuantitiy));
											$('#totalValueOfSupply').val(comma(totalPrice));
											$('#suggestionValueOfSupply').val(comma(totalPrice));
											$('#tex').val(comma(Math.round(totalPrice * 1.1 - totalPrice)));
											$('#total').val(comma(Math.round(totalPrice * 1.1)));
										}
									}
									
								})
							}
							if(ppsId != ''){
								a = '제안';
							}
							$('#offer_Name').val(name[0]+' '+a);
							$('#processId').val(id[3]);
							$('.ui.modal.opportunity').modal('hide');
							$('#progress').val(name[3]);
							if(name[3] == 1){
								$('#pro_select').find('option').eq(1).prop('selected', true);
							}else if(name[3] == 2){
								$('#pro_select').find('option').eq(2).prop('selected', true);
							}else if(name[3] == 3){
								$('#pro_select').find('option').eq(3).prop('selected', true);
							}else if(name[3] == 4){
								$('#pro_select').find('option').eq(4).prop('selected', true);
							}
						});
					},
					error:function(status) {
						
					}
				})
				
				return false;
				break;
			}
			$(".exit_Btn").click(function() {
				$('.ui.modal.opportunity').modal('hide');
			})
		})
	</script>
</body>
</html>