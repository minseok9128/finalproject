<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
	<div class="ui modal opportunity2">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						영업기회조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr >
							<td><input type="text" placeholder="영업기회"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내영업</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal">
						<table class="result_Table" id="result_Table_Modal">
							<thead>
									<tr>
										<th>영업기회</th>
									</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:set var="oppId" value="${oppId}" />
	<script>
		
			
			var memberId = ${sessionScope.loginEmp.empNo};
			var oppId = ${oppId};
			var index = 0;
		
			
				$.ajax({
					url:"selectOpportunityModal.so",
					data:{
						memberId : memberId
					},
					type:"post",
					success:function(data) {
					
						for(var i=0; i<data.length; i++) {
							
							if(data[i].oppId == oppId){
								index = i;
								
							}
															
						}
					
						 	$('#company').val(data[index].company.comName);
							$('#company_hidden').val(data[index].company.comId);
							$('#client').val(data[index].client.clientName);
							$('#client_hidden').val(data[index].client.clientId);
							$('#opportunity').val(data[index].oppName);
							$('#opportunity_hidden').val(oppId); 
							$('#processId').val(data[index].process.processId);
					
							var offerName = data[index].oppName+' 견적';
							
							var estId = ${estId}+'';
							var ppsId = ${ppsId}+'';
						
							
							$('#offer_Name').val(offerName);
							
								$.ajax({
									url:'selectOppPrd.est',
									data:{oppId : oppId},
									type:'post',
									success:function(data){
										 $table_tbody = $('#contract_Product_Table tbody');
										 $table_tfoot = $('#contract_Product_Table tfoot');
										 var totalQuantitiy = 0;
										 var totalPrice = 0;
										 if($table_tbody.children().length != 0){
											 $table_tbody[0].innerHTML = '';
											 $table_tfoot[0].innerHTML = ''; 
										} 
										 for(var i=0; i<data.length; i++) {
											$table_tbody.append(
													'<tr><td colspan="2">'+data[i].PDT_NAME+'<input type="hidden" name="pdtName" value="'+data[i].PDT_NAME+'"><input type="hidden" name="pdtId" value="'+data[i].PDT_ID+'"></td>'+
													'<td><input type="text" value="'+comma(data[i].PRICE)+'"><input type="hidden" name="price" value="'+comma(data[i].PRICE)+'"></td>'+
													'<td><input type="text" name="discountRate" value="'+data[i].DISCOUNT_RATE+'">%</td>'+
													'<td><input type="text" value="'+comma(Math.abs(Math.round(data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)))+'"><input type="hidden" name="detailPrice" value="'+(Math.abs(Math.round(data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)))+'"></td>'+
													'<td rowspan="2"><i class="trash alternate outline icon center"/></td></tr>')
											$table_tbody.append(
													'<tr><td>'+comma(data[i].QUANTITY)+'</td>'+
													'<td>'+data[i].STANDARD+'</td><td><input type="text" name="quantity" value="'+data[i].QUANTITY_DETAIL+'"></td>'+
													'<td colspan="2"><input type="text" value="'+comma(Math.abs((data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)*data[i].QUANTITY_DETAIL))+'"></td></tr>')
													totalQuantitiy += (data[i].QUANTITY_DETAIL);
													totalPrice += Number(Math.abs((data[i].DISCOUNT_RATE * data[i].PRICE - data[i].PRICE)*data[i].QUANTITY_DETAIL));
										}
										 
										if(data.length != 0){
											$table_tfoot.append("<tr><th colspan='2'>수량합계</th><th>"+comma(totalQuantitiy)+"</th><th>제안금액 합계</th><th>"+comma(totalPrice)+"</th><th></th></tr>");
										}
										if($('#VATCheckBox')[0].checked){
											$('#price').val(comma(totalQuantitiy));
											$('#totalValueOfSupply').val(comma(totalPrice- Math.abs(Math.round(totalPrice / 1.1 - totalPrice))));
											$('#suggestionValueOfSupply').val(comma(totalPrice- Math.abs(Math.round(totalPrice / 1.1 - totalPrice))));
											$('#tex').val(comma(Math.abs(Math.round(totalPrice / 1.1 - totalPrice))));
											$('#total').val(comma(totalPrice));
										} else {
											$('#price').val(comma(totalQuantitiy));
											$('#totalValueOfSupply').val(comma(totalPrice));
											$('#suggestionValueOfSupply').val(comma(totalPrice));
											$('#tex').val(comma(Math.round(totalPrice * 1.1 - totalPrice)));
											$('#total').val(comma(Math.round(totalPrice * 1.1)));
										}
									}
									
								})
							
						
						
					},
					error:function(status) {
						
					}
				})
				
			
		
		
	</script>
</body>
</html>