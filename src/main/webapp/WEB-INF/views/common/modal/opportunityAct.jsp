<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
	<div class="ui modal opportunity">
		<div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						영업기회조회<i class="exit_Btn times icon "></i>
					</h1> 
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal oneText">
						<tr >
							<td><input type="text" placeholder="영업기회"></td>
							<td>
							 <label> <input type="checkbox" class="myCustormer_modal"
									name="customerCheck"> <strong>내영업</strong>
							</label></td>
						</tr>
					</table>
					<div class="result_Modal">
						<table class="result_Table" id="result_Table_Modal">
							<thead>
									<tr>
										<th>영업기회</th>
									</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).click(function(e){
			var btnName = (e.target.id);
			var memberId = ${sessionScope.loginEmp.empNo};
			switch(btnName) {
			case 'opportunity_Modal':
				$.ajax({
					url:"selectOpportunityModal.so",
					data:{
						memberId : memberId
					},
					type:"post",
					success:function(data) {
						$('.ui.modal.opportunity').modal('show');
						console.log(data);
						$table_tbody = $('#result_Table_Modal tbody');
						$table_tbody.html('');
						for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].oppName
									+'<input type="hidden" value="'+data[i].oppId+'/'+data[i].clientId+'/'+data[i].company.comId+ '/' + data[i].processId+'">'
									+'<input type="hidden" value="'+data[i].oppName+'/'+data[i].agentName+'/'+data[i].company.comName+'/' + data[i].oppProgress +'"></td></tr>');
															
						}
						$('#result_Table_Modal td').bind('click',function(e){
							var id = e.target.childNodes[1].value.split('/');
							var name = e.target.childNodes[2].value.split('/');
							
							$('#company').val(name[2]);
							$('#company_hidden').val(id[2]);
							$('#client').val(name[1]);
							$('#client_hidden').val(id[1]);
							$('#opportunity').val(name[0]);
							$('#opportunity_hidden').val(id[0]);
							var estId = ${estId}+'';
							var ppsId = ${ppsId}+'';
							var a = '';
								a = '견적';
								$.ajax({
									url:'selectOppPrd.est',
									data:{oppId : id[0]},
									type:'post',
									success:function(data){
										 $table_tbody = $('#contract_Product_Table tbody');
										 $table_tfoot = $('#contract_Product_Table tfoot');
										 var totalQuantitiy = 0;
										 var totalPrice = 0;
										 if($table_tbody.children().length != 0){
											 $table_tbody[0].innerHTML = '';
											 $table_tfoot[0].innerHTML = ''; 
										}
										 $('.pdt_tr').empty();
										 for(var i=0; i<data.length; i++) {
											var pdt_tr = '<tr></tr>';
											
											var pdtName = '<td class="enroll_th"><input type="hidden" name="pdtId" value="'+data[i].PDT_ID+'">'+ data[i].PDT_NAME+'</td>';
											var quantity = '<td class="enroll_th">' + data[i].QUANTITY + '</td>';
											var standard = '<td class="enroll_th">' + data[i].STANDARD + '</td>';
											var price = '<td class="enroll_th">' + data[i].PRICE + '</td>';
											console.log(pdtName);
											
											 $('.pdt_tr').append('<tr></tr>');
											 $('.pdt_tr').find("tr:last-child").append(pdtName);
											 $('.pdt_tr').find("tr:last-child").append(quantity);
											 $('.pdt_tr').find("tr:last-child").append(standard);
											 $('.pdt_tr').find("tr:last-child").append(price);
											 $('.pdt_tr').find("tr:last-child").append('<td class="enroll_th"><i class="trash alternate outline icon center"/></td>');
										}
										 
										
									}
									
								});
							$('.ui.modal.opportunity').modal('hide');

							if(name[3] == 1){
								$('#pro_select').find('option').eq(1).prop('selected', true);
							}else if(name[3] == 2){
								$('#pro_select').find('option').eq(2).prop('selected', true);
							}else if(name[3] == 3){
								$('#pro_select').find('option').eq(3).prop('selected', true);
							}else if(name[3] == 4){
								$('#pro_select').find('option').eq(4).prop('selected', true);
							}
						});
					},
					error:function(status) {
						console.log(status);
					}
				})
				
				return false;
				break;
			}
			$(".exit_Btn").click(function() {
				$('.ui.modal.opportunity').modal('hide');
			})
		})
	</script>
</body>
</html>