<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
</head>
<body>
	<div class="ui modal privateDate">
		<div class="content_WindowCal">
			<div class="header_ModalCal">
				<div>
					<h2>
						개인일정<i class="exit_Btn times icon "></i>
					</h2> 
				</div>
			</div>
			<div class="contents_ModalCal">
				<div class="contents_Modal_DivCal">
					<form action="insertPriDate.sa" method="post">
						<input type="hidden" name="calId" value="0">
						<table class="htable">
							<colgroup>
								<col style="width:80px;">
								<col style="width:auto;">
							</colgroup>
							<tbody>
								<tr>
									<th>구분</th>
									<td class="label_space">
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category1" value="1"><label class="css-label-button ng-binding" for="category1">회의</label></span>
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category2" value="2"><label class="css-label-button ng-binding" for="category2">교육</label></span>
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category3" value="3"><label class="css-label-button ng-binding" for="category3">회식</label></span>
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category4" value="4"><label class="css-label-button ng-binding" for="category4">세미나</label></span>
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category5" value="5"><label class="css-label-button ng-binding" for="category5">업무</label></span>
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category6" value="6"><label class="css-label-button ng-binding" for="category6">휴가</label></span>
										<span><input class="css-checkbox" type="radio" name="calCategory" id="category7" value="7"><label class="css-label-button ng-binding" for="category7">기타</label></span>
									</td>
								</tr>
								<tr>
									<th>날짜</th>
									<td>
										<div class="ui calendar" id="standard_calendar">
										  <div class="ui input right icon">
										    <i class="calendar icon"></i>
										    <input type="text" placeholder="Date" name="calDate" autocomplete="off">
										  </div>
										</div>
									</td>
								</tr>
								<tr>
									<th>시간</th>
									
									<td>
		                               <div class="ui form">
										  <div class="two fields">
										    <div class="field">
										      <div class="ui calendar" id="rangestart">
										        <div class="ui input right icon">
										          <i class="time icon"></i>
										          <input type="text" placeholder="Start" name="calStart" autocomplete="off" name="calStart">
										        </div>
										      </div>
										    </div>
										    <div class="field">
										      <div class="ui calendar" id="rangeend">
										        <div class="ui input right icon">
										          <i class="time icon"></i>
										          <input type="text" placeholder="End" name="calEnd" autocomplete="off" name="calEnd">
										        </div>
										      </div>
										    </div>
										  </div>
										</div>
									</td>
								</tr>
								<tr>
									<th>활동내용</th>
									<td><textarea  cols="60" rows="7" name="calContent"></textarea></td>
								</tr>
							</tbody>	
						</table>
						<div class="priCalBtn">
							<button type="button" class="ui basic gray button" id="delPridate">삭제</button>
							<button class="ui basic gray button">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>

		$('#privateDate').on('click', function(){
			$(function(){
				$('#standard_calendar').calendar({
					type: 'date',
					formatter: {
		                date: function (date, settings) {
		                  if (!date) return '';
		                  var day = "";
		               		if(date.getDate() >= 1 && date.getDate() <= 9){
		                	  	day = '0' + date.getDate();
		                    }else{
		                    	day = date.getDate();
		                    }
		                  var month = "";
		                  if(date.getMonth() >= 0 && date.getMonth() <= 8){
		              	  	month = '0' + (date.getMonth() + 1);
		                  }else{
		                  	month = date.getMonth() + 1;
		                  }
		                  var year = date.getFullYear();
		                  return year + '-' + month + '-' + day;
		            	}
		            },
		            initialDate: new Date(2020,01,14),
		            on:'click'
				});
			    $('#rangestart').calendar({
			  	  type: 'time',
			  	  initialDate: new Date(2020,01,14),
			  	  endCalendar: $('#rangeend')
			  	});
			  	$('#rangeend').calendar({
			  	  type: 'time',
			  	  initialDate: new Date(2020,01,14),
			  	  startCalendar: $('#rangestart')
			  	});
			});
			if($('input[name=calId]').val() == 0){
		        jQuery('#delPridate').hide();  
			}else{
		        jQuery('#delPridate').show();								
			}
			$('textarea[name=calContent]').val('');
			$('input[name=calId]').val(0);			
			$('.ui.modal').css('width','600px');
			$('.ui.modal.privateDate').modal('show');
		});
		
		$(function(){
			$('#delPridate').on('click', function(){
				var calId = $('input[name=calId]').val();
				console.log('calId =' + calId);
				var result = confirm('정말 삭제하시겠습니까?');
				if(result){
					location.href="delPridate.sa?calId=" + calId;
				}else{
					
				}
			});
		});
		
	</script>
</body>
</html>