<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
<style>
	.procal div{
		width:inherit;
	}
</style>
</head>
<body>
<div class="ui modal product">
  <div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						제품 다중 선택<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal threeText">
						<tr >
							<td>
								<input type="text" id="searchName" name="pdtName">
							</td>
							<td>
								<input type="button" id="searchBtn" class="ui basic blue button	" value="조회">
							</td>
						</tr> 
					</table>
					<div class="result_Modal">
						<table class="result_Table right" id="result_Table_Modal">
							<thead>
								<tr>
									<th>선택</th>	
									<th>제품명</th>
									<th>포장수량</th>	
									<th>규격</th>
									<th>단가</th>	
								</tr>
							</thead>
							<tbody>	
							</tbody>
						</table>
					</div>
						<input type="button" value="추가" class="ui basic blue button" id="addBtn"> 
				</div>
			</div>
		</div>
</div>
<script>
	 $(function(){ $('.standard_calendar').calendar({
				type : 'date',
				formatter: {
					  date : function (date, settings) {
					        if (!date) return '';
					        var day = date.getDate();
					        var month = date.getMonth() + 1;
					        var year = date.getFullYear();
					        return year + '-' + month + '-' + day;
					        /* return day + '/' + month + '/' + year; */
					      }
					},
			});
		 //테이블 tfoo을 하나만 만들기 위한 것
		 var countTable = 0;
		 //테이블의 총 수량 
		 var totalQuantity = 0;
		 //제안금액 합계;
		 var totalPrice  = 0;
		 $("#addBtn").click(function(){
			 /* var b = $("input[type='checkbox']:checked") */
			 var b = $("input[name='chk']:checked")
			 console.log("b " + b);
			 for(var i = 0 ; i < b.length; i++){
				 console.log(b[i].parentNode.parentNode.childNodes[0]);
				 console.log(b[i].parentNode.parentNode.childNodes[1].innerHTML);
				 console.log(b[i].parentNode.parentNode.childNodes[2].innerHTML);
				 console.log(b[i].parentNode.parentNode.childNodes[3].innerHTML);
				 console.log(b[i].parentNode.parentNode.childNodes[4].innerHTML);
			 }
			 
			 if($("input:checkbox[name='chk']").is(":checked")){
				 console.log($("input:checkbox[name='chk']").parent().parent('tr'));
				 var tr = $("input:checkbox[name='chk']").is(":checked");
				 console.log(tr);
			 }
			 
			 $table_tbody = $('#contract_Product_Table tbody');
			 $table_tfoot = $('#contract_Product_Table tfoot');
			 $table_tr = $('<tr></tr>');
			 //총 수량 넣어둔 곳
			 totalQuantity = b.length;
			 for(var i=0; i< b.length; i++) {
				 /* var prId = b[i].parentNode.parentNode.childNodes[0].innerHTML; */
				 var prName = b[i].parentNode.parentNode.childNodes[1].innerHTML;
				 var quantity = b[i].parentNode.parentNode.childNodes[2].innerHTML;
				 var standard = b[i].parentNode.parentNode.childNodes[3].innerHTML;
				 console.log("stand : " +standard.split("<")[0]);
				 var stand = standard.split("<")[0];
				 console.log("id : " +standard.split("<")[1].split("\"")[3]);
				 var prId = standard.split("<")[1].split("\"")[3];
				 var price = b[i].parentNode.parentNode.childNodes[4].innerHTML;
				 var num = 1;
				 /* var $select = $('<select><option>옵션</option></select>'); */
				 var $input = $("<input>");
				 /* var $option = $("<option>" +"옵션"+"</option>");
				 
				 $select.append($option); */
				 var $input = '<input type="text" name="remark" value="">';
				 var $option1 = '<option value="ONCE">1회</option>';
				 var $option2 = '<option value="MONTH">월별</option>';
				 var $option3 = '<option value="QUARTER">분기별</option>';
				 var $option4 = '<option value="HALF">반기별</option>';		 
				 var $select = '<select name="revenuePeriod">'+ $option1 + $option2 + $option3 + $option4 +'</select>'; 
				 
				 var cal = '<div class="ui calendar standard_calendar procal" id="standard_calendar"> <div class="ui input right icon"> <i class="calendar icon"></i> <input name="revDate" type="text" readonly> </div></div>'
				 console.log($select);
					$table_tbody.before('<tr><td><input type="hidden" name="prId" value="'+prId +'">'+ prName +'</td><td>'+ price +'</td><td><input type="text" name="amount" value="'+ num +'"></td><td>' + (num * price) +'</td><td rowspan="2"><i class="trash alternate outline icon center"/></td></tr>' );
					$table_tbody.before('<tr><td>'+ $select +'</td><td>'+ cal +'</td><td colspan="2">'+ $input +'</td></tr>' );
					/* $table_tbody.append(
								'<tr><td colspan="2"><input type="hidden" name="prId" value="'+prId +'">'+prName+'</td><td><input type="text" value="'+comma(price)+'"></td><td><input type="text" value="0">%</td><td><input type="text" value="'
								+comma(price)+'"></td><td rowspan="2"><i class="trash alternate outline icon center"/></td></tr>')
					$table_tbody.append('<tr><td>'+quantity+'</td><td>'
							+standard+'</td><td><input type="text" value="1"></td><td colspan="2"><input type="text" value="'+comma(price)+'"></td></tr>')
							totalPrice = Number(totalPrice)+Number(price) */
			 }
			 	if(countTable == 0){
			 		$table_tfoot.append("<tr><th colspan='2'>수량합계</th><th>"+totalQuantity+"</th><th>제안금액 합계</th><th>"+comma(totalPrice)+"</th><th></th></tr>");
			 		countTable++;
			 	}
			 	/* changeTable() */
			 $('.standard_calendar').calendar({
					type : 'date',
					formatter: {
						  date : function (date, settings) {
						        if (!date) return '';
						        var day = date.getDate();
						        var month = date.getMonth() + 1;
						        var year = date.getFullYear();
						        return year + '-' + month + '-' + day;
						        /* return day + '/' + month + '/' + year; */
						      }
						},
				});
			 $('.ui.modal.product').modal('hide');
		 })
		 
		$("#searchBtn").click(function(){
			$.ajax({
				url:"search.pr",
				data:{pdtName:$("#searchName").val()},
				type:"get",
				success:function(data){
					 /* $('.ui.modal.product').modal('show');  */
					$table_tbody = $('#result_Table_Modal tbody');
					$table_tbody.html('');
					console.log("dl" + data);
					 for(var i=0; i<data.length; i++) {
						$table_tbody.append('<tr><td><input name="chk" type="checkbox"></td><td>'+data[i].pdtName+'</td><td>'
								+data[i].quantity +'</td><td>'
								+data[i].standard+'<input type="hidden" value="'+data[i].pdtId+'"> </td><td>'+data[i].price+'</td></tr>' );
					 } 
					 
					/*  $table_tbody.click(function(e){
						 var name = e.target.parentElement.children[1].innerText;
						 var id = e.target.parentElement.children[1].children[0].value;
						 $('#managerText').val(name);
						 $('#manager_hidden').val(id);
						 $('.ui.modal.manager').modal('hide');
					 })  */
					 console.log(data);
				},
				error:function(status){
					
				}
			})
		})
		
	}) 
	 $(document).on('click','.trash',function(){
			var removetr = 	$(this).parent().parent()[0]
			var removetr2 = $(this).parent().parent()[0].nextSibling;
			var rowIndex  = $(this).parent().parent().eq(0)[0].rowIndex;
			removetr.remove();
			removetr2.remove();
			changeTable();
		})
	function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
		/* $(document).click(function(e){
			var btnName = (e.target.id);
			switch(btnName) {
			case 'manager':
				$.ajax({
					url:"selectEmployeeModal.em",
					data:{},
					type:"post",
					success:function(data){
						$('.ui.modal.manager').modal('show');
						$table_tbody = $('#result_Table_Modal tbody');
						$table_tbody.html('');
						 for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].deptName+'</td><td>'+data[i].empName+'<input type="hidden" value="'+
									data[i].empNo+'"></td>');
						 } 
						 $table_tbody.click(function(e){
							 var name = e.target.parentElement.children[1].innerText;
							 var id = e.target.parentElement.children[1].children[0].value;
							 $('#managerText').val(name);
							 $('#manager_hidden').val(id);
							 $('.ui.modal.manager').modal('hide');
						 })
						 console.log(data);
					},
					error:function(status){
						
					}
				})
				return false;
				break;
			}
		}) */
	</script>
</body>
</html>