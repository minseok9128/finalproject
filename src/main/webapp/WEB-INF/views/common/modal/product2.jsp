<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
<div class="ui modal product2">
  <div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						제품 다중 선택<i class="exit_Btn times icon "></i>
					</h1>
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal threeText">
						<tr >
							<td>
								<input type="text" id="searchName" name="pdtName">
							</td>
							<td>
								<input type="button" id="searchBtn" class="ui basic blue button	" value="조회">
							</td>
						</tr> 
					</table>
					<div class="result_Modal">
						<table class="result_Table right" id="result_Table_Modal">
							<thead>
								<tr>
									<th>선택</th>	
									<th>제품명</th>
									<th>포장수량</th>	
									<th>규격</th>
									<th>단가</th>	
								</tr>
							</thead>
							<tbody>	
							</tbody>
						</table>
					</div>
						<input type="button" value="추가" class="ui basic blue button" id="addBtn"> 
				</div>
			</div>
		</div>
</div>
<script>
	$(function(){
		//console.log($(".ui.modal.product2").is(:visible));
		
	})

	 $(function(){ $('.standard_calendar').calendar({
				type : 'date',
				formatter: {
					  date : function (date, settings) {
					        if (!date) return '';
					        var day = date.getDate();
					        var month = date.getMonth() + 1;
					        var year = date.getFullYear();
					        return year + '-' + month + '-' + day;
					      }
					},
			});
		 //테이블의 총 수량 
		 var totalQuantity = 0;
		 //제안금액 합계;
		 var totalPrice  = 0;
		 $("#addBtn").off().on('click',function(){
			 var b = $("input[name='chk']:checked")
			 
			 if($("input:checkbox[name='chk']").is(":checked")){
				 var tr = $("input:checkbox[name='chk']").is(":checked");
			 }
			 $table_tbody = $('#contract_Product_Table tbody');
			 $table_tfoot = $('#contract_Product_Table tfoot');
			 $result_Table_Modal = $('#result_Table_Modal tbody'); 
			 $table_tr = $('<tr></tr>');
			 //총 수량 넣어둔 곳
			 for(var i=0; i< b.length; i++) {
				 var prName = b[i].parentNode.parentNode.childNodes[1].innerHTML;
				 var quantity = b[i].parentNode.parentNode.childNodes[2].innerHTML;
				 var standard = b[i].parentNode.parentNode.childNodes[3].innerHTML;
				 var stand = standard.split("<")[0];
				 var prId = standard.split("<")[1].split("\"")[3];
				 var price = b[i].parentNode.parentNode.childNodes[4].innerHTML;
				 
				
				 
				 var same = false;
				 for(var j=0; j<$($table_tbody).children().length; j++) 	{
					 if(j%2 == 0){
			 		 console.log($($table_tbody).children().eq(j).children().eq(0).children().eq(0).val());
			 		 console.log(prName);
					 	if($($table_tbody).children().eq(j).children().eq(0).children().eq(0).val() == prName){
					 		same = true;
					 	} 
					 }
				 }
				 if(same == false){
					 $table_tbody.append(
								'<tr><td colspan="2">'+prName+'<input type="hidden" name="pdtName" value="'+prName+'"><input type="hidden" name="pdtId" value="'+prId+'"></td>'+
								'<td><input type="text" value="'+comma(price)+'"><input type="hidden" name="price" value="'+price+'"></td>'+
								'<td><input type="text" name="discountRate" value="0">%</td>'+
								'<td><input type="text" value="'+comma(price)+'"><input type="hidden" name="detailPrice" value="'+Math.round(price)+'"></td>'+
								'<td rowspan="2"><i class="trash alternate outline icon center"/></td></tr>')
					$table_tbody.append(
							'<tr><td>'+comma(quantity)+'</td>'+
							'<td>'+standard+'</td><td><input type="text" name="quantity" value="1"></td>'+
							'<td colspan="2"><input type="text" value="'+comma(price)+'"></td></tr>')
							
							totalQuantity = Number(totalQuantity)+Number(price)
							$result_Table_Modal.html('');
							
							if($table_tbody[0].rows.length == 2){
						 		$table_tfoot.append("<tr><th colspan='2'>수량합계</th><th>"+comma(totalQuantity)+"</th><th>제안금액 합계</th><th>"+comma(totalQuantity)+"</th><th></th></tr>");
						 	} 
							
				 } else {
					 alert("["+prName+"] 동일 품목이 있습니다.")
				 }
				
			 }
			 $('.standard_calendar').calendar({
					type : 'date',
					formatter: {
						  date : function (date, settings) {
						        if (!date) return '';
						        var day = date.getDate();
						        var month = date.getMonth() + 1;
						        var year = date.getFullYear();
						        return year + '-' + month + '-' + day;
						      }
						},
				});
			 $('.ui.modal.product2').modal('hide');
				 changeTable();
		 })
		 
		$("#searchBtn").click(function(){
			$.ajax({
				url:"search.pr",
				data:{
					pdtName:$("#searchName").val()
				},
				type:"get",
				success:function(data){
					 /* $('.ui.modal.product').modal('show');  */
					$table_tbody = $('#result_Table_Modal tbody');
					$table_tbody.html('');
					 for(var i=0; i<data.length; i++) {
						$table_tbody.append('<tr><td><input name="chk" type="checkbox"></td><td>'+data[i].pdtName+'</td><td>'
								+data[i].quantity +'</td><td>'
								+data[i].standard+'<input type="hidden" value="'+data[i].pdtId+'"> </td><td>'+data[i].price+'</td></tr>' );
					 } 
					 
				},
				error:function(status){
				}
			})
		})
		
	}) 
	
		//삭제 버튼 클릭하면 발동하는 함수
		$(document).on('click','#contract_Product_Table .trash',function(e){
				var tbody = $('#contract_Product_Table tbody').children().length;
				var removetr = 	e.target.parentNode.parentNode
				var removetr2 = e.target.parentNode.parentNode.nextElementSibling
				
				var result = confirm("정말 삭제하시겠습니까?")
				
				if(result == true){
					$('#contract_Product_Table thead').append('<input type="hidden" name="deleteProductId" value="'+removetr.children[0].children[0].value+'">');
					removetr.remove();
					removetr2.remove();
					//삭제 버튼 클릭시 input으로 모으기
					changeTable();
					if($('#contract_Product_Table tbody')[0].rows.length == 0){
						$('#contract_Product_Table tbody').parent().children().eq(2).html('');
					}
				}
		})
		//품목 테이블이 변하면 수량합계, 제안금액 합계 숫자를 추가하는 함수
		function changeTable() {
				var tfoot = $('#contract_Product_Table tfoot').children().children();
				var tableRowlength = $("#contract_Product_Table").children().eq(1).children().length;
				var allTotal = 0;
				var totalQuantity = 0;
				 for(var i=0; i<tableRowlength; i++) {
					if(i%2 == 0){
					} else {
						//제안금액
						var total = changeNumber($("#contract_Product_Table").children().eq(1).children().eq(i).children().eq(3).children().val());
						allTotal += (total);
						totalQuantity += changeNumber($("#contract_Product_Table").children().eq(1).children().eq(i).children().eq(2).children().val());
					}
				} 	
			
				tfoot[1].innerHTML = '';
				tfoot[1].innerHTML = comma(totalQuantity);
				tfoot[3].innerHTML = '';
				tfoot[3].innerHTML = comma(allTotal);
				
				 if($('.product_add').is($('#estBtn')) == true){
					//전체 제안금액	 
					var low = $('#contract_Product_Table tfoot').children().children().eq(1);
					var price = 0; //수량
					var allSupplyValue = 0; //전체 공급가액
					var suggestionSupplyValue = 0;//제안 공급가액
					var text = 0;//세액
					var total = 0; //합계
					var inputPrice = $('#price');
					var inputAllSupplyValue = $('#totalValueOfSupply');
					var inputSuggestionSupplyValue =$('#suggestionValueOfSupply') ;
					var inputTex = $('#tex');
					var inputTotal = $('#total');
					if($('#VATCheckBox').is(':checked')){
						price = changeNumber($('#contract_Product_Table tfoot').children().children()[1].outerText);
						allSupplyValue = changeNumber($('#contract_Product_Table tfoot').children().children()[3].outerText).toFixed(0);
						text = Number((allSupplyValue/1.1) - allSupplyValue).toFixed(0);
						total = (allSupplyValue*1.1).toFixed(0);
						inputPrice.val(comma(Number(price)));
						inputAllSupplyValue.val(comma(Number(allSupplyValue-Math.abs(text))));
						inputSuggestionSupplyValue.val(comma(Number(allSupplyValue-Math.abs(text))));
						inputTex.val(comma(Math.abs(Number(text))));
						inputTotal.val(comma(Number(allSupplyValue)));						
					} else {
						price = changeNumber($('#contract_Product_Table tfoot').children().children()[1].outerText);
						allSupplyValue = changeNumber($('#contract_Product_Table tfoot').children().children()[3].outerText).toFixed(0);
						text = Number((allSupplyValue* 1.1) - allSupplyValue).toFixed(0);
						total = (allSupplyValue*1.1).toFixed(0);
						inputPrice.val(comma(Number(price)));
						inputAllSupplyValue.val(comma(Number(allSupplyValue)));
						inputSuggestionSupplyValue.val(comma(Number(allSupplyValue)));
						inputTex.val(comma(Number(text)));
						inputTotal.val(comma(Number(total)));
					}
					$('#VATCheckBox').click(function(){
						if($('#VATCheckBox')[0].checked == true){
							price = changeNumber($('#contract_Product_Table tfoot').children().children()[1].outerText);
							allSupplyValue = changeNumber($('#contract_Product_Table tfoot').children().children()[3].outerText).toFixed(0);
							text = Number((allSupplyValue/1.1) - allSupplyValue).toFixed(0);
							total = (allSupplyValue*1.1).toFixed(0);
							inputPrice.val(comma(Number(price).toFixed(0)));
							inputAllSupplyValue.val(comma(Number(allSupplyValue-Math.abs(text)).toFixed(0)));
							inputSuggestionSupplyValue.val(comma(Number(allSupplyValue-Math.abs(text)).toFixed(0)));
							inputTex.val(comma(Math.abs(Number(text).toFixed(0))));
							inputTotal.val(comma(Number(allSupplyValue).toFixed(0)));
						} else {
							price = changeNumber($('#contract_Product_Table tfoot').children().children()[1].outerText);
							allSupplyValue = changeNumber($('#contract_Product_Table tfoot').children().children()[3].outerText).toFixed(0);
							text = Number((allSupplyValue* 1.1) - allSupplyValue).toFixed(0);
							total = (allSupplyValue*1.1).toFixed(0);
							inputPrice.val(comma(Number(price)));
							inputAllSupplyValue.val(comma(Number(allSupplyValue)));
							inputSuggestionSupplyValue.val(comma(Number(allSupplyValue)));
							inputTex.val(comma(Number(text)));
							inputTotal.val(comma(Number(total)));
						}
					})
				}
				
				//견적 등록 수량 전체 공급가액, 제안 공급가액, 세액, 합계를 추가하는 것
			}
	 
	 //품목에 단가 할인 수량을 키보드로 입력하면 변하는 함수
	 $(document).keypress(function(e){
		 var tfoot = $('#contract_Product_Table tfoot').children().children();
		 
		  if(e.target.parentNode.offsetParent.id =='contract_Product_Table'){
			  //change
			  $(e.target).off().on('propertychange keyup paste',function(e) {
				  	var unitPrice = 0;
					var cellIndex = $(this).parent()[0].cellIndex;
					var low = $(this).parents().eq(2)[0].rows.length;
					var totalUnitPrice = 0; // 각 개인의 제안금액
					var sum = 0; // 합계 할인금액 안 더한거
					var dc = 0; //할인
					var total = 0; // 합게 할인금액 더한거
					var price = 0; // 수량
					var totalQuantity = 0; // 총 수량
					var propoSalamount = 0; //제안금액 합계
					
					for(var i=0; i<low; i++) {
						if(i%2 == 0){
							 totalUnitPrice += changeNumber($(this).parents().eq(2).children().eq(i).children().eq(1).children().val())
							 sum = changeNumber($(this).parents().eq(2).children().eq(i).children().eq(1).children().val())
							 dc =  changeNumber($(this).parents().eq(2).children().eq(i).children().eq(2).children().val())/100
							 total += sum - sum*dc;
							 $(this).parents().eq(2).children().eq(i).children().eq(3).children().eq(0).val(comma(Math.round(sum - sum*dc)));
							 $(this).parents().eq(2).children().eq(i).children().eq(3).children().eq(1).val(Math.round((sum - sum*dc)));
						} else {
							price = changeNumber($(this).parents().eq(2).children().eq(i).children().eq(2).children().val());
							totalQuantity += Number(price);
							//제안금액 값 입력
							$(this).parents().eq(2).children().eq(i).children().eq(3).children().val(comma(Math.round((sum - (sum*dc)) * price)));
							propoSalamount += changeNumber($(this).parents().eq(2).children().eq(i).children().eq(3).children().val());
						}
					}
					tfoot[1].innerHTML = '';
					tfoot[1].innerHTML = comma(totalQuantity.toFixed(0));
					tfoot[3].innerHTML = '';
					tfoot[3].innerHTML = comma(propoSalamount.toFixed(0));
					if($('.product_add').is($('#estBtn')) == true){
						//전체 제안금액	 
						var low = $('#contract_Product_Table tfoot').children().children().eq(1);
						var Quantity = 0; //수량
						var allSupplyValue = 0; //전체 공급가액
						var suggestionSupplyValue = 0;//제안 공급가액
						var text = 0;//세액
						var total = 0; //합계
						var inputPrice = $('#price');
						var inputAllSupplyValue = $('#totalValueOfSupply');
						var inputSuggestionSupplyValue =$('#suggestionValueOfSupply') ;
						var inputTex = $('#tex');
						var inputTotal = $('#total');
						if($('#VATCheckBox').is(':checked')){
							Quantity = changeNumber($('#contract_Product_Table tfoot').children().children()[1].outerText);
							allSupplyValue = changeNumber($('#contract_Product_Table tfoot').children().children()[3].outerText).toFixed(0);
							text = Number((allSupplyValue/1.1) - allSupplyValue).toFixed(0);
							total = (allSupplyValue*1.1).toFixed(3);
							inputPrice.val(comma(Number(Quantity)));
							inputAllSupplyValue.val(comma(Number(allSupplyValue-Math.abs(text)).toFixed(0)));
							inputSuggestionSupplyValue.val(comma(Number(allSupplyValue-Math.abs(text)).toFixed(0)));
							inputTex.val(comma(Math.abs(Number(text))));
							inputTotal.val(comma(Number(allSupplyValue)));						
						} else {
							Quantity = changeNumber($('#contract_Product_Table tfoot').children().children()[1].outerText);
							allSupplyValue = changeNumber($('#contract_Product_Table tfoot').children().children()[3].outerText).toFixed(0);
							text = Number((allSupplyValue* 1.1) - allSupplyValue).toFixed(0);
							total = (allSupplyValue*1.1).toFixed(0);
							inputPrice.val(comma(Number(totalQuantity)));
							inputAllSupplyValue.val(comma(Number(allSupplyValue)));
							inputSuggestionSupplyValue.val(comma(Number(allSupplyValue)));
							inputTex.val(comma(Number(text)));
							inputTotal.val(comma(Number(total)));
						}
						
					}
				 })
				 $(e.target).change( function(){
					 $(e.target).val(comma(changeNumber($(e.target).val())));	
				 })
		 } 
	 }) 
	 function comma(str) {
	        str = String(str);
	        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	    }
	 
	 function changeNumber(a) {
			var StringA = a.split(',');
			var numberA = '';
			for(var i=0; i<StringA.length; i++) {
				numberA += StringA[i];
			}
			return Number(numberA);
		}
</script>
</body>
</html>