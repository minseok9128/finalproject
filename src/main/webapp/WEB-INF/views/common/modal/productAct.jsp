<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
<style>
	.procal div{
		width:inherit;
	}
</style>
</head>
<body>
<div class="ui modal product">
  <div class="content_Window">
			<div class="header_Modal">
				<div>
					<h1>
						제품 다중 선택<i class="exit_Btn times icon "></i>
						
					</h1>
				</div>
			</div>
			<div class="contents_Modal">
				<div class="contents_Modal_Div">
					<table class="table_modal threeText">
						<tr >
							<td>
								<input type="text" id="searchName" name="pdtName">
							</td>
							<td>
								<input type="button" id="searchBtn" class="ui basic blue button	" value="조회">
							</td>
						</tr> 
					</table>
					<div class="result_Modal">
						<table class="result_Table right" id="result_Table_Modal">
							<thead>
								<tr>
									<th>선택</th>	
									<th>제품명</th>
									<th>포장수량</th>	
									<th>규격</th>
									<th>단가</th>	
								</tr>
							</thead>
							<tbody>	
							</tbody>
						</table>
					</div>
						<input type="button" value="추가" class="ui basic blue button" id="addBtn"> 
				</div>
			</div>
		</div>
</div>
<script>
	 $(function(){ 
		 //테이블 tfoo을 하나만 만들기 위한 것
		 var countTable = 0;
		 //테이블의 총 수량 
		 var totalQuantity = 0;
		 //제안금액 합계;
		 var totalPrice  = 0;
		 $("#addBtn").click(function(){
			 $('#result_Table_Modal tbody tr').each(function(){
				 if($(this).find($('input[name=chk]')).prop('checked')){
					 //$('.pdt_tr').append($(this));
					 console.log($(this).find('td').eq(0).siblings('td'));
					 $('.pdt_tr').append('<tr></tr>');
					 $('.pdt_tr').find("tr:last-child").append($(this).find('td').eq(0).siblings('td').addClass('enroll_th'));
					 $('.pdt_tr').find("tr:last-child").append('<td class="enroll_th"><i class="trash alternate outline icon center"/></td>');
				 }
			 });
			
			$('.ui.modal.product').modal('hide');
		 })
		 
		$("#searchBtn").click(function(){
			$.ajax({
				url:"search.pr",
				data:{pdtName:$("#searchName").val()},
				type:"get",
				success:function(data){
					 /* $('.ui.modal.product').modal('show');  */
					$table_tbody = $('#result_Table_Modal tbody');
					$table_tbody.html('');
					console.log("dl" + data);
					 for(var i=0; i<data.length; i++) {
						$table_tbody.append('<tr><td><input name="chk" type="checkbox"></td><td>'+data[i].pdtName+'</td><td>'
								+data[i].quantity +'</td><td>'
								+data[i].standard+'<input type="hidden" name="pdtId" value="'+data[i].pdtId+'"> </td><td>'+data[i].price+'</td></tr>' );
					 } 
					 
					/*  $table_tbody.click(function(e){
						 var name = e.target.parentElement.children[1].innerText;
						 var id = e.target.parentElement.children[1].children[0].value;
						 $('#managerText').val(name);
						 $('#manager_hidden').val(id);
						 $('.ui.modal.manager').modal('hide');
					 })  */
					 console.log(data);
				},
				error:function(status){
					
				}
			})
		})
		
	}) 
	 $(document).on('click','.trash',function(){
			$(this).parents("tr").remove();
		})
	function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
		/* $(document).click(function(e){
			var btnName = (e.target.id);
			switch(btnName) {
			case 'manager':
				$.ajax({
					url:"selectEmployeeModal.em",
					data:{},
					type:"post",
					success:function(data){
						$('.ui.modal.manager').modal('show');
						$table_tbody = $('#result_Table_Modal tbody');
						$table_tbody.html('');
						 for(var i=0; i<data.length; i++) {
							$table_tbody.append('<tr><td>'+data[i].deptName+'</td><td>'+data[i].empName+'<input type="hidden" value="'+
									data[i].empNo+'"></td>');
						 } 
						 $table_tbody.click(function(e){
							 var name = e.target.parentElement.children[1].innerText;
							 var id = e.target.parentElement.children[1].children[0].value;
							 $('#managerText').val(name);
							 $('#manager_hidden').val(id);
							 $('.ui.modal.manager').modal('hide');
						 })
						 console.log(data);
					},
					error:function(status){
						
					}
				})
				return false;
				break;
			}
		}) */
	</script>
</body>
</html>