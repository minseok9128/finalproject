<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/modal.css">
</head>
<body>
<div class="ui modal productEnrollment">
  <div class="content2" id="productEnrollment_Modal">
  	<div id="header"><h1>제품등록<i class="times icon exit_Btn" id="exit_Btn5"></i></h1></div>
    <div id="contain">
    	<div>
			<h2>정보 입력</h2>    		
		</div>
		<table class="table_Modal" id="table_productEnrollment">
			<tr>
				<td>제품코드</td>
				<td><input type="text" class="common_Text1_Modal"></td>
			</tr>
		</table>
    	</div>
    </div>
  </div>
</div>
</body>
</html> 