<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.half {
	width: 49.5%;
	display: inline-block;
}

.select, .input {
	display: inline-block;
}

.one input {
	width: 100%;
}

.one {
	padding-top: 10px;
	padding-left: 10px;
}

.select {
	width: 49.5%;
	padding-left: 10px;
}

.search div input {
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
	padding-left: 5px;
}

.select select {
	width: 100%;
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
}

.input input {
	width: 78%;
	border-radius: .28571429rem;
	border: 1px solid #D7DBDF;
	height: 36px;
}

.input {
	width: 49.5%;
	padding-left: 9.5px;
}

.ui.blue.basic.button {
	margin-right: 0;
}

.float {
	float: right;
}

.secondSearchDiv {
	margin-top: 10px;
}

.oneFifth {
	width: 20%;
	display: inline-block;
	padding-left: 10px;
}

.oneFifth select {
	width: 100%;
	height: 36px;
	/* margin-left:10px; */
}

.fourFifth {
	width: 79%;
	display: inline-block;
}
.oneThird {
	width: 33%;
	display: inline-block;
}
.fLeft {
	float: left;
}
.one .oneThird .input{
	padding-left: 0;
	padding-right: 5px;
}
</style>
</head>
<body>
	<jsp:include page="menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div class="search">
				<div class="1st_line lane lane2 lane3">
					<div class="select">
						<select>
							<option>영업팀</option>
						</select>
					</div>
					<div class="input">
						<input type="text" placeholder="담당자">
						<button class="ui blue basic button float">검색</button>
					</div>
				</div>
				<div class="secondSearchDiv">
					<div class="ui input right icon" id="rangestart">
						<i class="calendar icon"></i> <input type="text"
							placeholder="Start">
					</div>

					<div class="ui input right icon" id="rangeend">
						<i class="calendar icon"></i> <input type="text" placeholder="End">
					</div>
				</div>
			</div>
			<br>
			<hr>
			<div class="search">
				<div class="one">
					<input type="text" placeholder="유선번호/고객/고객사">
				</div>
				<div class="secondSearchDiv">
					<div class="select half">
						<select>
							<option>영업부</option>
						</select>
					</div>
					<div class="half input">
						<input type="text">
						<button class="ui blue basic button float">검색</button>
					</div>
				</div>
			</div>
			<br>
			<hr>
			<div class="search">
				<div>
					<div class="oneFifth">
						<select>
							<option>시작일</option>
						</select>
					</div>
					<div class="fourFifth">
						<div class="ui input right icon" id="rangestart">
							<i class="calendar icon"></i> <input type="text"
								placeholder="Start">
						</div>

						<div class="ui input right icon" id="rangeend">
							<i class="calendar icon"></i> <input type="text"
								placeholder="End">
						</div>
					</div>
				</div>
			</div>
			<br>
			<hr>
			<div>
				<button class="ui basic black button">등록</button>
				<button class="ui basic blue button">검색</button>
				<br>
				<hr>
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i> <input type="text"
						placeholder="고객사">
				</div>
			</div>
			<div class="secondSearchDiv">
				<div class="select">
					<select>
						<option>영업팀</option>
					</select>
				</div>
					<div class="input">
					<input type="text" placeholder="담당자">
					<button class="ui blue basic button float">검색</button>
				</div>
			</div>
			<br>
			<hr>
			<div class="search">
				<div class="one">
					<div class="oneThird">
						<div class="ui input right icon" id="rangestart">
							<i class="calendar icon"></i> <input type="text"
								placeholder="Start">
						</div>
						<div class="ui input right icon fLeft" id="rangeend">
							<i class="calendar icon"></i> <input type="text"
								placeholder="End">
						</div>
					</div>
					<div class="ui input right icon oneThird">
						<i class="ellipsis vertical icon"></i> <input type="text"
							placeholder="고객사">
					</div>
					<div class="ui input right icon oneThird">
						<i class="ellipsis vertical icon"></i> <input type="text"
							placeholder="고객사">
					</div>
				</div>
			</div>
			<div class="secondSearchDiv">
				<div class="ui input right icon oneThird">
					<i class="ellipsis vertical icon"></i> <input type="text"
						placeholder="고객사">
				</div>
				<div class="select oneThird">
					<select>
						<option>영업팀</option>
					</select>
				</div>
				<div class="ui input right icon oneThird">
					<i class="ellipsis vertical icon"></i> <input type="text"
						placeholder="고객사">
				</div>
			</div>
			<div>
				
			</div>
		</div>
	</div>
	</div>
</body>
</html>