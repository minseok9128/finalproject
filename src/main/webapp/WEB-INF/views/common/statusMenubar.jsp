<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
	<main id="container">
            <div class="inner_ct">
                <div class="menu">
                    <ul class="menu_cnt">
                        <li class="main_menu"><a href="showClientStatus.cs">고객현황</a></li>
                        <li class="main_menu"><a href="selectStatus.so">기회현황</a></li>
                        <li class="main_menu"><a href="showContractStatus.scs">계약현황</a></li>
                        <li class="main_menu"><a href="showActivityStatus.sas">활동현황</a></li>
                    </ul>
                </div>
                
    <script>
		$(function(){
			$('.nav').children().removeClass('navClick');
			$('.nav').children().eq(2).addClass('navClick');
		});
    </script>
</body>
</html>