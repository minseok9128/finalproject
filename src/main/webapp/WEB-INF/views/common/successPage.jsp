<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <!-- jquery -->
 <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<script>
	$(function(){
		var msg = '${msg}';
		var code = '${code}';
		if(code == 'completeAct'){
			alert('${msg}');
			location.href='actCalendar.sa';		
		}
		if(code == 'deleteAct'){
			alert('${msg}');
			location.href='actCalendar.sa';
		}
	});
</script>
</body>
</html>