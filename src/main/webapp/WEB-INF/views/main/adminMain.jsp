<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
</head>
<body>
	<jsp:include page="../common/adminMenubar.jsp"/>
		<!-- 여기서 코드작성 -->
		<div class="main_ctn">
                   <div class="total">
                            <div class="w_half f_left">	
                                <h2>고객</h2>
                                <p class="datehard">합계</p>
                                <div class="customer zoom">
                                    <div class="cus">
                                        <div class="round">
                                            <i class="ico "></i>
                                            <strong><c:out value="${ adminInfo.companyCount }"/></strong>
                                        </div>
                                        <p class="cus_title">고객사</p>
                                    </div>
                                    <div class="com">
                                        <div class="round">
                                            <i class="ico"></i>
                                            <strong><c:out value="${ adminInfo.clientCount }"/></strong>
                                        </div>
                                        <p class="cus_title">고객</p>
                                    </div>
                                    <div class="coms">
                                        <div class="round">
                                            <i class="ico"></i>
                                            <strong><c:out value="${ adminInfo.leadCount }"/></strong>
                                        </div>
                                        <p class="cus_title">잠재고객</p>
                                    </div>
                                </div>
                            </div><!-- //f_left -->
                            <div class="f_right w_half">
                                <h2>영업기회/활동</h2>
                                <!-- <p class="datehard">2020.01.12</p> -->								
                                <div class="opact zoom">
                                    <div class="op">
                                        <h3>영업기회</h3>
                                        <ul>
                                            <li>
                                                진행중 <em><c:out value="${ adminInfo.oppCount[0].COUNT }"/></em>
                                            </li>
                                            <li>
                                                성공 <em><c:out value="${ adminInfo.oppCount[1].COUNT }"/></em>
                                            </li>
                                            <li>
                                                실패 <em><c:out value="${ adminInfo.oppCount[2].COUNT }"/></em>
                                            </li>
                                            <li>
                                                보류 <em><c:out value="${ adminInfo.oppCount[3].COUNT }"/></em>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="act">
                                        <h3>활동</h3>
                                        <ul>
                                            <li>
                                                계획 <em>13</em>
                                            </li>
                                            <li>
                                                완료 <em>21</em>
                                            </li>
                                            <li>
                                                평균<em>17</em>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- //f_right -->
                            <div class="clearfix"></div>
                            
                            <div class="f_left w_half">
                                <h2>영업현황</h2>
                                <p class="datehard"><span>(단위 : 백만원)</span></p> <!-- (단위 : 만원, 성공확률 50% 이상) -->	
                                <div class="shclear zoom">
                                    <div class="left">
                                        <div class="shclear_pos">
                                            <h3>평균 (3개월)</h3>
                                            <div class="shbox">
                                                <div class="shtxt">
                                                    <p>목표</p>
                                                    <strong>50</strong>
                                                </div>
                                                <div class="shtxt">
                                                    <p>실적</p>
                                                    <strong>35.2</strong>
                                                </div>
                                                <div class="shtxt">
                                                    <p>달성</p>
                                                    <strong class="point">70.4<span class="subtxt">%</span></strong>
                                                </div>
                                            </div><!-- //shbox -->
                                        </div><!-- //shclear_pos -->
                                        
                                        <div class="shclear_pos">
                                            <h3>년</h3>
                                            <div class="shbox">
                                                <div class="shtxt">
                                                    <p>목표</p>
                                                    <strong>200</strong>
                                                </div>
                                                <div class="shtxt">
                                                    <p>실적</p>
                                                    <strong>40</strong>
                                                </div>
                                                <div class="shtxt">
                                                    <p>달성</p>
                                                    <strong class="point">20<span class="subtxt">%</span></strong>
                                                </div>
                                            </div><!-- //shbox -->
                                        </div><!-- //shclear_pos -->
                                    </div><!-- //left -->					
                                    <div class="right">
                                        <div class="shclear_pos">
                                            <h3>당월</h3>
                                            <ul>
                                                <li>
                                                    <em>목표</em>
                                                    <strong>20</strong>
                                                </li>
                                                <li>
                                                    <em>실적</em>
                                                    <strong>17</strong>
                                                </li>
                                                <li>
                                                    <em>달성</em>
                                                    <strong class="point">85<span class="subtxt">%</span></strong>
                                                </li>
                                            </ul>
                                        </div><!-- //shclear_pos -->
                                    </div><!-- //right -->				
                                </div>
                            </div><!-- //f_left -->
                    </div>


		        </div>
		       
            </div>
	    </main>
    </div>
</body>
<script>

</script>
</html>