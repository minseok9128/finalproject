<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
        <!-- chart.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
		<!-- 여기서 코드작성 -->
		<div class="main_ctn">
                   <div class="content">
                        <div class="chartArea">
                            <div class="change_chart">
	                            <ul class="tabs tab_activity">
	                                <li style="width:50%"><a class="active" style="cursor:default">당월 매출 달성별 사원 순위</a></li>
	                                <li style="width:50%"><a style="cursor:pointer" x-ng-click="moveTab('L');" href="actListTable.sa">당월 계약 달성 건수별 사원 순위</a></li>
	                            </ul>
                            </div>
                            <div class="view_chart">
                                <div class="chart1">
                                    <canvas id="myChart" width="100%" height="100%"></canvas>
                                </div>
                                <div class="chart2">
                                	<table class="rankTable">
                                		<tr>
	                                		<th>순위</th>
	                                		<th>매출액</th>
	                                		<th>이름</th>
                                		</tr>
                                		<tbody>
                                		<c:forEach items="${ revRank }" var="rev" varStatus="status"> 
                                			<tr class="rankTr">
                                				<td><c:out value="${ status.count }"/></td>
                                				<td><fmt:formatNumber value="${ rev.TOTAL }" pattern="#,###.##" /> </td>
                                				<td><c:out value="${ rev.EMP_NAME }"/><input type="hidden" value="${ rev.EMP_NO }"></td>
                                			</tr>
                                		</c:forEach>
                                		<tr>
                                			<td></td>
                                			<td><img class="dot" src="${ contextPath }/resources/images/dot.png"> </td>
                                			<td></td>
                                		</tr>
                                		<tr class="myTr">
                                			<td><c:out value="${ myRank.RNUM }"></c:out> </td>
                                			<td><fmt:formatNumber value="${ myRank.TOTAL }" pattern="#,###.##" /> </td>
                                			<td><c:out value="${ myRank.EMP_NAME }"/><input type="hidden" value="${ myRank.EMP_NO }"> </td>
                                		</tr>
                                		</tbody>
                                	</table>
                                    <span class="myRankSpan">나의순위 : <c:out value="${ myRank.RNUM }"></c:out>위</span>
                                </div>
                            </div>
                            <script>
                            	$(function(){
                            		$('.rankTr').each(function(){
                            			$(this).removeClass('rankTr');
                            			var revId = $(this).children().find($('input')).val();
                            			console.log(revId);
                            			var myId = $('.myTr').find($('input')).val();
                            			if(revId == myId){
                            				$(this).addClass('myTr');
                            			}
                            		});
                            	});
                                var ctx = document.getElementById("myChart"); //캔버스 id값 가져오기
                                var first = '${revRank[0].EMP_NAME}';
                                var second = '${revRank[1].EMP_NAME}';
                                var third = '${revRank[2].EMP_NAME}';
                                var four = '${revRank[3].EMP_NAME}';
                                var five = '${revRank[4].EMP_NAME}';
                                var revFirst = '${revRank[0].TOTAL}'
                                console.log(revFirst);
                                var myChart = new Chart(ctx, {
                                type: 'bar', //그래프 형태 지정하기
                                data: {
                                	labels: [first, second, third, four, five ], //X축 제목
                                    datasets: [{
                                        label: '월별매출합',
                                        data: [revFirst,  '${revRank[1].TOTAL}','${revRank[2].TOTAL}', '${revRank[3].TOTAL}', '${revRank[4].TOTAL}'],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)', //1번째 그래프의 바탕색
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',      //1번째 그래프의 선색
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)'
                                        ],
                                        borderWidth: 1 //선굵기
                                    }]
                                },
                                options: {
                                    scales: { //X,Y축 옵션
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero:true  //Y축의 값이 0부터 시작
                                            }
                                        }]
                                    }
                                }
                            });
                            
                            </script>
                        </div>
                        <div class="top_content">
                            <div class="left">
                            <h4>영업공지</h4>
                            <ul>
                            <c:forEach items="${ noticeList }" var="nt" varStatus="status">
                            
                            	<li><a href="showNoticeDetail.sn?notNum=${ nt.NOT_ID }">
                            	<c:if test="${ nt.MUST_READ_YN eq 'Y' }">
                                    <img src="${ contextPath }/resources/images/check.png">                            	
                            	</c:if>
                                    <span class="notice"><c:out value="${ nt.NOT_TITLE }"/></span>
                                    <span class="notice emp"><c:out value="${ nt.EMP_NAME }"/> </span> 
                                </a>
                            </li>
                            </c:forEach>
                
                            </ul>
                                <a href="showNoticeList.sn" class="see_more">
                                <span>더보기</span>
                                <img src="${ contextPath }/resources/images/plus.png" width="10px">
                                </a>  
                            </div>
                            <div class="right">
                                <h4>오늘의 영업활동</h4>
                                <ul>
                                <c:forEach items="${ activityList }" var="act">
                                    <li><a href="#">
                                    <span>[<c:out value="${ act.ACTIVITY_START }"/> </span> ~ <span><c:out value="${ act.ACTIVITY_END }"/>]</span>
                                    <span><c:out value="${ act.CLIENT_NAME }"/></span> / <span><c:out value="${ act.COM_NAME }"/></span></a></li>                                
                                </c:forEach>
                                </ul>
                                <a href="actCalendar.sa" class="see_more">
                                    <span>더보기</span>
                                    <img src="${ contextPath }/resources/images/plus.png" width="10px">
                                </a>
                            </div>
                        </div>
                        <div class="bottom_content">
                            <div class="left">
                                <h4>영업기회</h4>
                                <ul>
                                <c:forEach items="${ oppList }" var="opp">
                                    <li><a href="#"><c:out value="${ opp.OPP_NAME }"/></a></li>                                	
                                </c:forEach>
                                </ul>
                                <a href="opportunityMain.so?empNo=${ opp.OPP_ID }" class="see_more">
                                    <span>더보기</span>
                                    <img src="${ contextPath }/resources/images/plus.png" width="10px">
                                </a>
                            </div>
                            <div class="right">
                                <div class="sell_title"><h4>당월매출목표</h4></div>
                                <div class="yscroll">
                                    <table>
                                    <tbody>
                                    <colgroup>
                                        <col style="width:40%;">
                                        <col style="width:15%;">
                                        <col style="width:15%;">
                                        <col style="width:20%;">
                                        <col style="width:10%;">
                                    </colgroup>
                                    <c:forEach items="${ pdtList }" var="pdt">                                    	
                                    	<tr>
                                            <th><c:out value="${ pdt.PDT_NAME }"/></th>
                                            <td><c:out value="${ pdt.QUANTITY }"/></td>
                                            <td><c:out value="${ pdt.STANDARD }"/></td>
                                            <td><c:out value="${ pdt.MONTH2   }"/></td>
                                        </tr>
                                    </c:forEach>
                                       
                                    </tbody>
                                    </table>
                                        
                                </div>
                                <div class="see_more">(단위: 만원)</div>
                            </div>
                        </div>
                    </div>
                   
		        </div>
		       
            </div>
	    </main>
    </div>
</body>
<script>
	
</script>
</html>