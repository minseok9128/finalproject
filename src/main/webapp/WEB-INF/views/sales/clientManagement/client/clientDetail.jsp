<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunityEnroll.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">


</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>고객</h1>
				안동환
				<button id="calcel_Btn" style="float: right; margin-bottom:10px;">수정</button>
				<div
					style="display: inline-block; margin-left: 242px; float: right;">
					<h1 id="icon">
						<i class="ellipsis vertical icon"></i>
					</h1>
				</div>

				<br> <br>
			</div>
			<div id="content_div">
				<table id="table">
					<tr class="td">
						<td>고객사</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>부서</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>직책</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>휴대번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>유선번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>메일</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>등급</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>KeyMan</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
					<tr class="td">
						<td>담당자</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							readonly></td>
					</tr>
				</table>

				<h1 id="propose_report">첨부자료</h1>
				<table id="table3" style="text-align: center">
					<tr class="td">
						<td style="text-align: left">엑셀파일!</td>
						<td style="width: 10%">
							<button>다운로드</button>
						</td>
					</tr>

				</table>

				<table>

					<tr>
						<h3>메모 (0)</h3>
						<td><textarea id="contents"
								style="width: 600px; height: 50px;"></textarea></td>
						<td><button
								style="width: 100px; height: 50px; margin-left: 10px;">메모</button></td>
					</tr>
				</table>


			</div>
		</div>
	</div>
</body>
</html>