<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css ">
</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
			<div class="tb_cnt" >
				<div class="tb_title"></div>
				<div id="header_div">
					<h1>
						고객
						<button onclick="goInsert()">등록</button>
					</h1>
					<input type=text id="customer_text" placeholder="유선번호/고객/고객사">
					<br>
					<input type=text id="Department" placeholder="부서명">
					<input type=text id="employee" placeholder="사원명">
					<button>검색</button>
				</div>
				<div class="search">
					<div class="one">
						<input type="text" placeholder="견적/고객/고객사">
					</div>
					<div class="secondSearchDiv">
						<div class="select half">
							<select>
								<option>영업부</option>
							</select>
						</div>
						<div class="input">
							<input type="text">
							<button class="ui blue basic button float">검색</button>
						</div>
					</div>
				</div>
			</div>
			<div id="content_div">
				<div>
					<button id="filter_Btn">필터</button>
					<P>60건</P>
					<p class="filters" style="font-weight: bold;">등록일 ▼</p>
					<p class="filters">고객사 |</p>
					<p class="filters">접촉일 |</p>
				</div>
				<hr>
				<div class="resultContents">
					
				</div>
				<div class="resultContents">
					
				</div>
			</div>
		</div>
	</div>
	<script>
		function goInsert(){
			location.href="goInsert.cl";
		}
	</script>
</body>
</html>