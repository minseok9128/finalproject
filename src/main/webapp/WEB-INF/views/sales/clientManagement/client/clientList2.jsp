<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.sm.lead.model.vo.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/jy/clientManagement/lead/LeadList2.css ">
<style>
.half {
	width: 49.5%;
	display: inline-block;
}

.select, .input {
	display: inline-block;
}

.client input {
	width: 100%;
}

.client {
	padding-top: 10px;
	padding-left: 10px;
}

.select {
	width: 49.5%;
	padding-top: 10px;
	padding-left: 10px;
}

.search div input {
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
	padding-left: 5px;
}

.select select {
	width: 100%;
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
}

.input input {
	width: 78%;
	border-radius: .28571429rem;
	border: 1px solid #D7DBDF;
}

.input {
	width: 49.5%;
	padding-left: 9.5px;
}

.ui.blue.basic.button {
	margin-right: 0;
}

.float {
	float: right;
}

.2nd_line {
	margin-top: 10px;
}
#content_div{
	padding-left:0;
}
.memo{
	background:#f0f4f8;
	border:1px solid #E1E6EB;
	width:80px;
	text-align:center;
	position:absolute;
	 /* left:0; */
	right:0; 
	z-index:10;
	display:none;
	top:20px;
}
.one {
	width: 100%;
	display:inline-block;
}
.ellipsis.vertical.icon{
	float:right;
}
.ui.right.icon.one p{
	padding-top:5px !important;
	padding-left:16px !important;
}
</style>
</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<!-- <div id="header_div">
					<h1>
						잠재고객
						<button>등록</button>
					</h1>
					<input type=text id="customer_text" placeholder="유선번호/고객/고객사">
					<input type=text id="category" placeholder="전체">
					<button>검색</button>
				</div> -->

			<h1>
				고객
				<button class="ui basic black button" id="insert" style="float:right;">등록</button>
			</h1>
			<div class="search">
				<div class="1st_line lane lane2 lane3">
					<div class="select">
						<select>
							<option>미접촉</option>
							<option>접촉시도</option>
							<option>접촉중</option>
							<option>접촉금지</option>
							<option>고객전환</option>
							<option>기존고객</option>
						</select>
					</div>
					<div class="input">
						<input type="text" placeholder="유선번호/고객/고객사">
						<button class="ui blue basic button float">검색</button>
					</div>
				</div>
				<div class="2nd_line" style="margin-top:10px">
					<div class="ui input right icon" id="rangestart">
						<i class="calendar icon"></i> <input type="text"
							placeholder="Start">
					</div>

					<div class="ui input right icon" id="rangeend">
						<i class="calendar icon"></i> <input type="text" placeholder="End">
					</div>
				</div>
			</div>
			<div id="content_div">
				<!-- <div>
					<button id="filter_Btn">필터</button>
					<P>60건</P>
					<p class="filters" style="font-weight: bold;">등록일 ▼</p>
					<p class="filters">고객사 |</p>
					<p class="filters">접촉일 |</p>
				</div> -->
				<hr>
				
				<c:forEach var="lead"  items="${list}">
					<div class="resultContents" style="padding:10px;">
						<input type="hidden"  value="${lead.leadId}">
						<div style="display:inline-block; width:78px; height:78px;" >
							<img style="max-width: 60px; height: 70px;" src="${contextPath}/resources/uploadFiles/pictures/${lead.CHANGE_NAME}" >
						</div>
						<div style="float:right; width:89%;" >
							<div style="display:inline-block; width:30%; position:relative; bottom:18px; left:10px;">
								<b ><c:out value="${lead.CLIENT_NAME}"/></b><br>
								<span>회사명 : <c:out value="${lead.COM_NAME}"/></span>
							</div>
							<div style="display:inline-block;width:39% ">
								<span>이메일 : <c:out value="${lead.EMAIL}"/></span><br>
								<span>회사번호 : <c:out value="${lead.TEL}"/></span><br>
								<span>핸드폰 : <c:out value="${lead.PHONE}"/></span>
							</div>
							<div style="float:right; width:29%"  >
								<span>등록일 : <c:out value="${fn:split(lead.ENROLL_DATE,' ')[0]}" /> 
								</span>
							</div>
						</div>
						<div style="padding-left:15px;">
							<c:if test="${lead.phone != null}">
								<c:out value="${lead.PHONE}"/>/
							</c:if>
							<c:if test="${lead.email != null}">
								<c:out value="${lead.DEPARTMENT}"/>
							</c:if>
						</div>
						<div style="padding-left:15px;">
							<c:if test="${lead.enrollDate != null}">
								<c:out value="${lead.RANK}"/>/
							</c:if>
							<c:if test="${lead.contStatus != null}">
								<c:out value="${lead.EMAIL}"/>/
							</c:if>
							<c:if test="${lead.email != null}">
								<c:out value="접촉일(2020.01.12)"/>/
							</c:if>
							<c:if test="${lead.email != null}">
								<c:out value="안동환"/>
							</c:if>
						</div>
						<%-- <div class="memo">
							<div class="meet">접촉등록</div>
							<c:if test="${lead.contStatus ne 5}">
								<div class="switch">고객전환</div>
							</c:if>
						</div> --%>
					</div>
				</c:forEach>
				
			</div>
		</div>
	</div>
<script>
	function clickfunction(){
		console.log("HELLO");
	}
	$(function(){
		
		 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
	        
	        // 열리는 메뉴
	        $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
	        $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(0).addClass("on");

		
		$("#insert").click(function(){
			location.href="goInsert2.cl";
		})
		
		$(".switch").click(function(){
			var n1 = $(this).parent().parent().children('input').val();
			event.stopPropagation();
			console.log(n1);
			location.href="goInsert.cl?num="+n1;
		})
		
		 $(".ellipsis.vertical.icon:not(.resultContents)").click(function(){
			 
			if($(this).hasClass("memoOn")){
				event.stopPropagation();
				$(this).removeClass("memoOn");
				
				$(this).parent().parent().children(".memo").hide();
			} else{
				event.stopPropagation();
				$(this).addClass("memoOn");
				
				$(this).parent().parent().children(".memo").show();
				
			} 
		/* $("a").click(function(){
			if($(this).hasClass("memoOn")){
				event.stopPropagation();
				$(this).removeClass("memoOn");
				$(this).parent().parent().children(".memo").hide();
			} else{
				event.stopPropagation();
				$(this).addClass("memoOn");
				$(this).parent().parent().children(".memo").show();
			}
		}) */
		});
			
			$(".resultContents:not(.ellipsis)").mouseenter(function(){
				$(this).css("border","#6199B9 solid 1px");
				$(this).children().css("color","#6199B9");
				$(this).children().children("p").css("color","#6199B9");
				$(this).find(".memo").css("color","#000");
				$(this).css("cursor","pointer");
				console.log("enter");
			}).mouseleave(function(){
				
				$(this).css("border","1px solid #DEE2E8");
				$(this).children().css("color","#000");
				$(this).children().children("p").css("color","#000");
				console.log("out");
			}).click(function(){
				
				var n = $(this).children().eq(0).val();
				console.log(n);
				location.href="selOne.le?leadId="+n;
				
				
			})
			$('html').click(function(e){ 
				   if(!$(e.target).hasClass("ellipsis")) { 
				      $(".memo").hide();
				   } 
			});
			
			
	})
</script>
</body>
</html>