<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunityEnroll2.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<style>
	 .td td:nth-child(1) {
		/* width: 18%;
		height: 50px;
		border-right: 1px solid #E5E5E5;
		background: #F8FCFF; */
	}
	 /* #table2 td:nth-child(1) {
             width: 186px;
             height: 50px;
             border-right: 1px solid #E5E5E5;
             background:#F8FCFF;
       } */
	#titleImg {
		display:inline-block;
		width:100px;
		height:100px;
	}
	#titleImgArea{
		width:100%;
		height:100px;
	}
	#imgBox{
		width:20%;
		height:20%;
	}
	
	#chekbox {
		float: left;
		margin-left: 6px;
	}
	
</style>
</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<jsp:include page="../../../common/modal/manager.jsp"></jsp:include>
	<jsp:include page="../../../common/modal/company.jsp"></jsp:include>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<form action="insert.cl" method="post" enctype="multipart/form-data">
				<h1>
					고객
					<!-- <button id="calcel_Btn">취소</button> -->
					<!-- <input  type="submit" value="작성하기"> -->
					<input type="button" class="ui inverted red button" id="resetBtn" style="width: 70px; height:35px; float:right;     margin-left: 6px;" value="취소">
					<button class="ui inverted primary button" id="submitBtn" style="width: 70px; height:35px;">작성</button>
<!-- 					<button onclick="insertCl()" id="submit_Btn">작성</button> -->
				</h1>
				<br> <br>
			</div>
			<div id="content_div">
			<input type="hidden" value="${lead.leadId}" name="leadId" >
				<table id="table" class="mmain">
					<tr>
						<td class="td">고객<b class="point">*</b></td>
						<td><input type="text" id="propose_Name" value="${lead.userName}" name="clientName" class="common_Text1" style="width:95%;">
						</td>
						<td id="imgBox" rowspan="3" style="border-left:1px solid #D1D6DE;">
							<div id="titleImgArea">
								<img id="titleImg">
							</div>
							<div id="fileArea">
								<input type="file" id="thumbnailImg1" name="thumbnailImg1" onchange="loadImg(this ,1)">
							</div>
						</td>
						<!-- <td colspan="3">
							
						</td> -->
					</tr>
					<tr>
						<td class="td">고객사</td>
						<td>
							<input type="text" style="width:70%;" id="company" value="${lead.company}" name="comName" class="common_Text2"readonly> 
							<input type="hidden" id="company_hidden"value="0" name="comId">
							<%-- <input type="text" id="propose_Name" value="${lead.company}" name="comId" class="common_Text1" style="width:400px;"> --%>
							<button class="ui grey basic button srch searchBtn" id="company_Modal">검색</button>
						</td>
					</tr>
					<tr>
						<td class="td">부서</td>
						<td><input type="text" id="propose_Name" value="${lead.department}" name="department" class="common_Text1" style="width:95%;">
						</td>
					</tr>
					<tr>
						<td class="td">직책</td>
						<td colspan="2"><input type="text" id="propose_Name" value="${lead.job}" name="job" class="common_Text1">
						</td>
					</tr>
					<tr>
						<td class="td">휴대번호<b class="point">*</b></td>
						<td colspan="2"><input type="text" id="propose_Name" value="${lead.phone}" name="phone" class="common_Text1">
						</td>
					</tr>
					<tr >
						<td class="td">유선번호</td>
						<td colspan="2"><input type="text" id="propose_Name" value="${lead.tel}" name="tel" class="common_Text1">
						</td>
					</tr>
					<tr >
						<td class="td">메일<b class="point">*</b></td>
						<td colspan="2"><input type="text" id="propose_Name" value="${lead.email}" name="email" class="common_Text1">
						</td>
					</tr>
					<tr >
						<td class="td">등급</td>
						<td colspan="2"><select name="rank" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="NONE">선택하세요</option>
								<option value="S">S등급</option>
								<option value="A">A등급</option>
								<option value="B">B등급</option>
								<option value="C">C등급</option>
								<option value="D">D등급</option>
								<option value="ETC">기타</option>
						</select></td>
					</tr>
					<tr >
						<td class="td">KeyMan</td>
						<td colspan="2"><input type="checkbox" id="chekbox" name="keyMan">
						</td>
					</tr>
					<tr >
						<td class="td">생년월일</td>
						<td colspan="2">
							<div class="ui calendar" id="calendarOpen">
				        		<div class="ui input left icon">
				        			<i class="calendar icon" ></i>
				        			<input readonly class="cld" type="text" name="birthday" placeholder="Start">
				        		</div>
				      		</div>
						</td>
					</tr>
					<tr >
						<td class="td">종교</td>
						<td colspan="2"><input type="text" id="propose_Name" class="common_Text1" name="religion" style="width:100%;" >
					</tr>
					<tr >
						<td class="td">구분</td>
						<td colspan="2">
							<select name="category" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="1">가망고객</option>
								<option value="2">거래고객</option>
								<option value="3">해지고객</option>
								<option value="4">기타</option>
							</select>
						</td>
					</tr>
					<tr >
						<td class="td">담당자</td>
						<td colspan="2">
							<input class="common_Text1" type="text" readonly style="width: 100%; height: 32px;" value="${sessionScope.loginEmp.empName }">
							<input type="hidden" style="width: 250px; height: 32px;" name="empNo" value="${sessionScope.loginEmp.empNo }">
							<!-- <input type="text" class="common_Text2" id="managerText"value=""> 
							<input type="hidden" id="manager_hidden" value="0" name="empNo">
							<button class="ui grey basic button srch searchBtn" id="manager">검색</button> -->
						</td>
					</tr>
				</table>
				<!-- 첨부자료 주석 0209 -->
		<!-- 		<h1 id="propose_report">첨부자료
				 	<button id="productFile_btn"  class="ui inverted primary button" style="float:right;">업로드</button>
				</h1>
             	<hr>
       			<table id="file_table">
         
       			</table> -->
				</form>
			</div>
		</div>
	</div>
<script>
	function loadImg(value ,num){
		if(value.files && value.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				switch(num){
				case 1 : $("#titleImg").attr("src", e.target.result); break;
				}
			}
			reader.readAsDataURL(value.files[0]);
		}
	}
	$(function () {
		
		$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
        
        // 열리는 메뉴
        $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
        $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(0).addClass("on");
		
		$("#fileArea").hide(); //숨기는 부분
		
		$("#titleImgArea").click(function(){ //클릭될 때
			$("#thumbnailImg1").click(); //실행되는 이벤트
		})
	})
var count=1;
	$(function(){
		$('#productFile_btn').click(function (e) {
		    e.preventDefault();
		    var str = '<tr><td><input type="file" name="files'+ (count++) +'" style="display:none" ><input class="upload_name" id="upload_name' 
		   + (count++) + '" readonly style="border:0;""></td><td><i class="trash alternate outline icon" style="float: right" onclick="remove_btn($(this))"></i></td></tr>';
		   
		   console.log("트리거 시작전");
		   $("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
		   console.log("트리거 시작후");
		   $("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
		        var cur=$(this).val();
		        var arSplitUrl = cur.split('\\');
		        var nArLength = arSplitUrl.length;
		        var fileName = arSplitUrl[nArLength-1];
		          
		          $("#upload_name" + (count-1)).val(fileName);
		        console.log(cur);
		     }); 
		  }); 
	})
	
  
	$('#calendarOpen').calendar({
	  startMode: 'year',
	  type: 'date',
	  formatter: {
	      date: function (date, settings) {
	        if (!date) return '';
	        var day = date.getDate();
	        var month = date.getMonth() + 1;
	        var year = date.getFullYear();
	        return year + '-' + month + '-' + day;
	        /* return day + '/' + month + '/' + year; */
	      }
	    }
	});
	   function remove_btn(obj){
           obj.parent().parent().remove();
       }
	/* function insertCl(){
		location.href="insert.cl";
		
	} */
</script>
</body>
</html>