<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunityEnroll.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">


</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					고객
					<button id="calcel_Btn">취소</button>
					<button id="submit_Btn">작성</button>
				</h1>
				<br> <br>
			</div>
			<div id="content_div">
				<table id="table">
					<tr class="td">
						<td>고객<b class="point">*</b></td>
						<td><input type="text" id="propose_Name" class="common_Text1" style="width:500px;">
						</td>
						<td rowspan="3" style="border-left:1px solid #D1D6DE;"><div></div></td>
					</tr>
					<tr class="td">
						<td>고객사</td>
						<td><input type="text" id="propose_Name" class="common_Text1" style="width:400px;">
						
						<button class="search_Btn">검색</button></td>
					</tr>
					<tr class="td">
						<td>부서</td>
						<td><input type="text" id="propose_Name" class="common_Text1" style="width:500px;">
						</td>
					</tr>
					<tr class="td">
						<td>직책</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>휴대번호<b class="point">*</b></td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>유선번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>메일<b class="point">*</b></td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>등급</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">선택하세요</option>
								<option value="pro2">S등급</option>
								<option value="pro3">A등급</option>
								<option value="pro4">B등급</option>
								<option value="pro5">C등급</option>
								<option value="pro6">D등급</option>
								<option value="pro7">기타</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>KeyMan</td>
						<td><input type="checkbox" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>고객사</td>
						<td><input type="text" id="propose_Name" class="common_Text1" value="영업 1팀 김영수" style="width:400px;" readonly>
						
						<button class="search_Btn">검색</button></td>
					</tr>
				</table>

				<h1 id="propose_report">
					첨부자료
					<button id="propose_report_Btn">업로드</button>
				</h1>
				<table id="table3" style="text-align: center">
					<tr class="td">
						<td style="width: 20%; text-align: left">첨부자료</td>
						<td style="text-align: left">엑셀파일!</td>
						<td style="width: 10%"><i
							class="trash alternate outline icon"></i></td>



					</tr>

				</table>


			</div>
		</div>
	</div>
</body>
</html>