<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.sm.company.model.vo.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	List<Company> list = (List<Company>) request.getAttribute("companyList");
	System.out.println("list in jsp : " + list);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
</head>
<jsp:include page="../../../common/menubar.jsp" />
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div class="header_div">
				<div>
					<h1>고객사</h1>
					<button id="enrollment_estimate_Btn" class="search_Btn"
						onclick="goInsert()">등록</button>
				</div>
				<div class="search">
					<div class="one">
						<div class="half input">
							<input type="text" placeholder="고객사">
						</div>
						<div class="select half">
							<select>
								<option>경쟁사</option>
								<option>고객사</option>
								<option>파트너사</option>
							</select>
						</div>
					</div>
					<div class="secondSearchDiv">
						<div class="select half">
							<select>
								<option>영업부</option>
							</select>
						</div>
						<div class="input">
							<input type="text" placeholder="사원명">
							<button class="ui blue basic button float">검색</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="content_div">
			<div>
				<button id="filter_Btn">필터</button>
				<P>60건</P>
				<p class="filters">가나다</p>
				<p class="filters">고객사 |</p>
				<p class="filters" style="font-weight: bold;">견적일▼ |</p>
			</div>
			<hr>
			
			<c:forEach var="company" items="${companyList }">
				<div class="resultContents">
					<input type="hidden" value="${company.comId }"> 
					<p>
						<c:out value="${company.comName }" />
					</p>
				</div>
				<%-- <div style="padding-left: 15px;">
					<c:if test="${company.category != null}">
						<c:out value="${company.category }" />/
				</c:if>
					<c:if test="${company.revenueYear != null }">
						<c:out value="${company.revenueYear }" />
					</c:if>
				</div>
				<div style="padding-left: 15px;">
					<c:if test="${company.address != null }">
						<c:out value="${company.address }" />
					</c:if>
				</div> --%>
				
			</c:forEach>
			
		</div>
	</div>

	<script>
		function goInsert() {
			location.href = "goInsert.co";
		}
	</script>
	<script>
		$(function(){
			
			 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
		        
		        // 열리는 메뉴
		        $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
		        $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");

			
			$(".resultContents:not(.ellipsis)").mouseenter(function(){
				$(this).css("border","#6199B9 solid 1px");
				$(this).children().css("color","#6199B9");
				$(this).children().children("p").css("color","#6199B9");
				$(this).find(".memo").css("color","#000");
				$(this).css("cursor","pointer");
				console.log("enter");
			}).mouseleave(function(){
				
				$(this).css("border","1px solid #DEE2E8");
				$(this).children().css("color","#000");
				$(this).children().children("p").css("color","#000");
				console.log("out");
			}).click(function(){
				
				var n = $(this).children().eq(0).val();
				console.log(n);
				location.href="selOne.co?ComId="+n;
				
				
			})
		})
	</script>
</body>
</html>