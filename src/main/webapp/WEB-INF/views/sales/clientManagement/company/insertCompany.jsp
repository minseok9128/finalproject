<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/jy/clientManagement/company/insertCompany.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script>
	function goPopup() {
		window.open("${contextPath}/jusoPopup.jsp", "pop",
				"width=570, height=420, scrollbars=yes, resizable=yes");
	}
	function jusoCallBack(roadFullAddr, zipNo, roadAddrPart1, addrDetail) {
		document.getElementById("zipNo").value = zipNo;
		document.getElementById("roadAddrPart1").value = roadAddrPart1;
		document.getElementById("addrDetail").value = addrDetail;
	}
</script>

</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<form action="insert.co" method="post">
				<div id="header_div">
					<h1>
						고객사
						<button id="calcel_Btn">취소</button>
						<button id="submit_Btn">작성</button>
					</h1>
					<br> <br>
				</div>
				<div id="content_div">
					<table id="table">
						<tr class="td">
							<td>고객사<b class="point">*</b></td>
							<td><input name="comName" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<tr class="td">
							<td>구분</td>
							<td><select name="category" id="day_select"
								style="width: 600px; height: 33px;">
									<option value="1">경쟁사</option>
									<option value="2">고객사</option>
									<option value="3">파트너사</option>
							</select></td>
						</tr>
						<tr class="td">
							<td>등급</td>
							<td><select name="comRating" id="day_select"
								style="width: 600px; height: 33px;">
									<option value="S">S등급</option>
									<option value="A">A등급</option>
									<option value="B">B등급</option>
									<option value="C">C등급</option>
									<option value="D">D등급</option>
									<option value="ETC">기타</option>
									<option value="NONE">등급없음</option>
							</select></td>
						</tr>
						<!-- <tr class="td">
						<td>진행상태</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro2">미접촉</option>
								<option value="pro3">진행중</option>
								<option value="pro4">성공</option>
								<option value="pro5">실패</option>
								<option value="pro6">보류</option>
						</select></td>
					</tr> -->
						<tr class="td">
							<td>매출 (년)</td>
							<td><input name="revenueYear" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<tr class="td">
							<td>사원수</td>
							<td><input name="empNum" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<tr class="td">
							<td>사업자번호</td>
							<td><input name="bizLisence" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<tr class="td">
							<td>유선번호</td>
							<td><input name="tel" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<tr class="td">
							<td>팩스번호</td>
							<td><input name="fax" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<tr class="td">
							<td>웹사이트</td>
							<td><input name="webSite" type="text" id="propose_Name"
								class="common_Text1"></td>
						</tr>
						<!-- <tr class="td">
						<td>주소</td>
						<td><input name="address" type="text" id="propose_Name" class="common_Text1"
							style="background: #EAEAEA; width: 100px"></td>
						<td><button class="search_Btn">검색</button></td>
					</tr> -->
						<tr class="td">
							<td>우편번호</td>
							<td><input type="text" style="width: 30%"
								; padding-right:10px; text-align:center;" id="zipNo"
								name="zipNo" class="common_Text1"
								style="background: #EAEAEA; width: 100px"> <span
								onclick="goPopup()" class="search_Btn"
								style="float: left; margin-left: 10px; padding-top: 4.5px;">검색</span>
							</td>
						</tr>
						<tr class="td">
							<td>상세주소</td>
							<td><input type="text" style="width: 100%;" id="addrDetail"
								name="addrDetail" class="common_Text1"
								style="background:#EAEAEA; width: 100px"></td>
						</tr>
					</table>

					<h1 id="propose_report">
						첨부자료
						<button id="propose_report_Btn">업로드</button>
					</h1>
					<table id="table3" style="text-align: center">
						<tr class="td">
							<td style="width: 20%; text-align: left">첨부자료</td>
							<td style="text-align: left">엑셀파일!</td>
							<td style="width: 10%"><i
								class="trash alternate outline icon"></i></td>



						</tr>

					</table>
				</div>
			</form>

		</div>

	</div>

</body>
</html>