<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunityEnroll.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">


</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					고객사
					<button id="calcel_Btn">취소</button>
					<button id="submit_Btn">작성</button>
				</h1>
				<br> <br>
			</div>
			<div id="content_div">
				<table id="table">
					<tr class="td">
						<td>고객사<b class="point">*</b></td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>구분</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">선택하세요</option>
								<option value="pro2">경쟁사</option>
								<option value="pro3">고객사</option>
								<option value="pro4">파트너사</option>
								<option value="pro5">가망고객</option>
								<option value="pro6">거래고객</option>
								<option value="pro7">해지고객</option>
								<option value="pro8">기타</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>등급</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">선택하세요</option>
								<option value="pro2">S등급</option>
								<option value="pro3">A등급</option>
								<option value="pro4">B등급</option>
								<option value="pro5">C등급</option>
								<option value="pro6">D등급</option>
								<option value="pro7">기타</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>진행상태</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">선택하세요</option>
								<option value="pro2">미접촉</option>
								<option value="pro3">진행중</option>
								<option value="pro4">성공</option>
								<option value="pro5">실패</option>
								<option value="pro6">보류</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>매출 (년)</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>사원수</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>사업자번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>유선번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>팩스번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>웹사이트</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>주소</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							style="background: #EAEAEA; width: 100px"></td>
						<td><button class="search_Btn">검색</button></td>
					</tr>
				</table>

				<h1 id="propose_report">
					첨부자료
					<button id="propose_report_Btn">업로드</button>
				</h1>
				<table id="table3" style="text-align: center">
					<tr class="td">
						<td style="width: 20%; text-align: left">첨부자료</td>
						<td style="text-align: left">엑셀파일!</td>
						<td style="width: 10%"><i
							class="trash alternate outline icon"></i></td>



					</tr>

				</table>


			</div>
		</div>
	</div>
</body>
</html>