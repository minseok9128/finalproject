<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
   href="${ contextPath }/resources/css/hkh/opportunityEnroll3.css ">
<script
   src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
   href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script>
function goPopup(){
	// 주소검색을 수행할 팝업 페이지를 호출합니다.
	// 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)를 호출하게 됩니다.
	/* var pop = window.open("${contextPath}/jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes");  */
	window.open("${contextPath}/jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes");
	// 모바일 웹인 경우, 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(http://www.juso.go.kr/addrlink/addrMobileLinkUrl.do)를 호출하게 됩니다.
    //var pop = window.open("/popup/jusoPopup.jsp","pop","scrollbars=yes, resizable=yes"); 
}

function jusoCallBack(roadFullAddr,zipNo,roadAddrPart1,addrDetail/* ,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn,detBdNmList,bdNm,bdKdcd,siNm,sggNm,emdNm,liNm,rn,udrtYn,buldMnnm,buldSlno,mtYn,lnbrMnnm,lnbrSlno,emdNo */){
		// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
		/* document.form.roadFullAddr.value = roadFullAddr;
		document.form.roadAddrPart1.value = roadAddrPart1;
		document.form.roadAddrPart2.value = roadAddrPart2;
		document.form.addrDetail.value = addrDetail;
		document.form.engAddr.value = engAddr;
		document.form.jibunAddr.value = jibunAddr;
		document.form.zipNo.value = zipNo;
		document.form.admCd.value = admCd;
		document.form.rnMgtSn.value = rnMgtSn;
		document.form.bdMgtSn.value = bdMgtSn;
		document.form.detBdNmList.value = detBdNmList;
		/** 2017년 2월 추가제공 **/
		/* document.form.bdNm.value = bdNm;
		document.form.bdKdcd.value = bdKdcd;
		document.form.siNm.value = siNm;
		document.form.sggNm.value = sggNm;
		document.form.emdNm.value = emdNm;
		document.form.liNm.value = liNm;
		document.form.rn.value = rn;
		document.form.udrtYn.value = udrtYn;
		document.form.buldMnnm.value = buldMnnm;
		document.form.buldSlno.value = buldSlno;
		document.form.mtYn.value = mtYn;
		document.form.lnbrMnnm.value = lnbrMnnm;
		document.form.lnbrSlno.value = lnbrSlno; */
		/** 2017년 3월 추가제공 **/
		/* document.form.emdNo.value = emdNo; */ 
		document.getElementById("zipNo").value = zipNo;
		document.getElementById("roadAddrPart1").value = roadAddrPart1;
		document.getElementById("addrDetail").value = addrDetail;
}
</script>
</head>
<body>
<jsp:include page="../../../common/modal/client.jsp"></jsp:include>
   <jsp:include page="../../../common/menubar.jsp" />
   <div class="main_ctn" style="background: white;">
      <div class="title"></div>
      <div class="tb_cnt">
         <div class="tb_title"></div>
         <form action="insert.le" method="post">
         <div id="header_div">
            <h1>
               잠재고객
               <!--<button id="calcel_Btn">취소</button>  -->
               <!-- <button id="submit_Btn">작성</button> -->
               <input type="button" class="ui inverted red button" id="resetBtn" style="width: 70px; height:35px; float:right;     margin-left: 6px;" value="취소">
               <button class="ui inverted primary button" id="submit_Btn" style="width: 70px; height:35px;">작성</button>
            </h1>
            <br> <br>
         </div>
         <div id="content_div">
            <table id="table">
               <tr class="td">
                  <td>이름<b class="point">*</b></td>
                  <td><input type="text" name="userName" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>고객사</td>
                  <td><input type="text" name="company" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>부서</td>
                  <td><input type="text" name="department" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>직책</td>
                  <td><input type="text" name="job" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>첩촉구분</td>
                  <td><select name="contCategory" id="day_select"
                     style="width: 600px; height: 33px;">
                        <option value="1">자사홈페이지</option>
                        <option value="2">인터넷검색</option>
                        <option value="3">지인소개</option>
                        <option value="4">제품설명회</option>
                        <option value="5">세미나</option>
                        <option value="6">전화(인바운드)</option>
                        <option value="7">기타</option>
                  </select></td>
               </tr>
               <tr class="td">
                  <td>접촉상태</td>
                  <td><select name="contStatus" id="day_select"
                     style="width: 600px; height: 33px;">
                        <option value="1">미접촉</option>
                        <option value="2">접촉시도</option>
                        <option value="3">접촉중</option>
                        <option value="4">접촉금지</option>
                        <option value="5">고객전환</option>
                        <option value="6">기존고객</option>
                  </select></td>
               </tr>
               <tr class="td">
                  <td>가망등급</td>
                  <td><select name="rank" id="day_select"
                     style="width: 600px; height: 33px;">
                        <option value="S">S등급</option>
                        <option value="A">A등급</option>
                        <option value="B">B등급</option>
                        <option value="C">C등급</option>
                        <option value="NONE">등급없음</option>
                  </select></td>
               </tr>
               <tr class="td">
                  <td>휴대번호</td>
                  <td><input type="text" name="phone" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>유선번호</td>
                  <td><input type="text" name="tel" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>메일</td>
                  <td><input type="text" name="email" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td>팩스번호</td>
                  <td><input type="text" name="fax" class="common_Text1">
                  </td>
               </tr>
               <tr class="td">
                  <td >우편번호</td>
                  <td><input type="text" style="width:30%; padding-right:10px; text-align:center;" id="zipNo" name="zipNo" class="common_Text1"
                     style="background: #EAEAEA; width: 100px">
                     <span onclick="goPopup()"  class="search_Btn" style="float: left; margin-left:10px; padding-top:4.5px;">검색</span>
                     </td>
               </tr>
               <tr class="td">
                  <td >주소</td>
                  <td><input  style="width:100%; " type="text" id="roadAddrPart1" name="roadAddrPart1" class="common_Text1"
                     style="background:#EAEAEA; width: 100px"></td>
               </tr>
               <tr class="td">
                  <td >상세주소</td>
                  <td><input type="text" style="width:100%;" id="addrDetail" name="addrDetail" class="common_Text1"
                     style="background:#EAEAEA; width: 100px"></td>
               </tr>
               <tr class="td">
                  <td>담당자</td>
                  <td>
                  	<input type="text" class="common_Text1" style="width:100%; height: 32px;" readonly value="${sessionScope.loginEmp.empName }">
					<input type="hidden" style="width: 250px; height: 32px;" name="empNo" value="${sessionScope.loginEmp.empNo }">
                  </td>
               </tr>
               <tr class="td">
                  <td>비고</td>
                  <td><textarea name="remark" id="contents"></textarea></td>
               </tr>
            </table>

           <!--  <h1 id="propose_report">
               이력
               <button id="propose_report_Btn">추가</button>
            </h1>
            <table id="table2" style="text-align: center">
               <tr class="td">
                  <td style="border-right: 1px solid #E5E5E5; background: white;">접촉일</td>
                  <td style="border-right: 1px solid #E5E5E5; background: white;">접촉구분</td>
                  <td rowspan="2"
                     style="border-right: 1px solid #E5E5E5; background: white;">삭제</td>

               </tr>


               <tr>
                  <td colspan="2"
                     style="border-right: 1px solid #E5E5E5; background: white;">접촉내용</td>


               </tr>

               <tr class="td" style="border-top: 2px solid #E5E5E5">
                  <td style="border-right: 1px solid #E5E5E5;">캘린더</td>
                  <td style="border-right: 1px solid #E5E5E5;">
                     <select name="day" id="day_select"
                     style="width: 300px; height: 33px;">
                        <option value="pro1">전화</option>
                        <option value="pro2">메일</option>
                        <option value="pro3">방문</option>
                        <option value="pro4">기타</option>
                     </select>
                  </td>
                  <td rowspan="2" style="border-right: 1px solid #E5E5E5;"><i
                     class="trash alternate outline icon"></i></td>

               </tr>


               <tr>
                  <td colspan="2" style="border-right: 1px solid #E5E5E5;">
                     <textarea id="contents" style="width:700px; height:50px;"></textarea>
                  </td>


               </tr>


            </table> -->
            </form>


         </div>
      </div>
   </div>
   <script>
   $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
   
   // 열리는 메뉴
   $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
   $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(2).addClass("on");

   	
   	</script>
</body>
</html>