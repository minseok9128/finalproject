<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" ur uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
   href="${ contextPath }/resources/css/hkh/opportunityEnroll2.css ">
<script
   src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
   href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script>
function goPopup(){
	// 주소검색을 수행할 팝업 페이지를 호출합니다.
	// 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)를 호출하게 됩니다.
	/* var pop = window.open("${contextPath}/jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes");  */
	window.open("${contextPath}/jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes");
	// 모바일 웹인 경우, 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(http://www.juso.go.kr/addrlink/addrMobileLinkUrl.do)를 호출하게 됩니다.
    //var pop = window.open("/popup/jusoPopup.jsp","pop","scrollbars=yes, resizable=yes"); 
}

function jusoCallBack(roadFullAddr,zipNo,roadAddrPart1,addrDetail/* ,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn,detBdNmList,bdNm,bdKdcd,siNm,sggNm,emdNm,liNm,rn,udrtYn,buldMnnm,buldSlno,mtYn,lnbrMnnm,lnbrSlno,emdNo */){
		// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
		/* document.form.roadFullAddr.value = roadFullAddr;
		document.form.roadAddrPart1.value = roadAddrPart1;
		document.form.roadAddrPart2.value = roadAddrPart2;
		document.form.addrDetail.value = addrDetail;
		document.form.engAddr.value = engAddr;
		document.form.jibunAddr.value = jibunAddr;
		document.form.zipNo.value = zipNo;
		document.form.admCd.value = admCd;
		document.form.rnMgtSn.value = rnMgtSn;
		document.form.bdMgtSn.value = bdMgtSn;
		document.form.detBdNmList.value = detBdNmList;
		/** 2017년 2월 추가제공 **/
		/* document.form.bdNm.value = bdNm;
		document.form.bdKdcd.value = bdKdcd;
		document.form.siNm.value = siNm;
		document.form.sggNm.value = sggNm;
		document.form.emdNm.value = emdNm;
		document.form.liNm.value = liNm;
		document.form.rn.value = rn;
		document.form.udrtYn.value = udrtYn;
		document.form.buldMnnm.value = buldMnnm;
		document.form.buldSlno.value = buldSlno;
		document.form.mtYn.value = mtYn;
		document.form.lnbrMnnm.value = lnbrMnnm;
		document.form.lnbrSlno.value = lnbrSlno; */
		/** 2017년 3월 추가제공 **/
		/* document.form.emdNo.value = emdNo; */ 
		document.getElementById("zipNo").value = zipNo;
		document.getElementById("roadAddrPart1").value = roadAddrPart1;
		document.getElementById("addrDetail").value = addrDetail;
}
</script>
</head>
<body>
<jsp:include page="../../../common/modal/client.jsp"></jsp:include>
   <jsp:include page="../../../common/menubar.jsp" />
  <div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<c:set var="ppsId" value="${ppsId}"/>
			<c:if test="${ppsId == 0}">
				<c:set var="move" value="insertOfferEnrollment.of"/>
			</c:if>
			<c:if test="${ppsId != 0 }">
				<c:set var="move" value="modifyOfferEnrollment.of"/>
			</c:if>
				<form action="${move}" method="post" enctype="multipart/form-data">			
				<div class="header_div">
					<c:if test="${ppsId == 0 }">
						<c:set var="list" value="0"/>
					</c:if>
					<c:if test="${ppsId != 0 }">
						<c:set var="list" value="${list}"/>
					</c:if>
					<div>
					<c:if test="${ppsId == 0}">
						<h1>제안등록</h1>
					</c:if>
					<c:if test="${ppsId != 0}">
						<h1>제안수정</h1>
					</c:if>
							<button id="realBtn" hidden>이것은 입력하지 않은 file input 태그를 지우고 controller에 보내는 버튼</button>
							<button class="ui basic red button" id="cancelEnrollBtn">취소</button>
						<c:if test="${ppsId == 0}">
							<button class="ui basic blue button" id="submitBtn">작성</button>
						</c:if>
						<c:if test="${ppsId != 0}">
							<button class="ui basic blue button" id="modifyBtn">수정</button>
						</c:if>
					</div>
				</div>
				<div id="content_Enrollment_div">
					<table id="table2">
						<tr>
							<td class="td">제안명<b class="point">*</b></td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="offer_Name" name="ppsName"class="common_Text1">
									<input type="hidden" value="0" name="ppsId">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" id="offer_Name" name="ppsName"class="common_Text1" value="${list[0].ppsName}">
									<input type="hidden" value="${ppsId}" name="ppsId">									
								</c:if>							
							</td>
						</tr>
						<tr>
							<td class="td">고객사</td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="company" class="common_Text2"readonly> <input type="hidden" id="company_hidden"value="0" name="companyId">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" id="company" class="common_Text2"readonly value="${list[0].company.comName}"> <input type="hidden" id="company_hidden"value="${list[0].company.comId}" name="companyId">
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="company_Modal">검색</button>
							</td>
						</tr>
						<tr>
							<td class="td">고객<b class="point">*</b></td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="client" class="common_Text2"><input type="hidden" id="client_hidden" value="0"name="clientId">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" id="client" class="common_Text2" value="${list[0].client.clientName}"><input type="hidden" id="client_hidden" value="${list[0].client.clientId}"name="clientId">
								</c:if>							
								<button class="ui grey basic button srch searchBtn" id="client_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">영업기회</td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="opportunity" class="common_Text2">
									<input type="hidden" id="opportunity_hidden" value="0" name="oppId">
									<input type="hidden" value="0" id="processId" name="processId"/>
								</c:if>
								<c:if test="${ppsId != 0}">
									<c:set var="pssName" value="${fn:split(list[0].ppsName,' ')[0]}"/>
									<input type="text" id="opportunity" class="common_Text2" value="${pssName}">
									<input type="hidden" id="opportunity_hidden" value="${list[0].oppId}" name="oppId">
									<input type="hidden" value="0" id="processId" name="processId" value="0"/>
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="opportunity_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">내용</td>
							<td>
								<c:if test="${ppsId == 0}">
									<textarea class="contents" name="content"></textarea>
								</c:if>
								<c:if test="${ppsId != 0 }">
									<textarea class="contents" name="content"><c:out value="${list[0].content}"/></textarea>
								</c:if>
							</td>
						</tr>
						<tr>
							<td class="td">요청일</td>
							<td>
								<div class="field">
									<div class="ui calendar cf_left" id="standard_calendar">
										<div class="ui input right icon cf_left">
											<i class="calendar icon"></i> 
											<c:if test="${ppsId == 0}">
												<input type="text" readonly name="reday">
											</c:if>
											<c:if test="${ppsId != 0}">
													<input type="text" readonly name="reday" value="${list[0].requestDate }">
											</c:if>						
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td">제출일<b class="point">*</b></td>
							<td>
								<div class="field">
									<div class="ui calendar cf_left" id="standard_calendar2">
										<div class="ui input right icon cf_left" id="a">
											<i class="calendar icon"></i> 
											<c:if test="${ppsId == 0}">
												<input type="text" readonly name="suday">
											</c:if>
											<c:if test="${ppsId != 0 }">
												<input type="text" readonly name="suday" value="${list[0].submitDate}">
											</c:if>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td">담당자<b class="point">*</b></td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" class="common_Text2" id="managerText"value=""> 
									<input type="hidden" id="manager_hidden" value="0" name="empNo">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" class="common_Text2" id="managerText"value="${list[0].employee.empName }"> 
									<input type="hidden" id="manager_hidden" value="${list[0].empNo}" name="empNo">
								</c:if>						
								<button class="ui grey basic button srch searchBtn" id="manager">검색</button></td>
						</tr>
						<tr>
							<td class="td">비고</td>
							<td>
								<c:if test="${ppsId == 0 }">
									<textarea class="contents" name="remark"></textarea>
								</c:if>
								<c:if test="${ppsId != 0 }">
									<textarea class="contents" name="remark"><c:out value="${list[0].remark}"/></textarea>
								</c:if>						
							</td>
						</tr>
					</table>

					<div>
						<div class="fileUploadDiv" id="fileUp1">
							<c:set var="request" value="${requestList}"/>
							<c:set var="report" value="${reportList}"/>
							<div>
								<h1>제안요청서</h1>
								<button class="ui basic blue button right"
									id="proposeRequestBtn">요청서등록</button>
								<br>
								<hr>
							</div>
							<ul>
							<c:if test="${ppsId != 0 }">
								<c:forEach items="${request}" varStatus="status">
									<li>
										${request[status.index].originName} <i class="trash alternate outline icon right"></i>
										<input type="hidden" value="${request[status.index].fileId}">
										<input type="hidden" value="${request[status.index].category}">
									</li>
								</c:forEach>
							</c:if>
							</ul>
						</div>
						<div class="fileUploadDiv" id="fileUp2">
							<div>
								<h1>제안서</h1>
								<button class="ui basic blue button right" id="proposereportBtn">제안서등록</button>
								<br>
								<hr>
							</div>
								<ul>
								<c:if test="${pssId != 0}">
									<c:forEach items="${report}" varStatus="status">
									<li>
										${report[status.index].originName} <i class="trash alternate outline icon right"></i>
										<input type="hidden" value="${report[status.index].fileId}">
										<input type="hidden" value="${report[status.index].category}">
									</li>
								</c:forEach>
								</c:if>
								</ul>
						</div>
					</div>
				</div>
			</form>
		</div>
		</main>
	</div>
	</div>
	</div>
   <script>
   </script>
</body>
</html>