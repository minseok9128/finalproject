<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.sm.lead.model.vo.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/jy/clientManagement/lead/LeadList2.css ">
<style>
.half {
	width: 49.5%;
	display: inline-block;
}

.select, .input {
	display: inline-block;
}

.client input {
	width: 100%;
}

.client {
	padding-top: 10px;
	padding-left: 10px;
}

.select {
	width: 49.5%;
	padding-top: 10px;
	padding-left: 10px;
}

.search div input {
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
	padding-left: 5px;
}

.select select {
	width: 100%;
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
}

.input input {
	width: 78%;
	border-radius: .28571429rem;
	border: 1px solid #D7DBDF;
}

.input {
	width: 49.5%;
	padding-left: 9.5px;
}

.ui.blue.basic.button {
	margin-right: 0;
}

.float {
	float: right;
}

.2nd_line {
	margin-top: 10px;
}
#content_div{
	padding-left:0;
}
.memo{
	background:#f0f4f8;
	border:1px solid #E1E6EB;
	width:80px;
	text-align:center;
	position:absolute;
	 /* left:0; */
	right:0; 
	z-index:10;
	display:none;
	top:20px;
}
.one {
	width: 100%;
	display:inline-block;
}
.ellipsis.vertical.icon{
	float:right;
}
.ui.right.icon.one p{
	padding-top:5px !important;
	padding-left:16px !important;
}
</style>
</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<jsp:include page="../../../common/modal/leadHistory.jsp"/>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>

			<h1  style="margin-bottom:0">
				잠재고객
				<button class = "ui inverted primary button" id="insertLead" style="float:right;">등록하기</button>
				<!-- <button class="ui basic black button" id="insert" style="float:right;">등록</button> -->
			</h1>
			<div class="search">
				<div class="1st_line lane lane2 lane3">
					<div class="select">
						<select>
							<option>미접촉</option>
							<option>접촉시도</option>
							<option>접촉중</option>
							<option>접촉금지</option>
							<option>고객전환</option>
							<option>기존고객</option>
						</select>
					</div>
					<div class="input">
						<input type="text" id="searchName" name="leadName" placeholder="유선번호/고객/고객사">
						<button class = "ui inverted primary button" id="searchBtn" style="margin-left:2px;">검색</button>
					</div>
				</div>
				<div class="2nd_line" style="margin-top:10px">
					<div class="ui input right icon" id="rangestart">
						<i class="calendar icon"></i> <input type="text"
							placeholder="Start">
					</div>

					<div class="ui input right icon" id="rangeend">
						<i class="calendar icon"></i> <input type="text" placeholder="End">
					</div>
				</div>
			</div>
			<div id="content_div">
				<hr>
				<c:forEach var="lead"  items="${leadList}">
					<div class="resultContents">
						<input type="hidden"  value="${lead.leadId}">
						<div class="ui right icon one">
							<i class="ellipsis vertical icon" style="z-index:99999"></i>
							<p><c:out value="${lead.userName}"/></p>
						</div>
						<div style="padding-left:15px;">
							<c:if test="${lead.phone != null}">
								<c:out value="${lead.phone}"/>/
							</c:if>
							<c:if test="${lead.email != null}">
								<c:out value="${lead.email}"/>
							</c:if>
						</div>
						<div style="padding-left:15px;">
							<c:if test="${lead.enrollDate != null}">
								<c:out value="${lead.enrollDate}"/>/
							</c:if>
							<c:if test="${lead.contStatus != null}">
								<c:if test="${lead.contStatus eq '1'}">
									미접촉
								</c:if>
								<c:if test="${lead.contStatus eq '2'}">
									접촉시도
								</c:if>
								<c:if test="${lead.contStatus eq '3'}">
									접촉중
								</c:if>
								<c:if test="${lead.contStatus eq '4'}">
									접촉금지
								</c:if>
								<c:if test="${lead.contStatus eq '5'}">
									고객전환
								</c:if>
								<c:if test="${lead.contStatus eq '6'}">
									기존고객
								</c:if>
								<!-- 전환일 -->
								<%-- <c:if test="${lead.modifyDate ne lead.enrollDate}">
									<c:out value="${lead.modifyDate}"/>/
								</c:if> --%>
							</c:if>
							<%-- <c:if test="${lead.email != null}">
								<c:out value="${lead.email}"/>
							</c:if> --%>
						</div>
						<div class="memo">
							<div class="meet">접촉등록</div>
							<c:if test="${lead.contStatus ne 5}">
								<div class="switch">고객전환</div>
							</c:if>
						</div>
					</div>
				</c:forEach>
				
			</div>
		</div>
	</div>
<script>

	function clickfunction(){
		console.log("HELLO");
	}
	$(function(){
		$(".meet").click(function(){
			event.stopPropagation();
			var n1 = $(this).parent().parent().children('input').val();
			console.log("n1 : " + n1) ;
			$("#leadId").val(n1);
			
			$('.ui.modal.history').modal({
					onShow : function(value){
						$('#year_calendar').calendar({
							type: 'date',
							formatter : {
							date: function(date, settings){
								if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
								return year + '-' + month + '-' + day;
								}
							}
						}
						)
						}}).modal('show');
			/* $('.ui.modal.history').show();	 */
		$(".exit_Btn").click(function() {
			$('.ui.modal.history').modal('hide');
		})
	});
		
		$("#searchBtn").click(function(){
			console.log($("#searchName").val());
			$.ajax({
				url:"search.le",
				data:{leadName:$("#searchName").val()},
				type:"get",
				success:function(data){
					console.log(data.length);
					var content = $("#content_div");
					content.html('<hr>');
					
					 for(var i=0; i<data.length; i++) {
						 var str = "";
						 str += '<div class="resultContents">';
						 str += '<input type="hidden"  value="' +data[i].leadId + '">';
						 str += '<div class="ui right icon one"><i class="ellipsis vertical icon" style="z-index:99999"></i>';
						 str += '<p>' + data[i].userName + '</p></div>'
						 str += '<div style="padding-left:15px;">';
						if(data[i].phone !== null){
							str +=data[i].phone + " / "; 
						}
						if(data[i].email !== null){ 
							str +=data[i].email;
						}
						str +='</div><div style="padding-left:15px;">';
						if(data[i].enrollDate !== null){
							var dateEnroll = data[i].enrollDate.split(" ");
							var year = dateEnroll[1]
							var month = dateEnroll[0]
							var day = dateEnroll[2]
							month = month.split("월");
							day = day.split(",")
							month = month[0]
							year = year.split(",");
							day = day[0]
							str += day + "-" + month + "-" + year[0] + " / ";
						}
						if(data[i].contStatus !== null){
							if(data[i].contStatus === 1){
								str += "미접촉 ";	
							}
							if(data[i].contStatus === 2){
								str += "접촉시도 ";	
							}
							if(data[i].contStatus === 3){
								str += "접촉중 ";	
							}
							if(data[i].contStatus === 4){
								str += "접촉금지 ";	
							}
							if(data[i].contStatus === 5){
								str += "고객전환 ";	
							}
							if(data[i].contStatus === 6){
								str += "기존고객  ";	
							}
							
						} 
						/* if(data[i].modifyDate !== null){
							str +=	data[i].modifyDate+ " / ";
						} */
						str +='</div><div class="memo"><div class="meet">접촉등록</div>'
						if(data[i].contStatus != 5){
							str +='<div class="switch">고객전환</div>'
						}
						str +='</div></div>';
						console.log(str);
						content.append(str)
					 }
					 /* $('.ui.modal.product').modal('show');  */
					/* $table_tbody = $('#result_Table_Modal tbody');
					$table_tbody.html('');
					console.log("dl" + data);
					 for(var i=0; i<data.length; i++) {
						$table_tbody.append('<tr><td><input name="chk" type="checkbox"></td><td>'+data[i].pdtName+'</td><td>'
								+data[i].quantity +'</td><td>'
								+data[i].standard+'<input type="hidden" value="'+data[i].pdtId+'"> </td><td>'+data[i].price+'</td></tr>' );
					 } */ 
					 
					/*  $table_tbody.click(function(e){
						 var name = e.target.parentElement.children[1].innerText;
						 var id = e.target.parentElement.children[1].children[0].value;
						 $('#managerText').val(name);
						 $('#manager_hidden').val(id);
						 $('.ui.modal.manager').modal('hide');
					 })  */
					 //제이쿼리 재실행
					 $("#insertLead").click(function(){
							location.href="goInsert.le";
						})
						
						$(".switch").click(function(){
							var n1 = $(this).parent().parent().children('input').val();
							event.stopPropagation();
							console.log(n1);
							location.href="goInsert.cl?num="+n1;
						})
						
						 $(".ellipsis.vertical.icon:not(.resultContents)").click(function(){
							 
							if($(this).hasClass("memoOn")){
								event.stopPropagation();
								$(this).removeClass("memoOn");
								
								$(this).parent().parent().children(".memo").hide();
							} else{
								event.stopPropagation();
								$(this).addClass("memoOn");
								
								$(this).parent().parent().children(".memo").show();
								
							} 
						/* $("a").click(function(){
							if($(this).hasClass("memoOn")){
								event.stopPropagation();
								$(this).removeClass("memoOn");
								$(this).parent().parent().children(".memo").hide();
							} else{
								event.stopPropagation();
								$(this).addClass("memoOn");
								$(this).parent().parent().children(".memo").show();
							}
						}) */
						});
						 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
					        
					        // 열리는 메뉴
					        $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
					        $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(2).addClass("on");

						
							
							$(".resultContents:not(.ellipsis)").mouseenter(function(){
								$(this).css("border","#6199B9 solid 1px");
								$(this).children().css("color","#6199B9");
								$(this).children().children("p").css("color","#6199B9");
								$(this).find(".memo").css("color","#000");
								$(this).css("cursor","pointer");
								console.log("enter");
							}).mouseleave(function(){
								
								$(this).css("border","1px solid #DEE2E8");
								$(this).children().css("color","#000");
								$(this).children().children("p").css("color","#000");
								console.log("out");
							}).click(function(){
								
								var n = $(this).children().eq(0).val();
								console.log(n);
								location.href="selOne.le?leadId="+n;
								
								
							})
							$('html').click(function(e){ 
								   if(!$(e.target).hasClass("ellipsis")) { 
								      $(".memo").hide();
								   } 
							});
				},
				error:function(status){
					console.log(status);
				}
			})
		})
		
		$("#insertLead").click(function(){
			location.href="goInsert.le";
		})
		
		$(".switch").click(function(){
			var n1 = $(this).parent().parent().children('input').val();
			event.stopPropagation();
			console.log(n1);
			location.href="goInsert.cl?num="+n1;
		})
		
		
		 $(".ellipsis.vertical.icon:not(.resultContents)").click(function(){
			 
			if($(this).hasClass("memoOn")){
				event.stopPropagation();
				$(this).removeClass("memoOn");
				
				$(this).parent().parent().children(".memo").hide();
			} else{
				event.stopPropagation();
				$(this).addClass("memoOn");
				
				$(this).parent().parent().children(".memo").show();
				
			} 
		/* $("a").click(function(){
			if($(this).hasClass("memoOn")){
				event.stopPropagation();
				$(this).removeClass("memoOn");
				$(this).parent().parent().children(".memo").hide();
			} else{
				event.stopPropagation();
				$(this).addClass("memoOn");
				$(this).parent().parent().children(".memo").show();
			}
		}) */
		});
		 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
	        
	        // 열리는 메뉴
	        $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
	        $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(2).addClass("on");

		
			
			$(".resultContents:not(.ellipsis)").mouseenter(function(){
				$(this).css("border","#6199B9 solid 1px");
				$(this).children().css("color","#6199B9");
				$(this).children().children("p").css("color","#6199B9");
				$(this).find(".memo").css("color","#000");
				$(this).css("cursor","pointer");
				console.log("enter");
			}).mouseleave(function(){
				
				$(this).css("border","1px solid #DEE2E8");
				$(this).children().css("color","#000");
				$(this).children().children("p").css("color","#000");
				console.log("out");
			}).click(function(){
				
				var n = $(this).children().eq(0).val();
				console.log(n);
				location.href="selOne.le?leadId="+n;
				
				
			})
			$('html').click(function(e){ 
				   if(!$(e.target).hasClass("ellipsis")) { 
				      $(".memo").hide();
				   } 
			});
			
			
	})
</script>
</body>
</html>