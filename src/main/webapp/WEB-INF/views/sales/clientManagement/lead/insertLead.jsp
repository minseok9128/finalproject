<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunityEnroll.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">


</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					잠재고객
					<button id="calcel_Btn">취소</button>
					<button id="submit_Btn">작성</button>
				</h1>
				<br> <br>
			</div>
			<div id="content_div">
				<table id="table">
					<tr class="td">
						<td>이름<b class="point">*</b></td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>고객사</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>부서</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>직책</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>첩촉구분</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">자사홈페이지</option>
								<option value="pro2">인터넷검색</option>
								<option value="pro3">지인소개</option>
								<option value="pro4">제품설명회</option>
								<option value="pro5">세미나</option>
								<option value="pro6">전화(인바운드)</option>
								<option value="pro7">기타</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>접촉상태</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">미접촉</option>
								<option value="pro2">접촉시도</option>
								<option value="pro3">접촉중</option>
								<option value="pro4">접촉금지</option>
								<option value="pro5">고객전환</option>
								<option value="pro6">기존고객</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>가망등급</td>
						<td><select name="day" id="day_select"
							style="width: 600px; height: 33px;">
								<option value="pro1">S등급</option>
								<option value="pro2">A등급</option>
								<option value="pro3">B등급</option>
								<option value="pro4">C등급</option>
								<option value="pro5">등급없음</option>
						</select></td>
					</tr>
					<tr class="td">
						<td>휴대번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>유선번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>메일</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>팩스번호</td>
						<td><input type="text" id="propose_Name" class="common_Text1">
						</td>
					</tr>
					<tr class="td">
						<td>주소</td>
						<td><input type="text" id="propose_Name" class="common_Text1"
							style="background: #EAEAEA; width: 100px"></td>
						<td><button class="search_Btn" style="float: left">검색</button></td>
					</tr>
					<tr class="td">
						<td>비고</td>
						<td><textarea id="contents"></textarea></td>
					</tr>
				</table>

				<h1 id="propose_report">
					이력
					<button id="propose_report_Btn">추가</button>
				</h1>
				<table id="table2" style="text-align: center">
					<tr class="td">
						<td style="border-right: 1px solid #E5E5E5; background: white;">접촉일</td>
						<td style="border-right: 1px solid #E5E5E5; background: white;">접촉구분</td>
						<td rowspan="2"
							style="border-right: 1px solid #E5E5E5; background: white;">삭제</td>

					</tr>


					<tr>
						<td colspan="2"
							style="border-right: 1px solid #E5E5E5; background: white;">접촉내용</td>


					</tr>

					<tr class="td" style="border-top: 2px solid #E5E5E5">
						<td style="border-right: 1px solid #E5E5E5;">캘린더</td>
						<td style="border-right: 1px solid #E5E5E5;">
							<select name="day" id="day_select"
							style="width: 300px; height: 33px;">
								<option value="pro1">전화</option>
								<option value="pro2">메일</option>
								<option value="pro3">방문</option>
								<option value="pro4">기타</option>
							</select>
						</td>
						<td rowspan="2" style="border-right: 1px solid #E5E5E5;"><i
							class="trash alternate outline icon"></i></td>

					</tr>


					<tr>
						<td colspan="2" style="border-right: 1px solid #E5E5E5;">
							<textarea id="contents" style="width:700px; height:50px;"></textarea>
						</td>


					</tr>


				</table>


			</div>
		</div>
	</div>
</body>
</html>