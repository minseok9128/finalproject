<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunityEnroll3.css ">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">


</head>
<body>
<jsp:include page="../../../common/modal/leadHistory.jsp"></jsp:include>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>잠재고객</h1>
				<h3><c:out value="${lead.userName}"/></h3>
				<c:set var="leadId" value="${lead.leadId}"/>
				<button id="calcel_Btn" style="float: right; margin-bottom:10px;">수정</button>
				<div
					style="display: inline-block; margin-left: 242px; float: right;">
					<h1 id="icon">
						<i class="ellipsis vertical icon"></i>
					</h1>
				</div>

				<br> <br>
			</div>
			<div id="content_div">
				<table id="table">
					<tr class="td">
						<td>회사명</td>
						<td>
							<c:out value="${lead.company}"/>
						</td>
					</tr>
					<tr class="td">
						<td>부서</td>
						<td>
							<c:out value="${lead.department}"/>
						</td>
					</tr>
					<tr class="td">
						<td>직책</td>
						<td>
							<c:out value="${lead.job}"/>
						</td>
					</tr>
					<tr class="td">
						<td>접촉구분</td>
						<td>
							<c:if test="${lead.contCategory eq '1'}">
								자사홈페이지
							</c:if>
							<c:if test="${lead.contCategory eq '2'}">
								인터넷검색
							</c:if>
							<c:if test="${lead.contCategory eq '3'}">
								지인소개
							</c:if>
							<c:if test="${lead.contCategory eq '4'}">
								제품설명회
							</c:if>
							<c:if test="${lead.contCategory eq '5'}">
								세미나
							</c:if>
							<c:if test="${lead.contCategory eq '6'}">
								전화(인바운드)
							</c:if>
							<c:if test="${lead.contCategory eq '7'}">
								기타
							</c:if>
						</td>
					</tr>
					<tr class="td">
						<td>접촉상태</td>
						<td>
							<c:if test="${lead.contStatus eq '1'}">
								미접촉
							</c:if>
							<c:if test="${lead.contStatus eq '2'}">
								접촉시도
							</c:if>
							<c:if test="${lead.contStatus eq '3'}">
								접촉중
							</c:if>
							<c:if test="${lead.contStatus eq '4'}">
								접촉금지
							</c:if>
							<c:if test="${lead.contStatus eq '5'}">
								고객전환
							</c:if>
							<c:if test="${lead.contStatus eq '6'}">
								기존고객
							</c:if>
							
						</td>
					</tr>
					<tr class="td">
						<td>가망등급</td>
						<td>
							<c:out value="${lead.rank}"/>등급 
						</td>
					</tr>
					<tr class="td">
						<td>고객등록</td>
						<td>
							<c:if test="${lead.contStatus eq '5'}">
								등록
							</c:if>
							<c:if test="${lead.contStatus ne '5'}">
								미등록
							</c:if>
						</td>
					</tr>
					<tr class="td">
						<td>휴대번호</td>
						<td>
							<c:out value="${lead.phone}"/>
						</td>
					</tr>
					<tr class="td">
						<td>회사번호</td>
						<td>
							<c:out value="${lead.tel}"/>
						</td>
					</tr>
					<tr class="td">
						<td>메일</td>
						<td>
							<c:out value="${lead.email}"/>
						</td>
					</tr>
					<tr class="td">
						<td>팩스번호</td>
						<td>
							<c:out value="${lead.fax}"/>
						</td>
					</tr>
					<tr class="td">
						<td>주소</td>
						<td>
							<c:out value="${fn:split(lead.address,'/')[0]}" /> | 
							<c:out value="${fn:split(lead.address,'/')[1]}" />
							<c:out value="${fn:split(lead.address,'/')[2]}" />
						</td>
					</tr>
					<tr class="td">
						<td>비고</td>
						<td>
							<c:out value="${lead.remark}"/>
						</td>
					</tr>
				</table>

				<!-- <h1 id="propose_report">첨부자료</h1>
				<table id="table3" style="text-align: center">
					<tr class="td">
						<td style="text-align: left">엑셀파일!</td>
						<td style="width: 10%">
							<button>다운로드</button>
						</td>
					</tr>

				</table>

				<table>

					<tr>
						<h3>메모 (0)</h3>
						<td><textarea id="contents"
								style="width: 600px; height: 50px;"></textarea></td>
						<td><button
								style="width: 100px; height: 50px; margin-left: 10px;">메모</button></td>
					</tr>
				</table>

				<h1 id="propose_report">
					동일 업체 잠재고객
				</h1>
				<table id="table2" style="text-align: center">
					<tr class="td" style="background: white">
						<td colspan="2"
							style="border-right: 1px solid #E5E5E5; width: 30%">이름</td>
						<td style="border-right: 1px solid #E5E5E5; width: 15%">부서</td>
						<td style="border-right: 1px solid #E5E5E5;">직책</td>
						<td style="border-right: 1px solid #E5E5E5;">등록자</td>
						<td style="border-right: 1px solid #E5E5E5;">등록일</td>
						<td style="border-right: 1px solid #E5E5E5;">전환일</td>
					</tr>

					<tr class="td">
						<td colspan="2"
							style="border-right: 1px solid #E5E5E5; width: 30%">황규환</td>
						<td style="border-right: 1px solid #E5E5E5;">재무</td>
						<td style="border-right: 1px solid #E5E5E5;">사원</td>
						<td style="border-right: 1px solid #E5E5E5;">서범수</td>
						<td style="border-right: 1px solid #E5E5E5;">2020.01.02</td>
						<td style="border-right: 1px solid #E5E5E5;">2020.01.20</td>
					</tr>
				</table> -->
				
				<h1 id="propose_report">
					이력
				</h1>
				<button id="addHistory" style="float:right; margin-bottom:10px;" class="ui inverted primary button float">추가</button>
				<table id="table2" style="text-align: center">
					<tr class="td" style="background: white">
						<td colspan="2"
							style="border-right: 1px solid #E5E5E5; width: 30%">접촉일</td>
						<td style="border-right: 1px solid #E5E5E5; width: 15%">구분</td>
						<td style="border-right: 1px solid #E5E5E5; width: 40%">접촉내용</td>
						<td style="border-right: 1px solid #E5E5E5;">등록자</td>
					</tr>
					<c:forEach var="his" items="${list}" >
						<tr class="td">
							<td colspan="2"
								style="border-right: 1px solid #E5E5E5; width: 30%">
								<c:out value="${his.contactDate}"/>
							</td>
							<td style="border-right: 1px solid #E5E5E5; width: 15%">
								<c:if test="${his.category eq '1'}">
									전화
								</c:if>
								<c:if test="${his.category eq '2'}">
									메일
								</c:if>
								<c:if test="${his.category eq '3'}">
									방문
								</c:if>
								<c:if test="${his.category eq '4'}">
									기타
								</c:if>
							</td>
							<td style="border-right: 1px solid #E5E5E5; width: 40%">
								<c:out value="${his.contactContent}"/>
							</td>
							<td style="border-right: 1px solid #E5E5E5;">
								<c:out value="${sessionScope.loginEmp.empName }"/>
							</td>
						</tr>
					</c:forEach>
				</table>

			</div>
		</div>
	</div>
	<script>
	//모달에 달력있을 때에는 다시 액션을 모달에 설정해줘야함
		$(function(){
			
			 $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
		        
		        // 열리는 메뉴
		        $(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
		        $(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(2).addClass("on");

			
			$("#addHistory").click(function(){
				$("#leadId").val(<c:out value="${leadId}"/>);
				
				$('.ui.modal.history').modal({
						onShow : function(value){
							$('#year_calendar').calendar({
								type: 'date',
								formatter : {
								date: function(date, settings){
									if(!date) return '';
										var day = date.getDate();
										var month = date.getMonth() +1;
										var year = date.getFullYear();
									return year + '-' + month + '-' + day;
									}
								}
							}
							)
							}}).modal('show');
				/* $('.ui.modal.history').show();	 */
			$(".exit_Btn").click(function() {
				$('.ui.modal.history').modal('hide');
			})
		});
		});

	</script>
</body>
</html>