<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/jy/clientManagement/Lead/LeadList2.css ">
<style>
.half {
	width: 49.5%;
	display: inline-block;
}

.select, .input {
	display: inline-block;
}

.client input {
	width: 100%;
}

.client {
	padding-top: 10px;
	padding-left: 10px;
}

.select {
	width: 49.5%;
	padding-top: 10px;
	padding-left: 10px;
}

.search div input {
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
	padding-left: 5px;
}

.select select {
	width: 100%;
	height: 36px;
	border: 1px solid #D7DBDF;
	border-radius: .28571429rem;
}

.input input {
	width: 78%;
	border-radius: .28571429rem;
	border: 1px solid #D7DBDF;
}

.input {
	width: 49.5%;
	padding-left: 9.5px;
}

.ui.blue.basic.button {
	margin-right: 0;
}

.float {
	float: right;
}

.2nd_line {
	margin-top: 10px;
}
#content_div{
	padding-left:0;
}
</style>
</head>
<body>
	<jsp:include page="../../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<!-- <div id="header_div">
					<h1>
						잠재고객
						<button>등록</button>
					</h1>
					<input type=text id="customer_text" placeholder="유선번호/고객/고객사">
					<input type=text id="category" placeholder="전체">
					<button>검색</button>
				</div> -->

			<h1>
				잠재고객
				<button class="ui basic black button" style="float:right;">등록</button>
			</h1>
			<div class="search">
				<div class="1st_line lane lane2 lane3">
					<div class="select">
						<select>
							<option>미접촉</option>
							<option>접촉시도</option>
							<option>접촉중</option>
							<option>접촉금지</option>
							<option>고객전환</option>
							<option>기존고객</option>
						</select>
					</div>
					<div class="input">
						<input type="text" placeholder="유선번호/고객/고객사">
						<button class="ui blue basic button float">검색</button>
					</div>
				</div>
				<div class="2nd_line" style="margin-top:10px">
					<div class="ui input right icon" id="rangestart">
						<i class="calendar icon"></i> <input type="text"
							placeholder="Start">
					</div>

					<div class="ui input right icon" id="rangeend">
						<i class="calendar icon"></i> <input type="text" placeholder="End">
					</div>
				</div>
			</div>
			<div id="content_div">
				<div>
					<button id="filter_Btn">필터</button>
					<P>60건</P>
					<p class="filters" style="font-weight: bold;">등록일 ▼</p>
					<p class="filters">고객사 |</p>
					<p class="filters">접촉일 |</p>
				</div>
				<hr>
				<div class="resultContents">
					<div class="ui input right icon" >
						<i class="ellipsis vertical icon"></i>
						<p>고객포텐셜</p>
					</div>
					<div style="padding-left:15px;">
						<c:out value="2020.01.01"/>/
						<c:out value="미접촉"/>/
						<c:out value="접촉일(2020.01.12)"/>/
						<c:out value="안동환"/>
					</div>
				</div>
				<div class="resultContents">
					<div class="ui input right icon" >
						<i class="ellipsis vertical icon"></i>
						<p>고객포텐셜</p>
					</div>
					<div style="padding-left:15px;">
						<c:out value="2020.01.01"/>/
						<c:out value="미접촉"/>/
						<c:out value="접촉일(2020.01.12)"/>/
						<c:out value="안동환"/>
					</div>
				</div>
				<div class="resultContents">
					<div class="ui input right icon" >
						<i class="ellipsis vertical icon"></i>
						<p>고객포텐셜</p>
					</div>
					<div style="padding-left:15px;">
						<c:out value="2020.01.01"/>/
						<c:out value="미접촉"/>/
						<c:out value="접촉일(2020.01.12)"/>/
						<c:out value="안동환"/>
					</div>
				</div>
				<!-- <div class="resultContents">
					<img src="images/logo.png" align="left" width="90" height="90">
					<p>윤바이오</p>
					<p>2020.01.02</p>
					<p>고객전환</p>
					<p>전환일(2020.01.02)</p>
					<p>김영수</p>
				</div>
				<div class="resultContents">
					<img src="images/logo.png" align="left" width="90" height="90">
					<p>황성원</p>
					<p>2019.11.21</p>
					<p>접촉중</p>
					<p>전환일(2019.12.19)</p>
					<p>김영수</p>
				</div> -->
			</div>
		</div>
	</div>
<script>
	$(function(){
			$(".resultContents").mouseenter(function(){
				$(this).css("border","#6199B9 solid 1px");
				$(this).children().css("color","#6199B9");
				$(this).children().children("p").css("color","#6199B9");
				$(this).css("cursor","pointer");
				console.log("enter");
			}).mouseleave(function(){
				
				$(this).css("border","1px solid #DEE2E8");
				$(this).children().css("color","#000");
				$(this).children().children("p").css("color","#000");
				console.log("out");
			})
	})
</script>
</body>
</html>