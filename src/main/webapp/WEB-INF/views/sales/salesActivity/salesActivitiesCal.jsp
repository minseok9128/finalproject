<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <!-- 공통 css-->
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
    <!-- fullcalendar -->
    <script src='${ contextPath }/resources/fullcalendar/fullcalendar.js'></script>
	<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/fullcalendar.css">
	<script src='${ contextPath }/resources/fullcalendar/daygrid.js'></script>
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/daygrid.css">
    <script src='${ contextPath }/resources/fullcalendar/ko.js'></script>
    <script src='${ contextPath }/resources/fullcalendar/interaction.js'></script>
    <script src='${ contextPath }/resources/fullcalendar/timegrid.js'></script>
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/timegrid.css">
    <script>

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var priList = '${priList}';
        var empNo = '${sessionScope.loginEmp.empNo}';
		var dateInfo = '';
		var cataName = '';
		var purposeName = '';
		var category = '';
		var date = '';
		var str = '';
		var actCategory = '';
		var purpose = '';
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction', 'dayGrid', 'timeGrid'],
            header: {
                left: 'prev',
                center: 'title',  
                right: 'next'
            },
            editable: false,
            droppable: true, // this allows things to be dropped onto the calendar
            selectable: true,
            eventClick: function(info) {
				var dateArr = info.event.start.toLocaleDateString().split('. ');
				date = '';
				date += dateArr[0] + '-';
				date += dateArr[1] + '-';
				date += dateArr[2].split('.')[0];
				
				str = '<i class="ico_caleder"></i><span>' + date + '<span>';
				$('.sc_title').empty();
				$('.sc_title').append(str);
				$.ajax({
					url:"dateInfo.sa",
					data:{date:date,empNo:empNo},
					type:"post",
					success:function(data){
						$('.sc_list').empty();
						dateInfo = '';
						cataName = '';
						purposeName = '';
						for(var i = 0; i < data.calList.length; i++){
							category = data.calList[i].CAL_CATEGORY;
							if(category == 1){
								cataName = '회의';
							}else if(category == 2){
								cataName = '교육';
							}else if(category == 3){
								cataName = '회식';
							}else if(category == 4){
								cataName = '세미나';
							}else if(category == 5){
								cataName = '업무';
							}else if(category == 6){
								cataName = '휴가';
							}else if(category == 7){
								cataName = '기타';
							}
							
							dateInfo = '<li><a href="javascript:void(0)" class="priDetail"><table><tr><td rowspan="2" class="tdCenter"><input type="hidden" class="calId" value='+ data.calList[i].CAL_ID +'><i class="ico ico_personal"></i></td><td>'+ data.calList[i].CAL_START +'</td><td>'+ cataName +'</td></tr><tr><td>'+ data.calList[i].CAL_END +'</td><td><div class="ellipsis">'+ data.calList[i].CAL_CONTENT +'</div></td></tr></table></a></li>';							
							$('.sc_list').append(dateInfo);
							
							}
						for(var i = 0; i < data.actList.length; i++){
							actCategory = data.actList[i].CATEGORY;
							purpose = data.actList[i].PURPOSE;
							if(actCategory == 1){
								cateName = '전화';
							}else if(actCategory == 2){
								cateName = '메일';
							}else if(actCategory == 3){
								cateName = '방문';
							}else if(actCategory == 4){
								cateName = '기타';
							}
							if(purpose == 1){
								purposeName = '인사';
							}else if(purpose == 2){
								purposeName = '제품소개';
							}else if(purpose == 3){
								purposeName = '데모시연';
							}else if(purpose == 4){
								purposeName = '정보수집';
							}else if(purpose == 5){
								purposeName = '제안';
							}else if(purpose == 6){
								purposeName = '업무협의';
							}else if(purpose == 7){
								purposeName = '협상';
							}else if(purpose == 8){
								purposeName = '계약';
							}else if(purpose == 9){
								purposeName = '기타';
							}
							if(data.actList[i].COMPLETE_YN == 'N'){
								dateInfo = '<li><a href="javascript:void(0)" class="actDetail"><input type="hidden" name="activityId" value="'+ data.actList[i].ACTIVITY_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_plan"></i></td><td>'+ data.actList[i].ACTIVITY_START +'</td><td>';
								if(data.actList[i].COM_NAME != ''){
									dateInfo += '<span>' + data.actList[i].COM_NAME + '</span> / ';
								}
								dateInfo += '<span>' + data.actList[i].CLIENT_NAME +'</span></td></tr><tr><td>'+ data.actList[i].ACTIVITY_END +'</td><td>'+ cateName + ' / ' + purposeName +'</td></tr></table></a></li>';									
							}else{
								dateInfo = '<li><a href="javascript:void(0)" class="actDetail"><input type="hidden" name="activityId" value="'+ data.actList[i].ACTIVITY_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_run"></i></td><td>'+ data.actList[i].ACTIVITY_START +'</td><td>';
								if(data.actList[i].COM_NAME != ''){
									dateInfo += '<span>' + data.actList[i].COM_NAME + '</span> / ';
								}
								dateInfo += '<span>' + data.actList[i].CLIENT_NAME +'</span></td></tr><tr><td>'+ data.actList[i].ACTIVITY_END +'</td><td>'+ cateName + ' / ' + purposeName +'</td></tr></table></a></li>';
							}
							$('.sc_list').append(dateInfo);
						}
						//제안
						for(var i = 0; i < data.proposeList.length; i++){
							dateInfo = '';
							dateInfo = '<li><a href="javascript:void(0)" class="ppsDetail"><input type="hidden" value="'+ data.proposeList[i].PPS_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_work"></i></td><td>제안 / ' + data.proposeList[i].PPS_NAME + '</td></tr><tr><td>';
							if(data.proposeList[i].COM_NAME != ""){
								dateInfo += '<span>' + data.proposeList[i].COM_NAME + '</span> / ';
							}
							dateInfo += '<span>' + data.proposeList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
							$('.sc_list').append(dateInfo);
						}
						
						//견적
						for(var i = 0; i < data.estList.length; i++){
							dateInfo = '';
							dateInfo = '<li><a href="javascript:void(0)" class="estDetail"><input type="hidden" value="'+ data.estList[i].EST_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_friendly"></i></td><td>견적 / ' + data.estList[i].EST_NAME + '</td></tr><tr><td>';
							if(data.estList[i].COM_NAME != ""){
								dateInfo += '<span>' + data.estList[i].COM_NAME + '</span> / ';
							}
							dateInfo += '<span>' + data.estList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
							$('.sc_list').append(dateInfo);
						}
						
						//계약
						for(var i = 0; i < data.contList.length; i++){
							dateInfo = '';
							dateInfo = '<li><a href="javascript:void(0)" class="contDetail"><input type="hidden" value="'+ data.contList[i].CONT_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_todo"></i></td><td>계약 / ' + data.contList[i].CONT_NAME + '</td></tr><tr><td>';
							if(data.contList[i].COM_NAME != ""){
								dateInfo += '<span>' + data.contList[i].COM_NAME + '</span> / ';
							}
							dateInfo += '<span>' + data.contList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
							$('.sc_list').append(dateInfo);
						}
						console.log($(this));
					}//success end
				});
            },
			dateClick: function(info){
				date = info.dateStr;
				str = '<i class="ico_caleder"></i><span>' + date + '<span>';
				$('.sc_title').empty();
				$('.sc_title').append(str);
				$.ajax({
					url:"dateInfo.sa",
					data:{date:date,empNo:empNo},
					type:"post",
					success:function(data){
						$('.sc_list').empty();
						dateInfo = '';
						cataName = '';
						purposeName = '';
						//개인일정
						for(var i = 0; i < data.calList.length; i++){
							category = data.calList[i].CAL_CATEGORY;
							dateInfo = '';
							if(category == 1){
								cataName = '회의';
							}else if(category == 2){
								cataName = '교육';
							}else if(category == 3){
								cataName = '회식';
							}else if(category == 4){
								cataName = '세미나';
							}else if(category == 5){
								cataName = '업무';
							}else if(category == 6){
								cataName = '휴가';
							}else if(category == 7){
								cataName = '기타';
							}
							
							dateInfo = '<li><a href="javascript:void(0)" class="priDetail"><table><tr><td rowspan="2" class="tdCenter"><input type="hidden" class="calId" value='+ data.calList[i].CAL_ID +'><i class="ico ico_personal"></i></td><td>'+ data.calList[i].CAL_START +'</td><td>'+ cataName +'</td></tr><tr><td>'+ data.calList[i].CAL_END +'</td><td><div class="ellipsis">'+ data.calList[i].CAL_CONTENT +'</div></td></tr></table></a></li>';							
							$('.sc_list').append(dateInfo);
							
							}
						//영업활동
						for(var i = 0; i < data.actList.length; i++){
							actCategory = data.actList[i].CATEGORY;
							purpose = data.actList[i].PURPOSE;
							dateInfo = '';
							if(actCategory == 1){
								cateName = '전화';
							}else if(actCategory == 2){
								cateName = '메일';
							}else if(actCategory == 3){
								cateName = '방문';
							}else if(actCategory == 4){
								cateName = '기타';
							}
							if(purpose == 1){
								purposeName = '인사';
							}else if(purpose == 2){
								purposeName = '제품소개';
							}else if(purpose == 3){
								purposeName = '데모시연';
							}else if(purpose == 4){
								purposeName = '정보수집';
							}else if(purpose == 5){
								purposeName = '제안';
							}else if(purpose == 6){
								purposeName = '업무협의';
							}else if(purpose == 7){
								purposeName = '협상';
							}else if(purpose == 8){
								purposeName = '계약';
							}else if(purpose == 9){
								purposeName = '기타';
							}
							if(data.actList[i].COMPLETE_YN == 'N'){
								dateInfo = '<li><a href="javascript:void(0)" class="actDetail"><input type="hidden" name="activityId" value="'+ data.actList[i].ACTIVITY_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_plan"></i></td><td>'+ data.actList[i].ACTIVITY_START +'</td><td>';
								if(data.actList[i].COM_NAME != ''){
									dateInfo += '<span>' + data.actList[i].COM_NAME + '</span> / ';
								}
								dateInfo += '<span>' + data.actList[i].CLIENT_NAME +'</span></td></tr><tr><td>'+ data.actList[i].ACTIVITY_END +'</td><td>'+ cateName + ' / ' + purposeName +'</td></tr></table></a></li>';									
							}else{
								dateInfo = '<li><a href="javascript:void(0)" class="actDetail"><input type="hidden" name="activityId" value="'+ data.actList[i].ACTIVITY_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_run"></i></td><td>'+ data.actList[i].ACTIVITY_START +'</td><td>';
								if(data.actList[i].COM_NAME != ''){
									dateInfo += '<span>' + data.actList[i].COM_NAME + '</span> / ';
								}
								dateInfo += '<span>' + data.actList[i].CLIENT_NAME +'</span></td></tr><tr><td>'+ data.actList[i].ACTIVITY_END +'</td><td>'+ cateName + ' / ' + purposeName +'</td></tr></table></a></li>';
							}
							$('.sc_list').append(dateInfo);
						}
						//제안
						for(var i = 0; i < data.proposeList.length; i++){
							dateInfo = '';
							dateInfo = '<li><a href="javascript:void(0)" class="ppsDetail"><input type="hidden" value="'+ data.proposeList[i].PPS_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_work"></i></td><td>제안 / ' + data.proposeList[i].PPS_NAME + '</td></tr><tr><td>';
							if(data.proposeList[i].COM_NAME != ""){
								dateInfo += '<span>' + data.proposeList[i].COM_NAME + '</span> / ';
							}
							dateInfo += data.proposeList[i].CLIENT_NAME + '</td></tr></table></a></li>';
							$('.sc_list').append(dateInfo);
						}
						
						//견적
						for(var i = 0; i < data.estList.length; i++){
							dateInfo = '';
							dateInfo = '<li><a href="javascript:void(0)" class="estDetail"><input type="hidden" value="'+ data.estList[i].EST_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_friendly"></i></td><td>견적 / ' + data.estList[i].EST_NAME + '</td></tr><tr><td>';
							if(data.estList[i].COM_NAME != ""){
								dateInfo += data.estList[i].COM_NAME + ' / ';
							}
							dateInfo += '<span>' + data.estList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
							$('.sc_list').append(dateInfo);
						}
						
						//계약
						for(var i = 0; i < data.contList.length; i++){
							dateInfo = '';
							dateInfo = '<li><a href="javascript:void(0)" class="contDetail"><input type="hidden" value="'+ data.contList[i].CONT_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_todo"></i></td><td>계약 / ' + data.contList[i].CONT_NAME + '</td></tr><tr><td>';
							if(data.contList[i].COM_NAME != ""){
								dateInfo += data.contList[i].COM_NAME + ' / ';
							}
							dateInfo += '<span>' + data.contList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
							$('.sc_list').append(dateInfo);
						}


					}
				});
			},
            lang: 'ko'
            
        });
        calendar.setOption('height', 500);
        calendar.render();
        
        
        $.ajax({
        	url:"priDateList.sa",
			data:{empNo:empNo},
			type:"post",
			success:function(data){
				//계획#5bc0de
				//활동 #a0d546
				$(function(){
					var today = getToday();
				
					str = '<i class="ico_caleder"></i><span>' + today + '<span>';
					$('.sc_title').empty();
					$('.sc_title').append(str);
					
					$.ajax({
						url:"dateInfo.sa",
						data:{date:today,empNo:empNo},
						type:"post",
						success:function(data){
							$('.sc_list').empty();
							dateInfo = '';
							cataName = '';
							purposeName = '';
							//개인일정
							for(var i = 0; i < data.calList.length; i++){
								category = data.calList[i].CAL_CATEGORY;
								dateInfo = '';
								if(category == 1){
									cataName = '회의';
								}else if(category == 2){
									cataName = '교육';
								}else if(category == 3){
									cataName = '회식';
								}else if(category == 4){
									cataName = '세미나';
								}else if(category == 5){
									cataName = '업무';
								}else if(category == 6){
									cataName = '휴가';
								}else if(category == 7){
									cataName = '기타';
								}
								
								dateInfo = '<li><a href="javascript:void(0)" class="priDetail"><table><tr><td rowspan="2" class="tdCenter"><input type="hidden" class="calId" value='+ data.calList[i].CAL_ID +'><i class="ico ico_personal"></i></td><td>'+ data.calList[i].CAL_START +'</td><td>'+ cataName +'</td></tr><tr><td>'+ data.calList[i].CAL_END +'</td><td><div class="ellipsis">'+ data.calList[i].CAL_CONTENT +'</div></td></tr></table></a></li>';							
								$('.sc_list').append(dateInfo);
								
								}
							//영업활동
							for(var i = 0; i < data.actList.length; i++){
								actCategory = data.actList[i].CATEGORY;
								purpose = data.actList[i].PURPOSE;
								dateInfo = '';
								if(actCategory == 1){
									cateName = '전화';
								}else if(actCategory == 2){
									cateName = '메일';
								}else if(actCategory == 3){
									cateName = '방문';
								}else if(actCategory == 4){
									cateName = '기타';
								}
								if(purpose == 1){
									purposeName = '인사';
								}else if(purpose == 2){
									purposeName = '제품소개';
								}else if(purpose == 3){
									purposeName = '데모시연';
								}else if(purpose == 4){
									purposeName = '정보수집';
								}else if(purpose == 5){
									purposeName = '제안';
								}else if(purpose == 6){
									purposeName = '업무협의';
								}else if(purpose == 7){
									purposeName = '협상';
								}else if(purpose == 8){
									purposeName = '계약';
								}else if(purpose == 9){
									purposeName = '기타';
								}
								if(data.actList[i].COMPLETE_YN == 'N'){
									dateInfo = '<li><a href="javascript:void(0)" class="actDetail"><input type="hidden" name="activityId" value="'+ data.actList[i].ACTIVITY_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_plan"></i></td><td>'+ data.actList[i].ACTIVITY_START +'</td><td>';
									if(data.actList[i].COM_NAME != ''){
										dateInfo += '<span>' + data.actList[i].COM_NAME + '</span> / ';
									}
									dateInfo += '<span>' + data.actList[i].CLIENT_NAME +'</span></td></tr><tr><td>'+ data.actList[i].ACTIVITY_END +'</td><td>'+ cateName + ' / ' + purposeName +'</td></tr></table></a></li>';									
								}else{
									dateInfo = '<li><a href="javascript:void(0)" class="actDetail"><input type="hidden" name="activityId" value="'+ data.actList[i].ACTIVITY_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_run"></i></td><td>'+ data.actList[i].ACTIVITY_START +'</td><td>';
									if(data.actList[i].COM_NAME != ''){
										dateInfo += '<span>' + data.actList[i].COM_NAME + '</span> / ';
									}
									dateInfo += '<span>' + data.actList[i].CLIENT_NAME +'</span></td></tr><tr><td>'+ data.actList[i].ACTIVITY_END +'</td><td>'+ cateName + ' / ' + purposeName +'</td></tr></table></a></li>';
								}
								$('.sc_list').append(dateInfo);
							}
							//제안
							for(var i = 0; i < data.proposeList.length; i++){
								dateInfo = '';
								dateInfo = '<li><a href="javascript:void(0)" class="ppsDetail"><input type="hidden" value="'+ data.proposeList[i].PPS_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_work"></i></td><td>제안 / ' + data.proposeList[i].PPS_NAME + '</td></tr><tr><td>';
								if(data.proposeList[i].COM_NAME != ""){
									dateInfo += '<span>' + data.proposeList[i].COM_NAME + '</span> / ';
								}
								dateInfo += data.proposeList[i].CLIENT_NAME + '</td></tr></table></a></li>';
								$('.sc_list').append(dateInfo);
							}
							
							//견적
							for(var i = 0; i < data.estList.length; i++){
								dateInfo = '';
								dateInfo = '<li><a href="javascript:void(0)" class="estDetail"><input type="hidden" value="'+ data.estList[i].EST_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_friendly"></i></td><td>견적 / ' + data.estList[i].EST_NAME + '</td></tr><tr><td>';
								if(data.estList[i].COM_NAME != ""){
									dateInfo += data.estList[i].COM_NAME + ' / ';
								}
								dateInfo += '<span>' + data.estList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
								$('.sc_list').append(dateInfo);
							}
							
							//계약
							for(var i = 0; i < data.contList.length; i++){
								dateInfo = '';
								dateInfo = '<li><a href="javascript:void(0)" class="contDetail"><input type="hidden" value="'+ data.contList[i].CONT_ID +'"><table><tr><td rowspan="2" class="tdCenter"><i class="ico ico_todo"></i></td><td>계약 / ' + data.contList[i].CONT_NAME + '</td></tr><tr><td>';
								if(data.contList[i].COM_NAME != ""){
									dateInfo += data.contList[i].COM_NAME + ' / ';
								}
								dateInfo += '<span>' + data.contList[i].CLIENT_NAME + '</span></td></tr></table></a></li>';
								$('.sc_list').append(dateInfo);
							}


						}
					});
				});
				function getToday(){
					var dateToday = new Date();
					return dateToday.getFullYear() + '-' + ('0' + (dateToday.getMonth()+1)).slice(-2) + '-' + ('0' + dateToday.getDate()).slice(-2);
				}
				
				var pArr = [];
				var actArr = [];
				var planArr = [];
				var workArr = [];
				for(var i = 0; i < data.priList.length; i++){
					
					pArr[i] = {'title':data.priList[i].COUNT,'start': data.priList[i].CAL_DATE, 'end':data.priList[i].CAL_DATE, 'color':'#8484e1'}
					
					calendar.addEvent( pArr[i] );
				}
				for(var i = 0; i < data.actList.length; i++){
					actArr[i] = {'title':data.actList[i].COUNT,'start': data.actList[i].ACTIVITY_DATE, 'end':data.actList[i].ACTIVITY_DATE, 'color':'#a0d546'}
					
					calendar.addEvent( actArr[i] );
				}
				for(var i = 0; i < data.planList.length; i++){
					planArr[i] = {'title':data.planList[i].COUNT,'start': data.planList[i].ACTIVITY_DATE, 'end':data.planList[i].ACTIVITY_DATE, 'color':'#5bc0de'}
				
					calendar.addEvent( planArr[i] );
				}
				var length = data.proposeList.length;
				for(var i = 0; i < data.proposeList.length; i++){
					workArr[i] = {'title':data.proposeList[i].COUNT,'start': data.proposeList[i].SUBMIT_DATE, 'end':data.proposeList[i].SUBMIT_DATE, 'color':'#fdad53'}
				}
				var j = 0;
				for(i; i < data.estList.length + data.proposeList.length; i++){
					workArr[i] = {'title':data.estList[j].COUNT,'start': data.estList[j].EST_DATE, 'end':data.estList[j].EST_DATE, 'color':'#767676'};
					j++;
				}
				j = 0;
				for(i; i < data.contList.length + data.estList.length + data.proposeList.length; i++){
					workArr[i] = {'title':data.contList[j].COUNT,'start': data.contList[j].CONT_DATE, 'end':data.contList[j].CONT_DATE, 'color':'#fc5265'};
					j++;
				}
				for(var i = 0; i < data.contList.length + data.estList.length + data.proposeList.length; i++){
					calendar.addEvent( workArr[i] );
				}
			}
    	});
     	
        //개인일정 선택
		$(document).on("click",".priDetail",function(){
			var calId = $(this).find($('.calId')).val();
			$.ajax({
				url:"priDetail.sa",
				data:{calId:calId},
				type:"post",
				success:function(data){
					$('input[name=calCategory]').each(function(){
						if($(this).val() == data.priDetail.CAL_CATEGORY){
							$(this).prop('checked', true);
						}
					});
					$(function(){
						$('#standard_calendar').calendar({
							type: 'date',
							formatter: {
				                date: function (date, settings) {
				                  if (!date) return '';
				                  var day = "";
				               		if(date.getDate() >= 1 && date.getDate() <= 9){
				                	  	day = '0' + date.getDate();
				                    }else{
				                    	day = date.getDate();
				                    }
				                  var month = "";
				                  if(date.getMonth() >= 0 && date.getMonth() <= 8){
				              	  	month = '0' + (date.getMonth() + 1);
				                  }else{
				                  	month = date.getMonth() + 1;
				                  }
				                  var year = date.getFullYear();
				                  return year + '-' + month + '-' + day;
				            	}
				            },
				            initialDate:data.priDetail.CAL_DATE ,
				            on:'click'
						});
					    $('#rangestart').calendar({
					  	  type: 'time',
					  	  initialDate:data.priDetail.CAL_START,
					  	  endCalendar: $('#rangeend')
					  	});
					  	$('#rangeend').calendar({
					  	  type: 'time',
					  	  initialDate:data.priDetail.CAL_END,
					  	  startCalendar: $('#rangestart')
					  	});
					});
					//$('#privateDate').trigger("click");
					
					/* $('#standard_calendar').calendar({
						date:data.priDetail.CAL_DATE
					}); */
					//$('input[name=calDate]').val(data.priDetail.CAL_DATE);
					//$('input[name=calStart]').val(data.priDetail.CAL_START);
					//$('input[name=calEnd]').val(data.priDetail.CAL_END);
					$('textarea[name=calContent]').val(data.priDetail.CAL_CONTENT);
					$('input[name=calId]').val(data.priDetail.CAL_ID);
					//$('#standard_calendar').calendar("set","focusDate("+ data.priDetail.CAL_DATE +")");
					$('.ui.modal').css('width','600px');
					$('.ui.modal.privateDate').modal('show');
					
				}
			});
		});
        $(document).on("click",".actDetail",function(){
        	var actId = $(this).find($('input[name=activityId]')).val();
        	
        	//console.log(actId);
        	
        	location.href='actDetail.sa?activityId=' + actId;
        });
        $(document).on("click",".ppsDetail",function(){
        	var ppsId = $(this).find($('input[type=hidden]')).val();
        	location.href='offerDetail.of?proposeId=' + ppsId;
        });
        $(document).on("click",".estDetail",function(){
        	var estId = $(this).find($('input[type=hidden]')).val();
        	
        	location.href='EstimateDetail.est?estId=' + estId;
        });
        $(document).on("click",".contDetail",function(){
        	var contId = $(this).find($('input[type=hidden]')).val();
        	
        	location.href='selOne.con?contId=' + contId;
        });
      });
		

    </script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"/>
	
		<!-- 여기서 코드작성 -->
		<div class="main_ctn">
                     <div class="act">
                        <h2>영업활동</h2>
                           
                        <div class="content">
                            <ul class="tabs tab_activity">
                                <li style="width:50%"><a name="tab1" class="active" style="cursor:default">캘린더</a></li>
                                <li style="width:50%"><a name="tab2" style="cursor:pointer" x-ng-click="moveTab('L');" href="actListTable.sa">목록</a></li>
                            </ul>
                            <div id="tab1" class="tab_content">
                                <div class="calendar">
                                    <div id='calendar'></div>
                                    <div class="notes">
                                        <i class="ico ico_plan"></i>계획 
                                        <i class="ico ico_run"></i>활동
                                        <i class="ico ico_work"></i>제안
                                        <i class="ico ico_friendly"></i>견적
                                        <i class="ico ico_todo"></i>계약
                                        <i class="ico ico_personal"></i>개인
                                    </div>
                                </div>
                                <div class="schedule">
                                    <div class="sc_select inc_stretchy">
                                        <div class="dept_emp_span"><a href="#">영업부 / 서범수</a></div>
                                        <div class="cal_btn_area">
                                            <button class="enroll_btn" onclick="location.href='actCliList.sa'">활동등록</button>
                                            <button class="enroll_btn" id="privateDate">개인일정</button>
                                        </div>
                                    </div>
                                    <div class="sc_title">
                                        <i class="ico_caleder"></i>
                                        <span>2020-01-01</span>
                                    </div>
                                    
                                    <ul class="sc_list">
  										
                                    </ul>
                                    <ul class="work">
                                        
                                    </ul>
                                </div>
                            </div>
                            
                        </div><!-- //content -->
                    </div><!-- //act -->
                    <jsp:include page="../../common/modal/privateDate.jsp"/>

		 </div>
		       
            </div>
	    </main>
    </div>
        <script>
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on");
            
            // 열리는 메뉴
            //$(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            //$(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
            
    		var msg = '${msg}';
    		console.log(msg);
    		if(msg != ''){
    			alert(msg);
    		} 

        });
		//버튼 마우스 커서 변경
		$('.exit_Btn.times.icon').mouseover(function() {
			$('.exit_Btn.times.icon').css({
				"cursor" : "pointer"
			});
		});
		//취소 버튼
		 $(document).on("click",".exit_Btn",function(){

			 $('.ui.modal.privateDate').modal('hide');
			
        });

    </script>
</body>
</html>