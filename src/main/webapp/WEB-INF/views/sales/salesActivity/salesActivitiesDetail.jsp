<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <!-- 공통 css-->
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/style-albe-timeline.css">
</head>
<body>
    <jsp:include page="../../common/menubar.jsp"/>
    <jsp:include page="../../common/modal/completeAct.jsp"/>
    <script type="text/javascript" src="${ contextPath }/resources/js/jquery-albe-timeline.js"></script>
    <!-- 여기서 코드작성 -->
		       <div class="main_ctn">
		       		<h2>영업활동 상세조회</h2>
                    <div class="title_area">
                        <h3><a href="#"><c:out value="${ actDetail.COM_NAME } ${ actDetail.CLIENT_NAME }"/></a></h3>
                        <div class="top_btn_area">
                            <c:if test="${ actDetail.COMPLETE_YN eq 'N' }"><button class="ui inverted green button" onclick="completeAct()">완료</button></c:if>
                            <button class="ui inverted violet button" onclick="updateAct.sa">수정</button>
                            <button class="ui inverted red button" onclick="deleteAct()">삭제</button>
                        </div>
                    </div>
                    <div class="content_area"> 
                        <div class="enroll_table_area"> <!-- 등록테이블 시작 -->
                             <table class="act_enroll_table">
                            	<c:if test="${ actDetail.OPP_ID != null }">
                                <tr>
                                    <th rowspan="2" class="enroll_th">영업기회</th>
                                    <td>
                                        <div class="opp_srch">
                                        	<span><c:out value="${ actDetail.OPP_NAME }"/> </span>
                                            <input type="hidden" name="oppId" value="${ actDetail.OPP_ID }">
                                            <!-- <button class="ui grey button">검색</button> -->
                                        </div>
                                    </td>	
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="rate_val"  value="${ actDetail.PGS_NAME }" readonly>
                                        <input type="hidden" name="oppProgress" class="rate_val"  value="${ actDetail.OPP_PROGRESS }">
                                        <input type="hidden" name="processStage" class="rate_val"  value="${ actDetail.PROCESS_STAGE }">
                                        <input type="text" class="rate_val"  value="${ actDetail.STAGE_NAME }" readonly>
                                        <input class="rate_val" type="text" name="oppSuccess" value="${ actDetail.PROCESS_SUCCESS }" readonly>
                                        <span>%</span>
                                    </td>
                                </tr>
                                </c:if>
                                <tr>
                                    <th class="enroll_th">활동분류</th>
                                    <td>
                                    	<input type="hidden" value="${ actDetail.CATEGORY }">
                                        <c:if test="${ actDetail.CATEGORY eq 1 }">전화</c:if>
                                        <c:if test="${ actDetail.CATEGORY eq 2 }">메일</c:if>
                                        <c:if test="${ actDetail.CATEGORY eq 3 }">방문</c:if>
                                        <c:if test="${ actDetail.CATEGORY eq 4 }">기타</c:if>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">활동목적</th>
                                    <td>
                                    	<input type="hidden" value="${ actDetail.PURPOSE }">
                                    	<c:if test="${ actDetail.PURPOSE eq 1 }">인사</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 2 }">제품소개</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 3 }">데모시연</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 4 }">정보수집</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 5 }">제안</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 6 }">업무협의</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 7 }">협상</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 8 }">계약</c:if>
                                    	<c:if test="${ actDetail.PURPOSE eq 9 }">기타</c:if>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">날짜</th>
                                    <td>
                                    	<div class="ui calendar" id="date_calendar">
										  <div class="ui input right icon">
										    <i class="calendar icon"></i>
										    <input type="text" placeholder="Date" name="activityDateStr" autocomplete="off" value="${ actDetail.ACTIVITY_DATE }" disabled="disabled">
										  </div>
										</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">활동시간</th>
                                    <td>
                                    	<div class="ui form">
										  <div class="two fields">
										    <div class="field">
										      <div class="ui calendar" id="rangestart">
										        <div class="ui input right icon">
										          <i class="time icon"></i>
										          <input type="text" placeholder="Start" name="activityStart" autocomplete="off" value="${ actDetail.ACTIVITY_START }" disabled="disabled">
										        </div>
										      </div>
										    </div>
										    <div class="field">
										      <div class="ui calendar" id="rangeend">
										        <div class="ui input right icon">
										          <i class="time icon"></i>
										          <input type="text" placeholder="End" name="activityEnd" autocomplete="off" value="${ actDetail.ACTIVITY_END }" disabled="disabled">
										        </div>
										      </div>
										    </div>
										  </div>
										</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">완료</th>
                                    <td>
                                    	<c:if test="${ actDetail.COMPLETE_YN eq 'Y' }">Y</c:if>
                                    	<c:if test="${ actDetail.COMPLETE_YN eq 'N' }">N</c:if>
                                    	<input type="hidden" value='${ actDetail.COMPLETE_YN }' name="completeYn" id="completeYn">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">계획내용</th>
                                    <td><c:out value="${ actDetail.PLAN_CON }"/>
                                    	<textarea  cols="30" rows="10" name="planCon" style="display:none;">
                                    	<c:out value="${ actDetail.PLAN_CON }"/>
                                    	</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">활동내용</th>
                                    <td><c:out value="${ actDetail.ACTIVITY_CON }"/>
                                    	<textarea cols="30" rows="10" name="activityCon" style="display:none;">
                                    	<c:out value="${ actDetail.ACTIVITY_CON }"/>
                                    	</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">동반</th>
                                    <td>
                                        <div class="opp_srch">
                                        <c:forEach items="${ companion }" var="cp"  varStatus="status">
                                            <input type="hidden" name="companion" value="${ cp.EMP_NO }">
                                            <span>
                                            <c:if test="${ status.index eq 0 }"><c:out value="${ cp.EMP_NAME }"/></c:if>
                                            <c:if test="${ status.index > 0 }"><c:out value=", ${ cp.EMP_NAME }"/></c:if>
                                            </span>
                                        </c:forEach>
                                            <!-- <button class="ui grey button">검색</button> -->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">참가자</th>
                                    <td>
                                        <div class="opp_srch">
                                        <c:forEach items="${ participant }" var="pp"  varStatus="status">
                                            <input type="hidden" name="companion" value="${ pp.CLIENT_NO }">
                                            <span>
                                            <c:if test="${ status.index eq 0 }"><c:out value="${ pp.CLIENT_NAME }"/></c:if>
                                            <c:if test="${ status.index > 0 }"><c:out value=", ${ pp.CLIENT_NAME }"/></c:if>
                                            </span>
                                        </c:forEach>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">담당자</th>
                                    <td>
                                    	<input type="text" value="${ sessionScope.loginEmp.empName }" style="border:none;" readonly>
                                    	<input type="hidden" value="${ sessionScope.loginEmp.empNo }" name="empNo">
                                    </td>
                                </tr>
                            </table>
                        </div><!-- 등록테이블 끝 -->
                        <div class="product_area"><!-- 연관제품 -->
                            <h3>연관제품</h3>
                            <div class="enroll_table_area product_table_area">
                                <table class="act_enroll_table">
                                    <colgroup>
                                        <col style="width:40%;">
                                        <col style="width:15%;">
                                        <col style="width:15%;">
                                        <col style="width:20%;">
                                        <col style="width:10%;">
                                    </colgroup>
                                    <tr>
                                        <th class="enroll_th">제품명</th>
                                        <th class="enroll_th">포장수량</th>
                                        <th class="enroll_th">규격</th>
                                        <th class="enroll_th">단가</th>
                                        <th class="enroll_th">삭제</th>
                                    </tr>
                                    <tbody>
                                    	<c:forEach items="${ productList }" var="pd" varStatus="status">
					 							<tr>
					 								<td class="enroll_th">${ pd.PDT_NAME }</td>
					 								<td class="enroll_th">${ pd.QUANTITY }</td>
					 								<td class="enroll_th">${ pd.STANDARD }<input type="hidden" name="pdtId" value="${ pd.PDT_ID }"></td>
					 								<td class="enroll_th">${ pd.PRICE }</td>
					 								<td class="enroll_th"><i class="trash alternate outline icon center"/></td>
					 							</tr>				 							
                                    	</c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div> <!-- 연관제품 끝 -->
                        <div class="attc_area"> <!-- 첨부자료 -->
                            <h3>첨부자료</h3>
                            <div class="enroll_table_area product_table_area attach_table_area">
                                <table class="act_enroll_table">
                                    <colgroup>
                                        <col style="width:80%;">
                                        <col style="width:20%;">
                                    </colgroup>
                                    <tr>
                                        <th class="enroll_th">파일명</th>
                                        <th class="enroll_th">다운로드</th>
                                    </tr> 
                                    <c:forEach items="${attachList}" varStatus="status">
										<tr>
											<td><c:out value="${attachList[status.index].ORIGIN_NAME}" />
												<input type="hidden"
												value="${attachList[status.index].CHANGE_NAME}" /> <input
												type="hidden" value="${reportList[status.index].FILEPATH }" />
											</td>
											<td><button class="ui grey basic button not">다운로드</button></td>
										</tr>
									</c:forEach>
                                </table>
                            </div>
                        </div> <!-- 첨부자료 끝 -->
                        <!-- 타임라인 시작 -->
                        <div id="myTimeline">
                            
                        </div>
                    </div>
		       </div>
           </div>
       </main>
   </div>
        <script>
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on");
            
            // 열리는 메뉴
            //$(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            //$(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
            
            //제안서 다운할떄 필요한 스크립트
			$('.attach_table_area td button')
					.click(
							function(e) {
								var originName = $(this).parents('tr')
										.children()[0].innerText;
								var fileName = $(this).parents('tr').children()
										.eq(0).children().eq(0).val();
								var filePath = $(this).parents('tr').children()
										.eq(0).children().eq(1).val();
								var extension = originName.split(".");
								var encodefilePath = encodeURI(filePath);
								fileName = fileName + '.' + extension[1];
								location.href = "download.do?fileName="
										+ fileName + "&filePath="
										+ encodefilePath + "&originName="
										+ originName;
						});
        
        
			//버튼 마우스 커서 변경
			$('.exit_Btn.times.icon').mouseover(function() {
				$('.exit_Btn.times.icon').css({
					"cursor" : "pointer"
				});
			});
			
			//취소 버튼
			 $(document).on("click",".exit_Btn",function(){
				$('#updateConId').val('');
				$('#updateCon').val('');
				$('.ui.modal.completeAct').modal('hide');
				
	        });
        });
        
        function completeAct(){
        	var actId = '${actDetail.ACTIVITY_ID}';
        	$('#updateConId').val(actId);
        	$('.ui.modal').css('width','600px');
        	$('.ui.modal.completeAct').modal('show');
        }
        
        function deleteAct(){
        	var actId = '${actDetail.ACTIVITY_ID}';
			var result = confirm('정말 삭제하시겠습니까?');
			if(result){
				location.href="deleteAct.sa?actId=" + actId;
			}
        }

    </script>
</body>
</html>