<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <!-- 공통 css-->
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"/>
	<jsp:include page="../../common/modal/opportunityAct.jsp"/>
	<jsp:include page="../../common/modal/multiClient.jsp"/>
	<jsp:include page="../../common/modal/multiManager.jsp"/>
	<jsp:include page="../../common/modal/productAct.jsp"/>
		<!-- 여기서 코드작성 -->
		       <div class="main_ctn">
		       		<form action="actEnroll.sa" method="post" enctype="multipart/form-data">
					<h2>영업활동 등록</h2>
                    <div class="title_area">
                        <h3><a href="#"> <c:out value="${ comName } ${ client.clientName }"/> </a><input type="hidden" value="${ client.clientId }" name="clientId"></h3>
                        <div class="top_btn_area">
                            <button type="submit" class="ui inverted violet button">저장</button>
                            <button type="button" onclick="location.href='17.sa'" class="ui inverted red button">취소</button>
                        </div>
                    </div>
                    <div class="content_area"> 
                        <div class="enroll_table_area"> <!-- 등록테이블 시작 -->
                            <table class="act_enroll_table">
                                <tr>
                                    <th rowspan="2" class="enroll_th">영업기회</th>
                                    <td>
                                        <div class="opp_srch">
                                            <input type="text" id="opportunity">
                                            <input type="hidden" name="oppId" id="opportunity_hidden" value="0">
                                            <button class="ui basic grey button atth_btn" id="opportunity_Modal" type="button">검색</button>
                                        </div>
                                    </td>	
                                </tr>
                                <tr>
                                    <td>
                                    	<input type="hidden" id="progress">
                                        <select class="pro_select" id="pro_select">
                                            <option hidden="on">진행상태</option>
                                            <option value="1">진행중</option>
                                            <option value="2">종료(성공)</option>
                                            <option value="3">종료(실패)</option>
                                            <option value="4">보류 및 연기</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">활동분류*</th>
                                    <td>
                                    	<select class="pro_select" name="category">
                                            <option value="1">전화</option>
                                            <option value="2">메일</option>
                                            <option value="3">방문</option>
                                            <option value="4">기타</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">활동목적*</th>
                                    <td>
                                    	<select class="pro_select" name="purpose">
                                            <option value="1">인사</option>
                                            <option value="2">제품소개</option>
                                            <option value="3">데모시연</option>
                                            <option value="4">정보수집</option>
                                            <option value="5">제안</option>
                                            <option value="6">업무협의</option>
                                            <option value="7">협상</option>
                                            <option value="8">계약</option>
                                            <option value="9">기타</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">날짜*</th>
                                    <td>
                                    	<div class="ui calendar" id="date_calendar">
										  <div class="ui input right icon">
										    <i class="calendar icon"></i>
										    <input type="text" placeholder="Date" name="activityDateStr" autocomplete="off">
										  </div>
										</div>
                                    </td>

                                </tr>
                                <tr>
                                    <th class="enroll_th">활동시간*</th>
                                    <td>
                                    	<div class="ui form">
										  <div class="two fields">
										    <div class="field">
										      <div class="ui calendar" id="rangestart">
										        <div class="ui input right icon">
										          <i class="time icon"></i>
										          <input type="text" placeholder="Start" name="activityStart" autocomplete="off">
										        </div>
										      </div>
										    </div>
										    <div class="field">
										      <div class="ui calendar" id="rangeend">
										        <div class="ui input right icon">
										          <i class="time icon"></i>
										          <input type="text" placeholder="End" name="activityEnd" autocomplete="off">
										        </div>
										      </div>
										    </div>
										  </div>
										</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">완료</th>
                                    <td>
                                    	<input class="complete_ck" type="checkbox" id="comCk">
                                    	<input type="hidden" value="N" name="completeYn" id="completeYn">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">계획내용</th>
                                    <td><textarea  cols="30" rows="10" name="planCon"></textarea></td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">활동내용</th>
                                    <td><textarea cols="30" rows="10" name="activityCon" class="activityCon" disabled></textarea></td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">동반</th>
                                    <td>
                                        <div class="opp_srch emp_srch">
                                            <input type="text" id="managerText" readonly style="border:none;">
                                            <button class="ui basic grey button atth_btn" type="button" id="multiManager">검색</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">참가자</th>
                                    <td>
                                        <div class="opp_srch client_srch">
                                            <input type="text" readonly style="border:none;" id="participantText">
                                            <button class="ui basic grey button atth_btn" type="button" id="multiClient">검색</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="enroll_th">담당자</th>
                                    <td>
                                    	<input type="text" value="${ sessionScope.loginEmp.empName }" style="border:none;" readonly>
                                    	<input type="hidden" value="${ sessionScope.loginEmp.empNo }" name="empNo">
                                    </td>
                                </tr>
                            </table>
                        </div><!-- 등록테이블 끝 -->
                        <div class="product_area"><!-- 연관제품 -->
	                        <div class="attc_btn_div pdt">
	                            	<button type="button" class="ui inverted violet button atth_btn" id="estBtn">제품 등록</button>
							</div>
                            <div class="enroll_table_area product_table_area">
	                            <h3>연관제품</h3>
                                <table class="act_enroll_table">
                                    <colgroup>
                                        <col style="width:40%;">
                                        <col style="width:15%;">
                                        <col style="width:15%;">
                                        <col style="width:20%;">
                                        <col style="width:10%;">
                                    </colgroup>
                                    <tr>
                                        <th class="enroll_th">제품명</th>
                                        <th class="enroll_th">포장수량</th>
                                        <th class="enroll_th">규격</th>
                                        <th class="enroll_th">단가</th>
                                        <th class="enroll_th">삭제</th>
                                    </tr>
                                    <tbody class="pdt_tr">
										
                                    </tbody>
                                </table>
                            </div>
                            
                        </div> <!-- 연관제품 끝 -->
                        
                        <div class="attc_btn_div">
                            <button class="ui inverted violet button atth_btn" id="file_btn">추가</button>
                            <input type="file" id="file">
                        </div>
                        <script>
                            var count=1;
                            $(function () {
                            	$('#comCk').on('change', function(){
                            		if($('#comCk').prop("checked")){
                                		$('#completeYn').val("Y");
                                		$('.activityCon').prop('disabled', false);
                                		console.log($('#completeYn').val());
                                	}else{
                                		$('#completeYn').val("N");
                                		$('.activityCon').prop('disabled', true);
                                		console.log($('#completeYn').val());
                                	}
                            	});
                            	
                            	
                            	
                                $('#file_btn').click(function (e) {
                                		e.preventDefault();
                                		var str = '<tr><td><input type="file" name="files"><input class="upload_name" id="upload_name' 
                            			+ (count++) + '" readonly></td><td><button class="ui basic grey button not" onclick="remove_btn($(this))">삭제</button></td></tr>';
                            			
                            			$("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
                            			
                            			$("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
                                    		var cur=$(this).val();
                                    		var arSplitUrl = cur.split('\\');
                                    		var nArLength = arSplitUrl.length;
                                    		var fileName = arSplitUrl[nArLength-1];
                                            
                                            $("#upload_name" + (count-1)).val(fileName);
                                    		console.log(cur);
                                    	}); 
                                    }); 
                                });
                            
                            function remove_btn(obj){
                                obj.parent().parent().remove();
                            }
                        </script>
                        <div class="attc_area"> <!-- 첨부자료 -->
                            <h3>첨부자료</h3>
                            <div class="enroll_table_area product_table_area attach_table_area">
                                <table id="file_table" class="act_enroll_table">
                                    <colgroup>
                                        <col style="width:80%;">
                                        <col style="width:20%;">
                                    </colgroup>
                                    <tr>
                                        <th class="enroll_th">파일명</th>
                                        <th class="enroll_th">삭제</th>
                                    </tr>

                                </table>
                            </div>
                        </div> <!-- 첨부자료 끝 -->
                    </div>       			
		       		</form>
		       </div>
           </div>
       </main>
   </div>
        <script>
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on");
            
            // 열리는 메뉴
            //$(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            //$(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
 
            $('#date_calendar').calendar({
            	type: 'date',
                formatter: {
                    date: function (date, settings) {
                      if (!date) return '';
                      var day = "";
                   		if(date.getDate() >= 1 && date.getDate() <= 9){
                    	  	day = '0' + date.getDate();
                        }else{
                        	day = date.getDate();
                        }
                      var month = "";
                      if(date.getMonth() >= 0 && date.getMonth() <= 8){
                  	  	month = '0' + (date.getMonth() + 1);
                      }else{
                      	month = date.getMonth() + 1;
                      }
                      var year = date.getFullYear();
                      return year + '-' + month + '-' + day;
                	}
                }
            });
            
            $('#rangestart').calendar({
            	  type: 'time',
            	  endCalendar: $('#rangeend')
            	});
            	$('#rangeend').calendar({
            	  type: 'time',
            	  startCalendar: $('#rangestart')
            	});
            
            $("#opportunity_Modal").click(function() {
    			$('.ui.modal.opportunity').modal('show');
    		});
            
            $('#estBtn').click(function(){
            	$('#result_Table_Modal tbody').html('');
            	$('.ui.modal.product').modal('show');
            });
            
            $(".exit_Btn").click(function() {
				$('.ui.modal.opportunity').modal('hide');
				$('.ui.modal.product').modal('hide');
			})
			
			//버튼 마우스 커서 변경
			$('.exit_Btn.times.icon').mouseover(function() {
				$('.exit_Btn.times.icon').css({
					"cursor" : "pointer"
				});
			});
            $('#result_Table_Modal tbody').mouseover(function() {
				$(this).css({
					"cursor" : "pointer"
				});
			});
        });
    </script>
</body>
</html>