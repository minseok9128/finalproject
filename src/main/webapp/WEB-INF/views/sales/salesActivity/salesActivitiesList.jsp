<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <!-- 공통 css-->
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"/>
                <!-- 여기서 코드작성 -->
                <div class="main_ctn">
                    <h3>영업활동</h3>
                    <div class="search_list">
                        <ul>
                            <li class="srch_val">
                                <div class="srch_path">
                                    <input class="clicom" type="text" placeholder="고객사 / 고객" id="ccName_srch">
                                    <button class="ui grey basic button srch">검색</button>
                                </div>
                            </li>
                        </ul>
                    </div><!-- //search_list -->
                    <div class="accordionArea">
                        <ul class="accordion">
                        	<c:forEach items="${ clientList }" var="client" varStatus="status">
	                        	<li class="accordion__item">
	                                <a class="accordion__title"
	                                    href="javascript:void(0)">
	                                    <div class="activity_count">
	                                        <span class="count"><c:out value="${ count[status.index] }"/> </span>
	                                    </div>
	                                    
	                                    <div class="act_title">
	                                        <span class="acco_client"><c:out value="${ client.clientName }"/></span>
	                                        <span class="acco_company"><c:out value="${ comNameArr[status.index] }"/></span>
	                                    </div>
	                                    <div class="act_dep"><c:if test="${ !empty client.department }"><c:out value="${ client.department} /"/></c:if><c:if test="${ !empty client.job }"><c:out value="${ client.job }"></c:out> </c:if></div>
	                                    <div class="bmenu x7">
	                                        <span class="btop"></span><span class="bmid"></span><span
	                                            class="bbot"></span>
	                                    </div>
	                                </a>
	                                <div class="accordion__content">
	                                    <div class="acc_enroll_btn">
	                                    	<form action="actEnrollView.sa" method="post">
	                                    		<input class="scrolling" type="hidden" name="clientId" value="<c:out value='${ client.clientId }'/>">
	                                    		<input type="hidden" name="comName" value="<c:out value='${ comNameArr[status.index] }'/>">
	                                    		<input type="hidden" name="clientName" value="<c:out value='${ client.clientName }'/>">
	                                        	<button class="ui grey basic button srch" type="submit">활동등록</button>	                                    	
	                                    	</form>
	                                    </div>
	                                    <div class="">
	                                    <c:forEach items="${ activityList }" var="list" varStatus="status">
	                                     <c:forEach items="${ activityList.get(status.index) }" var="list2">
	                                      <c:if test="${ list2.clientId eq client.clientId }">
	                                      		<a href="actDetail.sa?activityId=${ list2.activityId }">
		                                    		<c:out value="${ list2.activityDate } / "/> 
		                                    		<c:if test="${ list2.category == 1 }">
		                                    			전화
		                                    		</c:if>
		                                    		<c:if test="${ list2.category == 2 }">
		                                    			메일
		                                    		</c:if>
		                                    		<c:if test="${ list2.category == 3 }">
		                                    			방문
		                                    		</c:if>
		                                    		<c:if test="${ list2.category == 4 }">
		                                    			기타
		                                    		</c:if>
		                                    		/ 
		                                    		<c:if test="${ list2.purpose == 1 }">
		                                    			인사
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 2 }">
		                                    			제품소개
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 3 }">
		                                    			데모시연
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 4 }">
		                                    			정보수집
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 5 }">
		                                    			제안
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 6 }">
		                                    			업무협의
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 7 }">
		                                    			협상
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 8 }">
		                                    			계약
		                                    		</c:if>
		                                    		<c:if test="${ list2.purpose == 9 }">
		                                    			기타
		                                    		</c:if>
		                                    	</a>
		                                    	<br>
	                                      </c:if>
		                                   	
		                                    </c:forEach>
	                                    </c:forEach>
	                                    </div>
	                                </div>
	                            </li>
                        	</c:forEach>

                        </ul>
                    </div>
                    
                </div>
            </div>
	    </main>
    </div>
        <script>
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on");
            
            // 열리는 메뉴
            //$(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            //$(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
            
            //아코디언
            $('.ui.accordion').accordion();
            $(document).on("click",".accordion .accordion__title",function(){
            		
            		// Adds active class
    				$(this).toggleClass('active');
    				// Expand or collapse this panel
    				$(this).next().slideToggle('fast');
    				// Hide the other panels
    				$('.accordion__content').not($(this).next()).slideUp('fast');
    				// Removes active class from other titles
    				$('.accordion__title').not($(this)).removeClass('active');
            	
            });

            

        });
    	var lastScrollTop = 0;
    	var empNo = '${ sessionScope.loginEmp.empNo }';
    	// 1. 스크롤 이벤트 발생
    	$(window).scroll(function(){ // ① 스크롤 이벤트 최초 발생
    		
    		var currentScrollTop = $(window).scrollTop();
    		
    		/*  
    			=================	다운 스크롤인 상태	================
    		*/
    		if( currentScrollTop - lastScrollTop > 0 ){
    			// down-scroll : 현재 게시글 다음의 글을 불러온다.
    			console.log("down-scroll");
    			// 2. 현재 스크롤의 top 좌표가  > (게시글을 불러온 화면 height - 윈도우창의 height) 되는 순간
    			if ($(window).scrollTop() >= ($(document).height() - $(window).height()) ){ //② 현재스크롤의 위치가 화면의 보이는 위치보다 크다면
    	           
    				// 3. class가 scrolling인 것의 요소 중 마지막인 요소를 선택한 다음 그것의 data-bno속성 값을 받아온다.
    				//		즉, 현재 뿌려진 게시글의 마지막 bno값을 읽어오는 것이다.( 이 다음의 게시글들을 가져오기 위해 필요한 데이터이다.)
    				var lastCno = $(".scrolling:last").val();
    				
    				console.log("lastbno 확인" + lastCno);
    				// 4. ajax를 이용하여 현재 뿌려진 게시글의 마지막 bno를 서버로 보내어 그 다음 20개의 게시물 데이터를 받아온다. 
    				$.ajax({
    					type : 'post',	// 요청 method 방식 
    					url : 'scrollDownClient.sa',// 요청할 서버의 url
    					data : {lastCno:lastCno, empNo:empNo},
    					success : function(data){// ajax 가 성공했을시에 수행될 function이다. 이 function의 파라미터는 서버로 부터 return받은 데이터이다.
    						//console.log("datalastCno : " + data.clientList);
    						var str = "";
    						
    						// 5. 받아온 데이터가 ""이거나 null이 아닌 경우에 DOM handling을 해준다.
    				 		if(data != ""){
    							//6. 서버로부터 받아온 data가 list이므로 이 각각의 원소에 접근하려면 each문을 사용한다.
    							//console.log("data : " + data)
								
    							
    							$(data.clientList).each(
    								// 7. 새로운 데이터를 갖고 html코드형태의 문자열을 만들어준다.
    								
    								function(index, client){
    									//console.log(this);
    									//console.log("data : " + this.CLIENTNAME);
    									//console.log("data : " + data.count[index]);
    									//console.log("data : " + data.comNameArr[index]);
    									str +=	'<li class="accordion__item"><a class="accordion__title" href="javascript:void(0)"><div class="activity_count"><span class="count">' + data.count[index] + '</span></div><div class="act_title"><span class="acco_client">' + this.CLIENTNAME + '</span><span class="acco_company">'+ data.comNameArr[index] +'</span></div><div class="act_dep">'+ this.DEPARTMENT +'</div><div class="bmenu x7"><span class="btop"></span><span class="bmid"></span><span class="bbot"></span></div></a><div class="accordion__content"><div class="acc_enroll_btn"><form action="actEnrollView.sa" method="post"><input class="scrolling" type="hidden" name="clientId" value="'+ this.CLIENTID +'"><input type="hidden" name="comName" value="'+data.comNameArr[index]+'"><input type="hidden" name="clientName" value="'+ this.CLIENTNAME +'"><button class="ui grey basic button srch" type="submit">활동등록</button></form></div><div class="">';
    									
    									$(data.activityList[index]).each(function(index2, actList){
    										if(actList.CLIENT_ID == client.CLIENTID){
    											str += '<a href="actDetail.sa?activityId=' + actList.ACTIVITY_ID + '">';
    											if(actList.CATEGORY == 1){
    												str += '전화';	
    											}else if(actList.CATEGORY == 2){
    												str += '메일';
    											}else if(actList.CATEGORY == 3){
    												str += '방문';
    											}else if(actList.CATEGORY == 4){
    												str += '기타';
    											}
    											str += ' / '
   												if(actList.PURPOSE == 1){
       												str += '인사';	
       											}else if(actList.PURPOSE == 2){
       												str += '제품소개';
       											}else if(actList.PURPOSE == 3){
       												str += '데모시연';
       											}else if(actList.PURPOSE == 4){
       												str += '정보수집';
       											}else if(actList.PURPOSE == 5){
       												str += '제안';
       											}else if(actList.PURPOSE == 6){
       												str += '업무협의';
       											}else if(actList.PURPOSE == 7){
       												str += '협상';
       											}else if(actList.PURPOSE == 8){
       												str += '계약';
       											}else if(actList.PURPOSE == 9){
       												str += '기타';
       											}
    											str += '</a><br>';
    										}
    									});
    									
    									str += '</div></div></li>';
    								
    									
    								
    							});// each
    							// 8. 이전까지 뿌려졌던 데이터를 비워주고, <th>헤더 바로 밑에 위에서 만든 str을  뿌려준다.
    							//$(".accordion__item").empty();// 셀렉터 태그 안의 모든 텍스트를 지운다.						
    							$(".accordion").append(str);
								
    						}// if : data!=null
    						else{ // 9. 만약 서버로 부터 받아온 데이터가 없으면 그냥 아무것도 하지말까..
    							alert("더 불러올 데이터가 없습니다.");
    						}// else 
    		
    					}// success
    				});// ajax
    				
    				// 여기서 class가 listToChange인 것중 가장 처음인 것을 찾아서 그 위치로 이동하자.
    				console.log("position : " + $(".accordion__item:first").offset());
    				var position = $(".accordion__item:first").offset();// 위치 값
    				
    				// 이동  위로 부터 position.top px 위치로 스크롤 하는 것이다. 그걸 500ms 동안 애니메이션이 이루어짐.
    				//$('html,body').stop().animate({scrollTop : position.top }, 600);
    	
    	        }//if : 현재 스크롤의 top 좌표가  > (게시글을 불러온 화면 height - 윈도우창의 height) 되는 순간
    			
    			// lastScrollTop을 현재 currentScrollTop으로 갱신해준다.
    			lastScrollTop = currentScrollTop;
    	        
    	        
    		}// 다운스크롤인 상태

     	});// scroll event
     	
     	$(function(){
     		$('#ccName_srch').keyup(function(){
     			var name = $('#ccName_srch').val();
     			$.ajax({
     				type:'post',
     				url:'ccNameSrch.sa',
     				data:{name:name,empNo:empNo},
     				success:function(data){
     					str = "";
						
						// 5. 받아온 데이터가 ""이거나 null이 아닌 경우에 DOM handling을 해준다.
				 		if(data != ""){
							//6. 서버로부터 받아온 data가 list이므로 이 각각의 원소에 접근하려면 each문을 사용한다.
							//console.log("data : " + data)
							
							
							$(data.clientList).each(
								// 7. 새로운 데이터를 갖고 html코드형태의 문자열을 만들어준다.
								
								function(index, client){
									//console.log(this);
									//console.log("data : " + this.CLIENTNAME);
									//console.log("data : " + data.count[index]);
									//console.log("data : " + data.comNameArr[index]);
									str +=	'<li class="accordion__item"><a class="accordion__title" href="javascript:void(0)"><div class="activity_count"><span class="count">' + data.count[index] + '</span></div><div class="act_title"><span class="acco_client">' + this.CLIENTNAME + '</span><span class="acco_company">'+ data.comNameArr[index] +'</span></div><div class="act_dep">'+ this.DEPARTMENT +'</div><div class="bmenu x7"><span class="btop"></span><span class="bmid"></span><span class="bbot"></span></div></a><div class="accordion__content"><div class="acc_enroll_btn"><form action="actEnrollView.sa" method="post"><input class="scrolling" type="hidden" name="clientId" value="'+ this.CLIENTID +'"><input type="hidden" name="comName" value="'+data.comNameArr[index]+'"><input type="hidden" name="clientName" value="'+ this.CLIENTNAME +'"><button class="ui grey basic button srch" type="submit">활동등록</button></form></div><div class="">';
									
									$(data.activityList[index]).each(function(index2, actList){
										if(actList.CLIENT_ID == client.CLIENTID){
											str += '<a href="actDetail.sa?activityId=' + actList.ACTIVITY_ID + '">';
											if(actList.CATEGORY == 1){
												str += '전화';	
											}else if(actList.CATEGORY == 2){
												str += '메일';
											}else if(actList.CATEGORY == 3){
												str += '방문';
											}else if(actList.CATEGORY == 4){
												str += '기타';
											}
											str += ' / '
												if(actList.PURPOSE == 1){
   												str += '인사';	
   											}else if(actList.PURPOSE == 2){
   												str += '제품소개';
   											}else if(actList.PURPOSE == 3){
   												str += '데모시연';
   											}else if(actList.PURPOSE == 4){
   												str += '정보수집';
   											}else if(actList.PURPOSE == 5){
   												str += '제안';
   											}else if(actList.PURPOSE == 6){
   												str += '업무협의';
   											}else if(actList.PURPOSE == 7){
   												str += '협상';
   											}else if(actList.PURPOSE == 8){
   												str += '계약';
   											}else if(actList.PURPOSE == 9){
   												str += '기타';
   											}
											str += '</a><br>';
										}
									});
									
									str += '</div></div></li>';
								
									
								
							});// each
							// 8. 이전까지 뿌려졌던 데이터를 비워주고, <th>헤더 바로 밑에 위에서 만든 str을  뿌려준다.
							$(".accordion__item").empty();// 셀렉터 태그 안의 모든 텍스트를 지운다.
							$(".accordion").append(str);
							
						}// if : data!=null
						else{ // 9. 만약 서버로 부터 받아온 데이터가 없으면 그냥 아무것도 하지말까..
							alert("더 불러올 데이터가 없습니다.");
						}// else 
     				},
     				error:function(){
     					console.log('fail');	
     				}
     			});
     		});
     	});
    </script>
</body>
</html>