<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <!-- 공통 css-->
    <link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
    <!-- You MUST include jQuery before Fomantic -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"/>
		<!-- 여기서 코드작성 -->
		<div class="main_ctn">
             <div class="act">
                <h3>영업활동</h3>
                    
                <div class="content not">
                    <ul class="tabs tab_activity">
                        <li style="width:50%"><a name="tab1" style="cursor:pointer" x-ng-click="moveTab('C');" href="actCalendar.sa"><i class="icon-calendar"></i>캘린더</a></li>
                        <li style="width:50%"><a name="tab2" class="active" style="cursor:default"><i class="icon-list"></i>목록</a></li>
                    </ul>
                    
                    <div id="tab2" class="tab_content">
                        <div class="rs clearboth">
                        <div class="search_list">
                            <ul>
                                <li class="srch_val">
                                    <div class="srch_path">
                                        <input type="text" placeholder="고객사">
                                        <input type="text" placeholder="고객">
                                        <button class="ui grey basic button srch">검색</button>
                                    </div>
                                </li>
                                <li class="cal">
                                    <div class="ui form">
                                        <div class="two fields">
                                          <div class="field">
                                            <div class="ui calendar" id="rangestart">
                                              <div class="ui input left icon">
                                                <i class="calendar icon"></i>
                                                <input type="text" value="${ actListTable[0].FIRSTDAY }">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="field">
                                            <div class="ui calendar" id="rangeend">
                                              <div class="ui input left icon">
                                                <i class="calendar icon"></i>
                                                <input type="text" value="${ actListTable[0].LASTDAY }">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                </li>
                            </ul>
                        </div><!-- //search_list -->
                        <script>
                            $('#rangestart').calendar({
                                type: 'date',
                                formatter: {
                                    date : function (date, settings) {
                                          if (!date) return '';
                                          var day = date.getDate();
                                          var month = date.getMonth() + 1;
                                          var year = date.getFullYear();
                                          return year + '-' + month + '-' + day;
                                          /* return day + '/' + month + '/' + year; */
                                        }
                                  },
                                endCalendar: $('#rangeend')
                            });
                            $('#rangeend').calendar({
                                type: 'date',
                                formatter: {
                                    date : function (date, settings) {
                                          if (!date) return '';
                                          var day = date.getDate();
                                          var month = date.getMonth() + 1;
                                          var year = date.getFullYear();
                                          return year + '-' + month + '-' + day;
                                          /* return day + '/' + month + '/' + year; */
                                        }
                                  },
                                startCalendar: $('#rangestart')
                            });
                        </script>
                        </div>
                        <div class="subcon" style="margin-top: 25px;">
                            <table class="wtable even">
                                <colgroup>
                                    <col style="width:30%;">
                                    <col style="width:10%;">
                                    <col style="width:10%;">
                                    <col style="width:20%;">
                                    <col style="width:20%;">
                                    <col style="width:10%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>날짜</th>
                                        <th>활동분류</th>
                                        <th>활동목적</th>
                                        <th>고객사</th>
                                        <th>고객</th>
                                        <th>담당자</th>
                                    </tr>
                                </thead>
                                <tbody style="cursor:pointer;">
                                <c:forEach items="${ actListTable }" var="act" varStatus="status">
                                	<tr class="goDetail">
                                        <td><input type="hidden" value="${ act.ACTIVITY_ID }" name="actId"><c:out value="${ act.ACTIVITY_DATE } ${ act.ACTIVITY_START } ~ ${ act.ACTIVITY_END }"/></td>
                                        <td>
                                        	<c:if test="${ act.CATEGORY == 1 }">전화</c:if>
		                                    <c:if test="${ act.CATEGORY == 2 }">메일</c:if>
		                                    <c:if test="${ act.CATEGORY == 3 }">방문</c:if>
		                                    <c:if test="${ act.CATEGORY == 4 }">기타</c:if> 
		                                </td>
                                        <td>
                                        	<c:if test="${ act.PURPOSE == 1 }">인사</c:if>
											<c:if test="${ act.PURPOSE == 2 }">제품소개</c:if>
											<c:if test="${ act.PURPOSE == 3 }">데모시연</c:if>
											<c:if test="${ act.PURPOSE == 4 }">정보수집</c:if>
											<c:if test="${ act.PURPOSE == 5 }">제안</c:if>
											<c:if test="${ act.PURPOSE == 6 }">업무협의</c:if>
											<c:if test="${ act.PURPOSE == 7 }">협상</c:if>
											<c:if test="${ act.PURPOSE == 8 }">계약</c:if>
											<c:if test="${ act.PURPOSE == 9 }">기타</c:if>
                                        </td>
                                        <td><span><c:out value="${ act.COM_NAME }"></c:out> </span></td>
                                        <td><c:out value="${ act.CLIENT_NAME }"></c:out> </td>
                                        <td><c:out value="${ act.EMP_NAME }"></c:out> </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- //content -->
            </div><!-- //act -->
		 </div>
		       
            </div>
	    </main>
    </div>
        <script>
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on");
            
            // 열리는 메뉴
            //$(".menu_cnt .main_menu").eq(2).addClass("on").addClass("open");
            //$(".menu_cnt .main_menu").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
        	$(".goDetail").click(function(){
        		var activityId = $(this).find("input[name=actId]").val();
        		console.log($(this).find("td").eq(4).text());
        		var clientName = $(this).find("td").eq(4).text();
        		var comName = $(this).find("td").eq(3).text();
        		location.href='detail.sa?activityId='+ activityId +'&clientName='+ clientName +'&comName='+ comName;
        	})
        });
        
    </script>
</body>
</html>