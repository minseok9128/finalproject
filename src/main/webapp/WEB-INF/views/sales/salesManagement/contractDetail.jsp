<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 계약등록</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management3.css">
<jsp:include page="../../common/menubar.jsp" />
</head>
<body> 

	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title "></div>
			<div id="header_Detail_div">
				<h1>
					계약세부사항
					<button class="cancel_Btn Enrollment_Btn">취소</button>
					<button class="submit_Btn Enrollment_Btn">작성</button>
				</h1>
				<h3><c:out value="${con.contName}"/></h3>
			</div>
			<div id="content_Enrollment_div">
				<table id="table2">
					<tr>
						<td class="td">고객사</td>
						<td>
							<a class="Detail_a"><c:out value="${con.comName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">고객<b class="point">*</b></td>
						<td>
							<a class="Detail_a"><c:out value="${con.clientName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">영업기회</td>
						<td>
							<a class="Detail_a"><c:out value="${con.oppName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">견적</td>
						<td>
							<a class="Detail_a"><c:out value="${con.estName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">계약일<b class="point">*</b></td>
						<td>
							<a class="Detail_a2"><c:out value="${con.contDate}"/></a>
						</td>
					</tr>
					<%-- <tr>
						<td class="td">계약기간<b class="point">*</b></td>
						<td>
							<a class="Detail_a2"><c:out value="${con.contStart}"/> ~ <c:out value="${con.contEnd}"/></a>
						</td>
					</tr> --%>
					<tr>
						<td class="td">계약금액</td>
						<td>
							<a class="Detail_a2">KR￦ <c:out value="${con.contPrice}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">부가세</td>
						<td>
							<a class="Detail_a2">
								<c:if test="${con.contVat eq '1' }">
										부가세 포함
								</c:if>
								<c:if test="${con.contVat eq '0' }">
										부가세 포함
								</c:if>
							</a>
						</td>
					</tr>
					<tr>
						<td class="td">하자보증기간</td>
						<td>
							<a class="Detail_a2"><c:out value="${con.warrantyPeriod}"/>(개월)</a>
						</td>
					</tr>
					<tr>
						<td class="td">입고예정일<b class="point">*</b></td>
						<td>
							<a class="Detail_a2"><c:out value="${con.expectDate}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">입고알람</td>
						<td>
							<a class="Detail_a2">
								<c:if test="${ con.expectAlarm  eq 'NONE' }">
									알림없음
								</c:if>
								<c:if test="${ con.expectAlarm  ne 'NONE' }">
									<c:out value="${con.expectAlarm}"/>
								</c:if>
							</a>
						</td>
					</tr>
					<%-- <tr>
						<td class="td">갱신예정일<b class="point">*</b></td>
						<td>
							<a class="Detail_a2"><c:out value="${con.updateDate}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">갱신알람</td>
						<td>
							<a class="Detail_a2">
								<c:if test="${ con.updateAlarm  eq 'NONE' }">
									알림없음
								</c:if>
								<c:if test="${ con.updateAlarm  ne 'NONE' }">
									<c:out value="${con.updateAlarm}"/>
								</c:if>
							</a>
						</td>
					</tr> --%>
					<tr>
						<td class="td">담당자</td>
						<td>
							<a class="Detail_a2"><c:out value="${con.empName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">비고</td>
						<td>
							<a class="Detail_a2">
								<c:out value="${con.contRemark}"/>
							</a>
						</td>
					</tr>
				</table>

				<div>
					<h1 id="propose_report">
						계약제품
					</h1>
					<table id="contract_Product_Table">
						<tr>
							<th>제품명</th>
							<th>단가</th>
							<th>수량</th>
							<th>금액</th>
						</tr>
						<tr>
							<th>매출주기</th>
							<th>매출일</th>
							<th colspan="2">비고</th>
						</tr>
						<c:set var="amountA" value="0" />
						<c:set var="amountP" value="0" />
						<c:forEach var="prod" items="${con.prList}">
							<c:set var="amountA" value="${amountA + prod.amount }" />
							<c:set var="amountP" value="${amountP + (prod.amount *prod.price)  }" />
							<tr>
								<td><c:out value="${prod.pdtName}"/></td>
								<td><c:out value="${prod.price}"/></td>
								<td><c:out value="${prod.amount}"/></td>
								<td><c:out value="${prod.price * prod.amount}"/></td>
							</tr>
							<tr>
								<td>
									<c:if test="${prod.revenuePeriod eq 'ONCE' }">
										1회
									</c:if>
									<c:if test="${prod.revenuePeriod eq 'MONTH' }">
										월별
									</c:if>
									<c:if test="${prod.revenuePeriod eq 'QUARTER' }">
										분기별
									</c:if>
									<c:if test="${prod.revenuePeriod eq 'HALF' }">
										반기별
									</c:if>
								</td>
								<td><c:out value="${prod.revenueDate}"/></td>
								<td colspan="2"><c:out value="${prod.contProductRemark}"/></td>
							</tr>
						</c:forEach>
						<tr>
							<th>총 수량</th>
							<th><c:out value="${amountA}"/></th>
							<th>합계</th>
							<th><c:out value="${amountP}"/></th>
						</tr>
					</table>
				</div>
				<!-- 첨부파일용 -->
				<%-- <h1 id="propose_report">
					<button id="propose_report_Btn">업로드</button>
					<input type="file" id="propose_report_file" hidden="true">
				</h1>
				<table id="contract_Sales_Table">
						<!-- <tr>
							<th>선택
								<br>
								<input type="checkbox">
							</th>
							<th>매출일</th>
							<th>제품명</th>
							<th>단가</th>
							<th>수량</th>
							<th>금액</th>
						</tr> -->
						<c:forEach var="att" items="${con.attList}">
							<tr>
								<td><a><c:out value="${att.originName}" /></a></td>
							</tr>
					</c:forEach>
					</table>	 --%>
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(2)
					.addClass("on");
		})
	</script>
</body>
</html>