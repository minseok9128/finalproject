<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<title>셀모아 계약등록</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
	<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
<jsp:include page="../../common/menubar.jsp" />
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<jsp:include page="../../common/modal/estimate3.jsp"></jsp:include>
<jsp:include page="../../common/modal/product.jsp"></jsp:include>
<style>
	.estumateTable {text-align: center;}
	.estumateTable  tr:nth-child(odd) td {vertical-align: middle; border-bottom: 1px solid #E5E5E5; border-right: 1px solid #E5E5E5;}
	.estumateTable  tr:nth-child(odd) td:nth-last-child(1) {border-right: none;}
	.estumateTable  tr:nth-child(even) td {height: 33px; vertical-align: middle; border-bottom: 1px solid #E5E5E5; border-right: 1px solid #E5E5E5;}
	.estumateTable  tr:nth-child(odd) td input {text-align: right; padding-right: 5px; border: 1px solid #a0a0a0; border-radius: 2px;}
	.estumateTable  tr:nth-child(even) td input {height:25px; text-align: right; padding-right: 5px; border: 1px solid #a0a0a0; border-radius: 2px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(1) {width: 35%; height: 33px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(3) {width: 17.5%; height: 33px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(3) input {width: 80%; height: 25px; margin-right: 3px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(4) input {width: 90%; height: 25px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(2) input {width: 80%; height: 25px;}
	.estumateTable  tr:nth-child(even) td:nth-child(3) input {width: 80%; height: 25px;}
	.estumateTable  tr:nth-child(even) td:nth-child(4) input {width: 95%; height: 25px;}
	.estumateTable tfoot th{ text-align: left; }
	.checkBox{float: left; margin-left: 6px; width: 16px; height: 16px;}
	#resultSum *{
		text-align:center;
	}
</style>
</head>
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title "></div>
			<form action="insert.con" method="post" enctype="multipart/form-data"><!-- form start -->
			<div id="header_Detail_div">
				<h1>
					계약등록
					<!-- <button class="cancel_Btn Enrollment_Btn">취소</button>
					<button class="submit_Btn Enrollment_Btn">작성</button> -->
					<input type="button" class="ui inverted red button" id="resetBtn" style="width: 70px; height:35px; float:right;     margin-left: 6px;" value="취소">
					<button class="ui inverted primary button" id="submitBtn" style="width: 70px; height:35px; float:right;">작성</button>
				</h1>
				<br>
			</div>
			<div id="content_Enrollment_div">
				<table id="table2">
					<tr>
						<td class="td">계약명<b class="point">*</b></td>
						<td><input name="contName" type="text" id="offer_Name" class="common_Text1">
						</td>
					</tr>
					<tr>
						<td class="td">고객사</td>
						<td>
							<input type="text" id="company" class="common_Text2"readonly value="${list[0].company.comName}"> 
							<input type="hidden" id="company_hidden"value="${list[0].company.comId}" name="comId">
							<!-- <input name="comId" type="text" id="company" class="common_Text2">
							<input name="comId" type="hidden" id="company_hidden" class="common_Text2"> -->
							<input type="button" class="search_Btn" id="company_Modal" value="검색">
						</td>
					</tr>
					<tr>
						<td class="td">고객<b class="point">*</b></td>
						<td>
							<input type="text" id="client" class="common_Text2" value="${list[0].client.clientName}">
							<input type="hidden" id="client_hidden" value="${list[0].client.clientId}"name="clientId">
							<!-- <input name="clientId" type="text" id="curstormer" class="common_Text2"> -->
							<input type="button" class="search_Btn" id="client_Modal" value="검색">
						</td>
					</tr>
					<tr>
						<td class="td">영업기회</td>
						<td>
							<input type="text" id="opportunity" class="common_Text2">
							<input type="hidden" id="opportunity_hidden" value="0" name="oppId">
							<!-- <input name="oppId" type="text" id="opportunity" class="common_Text2"> -->
							<input type="button" class="search_Btn" id="opportunity_Modal" value="검색">
						</td>
					</tr>
					<tr>
						<td class="td">견적</td>
						<td>
							<input type="text" id="estimate" class="common_Text2">
							<input type="hidden" id="estimate_hidden" value="0" name="estId">
							<input type="button" class="search_Btn" id="estimate_Modal" value="검색">
							<!-- <button class="search_Btn" id="estimate_search">검색</button> -->
						</td>
					</tr>
					<tr>
						<td class="td">계약일<b class="point">*</b></td>
						<td>
							<div class="two fields">
								<div class="field">
									<div class="ui calendar standard_calendar">
										<div class="ui input right icon" style="bottom: 6px;">
											<i class="calendar icon"></i> <input name="contDate" type="text" readonly>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<!-- <tr>
						<td class="td">계약기간<b class="point">*</b></td>
						<td>
							<div class="two fields">
								<div class="field">
									<div class="ui calendar start_calendar">
										<div class="ui input right icon" id="start_calendar">
											<i class="calendar icon"></i> <input name="contStart" type="text" readonly>
										</div>
									</div>
								</div>
							</div>
							<p id="contract_exclamationMark">~</p>
							<div class="two fields">
								<div class="field">
									<div class="ui calendar end_calendar">
										<div class="ui input right icon" id="start_calendar">
											<i class="calendar icon"></i> <input name="contEnd" type="text" readonly>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr> -->
					<tr>
						<td class="td">계약금액</td>
						<td><input name="contPrice" type="text" id="" class="common_Text1"></td>
					</tr>
					<tr>
						<td class="td">부가세</td>
						<td><select name="contVat" id="vatYn" class="common_Text1">
								<option value="0">VAT미포함</option>
								<option value="1">VAT포함</option>
						</select></td>
					</tr>
					<tr>
						<td class="td">하자보증기간</td>
						<td><input name="warrantyPeriod" type="text" class="common_Text4">
							<p class="common_Text4_p">(개월)</p></td>
					</tr>
					<tr>
						<td class="td">입고예정일<b class="point">*</b></td>
						<td>
							<div class="two fields">
								<div class="field">
									<div class="ui calendar standard_calendar"
										id="standard_calendar">
										<div class="ui input right icon" style="bottom: 6px;">
											<i class="calendar icon"></i> <input name="expectDate" type="text" readonly>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="td">입고알람</td>
						<td>
							<div class="select_checkBox_div">
								<input id="exCheck" type="checkbox">
								 <select id="exSel" name="expectAlarm" disabled>
								 	<option value="NONE">없음</option>
									<option value="3M">3개월전</option>
									<option value="1M">1개월전</option>
									<option value="2W">2주전</option>
									<option value="1W">1주전</option>
									<option value="3D">3일전</option>
									<option value="1D">1일전</option>
								</select>
							</div>
						</td>
					</tr>
					<!-- <tr>
						<td class="td">갱신예정일<b class="point">*</b></td>
						<td>
							<div class="two fields">
								<div class="field">
									<div class="ui calendar standard_calendar">
										<div class="ui input right icon">
											<i class="calendar icon"></i> <input name="updateDate" type="text" readonly>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="td">갱신알람</td>
						<td>
							<div class="select_checkBox_div">
								<input id="upCheck" type="checkbox">
								 <select id="upSel" name="updateAlarm" class="selectBox" disabled>
								 	<option value="NONE">없음</option>
									<option value="3M">3개월전</option>
									<option value="1M">1개월전</option>
									<option value="2W">2주전</option>
									<option value="1W">1주전</option>
									<option value="3D">3일전</option>
									<option value="1D">1일전</option>
								</select>
							</div>
						</td>
					</tr> -->
					<tr>
						<td class="td">사업자등록증보유</td>
						<td><input name="bizRegYn" type="checkbox" class="checkBox"></td>
					</tr>
					<tr>
						<td class="td">담당자</td>
						<td>
							<input readonly class="common_Text1" type="text" style="width: 100%; height: 32px;" value="${sessionScope.loginEmp.empName }">
							<input type="hidden" style="width: 250px; height: 32px;" name="empNo" value="${sessionScope.loginEmp.empNo }">
							<!-- <input type="text" class="common_Text2" id="managerText"value=""> 
							<input type="hidden" id="manager_hidden" value="0" name="empNo">
							<button class="ui grey basic button srch searchBtn" id="manager">검색</button> -->
						</td>
					</tr>
					<tr>
						<td class="td">비고</td>
						<td><textarea rows="5" cols="78"  style="margin-top:5px; resize: none;" name="contRemark" id="contents"></textarea></td>
					</tr>
				</table>

				<div class="estumateproduct">
                  <h1 class="propose_report">견적 품목</h1>
                  <input type="button" id="product_add" style="float:right;" class ="ui basic blue button product_add" value="제품 등록">
               </div>
               <table class="estumateTable" id="contract_Product_Table">
                  <thead>
                     <tr>	
                     	<th>제품명</th>
                     	<th>단가</th>
                     	<th>수량</th>
                     	<th>금액</th>
                     	<th rowspan="2">삭제</th>
                     <!-- <th colspan="2">제품명</th>
                     <th>단가</th>
                     <th>할인</th>
                     <th>합계</th>
                     <th rowspan="2">삭제</th> -->
                  	</tr>
                  	<tr>
                  		<th>매출주기</th>
                  		<th>매출일</th>
                  		<th colspan="2">비고</th>
                     <!-- <th>포장수량</th>
                     <th>규격</th>
                     <th>수량</th>
                     <th colspan="2">제안금액</th> -->
                  	</tr>
                  </thead>
                  <tbody>
                     
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
               </table>
					
				<hr>
				<div>	
				 	<button id="productFile_btn"  class="ui inverted primary button" style="float:right;">업로드</button>
				 	<h1 id="propose_report">첨부자료</h1>
				</div>
       			<hr>
       			<table id="file_table">
             
               
               			</table>
               		</form><!-- form end -->
                        <script>
                            var count=1;
                            $(function () {
                            	//버튼 마우스 커서 변경
                    			$('button , .standard_calendar').mouseover(function() {
                    				$('button, input').css({
                    					"cursor" : "pointer"
                    				});
                    			})
                            	
                            	//제품추가 되는 기능
                    			$("#product_add").click(function(){
                    				$('.ui.modal.product').modal('show');
                    			})
                    			//견적 팝업 
                    			$("#estimate_Modal").click(function(){
                    				$('.ui.modal.estimate').modal('show');
                    			})
                    			
                    			$(".exit_Btn").click(function() {
					            $('.ui.modal.client').modal('hide');
					            $('.ui.modal.company').modal('hide');
					            $('.ui.modal.opportunity').modal('hide');
					            $('.ui.modal.manager').modal('hide');
					            $('.ui.modal.estimate').modal('hide');
					            $('.ui.modal.product').modal('hide');
					        	 })
                            	
                            	var exCheck = $("#exCheck");
                            	var upCheck = $("#upCheck");
                            	exCheck.click(function(){
                            		if(exCheck.is(":checked")){
                                		console.log("체크됐어요");
                                		$("#exSel").removeAttr('disabled'); 
                                	} else{
                                		console.log("해제됐어요");
                                		
                                		$("#exSel").attr('disabled', 'true');
                                		$("#exSel").val('NONE');
                                	}	
                            	})
                            	upCheck.click(function(){
                            		if(upCheck.is(":checked")){
                                		console.log("체크됐어요");
                                		$("#upSel").removeAttr('disabled');
                                	} else{
                                		console.log("해제됐어요");
                                		
                                		$("#upSel").attr('disabled', 'true');
                                		$("#upSel").val('NONE');
                                	}	
                            	})
                            	
                            	if($('#comCk').prop("checked")){
                            		$('#completeYn').val("Y");
                            		console.log($('#completeYn').val("Y"));
                            	}else{
                            		$('#completeYn').val("N");
                            	}
                            	
                            	//업로드 클릭시 파일 열리는것
                    			$("#propose_report_Btn").click(function() {
                    				$("#propose_report_file").click();
                    			})
                    			
                    			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
                    					"open");

                    			// 열리는 메뉴
                    			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
                    			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(2)
                    					.addClass("on");

                    			
                    			$("#propose_Request_Btn").click(function() {
                    				$("#propose_Request_file").click();
                    			})
                    			$('#calcel_Btn').click(function() {
                    				location.href = "showcontractEnrollment.sm";
                    			})
                    			//요청일, 제안일 켈린더 뜨는 팝업
                    			$('.standard_calendar').calendar({
                    				type : 'date',
                    				formatter: {
                    					  date : function (date, settings) {
                    					        if (!date) return '';
                    					        var day = date.getDate();
                    					        var month = date.getMonth() + 1;
                    					        var year = date.getFullYear();
                    					        return year + '-' + month + '-' + day;
                    					      }
                    					},
                    			});
                    			$('.start_calendar').calendar({
                    				  type: 'date',
                    				  formatter: {
                    					  date : function (date, settings) {
                    					        if (!date) return '';
                    					        var day = date.getDate();
                    					        var month = date.getMonth() + 1;
                    					        var year = date.getFullYear();
                    					        return year + '-' + month + '-' + day;
                    					        /* return day + '/' + month + '/' + year; */
                    					      }
                    					},
                    				  endCalendar: $('.end_calendar')
                    				});
                    				$('.end_calendar').calendar({
                    				  type: 'date',
                    				  formatter: {
                    					  date : function (date, settings) {
                    					        if (!date) return '';
                    					        var day = date.getDate();
                    					        var month = date.getMonth() + 1;
                    					        var year = date.getFullYear();
                    					        return year + '-' + month + '-' + day;
                    					        /* return day + '/' + month + '/' + year; */
                    					      }
                    					},
                    				  startCalendar: $('.start_calendar')
                    				});

                                $('#productFile_btn').click(function (e) {
                                     e.preventDefault();
                                     var str = '<tr><td><input type="file" name="files'+ (count++) +'" style="display:none" ><input class="upload_name" id="upload_name' 
                                    + (count++) + '" readonly style="border:0;""></td><td><i class="trash alternate outline icon" style="float: right" onclick="remove_btn($(this))"></i></td></tr>';
                                    
                                    console.log("트리거 시작전");
                                    $("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
                                    console.log("트리거 시작후");
                                    $("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
                                         var cur=$(this).val();
                                         var arSplitUrl = cur.split('\\');
                                         var nArLength = arSplitUrl.length;
                                         var fileName = arSplitUrl[nArLength-1];
                                           
                                           $("#upload_name" + (count-1)).val(fileName);
                                         console.log(cur);
                                      }); 
                                   }); 
                                });
                            
                            function remove_btn(obj){
                                obj.parent().parent().remove();
                            }
                        </script>
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
		var gfv_count = 1;
		function fn_addFile(){
			$("#propose_report_file").click();
		}
		 $("#propose_report_file").change(function(e){
			 var fileName = $('input[type=file]')[0].files[0].name
			 var fileType = $("#propose_report_file")[0].files[0].type
			 var fileSize = $("#propose_report_file")[0].files[0].size
			 console.log(fileName);
			 console.log(fileType);
			 console.log(fileSize);
			 var str = "<p><input type='file' name='file_"+(gfv_count++)+"'><a href='#this' class='btn' name='delete'>삭제</a></p>";
			
			 $("#fileDiv").append(str); $("a[name='delete']").on("click", function(e){
			 
			 });
			 console.log($("#fileDiv").children().children('input'))
			 $("#fileDiv").children().children('input').val(fileName);
		 });
		
		 $(document).keypress(function(e){
			 var tfoot = $('#contract_Product_Table tfoot').children().children();
			 
			  if(e.target.parentNode.offsetParent.id =='contract_Product_Table'){
				  
				  $(e.target).off().on('propertychange keyup paste',function(e) {
				  //change
				  /* var $(this). */
				  var event = $(e.target);
				  var evt = e.target;
				  var th = $(this);
				  var sum = event.parent().parent().children().eq(3);
				  var amount = event.parent().parent().children().children().eq(2).val();
				  var price = event.parent().parent().children().children().eq(1).val();
				  var tfoot = $("tfoot").html();
				  console.log("tfoot");
				  var tf = $("#resultSum");
				  var resultA = 0;
				  var resultS = 0;
				  var amountArr  = $(".amount");
				  var sumArr  = $(".sum");
				  console.log("sum : " + sumArr);
				  console.log(sumArr);
				  
				  for(var i = 0; i < amountArr.length; i++){
					  console.log(amountArr[i].value);
					  resultA += Number(amountArr[i].value);
					}
				  for(var i = 0; i < sumArr.length; i++){
					  console.log(sumArr[i].value);
					  resultS += Number(sumArr[i].innerHTML);
					}
				  console.log(resultS);
				  //수량 , 합계 바꾸는 부분
				  tf.children().eq(1).html(resultA.toFixed()) 
				  /* tf.children().eq(3).html(resultS.toFixed()) */
				  /* var allAmount = $("#resultSum");
				  var allPrice = 0; */
				  console.log(tfoot);
					  if(event.hasClass("price")){
						 console.log("price 까격 변함");
						 //변한 제품에 대한 값 변화하는 곳
						 sum.html(event.val() * amount);
						  
						  console.log("all : " + tf.children().eq(3).html())
						  tf.children().eq(1).html(resultA.toFixed())
						  //총합계구하는 ㅓㄱㅅ 
						  var sArr  = $(".sum");
						  var s  = 0;
						  for(var i = 0; i < sArr.length; i++){
							  console.log(sumArr[i].value);
							  s += Number(sumArr[i].innerHTML);
							}
						 
						  tf.children().eq(3).html(s.toFixed()) 
					  } else if (event.hasClass("sum")){
						  /* console.log("sum");
						  console.log("all : " + tf.children().eq(3).html())
						  console.log(th.parent().children('.sum').val()) */
					  } else if (event.hasClass("amount")){
						  var sum = event.parent().parent().children().eq(3);
						  console.log("amount 수량변함");
						  console.log("all : " + tf.children().eq(3).html())
						  //변한 제품에 대한 값 변화하는 곳
						  sum.html(event.val() * price);
						  tf.children().eq(1).html(resultA.toFixed())
						//총합계구하는 ㅓㄱㅅ 
						  var sArr  = $(".sum");
						  var s  = 0;
						  for(var i = 0; i < sArr.length; i++){
							  console.log(sumArr[i].value);
							  s += Number(sumArr[i].innerHTML);
							}
						 
						  tf.children().eq(3).html(s.toFixed()) 
					  }
				  })
			 } 
		 }) 
		
			
		
	</script>
</body>
</html>