<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management2.css">
<style>
	.resultContents{
		/* height:0px; */
		height:100%;
		display:inline-block;
	}
	.resultContents:hover{
		border:1px solid #27aae1;
		color:#27aae1;
		cursor:pointer;
	}
	.resultContents:hover *{
		color:#27aae1;
	}
</style>
</head>
<jsp:include page="../../common/menubar.jsp" />
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div id="header_div">
				<h1 id="estimate_h1">
					계약
					<!-- <button id="contract_estimate_Btn" class="search_Btn">등록하기</button> -->
					<button class = "ui inverted primary button" id="contract_estimate_Btn" style="float:right;">등록하기</button>
				</h1>
				<div class="search">
					<div class="one">
						<div class="half input">
							<input type="text" placeholder="견적/고객/고객사">
						</div>
						<div class="half input">
							<input type="text" placeholder="제품명">
						</div>
					</div>
					<div class="secondSearchDiv">
						<div class="select half"> 
							<select>
								<option>영업부</option>
							</select>
						</div>
						<div class="input">
							<input type="text" placeholder="사원명">
							<button class = "ui inverted primary button" style="margin-left:2px;">검색</button>
							<!-- <button class="ui blue basic button float">검색</button> -->
						</div>
					</div>
				</div>
				<!-- <input type=text id="costomer_text_esimate" placeholder="계약/고객/고객사">
				<input type="text" id="product_Name" placeholder="제품명"> <br>
				<input type=text id="Department" placeholder="부서명"> <input
					type=text id="employee" placeholder="사원명">
				<button class='submit_Btn' id="search_estimate_Btn">검색</button>
				<div> 
					<div class="two fields">
						<div class="field">
							<div class="ui calendar" id="rangestart">
								<div class="ui input right icon" id="contract_start_calendar">
									<i class="calendar icon"></i> <input type="text"
										class="contract_input_calendar" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p id="exclamationMark">~</p>
				<div>
					<div class="two fields">
						<div class="field">
							<div class="ui calendar" id="rangeend">
								<div class="ui input right icon" id="contract_end_calendar">
									<i class="calendar icon"></i> <input type="text"
										class="contract_input_calendar" readonly>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
			<div id="content_div">
				<div>
					<button id="filter_Btn">필터</button>
					<P><c:out value="${fn:length(list)}"></c:out>건</P>
					<p class="filters">가나다</p>
					<p class="filters">고객사 |</p>
					<p class="filters" style="font-weight: bold;">견적일▼ |</p>
				</div>
				<!-- <hr> -->
				<c:forEach var="cont" items="${list}">
					<div class="resultContents">
						<input type="hidden" value="${cont.contId}" >
						<p><c:out value="${cont.contName}"/></p>
						<p><c:out value="${cont.comName}"/>  <c:out value="${cont.clientName}"/></p>
						<p><c:out value="${cont.contDate}"/></p>
					</div>
				</c:forEach>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	<script>
		$(function() {
			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(2)
					.addClass("on");
			
			$('#rangestart').calendar({
				type : 'date',
				endCalendar : $('#rangeend')
			});
			$('#rangeend').calendar({
				type : 'date',
				startCalendar : $('#rangestart')
			});
			$('#contract_estimate_Btn').click(function() {
				location.href = "showContractEnrollment.sm";
			})
			$('.resultContents').click(function() {
				var contId = $(this).children('input').val();
				/* location.href = "showContractDetail.sm?contId=" + contId; */
				location.href = "selOne.con?contId=" + contId;
			})
		})
	</script>
</body>
</html>