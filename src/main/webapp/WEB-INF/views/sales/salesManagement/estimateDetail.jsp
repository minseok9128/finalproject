<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 견적상세</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/menubar.jsp" />
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script 
	src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
</head>
<body>
	<c:set var="est" value="${est}"/>
	<c:set var="atList" value="${atList}"/>
	<c:set var="proList" value="${proList}"/>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title "></div>
			<div class="header_div">
				<div>
					<h1>견적세부사항</h1>
					<button class="ui inverted red button" id="cancelBtn">취소</button>
					<button class="ui inverted primary button" id="modifyBtn">수정</button>
				<h3><c:out value="${est.estName}"/></h3>
				<input type="hidden" id="hiddenEstId" value="${est.estId}">
				</div>
			</div>
			<div class="content_div">
				<table id="table2">
					<tr>
						<td class="td">고객사</td>
						<td>
							<a class="Detail_a" href="selOne.co?ComId=${est.companyId}"><c:out value="${est.comName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">고객</td>
						<td>
							<a class="Detail_a"><c:out value="${est.clientName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">영업기회</td>
						<td>
							<a class="Detail_a" href="selectOne.so?oppId=${est.oppId}"><c:out value="${est.estName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">견적일</td>
						<td>
							<a class="Detail_a2"><c:out value="${est.estDate}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">부가세</td>
						<td>
							<a class="Detail_a2">
								<c:if test="${est.estVatYn == 'N'}">
									미포함
								</c:if>
								<c:if test="${est.estVatYn == 'Y'}">
									포함
								</c:if>
							</a>
						</td>
					</tr>
					<tr>
						<td class="td">전체 공급가액</td>
						<td>
							<a class="Detail_a2" id="totalValue"></a>
						</td>
					</tr>
					<tr>
						<td class="td">제안 공급가액</td>
						<td>
							<a class="Detail_a2" id="estValue"></a>
						</td>
					</tr>
					<tr>
						<td class="td">세액</td>
						<td>
							<a class="Detail_a2" id="estTax"></a>
						</td>
					</tr>
					<tr>
						<td class="td">합계</td>
						<td>
							<a class="Detail_a2" id="estTotal"></a>
						</td>
					</tr>
					<tr>
						<td class="td">담당자</td>
						<td>
							<a class="Detail_a2"><c:out value="${est.empName}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">내용</td>
						<td>
							<a class="Detail_a2"><c:out value="${est.estContent}"/></a>
						</td>
					</tr>
				</table>
				<div class="estumateproduct">
					<h1 class="propose_report">견적품목</h1>
				</div>
				<table class="estumateTable" id="contract_Product_Table">
					<thead>
						<tr>
							<th colspan="2">제품명</th>
							<th>단가</th>
							<th>할인</th>
							<th>합계</th>
						</tr>
						<tr>
							<th>포장수량</th>
							<th>규격</th>
							<th>수량</th>
							<th colspan="2">제안금액</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${proList}" varStatus="status">
							<tr>
								<td colspan="2"><c:out value="${proList[status.index].PDT_NAME}"/></td>
								<td><c:out value="${proList[status.index].DETAIL_PRICE}"/></td>
								<td><c:out value="${proList[status.index].DISCOUNT_RATE}"/> % </td>
								<td><c:out value="${proList[status.index].TOTAL_PRICE}"/></td>
							</tr>
							<tr>
								<td><c:out value="${proList[status.index].QUANTITY}"/></td>
								<td><c:out value="${proList[status.index].STANDARD}"/></td>
								<td><c:out value="${proList[status.index].QUANTITY_DETAIL}"/></td>
								<td colspan="2"></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>

					</tfoot>
				</table>
				<br><br>
				<div class="bottom black">
					<h1 id="attachment">첨부자료</h1>
				</div>
				<div>
					<table class="download_Table" id="attachmentTable">
						<c:forEach items="${atList}" varStatus="status">
							<tr>
								<td>
									<c:out value="${atList[status.index].originName}"></c:out>
									<input type="hidden" value="${atList[status.index].fileId}">
								</td>
								<td>
									<button class="ui inverted primary button srch">다운로드</button>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
	$(function(){
		var totalValue = ${est.estTotalValue};
		var estValue = ${est.estValue};
		var estTax = ${est.estTax};
		var estTotal = ${est.estTotal};
		$('#totalValue').html(comma(totalValue));
		$('#estValue').html(comma(estValue));
		$('#estTax').html(comma(estTax));
		$('#estTotal').html(comma(estTotal));
		
		var msg = '${msg}';
		
		console.log(msg);
		
		var table = $('#contract_Product_Table').children().eq(1).children();
		 for(var i=0; i<table.length; i++) {
			if(i%2 == 0){
				var price = $('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(1).text();
				$('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(1).text(comma(price));
				var totalPrice = $('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(3).text();
				$('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(3).text(comma(totalPrice));
			} else {
				var quantity =  $('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(2).text()
				$('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(2).text(comma(quantity));
				 var ProAmount = totalPrice * quantity;
				$('#contract_Product_Table').children().eq(1).children().eq(i).children().eq(3).text(comma(ProAmount));
			}
			
		 } 
		
		$('.cancel_Btn').click(function(){
			location.href="showEstimateMain.sm";
		})
		//모달 뛰우는 스크립트
		$("#curstormer_search").click(function(){
			$('.ui.modal.curstormer')
			  .modal('show');
		})
		$("#company_search").click(function(){
			$('.ui.modal.company')
			  .modal('show');
		})
		$("#opportunity_search").click(function(){
			$('.ui.modal.opportunity')
			  .modal('show');
		})	
		$("#manager_search").click(function() { 
			$('.ui.modal.manager')
			  .modal('show');
		})
		$("#exit_Btn").click(function(){
			$('.ui.modal.curstormer').modal('hide');
		})
		$("#exit_Btn2").click(function(){
			$('.ui.modal.company').modal('hide');
		})
		$("#exit_Btn3").click(function(){
			$('.ui.modal.opportunity').modal('hide');
		})
		$("#exit_Btn4").click(function(){
			$('.ui.modal.manager').modal('hide');
		})
		$("#propose_Request_Btn").click(function(){
			$("#propose_Request_file").click();
		})
		$("#propose_report_Btn").click(function(){
			$("#propose_report_file").click();
		})
		$('#calcel_Btn').click(function(){
			location.href="showOfferView.sm";
		})
	
		//요청일, 제안일 켈린더 뜨는 팝업
			$('#standard_calendar').calendar({
				type : 'date'
			});
			$('#standard_calendar2').calendar({
				type : 'date'
			});
			//버튼 마우스 커서 변경
			$('button').mouseover(function() {
				$('button').css({
					"cursor" : "pointer"
				});
			})
		})
		$(function(){
        // 열리지 않는 메뉴
       $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
        
        // 열리는 메뉴
        $(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
        $(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(1).addClass("on");
    });
	function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
	$('#modifyBtn').on('click', function(){
		var estId = $('#hiddenEstId').val()
		location.href='modifyEstimate.est?estId='+estId;
	})
	$('#reportTable td button')
					.click(
							function(e) {
								var originName = $(this).parents('tr')
										.children()[0].innerText;
								var fileName = $(this).parents('tr').children()
										.eq(0).children().eq(0).val();
								var filePath = $(this).parents('tr').children()
										.eq(0).children().eq(1).val();
								var extension = originName.split(".");
								var encodefilePath = encodeURI(filePath);
								fileName = fileName + '.' + extension[1];
								location.href = "download.do?fileName="
										+ fileName + "&filePath="
										+ encodefilePath + "&originName="
										+ originName;
							})
	</script>
</body>
</html>