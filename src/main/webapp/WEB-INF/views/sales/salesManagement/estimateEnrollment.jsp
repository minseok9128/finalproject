<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 견적등록</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/menubar.jsp" />
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<jsp:include page="../../common/modal/product2.jsp"></jsp:include>
</head>
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>

		<div class="tb_cnt">
			<div class="tb_title "></div>
			<c:set var="estId" value="${estId}" />
			<c:if test="${estId == 0}">
				<!-- estId가 0 이면 등록 컨트롤러 이동 -->
				<c:set var="move" value="estimateEnrollment.est" />
			</c:if>
			<c:if test="${estId != 0}">
				<c:set var="move" value="estimateModify.est"/>
				<!-- estId가 0이 아니면 수정 컨트롤로 이동 -->
			</c:if>
			<form action="${move}" method="post" enctype="multipart/form-data">
				<div class="header_div">
					<c:if test="${estId == 0}">
						<c:set var="list" value="0" />
					</c:if>
					<c:if test="${estId != 0}">
						<c:set var="list" value="${list}" />
					</c:if>
					<div>
						<c:if test="${estId == 0}">
							<h1>견적 등록</h1>
						</c:if>
						<c:if test="${estId != 0}">
							<h1>견적 수정</h1>
						</c:if>
						<button id="realBtn" hidden>이것은 입력하지 않은 file input 태그를
							지우고 controller에 보내는 버튼</button>
						<button class="ui inverted red button float" id="cancelEnrollBtn">취소</button>
						<c:if test="${estId == 0}">
						</c:if>
						
							<button class="ui inverted primary button float" id="modifyBtn">
							<c:if test="${estId == 0}">
								작성
							</c:if>
							<c:if test="${estId != 0}">
								수정
							</c:if>
							</button>
					</div>
				</div>
				<div id="content_Enrollment_div">
					<c:set var="est" value="${est}" />
					<table id="table2">
						<tr>
							<td class="td">견적명<b class="point">*</b></td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="offer_Name" class="common_Text1"
										name="estName">
								</c:if> <c:if test="${estId != 0}">
									<input type="text" id="offer_Name" class="common_Text1"
										name="estName" value="${est.estName}">
									<!-- 불러온 값 넣어야 함 -->
									<input type="hidden" value="${est.estId}" name="estId">
								</c:if></td>
						</tr>
						<tr>
							<td class="td">고객사</td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="company" class="common_Text2" name="comName">
									<input type="hidden" id="company_hidden" value="0" name="companyId">
								</c:if> <c:if test="${estId != 0 }">
									<!-- 값 불러온거 넣어야 함 -->
									<input type="text" id="company" class="common_Text2"
										name="comName" value="${est.comName}">
									<input type="hidden" id="company_hidden"
										value="${est.companyId}" name="companyId">
								</c:if>
								<button class="ui grey basic button srch searchBtn"
									id="company_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">고객<b class="point">*</b></td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="client" class="common_Text2"
										name="clientName">
									<input type="hidden" id="client_hidden" value="0"
										name="clientId">
								</c:if> <c:if test="${estId != 0}">
									<!-- 불러온 값 넣어야 함 -->
									<input type="text" id="client" class="common_Text2"
										name="clientName" value="${est.clientName}">
									<input type="hidden" id="client_hidden" value="${est.clientId}"
										name="clientId">
								</c:if>
								<button class="ui grey basic button srch searchBtn"
									id="client_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">영업기회</td>
							<td>
								<c:if test="${estId == 0}">
									<input type="text" id="opportunity" class="common_Text2" name="oppName">
									<input type="hidden" id="opportunity_hidden" value="0" name="oppId" />
									<input type="hidden" value="0" id="processId" name="processId" value="0"/>
								</c:if> <c:if test="${estId != 0}">
									<!-- 불러온 값 넣어야 함 -->
									<input type="text" id="opportunity" class="common_Text2"
										name="oppName" value="${fn:split(est.estName,' ')[0]}">
									<input type="hidden" id="opportunity_hidden"value="${est.oppId}" name="oppId" />
								</c:if>
								<button class="ui grey basic button srch searchBtn"
									id="opportunity_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">견적일<b class="point">*</b></td>
							<td>
								<div class="field">
									<div class="ui calendar" id="standard_calendar2">
										<div class="ui input right icon">
											<i class="calendar icon"></i>
											<c:if test="${estId == 0}">
												<input type="text" id="estDate" readonly name="estDate">
											</c:if>
											<c:if test="${estId != 0}">
												<!-- 불러온 값 넣어야 함 -->
												<input type="text" id="estDate" readonly name="estDate" value="${est.estDate}">
											</c:if>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td">수량<b class="point">*</b></td>
							<td><c:if test="${estId == 0 }">
									<input type="text" id="price" class="common_Text1 text_text product_add" value="0" readonly
										name="estQuantityString">
								</c:if> <c:if test="${estId != 0 }">
									<input type="text" id="price" class="common_Text1 text_text product_add" readonly name="estQuantityString" value="${est.estQuantity}">
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p></td>
						</tr>
						<tr>
							<td class="td">전체 공급가액<b class="point">*</b></td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="totalValueOfSupply"
										class="common_Text1 text_text product_add" value="0" readonly
										name="estTotalValueString">
								</c:if> <c:if test="${estId != 0}">
									<input type="text" id="totalValueOfSupply" class="common_Text1 text_text product_add" readonly
										name="estTotalValueString" value="${est.estTotalValue}">
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p></td>
						</tr>
						<tr>
							<td class="td">제안 공급가액<b class="point">*</b></td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="suggestionValueOfSupply"
										class="common_Text1 text_text product_add" value="0" readonly
										name="estValueString">
								</c:if> <c:if test="${estId != 0}">
									<input type="text" id="suggestionValueOfSupply" class="common_Text1 text_text product_add" readonly
										name="estValueString" value="${est.estValue}">
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p></td>
						</tr>
						<tr>
							<td class="td">세액<b class="point">*</b></td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="tex"
										class="common_Text1 text_text product_add" value="0" readonly
										name="estTaxString">
								</c:if> <c:if test="${estId != 0}">
									<input type="text" id="tex" class="common_Text1 text_text product_add" readonly
										name="estTaxString" value="${est.estTax}">
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p></td>
						</tr>
						<tr>
							<td class="td">합계<b class="point">*</b></td>
							<td><c:if test="${estId == 0}">
									<input type="text" id="total"
										class="common_Text1 text_text product_add" value="0" readonly
										name="estTotalString">
								</c:if> <c:if test="${estId != 0}">
									<input type="text" id="total" class="common_Text1 text_text product_add"
										value="${est.estTotal}" readonly name="estTotalString">
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p></td>
						</tr>
						<tr>
							<td class="td">부가세</td>
							<td><c:if test="${estId == 0 }">
									<input type="checkbox" id="VATCheckBox"
										class="enrollment_CheckBox" value="Y" name="estVatYn">
								</c:if> <c:if test="${estId != 0}">
									<input type="checkbox" id="VATCheckBox" class="enrollment_CheckBox" value="Y"
										name="estVatYn">
								</c:if></td>
						</tr>
						<tr>
							<td class="td">담당자<b class="point">*</b></td>
							<td><c:if test="${estId == 0 }">
									<input type="text" class="common_Text2" value="${ sessionScope.loginEmp.empName }" readonly name="empName">
									<input id="empNo" type="hidden" value="${ sessionScope.loginEmp.empNo }" name="empNo">
								</c:if> <c:if test="${estId != 0}">
									<input type="text" class="common_Text2" value="${est.empName}">
									<input id="empNo" type="hidden" name="empNo" value="${est.empNo}" />
								</c:if> <c:if test="${ sessionScope.loginEmp.empName == '관리자'}">
									<button class="ui grey basic button srch searchBtn" id="manager">검색</button>
								</c:if></td>
						</tr>
						<tr>
							<td class="td">내용</td>
							<td><c:if test="${estId == 0}">
									<textarea class="contents" name="estContent"></textarea>
								</c:if> <c:if test="${estId != 0 }">
									<textarea class="contents" name="estContent"><c:out
											value="${est.estContent}" /></textarea>
								</c:if></td>
						</tr>
					</table>
					<div class="estumateproduct">
						<h1 class="propose_report">견적 품목</h1>
						<button class="ui inverted primary button float product_add" id="estBtn"
							value="estBtn">제품 등록</button>
					</div>
					<table class="estumateTable" id="contract_Product_Table">
						<c:set var="proList" value="${proList}" />
						<thead>
							<tr>
								<th colspan="2">제품명</th>
								<th>단가</th>
								<th>할인</th>
								<th>합계</th>
								<th rowspan="2">삭제</th>
							</tr>
							<tr>
								<th>포장수량</th>
								<th>규격</th>
								<th>수량</th>
								<th colspan="2">제안금액</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${proList}" varStatus="status">
								<tr>
									<td colspan="2">
										<c:out value="${proList[status.index].PDT_NAME}" />
										<input type="hidden" value="${proList[status.index].PDT_DTAIL_ID}"/>
										<input type="hidden" value="${proList[status.index].PDT_ID}" name="pdtId">
									</td>
									<td>
										<input type="text" name="price" value="${proList[status.index].DETAIL_PRICE}">
										<input type="hidden"  value="${proList[status.index].DETAIL_PRICE}"/>
									</td>
									<td>
										<input type="text" name="discountRate" value="${proList[status.index].DISCOUNT_RATE}">%
										<input type="hidden" value="${proList[status.index].DISCOUNT_RATE}"/>
									</td>
									<td>
										<input type="text" value="${proList[status.index].TOTAL_PRICE}">
										<input type="hidden" name="detailPrice" value="${proList[status.index].TOTAL_PRICE}">
									</td>
									<td rowspan="2"><i class="trash alternate outline icon center"></i></td>
								</tr>
								<tr>
									<td>
										<c:out value="${proList[status.index].QUANTITY}" />
									</td>
									<td><c:out value="${proList[status.index].STANDARD}" /></td>
									<td>
										<input type="text" name="quantity" value="${proList[status.index].QUANTITY_DETAIL}">
										<input type="hidden"  value="${proList[status.index].QUANTITY_DETAIL}">
									</td>
									<td colspan="2"><input type="text" value="${proList[status.index].QUANTITY_DETAIL * proList[status.index].TOTAL_PRICE}"></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							  <c:if test="${estId != 0}">
								<tr>
									<th colspan="2">수량합계</th>
									<th></th>
									<th>제안금액 합계</th>
									<th></th>
									<th></th>
								</tr>
							  </c:if>
						</tfoot>
					</table>


					<div class="fileUploadDiv" id="fileUp1">
						<c:set var="atList" value="${atList}" />
						<div>
							<h1>첨부자료</h1>
							<button class="ui inverted primary button float" id="attachmentBtn">자료등록</button>
							<br>
							<hr>
						</div>
						<ul>
							<c:if test="${estId != 0 }">
								<c:forEach items="${atList}" varStatus="status">
									<li>
										${atList[status.index].originName}<i class="trash alternate outline icon right"></i> <input type="hidden" value="${request[status.index].fileId}">
										<input type="hidden" value="${atList[status.index].fileId}">
										<input type="hidden" value="${atList[status.index].category}">
									</li>
								</c:forEach>
							</c:if>
						</ul>
					</div>
				</div>
			</form>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
	var changeYn = ${estId};
	
	if(changeYn != 0){
		$('#opportunity_Modal ,#client_Modal, #company_Modal').prop("disabled", true);
	}
	
		$(function() {
			var totalValue = ${est.estTotalValue}+'';
			var estValue = ${est.estValue}+'';
			var estTax = ${est.estTax}+'';
			var estTotal = ${est.estTotal}+'';
			var estcheck = '${est.estVatYn}';
			var quantity = 0;
			var total = 0;
			var totalPrice = 0;
			$('#totalValueOfSupply').val(comma(totalValue));
			$('#suggestionValueOfSupply').val(comma(estValue));
			$('#tex').val(comma(estTax));
			$('#total').val(comma(estTotal));
			console.log(estcheck)
			if(estcheck == 'Y'){
				$('#VATCheckBox').prop('checked', true);
			}
			 var table = $('#contract_Product_Table').children().eq(1).children();
			 var tfoot = $('#contract_Product_Table tfoot').children().children();
			 for(var i=0; i<table.length; i++) {
				if(i%2 == 0){
				   totalPrice = $(table).eq(i).children().eq(3).children().val()
				} else {
					quantity += Number($(table).eq(i).children().eq(2).children().val());
					total += Number(totalPrice * Number($(table).eq(i).children().eq(2).children().val()));
					
				}
			 }
			 if(table.length != 0){
					tfoot[1].innerHTML = '';
					tfoot[1].innerHTML = comma(quantity);
					tfoot[3].innerHTML = '';
					tfoot[3].innerHTML = comma(total);
			 }
				
			$('.cancel_Btn').click(function() {
				location.href = "showEstimateMain.sm";
			})
			//모달 뛰우는 스크립트
			//요청일, 제안일 켈린더 뜨는 팝업
			$('#standard_calendar2').calendar({
				monthFirst : false,
				type : 'date',
				formatter : {
					date : function(date, settings) {
						var day = date.getDate();
						var month = date.getMonth() + 1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				}
			});
			//버튼 마우스 커서 변경
			$('button').mouseover(function() {
				$('button').css({
					"cursor" : "pointer"
				});
			})
		})
		$(function() {
			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(1)
					.addClass("on");

			var tbody = $('#contract_Product_Table tbody');
			$(".product_add").click(function() {
				$('.ui.modal.product2').modal('show');
				if($(".ui.modal.product2").is(":visible")){
					console.log($('#result_Table_Modal').children()[1]);
					
				}
				return false;
			})
		});

		function changenTex(totalValueOfSupply) {
			var a = 0;
			var StringtotalValueOfSupply = totalValueOfSupply.split(',');
			var numtotalValueOfSupply = '';
			for (var i = 0; i < StringtotalValueOfSupply.length; i++) {
				numtotalValueOfSupply += StringtotalValueOfSupply[i]
			}
			a = numtotalValueOfSupply;
			var d = (a * 1.1 - a).toFixed(3);
			var b = d.split('.');
			var c = 0;

			if (b[1] == '000') {
				c = b[0];
			} else {
				c = d;
			}
			return c
		}
		//세액으로 변환 부가가치세 포함
		function changenTexVAT(totalValueOfSupply) {
			var a = 0;
			var StringtotalValueOfSupply = totalValueOfSupply.split(',');
			var numtotalValueOfSupply = '';
			for (var i = 0; i < StringtotalValueOfSupply.length; i++) {
				numtotalValueOfSupply += StringtotalValueOfSupply[i]
			}
			a = numtotalValueOfSupply;
			var d = Math.abs((a / 1.1 - a)).toFixed(3);
			var b = d.split('.');
			var c = 0;

			if (b[1] == '000') {
				c = b[0];
			} else {
				c = d;
			}
			return c
		}

		function total(a, b) {
			//세금
			var Tex = changeNumber(a);
			//전체 공급가액
			var totalValueOfSupply = changeNumber(b);

			return totalValueOfSupply + Tex
		}

		function changeNumber(a) {
			a = a+'';
			if (a.indexOf(',') != -1) {
				var StringA = a.split(',');
				var numberA = '';
				for (var i = 0; i < StringA.length; i++) {
					numberA += StringA[i];
				}
			} else {
				numberA = a;
			}

			return Number(numberA);
		}
		var a = 0;
		var b = 0;

		$(document).click(function(e) {
							var btnName = (e.target.id);
							switch (btnName) {
							case 'attachmentBtn':
								$(function() {
									var fileSelectEle = document.getElementById('fileinput');
									var ppsId = ${estId}+'';
									var fileName = $("#attachment" + a + "");
									$('#fileUp1 ul').append('<input type="file" id="attachment'+a+'" name="attachment" value="0"hidden>');
									$("#attachment" + a + "").click();
									var liFilelength = $('#fileUp1').children('ul').children().length;
									$("#attachment" + a + "").change(function() {
														var sameFileName = true;
														var cur = $(this).val();
														var arSplitUrl = cur.split('\\');
														var nArLength = arSplitUrl.length;
														var fileName = arSplitUrl[nArLength - 1];
														for (var i = 0; i < liFilelength; i++) {
															if ($('#fileUp1').children('ul').children()[i].innerText == fileName) {
																sameFileName = false;
															}
														}
														if (sameFileName) {
															$('#fileUp1 ul').append("<li>"+ fileName+ "<i class='trash alternate outline icon right'></i><input type='hidden' value='0'></li>");
														} else {
															alert("동일 이름의 파일이 있습니다.");
															$(this).remove();
														}
													})
									a++;
								})
								return false;
								break;
							case 'modifyBtn':
								console.log($('#estDate').val());
								if($('#offer_Name').val() == '' || $('#client_hidden').val() == 0 || $('#estDate').val() == '' || $('#price').val() == 0 || $('#empNo').val() == 0){
									 alert("[필수]정보를 입력해주세요!!")
								 } else {
											var filelength = $('#fileUp1').children('input').length;
											var file = $('#fileUp1').children('input');
											if(filelength != null){
												for (var i = 0; i < filelength; i++) {
														file[i].remove();
												}
											}
											$('#realBtn').click();
								 }
								return false;
								break;
								
							case 'cancelEnrollBtn' :
								 history.back();
									return false;
									break;
							}//스위치문 끝
						})

		$(document).on('click', '#fileUp1', function(e) {
			if (e.target.classList[0] == 'trash') {
				var result = confirm("정말 삭제하시겠습니까?");
				if (result == true) {
					var fileId = e.target.parentNode.children[2].value;
					console.log(fileId);
					$('#fileUp1 ul').append("<input type='hidden' name='deleteFile' value='"+fileId+"'>");
					//deletFile(fileId);
					//e.target.parentNode.remove();
					e.target.parentNode.remove();
				}
			}
		})

		$(document).on('click','#VATCheckBox',function(){
			var quantity = changeNumber($('#contract_Product_Table tfoot').children().children()[1].innerHTML);
			var totalPrice = changeNumber($('#contract_Product_Table tfoot').children().children()[3].innerHTML);
			var totalValue;
			var tax;
			if($('#VATCheckBox').is(':checked')){
				totalValue = Math.round(totalPrice/1.1);
				tax = totalPrice - totalValue
				 $('#totalValueOfSupply').val(comma(totalPrice - tax));
				 $('#suggestionValueOfSupply').val(comma(totalPrice - tax));
				 $('#tex').val(comma(tax));
				 $('#total').val(comma(totalPrice));	
				} else {
					totalValue = Math.round(totalPrice*1.1);
					tax = totalValue - totalPrice 
					 $('#totalValueOfSupply').val(comma(totalValue-tax));
					 $('#suggestionValueOfSupply').val(comma(totalValue-tax));
					 $('#tex').val(comma(tax));
					 $('#total').val(comma(totalPrice+tax));
				}
		 })
		 $(function(){
			 
		 })
	</script>
</body>
</html>