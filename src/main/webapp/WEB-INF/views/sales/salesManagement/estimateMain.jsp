<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
</head>
	<jsp:include page="../../common/menubar.jsp" />
<body> 
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div class="header_div">
				<div>
					<h1>견적</h1>
					<button id="enrollment_estimate_Btn" class="ui inverted primary button float">등록</button>
				</div>
				<div class="search">
				<div class="one">
					<div class="half input">
						<input type="text" placeholder="견적/고객/고객사">
					</div>
					<div class="half input">
						<input type="text" placeholder="제품명">
					</div>
				</div>
				<div class="secondSearchDiv">
					<div class="select half"> 
						<select>
							<option>영업부</option>
						</select>
					</div>
					<div class="input">
						<input type="text" placeholder="사원명">
						<button class="ui inverted primary button float">검색</button>
					</div>
				</div>
			</div>
			</div>
			</div>
			<c:set var="aa" value="${aa}"/>
			<c:set var="msg" value="${msg}"/>
			<div id="content_div">
				<div> 
					<button id="filter_Btn">필터</button>
					<c:set var="estCount" value="${estCount}"/>
					<P>${estCount}건</P>
					<p class="filters">가나다</p>
					<p class="filters">고객사 |</p>
					<p class="filters" style="font-weight: bold;">견적일▼ |</p>
				</div>
				<c:set var="list" value="${list}"/>
				<div>
					<ul>
						<c:forEach var="i" items="${list}" varStatus="status">
							<li>
								<div class="resultContents">
									<p><c:out value="${list[status.index].estName}"/></p>
									<input type="hidden" value="${list[status.index].estId}">
									<input type="hidden" class="scrolling" value="${status.index}">
									<p><c:out value="${list[status.index].comName}"/> / <c:out value="${list[status.index].clientName}"/></p>
									<p><c:out value="${list[status.index].estDate}"/></p>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>				
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
	$(function(){
		var aa = '${aa}';
		var msg = '${msg}';
		
		if(aa == 1){
			alert(msg);
		}
		
        // 열리지 않는 메뉴
       $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
        
        // 열리는 메뉴
        $(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
        $(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(1).addClass("on");
    });
	$(function(){
		$('#enrollment_estimate_Btn').on('click',function(){
			location.href="showEstimateEnrollment.sm";
		})
	})
	var lastScrollTop = 0;
	$(function(){
		var memberId = ${sessionScope.loginEmp.empNo};
		$(window).scroll(function(){
			var currentScrollTop = $(window).scrollTop();
			
			if(currentScrollTop - lastScrollTop > 0){
					
				if($(window).scrollTop() >= ($(document).height() - $(window).height())){
					var lastCno = $(".scrolling:last").val();
					var $ul = $('#content_div ul');
					$.ajax({
						type : 'post',
						url : 'estMainscroll.est',
						data : {
							lastCno:lastCno,
							memberId:memberId
							},
						success : function(data) {
							console.log(data);
							for(var i=0; i<data.length; i++) {
								$ul.append('<li>'+
										'<div class="resultContents">'
										+'<p>'+data[i].estName+'</p>'
										+'<input type="hidden" value="'+data[i].estId+'"/>'
										+'<input type="hidden" class="scrolling" value="'+(Number(lastCno)+(i+1))+'"/>'
										+'<p>'+data[i].comName+' / '+data[i].clientName+'</p>'
										+'<p>'+day(data[i].estDate)+'</p>'
										+'</div>'
										+'</li>')
							}
							
						}
					})
					
				}
			}
		})
	})
	function day(day) {
		if(day != null){
			if(day.indexOf(',') != -1){
				var day = day.split(',');
				//월과 달
				var md = day[0].split('월');
				var aa = md[1].split(' ');
				var year = day[1];

				return year+'-'+md[0]+'-'+aa[1];
			}
		} else {
			return "";
		}
	}
	$(document).on('click', '.resultContents p', function(){
		var estId = $(this).parent().children().eq(1).val();
		location.href="EstimateDetail.est?estId="+estId;
	})
	$(document).on('mouseover','.resultContents', function(){
		$(this).css({"border":"1px solid #6199B9"})
		$(this).children().css({"color":"#6199B9"})
		$(this).css({'cursor':'pointer'})
	})
	$(document).on('mouseout','.resultContents', function(){
		$(this).css({"border":"1px solid #DEE2E8"})
		$(this).children().css({"color":"#3D4959"})
	})
	</script>
</body>
</html>