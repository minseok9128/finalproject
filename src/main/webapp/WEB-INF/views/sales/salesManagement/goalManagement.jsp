<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath }/resources/css/common.css">
<style>
	.searchSelect, #searchSelect {
		width: 330px;
		margin-right: 10px;
		margin-top:10px;
	}
 
	.table {
		margin-top: 20px;
		width:760px;
	}
	.table>tbody>tr>td, .td, .firstTd {
		border-bottom:1px solid #E1E6EB;
		border-top:1px solid #E1E6EB;
	}
	.table>tbody>tr>th, .th {
		border-top:2px solid #6199B9;
		height:33px;
	}
	.inline {
		display:inline-block;
	}
	
	tr > .th {
		font-size:13px;
		margin:10px;
		text-align:center;
		vertical-align:middle;
		border-left:1px solid #E1E6EB;
		border-right:1px solid #E1E6EB;
	}
	
	tr > .td, tr > .firstTd {
		border-left:1px solid #E1E6EB;
		border-right:1px solid #E1E6EB;
		boder-bottom:1px solid #E1E6EB;
	}
	
	.thWidth {
		height:40px;
		width:45px;
	}

	.subUnit {
		font-size:13px;
	}
	
	.right {
		float:right;
	}

	#headerStyle {
		background-color:rgb(97,153,185);
	}
	.title {
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;
	}
	.header {
		color:white;
		border:none;
		width:400px;
		background:none;
		pointer-events:none;
		padding:17px;
		font-size:20px;
	}
	.table td:nth-child(1) {
		width: 55px;
}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn">
		<div class="title">목표관리</div>
		<div class="tb_cnt">
			<div style="width: 368px; height: 35px;"></div>
			<div style="margin-top: auto; margin-bottom: auto;">
				<select class="ui dropdown searchSelect">
					<option value="">상품명</option>
					<c:forEach var="Goal" items="${goalList}" varStatus="status">
						<option value="${Goal.pdtId}"><c:out value="${Goal.pdtName}"/></option>
					</c:forEach>
				</select>
				<div class="ui calendar inline" id="standard_calendar">
					<div class="ui input left icon">
						<i class="calendar icon"></i>
						<input type="text" id="searchSelect" class="productDate" placeholder="Date/Time">
					</div>
				</div>
				<button class="medium ui inverted blue button searchBtn">검색</button>
			</div>
			<div>
				<p class="subtitle"style="font-size:18px; font-weight:bold; color:#3D4959; margin-top:30px;">매출목표</p>
			</div>
			<div class="subUnit right">
				<p>(단위 : 만원)</p>
			</div>
			<div class=""></div>
			<table class="table" align="center">
				<tr>
					<th class="th"></th>
					<th class="th">제품명</th>
					<th class="th thWidth">합계</th>
					<th class="th thWidth">1월</th>
					<th class="th thWidth">2월</th>
					<th class="th thWidth">3월</th>
					<th class="th thWidth">4월</th>
					<th class="th thWidth">5월</th>
					<th class="th thWidth">6월</th>
					<th class="th thWidth">7월</th>
					<th class="th thWidth">8월</th>
					<th class="th thWidth">9월</th>
					<th class="th thWidth">10월</th>
					<th class="th thWidth">11월</th>
					<th class="th thWidth">12월</th>
				</tr>
				<c:forEach var="Goal" items="${goalList}">
					<tr align="center">
						<td class="firstTd"><button class="mini ui blue basic button showModal" style="margin:7px; padding:9px; font-size:10px; font-weight:bold;">수정</button></td>
						<td class="td"><c:out value="${Goal.pdtName}"/></td>
						<td class="td"><c:out value="${Goal.goalValue}"/></td>
						<td class="td"><c:out value="${Goal.one}"/></td>
						<td class="td"><c:out value="${Goal.two}"/></td>
						<td class="td"><c:out value="${Goal.three}"/></td>
						<td class="td"><c:out value="${Goal.four}"/></td>
						<td class="td"><c:out value="${Goal.five}"/></td>
						<td class="td"><c:out value="${Goal.six}"/></td>
						<td class="td"><c:out value="${Goal.seven}"/></td>
						<td class="td"><c:out value="${Goal.eight}"/></td>
						<td class="td"><c:out value="${Goal.nine}"/></td>
						<td class="td"><c:out value="${Goal.ten}"/></td>
						<td class="td"><c:out value="${Goal.eleven}"/></td>
						<td class="td"><c:out value="${Goal.twelve}"/></td>
						<td class="td" style="display:none;"><c:out value="${Goal.pdtId}"/></td>
						<td class="td" style="display:none;"><c:out value="${Goal.empNo}"/></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	
	<div class="ui modal" style="width:500px;">
		<div id="headerStyle">
			<input type="text" class="header" readonly>
			<i class="close icon" style="float:right; padding:17px; font-size:x-large; margin-right:17px; color:white;"></i>
		</div>
		<form action="updateGoal.sgm" method="post">
			<div align="center">
				<input type="hidden" class="goalPdtId" name="pdtId">
				<table>
					<tr>
						<td style="padding-left: 10px; font-weight:bold;">합계</td>
						<td colspan="4" style="vertical-align:middle;">
							<div class="ui small input">
								<input style="margin:10px;" type="number" size="40" class="total" name="goalValue">
							</div>
						</td>
						<td style="padding-left: 70px;">
							<button	type="button" class="small ui grey basic button totalDiv" style="margin:10px;">균등분할</button>
						</td>
					</tr>
					<tr>
						<td colspan="6"><hr></td>
					</tr>
					<tr>
						<td style="padding: 10px; font-weight:bold;">1월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="one" name="one">
							</div>
						</td>
						<td style="padding: 10px; font-weight:bold;">2월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="two" name="two">
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding: 10px; font-weight:bold;">3월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="three" name="three">
							</div>
						</td>
						<td style="padding: 10px; font-weight:bold;">4월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="four" name="four">
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding: 10px; font-weight:bold;">5월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="five" name="five">
							</div>
						</td>
						<td style="padding: 10px; font-weight:bold;">6월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="six" name="six">
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding: 10px; font-weight:bold;">7월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="seven" name="seven">
							</div>
						</td>
						<td style="padding: 10px; font-weight:bold;">8월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="eight" name="eight">
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding: 10px; font-weight:bold;">9월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="nine" name="nine">
							</div>
						</td>
						<td style="padding: 10px; font-weight:bold;">10월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="ten" name="ten">
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding: 10px; font-weight:bold;">11월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="eleven" name="eleven">
							</div>
						</td>
						<td style="padding: 10px; font-weight:bold;">12월</td>
						<td colspan="2" style="padding: 10px;">
							<div class="ui small input">
								<input type="number" class="number" id="twelve" name="twelve">
							</div>
						</td>
					</tr>
					<tr align="right">
						<td colspan="5"></td>
						<td><button class="ui inverted blue button" style="margin:10px;" type="submit">저장</button></td>	
					</tr>
				</table>
			</div>
		</form>
	</div>
	

	<script>
    	$(function(){
    		$(".menu_cnt .main_menu").eq(3).addClass("on");
    		$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(4).addClass("on");
    		
    		$(document).on('click',".showModal", function(){
    			var list = $(this).parent().parent().children();
    			$(".header").val(list.eq(1).text());
    			$(".total").val(list.eq(2).text());
    			$("#one").val(list.eq(3).text());
    			$("#two").val(list.eq(4).text());
    			$("#three").val(list.eq(5).text());
    			$("#four").val(list.eq(6).text());
    			$("#five").val(list.eq(7).text());
    			$("#six").val(list.eq(8).text());
    			$("#seven").val(list.eq(9).text());
    			$("#eight").val(list.eq(10).text());
    			$("#nine").val(list.eq(11).text());
    			$("#ten").val(list.eq(12).text());
    			$("#eleven").val(list.eq(13).text());
    			$("#twelve").val(list.eq(14).text());
    			$(".goalPdtId").val(list.eq(15).text());
    			
            	$(".ui.modal").modal('show');
            });
    		
    		$(".close").click(function(){
    			$(".ui.modal").modal('hide');
    		});
    		
    		$(".totalDiv").mouseenter(function(){
    			$(".totalDiv").css("background-color","#767676");
    			$(".totalDiv").css("color","white");
    		}).mouseleave(function(){
    			$(".totalDiv").css("color","#767676");
    			$(".totalDiv").css("background-color","white");
    		});
    		
    		$(".searchBtn").on('click', function(){
    			var date = $(".productDate").val();
    			var selectId = $(".searchSelect").val();
    			var str = "";
    			
    			console.log(selectId);
    			
    			if(selectId != "") {
    				$.ajax({
    					url:"searhGoalList.sgm",
    					type:"POST",
    					data:{
    						date:date,
    						selectId:selectId
    					},
    					success:function(data) {
    						if(data != null) {
    							str += '<tr><th class="th"></th><th class="th">제품명</th><th class="th thWidth">합계</th><th class="th thWidth">1월</th><th class="th thWidth">2월</th><th class="th thWidth">3월</th>'+
    							'<th class="th thWidth">4월</th><th class="th thWidth">5월</th><th class="th thWidth">6월</th><th class="th thWidth">7월</th><th class="th thWidth">8월</th>'+
    							'<th class="th thWidth">9월</th><th class="th thWidth">10월</th><th class="th thWidth">11월</th><th class="th thWidth">12월</th></tr>';
    								if(data.searchGoalList != null) {
    									$.each(data.searchGoalList, function(index, PersonalGoal) {
    											str += '<tr align="center">'+'<td class="firstTd"><button class="mini ui blue basic button showModal" style="margin:7px; padding:9px; font-size:10px; font-weight:bold;">수정</button></td>'+
    											'<td class="td">'+PersonalGoal.pdtName+'</td>'+
    											'<td class="td">'+PersonalGoal.goalValue+'</td>'+
    											'<td class="td">'+PersonalGoal.one+'</td>'+
    											'<td class="td">'+PersonalGoal.two+'</td>'+
    											'<td class="td">'+PersonalGoal.three+'</td>'+
    											'<td class="td">'+PersonalGoal.four+'</td>'+
    											'<td class="td">'+PersonalGoal.five+'</td>'+
    											'<td class="td">'+PersonalGoal.six+'</td>'+
    											'<td class="td">'+PersonalGoal.seven+'</td>'+
    											'<td class="td">'+PersonalGoal.eight+'</td>'+
    											'<td class="td">'+PersonalGoal.nine+'</td>'+
    											'<td class="td">'+PersonalGoal.ten+'</td>'+
    											'<td class="td">'+PersonalGoal.eleven+'</td>'+
    											'<td class="td">'+PersonalGoal.twelve+'</td>'+
    											'<td class="td" style="display:none;">'+PersonalGoal.pdtId+'</td>'+
    											'<td class="td" style="display:none;">'+PersonalGoal.empNo+'</td></tr>';
    									});
    								}
    							$(".table").empty();
    							$(".table").append(str);
    						}
    					},
    					error:function(status) {
    						console.log(status);
    					}
    				});
    			}else {
    				alert("상품명을 선택해 주세요");
    			}
    		});
    		
    		var date = new Date();
    		var initYear = date.getFullYear();
    		
    		$('#standard_calendar').calendar({
    			  type: 'year',
    			  initialDate: initYear
    		});
    		
    		$(".totalDiv").click(function(){
    			var total = $(".total").val();
    			var num = Math.floor(total / 12);
    			$(".number").val(num);
    			$("#one").val(num);
    			$("#two").val(num);
    			$("#three").val(num);
    			$("#four").val(num);
    			$("#five").val(num);
    			$("#six").val(num);
    			$("#seven").val(num);
    			$("#eight").val(num);
    			$("#nine").val(num);
    			$("#ten").val(num);
    			$("#eleven").val(num);
    			$("#twelve").val(num);
    		});
    		
    		$("#one").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#two").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#three").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#four").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#five").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#six").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#seven").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#eight").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#nine").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#ten").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#eleven").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    		
    		$("#twelve").keyup(function(){
    			$(".total").val(Number($("#one").val())+Number($("#two").val())+Number($("#three").val())+Number($("#four").val())
    					+Number($("#five").val())+Number($("#six").val())+Number($("#seven").val())+Number($("#eight").val())+
    					Number($("#nine").val())+Number($("#ten").val())+Number($("#eleven").val())+Number($("#twelve").val()));
    		});
    	});
    	
    </script>
</body>
</html>