<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath }/resources/css/common.css">
<style>
	.refortInsertForm {
		margin-top: 20px;
	}
	
	.btnArea {
		margin: 5px;
		text-align: end;
		width: 800px;
	}
	
	.plusReportArea {
		margin-top: 40px;
		text-align: end;
		width: 800px;
	}
	
	.insertTable {
		border-top: 2px solid #27aae1;
		width: 800px;
		margin-left: auto;
		margin-right: auto;
	}
	
	.insertTable>tr {
		border-bottom: 1px solid #E1E6EB;
		width: 720px;
	}
	
	.th {
		vertical-align: middle;
		background-color: #FBFCFD;
		text-align: center;
		height: 60px;
		border-right: 1px solid #E1E6EB;
		border-bottom: 1px solid #E1E6EB;
		padding-left:3px;
		padding-right:3px;
	}
	
	.insertTableTd {
		width: 635px;
		margin-right: auto;
		margin-left: auto;
		vertical-align: middle;
		border-bottom: 1px solid #E1E6EB;
	}
	
	.reportSelect {
		width: 680px;
		margin-left: auto;
		margin-right: auto;
	}
	
	.reportCalendar {
		width: 680px;
		margin-left: 10px;
		margin-top: 20px;
	}
	
	.calenderPeriod {
		margin-left: 20px;
		margin-right: 20px;
		margin-top: auto;
		margin-bottom: auto;
	}
	
	.sortA {
		color: black;
	}
	
	.subTitle {
		display: inline-block;
		float: left;
		padding-left: 5px;
		margin-left: 10px;
		font-size: 20px;
	    color: #3D4959;
	    margin-bottom: 20px;
	    margin-top: 50px;
	}
	.ActivityCalendar {
		display:inline-block;
		height:30x;
		width:100px;
		margin-left:5px;
	}
	.perfomanceCal {
		width:400px;
	}
	.checkbox {
		width:15px;
		height:15px;
	}
	.check_th {
		margin:20px;
		width:10%;
		vertical-align:middle;	
		border-right:1px solid rgb(169, 169, 169);
		height:60px;
		border-bottom:1px solid rgb(169, 169, 169);
	}
	.client, .actDate {
		font-size:17px;
		text-align:left;
		height:30px;
		vertical-align:middle;
		padding-left:5px;
	}
	.act_tr {
		border-bottom:1px solid rgb(169, 169, 169);
	}
	.act_select_table {
		width:100%;
	}
	input[type=date] {
		autocomplete:off;
	}
	.performanceHead {
		margin-top:10px;
		margin-bottom:10px;
		margin-left:10px;
		text-align:left;
	}
	.title h1{
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;
	}
	
</style>

</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn">
			<form action="insertReport.sr" method="post" enctype="multipart/form-data">
		<div class="title" style="height:80px; width:800px;">
			<h1>영업보고관리
				<input type="button" class="ui inverted red button" style="width: 70px; height:35px; float:right; margin-left: 6px;" value="취소" onclick="location.href='selectSalesReport.sr'">
				<input type="submit" class="ui inverted primary button" style="width: 70px; height:35px; float:right;" value="작성"></h1>
		</div>
		<div class="tb_cnt">
				<div class="refortInsertForm">
					<table class="insertTable">
						<tr>
							<th align="left" class="th">보고일</th>
							<td class="insertTableTd">
								<div class="ui calendar reportSelect" id="standard_calendar">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="date" name="bizDate" class="reportSelect" value="${report.bizDate}" readonly>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th align="left" class="th" style="height: 60px !important">기간</th>
							<td class="insertTableTd">
								<div class="ui form reportCalendar">
									<div class="two fields">
										<div class="field">
											<div class="ui calendar" id="rangestart">
												<div class="ui input left icon">
													<i class="calendar icon"></i>
													<input type="date" name="bizStart" class="reportSelect" value="${report.bizStart}" readonly>
												</div>
											</div>
										</div>
										<label class="calenderPeriod">~</label>
										<div class="field">
											<div class="ui calendar" id="rangeend">
												<div class="ui input left icon">
													<i class="calendar icon"></i>
													<input type="date" name="bizEnd" class="reportSelect" value="${report.bizEnd}" readonly>
												</div>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
					<input type="hidden" name="category" value="<c:out value='${report.category}'/>">
					<div align="center" class="plusReportArea">
						<hr style="width: 800px; height: 2px; background-color: #E1E6EB; border: none;">
						<div class="performanceContent" style="margin-top: 10px;">
							<p class="subTitle">영업활동 실적</p>
							<button type="button" class="ui inverted primary button" id="perfomance_btn">추가</button>
							<hr class="hr" style="border:0.7px solid #27aae1;">
							<div class="performanceArea">
								<table class="performanceTable"></table>
							</div>
						</div>
						<textarea name="perContent" rows="5" cols="110" style="resize: none; margin-top:10px;"></textarea>
	
						<div style="margin-top: 20px;">
							<p class="subTitle">영업활동 계획</p>
							<button type="button" class="ui inverted primary button" id="planplus">추가</button>
							<hr class="hr" style="border:0.7px solid #27aae1;">
							<div class="planArea">
								<table class="planTable"></table>
							</div>
						</div>
						<textarea name="planContent" rows="5" cols="110" style="resize: none; margin-top:10px;"></textarea>
	
						<div style="margin-top: 20px;">
							<p class="subTitle">기타사항</p>
							<textarea name="content" name="ir1" id="ir1" rows="10" cols="110" style="resize: none;"></textarea>
						</div>
	
						<script>
	                            var count=1;
	                            $(function () {
	                            	if($('#comCk').prop("checked")){
	                            		$('#completeYn').val("Y");
	                            	}else{
	                            		$('#completeYn').val("N");
	                            	}
	                            	
	                            	
	                                $('#file_btn').click(function (e) {
	                                	e.preventDefault();
	                                	var str = '<tr style="height:30px;"><td><input type="file" style="font-size:15px; display:none;" name="files"><input hidden="true" class="upload_name" id="upload_name'
	                            			+ (count++) + '" readonly style="border:0;"><input style="font-size:15px; border:none;" class="NewfileName"></td><td style="text-align:right;"><button type="button" class="ui basic grey button not" onclick="remove_btn($(this))">삭제</button></td></tr>';
	                            			
	                            		$("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
	                            			
	                            		$(".enroll_th").show();
	                            			
	                            		$("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
	                                    	var cur=$(this).val();
	                                    	var arSplitUrl = cur.split('\\');
	                                    	var nArLength = arSplitUrl.length;
	                                    	var fileName = arSplitUrl[nArLength-1];
	                                            
	                                    $("#upload_name" + (count-1)).val(fileName);
	                                    $(".NewfileName").val(cur.split('\\')[2]);
	                                    }); 
	                                 }); 
	                            });
	                            
	                            function remove_btn(obj){
	                                obj.parent().parent().remove();
	                            }
	                    </script>
						<div class="attc_area" style="margin-top:40px; text-align:left;">
							<!-- 첨부자료 -->
							<h1 style="margin-top: 50px; font-size: 23px; font-weight: bold; color: #3D4959; margin-bottom: 20px;">첨부자료
								<button type="button"  style="float:right;" class="ui inverted primary button" id="file_btn">추가</button>
								<input type="file" id="file" hidden="true">
							</h1>
							<hr style="border:0.7px solid #27aae1;">
							<div class="enroll_table_area product_table_area attach_table_area">
								<table id="file_table" class="act_enroll_table" style="width:100%;">
									<colgroup>
										<col style="width: 80%;">
										<col style="width: 20%;">
									</colgroup>
									<tr>
										<th class="enroll_th" style="display:none; height:30px; text-align:center;"></th>
										<th class="enroll_th" style="display:none; height:30px; text-align:center;"></th>
									</tr>
	
								</table>
							</div>
						</div>
						
					</div>
	
				</div>
		</div>
			</form>
	</div>
	
	<!-- 영업활동 실적 모달 -->
	<div class="ui tiny modal performance">
	  <div class="header" style="background-color:#6199B9; color:white;">
	  	영업활동 실적 선택<i class="close icon" style="float:right; color:white;"></i>
	  </div>
		  <div class="ActivitySelectArea" style="margin-top:5px;">
		    <div class="ui form ActivityCalendar">
				<div class="two fields perfomanceCal">
					<div class="field" style="width:180px;">
						<div class="ui calendar" id="rangestartPer">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input class="performanceSelect bizStart" style="width:200px; padding-right:0px;" name="start" value="${report.bizStart}">
							</div>
						</div>
					</div>
					<label class="calenderPeriod" style="margin-left:10px; margin-right:10px;">~</label>
					<div class="field" style="width:180px;">
						<div class="ui calendar" id="rangeendPer">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input class="performanceSelect bizEnd" style="width:200px; padding-right:0px;" name="end" value="${report.bizEnd}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<button type="button" class="small ui blue basic button" style="float:right; display:inline-block;" onclick="searchActivity1()">검색</button>
			<br>
			<button type="button" class="ui basic button allSelect" style="margin-left:7px; margin-right:7px; display:inline-block;">전체선택</button>
			<button type="button" class="ui secondary basic button allNone" style="margin-left:7px; margin-right:7px; display:inline-block;">전체해제</button>
		  </div>
	  <div class="actions" style="witdh:90%; height:300px; padding:0px; margin-top:20px; overflow:auto;">
	  	<table id="activity_table" class="act_select_table performance"></table>
	  </div>
	  <hr>
	  <button type="button" class="small ui blue basic button pushPerfor" style="float:right; display:inline-block; margin-bottom:10px; margin-top:10px;">확인</button>
	</div>
	
	<!-- 영업활동 계획 모달 -->
	<div class="ui tiny modal plan">
	 
	  <div class="header" style="background-color:#6199B9; color:white;">
	  	영업활동 계획 선택 <i class="close icon" style="float:right; color:white;"></i>
	  </div>
	  <div class="ActivitySelectArea" style="margin-top:5px;">
	  	<div class="ui form ActivityCalendar">
			<div class="two fields perfomanceCal">
				<div class="field" style="width:180px;">
					<div class="ui calendar" id="rangestartPlan">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input class="planSelect bizStart" style="width:200px; padding-right:0px;" name="start" value="${report.bizStart}">
						</div>
					</div>
				</div>
				<label class="calenderPeriod" style="margin-left:10px; margin-right:10px;">~</label>
				<div class="field" style="width:180px;">
					<div class="ui calendar" id="rangeendPlan">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input class="planSelect bizEnd" style=" padding-right:0px;" name="end" value="${report.bizEnd}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="button" class="small ui blue basic button" style="float:right; display:inline-block;" onclick="searchActivity2()">검색</button>
		<br>
		<button type="button" class="ui basic button allSelect" style="margin-left:7px; margin-right:7px; display:inline-block;">전체선택</button>
		<button type="button" class="ui secondary basic button allNone" style="margin-left:7px; margin-right:7px; display:inline-block;">전체해제</button>
	  </div>
	  <div class="actions" style="witdh:90%; height:300px; padding:0px; margin-top:10px; overflow:auto;">
	  	<table id="activity_table plan" class="act_select_table plan"></table>
	  </div>
	  <hr>
	  <button type="button" class="small ui blue basic button pushPlan" style="float:right; display:inline-block; margin-bottom:10px; margin-top:10px;">확인</button>
	</div>
	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(3).addClass("on");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(5).addClass("on");
			
			$("#perfomance_btn").on('click', function(){
				$(".tiny.modal.performance").modal({
					onShow: function(value) {
						$('#rangestartPer').calendar({
			 				type: 'date',
			  				endCalendar: $('#rangeendPer'),
			  				formatter : {
				                   date : function(date, settings) {
				                      var day = date.getDate();
				                      var month = date.getMonth() + 1;
				                      var year = date.getFullYear();
				                      
				                      if(month < 10) {
				                    	  if(day < 10) {
				                    		  return year + '-' + '0' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + '0' + month + '-' + day;
				                    	  }
				                     	 
				                      }else {
				                    	  if(day < 10) {
				                    		  return year + '-' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + month + '-' + day;
				                    	  }
				                      }
				                   }
				               	}
						});
						$('#rangeendPer').calendar({
			  				type: 'date',
			  				startCalendar: $('#rangestartPer'),
			  				formatter : {
				                   date : function(date, settings) {
				                      var day = date.getDate();
				                      var month = date.getMonth() + 1;
				                      var year = date.getFullYear();
				                      
				                      if(month < 10) {
				                    	  if(day < 10) {
				                    		  return year + '-' + '0' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + '0' + month + '-' + day;
				                    	  }
				                     	 
				                      }else {
				                    	  if(day < 10) {
				                    		  return year + '-' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + month + '-' + day;
				                    	  }
				                      }
				                   }
				               	}
						})
					}}).modal('show');
			});
			$(".close.icon").on('click', function(){
				$(".modal").modal('hide');
			});
			
			$("#planplus").on('click', function(){
				$(".tiny.modal.plan").modal({
					onShow:function(value){
						$('#rangestartPlan').calendar({
			 				type: 'date',
			  				endCalendar: $('#rangeendPer'),
			  				formatter : {
				                   date : function(date, settings) {
				                      var day = date.getDate();
				                      var month = date.getMonth() + 1;
				                      var year = date.getFullYear();
				                      
				                      if(month < 10) {
				                    	  if(day < 10) {
				                    		  return year + '-' + '0' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + '0' + month + '-' + day;
				                    	  }
				                     	 
				                      }else {
				                    	  if(day < 10) {
				                    		  return year + '-' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + month + '-' + day;
				                    	  }
				                      }
				                   }
				               	}
						});
						$('#rangeendPlan').calendar({
			  				type: 'date',
			  				startCalendar: $('#rangestartPer'),
			  				formatter : {
				                   date : function(date, settings) {
				                      var day = date.getDate();
				                      var month = date.getMonth() + 1;
				                      var year = date.getFullYear();
				                      
				                      if(month < 10) {
				                    	  if(day < 10) {
				                    		  return year + '-' + '0' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + '0' + month + '-' + day;
				                    	  }
				                     	 
				                      }else {
				                    	  if(day < 10) {
				                    		  return year + '-' + month + '-' + '0' + day;
				                    	  }else {
				                    		  return year + '-' + month + '-' + day;
				                    	  }
				                      }
				                   }
				               	}
						})
					}}).modal('show');
			});
			
			$(".allSelect").on('click', function(){
				$("input[name=checkVal]").prop("checked", true);
			});
			
			$(".allNone").on('click', function(){
				$("input[name=checkVal]").prop("checked", false);
			});
			
			$(".pushPerfor").on('click', function(){
				
				var table = $(".performanceTable");
				var activityId = null;
				
				$(".checkbox").each(function() {
					if($(this).prop("checked") == true) {
						if(activityId == null) {
							activityId += Number($(this).parent().parent().find('.activityId').text());
						}else {
							activityId += ", " + Number($(this).parent().parent().find('.activityId').text());
						}
						
						$.ajax({
							url:"inselectPerfor.sr",
							type:"POST",
							dataType:"json",
							data: {
								activityIdArr:activityId
							},
							success:function(data) {
								var list = data.selectPerfor;
								var performanceTable = $(".performanceTable");
								var performanceArea = $(".performanceArea");
								var tr;
								var head;
								var category;
								var purpose;
								var planCon;
								var activityCon;
								var delBtn;
								
								$.each(list, function(i) {
									
									if(i != 1) {
									if(list[i].category == 1) {
										category = '전화';
									}else if(list[i].category == 2) {
										category = '메일';
									}else if(list[i].category == 3) {
										category = '방문';
									}else if(list[i].category == 4) {
										category = '기타';
									}
									
									if(list[i].purpose == 1) {
										purpose = '인사';
									}else if(list[i].purpose == 2) {
										purpose = '제품소개';
									}else if(list[i].purpose == 3) {
										purpose = '데모시연';
									}else if(list[i].purpose == 4) {
										purpose = '정보수집';
									}else if(list[i].purpose == 5) {
										purpose = '제안';
									}else if(list[i].purpose == 6) {
										purpose = '업무협의';
									}else if(list[i].purpose == 7) {
										purpose = '협상';
									}else if(list[i].purpose == 8) {
										purpose = '계약';
									}else if(list[i].purpose == 9) {
										purpose = '기타';
									}
									
									head = '<td colspan="2"><h5 class="performanceHead" style="margin-top:10px; overflow:hidden;">'+list[i].companyName+' '+list[i].clientName + '</h5></td>';
									delBtn = '<td float:right><button type="button" class="mini ui red basic button perforDel_btn" onclick="delPerforTable($(this))">삭제</button><td>';
									
									tr += '<tr>' +
										'<th class="th" style="width:100px; height:30px; border-top:1px solid #E1E6EB;">시간 </th>' +
										'<td class="insertTableTd" style="text-align:left; padding-left:10px; border-top:1px solid #E1E6EB;"> '+list[i].aDate+'  '+list[i].aStart+' ~ '+list[i].aEnd+' </td>'+
									'</tr>'+
									'<tr>'+
										'<th class="th" style="width:100px; height:30px;">활동분류 / 목적</th>'+
										'<td class="insertTableTd" style="text-align:left; padding-left:10px;">'+category+' / '+purpose+'</td>'+
										'<td class="perforActivityId" style="display:none;"><input name="perforActivityId" type="hidden" value="'+ list[i].activityId+'"><td>'+
									'</tr>';
									
									if(list[i].planCon != null) {
										tr += '<tr>'+
													'<th class="th" style="width:100px; height:30px;">계획내용</th>'+
													'<td class="insertTableTd" style="text-align:left; padding-left:10px;">'+list[i].planCon+'</td>'
												'</tr>';
													
									}
										
									if(list[i].activityCon != null) {
										tr += '<tr>'+
													'<th class="th" style="width:100px; height:30px;">활동내용</th>'+
													'<td class="insertTableTd" style="text-align:left; padding-left:10px;">'+list[i].activityCon+'</td>'
												'</tr>';
										
									}
									performanceArea.append($("<table class='performanceTable'>").append($("<tr style='padding-top:10px'>").append(head).append(delBtn)).append(tr));
									
								}
									
								});
								$(".tiny.modal.performance").modal('hide');
							},
							error:function(status){
								console.log(status);
							}
						});
					}
				});
			});
			
			$(".pushPlan").on('click', function(){
				
				var table = $(".planTable");
				var ActivityId = null;
				
				$(".checkbox").each(function() {
					if($(this).prop("checked") == true) {
						if(ActivityId == null) {
							ActivityId += Number($(this).parent().parent().find('.activityId').text());
						}else {
							ActivityId += ", " + Number($(this).parent().parent().find('.activityId').text());
						}
						
						$.ajax({
							url:"inselectPlan.sr",
							type:"POST",
							dataType:"json",
							data: {
								ActivityIdArr:ActivityId
							},
							success:function(data) {
								var list = data.selectPlan;
								var planTable = $(".planTable");
								var planArea = $(".planArea");
								var tr;
								var head;
								var category;
								var purpose;
								var planCon;
								var activityCon;
								var delBtn;
								
								$.each(list, function(i) {
									if(list[i].category == 1) {
										category = '전화';
									}else if(list[i].category == 2) {
										category = '메일';
									}else if(list[i].category == 3) {
										category = '방문';
									}else if(list[i].category == 4) {
										category = '기타';
									}
									
									if(list[i].purpose == 1) {
										purpose = '인사';
									}else if(list[i].purpose == 2) {
										purpose = '제품소개';
									}else if(list[i].purpose == 3) {
										purpose = '데모시연';
									}else if(list[i].purpose == 4) {
										purpose = '정보수집';
									}else if(list[i].purpose == 5) {
										purpose = '제안';
									}else if(list[i].purpose == 6) {
										purpose = '업무협의';
									}else if(list[i].purpose == 7) {
										purpose = '협상';
									}else if(list[i].purpose == 8) {
										purpose = '계약';
									}else if(list[i].purpose == 9) {
										purpose = '기타';
									}
									
									head = '<td colspan="2"><h5 class="planHead" style="margin-top:10px; text-align:left;">'+list[i].companyName+' '+list[i].clientName + '</h5></td>';
									delBtn = '<td float:right><button type="button" class="mini ui red basic button planDel_btn" onclick="delPlanTable($(this))">삭제</button><td>';
									
									tr += '<tr>' +
										'<th class="th" style="height:30px; border-top:1px solid #E1E6EB;">시간 </th>' +
										'<td class="insertTableTd" style="text-align:left; padding-left:10px; border-top:1px solid #E1E6EB;"> '+list[i].aDate+'  '+list[i].aStart+' ~ '+list[i].aEnd+' </td>'+
									'</tr>'+
									'<tr>'+
										'<th class="th" style="height:30px;">활동분류 / 목적</th>'+
										'<td class="insertTableTd" style="text-align:left; padding-left:10px;">'+category+' / '+purpose+'</td>'+
										'<td class="planActivityId"  style="display:none;"><input name="planActivityId" type="hidden" value="'+ list[i].activityId +'"><td>'
									'</tr>';
									
									if(list[i].planCon != null) {
										tr += '<tr>'+
													'<th class="th" style="height:30px;">계획내용</th>'+
													'<td class="insertTableTd" style="text-align:left; padding-left:10px;">'+list[i].planCon+'</td>'
												'</tr>';
													
									}
										
									if(list[i].activityCon != null) {
										tr += '<tr>'+
													'<th class="th" style="height:30px;">활동내용</th>'+
													'<td class="insertTableTd" style="text-align:left; padding-left:10px;">'+list[i].activityCon+'</td>'
												'</tr>';
										
									}
									planArea.append($("<table class='planTable'>").append($("<tr style='padding-top:10px'>").append(head).append(delBtn)).append(tr));
									
								});
								
								$(".tiny.modal.plan").modal('hide');
							},
							error:function(status){
								console.log(status);
							}
						});
					}
				});
			});
		});
		
		function searchActivity1() {
			
			var start = $(".performanceSelect.bizStart").val();
			var end = $(".performanceSelect.bizEnd").val();
			var empNo = $("sessionScope.loginEmp.empNo").val(); 
			
			$.ajax({
				url:"searchActivityPerformance.sr",
				type:"POST",
				dataType: "json",
				data:{
					start : start,
					end : end,
					empNo : empNo
				},
				success:function(data) {
					var list = data.searchList;
					var table = $(".act_select_table.performance");
					var tr;
					var category;
					$.each(list, function(i) {
						if(list[i].category == 1) {
							category= '전화';
						}else if(list[i].category == 2) {
							category ='메일';
						}else if(list[i].category == 3) {
							category = '방문';
						}else if(list[i].category == 4){
							category = '기타';
						}
						
						tr += '<tr>' +
							'<th rowspan="2" align="center" class="check_th">'+
								'<input type="checkbox" class="checkbox" name="checkVal" style="vertical-align:middle;">'
						  + '</th>' +
						  	'<td style="display:none;" class="activityId">'+ list[i].activityId + '</td>'+
						  	'<td class="actDate" id="actDate">'+list[i].aDate+'  '+list[i].aStart+' ~ '+list[i].aEnd+'</td>'+
						'</tr>'+	
						'<tr class="act_tr">'+
							'<td class="client">'+list[i].companyName+'  '+list[i].clientName+' / '+category+'</td>'+
						'</tr>';
					});
					
					table.append(tr);
				},
				error:function(status) {
					console.log(status);
				}
			});
		};
		
		function searchActivity2() {
			
			var start = $(".planSelect.bizStart").val();
			var end = $(".planSelect.bizEnd").val();
			var empNo = $("sessionScope.loginEmp.empNo").val(); 
			
			$.ajax({
				url:"searchActivityPlan.sr",
				type:"POST",
				dataType: "json",
				data:{
					start : start,
					end : end,
					empNo : empNo
				},
				success:function(data) {
					var list = data.searchList;
					var table = $(".act_select_table.plan");
					var tr;
					var category;
					
					$.each(list, function(i) {
						if(list[i].category == 1) {
							category= '전화';
						}else if(list[i].category == 2) {
							category ='메일';
						}else if(list[i].category == 3) {
							category = '방문';
						}else if(list[i].category == 4){
							category = '기타';
						}
						
						tr += '<tr>' +
							'<th rowspan="2" align="center" class="check_th">'+
								'<input type="checkbox" class="checkbox" name="checkVal" style="vertical-align:middle;">'
						  + '</th>' +
						  	'<td style="display:none;" class="activityId">'+ list[i].activityId + '</td>'+
						  	'<td class="actDate" id="actDate">'+list[i].aDate+'  '+list[i].aStart+' ~ '+list[i].aEnd+'</td>'+
						'</tr>'+	
						'<tr class="act_tr">'+
							'<td class="client">'+list[i].companyName+'  '+list[i].clientName+' / '+category+'</td>'+
						'</tr>';
					});
					
					table.append(tr);
				},
				error:function(status) {
					console.log(status);
				}
			});
		};
		
		function delPerforTable(obj) {
			obj.parent().parent().parent().remove();
		}
		
		function delPlanTable(obj) {
			obj.parent().parent().parent().remove();
		}
	</script>
</body>
</html>