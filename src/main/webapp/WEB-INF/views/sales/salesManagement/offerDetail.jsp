<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 제안등록</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/menubar.jsp" />
</head>
<body>
	<c:set var="list" value="${ offerlist }" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<div class="header_div">
				<div>
					<h1>제안세부사항</h1>
					<button class="ui inverted red button float" id="cancelBtn">취소</button>
					<button class="ui inverted primary button float" id="modifyBtn">수정</button>
					<h3><c:out value="${list[0].ppsName}" /></h3>
				</div>
			</div>
			<div class="content_div">
				<table id="table2">
					<tr>
						<td class="td">고객사</td>
						<td><a class="Detail_a" href="selOne.co?ComId=${list[0].company.comId}"><c:out
									value="${list[0].company.comName}" /></a></td>
					</tr>
					<tr>
						<td class="td">고객</td>
						<td><a class="Detail_a"><c:out
									value="${list[0].client.clientName}" /></a></td>
					</tr>
					<tr>
						<td class="td">영업기회</td>
						<td><a class="Detail_a" href="selectOne.so?oppId=${list[0].oppId}"><c:out value="${list[0].ppsName}" /></a></td>
					</tr>
					<tr>
						<td class="td">내용</td>
						<td><a class="Detail_a2"><c:out
									value="${list[0].content}" /></a></td>
					</tr>
					<tr>
						<td class="td">요청일</td>
						<td><a class="Detail_a2"><c:out
									value="${list[0].requestDate}" /></a></td>
					</tr>
					<tr>
						<td class="td">제출일</td>
						<td><a class="Detail_a2"><c:out
									value="${list[0].submitDate}" /></a></td>
					</tr>
					<tr>
						<td class="td">담당자</td>
						<td><a class="Detail_a2"><c:out
									value="${list[0].employee.empName}" /></a></td>
					</tr>
					<tr>
						<td class="td">비고</td>
						<td><a class="Detail_a2"><c:out value="${list[0].remark}" /></a></td>
					</tr>
				</table>
				<c:set var="reportList" value="${reportList}" />
				<div class="bottom black">
					<h1 id="propose_report">
					제안 요청서 <input type="file" id="propose_Request_file" hidden="true">
					</h1>
				</div>
				<div>
					<table class="download_Table" id="requestTable">
						<c:forEach items="${requestList}" varStatus="status">
							<tr>
								<td><c:out value="${requestList[status.index].originName}" />
									<input type="hidden"
									value="${requestList[status.index].changeName}" /> <input
									type="hidden" value="${requestList[status.index].filePath }" />
								</td>
								<td><button class="ui inverted primary button float srch downBtn">다운로드</button></td>
							</tr>
						</c:forEach>
					</table>
				</div>

				<c:set var="requestList" value="${requestList}" />
				<div class="bottom black">
					<h1 id="propose_report">제안서</h1>
				</div>
				<div>
					<table class="download_Table" id="reportTable">

						<c:forEach items="${reportList}" varStatus="status">
							<tr>
								<td><c:out value="${reportList[status.index].originName}" />
									<input type="hidden"
									value="${reportList[status.index].changeName}" /> <input
									type="hidden" value="${reportList[status.index].filePath }" />
								</td>
								<td><button class="ui inverted primary button float srch">다운로드</button></td>
							</tr>
						</c:forEach>


					</table>
				</div>

			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
		$(function() {

			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(0)
					.addClass("on");

			//제안서 다운할떄 필요한 스크립트
			$('#reportTable td button')
					.click(
							function(e) {
								var originName = $(this).parents('tr')
										.children()[0].innerText;
								var fileName = $(this).parents('tr').children()
										.eq(0).children().eq(0).val();
								var filePath = $(this).parents('tr').children()
										.eq(0).children().eq(1).val();
								var extension = originName.split(".");
								var encodefilePath = encodeURI(filePath);
								fileName = fileName + '.' + extension[1];
								location.href = "download.do?fileName="
										+ fileName + "&filePath="
										+ encodefilePath + "&originName="
										+ originName;
							})
			$('#requestTable td button')
					.click(
							function(e) {
								var originName = $(this).parents('tr')
										.children()[0].innerText;
								var fileName = $(this).parents('tr').children()
										.eq(0).children().eq(0).val();
								var filePath = $(this).parents('tr').children()
										.eq(0).children().eq(1).val();
								var extension = originName.split(".");
								var encodefilePath = encodeURI(filePath);
								fileName = fileName + '.' + extension[1];
								location.href = "download.do?fileName="
										+ fileName + "&filePath="
										+ encodefilePath + "&originName="
										+ originName;
							})
			$('#modifyBtn').on('click', function() {
				location.href = "offerModify.of?ppsId=" +${list[0].ppsId};
			})
			
			$('#cancelBtn').click(function(){
				history.back();
			})

		});
	</script>
</body>
</html>