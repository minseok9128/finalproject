<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 제안등록</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/menubar.jsp" />
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
</head>
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title"></div>
			<c:set var="ppsId" value="${ppsId}"/>
			<c:if test="${ppsId == 0}">
				<c:set var="move" value="insertOfferEnrollment.of"/>
			</c:if>
			<c:if test="${ppsId != 0 }">
				<c:set var="move" value="modifyOfferEnrollment.of"/>
			</c:if>
				<form action="${move}" method="post" enctype="multipart/form-data">			
				<div class="header_div">
					<c:if test="${ppsId == 0 }">
						<c:set var="list" value="0"/>
					</c:if>
					<c:if test="${ppsId != 0 }">
						<c:set var="list" value="${list}"/>
					</c:if>
					<div>
					<c:if test="${ppsId == 0}">
						<h1>제안등록</h1>
					</c:if>
					<c:if test="${ppsId != 0}">
						<h1>제안수정</h1>
					</c:if>
							<button id="realBtn" hidden>이것은 입력하지 않은 file input 태그를 지우고 controller에 보내는 버튼</button>
							<button class="ui inverted red button" id="cancelEnrollBtn">취소</button>
						<c:if test="${ppsId == 0}">
							<button class="ui inverted primary button" id="submitBtn">작성</button>
						</c:if>
						<c:if test="${ppsId != 0}">
							<button class="ui inverted primary button" id="modifyBtn">수정</button>
						</c:if>
					</div>
				</div>
				<div id="content_Enrollment_div">
					<table id="table2">
						<tr>
							<td class="td">제안명<b class="point">*</b></td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="offer_Name" name="ppsName"class="common_Text1">
									<input type="hidden" value="0" name="ppsId">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" id="offer_Name" name="ppsName"class="common_Text1" value="${list[0].ppsName}">
									<input type="hidden" value="${ppsId}" name="ppsId">									
								</c:if>							
							</td>
						</tr>
						<tr>
							<td class="td">고객사</td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="company" class="common_Text2"readonly> <input type="hidden" id="company_hidden"value="0" name="companyId">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" id="company" class="common_Text2"readonly value="${list[0].company.comName}"> <input type="hidden" id="company_hidden"value="${list[0].company.comId}" name="companyId">
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="company_Modal">검색</button>
							</td>
						</tr>
						<tr>
							<td class="td">고객<b class="point">*</b></td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="client" class="common_Text2"><input type="hidden" id="client_hidden" value="0"name="clientId">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" id="client" class="common_Text2" value="${list[0].client.clientName}"><input type="hidden" id="client_hidden" value="${list[0].client.clientId}"name="clientId">
								</c:if>							
								<button class="ui grey basic button srch searchBtn" id="client_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">영업기회</td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" id="opportunity" class="common_Text2">
									<input type="hidden" id="opportunity_hidden" value="0" name="oppId">
									<input type="hidden" value="0" id="processId" name="processId"/>
								</c:if>
								<c:if test="${ppsId != 0}">
									<c:set var="pssName" value="${fn:split(list[0].ppsName,' ')[0]}"/>
									<input type="text" id="opportunity" class="common_Text2" value="${pssName}">
									<input type="hidden" id="opportunity_hidden" value="${list[0].oppId}" name="oppId">
									<input type="hidden" value="0" id="processId" name="processId" value="0"/>
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="opportunity_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">내용</td>
							<td>
								<c:if test="${ppsId == 0}">
									<textarea class="contents" name="content"></textarea>
								</c:if>
								<c:if test="${ppsId != 0 }">
									<textarea class="contents" name="content"><c:out value="${list[0].content}"/></textarea>
								</c:if>
							</td>
						</tr>
						<tr>
							<td class="td">요청일</td>
							<td>
								<div class="field">
									<div class="ui calendar cf_left" id="standard_calendar">
										<div class="ui input right icon cf_left">
											<i class="calendar icon"></i> 
											<c:if test="${ppsId == 0}">
												<input type="text" readonly name="reday">
											</c:if>
											<c:if test="${ppsId != 0}">
													<input type="text" readonly name="reday" value="${list[0].requestDate }">
											</c:if>						
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td">제출일<b class="point">*</b></td>
							<td>
								<div class="field">
									<div class="ui calendar cf_left" id="standard_calendar2">
										<div class="ui input right icon cf_left" id="a">
											<i class="calendar icon"></i> 
											<c:if test="${ppsId == 0}">
												<input type="text" readonly name="suday">
											</c:if>
											<c:if test="${ppsId != 0 }">
												<input type="text" readonly name="suday" value="${list[0].submitDate}">
											</c:if>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td">담당자<b class="point">*</b></td>
							<td>
								<c:if test="${ppsId == 0}">
									<input type="text" class="common_Text2" id="managerText" readonly value="${ sessionScope.loginEmp.empName }"> 
									<input type="hidden" id="manager_hidden" value="${ sessionScope.loginEmp.empNo }" name="empNo">
								</c:if>
								<c:if test="${ppsId != 0}">
									<input type="text" class="common_Text2" id="managerText" readonly value="${list[0].employee.empName }"> 
									<input type="hidden" id="manager_hidden" value="${list[0].empNo}" name="empNo">
								</c:if>						
								<c:if test="${ sessionScope.loginEmp.empName == '관리자'}">
									<button class="ui grey basic button srch searchBtn" id="manager">검색</button>
								</c:if>
								</td>
						</tr>
						<tr>
							<td class="td">비고</td>
							<td>
								<c:if test="${ppsId == 0 }">
									<textarea class="contents" name="remark"></textarea>
								</c:if>
								<c:if test="${ppsId != 0 }">
									<textarea class="contents" name="remark"><c:out value="${list[0].remark}"/></textarea>
								</c:if>						
							</td>
						</tr>
					</table>

					<div>
						<div class="fileUploadDiv" id="fileUp1">
							<c:set var="request" value="${requestList}"/>
							<c:set var="report" value="${reportList}"/>
							<div>
								<h1>제안요청서</h1>
								<button class="ui inverted primary button float"
									id="proposeRequestBtn">요청서등록</button>
								<br>
								<hr>
							</div>
							<ul>
							<c:if test="${ppsId != 0 }">
								<c:forEach items="${request}" varStatus="status">
									<li>
										${request[status.index].originName} <i class="trash alternate outline icon right"></i>
										<input type="hidden" value="${request[status.index].fileId}">
										<input type="hidden" value="${request[status.index].category}">
									</li>
								</c:forEach>
							</c:if>
							</ul>
						</div>
						<div class="fileUploadDiv" id="fileUp2">
							<div>
								<h1>제안서</h1>
								<button class="ui inverted primary button float" id="proposereportBtn">제안서등록</button>
								<br>
								<hr>
							</div>
								<ul>
								<c:if test="${pssId != 0}">
									<c:forEach items="${report}" varStatus="status">
									<li>
										${report[status.index].originName} <i class="trash alternate outline icon right"></i>
										<input type="hidden" value="${report[status.index].fileId}">
										<input type="hidden" value="${report[status.index].category}">
									</li>
								</c:forEach>
								</c:if>
								</ul>
						</div>
					</div>
				</div>
			</form>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
		var a = 0;
		var b = 0;
		$(function() {
			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");
			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(0)
					.addClass("on");
		});

		$(document).click(function(e) {
							var btnName = (e.target.id);
							switch (btnName) {
							case 'resetBtn':
								location.reload();
								return false;
								break;
							case 'submitBtn':
								var sumbitboolean = false;
								var offerName = $('#offer_Name')[0].value;
								var client = $('#client')[0].value;
								var suday = $('#standard_calendar2 input')[0].value
								var employee = $('#managerText')[0].value
								if (offerName && client && suday && employee != '') {
									$(function() {
										var filelength = $('#fileUp1').children('input').length;
										var file = $('#fileUp1').children('input');
										console.log($('#fileUp1').children('input').length);
										for(var i=0; i<filelength; i++) {
											if(file[i].files.length == 0){
												file[i].remove();
											}
										}
									})
									$(function() {
										var filelength = $('#fileUp2').children('input').length;
										var file = $('#fileUp2').children('input');
										console.log($('#fileUp2').children('input').length);
										for(var i=0; i<filelength; i++) {
											if(file[i].files.length == 0){
												file[i].remove();
											}
										}
									$('#realBtn').click();
								})
								} else {
									alert("[필수]정보를 입력해주세요!");
								}
								return false;
								break;
							case 'proposeRequestBtn':
								$(function() {
									var fileSelectEle = document.getElementById('fileinput');
									var ppsId = ${ppsId};
									var fileName = $("#requestFile" + a + "");
									$('#fileUp1').append('<input type="file" id="requestFile'+a+'" name="requestFile" value="0"hidden>');
									$("#requestFile" + a + "").click();
									var liFilelength = $('#fileUp1').children('ul').children().length;
									$("#requestFile" + a + "").change(
													function() {
														var sameFileName = true;
														var cur = $(this).val();
														var arSplitUrl = cur.split('\\');
														var nArLength = arSplitUrl.length;
														var fileName = arSplitUrl[nArLength - 1];
														for(var i=0; i<liFilelength; i++) {
															if($('#fileUp1').children('ul').children()[i].innerText == fileName){
																sameFileName = false;
															}
														}
														if(sameFileName){
																$('#fileUp1 ul').append("<li>"+ fileName+ "<i class='trash alternate outline icon right'></i><input type='hidden' value='0'></li>");
														} else {
															alert("동일 이름의 파일이 있습니다.");
															$(this).remove();
														}
													})
													a++;
								})
												return false;
												break;
							case 'proposereportBtn':
								$(function() {
									var fileSelectEle = document.getElementById('fileinput');
									var ppsId = ${ppsId};
									var fileName = $("#reportFile" + b + "");
									$('#fileUp2').append('<input type="file" id="reportFile'+b+'" name="reportFile" value="0"hidden>');
									$("#reportFile" + b + "").click();
									var liFilelength = $('#fileUp2').children('ul').children().length;
									$("#reportFile" + b + "").change(
													function() {
														var sameFileName = true;
														var cur = $(this).val();
														var arSplitUrl = cur.split('\\');
														var nArLength = arSplitUrl.length;
														var fileName = arSplitUrl[nArLength - 1];
														for(var i=0; i<liFilelength; i++) {
															if($('#fileUp2').children('ul').children()[i].innerText == fileName){
																sameFileName = false;
															}
														}
														console.log(sameFileName);
														if(sameFileName){
																$('#fileUp2 ul').append("<li>"+ fileName+ "<i class='trash alternate outline icon right'></i><input type='hidden' value='0'></li>");
														} else {
															alert("동일 이름의 파일이 있습니다.");
															$(this).remove();
														}
													})
													b++;
								})
												return false;
												break;
							case 'cancelEnrollBtn':
								 history.back();
								return false;
								break;
							case 'modifyBtn':
								$(function() {
									var filelength = $('#fileUp1').children('input').length;
									var file = $('#fileUp1').children('input');
									console.log($('#fileUp1').children('input').length);
									for(var i=0; i<filelength; i++) {
										if(file[i].files.length == 0){
											file[i].remove();
										}
									}
									var filelength = $('#fileUp2').children('input').length;
									var file = $('#fileUp2').children('input');
									console.log($('#fileUp2').children('input').length);
									for(var i=0; i<filelength; i++) {
										if(file[i].files.length == 0){
											file[i].remove();
										}
									}
									 $('#realBtn').click();
								})
								return false;
								break;
							}//스위치문 끝
						})
		$(function() {
			//모달 뛰우는 스크립트

			//요청일, 제안일 켈린더 뜨는 팝업
			$('#standard_calendar').calendar({
				monthFirst : false,
				type : 'date',
				formatter : {
					date : function(date, settings) {
						var day = date.getDate();
						var month = date.getMonth() + 1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				}
			})
			$('#standard_calendar2').calendar({
				monthFirst : false,
				type : 'date',
				formatter : {
					date : function(date, settings) {
						var day = date.getDate();
						var month = date.getMonth() + 1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				}
			});
			//버튼 마우스 커서 변경
			$('button').mouseover(function() {
				$('button').css({
					"cursor" : "pointer"
				});
			})
		})
			/* $('.trash').click(function(){
				var fileId = $(this).parent().children().eq(1).val();
				var ppsId = ${ppsId};
				var category = $(this).parent().children().eq(2).val();
				var fileli = $(this).parent();
				var result = confirm("정말 삭제하시겠습니까?");
				if(result == true){
					console.log(result)
					//deletFile(fileId, ppsId, category,fileli);
					//fileli.remove();
				}
			}); */
			
			$(document).on('click', '.trash',function(e){
					var result = confirm("정말 삭제하시겠습니까?");
					if(result == true){
						var fileId = e.target.parentNode.children[1].value;
						console.log(fileId);
						deletFile(fileId);
						e.target.parentNode.remove();
					}
			})
		function deletFile(fileId) {
				var list = [fileId];
				   $.ajax({
					url:"deletAttachment.of",
					traditional : true,
					data:{list : list},
					type:"post",
					success:function(data) {
							/* var $fileUp1 = $('#fileUp1 ul');
							var $fileUp2 = $('#fileUp2 ul');
							 $fileUp1.html('');
							 $fileUp2.html(''); */
						/* for(var a=0; a<data.length; a++) {
							if(data[a].category == 11){
								$fileUp1.append('<li>'+
										data[a].originName+
										'<i class="trash alternate outline icon right"></i>'+
										'<input type="hidden" value="'+data[a].fileId+'">'+
										'<input type="hidden" value="'+data[a].category+'">'
										+'</li>');
							} else {
								$fileUp2.append('<li>'+
										data[a].originName+
										'<i class="trash alternate outline icon right"></i>'+
										'<input type="hidden" value="'+data[a].fileId+'">'+
										'<input type="hidden" value="'+data[a].category+'">'
										+'</li>');
							}
						} */
						/* $('.trash').click(function(){
							var fileId = $(this).parent().children().eq(1).val();
							var ppsId = ${ppsId};
							var category = $(this).parent().children().eq(2).val();

							var result = confirm("정말 삭제하시겠습니까?");
							if(result){
								deletFile(fileId, ppsId, category);
							}
						}); */
					},
					error:function(status) {
					}
				})
			}
	</script>
</body>
</html>