<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
</head> 
	<jsp:include page="../../common/menubar.jsp" />
	<jsp:include page="successPage.jsp" />
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div class="header_div">
				<div>
					<h1>제안</h1> 
					<button id="offer_submit" class="ui inverted primary button">등록</button>
				</div>
				<div class="search">
				<div class="one">
					<input type="text" id="titleName" placeholder="견적/고객/고객사">
				</div>
				<div class="secondSearchDiv">
					<div class="select half">
						<select>
							<option>영업부</option>
						</select>
					</div>
					<div class="half input">
						<input type="text" id="department">
					</div>
				</div>
						<button id="searchBtn" class="ui inverted primary button float">검색</button>
			</div>
			</div>
			<div id="content_div">
			<c:set var="list" value="${list}"/>
			<c:set var="listCount" value="${listCount}"/>
				<div>
					<button id="filter_Btn">필터</button>
					<P><c:out value="${listCount}"/>건</P>
					<p id="filter3" class="filters">가나다</p>
					<p id="filter2" class="filters">고객사 |</p>
					<p id="filter1" class="filters" style="font-weight: bold;">제출일▼ |</p>
				</div>
					<ul>
						<c:forEach var="i" items="${list}" varStatus="status">
							<li>
								<div class="resultContents">
								<p><c:out value="${ list[status.index].ppsName }"/></p>
								<input type="hidden" value="${list[status.index].ppsId}">
								<input type="hidden" class="scrolling" value="${status.index}">
								<p>
									<c:out value="${ list[status.index].company.comName }"/> / <input type="hidden" value="${list[status.index].company.comId}">
									<c:out value="${ list[status.index].client.clientName }"></c:out>
									<input type="hidden" value="${list[status.index].client.clientId}">
								</p>
								<p>
									<c:out value="${ list[status.index].submitDate }"/>
								</p>
								</div>
							</li>
						</c:forEach>
				   </ul>	
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	 <script>
			$(function() {
				
				var aa = '${aa}';
				var msg = '${msg}';
				
				if(aa == 2){
					alert(msg);
				}
				
				// 열리지 않는 메뉴
				$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
						"open");

				// 열리는 메뉴
				$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
				$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(0)
						.addClass("on");
			});
			$(function() {
				$('#offer_submit').click(function() {
					location.href = "showOfferEnrollment.sm";
				})
				$('.resultContents').click(function() {
				})
			})
			$(function() {
				var msg = '${msg}';
				if(msg != ''){
					alert(msg);
				}
				$('#enrollment_estimate_Btn').click(function() {
					location.href = "showEstimateEnrollment.sm";
				})
				
			})
			$(document).on('click','.resultContents p',function(e){
				var proposeId = e.target.parentNode.children[1].value;
				
				location.href="offerDetail.of?proposeId="+proposeId;
			})
			$(document).on('mouseover','.resultContents', function(){
		$(this).css({"border":"1px solid #6199B9"})
		$(this).children().css({"color":"#6199B9"})
		$(this).css({'cursor':'pointer'})
	})
	$(document).on('mouseout','.resultContents', function(){
		$(this).css({"border":"1px solid #DEE2E8"})
		$(this).children().css({"color":"#3D4959"})
	})
	
		var lastScrollTop = 0;
			$(function(){
				var memberId = ${sessionScope.loginEmp.empNo};
				$(window).scroll(function(){
					var currentScrollTop = $(window).scrollTop();
					
					if(currentScrollTop - lastScrollTop > 0){
							
						if($(window).scrollTop() >= ($(document).height() - $(window).height())){
							var lastCno = $(".scrolling:last").val();
							var $ul = $('#content_div ul');
							$.ajax({
								type : 'post',
								url : 'offerMainscroll.of',
								data : {
									lastCno:lastCno,
									memberId:memberId
								},
								success : function(data) {
									console.log(data);
									for(var i=0; i<data.length; i++) {
										$ul.append('<li>'+
												'<div class="resultContents">'+
												'<p>'+data[i].ppsName+'</p>'+
												'<input type="hidden" value="'+data[i].ppsId+'">'+
												'<input type="hidden" class= "scrolling" value="'+(Number(lastCno)+(i+1))+'">'+
												'<p>'+
												''+data[i].company.comName+'<input type="hidden" value="'+data[i].company.comId+'">'+
												' / '+data[i].client.clientName+'<input type="hidden" value="'+data[i].client.clientId+'">'+
												'<p>'+day(data[i].submitDate)+'</p>'+
												'</p>'+
												'</div>'+
												'</li>')
									}
									
								}
							})
							
						}
					}
				})
			})
	function day(day) {
		if(day != null){
			if(day.indexOf(',') != -1){
				var day = day.split(',');
				//월과 달
				var md = day[0].split('월');
				var aa = md[1].split(' ');
				var year = day[1];

				return year+'-'+md[0]+'-'+aa[1];
			}
		} else {
			return "";
		}
	}
	var count = 0;
	$('#filter1').on('click',function(){
		var $ul = $('#content_div ul');
		$ul.html('');
		if(count%2 == 0){
			$('#filter1').text('');
			$('#filter1').text('제출일▲ |');
			
			
			$.ajax({
				type : 'post',
				url : 'filter.of',
				data : {filter : 1},
				success : function(data) {
					for(var i=0; i<data.length; i++) {
										$ul.append('<li>'+
												'<div class="resultContents">'+
												'<p>'+data[i].ppsName+'</p>'+
												'<input type="hidden" value="'+data[i].ppsId+'">'+
												'<input type="hidden" class= "scrolling" value="'+(Number(i+1))+'">'+
												'<p>'+
												''+data[i].company.comName+'<input type="hidden" value="'+data[i].company.comId+'">'+
												' / '+data[i].client.clientName+'<input type="hidden" value="'+data[i].client.clientId+'">'+
												'<p>'+day(data[i].submitDate)+'</p>'+
												'</p>'+
												'</div>'+
												'</li>')
									}
				}
			})
		} else {
			$('#filter1').text('');
			$('#filter1').text('제출일▼ |');
			$.ajax({
				type : 'post',
				url : '',
				success : function(data) {
					
				}
			})
		}
		$('#filter1').css({"font-weight":"bold"})
		$('#filter2').css({"font-weight":"normal"})
		$('#filter3').css({"font-weight":"normal"})
		count++;
	})
	$('#filter2').on('click', function() {
		$('#filter1').css({"font-weight":"normal"})
		$('#filter2').css({"font-weight":"bold"})
		$('#filter3').css({"font-weight":"normal"})
	})
	$('#filter3').on('click', function() {
		$('#filter1').css({"font-weight":"normal"})
		$('#filter2').css({"font-weight":"normal"})
		$('#filter3').css({"font-weight":"bold"})
	})
	
	$('#searchBtn').on('click', function(){
		var titleName = $('#titleName');
		var department = $('#department');
		
		console.log(titleName.val());
		console.log(department.val());
		
		loaction.href = "";
	})
</script>
</body>
</html>