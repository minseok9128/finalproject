<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
</head>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<jsp:include page="../../common/menubar.jsp" />
<jsp:include page="../../common/modal/productEnrollment.jsp"></jsp:include>
<body>
	<div class="inner_ct">
		<div class="main_ctn" style="background: white;">
			<div class="title"></div>
			<div class="tb_cnt">
				<div class="tb_title">
					<i class="home icon"></i>제품관리
				</div>
				<div style="margin-top: 20px;">
					<div id="contentDiv">
						<div>
							<h3 style="margin-left: 5px; margin-top: 5px; margin-bottom: 5px;">제품</h3>
						</div>
						<div>
							<p>제품명</p>
							<input type="text">
							<input type="button" value="조회">
						</div>
					</div>
					<div id="contentDiv2">
						<div>
							<h3 style="margin-left: 5px; margin-top: 5px; margin-bottom: 5px;">상세</h3>
						</div>
						<table id="product_Table">
							<tr>
								<td>
									<p>제품코드</p>
									<input type="text">
								</td>
								<td>
									<p>제품명</p>
									<input type="text">
								</td>
							</tr>
							<tr>
								<td>
									<p>출시일</p>
									<input type="text">
								</td>
								<td>
									<p>단가</p>
									<input type="text">
								</td>
							</tr>
							<tr>
								<td>
									<p>포장단위</p>
									<input type="text">
								</td>
								<td>
									<p>포장수량</p>
									<input type="text">
								</td>
							</tr>
							<tr>
								<td>
									<p>규격</p>
									<input type="text">
								</td>
								<td>
									<p>제품종류</p>
									<input type="text">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<h6>제품 설명</h6>
									<textarea></textarea>
								</td>
							</tr>
						</table>
					</div>
					<div id="contentDiv2_1">
						<button id="submit_Btn" class="submit_Btn">등록</button>
						<button id="modify_Btn" class="submit_Btn">수정</button>
						<button id="calcel_Btn" class="cancel_Btn">삭제</button>
					</div>
					<div id="contentDiv2_2">
						<div>
							<h3 style="margin-left: 5px; margin-top: 5px; margin-bottom: 5px;">이력</h3>
						</div>
						<table id="product_Table2">
							<tr>
								<th>단가</th>
								<th>등록자</th>
								<th>등록일</th>
								<th>변경자</th>
								<th>변경일</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function() {
			$("#submit_Btn").click(function(){
				$('.ui.modal.productEnrollment').modal('show');
			})
			$('#exit_Btn5').click(function(){
				$('.ui.modal.productEnrollment').modal('hide');
			})
		})
	</script>
</body>
</html>