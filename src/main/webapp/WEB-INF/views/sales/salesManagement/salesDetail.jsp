<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 매출</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/menubar.jsp" />
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script 
	src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<style>
	#resultSum *{
		text-align:center;
	}
</style>
</head>
<body>

	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title "></div>
			<div id="header_Detail_div">
				<h1>
					매출세부사항
					<button class="cancel_Btn Enrollment_Btn">취소</button>
					<button class="submit_Btn Enrollment_Btn">수정</button>
				</h1>
				<br>
				<!-- <h6>SBS제품판매 관련</h6> -->
				<br> <br>
			</div>
			<div id="content_Enrollment_Detail_div">
				<input type="hidden" value="${list[0].REV_ID}" >
				<table id="table2">
					<tr>
						<td class="td">매출구분</td>
						<td >
							<a class="Detail_a2">
								<c:if test="${list[0].REV_CATEGORY eq 'PRODUCT'}">
									상품매출
								</c:if>
								<c:if test="${list[0].REV_CATEGORY ne 'PRODUCT'}">
									커미션매출
								</c:if>
							</a>
						</td>
					</tr>
					<tr>
						<td class="td">고객사</td>
						<td>
							<a class="Detail_a"><c:out value="${list[0].COM_NAME}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">고객</td>
						<td>
							<a class="Detail_a"><c:out value="${list[0].CLIENT_NAME}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">견적</td>
						<td>
							<a class="Detail_a"><c:out value="${list[0].EST_NAME}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">계약</td>
						<td>
							<a class="Detail_a"><c:out value="${list[0].CONT_NAME}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">주문일</td>
						<td>
							<a class="Detail_a2">
								<c:out value="${fn:split(list[0].REV_ORDER,' ')[0]}" />
								<%-- <c:out value="${list[0].REV_ORDER}"/> --%>
							</a>
						</td>
					</tr>
					<tr>
						<td class="td">수량</td>
						<td>
							<a class="Detail_a2"><c:out value="${list[0].REV_QUANTITY}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">공급가액</td>
						<td>
							<a class="Detail_a2"><c:out value="${list[0].REV_TOTAL_VALUE}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">세액</td>
						<td>
							<a class="Detail_a2"><c:out value="${list[0].REV_TAX}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">합계</td>
						<td>
							<a class="Detail_a2"><c:out value="${list[0].REV_TOTAL}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">부가세</td>
						<td>
							<a class="Detail_a2">
								<c:if test="${list[0].REV_VAT_YN eq 'N'}">
									부가세 미포함
								</c:if>
								<c:if test="${list[0].REV_VAT_YN eq 'Y'}">
									부가세 포함
								</c:if>
							</a>
						</td>
					</tr>
					
					<tr>
						<td class="td">담당자</td>
						<td>
							<a class="Detail_a2"><c:out value="${list[0].EMP_NAME}"/></a>
						</td>
					</tr>
					<tr>
						<td class="td">상세</td>
						<td>
							<a class="Detail_a2"><c:out value="${list[0].REV_DETAIL}"/></a>
						</td>
					</tr>
				</table>

					<div class="estumateproduct">
						<h1 class="propose_report">제품</h1>
						<!-- <button class ="ui basic blue button product_add">제품 등록</button> -->
					</div>
					<table class="estumateTable" id="contract_Product_Table">
						<thead>
							<tr>
							<th>제품명</th>
							<th>단가</th>
							<th>수량</th>
							<th>공급가액</th>
						</tr>
						<tr>
							<th>포장수량</th>
							<th>규격</th>
							<th colspan="2">납품예정일</th>
						</tr>
						</thead>
						<c:set var="sumA" value="0"/>
						<c:set var="sumP" value="0"/>
						<tbody>
							<c:forEach var="pr" items="${list}">
								<c:set var="sumA" value="${sumA + pr.QUANTITY_DETAIL}"/>
								<c:set var="sumP" value="${sumP + pr.TOTAL_PRICE}"/>		
								<tr>
									<td><input type="hidden" name="prId" value="'+prId +'"><c:out value="${pr.PDT_NAME}"/></td>
									<td><c:out value="${pr.DETAIL_PRICE}"/></td>
									<td class="amount"><c:out value="${pr.QUANTITY_DETAIL}"/></td>
									<td class="sum"><c:out value="${pr.TOTAL_PRICE}"/></td>
								</tr>
								<tr>
									<td><c:out value="${pr.QUANTITY}"/></td>
									<td><c:out value="${pr.STANDARD}"/></td>
									<td colspan="2"><c:out value="${pr.DELIVERY_DATE}"/></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr id="resultSum">
								<th>수량합계</th>
								<th><c:out value="${sumA}"/></th>
								<th>제안금액 합계</th>
								<th><c:out value="${sumP}"/></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				<hr>
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
	$(function(){
		
		$("#resultSum").children().eq(1).html();
		$("#resultSum").children().eq(3).html();
		$('.cancel_Btn').click(function(){
			location.href="showEstimateMain.sm";
		})
		//모달 뛰우는 스크립트
		$("#curstormer_search").click(function(){
			$('.ui.modal.curstormer')
			  .modal('show');
		})
		$("#company_search").click(function(){
			$('.ui.modal.company')
			  .modal('show');
		})
		$("#opportunity_search").click(function(){
			$('.ui.modal.opportunity')
			  .modal('show');
		})	
		$("#manager_search").click(function() { 
			$('.ui.modal.manager')
			  .modal('show');
		})
		$("#exit_Btn").click(function(){
			$('.ui.modal.curstormer').modal('hide');
		})
		$("#exit_Btn2").click(function(){
			$('.ui.modal.company').modal('hide');
		})
		$("#exit_Btn3").click(function(){
			$('.ui.modal.opportunity').modal('hide');
		})
		$("#exit_Btn4").click(function(){
			$('.ui.modal.manager').modal('hide');
		})
		$("#propose_Request_Btn").click(function(){
			$("#propose_Request_file").click();
		})
		$("#propose_report_Btn").click(function(){
			$("#propose_report_file").click();
		})
		$('#calcel_Btn').click(function(){
			location.href="showOfferView.sm";
		})
	
		//요청일, 제안일 켈린더 뜨는 팝업
			$('#standard_calendar').calendar({
				type : 'date'
			});
			$('#standard_calendar2').calendar({
				type : 'date'
			});
			//버튼 마우스 커서 변경
			$('button').mouseover(function() {
				$('button').css({
					"cursor" : "pointer"
				});
			})
		})
		$(function(){
        // 열리지 않는 메뉴
       $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
        
        // 열리는 메뉴
        $(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
        $(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(3).addClass("on");
    });
	</script>
</body>
</html>