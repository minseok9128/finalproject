<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 매출등록</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/menubar.jsp" />
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<style>
	.estumateTable {text-align: center;}
	.estumateTable  tr:nth-child(odd) td {vertical-align: middle; border-bottom: 1px solid #E5E5E5; border-right: 1px solid #E5E5E5;}
	.estumateTable  tr:nth-child(odd) td:nth-last-child(1) {border-right: none;}
	.estumateTable  tr:nth-child(even) td {height: 33px; vertical-align: middle; border-bottom: 1px solid #E5E5E5; border-right: 1px solid #E5E5E5;}
	.estumateTable  tr:nth-child(odd) td input {text-align: right; padding-right: 5px; border: 1px solid #a0a0a0; border-radius: 2px;}
	.estumateTable  tr:nth-child(even) td input {height:25px; text-align: right; padding-right: 5px; border: 1px solid #a0a0a0; border-radius: 2px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(1) {width: 35%; height: 33px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(3) {width: 17.5%; height: 33px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(3) input {width: 80%; height: 25px; margin-right: 3px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(4) input {width: 90%; height: 25px;}
	.estumateTable  tr:nth-child(odd) td:nth-child(2) input {width: 80%; height: 25px;}
	.estumateTable  tr:nth-child(even) td:nth-child(3) input {width: 80%; height: 25px;}
	.estumateTable  tr:nth-child(even) td:nth-child(4) input {width: 95%; height: 25px;}
	.estumateTable tfoot th{ text-align: left; }
	.select_checkBox_div {
		width:95%;
	}
	#revCategory {
		width:100%;
	}
</style>
<script
	src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<jsp:include page="../../common/modal/product3.jsp"></jsp:include>
<jsp:include page="../../common/modal/estimate2.jsp"></jsp:include>
<jsp:include page="../../common/modal/contract.jsp"></jsp:include>
</head>
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>

		<div class="tb_cnt">
			<div class="tb_title "></div>
			<c:set var="revId" value="${revId}" />
			<c:if test="${revId == 0}">
				<!-- revId가 0 이면 등록 컨트롤러 이동 -->
				<c:set var="move" value="insert.sal" />
			</c:if>
			<c:if test="${revId != 0}">
				<!-- revId가 0이 아니면 수정 컨트롤로 이동 -->
				<c:set var="move" value="" />
			</c:if>
			<form action="${move}" method="post" enctype="multipart/form-data">
				<div class="header_div">
					<c:if test="${revId == 0}">
						<c:set var="list" value="0" />
					</c:if>
					<c:if test="${revId != 0}">
						<c:set var="list" value="${list}" />
					</c:if>
					<div>
						<c:if test="${revId == 0}">
							<h1>매출  등록</h1>
						</c:if>
						<c:if test="${revId != 0}">
							<h1>매출 수정</h1>
						</c:if>
						<button id="realBtn" hidden>이것은 입력하지 않은 file input 태그를
							지우고 controller에 보내는 버튼</button>
						<button class="ui basic red button" id="cancelEnrollBtn">취소</button>
						<c:if test="${revId == 0}">
							<button class="ui basic blue button" id="submitBtn">작성</button>
						</c:if>
						<c:if test="${revId != 0}">
							<button class="ui basic blue button" id="modifyBtn">수정</button>
						</c:if>
					</div>
				</div>
				<div id="content_Enrollment_div">
					<table id="table2">
						<tr>
							<td class="td">매출구분<b class="point">*</b></td>
							<td><c:if test="${revId == 0}">
									<div class="select_checkBox_div">
										 <select id="revCategory" name="revCategory">
										 	<option value="PRODUCT">상품매출</option>
										</select>
									</div>
									<!-- <input type="text" id="offer_Name" class="common_Text1">
									<input type="hidden" value="0" name="revCategory"> -->
								</c:if> <c:if test="${revId != 0}">
									<input type="text" id="offer_Name" class="common_Text1">
									<!-- 불러온 값 넣어야 함 -->
									<input type="hidden" value="0" name="revCategory">
								</c:if></td>
						</tr>
						<tr>
							<td class="td">고객사</td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="comName"  id="company" class="common_Text2"> 
									<input type="hidden" name="comId" id="company_hidden" value="0">
								</c:if> <c:if test="${revId != 0 }">
									<!-- 값 불러온거 넣어야 함 -->
									<input type="text" name="comName" id="company" class="common_Text2"> 
									<input type="hidden" name="comId" id="company_hidden" value="0">
								</c:if> 
								<button class="ui grey basic button srch searchBtn" id="company_Modal">검색</button>
							</td>
						</tr>
						<tr>
							<td class="td">고객<b class="point">*</b></td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="clientName" id="client" class="common_Text2">
									<input type="hidden" name="clientId" id="client_hidden" value="0">
								</c:if>
								<c:if test="${revId != 0}">
									<!-- 불러온 값 넣어야 함 -->
									<input type="text" name="clientName" id="client" class="common_Text2">
									<input type="hidden" name="clientId" id="client_hidden" value="0">
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="client_Modal">검색</button></td>
						</tr>
						<tr>
							<td class="td">견적</td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="estName" id="estimate" class="common_Text2">
									<input type="hidden" name="estId" id="estimate_hidden" value="0" />
								</c:if>
								<c:if test="${revId != 0}">
									<!-- 불러온 값 넣어야 함 -->
									<input type="text" name="estName" id="estimate" class="common_Text2">
									<input type="hidden" name="estId" id="estimate_hidden" value="0" />
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="estimate_Modal">검색</button>
							</td>
						</tr>
						<tr>
							<td class="td">계약</td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="contractName" id="contract" class="common_Text2">
									<input type="hidden" name="contractId" id="contract_hidden" value="0" />
								</c:if>
								<c:if test="${revId != 0}">
									<!-- 불러온 값 넣어야 함 -->
									<input type="text" name="contractName" id="contract" class="common_Text2">
									<input type="hidden" name="contractId" id="contract_hidden" value="0" />
								</c:if>
								<button class="ui grey basic button srch searchBtn" id="contract_Modal">검색</button>
							</td>
						</tr>
						<tr>
							<td class="td">주문일<b class="point">*</b></td>
							<td>
								<div class="two fields">
									<div class="field">
										<div class="ui calendar" id="standard_calendar2">
											<div class="ui input right icon" style="bottom: 6px;">
												<i class="calendar icon"></i> 
												<c:if test="${revId == 0}">
													<input type="text" name="revOrder" readonly>
												</c:if>
												<c:if test="${revId != 0}">
													<!-- 불러온 값 넣어야 함 -->
													<input type="text" name="revOrder" readonly>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td">수량<b class="point">*</b></td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="revQuantity" id="totalAmount" class="common_Text1 text_text product_add" value="0" readonly>
								</c:if>
								<c:if test="${revId != 0}">
									<input type="text" name="revQuantity" id="totalAmount" class="common_Text1 text_text" readonly>
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p>
							</td>
						</tr>
						<tr>
							<td class="td">공급가액<b class="point">*</b></td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="revTotalValue" id="totalValue" class="common_Text1 text_text product_add" value="0" readonly>
								</c:if>
								<c:if test="${revId != 0}">
									<input type="text" name="revTotalValue" id="totalValue" class="common_Text1 text_text" readonly>
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p>
							</td>
						</tr>
						<tr>
							<td class="td">세액<b class="point">*</b></td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="revTax" id="tax" class="common_Text1 text_text product_add" value="0" readonly>
								</c:if>
								<c:if test="${revId != 0}">
									<input type="text" name="revTax" id="tax" class="common_Text1 text_text" readonly>
								</c:if>
									<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p>
							</td>
						</tr>
						<tr>
							<td class="td">합계<b class="point">*</b></td>
							<td>
								<c:if test="${revId == 0}">
									<input type="text" name="revTotal" id="total" class="common_Text1 text_text product_add" value="0" readonly>
								</c:if>
								<c:if test="${revId != 0}">
									<input type="text" name="revTotal" id="total" class="common_Text1 text_text" value="0" readonly>
								</c:if>
								<p class="text_p">제품을 선택하시면 자동으로 변경됩니다.</p>
							</td>
						</tr>
						<tr>
							<td class="td">부가세</td>
							<td>
								<c:if test="${revId == 0 }">
									<input type="checkbox" name="revVatYn" value="Y" id="VATCheckBox" class="enrollment_CheckBox">
								</c:if>
								<c:if test="${revId != 0}">
									<input type="checkbox" name="revVatYn" class="enrollment_CheckBox" checked="checked">
								</c:if>
							</td>
						</tr>
						<tr>
							<td class="td">담당자<b class="point">*</b></td>
							<td>
								<c:if test="${revId == 0 }">
									<input readonly class="common_Text1" type="text" style="width: 100%; height: 32px;" value="${sessionScope.loginEmp.empName }">
									<input type="hidden" style="width: 250px; height: 32px;" name="empId" value="${sessionScope.loginEmp.empNo }">
									<!-- <input type="text" id="managerText" name="empName" class="common_Text2">
									<input type="hidden" id="manager_hidden" name="empId" class="common_Text2"> -->
								</c:if>
								<c:if test="${revId != 0}">
									<input readonly class="common_Text1" type="text" style="width: 100%; height: 32px;" value="${sessionScope.loginEmp.empName }">
									<input type="hidden" style="width: 250px; height: 32px;" name="empId" value="${sessionScope.loginEmp.empNo }">
									<!-- <input type="text" id="managerText" name="empName" class="common_Text2">
									<input type="hidden" id="manager_hidden" name="empId" class="common_Text2"> -->
								</c:if>
								<!-- <button class="ui grey basic button srch searchBtn" id="manager">검색</button> -->
							</td>
						</tr>
						<tr>
							<td class="td">상세</td>
							<td>
								<c:if test="${revId == 0}">
									<textarea name="revDetail" class="contents"></textarea>
								</c:if>
								<c:if test="${revId != 0 }">
									<textarea name="revDetail" class="contents"></textarea>
								</c:if>
							</td>
						</tr>
					</table>
					<div class="estumateproduct">
						<h1 class="propose_report">제품</h1>
						<button class ="ui basic blue button product_add">제품 등록</button>
					</div>
					<table class="estumateTable" id="contract_Product_Table">
						<thead>
							<tr>
							<th>제품명</th>
							<th>단가</th>
							<th>수량</th>
							<th>공급가액</th>
							<th rowspan="2">삭제</th>
						</tr>
						<tr>
							<th>포장수량</th>
							<th>규격</th>
							<th colspan="2">납품예정일</th>
						</tr>
						</thead>
						<tbody>
							
						</tbody>
						<tfoot>
						
						</tfoot>
					</table>
					
				</div>
			</form>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
		var gfv_count = 1;
		var count=1;
		
		
		$(function() {
			$('.cancel_Btn').click(function() {
				location.href = "showEstimateMain.sm";
			})
			//모달 뛰우는 스크립트
			//요청일, 제안일 켈린더 뜨는 팝업
			$('#standard_calendar').calendar({
				type : 'date',
				 formatter: {
					  date : function (date, settings) {
					        if (!date) return '';
					        var day = date.getDate();
					        var month = date.getMonth() + 1;
					        var year = date.getFullYear();
					        return year + '-' + month + '-' + day;
					        /* return day + '/' + month + '/' + year; */
					      }
					}
			});
			$('#standard_calendar2').calendar({
				type : 'date',
				 formatter: {
					  date : function (date, settings) {
					        if (!date) return '';
					        var day = date.getDate();
					        var month = date.getMonth() + 1;
					        var year = date.getFullYear();
					        return year + '-' + month + '-' + day;
					        /* return day + '/' + month + '/' + year; */
					      }
					}
			});
			//버튼 마우스 커서 변경
			$('button').mouseover(function() {
				$('button').css({
					"cursor" : "pointer"
				});
			})
		
			// 열리지 않는 메뉴
			$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
					"open");

			// 열리는 메뉴
			$(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
			$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(3)
					.addClass("on");
			
			$(".product_add").click(function(){
				$('.ui.modal.product').modal('show');
				return false;
			})
			$('#productFile_btn').click(function (e) {
                    e.preventDefault();
                    var str = '<tr><td><input type="file" name="files'+ (count++) +'" style="display:none" ><input class="upload_name" id="upload_name' 
                   + (count++) + '" readonly style="border:0;""></td><td><i class="trash alternate outline icon" style="float: right" onclick="remove_btn($(this))"></i></td></tr>';
                   
                   console.log("트리거 시작전");
                   $("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
                   console.log("트리거 시작후");
                   $("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
                        var cur=$(this).val();
                        var arSplitUrl = cur.split('\\');
                        var nArLength = arSplitUrl.length;
                        var fileName = arSplitUrl[nArLength-1];
                          
                          $("#upload_name" + (count-1)).val(fileName);
                        console.log(cur);
                     }); 
                  }); 
			
			function fn_addFile(){
				$("#propose_report_file").click();
			}
			 $("#propose_report_file").change(function(e){
				 var fileName = $('input[type=file]')[0].files[0].name
				 var fileType = $("#propose_report_file")[0].files[0].type
				 var fileSize = $("#propose_report_file")[0].files[0].size
				    /* alert($('input[type=file]')[0].files[0].name); //파일이름
				       alert($("#propose_report_file")[0].files[0].type); // 파일 타임
				       alert($("#propose_report_file")[0].files[0].size); // 파일 크기 */
				 console.log(fileName);
				 console.log(fileType);
				 console.log(fileSize);
				 var str = "<p><input type='file' name='file_"+(gfv_count++)+"'><a href='#this' class='btn' name='delete'>삭제</a></p>";
				
				 $("#fileDiv").append(str); $("a[name='delete']").on("click", function(e){
				 //  $('input[type=file]')[0].files[0].name;
				 //  $("#imgUpload")[0].files[0].type;
				 //  $("#imgUpload")[0].files[0].size;
				 
				 });
				 console.log($("#fileDiv").children().children('input'))
				 $("#fileDiv").children().children('input').val(fileName);
			 });

		});
		$(document).on('click','#VATCheckBox',function(){
			var quantity = changeNumber($('#contract_Product_Table tfoot').children().children()[1].innerHTML);
			var totalPrice = changeNumber($('#contract_Product_Table tfoot').children().children()[3].innerHTML);
			console.log("quantity : " + quantity);
			console.log("totalPrice : " + totalPrice);
			var totalValue;
			var tax;
			if($('#VATCheckBox').is(':checked')){
				totalValue = Math.round(totalPrice/1.1);
				tax = totalPrice - totalValue
				 $('#totalValue').val(comma(totalPrice - tax));
				 $('#tax').val(comma(tax));
				 $('#total').val(comma(totalPrice));	
				} else {
					totalValue = Math.round(totalPrice*1.1);
					tax = totalValue - totalPrice 
					 $('#totalValue').val(comma(totalValue-tax));
					 $('#tax').val(comma(tax));
					 $('#total').val(comma(totalPrice+tax));
				}
		 })
		
			/* $('#VATCheckBox').change(function(){
				var tablelength = $("#contract_Product_Table").children()[1].rows.length;
				
				if(tablelength != 0){
					//수량
					 var price = $('#contract_Product_Table tfoot').children().eq(0).children().eq(1).text();
					//전체 공급가액
					var totalValueOfSupply = $('#contract_Product_Table tfoot').children().eq(0).children().eq(3).text();
					//세액
					var Tex = changenTex(totalValueOfSupply);
					var TexVAT = changenTexVAT(totalValueOfSupply);
					//합계
					var total = total(Tex,totalValueOfSupply);
					
					if($('#VATCheckBox').is(":checked") == false){
						$('#price').val(comma(price));
						$('#totalValueOfSupply').val(comma(totalValueOfSupply));
						$('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
						$('#tex').val(comma(Tex));
						$('#total').val(comma(total));
				 	} else {
						 $('#total').val(comma(totalValueOfSupply));
						 $('#tex').val(comma(TexVAT));
						 var totalValueOfSupply = changeNumber(totalValueOfSupply)-changeNumber(TexVAT)
						 $('#totalValueOfSupply').val(comma(totalValueOfSupply));
						 $('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
				 }
					
				} 
				function total(a, b){
					//세금
					var Tex = changeNumber(a);
					//전체 공급가액
					var totalValueOfSupply = changeNumber(b);
					
					return totalValueOfSupply+Tex
				}
			}) */
			
			
			// 견적 품목 추가 되면 위에 수량등에 대한 정보를 입력하는 함수
			function changeTable() {
				var tablelength = $("#contract_Product_Table").children()[1].rows.length;
				//수량
				 var price = $('#contract_Product_Table tfoot').children().eq(0).children().eq(1).text();
				//전체 공급가액
				var totalValueOfSupply = $('#contract_Product_Table tfoot').children().eq(0).children().eq(3).text();
				//세액
				var Tex = changenTex(totalValueOfSupply);
				var TexVAT = changenTexVAT(totalValueOfSupply);
				//합계
				var total = total(Tex,totalValueOfSupply);
				
				if(tablelength != 0){
					 if($('#VATCheckBox').is(":checked") == false){
							$('#price').val(comma(price));
							$('#totalValueOfSupply').val(comma(totalValueOfSupply));
							$('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
							$('#tex').val(comma(Tex));
							$('#total').val(comma(total));
					 } else {
							 $('#total').val(comma(totalValueOfSupply));
							 $('#tex').val(comma(TexVAT));
							 var totalValueOfSupply = changeNumber(totalValueOfSupply)-changeNumber(TexVAT)
							 $('#totalValueOfSupply').val(comma(totalValueOfSupply));
							 $('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
					 }
					
				}
				
				function total(a, b){
					//세금
					var Tex = changeNumber(a);
					//전체 공급가액
					var totalValueOfSupply = changeNumber(b);
					
					return totalValueOfSupply+Tex
				}
			}	
			function changenTex(totalValueOfSupply){
				var a = 0;
				var StringtotalValueOfSupply = totalValueOfSupply.split(',');
				var numtotalValueOfSupply = '';
				for(var i=0; i<StringtotalValueOfSupply.length; i++) {
					numtotalValueOfSupply += StringtotalValueOfSupply[i]
				}
				a = numtotalValueOfSupply;
				var d = (a*1.1 - a).toFixed(3);
				var b = d.split('.');
				var c = 0;
				
				if(b[1] == '000'){
					c = b[0];
				} else {
					c = d;
				}
				return c
			}
			
			 
			
			 function changeNumber(a) {
				var StringA = a.split(',');
				var numberA = '';
				for(var i=0; i<StringA.length; i++) {
					numberA += StringA[i];
				}
				return Number(numberA);
			}
			//세액으로 변환 부가가치세 포함
			 function changenTexVAT(totalValueOfSupply){
					var a = 0;
					var StringtotalValueOfSupply = totalValueOfSupply.split(',');
					var numtotalValueOfSupply = '';
					for(var i=0; i<StringtotalValueOfSupply.length; i++) {
						numtotalValueOfSupply += StringtotalValueOfSupply[i]
					}
					a = numtotalValueOfSupply;
					var d = Math.abs((a/1.1 - a)).toFixed(3);
					var b = d.split('.');
					var c = 0;
					
					if(b[1] == '000'){
						c = b[0];
					} else {
						c = d;
					}
					return c
				}
				
				 function total(a, b){
					//세금
					var Tex = changeNumber(a);
					//전체 공급가액
					var totalValueOfSupply = changeNumber(b);
					
					return totalValueOfSupply+Tex
				}
				
				 function changeNumber(a) {
					var StringA = a.split(',');
					var numberA = '';
					for(var i=0; i<StringA.length; i++) {
						numberA += StringA[i];
					}
					return Number(numberA);
				}
				 
			 $(document).keypress(function(e){
				 var tfoot = $('#contract_Product_Table tfoot').children().children();
				 
				  if(e.target.parentNode.offsetParent.id =='contract_Product_Table'){
					  
					  $(e.target).off().on('propertychange keyup paste',function(e) {
					  //change
					  /* var $(this). */
					  var event = $(e.target);
					  var evt = e.target;
					  var th = $(this);
					  var sum = event.parent().parent().children().eq(3);
					  var amount = event.parent().parent().children().children().eq(2).val();
					  var price = event.parent().parent().children().children().eq(1).val();
					  var tfoot = $("tfoot").html();
					  console.log("tfoot");
					  var tf = $("#resultSum");
					  var resultA = 0;
					  var resultS = 0;
					  var amountArr  = $(".amount");
					  var sumArr  = $(".sum");
					  for(var i = 0; i < amountArr.length; i++){
						  console.log(amountArr[i].value);
						  resultA += Number(amountArr[i].value);
						}
					  for(var i = 0; i < sumArr.length; i++){
						  console.log(sumArr[i].value);
						  resultS += Number(sumArr[i].innerHTML);
						}
					  //totalAmount
					  //totalValue
					  //tax
					  //total
					  var ttA = $("#totalAmount");
					  var ttV = $("#totalValue");
					  var tax = $("#tax");
					  var tt = $("#total");
					  //수량 , 합계 바꾸는 부분
					  
					  
					  
					  
					  ttA.val(resultA.toFixed());
					  ttV.val(resultS.toFixed());
					  tax.val((resultS*0.1).toFixed());
					  tt.val((1.1 * resultS).toFixed());
					  tf.children().eq(1).html(resultA.toFixed()) 
					  /* tf.children().eq(3).html(resultS.toFixed()) */
					  
					  console.log(tfoot);
						  if(event.hasClass("price")){
							  console.log("price");
							  sum.html(event.val() * amount);
						  } else if (event.hasClass("sum")){
							  console.log("sum");
							  console.log(th.parent().children('.sum').val())
						  } else if (event.hasClass("amount")){
							  var sum = event.parent().parent().children().eq(3);
							  console.log("amount");
							  console.log(th.parent().children('.sum').val())
							  sum.html(event.val() * price);
						  }
					  })
				 } 
			 }) 
	</script>
</body>
</html>