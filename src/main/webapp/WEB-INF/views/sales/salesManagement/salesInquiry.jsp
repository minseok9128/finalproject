<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
</head> 
	<jsp:include page="../../common/menubar.jsp" />
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1 id="estimate_h1">
					견적
					<button id="enrollment_estimate_Btn" class="search_Btn">등록하기</button>
				</h1>
				<input type=text id="costomer_text_esimate" placeholder="제안/고객/고객사">
				<input type="text" id="product_Name" placeholder="제품명">
				<br> <input type=text id="Department" placeholder="부서명">
				<input type=text id="employee" placeholder="사원명">
				<button class='submit_Btn' id="search_estimate_Btn">검색</button>
			</div>
			<div id="content_div">
			
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	<script>
	$(function(){
        // 열리지 않는 메뉴
       $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
        
        // 열리는 메뉴
        $(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
        $(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(3).addClass("on");
    });
	$(function(){
		$('#enrollment_estimate_Btn').click(function(){
			location.href="showEstimateEnrollment.sm";
		})
		$('.resultContents').click(function() {
			location.href="showEstimateDetail.sm";
		})
	}) 
	</script>
</body>
</html>