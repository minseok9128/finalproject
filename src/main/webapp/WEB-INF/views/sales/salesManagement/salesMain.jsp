<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management2.css">
<style>

	.resultContents{
		/* display: flex; */
		position:relative;
		/* height:0; */
	}
	.fl {
		float:left;
		width:46%;
		display:inline-block;
	}
	.fr {
		padding-top:10px;
		/* flex : 1; */
		float:right;
		width:54%;
		
		text-align:center;
		vertical-align:middle;
	}	
	.fr li{
		display:inline-block;
	}
	.btm {
		width:743px;
		overflow : hidden;
		padding-left:10px;
		background:#FBFCFD;
		border-top:1px dashed #E1E6EB;
		/* position:absolute; */bottom:0px;
	}
	
	.ic{background-image:url(${contextPath}/resources/images/icon_com.png);
		background-size:contain;
		height:10px;
		/* display:inline-block; */
		width:10px;
		background-repeat:no-repeat;
		margin-top:10px;}
	.icl{background-image:url();
	margin-top:10px;}
	.itt{background-image:url();
	margin-top:10px;}
	.Cname {
		color:#27aae1;
	}
	/* .hoverB { color:#27aae1;} */
	.resultContents:hover{
		border:1px solid #27aae1;
		cursor:pointer;
	}
	.resultContents:hover .btm span{
		color:#27aae1;
	}
</style>
</head>
	<jsp:include page="../../common/menubar.jsp" />
<body> 
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div class="header_div">
				<div>
					<h1>매출</h1>
					<button id="enrollment_sales_Btn" class="search_Btn">등록하기</button>
				</div>
				<div class="search">
					<div class="one">
						<div class="half input">
							<input type="text" placeholder="견적/고객/고객사">
						</div>
						<div class="half input">
							<input type="text" placeholder="제품명">
						</div>
					</div>
					<div class="secondSearchDiv">
						<div class="select half"> 
							<select>
								<option>영업부</option>
							</select>
						</div>
						<div class="input">
							<input type="text" placeholder="사원명">
							<button class="ui blue basic button float">검색</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="content_div">
			<div> 
				<button id="filter_Btn">필터</button>
				<P><c:out value="${fn:length(list)}"></c:out>건</P>
				<p class="filters">가나다</p>
				<p class="filters">고객사 |</p>
				<p class="filters" style="font-weight: bold;">견적일▼ |</p>
			</div>
			<!-- <hr> -->
			<c:forEach var="sales" items="${list}" >
				<div class="resultContents">
					<input type="hidden" value="${sales.REV_ID}" >
					<div class="fl">
						<p class="Cname"><c:out value="${sales.CLIENT_NAME}" /></p>
						<c:if test="${sales.CONT_NAME != null }">
							<p>견적명 : <c:out value="${sales.CONT_NAME}" /></p>
						</c:if>
						<c:if test="${sales.EST_NAME != null }">
							<p>계약명 : <c:out value="${sales.EST_NAME}" /></p>
						</c:if>
						<p>
							주문일 : <c:out value="${fn:split(sales.REV_ORDER,' ')[0]}" />
							<%-- <c:out value="${sales.REV_ORDER}" /> --%>
						</p>
					</div>
					<div class="fr">
						<ul>
							회사명 : <li><span class="ic"></span><c:out value="${sales.COM_NAME}" /></li>
							<%-- <li><span class="icl"></span><c:out value="${sales.CLIENT_NAME}" /></li>
							<li><span class="itt"></span><c:out value="${sales.REV_TOTAL_VALUE}" /></li> --%>
						</ul>
					</div>
					<div class="btm">제품(<span><c:out value="${sales.PRC}" /></span>) | 공급가액(<span><c:out value="${sales.REV_TOTAL_VALUE}" /></span>) | 세액(<span><c:out value="${sales.REV_TAX}" /></span>) | 부가세(<span><c:if test="${sales.REV_VAT_YN eq 'Y'}" >포함</c:if><c:if test="${sales.REV_VAT_YN eq 'N'}" >미포함</c:if></span>) | 합계(<span><c:out value="${sales.REV_TOTAL}" /></span>) </div>
				</div>
			</c:forEach>
		</div>
	</div>

		</main>
	</div>
	</div>
	</div>
	<script>
	$(function(){
        // 열리지 않는 메뉴
       $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
        
        // 열리는 메뉴
        $(".menu_cnt .main_menu").eq(3).addClass("on").addClass("open");
        $(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(3).addClass("on");
    });
	$(function(){
		$('#enrollment_sales_Btn').click(function(){
			location.href="goInsert.sal";
		})
		$('.resultContents').click(function() {
			var id = $(this).children('input').val();
			location.href="selectOne.sal?revId=" + id;
		})
		$(".resultContents").mouseover(function(){
			
		})
	}) 
	</script>
</body>
</html>