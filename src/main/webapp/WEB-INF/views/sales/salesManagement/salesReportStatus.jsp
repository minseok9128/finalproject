<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style> 
	.searchArea {
		margin-top:20px;
	}
	
	.searchArea > label {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	.contentList {
		border:1px solid #DEE2E8;
		padding:10px;
		cursor:pointer;
	}
	
	.title {
		font-weight:bold;
		font-size: 23px;
    	color: #3D4959;
	}
	
	.hr {
		background-color:#B9C2CE;
		margin-top:10px;
		margin-bottom:10px;
	}
	
	.left {
		float:left;
	}
	
	.right {
		float:right;
	}
	.searhContent {
		cursor:pointer;
		height:120px;
	}
	.searhContent > div {
		margin-top:20px;
		overflow:hidden;
	}

	.calenderPeriod {
		margin-top:auto;
		margin-bottom:auto;
		margin-left:20px;
		margin-right:20px;
	}
	
	.calenderWidth {
		width:600px;
		display:inline-block;
	}
	.resultContents {
		padding:10px;
		margin-top:10px;
	}
	.resultContents div{
		margin-top:5px;
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<jsp:useBean id="now" class="java.util.Date" />
	<fmt:formatDate value='${now}' pattern='yyyy-MM-dd' var="nowDate"/>
	
	<div class="main_ctn">
		<div class="title">영업보고관리</div>
		<div class="tb_cnt">
			<div class="searchArea">
				<div class="ui form calenderWidth">
					<div class="two fields">
					    <div class="field">
							<div class="ui calendar" id="rangestart">
							  	<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" class="startDate" value="${nowDate}" autocomplete=off>
					        	</div>
							</div>
						</div>
						<label class="calenderPeriod">~</label>
						<div class="field">
							<div class="ui calendar" id="rangeend">
						    	<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" class="endDate" value="${nowDate}" autocomplete=off>
								 </div>
								</div>
							</div>
						</div>
					</div>
				<!-- <button class="medium ui inverted primary button right" onclick="location.href='selectSalesReport.sr'">등록</button> -->
				<button class="medium ui inverted primary button right searchReport">검색</button>
			</div>
			
			<div id="content_div">
				<div>
					<button id="filter_Btn">필터</button>
					<P><c:out value="${count}"/>건</P>
					<p class="filters" style="font-weight: bold;">영업기간▼ </p>
				</div>
				<hr>
				<c:forEach var="list" items="${ReportStatusList}">
					<div class="resultContents searhContent">
						<input type="hidden" class="searchBizId"  name="bizId" value="<c:out value='${list.bizId}'/>">
						<div><h5>보고일 : <input name="bizDate" style="border:none; pointer-events: none; color:#5D5D5D; text-align:center; width:90px;" value="<c:out value='${list.bizDate}'/>"></h5></div>
						<div>
							<c:if test="${list.category eq 'DAY'}">
								<c:out value='일일 업무 보고'/>
								<input style="border:none; color:#5D5D5D; text-align:center; width:90px;" type="hidden" name="category" value="DAY">
							</c:if>
							<c:if test="${list.category eq 'WEEK' }">
								<c:out value='주간 업무 보고'/>
								<input style="border:none; color:#5D5D5D; text-align:center; width:90px;" type="hidden" name="cateogry" value="WEEK">
							</c:if>
						</div>
						<div>기간 : 
							<input style="border:none; color:#5D5D5D; pointer-events: none; text-align:center; width:90px;" name="bizStart" value="<c:out value='${list.bizStart}'/>"> ~ 
							<input style=" color:#5D5D5D; text-align:center; pointer-events: none; width:90px; border:none;" name="bizEnd" value="<c:out value='${list.bizEnd}'/>"></div>
						<div>
							<input style="border:none; color:#5D5D5D; pointer-events: none; width:60px;" name="empName" value="<c:out value='${list.empName}'/>"><%--  / <c:out value='미제출'/> --%>
						</div>
					</div>
				</c:forEach>
			</div>
			
		</div>
	</div>
	
	<script>
    	$(function(){
    		$(".menu_cnt .main_menu").eq(3).addClass("on");
    		$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(6).addClass("on");
			
			$('#rangestart').calendar({
 				type: 'date',
  				endCalendar: $('#rangeend'),
  				formatter : {
	                   date : function(date, settings) {
	                      var day = date.getDate();
	                      var month = date.getMonth() + 1;
	                      var year = date.getFullYear();
	                      
	                      if(month < 10) {
	                    	  if(day < 10) {
	                    		  return year + '-' + '0' + month + '-' + '0' + day;
	                    	  }else {
	                    		  return year + '-' + '0' + month + '-' + day;
	                    	  }
	                     	 
	                      }else {
	                    	  if(day < 10) {
	                    		  return year + '-' + month + '-' + '0' + day;
	                    	  }else {
	                    		  return year + '-' + month + '-' + day;
	                    	  }
	                      }
	                   }
	               	}
			});
			$('#rangeend').calendar({
  				type: 'date',
  				startCalendar: $('#rangestart'),
  				formatter : {
	                   date : function(date, settings) {
	                      var day = date.getDate();
	                      var month = date.getMonth() + 1;
	                      var year = date.getFullYear();
	                      
	                      if(month < 10) {
	                    	  if(day < 10) {
	                    		  return year + '-' + '0' + month + '-' + '0' + day;
	                    	  }else {
	                    		  return year + '-' + '0' + month + '-' + day;
	                    	  }
	                     	 
	                      }else {
	                    	  if(day < 10) {
	                    		  return year + '-' + month + '-' + '0' + day;
	                    	  }else {
	                    		  return year + '-' + month + '-' + day;
	                    	  }
	                      }
	                   }
	               	}
			});
			
			$(".searhContent").click(function(){
				var bizId = $(this).children(".searchBizId").val();
				location.href="showSalesReportStatusDetail.sr?bizId="+bizId;
			});
			
			$(".searhContent").mouseenter(function(){
				$(this).css("border","#6199B9 solid 1px");
				$(this).children().css("color","#6199B9");
				$(this).children().children().css("color","#6199B9");
				$(this).children().children().children().css("color","#6199B9");
			}).mouseleave(function(){
				$(this).css("border","1px solid #DEE2E8");
				$(this).children().css("color","#000");
				$(this).children().children().css("color","#000");
				$(this).children().children().children().css("color","#000");
			});
			
			$(".searchReport").on('click', function(){
				var startDate = $(".startDate").val();
				var endDate = $(".endDate").val();
				
				if(startDate != "" || endDate != "") {
					var str = "";
					
					$.ajax({
						url:"searchReportList.sr",
						type:"POST",
						data:{
							startDate:startDate,
							endDate:endDate
						},
						success:function(data) {
							if(data.reportList != null) {
								str += '<div><button id="filter_Btn">필터</button><p>'+data.reportListCount+'</p><p class="filters" style="font-weight: bold;">영업기간▼</p>';
								$.each(data.reportList, function(index, ReportAjax){
									str += '<div class="resultContents searhContent"><input type="hidden" class="searchBizId" name="bizId" value="'+ReportAjax.bizId+'">'+
										'<div><h5>보고일:<input name="bizDate" style="border:none; color:"#5D5D5D; text-align:center; width:90px; value="'+ReportAjax.date+'"></h5></div>'+
										'<div>';
										if(ReportAjax.cateogry == "DAY") {
											str += "일일 업무 보고" + '<input style="border:none; color:#5D5D5D; text-align:center; width:90px;" type="hidden;" name="category" value="DAY">';
												
										}else if(reportAjax.cateogry == "WEEK") {
											str += "주간 업무 보고" + '<input style="border:none; color:#5D5D5D; text-align:center; width:90px;" type="hidden;" name="category" value="WEEK">';
										}
										str += '</div><div>기간 : '+'<input style="border:none; color:#5D5D5D; text-align:center; width:90px; nmae="bizStart" value="'+ReportAjax.start+'"> ~ '+
										'<input style="color:#5D5D5D; text-align:center; width:90px;" name="bizEnd" value="'+ReportAjax.end+'"></div>'+
										'<div><input style="border:none; color:#5D5D5D; width:60px;" name="empName" value="'+ReportAjax.empName+'"></div></div>';
								});
								$(".content_div").empty();
								$(".content_div").append(str);
							}
						},
						error:function(status) {
							console.log(status);
						}
					});
			
				}
			});
    	});
    </script>
</body>
</html>