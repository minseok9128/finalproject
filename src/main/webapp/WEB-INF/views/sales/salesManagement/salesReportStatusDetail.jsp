<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style> 
	.refortInsertForm {
		margin-top: 20px;
	}
	
	.plusReportArea {
		margin-top:40px;
		text-align:end;
		width:800px;
	}
	
	.insertTable {
		border-top: 2px solid #27aae1;
		width:800px;
		margin-left:auto;
		margin-right:auto;
		margin-top:20px;
	}
	
	.insertTable > tr {
		border-bottom:1px solid #E1E6EB;
		width:720px;
	}
	
	.th {
		vertical-align:middle;
		background-color:#FBFCFD;
		text-align:center;
		height:60px;
		border-right:1px solid #E1E6EB;
		border-bottom:1px solid #E1E6EB;
		font-weight:bold;
	}
	
	.insertTableTd {
		width:700px;
		margin-right:auto;
		margin-left:auto;
		vertical-align:middle;
		border-bottom:1px solid #E1E6EB;
		padding-left:15px;
	}
	
	.reportSelect {
		width:680px;
		margin-left:auto;
		margin-right:auto;
	}
	
	.reportCalendar {
		width:680px;
		margin-left:10px;
		margin-top:20px;
	}
	
	.calenderPeriod {
		margin-left:20px;
		margin-right:20px;
		margin-top:auto;
		margin-bottom:auto;
	}
	
	.sortA {
		color:black;
	}
	
	.contentArea {
		float:left;
		font-size:18px;
		padding-left:5px;
		paddgin-bottom:margin;
	}
	
	.hr {
	 	width:100%;
	 	height:1px;
	 	background-color:#E1E6EB;
	 	border:none;
	}
	
	.content {
		width:800px;
		background:#F5F9FB;
		text-align:left;
		font-size:13px;
		overflow:hidden;
	}
	.subTitle {
		text-align:left;
		margin:0;
		margin-top: 50px;
	    font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;
	    margin-bottom: 20px;
	}
	
	.subContent {
		text-align:left;
		font-size:18px;
		margin:0;
		margin-bottom:10px;
		display:inline-block;
	}
	.ReportReplyContent {
		width: 680px;
		height: 102px;
		float: left;
		margin-left: 6px;
		border: 1px solid #D1D6DE;
		resize: none;
		margin-top: 3px;
		margin-bottom: 6px;
	}
	
	.replyLi {
		margin-top:10px;
		float:left;
		width:745px;
		text-algin:left;
		margin-top:30px;
	}
	
	.main_menu {
		margin-top:0;
	}
	.ui.grey.basic.button.srch{font-size: 13px;padding: 9px; margin-bottom: 10px;}
	.ui.grey.basic.button.srch:hover{background: #767676;color: white;}
	#propose_report {
		margin-top: 50px;
		font-size: 23px;
		font-weight: bold;
		color: #3D4959;
		margin-bottom: 20px;
	}
	
	#propose_report_Btn, #propose_Request_Btn {
		float: right;
		width: 90px;
		height: 33px;
		font-size: 13px;
		background: white;
		border: 2px solid #87A8D2;
		color: #575B5C;
		font-weight: bold;
	}
	.download_Table {
		width: 100%;
	}
	
	.download_Table td {
		border: none;
		vertical-align: middle;
	}
	
	.download_Table button {
		float: right;
	}
	.ui.grey.basic.button.attachDown{font-size: 13px;padding: 9px; margin-bottom: 10px;}
	.title {
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="width:860px;">
		<div class="title">영업보고관리</div>
		<div class="tb_cnt">
			<div style="padding-top: 21px; padding-bottom: 21px;">
			<div class="refortInsertForm" style="margin-top:20px;">
				<table class="insertTable">
					<tr>
						<th align="left" class="th">보고자</th>
						<td class="insertTableTd">
							<p><c:out value="${reportDetail.empName}"/></p>
						</td>
					</tr>
					<tr>
						<th align="left" class="th">보고일</th>
						<td class="insertTableTd">
							<p><c:out value="${reportDetail.bizDate}"/></p>
						</td>
					</tr>
					<tr>
						<th align="left" class="th" style="height:60px !important">기간</th>
						<td class="insertTableTd">
							<p><c:out value="${reportDetail.bizStart}"/> ~ <c:out value="${reportDetail.bizEnd}"/></p>
						</td>
					</tr>
					<tr>
						<th align="left" class="th">제출일</th>
						<td class="insertTableTd">
							<p><c:out value="${reportDetail.bizSubmit}"/></p>
						</td>
					</tr>
				</table>
				<div align="center" class="plusReportArea">
				<hr class="hr">
					
					<div style="margin-top:30px;">
						<p class="subTitle">영업활동 실적</p>
						<hr class="hr" style="border:0.7px solid #27aae1; margin-top:0;">
						<c:if test="${ResultList ne null}">
							<c:forEach var="resultList" items="${ResultList}" >
								<h5 style="margin-top:10px; margin-bottom:10px; margin-left:10px; text-align:left;">
									<c:out value="${resultList.companyName}"/> / <c:out value="${resultList.clientName}"/>
								</h5>
								<div>
									<table style="margin-bottom:20px; border:1px solid #E1E6EB;">
										<tr>
											<th class="th" style="height:30px;">시간</th>
											<td class="insertTableTd" style="text-align:left;">
												<input style="border:none; color:#5D5D5D; pointer-events: none; width:80px; text-align:center;" value="<c:out value='${resultList.date}'/>" readonly>
												<input style="border:none; color:#5D5D5D; pointer-events: none; width:70px; text-align:center;" value="<c:out value='${resultList.start}'/>" readonly> ~ 
												<input style="border:none; color:#5D5D5D; pointer-events: none; width:70px; text-align:center;" value="<c:out value='${resultList.end}'/>" readonly>
											</td>
										</tr>
										<tr>
											<th class="th" style="height:30px;">영업기회</th>
											<td class="insertTableTd" style="text-align:left;">
												<input style="border:none; width:70%; color:#5D5D5D; pointer-events: none;" value="<c:out value='${resultList.oppName}'/>" readonly> 
											</td> 
										</tr>
										<tr>
											<th class="th" style="height:30px;">활동 분류/목적</th>
											<td class="insertTableTd" style="text-align:left;">
												<c:if test="${resultList.category eq 1}">
													<c:out value="전화"/>
												</c:if>
												<c:if test="${resultList.category eq 2}">
													<c:out value="메일"/>
												</c:if>
												<c:if test="${resultList.category eq 3}">
													<c:out value="방문"/>
												</c:if>
												<c:if test="${resultList.category eq 4}">
													<c:out value="기타"/>
												</c:if> /
												
												<c:if test="${resultList.purpose eq 1}">
													<c:out value="인사"/>
												</c:if>
												<c:if test="${resultList.purpose eq 2}">
													<c:out value="제품소개"/>
												</c:if>
												<c:if test="${resultList.purpose eq 3}">
													<c:out value="데모시연"/>
												</c:if>
												<c:if test="${resultList.purpose eq 4}">
													<c:out value="정보수집"/>
												</c:if>
												<c:if test="${resultList.purpose eq 5}">
													<c:out value="제안"/>
												</c:if>
												<c:if test="${resultList.purpose eq 6}">
													<c:out value="업무협의"/>
												</c:if>
												<c:if test="${resultList.purpose eq 7}">
													<c:out value="협상"/>
												</c:if>
												<c:if test="${resultList.purpose eq 8}">
													<c:out value="계약"/>
												</c:if>
												<c:if test="${resultList.purpose eq 9}">
													<c:out value="기타"/>
												</c:if>
											</td>
										</tr>
										<tr>
											<th class="th" style="height:30px;">계획내용</th>
											<td class="insertTableTd" style="text-align:left;">
												<p><input style="border:none; width:70%; pointer-events: none; color:#5D5D5D;" value="<c:out value='${resultList.planContent}'/>" readonly></p>
											</td>
										</tr>
										<tr>
											<th class="th" style="height:30px;">활동내용</th>
											<td class="insertTableTd" style="text-align:left;">
												<p><input style="border:none; width:70%; pointer-events: none; color:#5D5D5D;" value="<c:out value='${resultList.activityContent}'/>" readonly></p>
											</td>
										</tr>
									</table>
								</div>
							</c:forEach>
						</c:if>
						
						<c:if test="${reportDetail.perContent ne null }">
							<div class="content">
								<p style="margin:10px;">
									<c:if test="${reportDetail.perContent ne null }">
										<c:out value="${reportDetail.perContent}"/>
									</c:if>
								</p>
							</div>
						</c:if>
					</div>
					
					<br>
					
					<div style="margin-top:30px;">
						<p class="subTitle">영업활동 계획</p>
						<hr class="hr" style="border:0.7px solid #27aae1; margin-top:0;">
						<c:if test="${planList ne null}">
							<c:forEach var="planList" items="${planList}" >
								<h5 style="margin-top:10px; margin-bottom:10px; margin-left:10px; text-align:left;">
									<c:out value="${planList.companyName}"/> / <c:out value="${planList.clientName}"/>
								</h5>
								<div>
									<table style="margin-bottom:20px; border:1px solid #E1E6EB;">
										<tr>
											<th class="th" style="height:30px;">시간</th>
											<td class="insertTableTd" style="text-align:left;">
												<input style="border:none; pointer-events: none; color:#5D5D5D; width:80px; text-align:center;" value="<c:out value='${planList.date}'/>" readonly>
												<input style="border:none; pointer-events: none; color:#5D5D5D; width:70px; text-align:center;" value="<c:out value='${planList.start}'/>" readonly> ~ 
												<input style="border:none; pointer-events: none; color:#5D5D5D; width:70px; text-align:center;" value="<c:out value='${planList.end}'/>" readonly>
											</td>
										</tr>
										<tr>
											<th class="th" style="height:30px;">영업기회</th>
											<td class="insertTableTd" style="text-align:left;">
												<input style="border:none; width:70%; pointer-events: none; color:#5D5D5D;" value="<c:out value='${planList.oppName}'/>"> 
											</td> 
										</tr>
										<tr>
											<th class="th" style="height:30px;">활동 분류/목적</th>
											<td class="insertTableTd" style="text-align:left;">
												<c:if test="${planList.category eq 1}">
													<c:out value="전화"/>
												</c:if>
												<c:if test="${planList.category eq 2}">
													<c:out value="메일"/>
												</c:if>
												<c:if test="${planList.category eq 3}">
													<c:out value="방문"/>
												</c:if>
												<c:if test="${planList.category eq 4}">
													<c:out value="기타"/>
												</c:if> /
												
												<c:if test="${planList.purpose eq 1}">
													<c:out value="인사"/>
												</c:if>
												<c:if test="${planList.purpose eq 2}">
													<c:out value="제품소개"/>
												</c:if>
												<c:if test="${planList.purpose eq 3}">
													<c:out value="데모시연"/>
												</c:if>
												<c:if test="${planList.purpose eq 4}">
													<c:out value="정보수집"/>
												</c:if>
												<c:if test="${planList.purpose eq 5}">
													<c:out value="제안"/>
												</c:if>
												<c:if test="${planList.purpose eq 6}">
													<c:out value="업무협의"/>
												</c:if>
												<c:if test="${planList.purpose eq 7}">
													<c:out value="협상"/>
												</c:if>
												<c:if test="${planList.purpose eq 8}">
													<c:out value="계약"/>
												</c:if>
												<c:if test="${planList.purpose eq 9}">
													<c:out value="기타"/>
												</c:if>
											</td>
										</tr>
										<tr>
											<th class="th" style="height:30px;">계획내용</th>
											<td class="insertTableTd" style="text-align:left;">
												<p><input style="border:none; width:70%; pointer-events: none; color:#5D5D5D;" value="<c:out value='${planList.planContent}'/>" readonly></p>
											</td>
										</tr>
										<tr>
											<th class="th" style="height:30px;">활동내용</th>
											<td class="insertTableTd" style="text-align:left;">
												<p><input style="border:none; width:70%; pointer-events: none; color:#5D5D5D;" value="<c:out value='${planList.activityContent}'/>" readonly></p>
											</td>
										</tr>
									</table>
								</div>
							</c:forEach>
						</c:if>
						<c:if test="${reportDetail.planContent ne null }">
							<div class="content">
								<p style="margin:10px;">
									<c:if test="${reportDetail.planContent ne null }">
										<c:out value="${reportDetail.planContent}"/>
									</c:if>
								</p>
							</div>
						</c:if>
					</div>
					<br>
					<c:if test="${reportDetail.content ne null }">
						<div style="margin-top:30px;">
							<p class="subTitle">기타사항</p>
							<hr class="hr" style="border:0.7px solid #27aae1; height:0.7px; background-color:#27aae1; margin-top:0;">
							<div class="content">
								<p style="margin:10px;">
									<c:if test="${reportDetail.content ne null }">
										<c:out value="${reportDetail.content}"/>
									</c:if>
								</p>
							</div>
						</div>
					</c:if>
					
					<c:if test="${reportAttachList ne null}">
						<div class="attc_area" style="margin-top:30px; text-align:left;">
							<h3 style="display:inline-block; text-align:left;">첨부자료</h3>
							<hr class="hr hrBold" style="border:0.7px solid #27aae1; height:0.7px; background-color:#27aae1;">
							<div>
								<table class="download_Table reportTable">
									<c:forEach items="${reportAttachList}" varStatus="status">
										<tr>
											<td>
												<c:out value="${reportAttachList[status.index].originName}" />
												<input type="hidden" value="${reportAttachList[status.index].changeName}" />
												<input type="hidden" value="${reportAttachList[status.index].filePath }" />
											</td>
											<td><button type="button" class="ui grey basic button attachDown">다운로드</button></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						<hr class="hr">
						</div>
					</c:if>
					
					<!-- 댓글 Area 시작 -->
					<div style="margin-top:30px;">
						<div align="left" style="margin-bottom:14px;">
							<h3>의견
							<label>(<c:out value="${replyCount}"/>)</label></h3>
						</div>
						<form action="replyInsert.sr" style="align:left; width:800px;" method="POST">
							<div style="vertical-align: middle; display: inline-block; padding: 0px;">
								<textarea rows="5" cols="80" class="ReportReplyContent" name="content"></textarea>
								<input type="hidden" name="categoryId" class="categoryId" value="${reportDetail.bizId}">
								<button class="ui primary button" style="height:100px;width:100px; font-size:16px; margin-top:3px; maring-botton:6px; margin-left:10px;">작성</button>	
							</div>
						</form>
						<!-- 댓글 출력 시작 -->
						<ul style="margin-left:15px;" class="replyListArea">
							<c:forEach var="reply" items="${replyList}">
								<li class="replyLi">
									<div style="text-align:left; width:97%;">
										<strong style="text-align:left;">
											<input type="hidden" class="delReplyId" value="<c:out value='${reply.replyId}'/>">
											<c:out value="${reply.empName}"/>
											<span>(<c:out value="${reply.enrollDate}"/>)</span>
										</strong>
										<div style="display:inline-block; float:right;">
											<button class="small ui yellow basic button delReply" style="margin-right:3.250px; display:inline-block;">삭제</button>
										</div>
									</div>
									<br>
									<p style="font-size: 12px; text-align:left;"><c:out value="${reply.content}"/></p>
									<hr class="hr" style="width:97%;">
								</li>
							</c:forEach>
						</ul>
					</div>
					
				</div>
				
			</div>
			</div>
		</div>
	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(3).addClass("on");
    		$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(6).addClass("on");
    		
    		var categoryId = $(".categoryId").val();
    		$(".content").val(categoryId);
    		
    		$(".delReply").click(function() {
    			var replyId = $(this).parent().parent().find('.delReplyId').val();
    			
    			location.href="delReply.sr?replyId="+replyId+"&reportId="+categoryId;
    		});
    		
    		$('.reportTable td button').click(function(e) {
    			var originName = $(this).parents('tr').children()[0].innerText;
				var fileName = $(this).parents('tr').children().eq(0).children().eq(0).val();
				var filePath = $(this).parents('tr').children().eq(0).children().eq(1).val();
				var extension = originName.split(".");
				var encodefilePath = encodeURI(filePath);
			
				fileName = fileName + '.' + extension[1];
				location.href = "download.do?fileName=" + fileName + "&filePath=" + encodefilePath + "&originName="
							+ originName;
			});
    		
    		$(".attachDown").mouseenter(function(){
    			$(".attachDown").css("background-color", "#767676");
    			$(".attachDown").css("color", "white");
    		}).mouseout(function(){
    			$(".attachDown").css("background-color", "white");
    			$(".attachDown").css("color", "#767676");
    		});
		});
	</script>
</body>
</html>