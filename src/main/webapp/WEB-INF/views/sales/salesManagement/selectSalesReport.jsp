<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath }/resources/css/common.css">
<style>
	.refortInsertForm {
		margin-top: 20px;
		width:800px;
	} 

	.insertTable {
		border-top: 2px solid #27aae1;
		margin-top: 20px;
		margin-bottom: 20px;
		width:800px;
		border-bottom:1px solid #E1E6EB;
	}
	
	.insertTable > tr {
		border-bottom:1px solid #E1E6EB;
		width:720px;
	}
	
	.insertTable > tr > th {
		vertical-align:middle;
		background-color:#FBFCFD;
	}
	
	.insertTableTd {
		width:700px;
		margin-right:auto;
		margin-left:auto;
		vertical-align:middle;
		border-bottom:1px solid #E1E6EB;
	}
	
	.reportSelect, #reportSelect {
		width:680px;
		margin-left:auto;
		margin-right:auto;
	}
	
	.reportCalendar {
		width:680px;
		margin-left:10px;
		margin-top:20px;
	}

	.calenderPeriod {
		margin-left:20px;
		margin-right:20px;
		margin-top:auto;
		margin-bottom:auto;
	}
	
	.reportBtnArea {
		width:800px;
		margin-top:40px;
	}
	
	.th {
		vertical-align:middle;
		background-color:#FBFCFD;
		text-align:center;
		height:60px;
		border-right:1px solid #E1E6EB;
		border-bottom:1px solid #E1E6EB;
	}
	.reportSelect {
		autocomplete:off;
	}
	.title {
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;		
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<jsp:useBean id="now" class="java.util.Date" />
	<fmt:formatDate value='${now}' pattern='yyyy-MM-dd' var="nowDate"/>

	<div class="main_ctn">
		<div class="title">영업보고관리</div>
		<div class="tb_cnt">
			<div class="refortInsertForm">
				<form action="showInsertSalesReport.sr" method="POST" autocomplete="off">
				<table class="insertTable">
					<tr>
						<th align="left" class="th">보고일</th>
						<td class="insertTableTd">
							<div class="ui calendar reportSelect" id="standard_calendar">
								<div class="ui input left icon">
								  <i class="calendar icon"></i>
								  <input type="text" class="reportSelect" name="bizDate" value="${nowDate}">
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th align="left" class="th" style="height:60px !important">기간</th>
						<td class="insertTableTd">
							<div class="ui form reportCalendar">
								<div class="two fields">
								    <div class="field">
										<div class="ui calendar" id="rangestart">
									    	<div class="ui input left icon">
												<i class="calendar icon"></i>
												<input type="text" class="reportSelect" id="start" name="bizStart" value="${nowDate}">
									 		</div>
										</div>
									</div>
									<label class="calenderPeriod">~</label>
									<div class="field">
										<div class="ui calendar" id="rangeend">
									   		<div class="ui input left icon">
												<i class="calendar icon"></i>
												<input type="text" class="reportSelect" id="end" name="bizEnd" value="${nowDate}">
									  		</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th align="left" class="th">템플릿</th>
						<td class="insertTableTd">
							<select class="ui dropdown category" name="category" id="reportSelect" style="margin-left:10px;">
								<option class=""value="">선택하세요</option>
								<option value="DAY">일일 업무 보고</option>
								<option value="WEEK">주간 업무 보고</option>
							</select>
						</td>
					</tr>
				</table>
				<div align="center" class="reportBtnArea">
					<button class="small ui yellow basic button" type="button" onclick="location.href='showSalesReport.sr'">취소</button>
					<button class="small ui blue basic button" type="submit">다음</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(3).addClass("on");
    		$(".menu_cnt .main_menu").eq(3).find(".sub_menu_list").eq(5).addClass("on");
			
    		$('#standard_calendar').on('click', function(){
    			$('#standard_calendar').calendar({
	                monthFirst : false,
	                type : 'date',
	                formatter : {
	                   date : function(date, settings) {
	                      var day = date.getDate();
	                      var month = date.getMonth() + 1;
	                      var year = date.getFullYear();
	                      
	                      if(month > 10) {
	                     	 return year + '-' + '0' + month + '-' + day;
	                      }else {
	                     	 return year + '-' + month + '-' + day;
	                      }
	                   }
	               	}
	           	});
    		});
	    		
    		var today = new Date();
    		
    		$('#rangestart').on('click', function(){
    			
				$('#rangestart').calendar({
				  type: 'date',
				  endCalendar: $('#rangeend'),
				  formatter : {
	                   date : function(date, settings) {
	                      var day = date.getDate();
	                      var month = date.getMonth() + 1;
	                      var year = date.getFullYear();
	                      
	                      if(month > 10) {
	                     	 return year + '-' + '0' + month + '-' + day;
	                      }else {
	                     	 return year + '-' + month + '-' + day;
	                      }
	                   }
	                }
				});
    		});
    		
    		$('#rangeend').on('click', function(){
				$('#rangeend').calendar({
				  type: 'date',
				  startCalendar: $('#rangestart'),
				  formatter : {
	                  date : function(date, settings) {
	                     var day = date.getDate();
	                     var month = date.getMonth() + 1;
	                     var year = date.getFullYear();
	                     
	                     if(month > 10) {
	                    	 return year + '-' + '0' + month + '-' + day;
	                     }else {
	                    	 return year + '-' + month + '-' + day;
	                     }
	                  }
	               }
				});	
    		});
		});
	</script>
</body>
</html>