<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath }/resources/css/common.css">
<style>
	.btnArea {
		margin: 5px;
		text-align: end;
		width: 800px;
	}
	
	.plusReportArea {
		margin-top: 40px;
		text-align: end;
		width: 800px;
	}
	
	.insertTable {
		border-top: 2px solid #27aae1;
		width: 800px;
		margin-left: auto;
		margin-right: auto;
		margin-top: 10px;
	}
	
	.insertTable>tr {
		border-bottom: 1px solid #E1E6EB;
		width: 720px;
	}
	
	.th {
		vertical-align: middle;
		background-color: #FBFCFD;
		text-align: center;
		height: 60px;
		border-right: 1px solid #E1E6EB;
		border-bottom: 1px solid #E1E6EB;
	}
	
	.insertTableTd {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
		vertical-align: middle;
		border-bottom: 1px solid #E1E6EB;
	}
	
	.subTitle {
		float: left;
		font-size: 18px;
		padding-left: 5px;
		margin: 0;
		vertical-align: middle;
		height: 36px;
	}
	
	.checkbox {
		margin-left: 15px;
	}
	
	.ui.input {
		margin-left: 15px;
	}
	
	.inputWidth {
		width: 700px;
	}
	
	#dis {
		background-color: #EAEAEA;
	}
	
	.hr {
		width: 800px;
		height: 2px;
		background-color: #E1E6EB;
		border: none;
	}
	
	#AttPlus {
		margin-left: 10px;
	}
	.attc_area {
		margin-top: 20px;
	}
	.attc_btn_div input[type="file"] {
		position: absolute;
	    width: 0;
	    height: 0;
	    padding: 0;
	    overflow: hidden;
	    border: 0;
	}
	.main_ctn .content_area .enroll_table_area .act_enroll_table {
		width: 100%;
   		border-top: 2px solid #6199B9;
	}
	.main_ctn .content_area .enroll_table_area .act_enroll_table .enroll_th {
		vertical-align: middle;
	    border-bottom: 1px solid #929FA2;
	    border-right: 1px solid #929FA2;
	}
	.main_ctn .content_area .enroll_table_area.attach_table_area .enroll_th:nth-child(2) {
		border-right: none;
	}
	.main_ctn .content_area .enroll_table_area .act_enroll_table td input[type="file"] {
		position: absolute;
	    width: 0;
	    height: 0;
	    padding: 0;
	    overflow: hidden;
	    border: 0;
	}
	.upload_name {
		border: none!important;
	}
	.enroll_table_area.product_table_area.attach_table_area td button {
		height: 30px;
   		line-height: 1px;
	}
	.title {
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;		
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn">
		<div class="title">영업공지</div>
		<div class="tb_cnt">
			<form action="insertNotice.sn" method="post"
				enctype="multipart/form-data">
				<div class="refortInsertForm">
					<div class="right btnArea">
						<button class="ui inverted red button" type="submit">등록</button>
						<button class="ui inverted primary button" type="button" onclick="location.href='showNoticeList.sn'">취소</button>
					</div>
					<table class="insertTable">
						<tr>
							<th align="left" class="th">제목 *</th>
							<td class="insertTableTd">
								<div class="ui input focus">
									<input type="text" class="inputWidth" id="title" name="notTitle">
								</div>
							</td>
						</tr>
						<tr>
							<th align="left" class="th" style="height: 60px">작성자 *</th>
							<td class="insertTableTd">
								<div class="ui input">
									<input type="text" id="dis" class="inputWidth" name="writer" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th align="left" class="th" style="height: 60px">필독</th>
							<td class="insertTableTd">
								<div class="ui checked checkbox">
									<input type="checkbox" class="essentCheck"> <label></label>
									<input type="hidden" class="check" name="essent" value="N">
								</div>
							</td>
						</tr>
					</table>
					<div align="center" style="margin-top: 40px; margin-left: 5px;">
						<textarea class="content" id="ir1" rows="10" cols="115" style="margin-right:-30px; resize: none;" name="notContent"></textarea>
					</div>

					<script>
                            var count=1;
                            $(function () {
                            	if($('#comCk').prop("checked")){
                            		$('#completeYn').val("Y");
                            		console.log($('#completeYn').val("Y"));
                            	}else{
                            		$('#completeYn').val("N");
                            	}
                            	
                            	
                                $('#file_btn').click(function (e) {
                                		e.preventDefault();
                                		var str = '<tr style="height:30px;"><td><input type="file" style="font-size:15px; display:none;" name="files"><input hidden="true" class="upload_name" id="upload_name'
	                            			+ (count++) + '" readonly style="border:0;"><input style="font-size:15px; border:none;" class="NewfileName"></td><td style="text-align:center;"><button type="button" class="ui basic grey button not" onclick="remove_btn($(this))">삭제</button></td></tr>';
	                            			
                            			$("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
                            			
                            			$(".enroll_th").show();
                            			
                            			$("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
                                    		var cur=$(this).val();
                                    		var arSplitUrl = cur.split('\\');
                                    		var nArLength = arSplitUrl.length;
                                    		var fileName = arSplitUrl[nArLength-1];
                                            
                                            $("#upload_name" + (count-1)).val(fileName);
                                    		console.log(cur);
                                    		$(".NewfileName").val(cur.split('\\')[2]);
                                    	}); 
                                    }); 
                                });
                            
                            function remove_btn(obj){
                                obj.parent().parent().remove();
                            }
                        </script>
					<div class="attc_area" style="margin-top:40px;">
						<!-- 첨부자료 -->
						<h5 style="margin-top: 50px; width:800px; font-size: 18px; font-weight: bold; color: #3D4959; margin-bottom: 20px;">첨부자료
							<button type="button"  style="float:right;" class="ui inverted primary button" id="file_btn">추가</button>
							<input type="file" id="file" hidden="true">
						</h5>
						<hr style="width:800px; border:0.7px solid #27aae1;">
						<div class="enroll_table_area product_table_area attach_table_area">
							<table id="file_table" class="act_enroll_table" style=" width:100%;">
								<colgroup>
									<col style="width: 80%;">
									<col style="width: 20%;">
								</colgroup>
								<tbody>
									<tr>
										<th class="enroll_th" style="display:none; height:30px;">파일명</th>
										<th class="enroll_th" style="display:none; height:30px;">삭제</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(5).addClass("on");
			$(".menu_cnt .main_menu").eq(5).find(".sub_menu_list").eq(6).addClass("on");
			
			var name = ('${sessionScope.loginEmp.empName}');
			var empNo = ('${sessionScope.loginEmp.empNo}');
			
			if(name == "") {
				$("#dis").val("서범수");
			}else {
				$("#dis").val(name);
				$(".writerNo").val(empNo);
			}
			
			$(".essentCheck").on('click', function(){
				if($(this).is(":checked") == true) {
					$(".check").val("Y");
				}else if($(this).is(":checked") == false) {
					$(".check").val("N");
				}
			});

		});
	</script>
</body>
</html>