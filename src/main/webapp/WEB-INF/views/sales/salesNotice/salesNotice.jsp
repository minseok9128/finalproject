<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath }/resources/css/common.css">
<style>
	.notice{
		width: 745px;
		height: 86px;
		left: 244px;
		top: 251px;
		border: 1px solid #E1E6EB;
		border-radius: 5px;
		vertical-align:middle;
		align:center;
		padding:5px;
		cursor:pointer;
	}
	
	.searchInput {
		width:280px;
	}
	
	#firstSearchInput {
		margin-right:10px;
	}
	.title {
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;		
	}
	.searchInputTwo{
		width:320px;
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn">
		<div class="title">영업공지</div>
		<br>
		<c:if test="${ sessionScope.loginEmp.empType ne 3 }">
			<div class="ui input" id="firstSearchInput">
	 			<input type="text" class="searchInput titleContent" placeholder="제목 / 내용">
			</div>
			<div class="ui input">
	  			<input type="text" class="searchInput writer" placeholder="작성자">
			</div> &nbsp; &nbsp;
		</c:if>
		<c:if test="${ sessionScope.loginEmp.empType eq 3 }">
			<div class="ui input" id="firstSearchInput">
	 			<input type="text" class="searchInputTwo titleContent" placeholder="제목 / 내용">
			</div>
			<div class="ui input">
	  			<input type="text" class="searchInputTwo writer" placeholder="작성자">
			</div> &nbsp; &nbsp;
		</c:if>
		<c:if test="${ sessionScope.loginEmp.empType ne 3 }">
			<button class="medium ui inverted blue button" style="float:right; margin-left:5px;" onclick="location.href='showInsertNotice.sn'">작성</button>
		</c:if>
		<button class="medium ui inverted blue button searchBtn" style="float:right;">검색</button>
		<div class="tb_cnt">
			<br>
			<div class="tb_title">
				<p class="titleCount"><c:out value="${count}"/>건</p>
				<hr>
			</div>
		</div>
		<br>
		<br>
		<div class="noticeArea">
			<c:forEach var="Noitce" items="${noticeList}">
				<div class="notice">
					<div class="notice_content">
						<c:if test="${Noitce.essent eq 'Y'}">
							<label style="color: orangered">
								[필독]
							</label>
						</c:if>
						<label style="font-weight:bold; display:inline-block;">
							<c:out value="${Noitce.notTitle}"/>
						</label>
						<input style="float:right; text-align:right; color:#5D5D5D; display:inline-block; border:none;" value="조회수 : <c:out value='${Noitce.count}'/>"> 
					</div>
					<hr>
					<div align="left" style="display: inline-block">
						<p>
							<c:out value="${Noitce.empName}"/>/<c:out value="${Noitce.enrollDate}"/>
						</p>
					</div>
					<div align="right">
						<input type="hidden" class="noticeNum" value="<c:out value='${Noitce.notId}'/>">
						
					</div>
				</div>
				<br>
			</c:forEach>
		</div>
	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(5).addClass("on");
			$(".menu_cnt .main_menu").eq(5).find(".sub_menu_list").eq(6).addClass("on");
			
			$(".notice").click(function(){
				var notNum = $(this).find(".noticeNum").val();
				location.href="showNoticeDetail.sn?notNum="+notNum;
			});
			
			
			$(".searchBtn").on('click', function(){
				var titleContent = $(".titleContent").val();
				var writer = $(".writer").val();
				var str = "";
				
				 if(titleContent != "" || writer != "") {
					$.ajax({
						url:"searchNoticeList.sn",
						type:"POST",
						data:{
							titleContent:titleContent,
							empName:writer
						},
						success:function(data) {
							var count = data.count;
							if(data != null) {
								$(".titleCount").text(count + "건");
								if(data.searchNoticeList != null) {
									$.each(data.searchNoticeList, function(index, ajaxNotice) {
										str += '<div class="notice"><div class="notice_content">';
										if(ajaxNotice.essent == 'Y') {
											str += '<label style="color:orangered">[필독]</label>';
										}
										str += '<label style="font-weight:bold">'+ajaxNotice.notTitle+'<label></div><hr>'+
										'<div align="lef" style="display:inline-block"><p>'+ajaxNotice.empName+'/'+ajaxNotice.enrollDate+
										'</p></div><div align="right"><input type="hidden" class="noticeNum" value="'+ajaxNotice.notId+'">'+
										'<label>'+ajaxNotice.count+'</label></div></div><br>';
									});
									$(".noticeArea").empty();
									$(".noticeArea").append(str);
								}else if(data.containContentNoticeList != null) {
									$.each(data.containContentNoticeList, function(index, ajaxNotice) {
										str += '<div class="notice"><div class="notice_content">';
										if(ajaxNotice.essent == 'Y') {
											str += '<label style="color:orangered">[필독]</label>';
										}
										str += '<label style="font-weight:bold">'+ajaxNotice.notTitle+'<label></div><hr>'+
										'<div align="lef" style="display:inline-block"><p>'+ajaxNotice.empName+'/'+ajaxNotice.enrollDate+
										'</p></div><div align="right"><input type="hidden" class="noticeNum" value="'+ajaxNotice.notId+'">'+
										'<label>'+ajaxNotice.count+'</label></div></div><br>';
									});
									$(".noticeArea").empty();
									$(".noticeArea").append(str);
								}else if(data.containEmpNameNoticeList != null) {
									$.each(data.containEmpNameNoticeList, function(index, ajaxNotice) {
										str += '<div class="notice"><div class="notice_content">';
										if(ajaxNotice.essent == 'Y') {
											str += '<label style="color:orangered">[필독]</label>';
										}
										str += '<label style="font-weight:bold">'+ajaxNotice.notTitle+'<label></div><hr>'+
										'<div align="lef" style="display:inline-block"><p>'+ajaxNotice.empName+'/'+ajaxNotice.enrollDate+
										'</p></div><div align="right"><input type="hidden" class="noticeNum" value="'+ajaxNotice.notId+'">'+
										'<label>'+ajaxNotice.count+'</label></div></div><br>';
									});
									
									$(".noticeArea").empty();
									$(".noticeArea").append(str);
								}
							}else {
								alert("검색한 결과가 없습니다.");
							}
						},
						error:function(status) {
							console.log(status);
						}
					});
				}else {
					alert("정보 입력 후 검색이 가능합니다.");
				}
			});
		});
	</script>
</body>
</html>