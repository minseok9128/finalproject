<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.firstHr {
		background-color: #27AAE1;
	}
	 .hrColor {
		color: rgb(225, 230, 235);
	}
	.headColor {
		background: rgb(251, 252, 253);
	}
	.headLabel {
		font-size: 20px;
		font-weight: bold;
		border:none;
		color:#5D5D5D;
		pointer-events: none;
		background-color:rgb(251, 252, 253);
		width: 500px;
	}
	.subLabel {
		font-size: 13px;
	}
	div>.replyText {
		width: 680px;
		height: 102px;
		float: left;
		margin-left: 6px;
		border: 1px solid #D1D6DE;
		resize: none;
		margin-top: 3px;
		margin-bottom: 6px;
	}
	.massive.ui.blue.basic.button.btnPlus {
		position: absolute;
		height: 83px;
		width: 83px;
		padding: 0;
	}
	ul>.replyLi {
		margin-top: 20px;
	}
	strong {
		font-size: 13px;
		display: inline-block;
		width: 640px;
	}
	span {
		font-size: 11px;
		display: inline-block;
	}
	.hr {
		width: 710px;
		margin: 0px;
	}
	.ui.grey.basic.button.srch{font-size: 13px;padding: 9px; margin-bottom: 10px;}
	.ui.grey.basic.button.srch:hover{background: #767676;color: white;}
	#propose_report {
		margin-top: 50px;
		font-size: 23px;
		font-weight: bold;
		color: #3D4959;
		margin-bottom: 20px;
	}
	#propose_report_Btn, #propose_Request_Btn {
		float: right;
		width: 90px;
		height: 33px;
		font-size: 13px;
		background: white;
		border: 2px solid #87A8D2;
		color: #575B5C;
		font-weight: bold;
	}
	.download_Table {
		width: 100%;
	}
	
	.download_Table td {
		border: none;
		vertical-align: middle;
	}
	
	.download_Table button {
		float: right;
	}
	.ui.grey.basic.button.attachDown{font-size: 13px;padding: 9px; margin-bottom: 10px;}
	.title{
		font-size: 23px;
	    font-weight: bold;
	    color: #3D4959;		
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 여기서 코드작성 -->
	<div class="main_ctn">
		<div class="title">영업공지</div>
		<div class="tb_cnt">
			
			<div style="padding-top: 21px; width:800px; padding-bottom: 21px;">
			
				<form action="showUpdateNotice.sn" method="post" enctype="multipart/form-data">
					<c:forEach var="content" items="${noticeContent}">
						<div align="right">
							<button type="submit" class="medium ui inverted primary button">수정</button>
							<button type="button" class="ui inverted red button" onclick="location.href='showNoticeList.sn'">취소</button>
						</div>
						<div class="headColor" style="margin-top:0;">
							<hr class="hrColor" style="border:0.7px solid #27aae1;">
							<div style="vertical-align:middle;">
								<input type="hidden" class="categoryId" name="categoryId" value="<c:out value='${content.notId}'/>">
								<p style="margin-left: 5px; display:inline-block;"><input class="headLabel" name="notTitle" value="<c:out value='${content.notTitle}'/>"></p>
								<label class="subLabel" style="float:right; font-size:14px; padding-right:10px;">등록일 : <c:out value="${content.enrollDate}"/></label>
								<label style="float:right; font-size:14px; vertical-align:middle;">작성자 : <input style="float:right; border:none; pointer-events: none; color:#5D5D5D; width:50px;" name="empName" value="<c:out value='${content.empName}'/>"></label>
							</div>
							
						</div>
						
						<hr class="hrColor" style="margin-top:0;">
						<div style="padding: 20px;">
							<input type="hidden" name="empNo" value="<c:out value='${content.empNo}'/>">
							<input type="hidden" name="essent" value="<c:out value='${content.essent}'/>">
							<p><textarea style="border:none;resize: none;pointer-events: none;" rows="10" cols="105" readonly><c:out value='${content.notContent}'/></textarea></p>
							
						</div>
						<hr class="hrColor">
						<br><br>
					</c:forEach>
					
					<c:if test="${!empty AttchList}">
						<div class="attc_area" style="margin-top:40px; text-align:left;">
							<h3 style="display:inline-block; text-align:left; font-size:17px;">첨부자료</h3>
							<hr class="hrBold" style="border:0.7px solid #27aae1;">
							<div>
								<table class="download_Table noticeTable">
									<c:forEach items="${AttchList}" varStatus="status">
										<tr>
											<td>
												<c:out value="${AttchList[status.index].originName}" />
												<input type="hidden" value="${AttchList[status.index].changeName}" />
												<input type="hidden" value="${AttchList[status.index].filePath }" />
											</td>
											<td><button type="button" class="small ui grey basic button attachDown">다운로드</button></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</c:if>
					
				</form>
				<br><br>
				<div style="margin-left:15px;">
					<div align="left" style="margin-bottom:14px;">
						<h3>의견
						<label>(<c:out value="${replyCount}"/>)</label></h3>
					</div>
				</div>
				 <form action="replyInsert.sn" method="POST" style="align:left; width:800px;">
					<div style="vertical-align: middle; display: inline-block; padding: 0px;">
						<textarea class="replyText" rows="5" cols="89" name="content" style="resize: none;"></textarea>
						<input type="hidden" class="content" name="categoryId" value="${content.notId}">
						<button class="ui primary button" type="submit" style="height:100px; width:100px;margin-top:3px; font-size:16px; font-weight:bold; margin-bottom:6px; margin-left:10px;">작성</button>
					</div>
				 </form>
				<ul style="margin-left:15px;">
					<c:forEach var="reply" items="${ReplyList}">
						<li class="replyLi">
							<div>
								<strong>
									<input type="hidden" class="delReplyId" value="<c:out value='${reply.replyId}'/>">
									<c:out value="${reply.empName}"/>
									<span>(<c:out value="${reply.enrollDate}"/>)</span>
								</strong>
								<button class="small ui yellow basic button delReply">삭제</button>
							</div>
							<p style="font-size: 12px; display: inline-block;"><c:out value="${reply.content}"/></p>
							<hr class="hr">
						</li>
					</c:forEach>
				</ul>
			</div>
		
		</div>
	</div>
	<script>
    	$(function(){
    		$(".menu_cnt .main_menu").eq(5).addClass("on").addClass("open");
    		
    		var categoryId = $(".categoryId").val();
    		$(".content").val(categoryId);
    		
    		$(".delReply").click(function() {
    			var replyId = $(this).parent().find('.delReplyId').val();
    			
    			location.href="delReply.sn?replyId="+replyId+"&notId="+categoryId;
    		});
    		
    		$(".attachDown").mouseenter(function(){
    			$(".attachDown").css("background-color", "#767676");
    			$(".attachDown").css("color", "white");
    		}).mouseout(function(){
    			$(".attachDown").css("background-color", "white");
    			$(".attachDown").css("color", "#767676");
    		});
    		
    		$('.noticeTable td button').click(function(e) {
				var originName = $(this).parents('tr').children()[0].innerText;
				var fileName = $(this).parents('tr').children().eq(0).children().eq(0).val();
				var filePath = $(this).parents('tr').children().eq(0).children().eq(1).val();
				var extension = originName.split(".");
				var encodefilePath = encodeURI(filePath);
			
				fileName = fileName + '.' + extension[1];
				location.href = "download.do?fileName=" + fileName + "&filePath=" + encodefilePath + "&originName="
							+ originName;
			});
    	});
    	
    </script>
</body>
</html>