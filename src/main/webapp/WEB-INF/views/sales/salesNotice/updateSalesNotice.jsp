<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath }/resources/css/common.css">
<style>
	.btnArea {
		margin: 5px;
		text-align: end;
		width: 800px;
	}
	
	#btnArea {
		margin-top: 10px;
	}
	
	.plusReportArea {
		margin-top: 40px;
		text-align: end;
		width: 800px;
	}
	
	.insertTable {
		border-top: 2px solid #27aae1;
		width: 800px;
		margin-left: auto;
		margin-right: auto;
		margin-top: 10px;
	}
	
	.insertTable>tr {
		border-bottom: 1px solid #E1E6EB;
		width: 720px;
	}
	
	.th {
		vertical-align: middle;
		background-color: #FBFCFD;
		text-align: center;
		height: 60px;
		border-right: 1px solid #E1E6EB;
		border-bottom: 1px solid #E1E6EB;
	}
	
	.insertTableTd {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
		vertical-align: middle;
		border-bottom: 1px solid #E1E6EB;
	}
	
	.subTitle {
		float: left;
		font-size: 18px;
		padding-left: 5px;
		vertical-align: middle;
	}
	
	.checkbox {
		margin-left: 15px;
	}
	
	.ui.input {
		margin-left: 15px;
	}
	
	.inputWidth {
		width: 700px;
	}
	
	#dis {
		background-color: #EAEAEA;
	}
	
	.hr {
		width: 800px;
		height: 2px;
		background-color: #E1E6EB;
		border: none;
	}
	
	#AttpulsBtnArea {
		margin-left: 10px;
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn">
		<div class="title">영업공지</div>
			<div class="tb_cnt">
			
				<form action="updateNoticeContent.sn" method="post" enctype="multipart/form-data">
					<div class="refortInsertForm">
						<div class="right btnArea">
							<button class="medium ui inverted blue button">등록</button>
							<button class="medium ui inverted yellow button" onclick="location.href='showNoticeDetail.sn'">취소</button>
						</div>
						<table class="insertTable">
							<tr>
								<th align="left" class="th">제목 *</th>
								<td class="insertTableTd">
									<div class="ui input focus">
										<input type="text" class="inputWidth" name="notTitle" value="${notice.notTitle}">
									</div>
								</td>
							</tr>
							<tr>
								<th align="left" class="th" style="height: 60px">작성자 *</th>
								<td class="insertTableTd">
									<div class="ui input">
										<input type="text" id="dis" name="empName" class="inputWidth" value="${notice.empName}" readonly>
									</div>
								</td>
							</tr>
							<tr>
								<th align="left" class="th" style="height: 60px">필독</th>
								<td class="insertTableTd">
									<div class="ui checked checkbox">
										<c:if test="${notice.essent eq 'Y'}">
											<input type="checkbox" class="essentCheck" value="Y" checked="checked"> 
										</c:if>
										<c:if test="${notice.essent ne 'Y'}">
											<input type="checkbox" class="essentCheck" value="N"> 
										</c:if>
										<label></label>
										<input type="hidden" class="check" name="essent" value="${notice.essent}">
									</div>
								</td>
							</tr>
						</table>
						<div align="center" style="margin-top: 40px; margin-left: 5px;">
							<input type="hidden" name="notId" value="<c:out value='${notice.notId}'/>">
							<input type="hidden" name="empNo" value="<c:out value='${notice.empNo}'/>">
							<textarea class="content" id="ir1" rows="10" cols="105" style="margin-right:-30px;" name="notContent"><c:out value="${notice.notContent}"/></textarea>
						</div>
						
						<script>
		                	var count=1;
		                    $(function () {
		                    	if($('#comCk').prop("checked")){
		                        	$('#completeYn').val("Y");
		                            }else{
		                            	$('#completeYn').val("N");
		                            }
		                            	
	                                $('#file_btn').click(function (e) {
	                                	e.preventDefault();
	                                	var str = '<tr><td><input type="file" style="font-size:15px;" name="files"><input hidden="true" class="upload_name" id="upload_name'
	                            			+ (count++) + '" readonly></td><td><button type="button" class="ui basic grey button not" onclick="remove_btn($(this))">삭제</button></td></tr>';
	                            			
	                            		$("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
	                            			
	                            		$(".enroll_th").show();
		                            			
	                            		$("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
	                                    	var cur=$(this).val();
	                                    	var arSplitUrl = cur.split('\\');
	                                    	var nArLength = arSplitUrl.length;
	                                    	var fileName = arSplitUrl[nArLength-1];
	                                            
	                                    $("#upload_name" + (count-1)).val(fileName);
	                                    	console.log(cur);
	                                    }); 
	                                 }); 
	                            });
		                            
	                            function remove_btn(obj){
	                                obj.parent().parent().remove();
	                            }
		                    </script>
		                    
							<div class="attc_area" style="margin-top:40px; text-align:left;">
								<!-- 첨부자료 -->
								<h3 style="display:inline-block; text-align:left;">첨부자료</h3>
								<div class="attc_btn_div" style="display:inline-block;">
									<button type="button" class="ui basic grey button atth_btn" id="file_btn">추가</button>
									<input type="file" id="file" hidden="true">
								</div>
								<hr><br>
								<div class="enroll_table_area product_table_area attach_table_area">
									<table id="file_table" class="act_enroll_table">
										<colgroup>
											<col style="width: 80%;">
											<col style="width: 20%;">
										</colgroup>
										<tr>
											<th class="enroll_th" style="display:none; text-align:center;">파일명</th>
											<th class="enroll_th" style="display:none; text-align:center;">삭제</th>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
					
			</div>
	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(5).addClass("on");
			$(".menu_cnt .main_menu").eq(5).find(".sub_menu_list").eq(6).addClass("on");
			
			$(".essentCheck").on('click', function(){
				if($(this).is(":checked") == true) {
					$(".check").val("Y");
				}else if($(this).is(":checked") == false) {
					$(".check").val("N");
				}
			});
		});
	</script>
	
</body>
</html>