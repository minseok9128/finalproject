<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
	
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/hkh/opportunity.css ">
	
 <style>
 .memo2{
	background:#f0f4f8;
	border:1px solid #E1E6EB;
	width:80px;
	text-align:center;
	position:absolute;
	 /* left:0; */
	right:0; 
	z-index:10;
	display:none;
    top: 50px;
    right: 10px;
}

 .resultContents2 {
            width: 720px;
            height: 130px;
            border: 1px solid #DEE2E8;
            margin-bottom: 14px;
            position:relative;
           
        }
        
       #content_div li{
        height:150px;
        
        }
       
        
     
        
 
 </style>
	
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" >
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					영업기회
					<button class = "ui inverted primary button" id="opportunityEnroll_btn" style="float:right;">등록하기</button>
				</h1>
				<input type=text id="costomer_text" placeholder=" 영업기회/고객/고객사"> 
				
				
				<select name="status" id="product_text">
    				<option value="ing">진행중</option>
    				<option value="success">종료(성공)</option>
    				<option value="fail">종료(실패)</option>
    				<option value="defer">연기/보류</option>
				</select>
				<br> 
				
			
				
				
				<br>
				<div class="ui form">
				
				
				
  <div class="two fields">
  
    <div class="field" style="width:325px;  ">
     
      <div class="ui calendar" id="rangestart">
        <div class="ui input left icon">
          <i class="calendar icon"></i>
          <input type="text" placeholder="Start">
        </div>
      </div>
    </div>
    
    <div class="field" style="width:325px; ">
      
      <div class="ui calendar" id="rangeend">
        <div class="ui input left icon">
          <i class="calendar icon"></i>
          <input type="text" placeholder="End">
        </div>
      </div>
    </div>
    <button class = "ui inverted primary button" style="margin-left:2px;  height:37px;">검색</button>
  </div>
</div>
				
				
<script>

$('#rangestart').calendar({
	  type: 'date',
	  endCalendar: $('#rangeend')
	});
	$('#rangeend').calendar({
	  type: 'date',
	  startCalendar: $('#rangestart')
	});

</script>
				
				
			</div>
			

			
			<div id="content_div" style="margin-top:50px;">
			<c:set var="OppCount" value="${ OppCount }"/>
			<c:set var="list" value="${ list }"/>
				<div>
					<button id="filter_Btn">필터</button>
					<P><c:out value="${OppCount}"></c:out>건</P>
					<p class="filters">성공확률</p>
					<p class="filters">진행단계 |</p>
					<p class="filters" style="font-weight: bold;">시작일▼ |</p>
				</div>
				<hr>
				
				
				<c:forEach var="i" items="${list}" varStatus="status">
				
				<div class="resultContents2" >
				
				
					
					<div >
					
					
					<input type="hidden" value="${list[status.index].oppId}">
					<c:choose>
					<c:when test="${list[status.index].process.processStage == 1}">
					<div style="margin-top:15px; display:inline-block; margin-right:10px;"><p style="margin-right:20px;">진행단계</p>  <img src="${ contextPath }/resources/images/pro1.JPG" style="width:350px;"> </div>
						</c:when>
					
					
						<c:when test="${list[status.index].process.processStage == 2}">
					<div style="margin-top:15px; display:inline-block; margin-right:10px;"><p style="margin-right:20px;">진행단계</p>  <img src="${ contextPath }/resources/images/pro2.JPG" style="width:350px;"> </div>
						</c:when>
						
						<c:when test="${list[status.index].process.processStage == 3}">
					<div style="margin-top:15px; display:inline-block; margin-right:10px;"><p style="margin-right:20px;">진행단계</p>  <img src="${ contextPath }/resources/images/pro3.JPG" style="width:350px;"> </div>
						</c:when>
						
						<c:when test="${list[status.index].process.processStage == 4}">
					<div style="margin-top:15px; display:inline-block; margin-right:10px;"><p style="margin-right:20px;">진행단계</p>  <img src="${ contextPath }/resources/images/pro3.JPG" style="width:350px;"> </div>
						</c:when>
						
						<c:when test="${list[status.index].process.processStage == 5}">
					<div style="margin-top:15px; display:inline-block; margin-right:10px;"><p style="margin-right:20px;">진행단계</p>  <img src="${ contextPath }/resources/images/pro4.JPG" style="width:350px;"> </div>
						</c:when>
					</c:choose>
					
					
					
					<div style="display:inline-block; position:absolute; ">
					
					
					
					
					<label style="margin-right:10px; "><p style="margin-top:15px; ">성공확률</p></label>
					<div class="ui progress warning example1"   style="width:170px; float:right; margin-right:50px;" data-percent= "<c:out value="${ list[status.index].process.processSuccess }"/>">
  						<div class="bar" style="background:#f0be78" ><p style="color:white"><c:out value="${ list[status.index].process.processSuccess }"/>%</p></div>
  						
					</div>
					
				
					
					</div>
						
					</div>
					<div style=" margin-left:242px;" class="ui right icon one">
							<i class="ellipsis vertical icon" style="z-index:99999; margin-left:450px; position:absolute; "></i>
							
							
						</div>
						
					
				
					<br>
					<div><p><label style="font-weight:bold;"><font size="4"><c:out value="${ list[status.index].oppName }"/></font></label> | 	<fmt:formatNumber value="${list[status.index].expectProfitPrice }" pattern="#,###" /> 
					| <c:out value="${ list[status.index].company.comName }"/> <c:out value="${ list[status.index].client.clientName }"/></p> </div>
					<br><br>
					<div><p><label style="font-weight:bold;"><font size="3">
					
					<c:choose>
	                   <c:when test="${list[status.index].oppProgress == 1}">
	                  		 [진행중]
	                   </c:when>
	                   
	                    <c:when test="${list[status.index].oppProgress == 2}">
	                  		 [종료(성공)]
	                   </c:when>
	                   
	                    <c:when test="${list[status.index].oppProgress == 3}">
	                  		 [종료(실패)]
	                   </c:when>
	                   
	                    <c:when test="${list[status.index].oppProgress == 4}">
	                  		 [보류및연기]
	                   </c:when>
	                   
                   </c:choose>
					
					
					
					
					</font></label> <c:out value="${ list[status.index].oppStart }"/> ~ <c:out value="${ list[status.index].oppEnd }"/></p></div>
					<input type="hidden" class="scrolling" value="${status.index}">
				<div class="memo2">
							<div>영업활동</div>
							<div>제안</div>
							<div class="estimate">견적</div>
							<div>계약</div>
							<div>매출</div>
						</div>
			
			</div>
			
			</c:forEach>
		
		</div>
		

<script>

var lastScrollTop = 0;
$(function(){
	
	$(".estimate").click(function(){
		event.stopPropagation();
		var oppId = $(this).parent().parent().children().eq(0).children('input').val();
		
		console.log(oppId);
		location.href="showEstimateEnrollment2.sm?oppId="+oppId;
		
	})
	
	
	$(".menu_cnt .main_menu").eq(0).addClass("on").addClass("open");
	
	$(window).scroll(function(){
		var currentScrollTop = $(window).scrollTop();
		
		if(currentScrollTop - lastScrollTop > 0){
				
			if($(window).scrollTop() >= ($(document).height() - $(window).height())){
				var lastCno = $(".scrolling:last").val();
				  var $ul = $('#content_div li' );  
				$.ajax({
					type : 'post',
					url : 'oppMainscroll.so',
					data : {lastCno:lastCno},
					success : function(data) {
						
						
						
						console.log(data);
						for(var i=0; i<data.length; i++) {
							$ul.append();
							
							
						}
						
					}
				})
				
			}
		}
	})
	
	$('.example1').progress();

	$(".memo2 div").mouseenter(function(){
		
		console.log('마우스들어왔음');
		
		$(this).css("color","#6199B9");
		$(this).css("cursor","pointer");
		

		
	}).mouseleave(function(){
		
		$(this).css("color","#3D4959");
		
	}).click(function(){
		
		event.stopPropagation();
		
	})
	


	
	//div 클릭했을시
	$(".resultContents2:not(.ellipsis)").mouseenter(function(){
		
		
		$(this).css("border","#6199B9 solid 1px");
		$(this).children().css("color","#6199B9");
		$(this).children().children("p").css("color","#6199B9");
		$(this).children().children("p").children().children("font").css("color","#6199B9");
		$(this).find(".memo2").css("color","black");
		$(this).css("cursor","pointer");
		
	}).mouseleave(function(){
		
		$(this).css("border","1px solid #DEE2E8");
		$(this).children().css("color","#3D4959");
		$(this).children().children("p").css("color","#3D4959");
		$(this).children().children("p").children().children("font").css("color","#3D4959");
		
	}).click(function(){
		console.log('div클릭');
		event.stopPropagation();
		 var n = $(this).children().children().eq(0).val();
		console.log(n);
		
		location.href="selectOne.so?oppId=" +n;
		
		
	 }) 
	

 //아이콘 클릭했을시
	 $(".ellipsis.vertical.icon:not(.resultContents2)").click(function(){
			 
			console.log('아이콘클릭');
			
			if($(this).hasClass("memoOn")){
				event.stopPropagation();
				$(this).removeClass("memoOn");
				
				$(this).parent().parent().children(".memo2").hide();
			} else{
				event.stopPropagation();
				$(this).addClass("memoOn");
				
				$(this).parent().parent().children(".memo2").show();
				
			} 

		}); 
		
	
$('html').click(function(e){ 
	if(!$(e.target).hasClass("ellipsis")) { 
		event.stopPropagation();
		$(".memo2").hide();
		
		} 
	});


	

$('#opportunityEnroll_btn').click(function() {
	location.href = "showOpprotunityEnroll.so";
})


})



</script>
		
	</div>
		</div>
	

</body>
</html>