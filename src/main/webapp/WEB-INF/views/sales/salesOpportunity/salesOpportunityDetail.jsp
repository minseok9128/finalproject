<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.sm.salesOpportunity.model.vo.*"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   
   

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 견적</title>
<link rel="stylesheet"
   href="${ contextPath }/resources/css/hkh/opportunityEnroll.css ">
   <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
   
   <style>
   .download_Table {
	width: 100%;
}
.download_Table td {
	border: none;
	vertical-align: middle;
}

.download_Table button {
	float: right;
}
   
   
   #table td:nth-child(2) {
   
   padding-left:10px;
   }
   
   #textarea {
             width: 660px;
             height: 102px;
             float: left;
             margin-left: 6px;
             border: 1px solid #D1D6DE;
             resize: none;
             margin-top: 3px;
             margin-bottom: 6px;
       }
   
  .li {
  margin-bottom:5px;
  
  }
  
  .ui.grey.basic.button.srch{font-size: 13px;padding: 9px; margin-bottom: 10px;}
.ui.grey.basic.button.srch:hover{background: #767676;color: white;}
  
  
   </style>
   
   
</head>
<body>
   <jsp:include page="../../common/menubar.jsp" />
   <div class="main_ctn" style="background: white;">
      <div class="title"></div>
      <div class="tb_cnt" >
         <div class="tb_title"></div>
         <div id="header_div" style="margin-bottom:10px;">
            <div style="display:inline-block;"><h1>
              영업기회 상세조회
              
     
             
            </h1>
            </div>
            
             
             
           
               <br>
               <br>
               <h3><c:out value="${oppOne.oppName}"/> <input type="button" id="update_Btn" class = "ui inverted primary button" style="  margin-bottom: 17px;  width:70px; height:35px; float:right;" value="수정"></h3>
         </div>
         
        
         <div id="content_div">
            <table id="table">
               
               <tr  > 
                  <td class="td">고객사</td>
                  <td style="text-align:left">
                    	<c:out value="${oppOne.company.comName}"/>
                  </td>
               </tr>
               <tr >
                  <td class="td">고객</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.client.clientName}"/>
                  </td>
               </tr>
               <tr >
                  <td class="td">진행상태</td>
                   <td style="text-align:left">
                   
                   <c:choose>
	                   <c:when test="${oppOne.oppProgress == 1}">
	                  		 진행중
	                   </c:when>
	                   
	                    <c:when test="${oppOne.oppProgress == 2}">
	                  		 종료(성공)
	                   </c:when>
	                   
	                    <c:when test="${oppOne.oppProgress == 3}">
	                  		 종료(실패)
	                   </c:when>
	                   
	                    <c:when test="${oppOne.oppProgress == 4}">
	                  		 보류및연기
	                   </c:when>
	                   
                   </c:choose>
                   
                  </td>
               </tr>
               
               <tr >
                  <td class="td">예상매출</td>
                   <td style="text-align:left">
                    	<fmt:formatNumber value="${oppOne.expectRev }" pattern="#,###" />
                    		
                  </td>
               </tr>
               
               <tr >
                  <td class="td">예상이익률</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.expectProfitRate}"/>%
                  </td>
               </tr>
               
              <tr >
                  <td class="td">예상이익금액</td>
                   <td style="text-align:left">
                    <fmt:formatNumber value="${oppOne.expectProfitPrice}" pattern="#,###" />
                    
                  </td>
               </tr>
               
               <tr >
                  <td class="td">영업기간</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.oppStart}"/> ~ <c:out value="${oppOne.oppEnd}"/>
                  </td>
               </tr>
               
               <tr >
                  <td class="td">매출구분</td>
                   <td style="text-align:left">
                    
                    		상품매출
                  </td>
               </tr>
                  <tr>
                  <td class="td">프로세스</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.process.process}"/>
                  </td>
               </tr>
               
               
               <tr >
                  <td class="td">진행단계</td>
                   <td style="text-align:left">
                    
                    		  <c:choose>
	                   <c:when test="${oppOne.process.processStage == 1}">
	                  		 인지
	                   </c:when>
	                   
	                    <c:when test="${oppOne.process.processStage == 2}">
	                  		 제안
	                   </c:when>
	                   
	                    <c:when test="${oppOne.process.processStage == 3}">
	                  		 견적
	                   </c:when>
	                   
	                    <c:when test="${oppOne.process.processStage == 5}">
	                  		 계약
	                   </c:when>
	                   
	                   
                   </c:choose>
                  </td>
               </tr>
                  
          <tr>
                  <td class="td">성공확률</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.process.processSuccess}"/>%
                  </td>
               </tr>
               <tr >
                  <td class="td">인지경로</td>
                   <td style="text-align:left">
                    
                    		  <c:choose>
	                   <c:when test="${oppOne.recognitionPath == 1}">
	                  		 신문
	                   </c:when>
	                   
	                    <c:when test="${oppOne.recognitionPath == 2}">
	                  		 인터넷검색
	                   </c:when>
	                   
	                    <c:when test="${oppOne.recognitionPath == 3}">
	                  		 지인소개
	                   </c:when>
	                   
	                    <c:when test="${oppOne.recognitionPath == 4}">
	                  		 기존고객
	                   </c:when>
	                   
	                    <c:when test="${oppOne.recognitionPath == 5}">
	                  		 기타
	                   </c:when>
	                   
	                 
	                   
	                   </c:choose>
                  </td>
               </tr>
               <tr >
                  <td class="td">담당자</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.employee.empName}"/>
                  </td>
               </tr>
               <tr>
                  <td class="td">비고</td>
                   <td style="text-align:left">
                    
                    	<c:out value="${oppOne.remark}"/>
                  </td>
               </tr>
               <tr >
                  <td class="td">대표자명</td>
                   <td style="text-align:left">
                    
                    		<c:out value="${oppOne.agentName}"/>
                  </td>
               </tr>
            </table>
            
          <!--  <div style="margin-top:30px;">
           <h3>의견(<label>0</label>)</h3>
           
           <textarea id="textarea" ></textarea> <button class="ui primary button" style="height:80px;width:80px;margin-top:15px; margin-left:10px;">의견</button>
           
           </div> -->
          
           <div class="ui styled fluid accordion" style="margin-top:30px; margin-left:7px;">
			  <div class="title">
			    <i class="dropdown icon"></i>
			    	<c:set var="rcCount" value="${rcCount}"/>
			   <font size="3" color="black">연관고객(<c:out value="${rcCount}"/>)</font>
			  </div>
			  <c:set var="rcList" value="${rcList}"/>
			  
			  <div class="content">
			<c:forEach var="i" items="${rcList}" varStatus="status">
			
			  <div class="content active" style="margin-left:30px;">
			     
			  <ul>
			  	 <li type="circle" class="li">고객명 : <c:out value="${rcList[status.index].client.clientName}"/> </li>
			  	<li type="circle" class="li">고객사명 : <c:out value="${rcList[status.index].company.comName}"/></li>
			  	<li type="circle" class="li">휴대번호 : <c:out value="${rcList[status.index].client.phone}"/></li>
			  	<li type="circle" class="li">유선번호 : <c:out value="${rcList[status.index].client.tel}"/></li>
			  	<li type="circle" class="li">담당자 : <c:out value="${rcList[status.index].employee.empName}"/></li> 
			  </ul>
			  <hr>
			  </div>
			  
			  </c:forEach>
			 </div>
			 </div>
			 <div class="ui styled fluid accordion" style="margin-top:10px; margin-left:7px;">
			  <div class="title">
			    <i class="dropdown icon"></i>
			    <c:set var="sCount" value="${sCount}"/>
			    <font size="3" color="black">지원인력(<c:out value="${sCount}"/>)</font>
			  </div>
			   <c:set var="sList" value="${sList}"/>
			  <div class="content">
			<c:forEach var="i" items="${sList}" varStatus="status">
			
			  <div class="content active" style="margin-left:30px;">
			     
			  <ul>
			  	 <li type="circle" class="li">부서명 : <c:out value="${sList[status.index].employee.deptName}"/></li>
			  	 <li type="circle" class="li">담당자 : <c:out value="${sList[status.index].employee.empName}"/></li>
			  	
			  	
			  	<c:choose>
	                   <c:when test="${sList[status.index].category == 1}">
	                  		<li type="circle" class="li">역할구분 : 영업</li>
	                   </c:when>
	                   
	                   <c:when test="${sList[status.index].category == 2}">
	                  		<li type="circle" class="li">역할구분 : 기술지원</li>
	                   </c:when>
	                   
	                   <c:when test="${sList[status.index].category == 3}">
	                  		<li type="circle" class="li">역할구분 : 기타</li>
	                   </c:when>
	                   
	                   
	             </c:choose>
			  	
			  	<li type="circle" class="li">등록자 : <c:out value="${sList[status.index].employee2.empName}"/></li> 
			  </ul>
			  <hr>
			  </div>
			  
			  </c:forEach>
			 </div>
			  </div>
			  <div class="ui styled fluid accordion" style="margin-top:10px; margin-left:7px;">
			  <div class="title">
			    <i class="dropdown icon"></i>
			    <c:set var="pdCount" value="${pdCount}"/>
			    <font size="3" color="black">영업기회제품(<c:out value="${pdCount}"/>)</font>
			  </div>
			  <c:set var="pdList" value="${pdList}"/>
			  
			  <div class="content">
			<c:forEach var="i" items="${pdList}" varStatus="status">
			
			  <div class="content active" style="margin-left:30px;">
			     
			  <ul>
			  	 <li type="circle" class="li">제품명 : <c:out value="${pdList[status.index].productAjax.pdtName}"/></li>
			  	  <li type="circle" class="li">단가 : <fmt:formatNumber value="${pdList[status.index].detailPrice}" pattern="#,###" />원</li>
			  	 <li type="circle" class="li">할인율 : <c:out value="${pdList[status.index].discountRate}"/>%</li>
			  	 <li type="circle" class="li">수량 : <c:out value="${pdList[status.index].quantity}"/>개</li>
			  	 <li type="circle" class="li">합계 :  <fmt:formatNumber value="${pdList[status.index].totalPrice}" pattern="#,###" />원</li>
				 	<li type="circle" class="li">제품설명 : <c:out value="${pdList[status.index].productAjax.info}"/></li>		  
			  </ul>
			  <hr>
			  </div>
			  
			 
			  
			  
			  </c:forEach>
			 </div>
			</div>
			
			<div style="margin-top:30px;">
           <h3>첨부자료</h3>
           <table class="download_Table" id="reportTable">

						<c:forEach items="${oppoAtt}" varStatus="status">
							<tr>
								<td><c:out value="${oppoAtt[status.index].originName}" />
									<input type="hidden"
									value="${oppoAtt[status.index].changeName}" /> <input
									type="hidden" value="${oppoAtt[status.index].filePath }" />
								</td>
								<td><button class="ui grey basic button srch">다운로드</button></td>
							</tr>
						</c:forEach>


					</table>
          
           
           </div>
           
           
          <input type="hidden" id="hiddenOppId" value="<c:out value="${oppOne.oppId}"/>">

<script>

$('.ui.accordion').accordion();

$('body')
.toast({
  displayTime: 0,
  position: 'bottom right',
  message: `이력남기는곳`,
  closeIcon: true,
  transition: {
      showMethod   : 'zoom',
      showDuration : 1000,
      hideMethod   : 'fade',
      hideDuration : 1000
    }
});

$('#reportTable td button')
.click(
		function(e) {
			var originName = $(this).parents('tr')
					.children()[0].innerText;
			var fileName = $(this).parents('tr').children()
					.eq(0).children().eq(0).val();
			var filePath = $(this).parents('tr').children()
					.eq(0).children().eq(1).val();
			var extension = originName.split(".");
			var encodefilePath = encodeURI(filePath);
			fileName = fileName + '.' + extension[1];
			location.href = "download.do?fileName="
					+ fileName + "&filePath="
					+ encodefilePath + "&originName="
					+ originName;
		})
		
		$(function(){
			$(".menu_cnt .main_menu").eq(0).addClass("on").addClass("open");
			
			$("#update_Btn").click(function(){
				
				var oppId= $("#hiddenOppId").val();
				
				location.href="SelectOneUpdate.so?oppId=" + oppId;
				
				
				
			})
		})
</script>
         
         
         
            
         </div>
      </div>
      </main>
   </div>
   </div>
   </div>
   
   <br>  <br>  <br>  <br>  <br>  <br>  <br>
</body>
<script type="text/javascript">
      $('#standard_calendar').calendar({type : 'date'});
      $('#standard_calendar2').calendar({type : 'date'});
   </script>
</html>