<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    

    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 영업기회 등록</title>
<link rel="stylesheet"
   href="${ contextPath }/resources/css/hkh/opportunityEnroll.css ">

   <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
   
   <style>
   
       
       .estumateTable{width: 100%; border-top: 1px solid #27AAE1;}
.estumateTable th{border-bottom: 1px solid #E5E5E5; border-right: 1px solid #E5E5E5; vertical-align: middle;background: #EfEfEf;}
.estumateTable th:nth-last-child(1){border-right: none;}
.estumateTable tr:nth-child(1) th:nth-child(1){width: 35%; height: 33px;}
.estumateTable tr:nth-child(1) th:nth-child(2){width: 20%; height: 33px;}
.estumateTable tr:nth-child(1) th:nth-child(3){width: 17.5%; height: 33px;}
.estumateTable tr:nth-child(2) th:nth-child(4){width: 35%; height: 33px;}
.estumateTable tr:nth-child(2) th:nth-child(1){width: 17.5%; height: 33px;}
   
   
   #file_table {
	width: 100%;
   
	
	
	}
	
	  #managerTable {
             width: 100%;
             border-top: 2px solid #27AAE1;
             text-align: center;
       }
       
       #managerTable td {
             vertical-align: middle;
             border-bottom: 1px solid #E5E5E5;
       }
       #managerTable td:nth-child(1) {
             width: 186px;
             height: 38px;
           /*   border-right: 1px solid #E5E5E5; */
       }
       
.estumateproduct{margin-top: 21px; border-bottom: 1px solid #27AAE1;}
.estumateproduct h1{display: inline-block; font-size: 24px;}
.estumateproduct button {float: right;}
	#propose_report2>#product_add, #product_del {
	float: right;
	margin-left: 6px;
}
#propose_report2{
	width: 200px;
}
#propose_report2 {
	margin-top: 50px;
	font-size: 23px;
	font-weight: bold;
	color: #3D4959;
	margin-bottom: 20px;
}

#propose_report_Btn, #propose_Request_Btn {
	float: right;
	width: 90px;
	height: 33px;
	font-size: 13px;
	background: white;
	border: 2px solid #87A8D2;
	color: #575B5C;
	font-weight: bold;
}

.searchBtn{width: 67px;}
.ui.grey.basic.button.srch{font-size: 13px;padding: 9px; margin-bottom: 10px;}
.ui.grey.basic.button.srch:hover{background: #767676;color: white;}
	
	
   
   </style>
   
</head>
<body>
   <jsp:include page="../../common/menubar.jsp" />
   <jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/client.jsp"></jsp:include>
<jsp:include page="../../common/modal/clientAll.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<jsp:include page="../../common/modal/managerAll.jsp"></jsp:include>
<jsp:include page="../../common/modal/product4.jsp"></jsp:include>
   <div class="main_ctn" >
      <div class="title"></div>
      <div class="tb_cnt" >
         <div class="tb_title"></div>
         <form action="insertOpportunityEnrollment.so" method="post" enctype="multipart/form-data" id="insertOppForm">
         <div id="header_div">
            <h1>
              영업기회등록
             
                 <input type="button" class="ui inverted red button" id="resetBtn" style="width: 70px; height:35px; float:right;     margin-left: 6px;" value="취소">
						<input type="button" class="ui inverted primary button" id="submitBtn" style="width: 70px; height:35px; float:right;"value="작성">
               
            </h1>
               <br>
               <br>
         </div>
         <div id="content_div">
            <table id="table">
               <tr >
                  <td class="td">영업기회명<b class="point">*</b></td>
                  <td>
                     <input type="text" id="oppName" name="oppName" class="common_Text1">
                  </td>
               </tr>
            <tr>
					<td class="td">고객사</td>
					<td style="padding-top: 10px;"><input type="text" id="company" class="common_Text2" readonly> 
					<input type="hidden" id="company_hidden" value="0" name="companyId">
					<button class="ui grey basic button srch searchBtn" id="company_Modal">검색</button></td>
			</tr>
            <tr>
					<td class="td">고객<b class="point">*</b></td>
					<td style="padding-top: 10px;"><input type="text" id="client" class="common_Text2">
					<input type="hidden" id="client_hidden" value="0" name="clientId">
					<button class="ui grey basic button srch searchBtn" id="client_Modal">검색</button></td>
			</tr>
               <tr >
                  <td class="td">진행상태</td>
                  <td>
                     <select name="oppProgress" id="oppProgress" style="width:600px;  height: 33px;">
    				<option value="1">진행중</option>
    				<option value="2">종료(성공)</option>
    				<option value="3">종료(실패)</option>
    				<option value="4">보류및연기</option>
    				
    			
				</select>
                     
                  </td>
               </tr>
               
               <tr >
                  <td class="td">예상매출</td>
                  <td>
                  
                     <input type="text" id="expectRev" name="expectRev" class="common_Text1" >
                  </td>
               </tr>
               
               <tr>
                  <td class="td">예상이익률(%)</td>
                  <td>
                     <input type="text" id="expectProfitRate" name="expectProfitRate" class="common_Text1" style="width:200px;" >
                  </td>
                  
               </tr>
               
               <tr>
                  <td class="td">예상이익금액</td>
                  <td>
                     <input type="text" id="expectProfitPrice" name="expectProfitPrice" class="common_Text1" style="background:#EAEAEA" readonly>
                  </td>
               </tr>
               
               <tr >
                  <td class="td">매출구분</td>
                  <td>
                     <select name="category" id="category" style="width:600px;  height: 33px;">
    				<option value="PRODUCT">상품매출</option>
    				
    				
    				
    			
				</select>
                     
                  </td>
               </tr>
               
               <tr >
                  <td class="td">프로세스<b class="point">*</b></td>
                  <td>
                     <input type="text" id="processId" class="common_Text1" value="신규 영업 프로세스" style="background:#EAEAEA; width:65%; text-align:center" disabled>
                     
                     <select name="processStage" id="processStage" style="width:200px;  height: 33px;" onchange="OnChange(this.options[this.selectedIndex].text)" >
    				<option value=1>인지</option>
    				<option value=2>제안</option>
    				<option value=3>견적</option>
    				
    				<option value=4>계약</option>
    			
				</select>
                  </td>
                  </tr>
                
                <tr >
                  <td class="td">성공확률(%)</td>
                  <td>
                     <input type="text" id="processSuccess" name="processSuccess" class="common_Text1" style="width:200px;" value="10">
                  </td>
                  
               </tr>
               
               <tr >
                  <td class="td">인지경로</td>
                  <td>
                     <select name="recognitionPath" id="recognitionPath" style="width:600px;  height: 33px;">
    				<option value=1>신문</option>
    				<option value=2>인터넷검색</option>
    				<option value=3>지인소개</option>
    				<option value=4>기존고객</option>
    				<option value=5>기타</option>
    				
    				
    			
				</select>
                     
                  </td>
               </tr>
                  
          
               
               <tr >
                  <td class="td">영업시작일</td>
                  <td id="start" >
                   <div class="two fields" >
                      <div class="field" >
                        <div class="ui calendar" id="date_calendar" >
                          <div class="ui input right icon" id="standard_calendar" >
                            <i class="calendar icon"></i>
                            <input type="text" readonly style="background:#EAEAEA;" id="oppStart" name="oppStart" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
               </tr>
               <tr >
                  <td class="td">영업종료일</td>
                  <td class="end">
                   <div class="two fields">
                      <div class="field">
                        <div class="ui calendar" id="rangestart">
                          <div class="ui input right icon" id="standard_calendar2">
                            <i class="calendar icon"></i>
                            <input type="text" style="background:#EAEAEA;" id="oppEnd" name="oppEnd" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
               </tr>
               <tr>
							<td class="td">담당자<b class="point">*</b></td>
							<td ><input type="text" class="common_Text2" id="managerText" value="${sessionScope.loginEmp.empName }" style="width:600px;  height: 33px;" readonly> 
								<input type="hidden" style="width:600px;  height: 33px;" name="empNo" value="${sessionScope.loginEmp.empNo }"></td>
						</tr>
               <tr >
                  <td class="td">비고</td>
                  <td>
                     <textarea id="contents" name="remark"></textarea>
                  </td>
               </tr>
               <tr >
                  <td class="td">대표자명<b class="point">*</b></td>
                  <td>
                     <input type="text" id="agentName" name="agentName" class="common_Text1">
                  </td>
               </tr>
            </table>
            
            <h1 id="propose_report">연관고객 <input type="hidden" id="client_hidden2" value="0" name="clientId">
					<input type="button" class="ui inverted primary button" id="clientAll_Modal" value="검색" style="float:right;"></h1>
					<input type="hidden" id="company_hidden2" value="0" name="companyId">
             <table id="table2" style="text-align:center">
               <tr class="td" style="background:#EfEfEf;">
                  <td colspan="2" style="width:40%; border-right: 1px solid #E5E5E5; ">고객</td>
                   
                    <td style="border-right: 1px solid #E5E5E5; ">유선번호</td>
                     <td style="border-right: 1px solid #E5E5E5; ">휴대번호</td>
                     <td rowspan="2" style="border-right: 1px solid #E5E5E5; ">삭제</td>
                  
               </tr>
               
               
               <tr style="background:#EfEfEf;">
               
               <td colspan="2" style="border-right: 1px solid #E5E5E5; ">고객사</td>
                <td colspan="2" style=" border-right: 1px solid #E5E5E5;  ">담당자</td>
                
               </tr>
               
               
               
               </table>
           
            <div class="estumateproduct">
            <h1 id="propose_report2">영업기회제품</h1>
            <button class ="ui inverted primary button product_add" id="opporBtn" value="estBtn" style="margin-top:50px;">제품 등록</button>
            </div>
            <table class="estumateTable" id="contract_Product_Table"  style="text-align:center">
               <thead  >
               <tr >
							<th colspan="2">제품명</th>
							<th>단가</th>
							<th>할인</th>
							<th>합계</th>
							<th rowspan="2">삭제</th>
						</tr>
						<tr>
							<th>포장수량</th>
							<th>규격</th>
							<th>수량</th>
							<th colspan="2">제안금액</th>
						</tr>
               </thead>
            
               <tbody>
							
						</tbody>
						<tfoot>
						
						
						</tfoot>
               
               </table>
            
             
            <h1 id="propose_report">지원인력<input type="hidden" id="manager_hidden2"
								value="0" name="empNo">
								<input type="button" class="ui inverted primary button" id="manager2" style="float:right;" value="인력추가"></h1>
            <table id="managerTable" style="text-align:center">
               <tr class="td" style="background:#EfEfEf;">
                  <td colspan="2" style=" border-right: 1px solid #E5E5E5; width:35%; ">담당자</td>
                   
                    <td style="border-right: 1px solid #E5E5E5;">역할구분</td>
                     <td style="border-right: 1px solid #E5E5E5;">등록자</td>
                     <td style="border-right: 1px solid #E5E5E5;">삭제</td>
                    
          
                  
               </tr>
               
              
               
               </table>
            
            
             <h1 id="propose_report">첨부자료<button id="productFile_btn"  class="ui inverted primary button" style="float:right;">업로드</button></h1>
             <hr>
             <table id="file_table">
             
               
               </table>
         </div>
         </form>
      </div>
      </main>
   </div>
   </div>
   </div>
   
   <br>  <br>  <br>  <br>  <br>  <br>  <br>
</body>
<script >



	
	$("#expectProfitRate").keyup(function(){
		
		  var expectRev = document.getElementById("expectRev").value;
		  var expectProfitRate = document.getElementById("expectProfitRate").value;
		 
		
		  
		  if(expectRev && expectProfitRate){
			  document.getElementById('expectProfitPrice').value =  Math.round(expectRev * (expectProfitRate / 100));
		  }
		
	})
	
	$("#expectRev").keyup(function(){
		
		  var expectRev = document.getElementById("expectRev").value;
		  var expectProfitRate = document.getElementById("expectProfitRate").value;
		 
		
		  
		  if(expectRev && expectProfitRate){
			  document.getElementById('expectProfitPrice').value =  Math.round(expectRev * (expectProfitRate / 100));
		  }
		
	})
	
	




	
 

</script>
<script type="text/javascript">




$('#standard_calendar').calendar({
	type : 'date',
	formatter: {
		  date : function (date, settings) {
		        if (!date) return '';
		        var day = date.getDate();
		        var month = date.getMonth() + 1;
		        var year = date.getFullYear();
		        return year + '-' + month + '-' + day;
		        /* return day + '/' + month + '/' + year; */
		      }
		},
});
$('#standard_calendar2').calendar({
	type : 'date',
	formatter: {
		  date : function (date, settings) {
		        if (!date) return '';
		        var day = date.getDate();
		        var month = date.getMonth() + 1;
		        var year = date.getFullYear();
		        return year + '-' + month + '-' + day;
		        /* return day + '/' + month + '/' + year; */
		      }
		},
});
      

      
      
      
      function OnChange() {
  		
    	  var target = document.getElementById("processStage");
          var gubun = target.options[target.selectedIndex].text;
        
		  
		  if(gubun == "인지") {
			
			  document.getElementById("processSuccess").value =10;
			  
		  }else if(gubun == "제안") {
			
			  document.getElementById("processSuccess").value =30;
		  }else if(gubun == "견적") {
			 
			  document.getElementById("processSuccess").value =50;
		  }else if(gubun == "계약") {
			  
			  document.getElementById("processSuccess").value =100;
		  }
	
		  
		  
		 
	  } 
      
     
    	  
    	
			
			
		$('#VATCheckBox').change(function(){
				var tablelength = $("#contract_Product_Table").children()[1].rows.length;
				
				if(tablelength != 0){
					//수량
					 var price = $('#contract_Product_Table tfoot').children().eq(0).children().eq(1).text();
					//전체 공급가액
					var totalValueOfSupply = $('#contract_Product_Table tfoot').children().eq(0).children().eq(3).text();
					//세액
					var Tex = changenTex(totalValueOfSupply);
					var TexVAT = changenTexVAT(totalValueOfSupply);
					//합계
					var total = total(Tex,totalValueOfSupply);
					
					if($('#VATCheckBox').is(":checked") == false){
						$('#price').val(comma(price));
						$('#totalValueOfSupply').val(comma(totalValueOfSupply));
						$('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
						$('#tex').val(comma(Tex));
						$('#total').val(comma(total));
				 	} else {
						 $('#total').val(comma(totalValueOfSupply));
						 $('#tex').val(comma(TexVAT));
						 var totalValueOfSupply = changeNumber(totalValueOfSupply)-changeNumber(TexVAT)
						 $('#totalValueOfSupply').val(comma(totalValueOfSupply));
						 $('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
				 }
					
				} 
				function total(a, b){
					//세금
					var Tex = changeNumber(a);
					//전체 공급가액
					var totalValueOfSupply = changeNumber(b);
					
					return totalValueOfSupply+Tex
				}
			})	
			
			
			// 견적 품목 추가 되면 위에 수량등에 대한 정보를 입력하는 함수
			function changeTable() {
				var tablelength = $("#contract_Product_Table").children()[1].rows.length;
				//수량
				 var price = $('#contract_Product_Table tfoot').children().eq(0).children().eq(1).text();
				//전체 공급가액
				var totalValueOfSupply = $('#contract_Product_Table tfoot').children().eq(0).children().eq(3).text();
				//세액
				var Tex = changenTex(totalValueOfSupply);
				var TexVAT = changenTexVAT(totalValueOfSupply);
				//합계
				var total = total(Tex,totalValueOfSupply);
				
				if(tablelength != 0){
					 if($('#VATCheckBox').is(":checked") == false){
							$('#price').val(comma(price));
							$('#totalValueOfSupply').val(comma(totalValueOfSupply));
							$('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
							$('#tex').val(comma(Tex));
							$('#total').val(comma(total));
					 } else {
							 $('#total').val(comma(totalValueOfSupply));
							 $('#tex').val(comma(TexVAT));
							 var totalValueOfSupply = changeNumber(totalValueOfSupply)-changeNumber(TexVAT)
							 $('#totalValueOfSupply').val(comma(totalValueOfSupply));
							 $('#suggestionValueOfSupply').val(comma(totalValueOfSupply));
					 }
					
				}
				
				
				function total(a, b){
					//세금
					var Tex = changeNumber(a);
					//전체 공급가액
					var totalValueOfSupply = changeNumber(b);
					
					return totalValueOfSupply+Tex
				}
			}	
			
			function changeTable() {
				var tfoot = $('#contract_Product_Table tfoot').children().children();
				var tableRowlength = $("#contract_Product_Table").children().eq(1)[0].rows.length;
				var allTotal = 0;
				var length = 0;
				for(var i=0; i<tableRowlength; i++) {
					if(i%2 == 0){
						//제안금액
						var total = $("#contract_Product_Table").children().eq(1).children().eq(i).children().eq(3).children().val();
						allTotal += changeNumber(total);
						length += 1;
					}
				}	
				tfoot[1].innerHTML = '';
				tfoot[1].innerHTML = length;
				tfoot[3].innerHTML = '';
				tfoot[3].innerHTML = allTotal;
				
				console.log($('.product_add').is($('#estBtn')));
			}
			function changenTex(totalValueOfSupply){
				var a = 0;
				var StringtotalValueOfSupply = totalValueOfSupply.split(',');
				var numtotalValueOfSupply = '';
				for(var i=0; i<StringtotalValueOfSupply.length; i++) {
					numtotalValueOfSupply += StringtotalValueOfSupply[i]
				}
				a = numtotalValueOfSupply;
				var d = (a*1.1 - a).toFixed(3);
				var b = d.split('.');
				var c = 0;
				
				if(b[1] == '000'){
					c = b[0];
				} else {
					c = d;
				}
				return c
			}
			
			 
			
			 function changeNumber(a) {
				var StringA = a.split(',');
				var numberA = '';
				for(var i=0; i<StringA.length; i++) {
					numberA += StringA[i];
				}
				return Number(numberA);
			}
			//세액으로 변환 부가가치세 포함
			 function changenTexVAT(totalValueOfSupply){
					var a = 0;
					var StringtotalValueOfSupply = totalValueOfSupply.split(',');
					var numtotalValueOfSupply = '';
					for(var i=0; i<StringtotalValueOfSupply.length; i++) {
						numtotalValueOfSupply += StringtotalValueOfSupply[i]
					}
					a = numtotalValueOfSupply;
					var d = Math.abs((a/1.1 - a)).toFixed(3);
					var b = d.split('.');
					var c = 0;
					
					if(b[1] == '000'){
						c = b[0];
					} else {
						c = d;
					}
					return c
				}
				
				 function total(a, b){
					//세금
					var Tex = changeNumber(a);
					//전체 공급가액
					var totalValueOfSupply = changeNumber(b);
					
					return totalValueOfSupply+Tex
				}
				
				 function changeNumber(a) {
					var StringA = a.split(',');
					var numberA = '';
					for(var i=0; i<StringA.length; i++) {
						numberA += StringA[i];
					}
					return Number(numberA);
				}
			
				 $(document).keypress(function(e){
					 var tfoot = $('#contract_Product_Table tfoot').children().children();
					 
					  if(e.target.parentNode.offsetParent.id =='contract_Product_Table'){
						  //change
						  $(e.target).off().on('propertychange keyup paste',function(e) {
							  	var unitPrice = 0;
								var cellIndex = $(this).parent()[0].cellIndex;
								var low = $(this).parents().eq(2)[0].rows.length;
								var totalUnitPrice = 0; // 각 개인의 제안금액
								var sum = 0; // 합계 할인금액 안 더한거
								var dc = 0; //할인
								var total = 0; // 합게 할인금액 더한거
								var price = 0; // 수량
								var totalPrice = 0; // 총 수량
								var propoSalamount = 0; //제안금액 합계
								for(var i=0; i<low; i++) {
									if(i%2 == 0){
										 totalUnitPrice += changeNumber($(this).parents().eq(2).children().eq(i).children().eq(1).children().val())
										 sum = changeNumber($(this).parents().eq(2).children().eq(i).children().eq(1).children().val())
										 dc =  Number($(this).parents().eq(2).children().eq(i).children().eq(2).children().val())/100
										 total += sum - sum*dc;
										 $(this).parents().eq(2).children().eq(i).children().eq(3).children().val(sum - sum*dc);
										
									} else {
										price = ($(this).parents().eq(2).children().eq(i).children().eq(2).children().val());
										totalPrice += Number(price);
										$(this).parents().eq(2).children().eq(i).children().eq(3).children().val((sum - (sum*dc)) * price);
										propoSalamount += Number($(this).parents().eq(2).children().eq(i).children().eq(3).children().val());
									}
								}
								tfoot[1].innerHTML = '';
								tfoot[1].innerHTML = totalPrice;
								tfoot[3].innerHTML = '';
								tfoot[3].innerHTML = propoSalamount;
								
							 })
					 } 
				 }) 
			
			
			
			
    	  
    
      
     
      
     
   </script>
   
   <script>
   var count=1;
   $(function(){
	   
	   
	   
	   
	   $(".menu_cnt .main_menu").eq(0).addClass("on").addClass("open");
	   
	   $("#submitBtn").click(function(){
		   $("#insertOppForm").submit();
	   })
	   
	   
	   $('#resetBtn').click(function() {
			location.href = "opportunityMain.so?empNo=${sessionScope.loginEmp.empNo}";
		})
	   
	   $(".product_add").click(function(){
			$('.ui.modal.product4').modal('show');
			return false;
		})
	  
	  $(".exit_Btn").click(function() {
			$('.ui.modal.client').modal('hide');
			$('.ui.modal.company').modal('hide');
			$('.ui.modal.clientAll').modal('hide');
			$('.ui.modal.manager').modal('hide');
			$('.ui.modal.manager2').modal('hide');
			$('.ui.modal.product4').modal('hide');
		})
	   
	   
	   
	   $('#productFile_btn').click(function (e) {
           e.preventDefault();
           var str = '<tr><td><input type="file" name="files" style="display:none" ><input class="upload_name" id="upload_name' 
          + (count++) + '" readonly style="border:0;""></td><td><i class="trash alternate outline icon" style="float: right" onclick="remove_btn($(this))"></i></td></tr>';
          
          $("#file_table").append(str).find('tr:last-child').find('input[type=file]').trigger("click");
          
          $("#file_table").find('tr:last-child').find('input[type=file]').on('change', function(){ // 값이 변경되면 
               var cur=$(this).val();
               var arSplitUrl = cur.split('\\');
               var nArLength = arSplitUrl.length;
               var fileName = arSplitUrl[nArLength-1];
                 
                 $("#upload_name" + (count-1)).val(fileName);
               console.log(cur);
            }); 
         }); 
	   
	   
   })
   
   function remove_btn(obj) {
			 obj.parent().parent().remove();   
		}
   
   
   </script>
</html>