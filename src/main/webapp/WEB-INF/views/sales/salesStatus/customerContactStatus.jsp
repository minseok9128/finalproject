<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style>
	.cld  {
		background:#EAEAEA;
	}
	.table2 th {
		background:#FBFCFD;
		width:7%;
	}
	.table2 th:nth-child(1) {
		background:#FBFCFD;
		width:9%;
	}
	.table2 tr:nth-child(even) {
		background:#F5F9FB;
	}
	.table2{
		/* table-layout:inherit; */
		width:100%;
		border-top:2px solid #27AAE1;
	}
	.table2 tr *{
		text-align:center;
		border-bottom:1px solid #E1E6EB;
		height:38px;
		vertical-align:middle;
		
	}
	.table2 tr *:not(:last-child){
		border-right:1px solid #E1E6EB;
	}
	
	.notice {
		color:#6B6B6B !important;
		margin-bottom:0;
	}
	#employee{
		width:88%;
		margin-right:12px;
		height:38px;
		border-radius:.28571429rem;
		padding-left:10px;
	}
	#content_div{
		margin-top:0;
	}
	.input2 {
		width : 42%;
		height:38px;
		margin-right:15px;
		border-radius:.28571429rem;
		padding:10px;
	}
	.field{
		width:42%;
	}
	#header_div{
		padding-top:0;
		padding-left:0;
		
	}
	.ui.form{
		margin-top:10px;
	}
	.ui.input.right.icon{
		width:44%;
		margin-right:7px;
	}
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					고객접점현황
				</h1>
				<input type=text class="input2" placeholder="고객사"> 
				<!-- <input type=text id="employee" placeholder="제품명"> -->
				
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="담당자">
				</div>
				
				<!-- <input type=text id="employee" placeholder="사원명"> -->
				<button id="search" class="ui button">검색</button>
				<div class="ui form">
				  <div class="two fields">
				    <div class="field">
				      <div class="ui calendar" id="year_calendar">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="Start">
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div id="content_div">
				<div>
					<h3>영업현황</h3>
				</div>
				<table class="table2">
					<tr>
						<th>영업구분</th>
						<th>합계</th>
						<th>1월</th>
						<th>2월</th>
						<th>3월</th>
						<th>4월</th>
						<th>5월</th>
						<th>6월</th>
						<th>7월</th>
						<th>8월</th>
						<th>9월</th>
						<th>10월</th>
						<th>11월</th>
						<th>12월</th>
					</tr>
					<tr>
						<td>잠재고객<br>(합계)</td>
						<td>200</td>
						<td>15</td>
						<td>15</td>
						<td>15</td>
						<td>15</td>
						<td>20</td>
						<td>20</td>
						<td>20</td>
						<td>20</td>
						<td>15</td>
						<td>15</td>
						<td>15</td>
						<td>15</td>
					</tr>
					<tr>
						<td>잠재고객<br>(신규)</td>
						<td>10</td>
						<td>1</td>
						<td>1</td>
						<td>0</td>
						<td>0</td>
						<td>2</td>
						<td>1</td>
						<td>2</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>0</td>
					</tr>
					<tr>
						<td>고객사<br>(합계)</td>
						<td>100</td>
						<td>10</td>
						<td>10</td>
						<td>10</td>
						<td>10</td>
						<td>10</td>
						<td>8</td>
						<td>12</td>
						<td>15</td>
						<td>2</td>
						<td>10</td>
						<td>2</td>
						<td>1</td>
					</tr>
					<tr>
						<td>고객사<br>(신규)</td>
						<td>10</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>2</td>
						<td>0</td>
						<td>0</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>0</td>
					</tr>
					<tr>
						<td>고객<br>(합계)</td>
						<td>200</td>
						<td>10</td>
						<td>10</td>
						<td>30</td>
						<td>20</td>
						<td>20</td>
						<td>10</td>
						<td>10</td>
						<td>20</td>
						<td>30</td>
						<td>10</td>
						<td>10</td>
						<td>20</td>
					</tr>
					<tr>
						<td>고객<br>(신규)</td>
						<td>20</td>
						<td>2</td>
						<td>2</td>
						<td>3</td>
						<td>4</td>
						<td>1</td>
						<td>3</td>
						<td>2</td>
						<td>1</td>
						<td>3</td>
						<td>2</td>
						<td>1</td>
						<td>1</td>
					</tr>
					<tr>
						<td>영업기회<br>(신규)</td>
						<td>20</td>
						<td>1</td>
						<td>2</td>
						<td>3</td>
						<td>4</td>
						<td>5</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>0</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
					</tr>
					<tr>
						<td>영업활동</td>
						<td>50</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>5</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>매출</td>
						<td>20</td>
						<td>2</td>
						<td>2</td>
						<td>2</td>
						<td>1</td>
						<td>1</td>
						<td>2</td>
						<td>2</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
					</tr>
				</table>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	<br><br><br><br>
	<br><br><br><br>
	<br><br><br><br>
	<br><br><br><br>
	
		<%-- <jsp:include page="../../common/footer.jsp" /> --%>
	
	<script>
	$('#year_calendar')
	  .calendar({
	    type: 'year'
	  })
	;
		</script>
</body>
</html>