<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style>
	.cld  {
		background:#EAEAEA;
	}
	.table2 th {
		background:#FBFCFD;
		
	}
	.table2 *{
		height:38px;
		vertical-align:middle;
	}
	.table2 th:nth-child(1):not('input'){
		width:40%;
	}
	.table2 th:nth-child(2),.table2 th:nth-child(3),.table2 th:nth-child(4){
		width:20%;
	}
	.table2 tr:nth-child(4n+1) {
		background:#F5F9FB;
	}
	.table2 tr:nth-child(4n+2) {
		background:#F5F9FB;
	}
	.table2{
		/* table-layout:inherit; */
		width:100%;
		margin-bottom:10px;
	}
	.table3{
		margin-top:10px;
		/* table-layout:inherit; */
		border-top:1px solid #27AAE1;
	}
	.table2 tr *{
		text-align:center;
		border-bottom:1px solid #E1E6EB;
	}
	.table2 tr *:not(:last-child){
		border-right:1px solid #E1E6EB;
	}
	
	.notice {
		color:#6B6B6B !important;
		margin-bottom:0;
		display:inline-block;
	}
	#employee{
		width:88%;
		margin-right:12px;
		height:38px;
		border-radius:.28571429rem;
		padding-left:10px;
	}
	#content_div{
		margin-top:0;
		padding-left:0;
	}
	.input2 {
		width : 43%;
		height:38px;
		margin-right:15px;
		border-radius:.28571429rem;
		padding:10px;
	}
	.field{
		width:42%;
	}
	#header_div{
		padding-left:0;
		padding-top:0;
	}
	.ui.form{
		margin-top:10px;
	}
	.ui.input.right.icon{
		width:44%;
		margin-right:7px;
	}
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					영업기회현황
				</h1>
				<input type=text class="input2" placeholder="(주)핑거전자"> 
				<!-- <input type=text id="employee" placeholder="제품명"> -->
				
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="제품명">
				</div>
				
				<!-- <input type=text class="input2" placeholder="부서명"> -->
				
				<!-- <input type=text id="employee" placeholder="사원명"> -->
				<button id="search" class="ui button">검색</button>
				<div class="ui form">
				  <div class="two fields">
				    <div class="field">
				      <div class="ui calendar" id="rangestart">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="Start">
				        </div>
				      </div>
				    </div>
				    <div class="field">
				      <div class="ui calendar" id="rangeend">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="End">
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div id="content_div">
				<div>
					<h3 class="notice">협상단계 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				</div>
				<div class="table3">
					<table class="table2">
						<tr>
							<th>영업기회</th>
							<th>고객사</th>
							<th>고객</th>
							<th>예상매출</th>
						</tr>
						<tr>
							<th>진행단계/성공확률</th>
							<th>담당자</th>
							<th>진행상태</th>
							<th>활동횟수</th>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>	
						<tr>
							<td>노트북</td>
							<td>1200</td>
							<td>1000</td>
							<td>100</td>
						</tr>
						<tr>
							<td>세탁기</td>
							<td>800</td>
							<td>700</td>
							<td>10</td>
						</tr>
						<tr>
							<td>태블릿 PC</td>
							<td>800</td>
							<td>1200</td>
							<td>50</td>
						</tr>
						<tr>
							<td>스타일러</td>
							<td>20000</td>
							<td>22000</td>
							<td>400</td>
						</tr>
					</table>
				</div>
				<div>
					<h3 class="notice">견적단계 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<div class="table3">
					<table class="table2">
						<tr>
							<th>영업기회</th>
							<th>고객사</th>
							<th>고객</th>
							<th>예상매출</th>
						</tr>
						<tr>
							<th>진행단계/성공확률</th>
							<th>담당자</th>
							<th>진행상태</th>
							<th>활동횟수</th>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>	
						<tr>
							<td>노트북</td>
							<td>1200</td>
							<td>1000</td>
							<td>100</td>
						</tr>
						<tr>
							<td>세탁기</td>
							<td>800</td>
							<td>700</td>
							<td>10</td>
						</tr>
						<tr>
							<td>태블릿 PC</td>
							<td>800</td>
							<td>1200</td>
							<td>50</td>
						</tr>
						<tr>
							<td>스타일러</td>
							<td>20000</td>
							<td>22000</td>
							<td>400</td>
						</tr>
					</table>
				</div>
				<div>
					<h3 class="notice">제안단계 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<div class="table3">
					<table class="table2">
						<tr>
							<th>영업기회</th>
							<th>고객사</th>
							<th>고객</th>
							<th>예상매출</th>
						</tr>
						<tr>
							<th>진행단계/성공확률</th>
							<th>담당자</th>
							<th>진행상태</th>
							<th>활동횟수</th>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>	
						<tr>
							<td>노트북</td>
							<td>1200</td>
							<td>1000</td>
							<td>100</td>
						</tr>
						<tr>
							<td>세탁기</td>
							<td>800</td>
							<td>700</td>
							<td>10</td>
						</tr>
						<tr>
							<td>태블릿 PC</td>
							<td>800</td>
							<td>1200</td>
							<td>50</td>
						</tr>
						<tr>
							<td>스타일러</td>
							<td>20000</td>
							<td>22000</td>
							<td>400</td>
						</tr>
					</table>
				</div>
				<div>
					<h3 class="notice">인지단계 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<div class="table3">
					<table class="table2">
						<tr>
							<th>영업기회</th>
							<th>고객사</th>
							<th>고객</th>
							<th>예상매출</th>
						</tr>
						<tr>
							<th>진행단계/성공확률</th>
							<th>담당자</th>
							<th>진행상태</th>
							<th>활동횟수</th>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>	
						<tr>
							<td>노트북</td>
							<td>1200</td>
							<td>1000</td>
							<td>100</td>
						</tr>
						<tr>
							<td>세탁기</td>
							<td>800</td>
							<td>700</td>
							<td>10</td>
						</tr>
						<tr>
							<td>태블릿 PC</td>
							<td>800</td>
							<td>1200</td>
							<td>50</td>
						</tr>
						<tr>
							<td>스타일러</td>
							<td>20000</td>
							<td>22000</td>
							<td>400</td>
						</tr>
					</table>
				</div>
				<div>
					<h3 class="notice">성공한 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<div class="table3">
					<table class="table2">
						<tr>
							<th>영업기회</th>
							<th>고객사</th>
							<th>고객</th>
							<th>예상매출</th>
						</tr>
						<tr>
							<th>진행단계/성공확률</th>
							<th>담당자</th>
							<th>진행상태</th>
							<th>활동횟수</th>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>	
						<tr>
							<td>노트북</td>
							<td>1200</td>
							<td>1000</td>
							<td>100</td>
						</tr>
						<tr>
							<td>세탁기</td>
							<td>800</td>
							<td>700</td>
							<td>10</td>
						</tr>
						<tr>
							<td>태블릿 PC</td>
							<td>800</td>
							<td>1200</td>
							<td>50</td>
						</tr>
						<tr>
							<td>스타일러</td>
							<td>20000</td>
							<td>22000</td>
							<td>400</td>
						</tr>
					</table>
				</div>
				<div>
					<h3 class="notice">실패한 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<div class="table3">
					<table class="table2">
						<tr>
							<th>영업기회</th>
							<th>고객사</th>
							<th>고객</th>
							<th>예상매출</th>
						</tr>
						<tr>
							<th>진행단계/성공확률</th>
							<th>담당자</th>
							<th>진행상태</th>
							<th>활동횟수</th>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>
						<tr>
							<td>CRM 프로젝트</td>
							<td>(주)오렌지레드</td>
							<td>김진호</td>
							<td>2000</td>
						</tr>
						<tr>
							<td>협상/100%</td>
							<td>안동환</td>
							<td>진행중</td>
							<td>0</td>
						</tr>	
						<tr>
							<td>노트북</td>
							<td>1200</td>
							<td>1000</td>
							<td>100</td>
						</tr>
						<tr>
							<td>세탁기</td>
							<td>800</td>
							<td>700</td>
							<td>10</td>
						</tr>
						<tr>
							<td>태블릿 PC</td>
							<td>800</td>
							<td>1200</td>
							<td>50</td>
						</tr>
						<tr>
							<td>스타일러</td>
							<td>20000</td>
							<td>22000</td>
							<td>400</td>
						</tr>
					</table>
				</div>
				<div>
					<h3 class="notice">보류한 영업기회(14건/ 158,335)</h3>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
			<div class="table3">
				<table class="table2">
					<tr>
						<th>영업기회</th>
						<th>고객사</th>
						<th>고객</th>
						<th>예상매출</th>
					</tr>
					<tr>
						<th>진행단계/성공확률</th>
						<th>담당자</th>
						<th>진행상태</th>
						<th>활동횟수</th>
					</tr>
					<tr>
						<td>CRM 프로젝트</td>
						<td>(주)오렌지레드</td>
						<td>김진호</td>
						<td>2000</td>
					</tr>
					<tr>
						<td>협상/100%</td>
						<td>안동환</td>
						<td>진행중</td>
						<td>0</td>
					</tr>
					<tr>
						<td>CRM 프로젝트</td>
						<td>(주)오렌지레드</td>
						<td>김진호</td>
						<td>2000</td>
					</tr>
					<tr>
						<td>협상/100%</td>
						<td>안동환</td>
						<td>진행중</td>
						<td>0</td>
					</tr>
					<tr>
						<td>CRM 프로젝트</td>
						<td>(주)오렌지레드</td>
						<td>김진호</td>
						<td>2000</td>
					</tr>
					<tr>
						<td>협상/100%</td>
						<td>안동환</td>
						<td>진행중</td>
						<td>0</td>
					</tr>
					<tr>
						<td>CRM 프로젝트</td>
						<td>(주)오렌지레드</td>
						<td>김진호</td>
						<td>2000</td>
					</tr>
					<tr>
						<td>협상/100%</td>
						<td>안동환</td>
						<td>진행중</td>
						<td>0</td>
					</tr>	
					<tr>
						<td>노트북</td>
						<td>1200</td>
						<td>1000</td>
						<td>100</td>
					</tr>
					<tr>
						<td>세탁기</td>
						<td>800</td>
						<td>700</td>
						<td>10</td>
					</tr>
					<tr>
						<td>태블릿 PC</td>
						<td>800</td>
						<td>1200</td>
						<td>50</td>
					</tr>
					<tr>
						<td>스타일러</td>
						<td>20000</td>
						<td>22000</td>
						<td>400</td>
					</tr>
				</table>
				</div>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
		<%-- <jsp:include page="../../common/footer.jsp" /> --%>
	
	<script>
	$(function(){
		$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
		$(".menu_cnt .main_menu").eq(4).addClass("open");
		$(this).parent(".main_menu.add").siblings(".main_menu").eq(4).slideUp();
		$(this).parent(".main_menu.add").find(".sub_menu ").eq(4).slideUp();
        /* $(this).parent(".main_menu.add").find(".sub_menu ").eq(4).slideToggle(); */
		 /* $(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
         $(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(1).addClass("on");
         $(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(1).addClass("open"); */
         /* $(this).parent(".main_menu.add").siblings(".main_menu").addClass("open"); */
	})
	
	
	$('#rangestart').calendar({
		  type: 'date',
		  endCalendar: $('#rangeend')
		});
		$('#rangeend').calendar({
		  type: 'date',
		  startCalendar: $('#rangestart')
		});
	
	
		</script>
		
		

</body>
</html>