<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style>
	#header_div{
		padding:0;
	}
	.cld  {
		background:#EAEAEA;
	}
	.table2 th {
		background:#FBFCFD;
	}
	.table2 tr:nth-child(even) {
		background:#F5F9FB;
	}
	.table2{
		/* table-layout:inherit; */
		width:100%;
		border-top:2px solid #27AAE1;
	}
	.table2 tr *{
		text-align:center;
		border-bottom:1px solid #E1E6EB;
		height:38px;
		vertical-align:middle;
		
	}
	.table2 tr *:not(:last-child){
		border-right:1px solid #E1E6EB;
	}
	
	.notice {
		color:#6B6B6B !important;
		margin-bottom:0;
	}
	#employee{
		width:88%;
		margin-right:12px;
		height:38px;
		border-radius:.28571429rem;
		padding-left:10px;
	}
	#content_div{
		margin-top:0;
	}
	.same{
		width:auto;
	}
	.paging{
		margin-top:10px;
	}
	.right{
		float:right;
	}
	.icon2 {
		font-size:2em !important; 
		color:#27AAE1;
	}
	input[type="text"] {
		margin-bottom:10px;
	}
	.bar {
		width:100%;
		height:38px;
		border-radius:.28571429rem;
	} 
	
	.ui.input.right.icon.bar {
		margin-bottom:10px;
	}
	.halfbar{
		width:44%;
		height:38px;
		border-radius:.28571429rem;
		padding-left:10px;
	}
	#search{
		margin-top:0;
		float:right;
		margin-left:14px;
	}
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
<script>
	'use strict';

	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		black: 'rgb(67, 67, 72)',
		bgreen: 'rgb(147, 235, 130)',
		grey: 'rgb(201, 203, 207)'
	};

	(function(global) {
		var MONTHS = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		];

		var COLORS = [
			'#4dc9f6',
			'#f67019',
			'#f53794',
			'#537bc4',
			'#acc236',
			'#166a8f',
			'#00a950',
			'#58595b',
			'#8549ba'
		];

		var Samples = global.Samples || (global.Samples = {});
		var Color = global.Color;

		Samples.utils = {
			// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
			srand: function(seed) {
				this._seed = seed;
			},

			rand: function(min, max) {
				var seed = this._seed;
				min = min === undefined ? 0 : min;
				max = max === undefined ? 1 : max;
				this._seed = (seed * 9301 + 49297) % 233280;
				return min + (this._seed / 233280) * (max - min);
			},

			numbers: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 1;
				var from = cfg.from || [];
				var count = cfg.count || 8;
				var decimals = cfg.decimals || 8;
				var continuity = cfg.continuity || 1;
				var dfactor = Math.pow(10, decimals) || 0;
				var data = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = (from[i] || 0) + this.rand(min, max);
					if (this.rand() <= continuity) {
						data.push(Math.round(dfactor * value) / dfactor);
					} else {
						data.push(null);
					}
				}

				return data;
			},

			labels: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 100;
				var count = cfg.count || 8;
				var step = (max - min) / count;
				var decimals = cfg.decimals || 8;
				var dfactor = Math.pow(10, decimals) || 0;
				var prefix = cfg.prefix || '';
				var values = [];
				var i;

				for (i = min; i < max; i += step) {
					values.push(prefix + Math.round(dfactor * i) / dfactor);
				}

				return values;
			},

			months: function(config) {
				var cfg = config || {};
				var count = cfg.count || 12;
				var section = cfg.section;
				var values = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = MONTHS[Math.ceil(i) % 12];
					values.push(value.substring(0, section));
				}

				return values;
			},

			color: function(index) {
				return COLORS[index % COLORS.length];
			},

			transparentize: function(color, opacity) {
				var alpha = opacity === undefined ? 0.5 : 1 - opacity;
				return Color(color).alpha(alpha).rgbString();
			}
		};

		// DEPRECATED
		window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(0, 100));
	};

		// INITIALIZATION

		Samples.utils.srand(Date.now());

		// Google Analytics
		/* eslint-disable */
		if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-28909194-3', 'auto');
			ga('send', 'pageview');
		}
		/* eslint-enable */

	}(this));
	</script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					매출달성현황
				</h1>
				
				<div class="ui input right icon bar">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="제품명">
				</div>
				
				<div class="ui form">
				  <div class="two fields">
				    <div class="field">
				      <div class="ui calendar" id="year_calendar">
				        <div class="ui input right icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="Start">
				        </div>
				      </div>
				    </div>
				    <div class="field">
				    	<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="전체">
						</div>
				    </div>
				  </div>
				</div>
				
				<input type=text class="halfbar" placeholder="(주)핑거전자">
				
				<button id="search" class="ui button">검색</button>
				
				<div class="ui input right icon halfbar">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="담당자">
				</div>
			
			</div>
			<div id="content_div">
				<canvas id="canvas" width="720" height="400"></canvas>
				<div>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<table class="table2">
					<tr>
						<th rowspan="2">담당자명</th>
						<th colspan="3">목표(년)</th>
						<th colspan="3">01월</th>
						<th colspan="3">02월</th>
						<th colspan="3">03월</th>
					</tr>
					<tr class="same">
						<th>목표</th>
						<th>매출</th>
						<th>달성</th>
						<th>목표</th>
						<th>매출</th>
						<th>달성</th>
						<th>목표</th>
						<th>매출</th>
						<th>달성</th>
						<th>목표</th>
						<th>매출</th>
						<th>달성</th>
					</tr>
					<tr>
						<td>안동환</td>
						<td>1000</td>
						<td>1100</td>
						<td>110%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
					</tr>
					<tr>
						<td>황규환</td>
						<td>1000</td>
						<td>1100</td>
						<td>110%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
					</tr>
					<tr>
						<td>서범수</td>
						<td>1000</td>
						<td>1100</td>
						<td>110%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
					</tr>
					<tr>
						<td>박재영</td>
						<td>1000</td>
						<td>1100</td>
						<td>110%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
						<td>100</td>
						<td>100</td>
						<td>100%</td>
					</tr>
				</table>
				<div class="paging">
					<i class="chevron left icon icon2"></i>
					<i class="chevron right icon icon2"></i>
				</div>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
		<%-- <jsp:include page="../../common/footer.jsp" /> --%>
	
	<script>
	
	
	
	$('#year_calendar')
	  .calendar({
	    type: 'year'
	  })
	;
	
	var chartData = {
			labels: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			datasets: [{
				type: 'line',
				label: '달성',
				borderColor: window.chartColors.bgreen,
				borderWidth: 2,
				fill: false,
				data: [
					10,
					20,
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				type: 'bar',
				label: '목표',
				backgroundColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				],
				borderColor: 'white',
				borderWidth: 2
			}, {
				type: 'bar',
				label: '실적',
				backgroundColor: window.chartColors.black,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myMixedChart = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				options: {
					responsive: false,
					title: {
						display: true
					},
					tooltips: {
						mode: 'index',
						intersect: true
					}
				}
			});
		};
		</script>
		
		

</body>
</html>