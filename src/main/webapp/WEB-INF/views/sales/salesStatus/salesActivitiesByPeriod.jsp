<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style>
 #employee, #Department{
 	height:38px;
 	padding:10px;
 	border-radius:.28571429rem;
 	margin-right:10px;	
 }
#header_div{
	padding:0;
}
.box{
	border:1px solid black;
	border-radius:.28571429rem;
	height:38px;
	/* width:40%; */
}
input[type="text"]{
 	border:0;
}
.ui {
	width:47%;
	margin-top:10px;
	margin-right:10px;
}
.ui input{
	width:100%;
}
.ui.input.right.icon {
}
}
.resultContents {
    width: 745px;
    height: 96px;
    border: 1px solid #DEE2E8;
    margin-bottom: 14px;
    margin-right: auto;
    margin-left: auto;
}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					기간별영업활동
				</h1>
				<!-- <div class="box">
					<input type=text id="Department" placeholder="고객사">
					<i class="ellipsis vertical icon"></i>
				</div> -->
				
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="고객사">
				</div>
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="고객">
				</div>
				
				<!-- <input type=text id="employee" placeholder="제품명"> -->
				
				<br>
				<input type=text id="Department" placeholder="부서명">
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="담당자">
				</div>
				
				<button class="button">검색</button>
				
				<div class="ui input right icon" id="rangestart">
		        	<i class="calendar icon" ></i>
		        	<input type="text" placeholder="Start">
		        </div>
		        
		        <div class="ui input right icon" id="rangeend">
		        	<i class="calendar icon" ></i>
		        	<input type="text" placeholder="End">
		        </div>
				
			</div>
			<div id="content_div">
				<div>
					<button id="filter_Btn">필터</button>
					<P>60건</P>
					<p class="filters">고객</p>
					<p class="filters">고객사 |</p>
					<p class="filters" style="font-weight: bold;">영업기간▼ |</p>
				</div>
				<hr>
				<div class="resultContents">
					<div><h3>(주)KH전자 김민석</h3></div>
					<div>안동환 / 기타</div>
					<div>2019.12.18 ~ 2019.12.31 / 미완료 </div>
				</div>
				<div class="resultContents">
					<div><h3>(주)KH전자 황규환</h3></div>
					<div>서범수 / 기타</div>
					<div>2020.01.18 ~ 2020.01.31 / 미완료 </div>
				</div>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
	<script>
	$('#rangestart').calendar({
		  type: 'date',
		  endCalendar: $('#rangeend')
		});
		$('#rangeend').calendar({
		  type: 'date',
		  startCalendar: $('#rangestart')
		});
	</script>
</body>
</html>