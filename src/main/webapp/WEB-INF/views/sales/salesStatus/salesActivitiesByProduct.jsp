<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style>
	.cld  {
		background:#EAEAEA;
	}
	.table2 th {
		background:#FBFCFD;
	}
	.table2 th:nth-child(1):not('input'){
		width:30%;
	}
	.table2 th:nth-child(2),.table2 th:nth-child(3){
		width:15%;
	}
	.table2 th:nth-child(4),.table2 th:nth-child(5),.table2 th:nth-child(6),.table2 th:nth-child(7){
		width:10%;
	}
	.table2 tr:nth-child(even) {
		background:#F5F9FB;
	}
	.table2{
		/* table-layout:inherit; */
		width:100%;
		border-top:2px solid #27AAE1;
	}
	.table2 tr *{
		text-align:center;
		border-bottom:1px solid #E1E6EB;
		height:38px;
		vertical-align:middle;
		
	}
	.table2 tr *:not(:last-child){
		border-right:1px solid #E1E6EB;
	}
	
	.notice {
		color:#6B6B6B !important;
		margin-bottom:0;
	}
	#employee{
		width:88%;
		margin-right:12px;
		height:38px;
		border-radius:.28571429rem;
		margin-bottom:10px;
		padding-left:10px;
	}
	#content_div{
		margin-top:0;
	}
	#header_div{
		padding-top:0;
		padding-left:0;
	}
	.ui.input.right.icon{
		width:89%;
		margin-bottom:10px;
		margin-right:10px;
	}
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
<script>
	'use strict';

	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};

	(function(global) {
		var MONTHS = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		];

		var COLORS = [
			'#4dc9f6',
			'#f67019',
			'#f53794',
			'#537bc4',
			'#acc236',
			'#166a8f',
			'#00a950',
			'#58595b',
			'#8549ba'
		];

		var Samples = global.Samples || (global.Samples = {});
		var Color = global.Color;

		Samples.utils = {
			// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
			srand: function(seed) {
				this._seed = seed;
			},

			rand: function(min, max) {
				var seed = this._seed;
				min = min === undefined ? 0 : min;
				max = max === undefined ? 1 : max;
				this._seed = (seed * 9301 + 49297) % 233280;
				return min + (this._seed / 233280) * (max - min);
			},

			numbers: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 1;
				var from = cfg.from || [];
				var count = cfg.count || 8;
				var decimals = cfg.decimals || 8;
				var continuity = cfg.continuity || 1;
				var dfactor = Math.pow(10, decimals) || 0;
				var data = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = (from[i] || 0) + this.rand(min, max);
					if (this.rand() <= continuity) {
						data.push(Math.round(dfactor * value) / dfactor);
					} else {
						data.push(null);
					}
				}

				return data;
			},

			labels: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 100;
				var count = cfg.count || 8;
				var step = (max - min) / count;
				var decimals = cfg.decimals || 8;
				var dfactor = Math.pow(10, decimals) || 0;
				var prefix = cfg.prefix || '';
				var values = [];
				var i;

				for (i = min; i < max; i += step) {
					values.push(prefix + Math.round(dfactor * i) / dfactor);
				}

				return values;
			},

			months: function(config) {
				var cfg = config || {};
				var count = cfg.count || 12;
				var section = cfg.section;
				var values = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = MONTHS[Math.ceil(i) % 12];
					values.push(value.substring(0, section));
				}

				return values;
			},

			color: function(index) {
				return COLORS[index % COLORS.length];
			},

			transparentize: function(color, opacity) {
				var alpha = opacity === undefined ? 0.5 : 1 - opacity;
				return Color(color).alpha(alpha).rgbString();
			}
		};

		// DEPRECATED
		window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(0, 100));
	};

		// INITIALIZATION

		Samples.utils.srand(Date.now());

		// Google Analytics
		/* eslint-disable */
		if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-28909194-3', 'auto');
			ga('send', 'pageview');
		}
		/* eslint-enable */

	}(this));
	</script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<h1>
					제품별영업활동
				</h1>
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="제품명">
				</div>
				
				
				<!-- <br> <input type=text id="Department" placeholder="부서명">
				<input type=text id="employee" placeholder="사원명"> -->
				<button id="search" class="ui button">검색</button>
				<div class="ui form">
				  <div class="two fields">
				    <div class="field">
				      <div class="ui calendar" id="rangestart">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="Start">
				        </div>
				      </div>
				    </div>
				    <div class="field">
				      <div class="ui calendar" id="rangeend">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="End">
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div id="content_div">
				<canvas id="myChart" width="720" height="400"></canvas>
				
				<!-- <button id="randomizeData">Randomize Data</button>
				<button id="addDataset">Add Dataset</button>
				<button id="removeDataset">Remove Dataset</button>
				<button id="addData">Add Data</button>
				<button id="removeData">Remove Data</button> -->
				<div>
					<p class="notice">* 매출관리여부 대상인 품목만 표시됩니다</p>
					<p class="filters notice">(단위 : 만원)</p>
				</div>
				<table class="table2">
					<tr>
						<th>제품명</th>
						<th>목표(년)</th>
						<th>매출(년)</th>
						<th>영업기회</th>
						<th>영업활동</th>
						<th>견적</th>
						<th>계약</th>
					</tr>
					<tr>
						<td>TV</td>
						<td>1000</td>
						<td>800</td>
						<td>80</td>
						<td>180</td>
						<td>60</td>
						<td>40</td>
					</tr>
					<tr>
						<td>냉장고</td>
						<td>500</td>
						<td>350</td>
						<td>60</td>
						<td>100</td>
						<td>40</td>
						<td>20</td>
					</tr>
					<tr>
						<td>스마트폰</td>
						<td>2000</td>
						<td>1800</td>
						<td>500</td>
						<td>600</td>
						<td>400</td>
						<td>300</td>
					</tr>
					<tr>
						<td>노트북</td>
						<td>1200</td>
						<td>1000</td>
						<td>100</td>
						<td>150</td>
						<td>70</td>
						<td>50</td>
					</tr>
					<tr>
						<td>세탁기</td>
						<td>800</td>
						<td>700</td>
						<td>10</td>
						<td>16</td>
						<td>8</td>
						<td>5</td>
					</tr>
					<tr>
						<td>태블릿 PC</td>
						<td>800</td>
						<td>1200</td>
						<td>50</td>
						<td>70</td>
						<td>40</td>
						<td>20</td>
					</tr>
					<tr>
						<td>스타일러</td>
						<td>20000</td>
						<td>22000</td>
						<td>400</td>
						<td>500</td>
						<td>300</td>
						<td>250</td>
					</tr>
				</table>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
		<%-- <jsp:include page="../../common/footer.jsp" /> --%>
	
	<script>
	
	
	
	$('#rangestart').calendar({
		  type: 'date',
		  endCalendar: $('#rangeend')
		});
		$('#rangeend').calendar({
		  type: 'date',
		  startCalendar: $('#rangestart')
		});
	
	
	/* var ctx = document.getElementById('myChart');
	var chart = new Chart(ctx, {
	    // The type of chart we want to create
	    type: 'line',

	    // The data for our dataset
	    data: {
	        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
	        datasets: [{
	            label: 'My First dataset',
	            backgroundColor: 'rgb(255, 99, 132)',
	            borderColor: 'rgb(255, 99, 132)',
	            data: [0, 10, 5, 2, 20, 30, 45]
	        }]
	    },

	    // Configuration options go here
	    options: {}
	});  */
	
	var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var color = Chart.helpers.color;
	var horizontalBarChartData = {
			labels: ['TV', '냉장고', '스마트폰', '노트북', '세탁기', '태블릿 PC', '스타일러'],
			datasets: [{
				label: '영업기회',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: '영업활동',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: '견적수',
				backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
				borderColor: window.chartColors.yellow,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: '계약수',
				backgroundColor: color(window.chartColors.orange).alpha(0.5).rgbString(),
				borderColor: window.chartColors.orange,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
	 var ctx = document.getElementById('myChart');
		var chart = new Chart(ctx, {
			type: 'horizontalBar',
			data: horizontalBarChartData,
			options: {
				// Elements options apply to all of the options unless overridden in a dataset
				// In this case, we are setting the border of each horizontal bar to be 2px wide
				elements: {
					rectangle: {
						borderWidth: 2,
					}
				},
				responsive: false,
				legend: {
					position: 'right',
				},
				title: {
					display: true
				}
			}
		});

		window.onload = function() {

			document.getElementById('randomizeData').addEventListener('click', function() {
				var zero = Math.random() < 0.2 ? true : false;
				horizontalBarChartData.datasets.forEach(function(dataset) {
					dataset.data = dataset.data.map(function() {
						return zero ? 0.0 : randomScalingFactor();
					});

				});
				window.myHorizontalBar.update();
			});
		}
		</script>
		
		
<!-- 	/* var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var color = Chart.helpers.color;
	var horizontalBarChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};*/

		/* window.onload = function() {

		document.getElementById('randomizeData').addEventListener('click', function() {
			var zero = Math.random() < 0.2 ? true : false;
			horizontalBarChartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return zero ? 0.0 : randomScalingFactor();
				});

			});
			window.myHorizontalBar.update();
		});

		var colorNames = Object.keys(window.chartColors);

		document.getElementById('addDataset').addEventListener('click', function() {
			var colorName = colorNames[horizontalBarChartData.datasets.length % colorNames.length];
			var dsColor = window.chartColors[colorName];
			var newDataset = {
				label: 'Dataset ' + (horizontalBarChartData.datasets.length + 1),
				backgroundColor: color(dsColor).alpha(0.5).rgbString(),
				borderColor: dsColor,
				data: []
			};

			for (var index = 0; index < horizontalBarChartData.labels.length; ++index) {
				newDataset.data.push(randomScalingFactor());
			}

			horizontalBarChartData.datasets.push(newDataset);
			window.myHorizontalBar.update();
		});

		document.getElementById('addData').addEventListener('click', function() {
			if (horizontalBarChartData.datasets.length > 0) {
				var month = MONTHS[horizontalBarChartData.labels.length % MONTHS.length];
				horizontalBarChartData.labels.push(month);

				for (var index = 0; index < horizontalBarChartData.datasets.length; ++index) {
					horizontalBarChartData.datasets[index].data.push(randomScalingFactor());
				}

				window.myHorizontalBar.update();
			}
		});

		document.getElementById('removeDataset').addEventListener('click', function() {
			horizontalBarChartData.datasets.pop();
			window.myHorizontalBar.update();
		});

		document.getElementById('removeData').addEventListener('click', function() {
			horizontalBarChartData.labels.splice(-1, 1); // remove the label first

			horizontalBarChartData.datasets.forEach(function(dataset) {
				dataset.data.pop();
			});

			window.myHorizontalBar.update();
		}); */
		
		
	</script> -->
</body>
</html>