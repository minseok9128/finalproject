<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ contextPath }/resources/css/kms/propose.css ">
<style>
	.cld  {
		background:#EAEAEA;
	}
	.table2 th {
		background:#FBFCFD;
	}
	.table2 *{
		height:38px;
		vertical-align:middle;
	}
	.table2 th:nth-child(1):not('input'){
		width:40%;
	}
	.table2 th:nth-child(2),.table2 th:nth-child(3),.table2 th:nth-child(4){
		width:20%;
	}
	.table2 tr:nth-child(4n+1) {
		background:#F5F9FB;
	}
	.table2 tr:nth-child(4n+2) {
		background:#F5F9FB;
	}
	.table2{
		margin-top:10px;
		/* table-layout:inherit; */
		width:100%;
		border-top:1px solid #27AAE1;
		margin-bottom:10px;
	}
	.table2 tr *{
		text-align:center;
		border-bottom:1px solid #E1E6EB;
	}
	.table2 tr *:not(:last-child){
		border-right:1px solid #E1E6EB;
	}
	
	.notice {
		color:#6B6B6B !important;
		margin-bottom:0;
		display:inline-block;
	}
	#employee{
		width:88%;
		margin-right:12px;
		height:38px;
		border-radius:.28571429rem;
		padding-left:10px;
	}
	#content_div{
		margin-top:0;
		padding-left:0;
	}
	.input2 {
		width : 43%;
		height:38px;
		margin-right:15px;
		border-radius:.28571429rem;
		padding:10px;
	}
	.input3 {
		margin-bottom:10px;
		width : 46%;
		height:38px;
		margin-right:15px;
		border-radius:.28571429rem;
		padding:10px;
	}
	.field{
		width:42%;
	}
	#header_div{
		padding-left:0;
		padding-top:0;
	}
	.ui.form{
		margin-top:10px;
	}
	.ui.input.right.icon{
		width:43%;
		margin-bottom:10px;
		margin-right:10px;
	}
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			
			<div id="header_div">
				<h1>
					매출현황
				</h1>
				
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="제품명">
				</div>
				 
				<input type=text class="input3" placeholder="전체">
				
				<br>
				
				<input type=text class="input2" placeholder="(주)핑거전자">
				
				<div class="ui input right icon">
					<i class="ellipsis vertical icon"></i>
					<input type="text" placeholder="담당자">
				</div>
				<!-- <input type=text id="employee" placeholder="사원명"> -->
				<button id="search" class="ui button">검색</button>
				
				
				<div class="ui form">
				  <div class="two fields">
				    <div class="field">
				      <div class="ui calendar" id="rangestart">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="Start">
				        </div>
				      </div>
				    </div>
				    <div class="field">
				      <div class="ui calendar" id="rangeend">
				        <div class="ui input left icon">
				          <i class="calendar icon" ></i>
				          <input class="cld" type="text" placeholder="End">
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div id="content_div">
				<div>
					<h3 class="notice">3건 / 2,550,000원</h3>
				</div>
				</div>
				<table class="table2">
					<tr>
						<th>매출일</th>
						<th>고객사</th>
						<th>고객</th>
						<th>담당자</th>
					</tr>
					<tr>
						<th>연관제품</th>
						<th>공급가액</th>
						<th>세액</th>
						<th>합계</th>
					</tr>
					<tr>
						<td>2020.01.14</td>
						<td>아이피씨</td>
						<td>윤지희</td>
						<td>안동환</td>
					</tr>
					<tr>
						<td>삼성노트북</td>
						<td>1,000,000</td>
						<td>100,000</td>
						<td>1,100,000</td>
					</tr>
					<tr>
						<td>2020.01.11</td>
						<td>거성티에스</td>
						<td>김동환</td>
						<td>서범수</td>
					</tr>
					<tr>
						<td>LG 주전자</td>
						<td>50,000</td>
						<td>5,000</td>
						<td>55,000</td>
					</tr>
					<tr>
						<td>2020.01.10</td>
						<td>(주)동환</td>
						<td>황규환</td>
						<td>문호승</td>
					</tr>
					<tr>
						<td>LG 스타일러</td>
						<td>1,500,000</td>
						<td>150,000</td>
						<td>1,650,000</td>
					</tr>
				</table>
				</div>
			</div>
		</div>

		</main>
	</div>
	</div>
	</div>
		<%-- <jsp:include page="../../common/footer.jsp" /> --%>
	
	<script>
	$(function(){
		$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
		$(".menu_cnt .main_menu").eq(4).addClass("open");
		$(this).parent(".main_menu.add").siblings(".main_menu").eq(4).slideUp();
		$(this).parent(".main_menu.add").find(".sub_menu ").eq(4).slideUp();
        /* $(this).parent(".main_menu.add").find(".sub_menu ").eq(4).slideToggle(); */
		 /* $(".menu_cnt .main_menu").eq(4).addClass("on").addClass("open");
         $(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(1).addClass("on");
         $(".menu_cnt .main_menu").eq(4).find(".sub_menu_list").eq(1).addClass("open"); */
         /* $(this).parent(".main_menu.add").siblings(".main_menu").addClass("open"); */
	})
	
	
	$('#rangestart').calendar({
		  type: 'date',
		  endCalendar: $('#rangeend')
		});
		$('#rangeend').calendar({
		  type: 'date',
		  startCalendar: $('#rangestart')
		});
	
	
		</script>
		
		

</body>
</html>