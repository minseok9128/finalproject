<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath}/resources/css/common.css">
<style>
	.searchArea {
		width: 100%;
		background-color: #E1E6EB;
		margin-top: auto;
		margin-bottom: auto;
	}
	#calendarWid {
		width: 350px;
		margin-top: 10px;
		margin-bottom: 10px;
		margin-left: 10px;
	}
	.calLabel {
		margin-left: 10px;
		margin-right: 10px;
		margin-top: auto;
		margin-bottom: auto;
	}
	#selctSales {
		width: 250px;
		margin-left: 15px;
	}
	#line {
		display: inline-block;
	}
	#searchBtn {
		float: right;
		margin-top: 15px;
		margin-bottom: 15px;
		margin-right: 15px;
	}
	.selectArea {
		margin-top: 30px;
	}
	.graphArea {
		border-bottom: 1px solid black;
		font-size: 12px;
		color: #667277;
		text-align: center;
		padding: 10px;
		cursor: pointer;
		height:40px;
	}
	.dataArea {
		border-bottom: 1px solid #E1E6EB;
		font-size: 12px;
		color: #E1E6EB;
		text-align: center;
		padding: 10px;
		cursor: pointer;
		height:40px;
	}
	.firstArea {
		width: 49%;
	}
	table{
	 	width:150%;
	 	height:150%;
	}
	.secondArea {
		width: 49%;
	}
	.images {
		width: 80px;
		height: 80px;
	}
	.summary {
		align: center;
		display: inline-block;
		margin-right: 10px;
		margin-top: 110px;
	}
	.explan {
		margin-top: 5px;
		margin-bottm: 5px;
	}
	.summaryCount {
		background-color: #797979;
		border-radius: 3px;
		font-size: 13px;
		color: white;
		height: 20px;
		width: 80px;
		vertical-align: middle;
		margin-left: auto;
		margin-right: auto;
	}
	.Area {
		width: 370px;
		height: 373px;
	}
	.explanTitle {
		font-size: 14px;
		font-weight: bold;
		margin-top: 30px;
	}
	.explanHeader {
		margin-left: 10px;
		text-align: left;
	}
	.alignCategory {
		font-size: 14px;
		margin-bottom: 0;
		margin-right: 5px;
		width: 45px;
		color: #5B5B5B;
	}
	.th {
		border-bottom: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;
		border-right: 1px solid #DDDDDD;
		height: 30px;
		vertical-align: middle;
		color: #00A1DB;
	}
	tr > td {
		height:30px;
		vertical-align:middle;
		text-align:center;
	}
	.border {
		border:1px solid #DDDDDD;
	}
</style>
<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
<!-- chart.js -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
</head>
</head>
<body>
	<jsp:include page="../../common/statusMenubar.jsp" />

	<div class="main_ctn">
		<div class="searchArea">
			<div class="ui form" id="line">
				<div class="two fields" id="calendarWid">
					<div class="field">
						<label></label>
						<div class="ui calendar" id="rangestart">
							<div class="ui input left icon">
								<i class="calendar icon"></i> 
								<input type="text" placeholder="2020.01.01">
							</div>
						</div>
					</div>
					<label class="calLabel">~</label>
					<div class="field">
						<label></label>
						<div class="ui calendar" id="rangeend">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="2020.01.01">
							</div>
						</div>
					</div>
				</div>
			</div>

			<select class="ui dropdown" id="selctSales">
				<option value="">담당자</option>
				<c:forEach var="empName" items="${empList}" varStatus="status">
					<option value="<c:out value='${status.index}'/>"><c:out value="${nameList.empName}"/></option>
				</c:forEach>
			</select>

			<button class="medium ui inverted primary button" id="searchBtn">검색</button>
		</div>
		
		<div style="margin-top:30px;">
			<ul class="tabs tab_activity">
		       	<li style="width:50%">
		       		<a name="tab1" style="cursor:pointer" x-ng-click="moveTab('L');" href="showActivityStatus.sas"><i class="chart line icon"></i>그래프</a>
		       	</li>
		       	<li style="width:50%">
		       		<a name="tab2" class="active" style="cursor:pointer"><i class="align justify icon"></i>데이터</a>
		       	</li>
	        </ul>
		</div>

		<div>
			<p class="explanHeader"	style="font-size: 17px; display: inline-block">목록</p>
			<p style="font-size: 13px; display: inline-block">(<c:out value="${activityDataCount}"/>)건</p>
		</div>
		<div style="overflow: auto; height: 580px; width: 800px; border: 1px solid #E1E6EB;">
			<table style="border-collapse: collapse; border-spacing: 0; border-bottom: 1px solid #DFDBDF;">
				<tr>
					<th class="th" style="width: 200px;">날짜</th>
					<th class="th" style="width: 80px;">활동분류</th>
					<th class="th" style="width: 80px;">상세</th>
					<th class="th" style="width: 120px;">고객사</th>
					<th class="th" style="width: 80px;">고객</th>
					<th class="th" style="width: 180px;">영업기회</th>
					<th class="th" style="width: 200px;">내용</th>
					<th class="th" style="width: 80px;">작성자</th>
				</tr>
				<c:forEach var="dataList" items="${activityDataList}">
					<tr style="text-align:center; vertical-align:middle; border-bottom: 1px solid #DFDBDF;">
						<td>
							<c:out value="${dataList.activityDate}"/>
						</td>
						<td class="border">
							<c:if test="${dataList.category eq 1}">
								<c:out value="전화"/>
							</c:if>
							<c:if test="${dataList.category eq 2}">
								<c:out value="메일"/>
							</c:if>
							<c:if test="${dataList.category eq 3}">
								<c:out value="방문"/>
							</c:if>
							<c:if test="${dataList.category eq 4}">
								<c:out value="기타"/>
							</c:if>
						</td>
						<td class="border">
							<c:if test="${dataList.purpose eq 1}">
								<c:out value="인사"/>
							</c:if>
							<c:if test="${dataList.purpose eq 2}">
								<c:out value="제품소개"/>
							</c:if>
							<c:if test="${dataList.purpose eq 3}">
								<c:out value="데모시연"/>
							</c:if>
							<c:if test="${dataList.purpose eq 4}">
								<c:out value="정보수집"/>
							</c:if>
							<c:if test="${dataList.purpose eq 5}">
								<c:out value="제안"/>
							</c:if>
							<c:if test="${dataList.purpose eq 6}">
								<c:out value="업무협의"/>
							</c:if>
							<c:if test="${dataList.purpose eq 7}">
								<c:out value="협상"/>
							</c:if>
							<c:if test="${dataList.purpose eq 8}">
								<c:out value="계약"/>
							</c:if>
							<c:if test="${dataList.purpose eq 9}">
								<c:out value="기타"/>
							</c:if>
						</td>
						<td class="border">
							<c:out value="${dataList.companyName}"/>
						</td>
						<td class="border">
							<c:out value="${dataList.clientName}"/>
						</td>
						<td class="border">
							<c:out value="${dataList.oppName}"/>
						</td>
						<td class="border">
							<c:out value="${dataList.activityCon}"/>
						</td>
						<td class="border">
							<c:out value="${dataList.empName}"/>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>

	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(3).addClass("on");
		});
		
			$('#rangestart').calendar({
			type : 'date',
			endCalendar : $('#rangeend')
		});
			
		$('#rangeend').calendar({
			type : 'date',
			startCalendar : $('#rangestart')
		});
	</script>
</body>
</html>