<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath}/resources/css/common.css">
<style>
	.searchArea {
		width: 100%;
		background-color: #E1E6EB;
		margin-top: auto;
		margin-bottom: auto;
	}
	#calendarWid {
		width: 350px;
		margin-top: 10px;
		margin-bottom: 10px;
		margin-left: 10px;
	}
	.calLabel {
		margin-left: 10px;
		margin-right: 10px;
		margin-top: auto;
		margin-bottom: auto;
	}
	
	#selctSales {
		width: 250px;
		margin-left: 15px;
	}
	
	#line {
		display: inline-block;
	}
	
	#searchBtn {
		float: right;
		margin-top: 15px;
		margin-bottom: 15px;
		margin-right: 15px;
	}
	.selectArea {
		margin-top: 30px;
	}
	.graphArea {
		border-bottom: 1px solid black;
		font-size: 12px;
		color: #667277;
		text-align: center;
		padding: 10px;
		cursor: pointer;
		height:40px;
	}
	.dataArea {
		border-bottom: 1px solid #E1E6EB;
		font-size: 12px;
		color: #E1E6EB;
		text-align: center;
		padding: 10px;
		cursor: pointer;
		height:40px;
	}
	.firstArea {
		width: 49%;
	}
	.secondArea {
		width: 49%;
	}
	.images {
		width: 80px;
		height: 80px;
	}
	.summary {
		align: center;
		display: inline-block;
		margin-right: 10px;
		margin-top: 110px;
	}
	.explan {
		margin-top: 5px;
		margin-bottm: 5px;
	}
	.summaryCount {
		background-color: #797979;
		border-radius: 3px;
		font-size: 13px;
		color: white;
		height: 20px;
		width: 80px;
		vertical-align: middle;
		margin-left: auto;
		margin-right: auto;
	}
	.Area {
		width: 370px;
		height: 373px;
	}
	.explanTitle {
		font-size: 14px;
		font-weight: bold;
		margin-top: 30px;
	}
	.explanHeader {
		margin-left: 10px;
		text-align: left;
	}
	.alignCategory {
		font-size: 11px;
		margin-bottom: 0;
		margin-right: 5px;
		width: 45px;
		color: #5B5B5B;
	}
	.th {
		border-bottom: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;
		border-right: 1px solid #DDDDDD;
		height: 30px;
		vertical-align: middle;
		color: #00A1DB;
	}
	.left.border{
	 	margin-top:10px;
	 	margin-bottom:10px;
		border-left:3px solid #27AAE1;
		padding-left:10px;
	}
</style>
<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
<!-- chart.js -->
<script	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
</head>
</head>
<body>
	<jsp:include page="../../common/statusMenubar.jsp" />
	<jsp:useBean id="now" class="java.util.Date" />
	<fmt:formatDate value='${now}' pattern='yyyy-MM-dd' var="nowDate"/>
	
	<div class="main_ctn">
		<div class="searchArea">
			<div class="ui form" id="line">
				<div class="two fields" id="calendarWid">
					<div class="field">
						<label></label>
						<div class="ui calendar" id="rangestart">
							<div class="ui input left icon">
								<i class="calendar icon"></i> 
								<input type="text" value="${nowDate}">
							</div>
						</div>
					</div>
					<label class="calLabel">~</label>
					<div class="field">
						<label></label>
						<div class="ui calendar" id="rangeend">
							<div class="ui input left icon">
								<i class="calendar icon"></i> 
								<input type="text" value="${nowDate}">
							</div>
						</div>
					</div>
				</div>
			</div>

			<select class="ui dropdown" id="selctSales">
				<c:forEach var="nameList" items="${empList}" varStatus="status">
					<option value="">담당자</option>
					<option value="<c:out value='${status.index}'/>"><c:out value="${nameList.empName}"/></option>
				</c:forEach>
			</select>

			<button class="medium ui inverted primary button" id="searchBtn">검색</button>
		</div>
		
		<div style="margin-top:30px;">
			<ul class="tabs tab_activity">
		       	<li style="width:50%"><a name="tab1" class="active" style="cursor:default"><i class="chart line icon"></i>그래프</a></li>
		       	<li style="width:50%"><a name="tab2" style="cursor:pointer" x-ng-click="moveTab('L');" href="dataActivityStatus.sas"><i class="align justify icon"></i>데이터</a></li>
	        </ul>
		</div>

		<div class="ui segment" id="showGraphArea" style="border: none; box-shadow: none;">
			<div class="ui two column very relaxed grid">
				<div class="column" style="padding: 0;">
					<div style="" align="center">
						<div class="Area">
							<div class="explanTitle">
								<div class="left border">
									<h4 class="explanHeader">요약</h4>
								</div>
								</div>
								<div class="summary">
									<img class="images" src="${ contextPath }/resources/images/month_01.png">
									<p style="">방문</p>
									<div class="summaryCount three">
										<c:if test="${!empty categoryCount['3']}">
											<c:out value="${categoryCount['3']}"/>
										</c:if>
										<c:if test="${empty categoryCount['3']}">
											<c:out value="0"/>
										</c:if>
									</div>
								</div>
								<div class="summary">
									<img class="images" src="${ contextPath }/resources/images/month_02.png">
									<p style="">전화</p>
									<div class="summaryCount one">
										<c:if test="${!empty categoryCount['1']}">
											<c:out value="${categoryCount['1']}"/>
										</c:if>
										<c:if test="${empty categoryCount['1']}">
											<c:out value="0"/>
										</c:if>
									</div>
								</div>
								<div class="summary">
									<img class="images" src="${ contextPath }/resources/images/month_03.png">
									<p style="">메일</p>
									<div class="summaryCount two">
										<c:if test="${!empty categoryCount['2']}">
											<c:out value="${categoryCount['2']}"/>
										</c:if>
										<c:if test="${empty categoryCount['2']}">
											<c:out value="0"/>
										</c:if>
									</div>
								</div>
								<div class="summary" style="margin-right: 0px;">
									<img class="images" src="${ contextPath }/resources/images/month_04.png">
									<p style="">기타</p>
									<div class="summaryCount four">
										<c:if test="${!empty categoryCount['4']}">
											<c:out value="${categoryCount['4']}"/>
										</c:if>
										<c:if test="${empty categoryCount['4']}">
											<c:out value="0"/>
										</c:if>
									</div>
								</div>
						</div>

						<div class="Area">
							<div class="explanTitle">
								<div class="left border">
									<h4 class="explanHeader">영업담당</h4>
								</div>
							</div>
							<div class="view_chart">
								<div class="chart1">
									<canvas id="myChartGroup" width="100%" height="100%"></canvas>
								</div>
							</div>
							<script>
								var one = $(".one").text();
								var two = $(".summaryCount.two").text();
								var three = $(".three").text();
								var four = $(".four").text();	
								
								var One = $.trim(one);
								var Two = $.trim(two);
								var Three = $.trim(three);
								var Four = $.trim(four);
								
								var ctx = document.getElementById("myChartGroup").getContext("2d");
								var data = {
								  labels: ["전체"],
								  datasets: [{
								    label: "전화",
								    backgroundColor: 'rgba(255, 99, 132, 0.2)',
								    borderColor: 'rgba(255, 99, 132, 0.2)',
								    data: [One]
								  }, {
								    label: "메일",
								    backgroundColor: 'rgba(54, 162, 235, 0.2)',
								    borderColor: 'rgba(54, 162, 235, 0.2)',
								    data: [Two]
								  }, {
								    label: "방문",
								    backgroundColor: 'rgba(255, 206, 86, 0.2)',
								    borderColor: 'rgba(255, 206, 86, 0.2)',
								    data: [Three]
								  }, {
								    label: "기타",
								    backgroundColor: 'rgba(75, 192, 192, 0.2)',
								    borderColor: 'rgba(75, 192, 192, 0.2)',
								    data: [Four]
								  }]
								};
	
								var myBarChart = new Chart(ctx, {
								  type: 'bar',
								  data: data,
								  options: {
								    scales: {
								      xAxes: [{
								        stacked: true
								      }],
								      yAxes: [{
								        stacked: true,
								        ticks: {
								          beginAtZero: true
								        }
								      }]
								    }
								  }
								});
							</script>
						</div>

						<div class="Area">
							<div class="explanTitle">
								<div class="left border">
									<h4 class="explanHeader">활동분류</h4>
								</div>
							</div>
							<div class="view_chart">
								<div class="chart1">
									<canvas id="myChartOne" width="100%" height="100%"></canvas>
								</div>
							</div>
							<script>
								var one = $(".one").text();
								var two = $(".summaryCount.two").text();
								var three = $(".three").text();
								var four = $(".four").text();
								
								var One = $.trim(one);
								var Two = $.trim(two);
								var Three = $.trim(three);
								var Four = $.trim(four);
								
	                            var ctx = document.getElementById("myChartOne"); //캔버스 id값 가져오기
	                            var myChart = new Chart(ctx, {
	                            	type: 'bar', //그래프 형태 지정하기
	                                data: {
	                                labels: ["전화", "메일", "방문", "기타"], //X축 제목
	                                	datasets: [
	                                   	{
	                                   	      label: "전체",
	                                          backgroundColor: "rgba(255, 99, 132, 0.2)",
	                                          borderColor: "pink",
	                                          borderWidth: 1,
	                                          data: [One, Two, Three, Four],
	                                          borderWidth: 1
	                                    }]
		                           	},
		                            options: {
		                            	scales: { //X,Y축 옵션
		                                	yAxes: [{
		                                    	ticks: {
		                                        	beginAtZero:true  //Y축의 값이 0부터 시작
		                                        }
		                                    }]
		                                }
		                            }
		                        });
	                    	</script>
						</div>

					</div>
				</div>
				
				<div class="column" style="padding: 0;">
					<div class="secondArea">
						<div class="Area">
							<div class="explanTitle">
								<div class="left border">
									<h4 class="explanHeader">월</h4>
								</div>
							</div>
							<div class="view_chart">
								<div class="chart1">
									<canvas id="myChart" width="100%" height="100%"></canvas>
								</div>
							</div>
							<script>
								var one = ${month['01']};
								var two = ${month['02']};
								var three = ${month['03']};
								var four = ${month['04']};
								var five = ${month['05']};
								var six = ${month['06']};
								var seven = ${month['07']};
								var eight = ${month['08']};
								var nine = ${month['09']};
								var ten = ${month['10']};
								var eleven = ${month['11']};
								var twelve = ${month['12']};
							
	                            var ctx = document.getElementById("myChart"); //캔버스 id값 가져오기
	                            var myChart = new Chart(ctx, {
	                            type: 'bar', //그래프 형태 지정하기
	                            data: {
	                            	labels: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"], //X축 제목
	                                datasets: [{
	                                	label: '활동횟수',
	                                    data: [one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve],
	                                    backgroundColor: [
	                                    	'rgba(255, 99, 132, 0.2)', //1번째 그래프의 바탕색
	                                        'rgba(54, 162, 235, 0.2)',
	                                        'rgba(255, 206, 86, 0.2)',
	                                        'rgba(75, 192, 192, 0.2)',
	                                        'rgba(153, 102, 255, 0.2)',
	                                        'rgba(255, 159, 64, 0.2)',
	                                        'rgba(255, 99, 132, 0.2)', 
	                                        'rgba(54, 162, 235, 0.2)',
	                                        'rgba(255, 206, 86, 0.2)',
	                                        'rgba(75, 192, 192, 0.2)',
	                                        'rgba(153, 102, 255, 0.2)',
	                                        'rgba(255, 159, 64, 0.2)'
	                                    ],
	                                    borderColor: [
	                                    	'rgba(255,99,132,1)',      //1번째 그래프의 선색
	                                        'rgba(54, 162, 235, 1)',
	                                        'rgba(255, 206, 86, 1)',
	                                        'rgba(75, 192, 192, 1)',
	                                        'rgba(153, 102, 255, 1)',
	                                        'rgba(255, 159, 64, 1)',
	                                        'rgba(255,99,132,1)',      
	                                        'rgba(54, 162, 235, 1)',
	                                        'rgba(255, 206, 86, 1)',
	                                        'rgba(75, 192, 192, 1)',
	                                        'rgba(153, 102, 255, 1)',
	                                        'rgba(255, 159, 64, 1)'
	                                    ],
	                                    borderWidth: 1 //선굵기
	                                    }]
	                                },
	                                options: {
	                                    scales: { //X,Y축 옵션
	                                        yAxes: [{
	                                            ticks: {
	                                                beginAtZero:true  //Y축의 값이 0부터 시작
	                                            }
	                                        }]
	                                    }
	                                }
	                            });
	                    	</script>
						</div>

						<div class="Area">
							<div class="explanTitle">
								<div class="left border">
									<h4 class="explanHeader">영업담당</h4>
								</div>
							</div>
							<div>
								<canvas id="pie-chart-first" width="800" height="600"></canvas>
							</div>
							<script>
								var names = '${names}';
		                        var nameArr = [];
		                        nameArr = names.split(", ");
		                        
		                        var countList = ${countList};
		                        
		                        new Chart(document.getElementById("pie-chart-first"), {
		                            type: 'pie',
		                            data: {
		                              labels: nameArr,
		                              datasets: [{
		                                 label: "Population (millions)",
		                                 backgroundColor: [
		                                   'rgba(255, 99, 132, 0.2)',
		                                     'rgba(54, 162, 235, 0.2)',
		                                     'rgba(255, 206, 86, 0.2)',
		                                     'rgba(75, 192, 192, 0.2)',
		                                     'rgba(153, 102, 255, 0.2)',
		                                     'rgba(255, 159, 64, 0.2)'],
		                                data: countList
		                              }]
		                            },
									    options: {
									      title: {
									        display: true,
									      }
									    }
									});
							</script>
						</div>

						<div class="Area">
							<div class="explanTitle">
								<div class="left border">
									<h4 class="explanHeader">활동분류</h4>
								</div>
							</div>
							<div>
								<canvas id="pie-chart" width="800" height="600"></canvas>
							</div>
							<script>
								
								var one = $(".one").text();
								var two = $(".summaryCount.two").text();
								var three = $(".three").text();
								var four = $(".four").text();
								
								var One = $.trim(one);
								var Two = $.trim(two);
								var Three = $.trim(three);
								var Four = $.trim(four);
								
								console.log(One);
								console.log(Two);
								console.log(Three);
								console.log(Four);
								
								if(one == 0 && two == 0 && three == 0 && four == 0) {
									new Chart(document.getElementById("pie-chart"), {
									    type: 'pie',
									    data: {
									      labels: ["활동없음"],
									      datasets: [{
									        label: "Population (millions)",
									        backgroundColor: [
									        	'rgba(255, 99, 132, 0.2)',
								                'rgba(54, 162, 235, 0.2)',
								                'rgba(255, 206, 86, 0.2)',
								                'rgba(75, 192, 192, 0.2)',
								                'rgba(153, 102, 255, 0.2)'
								             ],
									        data: [100]
									      }]
									    },
									    options: {
									      title: {
									        display: true,
									      }
									    }
									});
								}else {
									new Chart(document.getElementById("pie-chart"), {
									    type: 'pie',
									    data: {
									      labels: ["전화", "메일", "방문", "기타"],
									      datasets: [{
									        label: "Population (millions)",
									        backgroundColor: [
									        	'rgba(255, 99, 132, 0.2)',
								                'rgba(54, 162, 235, 0.2)',
								                'rgba(255, 206, 86, 0.2)',
								                'rgba(75, 192, 192, 0.2)',
								                'rgba(153, 102, 255, 0.2)'
								             ],
									        data: [One,Two,Three,Four]
									      }]
									    },
									    options: {
									      title: {
									        display: true,
									      }
									    }
									});
								}
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(function() {
			$(".menu_cnt .main_menu").eq(3).addClass("on");
		});
		
			$('#rangestart').calendar({
			type : 'date',
			endCalendar : $('#rangeend'),
			formatter : {
                date : function(date, settings) {
                   var day = date.getDate();
                   var month = date.getMonth() + 1;
                   var year = date.getFullYear();
                   
                   if(month < 10) {
                 	  if(day < 10) {
                 		  return year + '-' + '0' + month + '-' + '0' + day;
                 	  }else {
                 		  return year + '-' + '0' + month + '-' + day;
                 	  }
                  	 
                   }else {
                 	  if(day < 10) {
                 		  return year + '-' + month + '-' + '0' + day;
                 	  }else {
                 		  return year + '-' + month + '-' + day;
                 	  }
                   }
                }
            }
		});
			
		$('#rangeend').calendar({
			type : 'date',
			startCalendar : $('#rangestart'),
			formatter : {
                date : function(date, settings) {
                   var day = date.getDate();
                   var month = date.getMonth() + 1;
                   var year = date.getFullYear();
                   
                   if(month < 10) {
                 	  if(day < 10) {
                 		  return year + '-' + '0' + month + '-' + '0' + day;
                 	  }else {
                 		  return year + '-' + '0' + month + '-' + day;
                 	  }
                  	 
                   }else {
                 	  if(day < 10) {
                 		  return year + '-' + month + '-' + '0' + day;
                 	  }else {
                 		  return year + '-' + month + '-' + day;
                 	  }
                   }
                }
            }
		});
		</script>
</body>
</html>