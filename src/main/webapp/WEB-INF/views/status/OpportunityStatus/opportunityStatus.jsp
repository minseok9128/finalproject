<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.title{
		display:inline-block;
	}
 	.btn_box{
 		float:right;
 		display:inline-block;
 	}
 	.search_box{
 		border:1px solid #D7DBDF;
 		margin-bottom:10px;
 		border-radius:.28571429rem;
 		background:#FBFCFD;
 	}
 	.first_line {
 		margin-top:10px;
 		margin-bottom:10px;
 		margin-left:10px;
 	}
 	.second_line {
 		margin-bottom:10px;
 		margin-left:10px;
 	}
 	.search_box *{
 		/* margin-left:10px; */
 		margin-right:10px;
 	}
 	.ui.form {
 		display:inline-block;
 		margin:0;
 	}
 	.ui.form > .two.fields{
 		min-width: 280px;
 		margin: 0;
 	}
 	.ui.form > .two.fields > .field{
 		padding: 0;
 	}
 	.ui.calendar{
 		margin-left:0;
 	}
 	.first_line *{
 	}
 	.table_border{
 		border:1px solid #D7DBDF;
 		border-radius:.28571429rem;
 		width:100%;
 		height:250px;
 		overflow:auto;
 	}
 	h3 {
 		padding-top:10px;
 	}
 	.table_head{
 		background:#FBFCFD;
 		padding-top:10px;
 		padding-left:10px;
 	}
 	.table_head * *{
 		vertical-align:middle;
 		
 	}
 	.table_content{
 		overflow:auto;
 	}
 	table{
 		width:100%;
 	}
 	th ,td{
 		text-align:center;
 		vertical-align:middle !important;
 		height:30px;
 	}
 	th{
 		color:#5b5b5b !important;
 		
 	}
 	.table_content tr *{
 		padding-left:10px;
 	}
 	tr:nth-child(even){
 		background:#F9F9F9;
 	}
 	.line{
 		text-decoration:underline;
 		cursor:pointer;
 		color:#27AAE1;
 	}
 	ul{
 		list-style:none;
 	}
 	.tab li:hover{
 		border-bottom:1px solid #000000;
 		color:#000000;
 	}
 	.tab li {
 		width:49%;
 		display: inline-block;
 		border-bottom:1px solid #939393;
 		text-align:center;	
 		color:#939393;
 	}
 	.left.border{
 		margin-top:10px;
 		margin-bottom:10px;
 		border-left:3px solid #27AAE1;
 		padding-left:10px;
 	}
 	.panel{
 		border:1px solid black;
 		margin-bottom:10px;
 	}
 	.panel ul {
 		width:auto;
 	}
 	.panel ul li {
 		width:24.5%;
 		border-bottom:0;
 		padding-top:5px;
 		padding-bottom:5px;
 		
 		display: inline-block;
 		border-bottom:1px solid #939393;
 		text-align:center;	
 		color:#939393;
 	}
 	.panel ul li:not(:first-child) {
 		border-left:1px solid #939393;
 	}
 	.active, .action  {
 		/* color:red;
 		background:blue; */
 		color:black !important;
 		border-color:black !important;
 	}
	.chart{
		background:url("${contextPath}/resources/images/box_chart2.png");
		background-size:contain;
		background-repeat:no-repeat;
	}
	.chart li span {
		border-top:1px solid #ddd;
		padding-top:10px;
		padding-left:20px;
		padding-right:20px;
		margin-left:70px;
		
	}
	.chart li *{
		margin-left:20px;
	}
	.chart li p {
		display:inline-block;
		background:#F2F3F5;
		width:170px;
		height:50px;
		text-align:center;
		padding-top:15px;
		
	}
	.chart li:nth-child(1) p:last-child {
		margin-top:13px;
		border-right:4px solid #FFC800;
	}
	.chart li:nth-child(2) p:last-child {
		border-right:4px solid #00C3D2;
	}
	.chart li:nth-child(3) p:last-child {
		border-right:4px solid #6A8B9B;
	}
	.chart li:nth-child(4) p:last-child {
		border-right:4px solid #FF8E56;
	}
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>

<script>
	'use strict';

	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		black: 'rgb(67, 67, 72)',
		bgreen: 'rgb(147, 235, 130)',
		grey: 'rgb(201, 203, 207)'
	};

	(function(global) {
		var MONTHS = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		];

		var COLORS = [
			'#4dc9f6',
			'#f67019',
			'#f53794',
			'#537bc4',
			'#acc236',
			'#166a8f',
			'#00a950',
			'#58595b',
			'#8549ba'
		];

		var Samples = global.Samples || (global.Samples = {});
		var Color = global.Color;

		Samples.utils = {
			// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
			srand: function(seed) {
				this._seed = seed;
			},

			rand: function(min, max) {
			var seed = this._seed;
				min = min === undefined ? 0 : min;
				max = max === undefined ? 1 : max;
				this._seed = (seed * 9301 + 49297) % 233280;
				return min + (this._seed / 233280) * (max - min);
			},

			numbers: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 1;
				var from = cfg.from || [];
				var count = cfg.count || 8;
				var decimals = cfg.decimals || 8;
				var continuity = cfg.continuity || 1;
				var dfactor = Math.pow(10, decimals) || 0;
				var data = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = (from[i] || 0) + this.rand(min, max);
					if (this.rand() <= continuity) {
						data.push(Math.round(dfactor * value) / dfactor);
					} else {
						data.push(null);
					}
				}

				return data;
			},

			labels: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 100;
				var count = cfg.count || 8;
				var step = (max - min) / count;
				var decimals = cfg.decimals || 8;
				var dfactor = Math.pow(10, decimals) || 0;
				var prefix = cfg.prefix || '';
				var values = [];
				var i;

				for (i = min; i < max; i += step) {
					values.push(prefix + Math.round(dfactor * i) / dfactor);
				}

				return values;
			},

			months: function(config) {
				var cfg = config || {};
				var count = cfg.count || 12;
				var section = cfg.section;
				var values = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = MONTHS[Math.ceil(i) % 12];
					values.push(value.substring(0, section));
				}

				return values;
			},

			color: function(index) {
				return COLORS[index % COLORS.length];
			},

			transparentize: function(color, opacity) {
				var alpha = opacity === undefined ? 0.5 : 1 - opacity;
				return Color(color).alpha(alpha).rgbString();
			}
		};

		// DEPRECATED
		window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(0, 100));
	};

		// INITIALIZATION

		Samples.utils.srand(Date.now());

		// Google Analytics
		/* eslint-disable */
		if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-28909194-3', 'auto');
			ga('send', 'pageview');
		}
		/* eslint-enable */

	}(this));
	</script>
</head>
<body>
	<jsp:include page="../../common/statusMenubar.jsp" />
	<div class="main_ctn" style="background: white;">
	<div class="tb_title" ><i class="home icon"></i>status > 영업기회</div>
		<div class="title"></div>
		<div class="tb_cnt" >
			<div class="tb_title"></div>
			<div id="header_div">
				<div class="search_box">
					<div class="first_line">
						<div class="ui form">
						<div class="two fields">
						    <div class="field">
						      <div class="ui calendar" id="rangestart">
						        <div class="ui input right icon">
						          <i class="calendar icon" ></i>
						          <input class="cld" type="text" placeholder="시작일">
						        </div>
						      </div>
						    </div>
						    <div class="field">
						      <div class="ui calendar" id="rangeend">
						        <div class="ui input right icon">
						          <i class="calendar icon" ></i>
						          <input class="cld" type="text" placeholder="종료일">
						        </div>
						      </div>
						    </div>
				  		</div>
				  		</div> 	
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="고객사">
						</div>
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="고객">
						</div>
					</div>
					<div class="second_line">
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="영업기회">
						</div>
						<div class="ui input right icon">
							<input type="text" placeholder="부서">
						</div>
						<div class="ui input right icon">
							<i class="ellipsis vertical icon"></i>
							<input type="text" placeholder="담당자">
						</div>
					<button class="ui basic button">조회</button>
					</div>
				</div>
				<div>
					<ul class="tab">
						<li class="action"><i class="chart line icon"></i>그래프</li>
						<li id="statusData"><i class="align justify icon"></i>데이터</li>
					</ul>
					<div class="left border"><h2>월</h2></div>
					
					<div class="panel"> 
						<ul class="no">
						<li><p>전체</p><c:out value="${hmap.header.cnt}"/></li>
						<li><p>계약금액</p>
							<fmt:formatNumber value="${hmap.header.expRev }" pattern="#,###.##" />
							<%-- <c:out value="${hmap.header.expRev}"/> --%>
						</li>
						<li>
							<p>예상이익률</p>
							<fmt:formatNumber value="${hmap.header.expRate }" pattern="#,###.##" />
							<%-- <c:out value="${hmap.header.expRate}"/> --%>%
						</li>
						<li>
							<p>이익금액</p>
							<fmt:formatNumber value="${hmap.header.expPrice }" pattern="#,###.##" />
							<%-- <c:out value="${hmap.header.expPrice}"/> --%>
						</li>
					</ul>	
					</div>
					
					
					<div class="table_border">
						<%-- <div class="table_head">
							<h3 class="title">영업활동[<c:out value="9"/>]건</h3>
							<div class="btn_box">
								<button class="ui green basic button">엑셀</button>
								<button class="ui red basic button">초기화</button>
							</div>
						</div> --%>
						<div class="table_content">
							<table>
								<tr>	
									<th>진행단계</th>
									<th>건</th>
									<th>계약금액</th>
									<th>이익금액</th>
								</tr>
								<c:forEach var="list" items="${hmap.tablelist}">
									<tr>
										<td>
											<c:if test="${list.OPP_PROGRESS eq '1' }">
												진행중
											</c:if>
											<c:if test="${list.OPP_PROGRESS eq '2' }">
												종료(성공)
											</c:if>
											<c:if test="${list.OPP_PROGRESS eq '3' }">
												종료(실패)
											</c:if>
											<c:if test="${list.OPP_PROGRESS eq '4' }">
												보류 및 연기
											</c:if>
										</td>
										
										<td>
											<fmt:formatNumber value="${list.cnt }" pattern="#,###.##" />
											<%-- <c:out value="${list.cnt}"/> --%>
										</td>
										<td>
											<fmt:formatNumber value="${list.expRev }" pattern="#,###.##" />
											<%-- <c:out value="${list.expRev}"/> --%>
										</td>
										<td>
											<fmt:formatNumber value="${list.expPrice }" pattern="#,###.##" />
											<%-- <c:out value="${list.expPrice}"/> --%>
										</td>
									</tr>
								</c:forEach>
							</table>
						</div>
					</div><!-- table end -->
					<div >
						<div class="left border"><h2>영업단계</h2></div>
						<div >
							<c:set var="box" value="${hmap.box}"/>
							<ul class="chart">
								<c:forEach var="box" items="${box}">
									<c:if test="${box.stage eq '5'}">
										<li><span>계약</span> <p><c:out value="${box.cnt}"/>건</p> 
										<p>
											<fmt:formatNumber value="${box.expRev }" pattern="#,###.##" />
											<%-- <c:out value="${box.expRev}"/> --%>
										</p></li>
									</c:if>
									<c:if test="${box.stage eq '3'}">
										<li><span>협상</span> <p><c:out value="${box.cnt}"/>건</p>
										 <p>
										 	<fmt:formatNumber value="${box.expRev }" pattern="#,###.##" />
										 	<%-- <c:out value="${box.expRev}"/> --%>
										 </p></li>
									</c:if>
									<c:if test="${box.stage eq '2'}">
										<li><span>제안</span> <p><c:out value="${box.cnt}"/>건</p>
										 <p>
										 	<fmt:formatNumber value="${box.expRev }" pattern="#,###.##" />
										 	<%-- <c:out value="${box.expRev}"/> --%>
										 </p></li>
									</c:if>
									<c:if test="${box.stage eq '1'}">
										<li><span>인지</span> <p><c:out value="${box.cnt}"/>건</p>
										<p>
											<fmt:formatNumber value="${box.expRev }" pattern="#,###.##" />
											<%-- <c:out value="${box.expRev}"/> --%>
										</p></li>
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</div> <!-- 진행단계 -->
					<div >
					<div class="left border"><h2>영업담당</h2></div>
					<canvas id="canvas1" width="720" height="400"></canvas>
					<div class="left border"><h2>월</h2></div>
					<canvas id="canvas2" width="720" height="400"></canvas>
					</div> <!-- 영업담당 -->
				</div>
			</div>
		</div>
	<c:set var="chartE" value="${hmap.emp}"/>
	<c:set var="chartM" value="${hmap.month}"/>
		</main>
	</div>
	</div>
	</div>
	
	<script>
	$(function(){
		$("#statusData").click(function(){
			location.href="selectStatusData.so";
		})
	})
	
	function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
	var arrName = []
	var arrRev = []
	var arrPrice = []
	var arrCnt = []
	
	var arrMon = []
	var arrMRev = []
	var arrMPrice = []
	var arrMCnt = []
	<c:forEach var="mon" items="${chartM}">
		arrMon.push('<c:out value="${mon.MON}"/>월')
		arrMRev.push(Number(<c:out value="${mon.expRev}"/>))
		arrMPrice.push(Number(<c:out value="${mon.expPrice}"/>))
		arrMCnt.push(Number(<c:out value="${mon.cnt}"/>))
	</c:forEach>
		console.log("arrName : " + arrName);
		console.log("arrRev : " + arrRev);
		console.log("arrPrice : " + arrPrice);
		console.log("arrCnt : " + arrCnt);
	<c:forEach var="emp" items="${chartE}">
		arrName.push('<c:out value="${emp.EMP_NAME}"/>')
		arrRev.push(Number(<c:out value="${emp.expRev}"/>))
		arrPrice.push(Number(<c:out value="${emp.expPrice}"/>))
		arrCnt.push(Number(<c:out value="${emp.cnt}"/>))
	</c:forEach>
	console.log("arrName : " + arrName);
	console.log("arrRev : " + arrRev);
	console.log("arrPrice : " + arrPrice);
	console.log("arrCnt : " + arrCnt);
		$(function(){
			$(".tab li").click(function(){
				console.log("clicked");
				$(".tab li").removeClass("action");
				$(this).addClass("action");
			})	
		})
	
		$('#rangestart').calendar({
			  type: 'date',
			  endCalendar: $('#rangeend')
		});
		$('#rangeend').calendar({
			  type: 'date',
			  startCalendar: $('#rangestart')
		});
		
        $(function(){
            // 열리지 않는 메뉴
            $(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass("open");
            
            // 열리는 메뉴
            $(".menu_cnt .main_menu").eq(1).addClass("on").addClass("open");
            $(".menu_cnt .main_menu").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        });
        //전체 체크 기능
        var j = $("th input[type='checkbox']");
        j.click(function(){
        	if(j.is(":checked")){
        		$("td input[type='checkbox']").prop("checked", true);
        	} else{
        		$("td input[type='checkbox']").prop("checked", false);
        	}
        })
    	
        
        var chartData1 = {
			labels: arrName,
			datasets: [{
				type: 'line',
				label: '건',
				yAxisID: 'B',
				borderColor: window.chartColors.bgreen,
				borderWidth: 2,
				fill: false,
				data: arrCnt
			}, {
				type: 'bar',
				label: '예상계약금액',
				yAxisID: 'A',
				backgroundColor: window.chartColors.blue,
				data: arrRev,
				borderColor: 'white',
				borderWidth: 2
			}, {
				type: 'bar',
				label: '예상이익금액',
				yAxisID: 'A',
				backgroundColor: window.chartColors.black,
				data: arrPrice
			}]

		};
		
		var chartData2 = {
				labels: arrMon,
				datasets: [{
					type: 'line',
					label: '건',
					yAxisID: 'B',
					borderColor: window.chartColors.bgreen,
					borderWidth: 2,
					fill: false,
					data: arrMCnt
				}, {
					type: 'bar',
					label: '예상계약금액',
					yAxisID: 'A',
					backgroundColor: window.chartColors.blue,
					data: arrMRev,
					borderColor: 'white',
					borderWidth: 2
				}, {
					type: 'bar',
					label: '예상이익금액',
					yAxisID: 'A',
					backgroundColor: window.chartColors.black,
					data: arrMPrice
				}]

			};
			window.onload = function() {
				 
				var ctx1 = document.getElementById('canvas1').getContext('2d');
				window.myMixedChart = new Chart(ctx1, {
					type: 'bar',
					data: chartData1,
					options: {
						responsive: false,
						title: {
							display: true
						},
						tooltips: {
							mode: 'index',
							intersect: true
						},
						scales: {
							yAxes: [{
						        id: 'A',
						        type: 'linear',
						        position: 'left',
						      }, {
						        id: 'B',
						        type: 'linear',
						        position: 'right',
						        ticks: {
						          stepSize:4,
						          min: 0
						        }
						      }]
						}
					}
				});
				
				
				var ctx2 = document.getElementById('canvas2').getContext('2d');
				window.myMixedChart = new Chart(ctx2, {
					type: 'bar',
					data: chartData2,
					options: {
						responsive: false,
						title: { display: true },
						tooltips: {
							mode: 'index',
							intersect: false
						},
						scales: {
							yAxes: [{
						        id: 'A',
						        type: 'linear',
						        position: 'left',
						      }, {
						        id: 'B',
						        type: 'linear',
						        position: 'right',
						        ticks: {
						          stepSize:4,
						          min: 0
						        }
						      }]
						}
					}
				});
			};
			</script>
</body>
</html>