<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 고객현황</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/statusMenubar.jsp" />
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<style type="text/css">
   .talkbubble {
      width: 80px;
      height: 40px;
      background: white;
      border: 2px solid #6199B9;
      position: relative;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;
      border-radius: 10px;
      color: #6199B9;
    }
    .talkbubble{
	 text-align: center;
	 padding-top: 10px;
	 }
    .talkbubble:before {
      content: "";
      position: absolute;
      right: 45%;
      top: 38px;
      width: 0;
      height: 0;
      border-right: 5px solid transparent;
      border-left: 5px solid transparent;
      border-top: 10px solid #6199B9;
    }
     #talkbubbles {
     	width: fit-content;
     	margin: 0 auto;
     	margin-top: 100px;
     	height: 100px;
     }
    #talkbubbles > div {
		float: left;
    	margin: 22px;
    }
    #todaytalkbubble {
    	color: white;
    	background: #6199B9;
    }
    .circle {
    	width: 30px;
    	height: 30px;
    	background: white;
    	border: 10px solid #595959;
    	border-radius: 50px;
    	float: left;
    	margin-left: 45px;
    	margin-right: 49px;
    	
    }
    #line {
    	border-top: 3px solid #E1E6EB;
		width: 100%;
		height: 100px;
		text-align: center;
    }
    #circles {
		width: fit-content;
		height: 30px;
		margin-right: auto;
   		margin-left: auto;
   		transform:translateY(-16px)
    }
    #clientValue {
    	width: 100%;
    	height: 500px;
    }
    #clientValue > ul {
    	width: 100%;
    	height:  60px;
    }
    #clientValue  li {
    	width: 116px;
    	height: 40px;
    	float: left;
    	margin: 5px;
    	border: 1px solid #E1E6EB;
    	padding: 5px;
    }
    #clientValue p:nth-child(2) {
	float: right;
	font-weight: bold;
}
	.left.border{
 		margin-top:10px;
 		margin-bottom:10px;
 		border-left:3px solid #27AAE1;
 		padding-left:10px;
 	}
	#clientRank{
		margin-top: 30px;
		margin-left: auto;
		margin-top: 30px;
	}
	#clientRank  td:nth-child(even) {
		padding-right: 10px;
	}
	#clientRank  td:nth-child(1) {
		background: rgba(255, 99, 132, 0.2);
		color: red;
		border: 2px solid rgba(255, 99, 132, 1);
	}
	#clientRank  td:nth-child(3) {
		background: rgba(54, 162, 235, 0.2);
		color: blue;
	#clientRank td:nth-child(odd) {
		width: 55px;
		text-align: right;
		padding-right: 5px;
	}
</style>
</head>
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title "></div>
			<div id="header_Detail_div2">
				<div>
					<div class="two fields">
						<div class="field">
							<div class="ui calendar" id="rangestart">
								<div class="ui input right icon" id="customerStatus_calendar">
									<i class="calendar icon"></i> <input type="text"
										class="contract_input_calendar" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p id="exclamationMark2">~</p>
				<div>
					<div class="two fields">
						<div class="field">
							<div class="ui calendar" id="rangeend">
								<div class="ui input right icon" id="customerStatus_calendar">
									<i class="calendar icon"></i> <input type="text"
										class="contract_input_calendar" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
				<select id="customerStatus_select">
					<option>담당자 전체</option>
					<option></option>
					<option></option>
				</select>
				<button class="submit_Btn" id="customerStatus_search_Btn">검색</button>
			</div>
			<div id="customerStatus_search_div">
				<ul class="tabs tab_activity">
		       	<li style="width:50%"><a name="tab1" class="active" style="cursor:default"><i class="chart line icon"></i>그래프</a></li>
		       	<li style="width:50%"><a name="tab2" style="cursor:pointer" x-ng-click="moveTab('L');" href="showClientDataStatus.cs"><i class="align justify icon"></i>데이터</a></li>
	        </ul>
				<div>
					<b>단위 : 건/만원</b>
				</div>
				<div>
					<div class="left border">
						<h1>신규 고객</h1>
					</div>
					<c:set var="nowClient" value="${nowClientStatusValue}"/>
					<c:set var="lastMonthClient" value="${lastMonthClientSatusValue}"/>
					<c:set var="threeMonthClient" value="${threeMonthClientSatusValue}"/>
					<div id="talkbubbles">
						<div class="talkbubble">
							전월
						</div>
						<div id="todaytalkbubble" class="talkbubble">
							당월
						</div>
						<div class="talkbubble">
							평균(3개월)
						</div>
					</div>
					<div id="line">
						<div id="circles">
							<div class="circle"></div>
							<div class="circle"></div>
							<div class="circle"></div>
						</div>
						<div id="clientValue">
							<ul>
								<li><p>고객사</p><p><c:out value="${lastMonthClient.lastMonthCompany}"/></p></li>
								<li><p>고객사</p><p><c:out value="${nowClient.NowCompany}"/></p></li>
								<li><p>고객사</p><p><fmt:formatNumber value="${threeMonthClient.threeMonthCompany / 3}" maxFractionDigits="2"/></p></li>
							</ul>
							<ul>
								<li><p>고객</p><p><c:out value="${lastMonthClient.lastMonthClient}"/></p></li>
								<li><p>고객</p><p><c:out value="${nowClient.NowClient}"/></p></li>
								<li><p>고객</p><p><fmt:formatNumber value="${threeMonthClient.threeMonthClient / 3}" maxFractionDigits="2"/></p></li>
							</ul>
							<ul>
								<li><p>잠재고객</p><p><c:out value="${lastMonthClient.lastMonthLeadClient}"/></p></li>
								<li><p>잠재고객</p><p><c:out value="${nowClient.NowLeadClient}"/></p></li>
								<li><p>잠재고객</p><p><fmt:formatNumber  value="${threeMonthClient.threeMonthLeadClient / 3}" maxFractionDigits="2"/></p></li>
							</ul>
						</div>
					</div>
				</div>
				<div>
					<div class="left border">
						<h1>고객 등급</h1>
					</div>
					<div>
						<canvas id="myChart" height="200px;" style="margin-top:80px; padding-left:16px;"></canvas>
					</div>
					<%-- <table id="clientRank">
						<tr>
							<td>S등급 : </td>
							<td>${clientRankList[0].S}</td>
							<td>A등급 : </td>
							<td>${clientRankList[0].A}</td>
							<td>B등급 : </td>
							<td>${clientRankList[0].B}</td>
							<td>C등급 : </td>
							<td>${clientRankList[0].C}</td>
							<td>미지정 : </td>
							<td>${clientRankList[0].NONE}</td>
						</tr>
					</table> --%>
				</div>
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
	var S = ${clientRankList[0].S}
	var A = ${clientRankList[0].A}
	var B = ${clientRankList[0].B}
	var C = ${clientRankList[0].C}
	var NONE = ${clientRankList[0].NONE}
	var ctx = document.getElementById('myChart').getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'pie',
	    data: {
	        datasets: [{
	            data: [S, A, B, C, NONE],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
	            ],
	            borderWidth: 1
	        }],
	        labels: [
				'S등급',
				'A등급',
				'B등급',
				'C등급',
				'미지정'
			]
	    }
	});
	$(function() {
		// 열리지 않는 메뉴
		$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
				"open");

		// 열리는 메뉴
		$(".menu_cnt .main_menu").eq(0).addClass("on").addClass("open");
		$(".menu_cnt .main_menu").eq(0).find(".sub_menu_list").eq(0)
				.addClass("on");
	});
	</script>
</body>
</html>