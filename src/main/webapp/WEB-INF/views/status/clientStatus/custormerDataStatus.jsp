<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>셀모아 고객현황</title>
<link rel="stylesheet"
	href="${ contextPath }/resources/css/adh/adh_sales_Management.css">
<jsp:include page="../../common/statusMenubar.jsp" />
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<jsp:include page="../../common/modal/company.jsp"></jsp:include>
<jsp:include page="../../common/modal/opportunity.jsp"></jsp:include>
<jsp:include page="../../common/modal/manager.jsp"></jsp:include>
<style type="text/css">
   .talkbubble {
      width: 80px;
      height: 40px;
      background: white;
      border: 2px solid #6199B9;
      position: relative;
      -moz-border-radius: 10px;
      -webkit-border-radius: 10px;
      border-radius: 10px;
      color: #6199B9;
    }
    .talkbubble{
	 text-align: center;
	 padding-top: 10px;
	 }
    .talkbubble:before {
      content: "";
      position: absolute;
      right: 45%;
      top: 38px;
      width: 0;
      height: 0;
      border-right: 5px solid transparent;
      border-left: 5px solid transparent;
      border-top: 10px solid #6199B9;
    }
     #talkbubbles {
     	width: fit-content;
     	margin: 0 auto;
     	margin-top: 100px;
     	height: 100px;
     }
    #talkbubbles > div {
		float: left;
    	margin: 22px;
    }
    #todaytalkbubble {
    	color: white;
    	background: #6199B9;
    }
    .circle {
    	width: 30px;
    	height: 30px;
    	background: white;
    	border: 10px solid #595959;
    	border-radius: 50px;
    	float: left;
    	margin-left: 45px;
    	margin-right: 49px;
    	
    }
    #line {
    	border-top: 3px solid #E1E6EB;
		width: 100%;
		height: 100px;
		text-align: center;
    }
    #circles {
		width: fit-content;
		height: 30px;
		margin-right: auto;
   		margin-left: auto;
   		transform:translateY(-16px)
    }
    #clientValue {
    	width: 100%;
    	height: 500px;
    }
    #clientValue > ul {
    	width: 100%;
    	height:  60px;
    }
    #clientValue  li {
    	width: 116px;
    	height: 40px;
    	float: left;
    	margin: 5px;
    	border: 1px solid #E1E6EB;
    	padding: 5px;
    }
    #clientValue p:nth-child(2) {
	float: right;
	font-weight: bold;
}
	.data > div> h3{
		float: left;
	}
	.data > div {
		width: 100%;
		height: 40px;
	}
	.data table {
		width: 100%;
	}
	.data table th {
		text-align: center;
		vertical-align: middle;
		border: 1px solid #E1E6EB;
		height: 29px;
		background: #FBFCFD;
	}
	.data table td:nth-child(1) {
		width: 90px;
	}
	.data table td {
		text-align: center;
		border: 1px solid #E1E6EB;
		vertical-align: middle;
	}
</style>
</head>
<body>
	<div class="main_ctn" style="background: white;">
		<div class="title"></div>
		<div class="tb_cnt">
			<div class="tb_title "></div>
			<div id="header_Detail_div2">
				<div>
					<div class="two fields">
						<div class="field">
							<div class="ui calendar" id="rangestart">
								<div class="ui input right icon" id="customerStatus_calendar">
									<i class="calendar icon"></i> <input type="text"
										class="contract_input_calendar" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
				<p id="exclamationMark2">~</p>
				<div>
					<div class="two fields">
						<div class="field">
							<div class="ui calendar" id="rangeend">
								<div class="ui input right icon" id="customerStatus_calendar">
									<i class="calendar icon"></i> <input type="text"
										class="contract_input_calendar" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
				<select id="customerStatus_select">
					<option>담당자 전체</option>
					<option></option>
					<option></option>
				</select>
				<button class="submit_Btn" id="customerStatus_search_Btn">검색</button>
			</div>
			<c:set var="list" value="${list}"/>
			<c:set var="length" value="${length}"/>
			<div id="customerStatus_search_div">
				<ul class="tabs tab_activity">
		       	<li style="width:50%"><a name="tab1" style="cursor:default" href="showClientStatus.cs"><i class="chart line icon"></i>그래프</a></li>
		       	<li style="width:50%"><a name="tab2" class="active" style="cursor:pointer" x-ng-click="moveTab('L');"><i class="align justify icon"></i>데이터</a></li>
	        </ul>
				<div class="data">
					<div>
						<h3>목록</h3><p>(<c:out value="${length}"></c:out>건)</p>
					</div>
					<table>
						<thead>
							<tr>
								<th>고객사</th>
								<th>고객</th>
								<th>직책</th>
								<th>휴대폰번호</th>
								<th>유선번호</th>
								<th>메일</th>
								<th>고객등급</th>
								<th>담당자</th>
								<th>등록일</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" varStatus="status">
								<tr>
									<td><c:out value="${list[status.index].CLIENT_NAME}"/></td>
									<td><c:out value="${list[status.index].COM_NAME}"/></td>
									<td><c:out value="${list[status.index].JOB}"/></td>
									<td><c:out value="${list[status.index].PHONE}"/></td>
									<td><c:out value="${list[status.index].TEL}"/></td>
									<td><c:out value="${list[status.index].EMAIL}"/></td>
									<td><c:out value="${list[status.index].RANK}"/></td>
									<td><c:out value="${list[status.index].EMP_NO}"/></td>
									<td><c:out value="${fn:split(list[status.index].ENROLL_DATE, ' ')[0]}"/></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</main>
	</div>
	</div>
	</div>
	<script>
	$(function() {
		// 열리지 않는 메뉴
		$(".menu_cnt .main_menu").eq(2).removeClass("on").removeClass(
				"open");

		// 열리는 메뉴
		$(".menu_cnt .main_menu").eq(0).addClass("on").addClass("open");
		$(".menu_cnt .main_menu").eq(0).find(".sub_menu_list").eq(0)
				.addClass("on");
	});
	</script>
</body>
</html>