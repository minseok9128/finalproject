<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
   
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
   <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath}/resources/css/common.css">

<style>
.searchArea {
   width: 100%;
   background-color: white;
   border: 1px solid #D7DBDF;
   margin-top: auto;
   margin-bottom: auto;
}


#calendarWid {
   width: 350px;
   margin-top: 10px;
   margin-bottom: 10px;
   margin-left: 10px;
}

.calLabel {
   margin-left: 10px;
   margin-right: 10px;
   margin-top: auto;
   margin-bottom: auto;
}

#selctSales {
   width: 250px;
   margin-left: 15px;
}

#line {
   display: inline-block;
}

#searchBtn {
   float: right;
   margin-top: 15px;
   margin-bottom: 15px;
   margin-right: 15px;
}

.selectArea {
   margin-top: 30px;
}

.graphArea {
   border-bottom: 1px solid black;
   font-size: 12px;
   color: #667277;
   text-align: center;
   padding: 10px;
}

.dataArea {
   border-bottom: 1px solid #E1E6EB;
   font-size: 12px;
   color: #E1E6EB;
   text-align: center;
   padding: 10px;
}

.firstArea {
   width: 49%;
}

.secondArea {
   width: 49%;
}

.images {
   width: 70px;
   height: 70px;
}

.summary {
   align: center;
   display: inline-block;
   margin-right: 15px;
   margin-top: 40px;
}

.explan {
   margin-top: 5px;
   margin-bottm: 5px;
}

.summaryCount {
   background-color: #797979;
   border-radius: 3px;
   font-size: 11px;
   color: white;
   height: 18px;
   width: 70px;
   vertical-align: middle;
   margin-left: auto;
   margin-right: auto;
}

.Area {
   width: 370px;
   height: 373px;
}

.Area2 {
   width: 600px;
   height: 373px;
}

.explanTitle {
   font-size: 14px;
   font-weight: bold;
   margin-top: 30px;
}

.explanHeader {
   margin-left: 10px;
   text-align: left;
}

.tab li:hover{
 		border-bottom:1px solid #000000;
 		color:#000000;
 	}
 	.tab li {
 		width:49%;
 		display: inline-block;
 		border-bottom:1px solid #939393;
 		text-align:center;	
 		color:#939393;
 	}
 	.left.border{
 		margin-top:10px;
 		margin-bottom:10px;
 		border-left:3px solid #27AAE1;
 		padding-left:10px;
 	}
 	.panel{
 		border:1px solid black;
 		margin-bottom:30px;
 	}
 	.panel ul {
 		width:auto;
 	}
 	.panel ul li {
 		width:24.5%;
 		border-bottom:0;
 		padding-top:5px;
 		padding-bottom:5px;
 		
 		display: inline-block;
 		border-bottom:1px solid #939393;
 		text-align:center;	
 		color:#939393;
 	}
 	.panel ul li:not(:first-child) {
 		border-left:1px solid #939393;
 	}
 	
 	.line{
 		text-decoration:underline;
 		cursor:pointer;
 		color:#27AAE1;
 	}
</style>
<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">
<!-- chart.js -->

   
   <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<script>
	'use strict';

	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		black: 'rgb(67, 67, 72)',
		bgreen: 'rgb(147, 235, 130)',
		grey: 'rgb(201, 203, 207)' 
	};

	(function(global) {
		var MONTHS = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		];

		var COLORS = [
			'#4dc9f6',
			'#f67019',
			'#f53794',
			'#537bc4',
			'#acc236',
			'#166a8f',
			'#00a950',
			'#58595b',
			'#8549ba'
		];

		var Samples = global.Samples || (global.Samples = {});
		var Color = global.Color;

		Samples.utils = {
			// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
			srand: function(seed) {
				this._seed = seed;
			},

			rand: function(min, max) {
				var seed = this._seed;
				min = min === undefined ? 0 : min;
				max = max === undefined ? 1 : max;
				this._seed = (seed * 9301 + 49297) % 233280;
				return min + (this._seed / 233280) * (max - min);
			},

			numbers: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 1;
				var from = cfg.from || [];
				var count = cfg.count || 8;
				var decimals = cfg.decimals || 8;
				var continuity = cfg.continuity || 1;
				var dfactor = Math.pow(10, decimals) || 0;
				var data = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = (from[i] || 0) + this.rand(min, max);
					if (this.rand() <= continuity) {
						data.push(Math.round(dfactor * value) / dfactor);
					} else {
						data.push(null);
					}
				}

				return data;
			},

			labels: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 100;
				var count = cfg.count || 8;
				var step = (max - min) / count;
				var decimals = cfg.decimals || 8;
				var dfactor = Math.pow(10, decimals) || 0;
				var prefix = cfg.prefix || '';
				var values = [];
				var i;

				for (i = min; i < max; i += step) {
					values.push(prefix + Math.round(dfactor * i) / dfactor);
				}

				return values;
			},

			months: function(config) {
				var cfg = config || {};
				var count = cfg.count || 12;
				var section = cfg.section;
				var values = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = MONTHS[Math.ceil(i) % 12];
					values.push(value.substring(0, section));
				}

				return values;
			},

			color: function(index) {
				return COLORS[index % COLORS.length];
			},

			transparentize: function(color, opacity) {
				var alpha = opacity === undefined ? 0.5 : 1 - opacity;
				return Color(color).alpha(alpha).rgbString();
			}
		};

		// DEPRECATED
		window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(0, 100));
	};

		// INITIALIZATION

		Samples.utils.srand(Date.now());

		// Google Analytics
		/* eslint-disable */
		if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-28909194-3', 'auto');
			ga('send', 'pageview');
		}
		/* eslint-enable */

	}(this));
	</script>
</head>

<body>
   <jsp:include page="../../common/statusMenubar.jsp" />

   <div class="main_ctn" style="background: white">
      <div class="searchArea" style="background: white; margin-bottom:50px;">
         <div class="ui form" id="line">
            <div class="two fields" id="calendarWid">
               <div class="field">
                  <label></label>
                  <div class="ui calendar" id="rangestart">
                     <div class="ui input left icon">
                        <i class="calendar icon"></i> <input type="text"
                           placeholder="2020.01.01">
                     </div>
                  </div>
               </div>
               <label class="calLabel">~</label>
               <div class="field">
                  <label></label>
                  <div class="ui calendar" id="rangeend">
                     <div class="ui input left icon">
                        <i class="calendar icon"></i> <input type="text"
                           placeholder="2020.01.01">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         
          <c:set var="oppList" value="${oppList}"/>
         

         <select class="ui dropdown" id="selctSales">
            <option value="">담당자 전체</option>
            <c:forEach var="i" items="${oppList}" varStatus="status">
            <option value="<c:out value="${oppList[status.index].employee.empId}"/>"><c:out  value="${oppList[status.index].employee.empName}"/></option>
           
            </c:forEach>
         </select>

         <button class="medium ui inverted primary button" id="searchBtn">검색</button>



      </div>
      
      <c:set var="oppTotalCount" value="${oppTotalCount}"/>
      
   					<ul class="tabs tab_activity" style="margin-bottom:50px;">
						<li class="action"><a href="showContractStatus.scs" name="tab1" class="active" style="cursor:default"><i class="chart line icon"></i>그래프</a></li>
						<li><a href="dataContractStatus.scs" name="tab2" style="cursor:pointer" x-ng-click="moveTab('L');"><i class="align justify icon" ></i>데이터</a></li>
					</ul>
					<div class="left border"><h3>요약</h3></div>
					
					<div class="panel"> 
						<ul class="no">
						<li><p>전체</p><c:out value="${oppTotalCount}"/></li>
						
						<li><p>계약금액</p> <fmt:formatNumber value="${opp.expectRev}" pattern="#,###" /></li>
						<li><p>예상이익률</p><c:out value="${opp.expectProfitRate}"/>%</li>
						<li><p>이익금액</p> <fmt:formatNumber value="${opp.expectProfitPrice}" pattern="#,###" /></li>
					</ul>	
					</div>
					
					<div >
					<div class="left border"><h3>영업담당</h3></div>
					<canvas id="canvas1" width="720" height="400"></canvas>
					<div class="left border"><h3>월</h3></div>
					<canvas id="canvas2" width="720" height="400"></canvas>
					</div>

      
     	 			<c:set var="chartM" value="${hmap.month}"/>
      

   </div>
   <script>
   
   var arrName = []
   var arrRev = []
   var arrRate = []
   var arrPrice = []
   var arrCount = []
   
   var arrMon=[]
   var arrMRev=[]
   var arrMPrice=[]
   var arrMCnt=[]
   
   		<c:forEach var="mon" items="${chartM}" >
			arrMon.push('<c:out value="${mon.MON}"/>월')
			arrMRev.push(Number(<c:out value="${mon.expRev}"/>))
			arrMPrice.push(Number(<c:out value="${mon.expPrice}"/>))
			arrMCnt.push(Number(<c:out value="${mon.cnt}"/>))
		</c:forEach>
			
			console.log(arrMon);
			console.log(arrMRev);
			console.log(arrMPrice);
			console.log(arrMCnt);
			
		
   		<c:forEach var="i" items="${oppList}" varStatus="status">
			arrName.push('<c:out value="${oppList[status.index].employee.empName}"/>')
			arrRev.push(Number(<c:out value="${oppList[status.index].expectRev}"/>))
			arrRate.push(Number(<c:out value="${oppList[status.index].expectProfitRate}"/>))
			arrPrice.push(Number(<c:out value="${oppList[status.index].expectProfitPrice}"/>))
			arrCount.push(Number(<c:out value="${oppList[status.index].count}"/>))
		</c:forEach>
		
			
			
	
   
		$(function(){
			
			$(".tab li").click(function(){
				console.log("clicked");
				$(".tab li").removeClass("action");
				$(this).addClass("action");
			})
			
			  $(".menu_cnt .main_menu").eq(5).addClass("on").addClass("open");
	         $('#rangestart').calendar({
	         type : 'date',
	         endCalendar : $('#rangeend')
	      });
	         
	      $('#rangeend').calendar({
	         type : 'date',
	         startCalendar : $('#rangestart')
	      });
	      
	      
			 
		})
	
 
   var chartData1 = {
						
					labels: arrName,
					datasets: [{
						type: 'line',
						label: '건',
						borderColor: window.chartColors.bgreen,
						borderWidth: 2,
						fill: false,
						 yAxisID: 'A',
						data: arrCount
					}, {
						type: 'bar',
						 yAxisID: 'B',
						label: '예상계약금액',
						backgroundColor: window.chartColors.blue,
						data: arrRev,
						borderColor: 'white',
						borderWidth: 2
					}, {
						type: 'bar',
						label: '예상이익금액',
						 yAxisID: 'B',
						backgroundColor: window.chartColors.black,
						data: arrPrice
					}]

				};
 	
 	
	
	var chartData2 = {
			labels: arrMon,
			datasets: [{
				type: 'line',
				label: '건',
				yAxisID: 'B',
				borderColor: window.chartColors.bgreen,
				borderWidth: 2,
				fill: false,
				data: arrMCnt
			}, {
				type: 'bar',
				label: '예상계약금액',
				backgroundColor: window.chartColors.blue,
				yAxisID: 'A',
				data: arrMRev,
				borderColor: 'white',
				borderWidth: 2
			}, {
				type: 'bar',
				label: '예상이익금액',
				yAxisID: 'A',
				backgroundColor: window.chartColors.black,
				data: arrMPrice
			}]

		};
		window.onload = function() {
			 
			var ctx1 = document.getElementById('canvas1').getContext('2d');
			window.myMixedChart = new Chart(ctx1, {
				type: 'bar',
				data: chartData1,
				options: {
					responsive: false,
					title: {
						display: true
					},
					tooltips: {
						mode: 'index',
						intersect: true
				}, scales: {
					 yAxes: [{
				           id: 'A',
				           type: 'linear',
				           position: 'left',
				           ticks: {
					             
					             stepSize:1,
					             min: 0
					           }
				         }, {
				           id: 'B',
				           type: 'linear',
				           position: 'right' 
				           
				           
				         }]
			
				}
				}
			});
			
		
			
			var ctx2 = document.getElementById('canvas2').getContext('2d');
			window.myMixedChart = new Chart(ctx2, {
				type: 'bar',
				data: chartData2,
				options: {
					responsive: false,
					title: {
						display: true
					},
					tooltips: {
						mode: 'index',
						intersect: true
					},
					scales: {
						yAxes: [{
					        id: 'A',
					        type: 'linear',
					        position: 'left',
					      }, {
					        id: 'B',
					        type: 'linear',
					        position: 'right',
					        ticks: {
					          stepSize:4,
					          min: 0
					        }
					      }]
					}
				}
			});
		};
   
 /*   jQuery(document).ready(function() {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    }); */
     
      
       
     
     
      </script>
      
        
</body>
</html>