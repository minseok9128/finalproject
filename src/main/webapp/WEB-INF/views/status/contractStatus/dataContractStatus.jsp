<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
   
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${contextPath}/resources/css/common.css">

<style>
.searchArea {
   width: 100%;
   background-color: white;
   border: 1px solid #D7DBDF;
   margin-top: auto;
   margin-bottom: auto;
}


#calendarWid {
   width: 350px;
   margin-top: 10px;
   margin-bottom: 10px;
   margin-left: 10px;
}

.calLabel {
   margin-left: 10px;
   margin-right: 10px;
   margin-top: auto;
   margin-bottom: auto;
}

#selctSales {
   width: 250px;
   margin-left: 15px;
}

#line {
   display: inline-block;
}

#searchBtn {
   float: right;
   margin-top: 15px;
   margin-bottom: 15px;
   margin-right: 15px;
}

.selectArea {
   margin-top: 30px;
}

.graphArea {
   border-bottom: 1px solid black;
   font-size: 12px;
   color: #667277;
   text-align: center;
   padding: 10px;
}

.dataArea {
   border-bottom: 1px solid #E1E6EB;
   font-size: 12px;
   color: #E1E6EB;
   text-align: center;
   padding: 10px;
}

.firstArea {
   width: 49%;
}

.secondArea {
   width: 49%;
}

.images {
   width: 70px;
   height: 70px;
}

.summary {
   align: center;
   display: inline-block;
   margin-right: 15px;
   margin-top: 40px;
}

.explan {
   margin-top: 5px;
   margin-bottm: 5px;
}

.summaryCount {
   background-color: #797979;
   border-radius: 3px;
   font-size: 11px;
   color: white;
   height: 18px;
   width: 70px;
   vertical-align: middle;
   margin-left: auto;
   margin-right: auto;
}

.Area {
   width: 370px;
   height: 373px;
}

.Area2 {
   width: 600px;
   height: 373px;
}

.explanTitle {
   font-size: 14px;
   font-weight: bold;
   margin-top: 30px;
}

.explanHeader {
   margin-left: 10px;
   text-align: left;
}

.tab li:hover{
 		border-bottom:1px solid #000000;
 		color:#000000;
 	}
 	.tab li {
 		width:49%;
 		display: inline-block;
 		border-bottom:1px solid #939393;
 		text-align:center;	
 		color:#939393;
 	}
 	.left.border{
 		margin-top:10px;
 		margin-bottom:10px;
 		border-left:3px solid #27AAE1;
 		padding-left:10px;
 	}
 	.panel{
 		border:1px solid black;
 		margin-bottom:30px;
 	}
 	.panel ul {
 		width:auto;
 	}
 	.panel ul li {
 		width:24.5%;
 		border-bottom:0;
 		padding-top:5px;
 		padding-bottom:5px;
 		
 		display: inline-block;
 		border-bottom:1px solid #939393;
 		text-align:center;	
 		color:#939393;
 	}
 	.panel ul li:not(:first-child) {
 		border-left:1px solid #939393;
 	}
 	
 	.line{
 		text-decoration:underline;
 		cursor:pointer;
 		color:#27AAE1;
 	}
 	table{
	 	width:100%;
	 	height:150%;
	}
 	.th {
		border-bottom: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;
		border-right: 1px solid #DDDDDD;
		
		vertical-align: middle;
		color: #00A1DB;
	}
	tr > td {
		height:10px;
		vertical-align:middle;
		text-align:center;
	}
	.border2 {
		border:1px solid #DDDDDD;
	}
</style>
<link rel="stylesheet" href="${ contextPath }/resources/css/sbs/sbs.css">


</head>

<body>
   <jsp:include page="../../common/statusMenubar.jsp" />

   <div class="main_ctn" style="background: white">
      <div class="searchArea" style="background: white; margin-bottom:50px;">
         <div class="ui form" id="line">
            <div class="two fields" id="calendarWid">
               <div class="field">
                  <label></label>
                  <div class="ui calendar" id="rangestart">
                     <div class="ui input left icon">
                        <i class="calendar icon"></i> <input type="text"
                           placeholder="2020.01.01">
                     </div>
                  </div>
               </div>
               <label class="calLabel">~</label>
               <div class="field">
                  <label></label>
                  <div class="ui calendar" id="rangeend">
                     <div class="ui input left icon">
                        <i class="calendar icon"></i> <input type="text"
                           placeholder="2020.01.01">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         
       
         

         <select class="ui dropdown" id="selctSales">
           <option value="">담당자 전체</option>
            <c:forEach var="i" items="${oppList}" varStatus="status">
            <option value="<c:out value="${oppList[status.index].employee.empId}"/>"><c:out  value="${oppList[status.index].employee.empName}"/></option>
           
            </c:forEach>
            
         </select>
         
         

         <button class="medium ui inverted primary button" id="searchBtn">검색</button>



        </div>
      
   
      
  		 <ul class="tabs tab_activity"  style="margin-bottom:50px;">
				<li class="action"><a href="showContractStatus.scs"  name="tab2" style="cursor:pointer" x-ng-click="moveTab('L');"><i class="chart line icon"></i>그래프</a></li>
				<li><a href="dataContractStatus.scs" name="tab1" class="active" style="cursor:default"><i class="align justify icon" ></i>데이터</a></li>
		</ul>
		
		<c:set var="oppTotalCount" value="${oppTotalCount}"/>
					
		<div class="left border"><h3>목록(<c:out value="${oppTotalCount}"/>)</h3></div>
		
		<div style="overflow: auto; height: 400px; width: 800px; border: 1px solid #E1E6EB;">
			<table style="border-collapse: collapse; border-spacing: 0; border-bottom: 1px solid #DFDBDF;">
				<tr>
					<th class="th" style="width:40%;">영업기회</th>
					<th class="th" style="width:30%;">고객사</th>
					<th class="th" style="width:30%;">고객</th>
					
				</tr>
				
				 <c:forEach var="oppList2" items="${oppList2}"> 
					<tr style="text-align:center; vertical-align:middle; border-bottom: 1px solid #DFDBDF;">
						<td>
						<input type="hidden" value="<c:out value="${oppList2.oppId}"/>">
							<c:out value="${oppList2.oppName}"/>
						</td>
						<td class="border2">
							
						</td>
						<td class="border2">
							
						</td>
						
						
						
						
					</tr>
				 </c:forEach> 
			</table>
		</div>
					
			
					
					

     

      
      

   </div>
   <script>
 
		$(function(){
			
			$(".tab li").click(function(){
				console.log("clicked");
				$(".tab li").removeClass("action");
				$(this).addClass("action");
			})
			
			  $(".menu_cnt .main_menu").eq(5).addClass("on").addClass("open");
	         $('#rangestart').calendar({
	         type : 'date',
	         endCalendar : $('#rangeend')
	      });
	         
	      $('#rangeend').calendar({
	         type : 'date',
	         startCalendar : $('#rangestart')
	      });
	      
	      
			 
		})
	
      </script>
      
        
</body>
</html>