<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>
	<c:set var="contextPath" value="${pageContext.servletContext.contextPath}" scope="application"/>

 	<c:if test="${ (!empty sessionScope.loginEmp) and (sessionScope.loginEmp.empType eq '1'  or sessionScope.loginEmp.empType eq '2' ) }">
		<jsp:forward page="WEB-INF/views/main/adminMain.jsp"></jsp:forward>	
	</c:if>
	<c:if test="${ (!empty sessionScope.loginEmp) and (sessionScope.loginEmp.empType eq '3' ) }">
		<jsp:forward page="WEB-INF/views/main/salesMain.jsp"></jsp:forward>	
	</c:if>

	<c:if test="${ empty sessionScope.loginEmp }">
		<jsp:forward page="WEB-INF/views/common/login.jsp" />
	</c:if>
	
	
	

</body>
</html>