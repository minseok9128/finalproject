$(function(){
    $(".main_menu.add>a").on("click", function(){
        if($(this).parent(".main_menu.add").hasClass("open")){
            $(this).parent(".main_menu.add").removeClass("open");
            //$(this).css({"background":"white","color":"#929FA2"});
        }else{
            $(this).parent(".main_menu.add").removeClass("open");
            $(this).parent(".main_menu.add").siblings(".main_menu").removeClass("open");
            $(this).parent(".main_menu.add").addClass("open");
            //$(this).css({"background":"#6199B9","color":"white"});
        }
        $(this).parent(".main_menu.add").siblings(".main_menu").find(".sub_menu").slideUp();
        $(this).parent(".main_menu.add").find(".sub_menu ").slideToggle();
    });
})